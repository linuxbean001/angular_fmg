To run front

```sh
nvm exec yarn start
```


To build and deploy


```sh
./mvnw package -Pprod -DskipTests && boxfuse run -env=prod
```
