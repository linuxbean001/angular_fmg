package com.fmg.cma.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Cma.
 * <p>
 * Properties are configured in the application.yml file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

    private final RememberMe rememberMe = new RememberMe();

    public RememberMe getRememberMe() {
        return rememberMe;
    }

    public static class RememberMe {

        private boolean visible = true;

        public boolean isVisible() {
            return visible;
        }

        public void setVisible(boolean visible) {
            this.visible = visible;
        }
    }
}
