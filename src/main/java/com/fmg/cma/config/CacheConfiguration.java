package com.fmg.cma.config;

import io.github.jhipster.config.JHipsterProperties;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.ehcache.jsr107.Eh107Configuration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
@EnableCaching
@AutoConfigureAfter(value = {MetricsConfiguration.class})
@AutoConfigureBefore(value = {WebConfigurer.class, DatabaseConfiguration.class})
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(Expirations.timeToLiveExpiration(Duration.of(ehcache.getTimeToLiveSeconds(), TimeUnit.SECONDS)))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(com.fmg.cma.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(com.fmg.cma.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.UserCompany.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.ClientCompanyStatus.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.ClientCompany.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.ClientCampaignTemplate.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.ClientCampaign.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.ClientList.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.ClientList.class.getName() + ".portalUsers", jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.Field.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.ClientField.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.ClientCampaign.class.getName() + ".managers", jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.ClientCampaign.class.getName() + ".users", jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.ClientCampaign.class.getName() + ".qaUsers", jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.ClientListData.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.ClientListRowNote.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.Outcome.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.CampaignPortalUser.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.CampaignPortalUserOutcome.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.EmailTemplate.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.ClientCampaign.class.getName() + ".emailTemplates", jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.ClientCampaign.class.getName() + ".clientLists", jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.OutcomeType.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.ClientCampaignScript.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.ClientCampaignSms.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.AppointmentAttendee.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.SurveyQuestion.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.SurveyQuestionField.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.SurveyQuestion.class.getName() + ".fields", jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.BookingQuestion.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.BookingQuestionField.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.BookingQuestion.class.getName() + ".fields", jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.ClientCampaignAddDoc.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.CallHistoryType.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.CallHistory.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.Industry.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.OutcomeSubReason.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.CampaignListContactResult.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.CampaignListContactQuestionResult.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.CampaignListContactManagement.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.ConsoleStats.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.Alert.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.Alert.class.getName() + ".clientCampaigns", jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.Alert.class.getName() + ".users", jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.AlertUser.class.getName(), jcacheConfiguration);
            cm.createCache(com.fmg.cma.domain.Alert.class.getName() + ".alertUsers", jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
