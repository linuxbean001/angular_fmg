package com.fmg.cma.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Alert.
 */
@Entity
@Table(name = "alert")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Alert implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "stamp")
    private ZonedDateTime stamp;

    @Column(name = "description")
    private String description;

    @Column(name = "jhi_type")
    private String type;

    @Column(name = "status")
    private String status;

    @Column(name = "contact_row_nums")
    private String contactRowNums;

    @ManyToOne
    private ClientCampaign clientCampaign;

    @ManyToOne
    private ClientList clientList;

    @OneToMany(mappedBy = "alert")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<AlertUser> alertUsers = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getStamp() {
        return stamp;
    }

    public Alert stamp(ZonedDateTime stamp) {
        this.stamp = stamp;
        return this;
    }

    public void setStamp(ZonedDateTime stamp) {
        this.stamp = stamp;
    }

    public String getDescription() {
        return description;
    }

    public Alert description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public Alert type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public Alert status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContactRowNums() {
        return contactRowNums;
    }

    public Alert contactRowNums(String contactRowNums) {
        this.contactRowNums = contactRowNums;
        return this;
    }

    public void setContactRowNums(String contactRowNums) {
        this.contactRowNums = contactRowNums;
    }

    public ClientCampaign getClientCampaign() {
        return clientCampaign;
    }

    public Alert clientCampaign(ClientCampaign clientCampaign) {
        this.clientCampaign = clientCampaign;
        return this;
    }

    public void setClientCampaign(ClientCampaign clientCampaign) {
        this.clientCampaign = clientCampaign;
    }

    public ClientList getClientList() {
        return clientList;
    }

    public Alert clientList(ClientList clientList) {
        this.clientList = clientList;
        return this;
    }

    public void setClientList(ClientList clientList) {
        this.clientList = clientList;
    }

    public Set<AlertUser> getAlertUsers() {
        return alertUsers;
    }

    public Alert alertUsers(Set<AlertUser> alertUsers) {
        this.alertUsers = alertUsers;
        return this;
    }

    public Alert addAlertUser(AlertUser alertUser) {
        this.alertUsers.add(alertUser);
        alertUser.setAlert(this);
        return this;
    }

    public Alert removeAlertUser(AlertUser alertUser) {
        this.alertUsers.remove(alertUser);
        alertUser.setAlert(null);
        return this;
    }

    public void setAlertUsers(Set<AlertUser> alertUsers) {
        this.alertUsers = alertUsers;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Alert alert = (Alert) o;
        if (alert.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), alert.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Alert{" +
            "id=" + getId() +
            ", stamp='" + getStamp() + "'" +
            ", description='" + getDescription() + "'" +
            ", type='" + getType() + "'" +
            ", status='" + getStatus() + "'" +
            ", contactRowNums='" + getContactRowNums() + "'" +
            "}";
    }
}
