package com.fmg.cma.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A AppointmentAttendee.
 */
@Entity
@Table(name = "appointment_attendee")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AppointmentAttendee implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "contact_detail")
    private String contactDetail;

    @Column(name = "domain")
    private String domain;

    @Column(name = "username")
    private String username;

    @Column(name = "jhi_password")
    private String password;

    @Column(name = "appointment_days")
    private String appointmentDays;

    @ManyToOne
    private ClientCampaign clientCampaign;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public AppointmentAttendee name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactDetail() {
        return contactDetail;
    }

    public AppointmentAttendee contactDetail(String contactDetail) {
        this.contactDetail = contactDetail;
        return this;
    }

    public void setContactDetail(String contactDetail) {
        this.contactDetail = contactDetail;
    }

    public String getDomain() {
        return domain;
    }

    public AppointmentAttendee domain(String domain) {
        this.domain = domain;
        return this;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getUsername() {
        return username;
    }

    public AppointmentAttendee username(String username) {
        this.username = username;
        return this;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public AppointmentAttendee password(String password) {
        this.password = password;
        return this;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAppointmentDays() {
        return appointmentDays;
    }

    public AppointmentAttendee appointmentDays(String appointmentDays) {
        this.appointmentDays = appointmentDays;
        return this;
    }

    public void setAppointmentDays(String appointmentDays) {
        this.appointmentDays = appointmentDays;
    }

    public ClientCampaign getClientCampaign() {
        return clientCampaign;
    }

    public AppointmentAttendee clientCampaign(ClientCampaign clientCampaign) {
        this.clientCampaign = clientCampaign;
        return this;
    }

    public void setClientCampaign(ClientCampaign clientCampaign) {
        this.clientCampaign = clientCampaign;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AppointmentAttendee appointmentAttendee = (AppointmentAttendee) o;
        if (appointmentAttendee.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), appointmentAttendee.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AppointmentAttendee{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", contactDetail='" + getContactDetail() + "'" +
            ", domain='" + getDomain() + "'" +
            ", username='" + getUsername() + "'" +
            ", password='" + getPassword() + "'" +
            ", appointmentDays='" + getAppointmentDays() + "'" +
            "}";
    }
}
