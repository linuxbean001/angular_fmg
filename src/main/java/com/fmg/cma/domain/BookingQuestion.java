package com.fmg.cma.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A BookingQuestion.
 */
@Entity
@Table(name = "booking_question")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class BookingQuestion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "question_text")
    private String questionText;

    @Column(name = "jhi_order")
    private Long order;

    @Column(name = "is_visible")
    private Boolean isVisible;

    @OneToMany(mappedBy = "bookingQuestion")
    // @JsonIgnore
    // @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<BookingQuestionField> fields = new HashSet<>();

    @ManyToOne
    private ClientCampaign clientCampaign;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public BookingQuestion name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuestionText() {
        return questionText;
    }

    public BookingQuestion questionText(String questionText) {
        this.questionText = questionText;
        return this;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public Long getOrder() {
        return order;
    }

    public BookingQuestion order(Long order) {
        this.order = order;
        return this;
    }

    public void setOrder(Long order) {
        this.order = order;
    }

    public Boolean isIsVisible() {
        return isVisible;
    }

    public BookingQuestion isVisible(Boolean isVisible) {
        this.isVisible = isVisible;
        return this;
    }

    public void setIsVisible(Boolean isVisible) {
        this.isVisible = isVisible;
    }

    public Set<BookingQuestionField> getFields() {
        return fields;
    }

    public BookingQuestion fields(Set<BookingQuestionField> bookingQuestionFields) {
        this.fields = bookingQuestionFields;
        return this;
    }

    public BookingQuestion addFields(BookingQuestionField bookingQuestionField) {
        this.fields.add(bookingQuestionField);
        bookingQuestionField.setBookingQuestion(this);
        return this;
    }

    public BookingQuestion removeFields(BookingQuestionField bookingQuestionField) {
        this.fields.remove(bookingQuestionField);
        bookingQuestionField.setBookingQuestion(null);
        return this;
    }

    public void setFields(Set<BookingQuestionField> bookingQuestionFields) {
        this.fields = bookingQuestionFields;
    }

    public ClientCampaign getClientCampaign() {
        return clientCampaign;
    }

    public BookingQuestion clientCampaign(ClientCampaign clientCampaign) {
        this.clientCampaign = clientCampaign;
        return this;
    }

    public void setClientCampaign(ClientCampaign clientCampaign) {
        this.clientCampaign = clientCampaign;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BookingQuestion bookingQuestion = (BookingQuestion) o;
        if (bookingQuestion.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bookingQuestion.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BookingQuestion{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", questionText='" + getQuestionText() + "'" +
            ", order=" + getOrder() +
            ", isVisible='" + isIsVisible() + "'" +
            "}";
    }
}
