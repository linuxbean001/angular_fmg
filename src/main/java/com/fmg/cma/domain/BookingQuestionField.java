package com.fmg.cma.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

/**
 * A BookingQuestionField.
 */
@Entity
@Table(name = "booking_question_field")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class BookingQuestionField implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "data_type")
    private String dataType;

    @Column(name = "category")
    private String category;

    @Column(name = "pre_population_data")
    private String prePopulationData;

    @Column(name = "is_visible")
    private Boolean isVisible;

    @Column(name = "is_required")
    private Boolean isRequired;

    @ManyToOne
    private BookingQuestion bookingQuestion;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public BookingQuestionField name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDataType() {
        return dataType;
    }

    public BookingQuestionField dataType(String dataType) {
        this.dataType = dataType;
        return this;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getCategory() {
        return category;
    }

    public BookingQuestionField category(String category) {
        this.category = category;
        return this;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPrePopulationData() {
        return prePopulationData;
    }

    public BookingQuestionField prePopulationData(String prePopulationData) {
        this.prePopulationData = prePopulationData;
        return this;
    }

    public void setPrePopulationData(String prePopulationData) {
        this.prePopulationData = prePopulationData;
    }

    public Boolean isIsVisible() {
        return isVisible;
    }

    public BookingQuestionField isVisible(Boolean isVisible) {
        this.isVisible = isVisible;
        return this;
    }

    public void setIsVisible(Boolean isVisible) {
        this.isVisible = isVisible;
    }

    public Boolean isIsRequired() {
        return isRequired;
    }

    public BookingQuestionField isRequired(Boolean isRequired) {
        this.isRequired = isRequired;
        return this;
    }

    public void setIsRequired(Boolean isRequired) {
        this.isRequired = isRequired;
    }

    public BookingQuestion getBookingQuestion() {
        return bookingQuestion;
    }

    public BookingQuestionField bookingQuestion(BookingQuestion bookingQuestion) {
        this.bookingQuestion = bookingQuestion;
        return this;
    }

    public void setBookingQuestion(BookingQuestion bookingQuestion) {
        this.bookingQuestion = bookingQuestion;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BookingQuestionField bookingQuestionField = (BookingQuestionField) o;
        if (bookingQuestionField.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bookingQuestionField.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BookingQuestionField{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", dataType='" + getDataType() + "'" +
            ", category='" + getCategory() + "'" +
            ", prePopulationData='" + getPrePopulationData() + "'" +
            ", isVisible='" + isIsVisible() + "'" +
            ", isRequired='" + isIsRequired() + "'" +
            "}";
    }
}
