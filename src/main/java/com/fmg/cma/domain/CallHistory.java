package com.fmg.cma.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A CallHistory.
 */
@Entity
@Table(name = "call_history")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CallHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "created_date")
    private ZonedDateTime createdDate;

    @Column(name = "details")
    private String details;

    @Column(name = "note")
    private String note;

    /**
     * Row num of organization, it's equal to first (minimum) row num. See {@link com.fmg.cma.service.dto.DataListCompany#rowNum}.
     * It's not null.
     */
    @Column(name = "org_row_num")
    private Long orgRowNum;

    /**
     * Contact's row num, it can be null. If it is filled history row belongs to a contact otherwise to company.
     */
    @Column(name = "contact_row_num")
    private Long contactRowNum;

    @ManyToOne
    private User user;

    @ManyToOne
    private CallHistoryType type;

    @ManyToOne
    private ClientList list;

    @Column(name = "sms_text")
    private String smsText;

    @ManyToOne
    private ClientCampaignSms sms;

    @ManyToOne
    private EmailTemplate emailTemplate;

    private Boolean isNoContactHoldFromCallPool;

    private ZonedDateTime noContactStamp;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public CallHistory createdDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getDetails() {
        return details;
    }

    public CallHistory details(String details) {
        this.details = details;
        return this;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getNote() {
        return note;
    }

    public CallHistory note(String note) {
        this.note = note;
        return this;
    }

    public CallHistory setNote(String note) {
        this.note = note;
        return this;
    }

    public Long getOrgRowNum() {
        return orgRowNum;
    }

    public CallHistory setOrgRowNum(Long orgRowNum) {
        this.orgRowNum = orgRowNum;
        return this;
    }

    public Long getContactRowNum() {
        return contactRowNum;
    }

    public CallHistory setContactRowNum(Long contactRowNum) {
        this.contactRowNum = contactRowNum;
        return this;
    }

    public User getUser() {
        return user;
    }

    public CallHistory user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public CallHistoryType getType() {
        return type;
    }

    public CallHistory type(CallHistoryType callHistoryType) {
        this.type = callHistoryType;
        return this;
    }

    public CallHistory setType(CallHistoryType type) {
        this.type = type;
        return this;
    }

    public ClientList getList() {
        return list;
    }

    public CallHistory setList(ClientList clientList) {
        this.list = clientList;
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    public String getSmsText() {
        return smsText;
    }

    public CallHistory setSmsText(String smsText) {
        this.smsText = smsText;
        return this;
    }

    public ClientCampaignSms getSms() {
        return sms;
    }

    public CallHistory setSms(ClientCampaignSms sms) {
        this.sms = sms;
        return this;
    }

    public EmailTemplate getEmailTemplate() {
        return emailTemplate;
    }

    public CallHistory setEmailTemplate(EmailTemplate emailTemplate) {
        this.emailTemplate = emailTemplate;
        return this;
    }

    public Boolean getIsNoContactHoldFromCallPool() {
        return isNoContactHoldFromCallPool;
    }

    public void setIsNoContactHoldFromCallPool(Boolean noContactHoldFromCallPool) {
        isNoContactHoldFromCallPool = noContactHoldFromCallPool;
    }

    public ZonedDateTime getNoContactStamp() {
        return noContactStamp;
    }

    public void setNoContactStamp(ZonedDateTime noContactStamp) {
        this.noContactStamp = noContactStamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CallHistory callHistory = (CallHistory) o;
        if (callHistory.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), callHistory.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CallHistory{" +
            "id=" + getId() +
            ", createdDate='" + getCreatedDate() + "'" +
            ", details='" + getDetails() + "'" +
            ", note='" + getNote() + "'" +
            ", orgRowNum=" + getOrgRowNum() +
            ", contactRowNum=" + getContactRowNum() +
            "}";
    }
}
