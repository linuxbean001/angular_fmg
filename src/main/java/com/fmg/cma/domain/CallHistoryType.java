package com.fmg.cma.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

/**
 * A CallHistoryType.
 */
@Entity
@Table(name = "call_history_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CallHistoryType implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String SMS = "SMS";
    public static final String EMAIL = "EMAIL";
    public static final String CALL = "CALL";
    public static final String ORGANIZATION_CHANGE = "ORGANIZATION_CHANGE";
    public static final String CONTACT_CHANGE = "CONTACT_CHANGE";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "ident")
    private String ident;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public CallHistoryType name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdent() {
        return ident;
    }

    public CallHistoryType ident(String ident) {
        this.ident = ident;
        return this;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CallHistoryType callHistoryType = (CallHistoryType) o;
        if (callHistoryType.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), callHistoryType.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CallHistoryType{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", ident='" + getIdent() + "'" +
            "}";
    }
}
