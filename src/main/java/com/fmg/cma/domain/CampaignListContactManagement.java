package com.fmg.cma.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A CampaignListContactManagement.
 */
@Entity
@Table(name = "campaign_list_contact_manag")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CampaignListContactManagement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "contact_row_num")
    private Long contactRowNum;

    @Column(name = "hold_from_call_pool")
    private Boolean holdFromCallPool;

    @Column(name = "stamp")
    private ZonedDateTime stamp;

    @ManyToOne
    private ClientCampaign clientCampaign;

    @ManyToOne
    private ClientList clientList;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getContactRowNum() {
        return contactRowNum;
    }

    public CampaignListContactManagement contactRowNum(Long contactRowNum) {
        this.contactRowNum = contactRowNum;
        return this;
    }

    public void setContactRowNum(Long contactRowNum) {
        this.contactRowNum = contactRowNum;
    }

    public Boolean isHoldFromCallPool() {
        return holdFromCallPool;
    }

    public CampaignListContactManagement holdFromCallPool(Boolean holdFromCallPool) {
        this.holdFromCallPool = holdFromCallPool;
        return this;
    }

    public void setHoldFromCallPool(Boolean holdFromCallPool) {
        this.holdFromCallPool = holdFromCallPool;
    }

    public ZonedDateTime getStamp() {
        return stamp;
    }

    public CampaignListContactManagement stamp(ZonedDateTime stamp) {
        this.stamp = stamp;
        return this;
    }

    public void setStamp(ZonedDateTime stamp) {
        this.stamp = stamp;
    }

    public ClientCampaign getClientCampaign() {
        return clientCampaign;
    }

    public CampaignListContactManagement clientCampaign(ClientCampaign clientCampaign) {
        this.clientCampaign = clientCampaign;
        return this;
    }

    public void setClientCampaign(ClientCampaign clientCampaign) {
        this.clientCampaign = clientCampaign;
    }

    public ClientList getClientList() {
        return clientList;
    }

    public CampaignListContactManagement clientList(ClientList clientList) {
        this.clientList = clientList;
        return this;
    }

    public void setClientList(ClientList clientList) {
        this.clientList = clientList;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CampaignListContactManagement campaignListContactManagement = (CampaignListContactManagement) o;
        if (campaignListContactManagement.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), campaignListContactManagement.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CampaignListContactManagement{" +
            "id=" + getId() +
            ", contactRowNum=" + getContactRowNum() +
            ", holdFromCallPool='" + isHoldFromCallPool() + "'" +
            ", stamp='" + getStamp() + "'" +
            "}";
    }
}
