package com.fmg.cma.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

/**
 * A CampaignListContactQuestionResult.
 */
@Entity
@Table(name = "campaign_list_contact_question_result")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CampaignListContactQuestionResult implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "contact_row_num")
    private Long contactRowNum;

    @Column(name = "cvalue")
    private String cvalue;

    @ManyToOne
    private ClientCampaign clientCampaign;

    @ManyToOne
    private ClientList clientList;

    @ManyToOne
    private BookingQuestionField bookingQuestionField;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getContactRowNum() {
        return contactRowNum;
    }

    public CampaignListContactQuestionResult contactRowNum(Long contactRowNum) {
        this.contactRowNum = contactRowNum;
        return this;
    }

    public void setContactRowNum(Long contactRowNum) {
        this.contactRowNum = contactRowNum;
    }

    public String getCvalue() {
        return cvalue;
    }

    public CampaignListContactQuestionResult cvalue(String cvalue) {
        this.cvalue = cvalue;
        return this;
    }

    public void setCvalue(String cvalue) {
        this.cvalue = cvalue;
    }

    public ClientCampaign getClientCampaign() {
        return clientCampaign;
    }

    public CampaignListContactQuestionResult clientCampaign(ClientCampaign clientCampaign) {
        this.clientCampaign = clientCampaign;
        return this;
    }

    public void setClientCampaign(ClientCampaign clientCampaign) {
        this.clientCampaign = clientCampaign;
    }

    public ClientList getClientList() {
        return clientList;
    }

    public CampaignListContactQuestionResult clientList(ClientList clientList) {
        this.clientList = clientList;
        return this;
    }

    public void setClientList(ClientList clientList) {
        this.clientList = clientList;
    }

    public BookingQuestionField getBookingQuestionField() {
        return bookingQuestionField;
    }

    public CampaignListContactQuestionResult bookingQuestionField(BookingQuestionField bookingQuestionField) {
        this.bookingQuestionField = bookingQuestionField;
        return this;
    }

    public void setBookingQuestionField(BookingQuestionField bookingQuestionField) {
        this.bookingQuestionField = bookingQuestionField;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CampaignListContactQuestionResult campaignListContactQuestionResult = (CampaignListContactQuestionResult) o;
        if (campaignListContactQuestionResult.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), campaignListContactQuestionResult.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CampaignListContactQuestionResult{" +
            "id=" + getId() +
            ", contactRowNum=" + getContactRowNum() +
            ", cvalue='" + getCvalue() + "'" +
            "}";
    }
}
