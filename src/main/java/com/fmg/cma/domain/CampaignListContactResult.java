package com.fmg.cma.domain;

import org.apache.commons.lang3.BooleanUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A CampaignListContactResult.
 */
@Entity
@Table(name = "campaign_list_contact_result")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CampaignListContactResult implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "contact_row_num")
    private Long contactRowNum;

    @ManyToOne
    private ClientCampaign clientCampaign;

    @ManyToOne
    private ClientList clientList;

    @ManyToOne
    private OutcomeSubReason outcomeSubReason;

    private Boolean isApproved;

    private Boolean emailIsSent;

    /**
     * See {@link ToEmailTemplate}.
     */
    private String emailTemplateJson;

    private Boolean smsIsSent;

    /**
     * See RecordState at client & {@link DataListContactState}.
     */
    private String state;

    @ManyToOne
    private User assignUser;

    private ZonedDateTime assignStamp;

    /**
     * The outcome that was selected by the user for this contact.
     * Console -> Outcome (outcome.component.html).
     */
    @ManyToOne
    private Outcome clickedOutcome;

    private Long orgRowNum;

    @ManyToOne
    private User userClickedFinish;

    public CampaignListContactResult() {
    }

    public CampaignListContactResult(ClientCampaign clientCampaign, ClientList clientList, Long contactRowNum) {
        this.clientCampaign = clientCampaign;
        this.clientList = clientList;
        this.contactRowNum = contactRowNum;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getContactRowNum() {
        return contactRowNum;
    }

    public CampaignListContactResult setContactRowNum(Long contactRowNum) {
        this.contactRowNum = contactRowNum;
        return this;
    }

    public ClientCampaign getClientCampaign() {
        return clientCampaign;
    }

    public CampaignListContactResult clientCampaign(ClientCampaign clientCampaign) {
        this.clientCampaign = clientCampaign;
        return this;
    }

    public void setClientCampaign(ClientCampaign clientCampaign) {
        this.clientCampaign = clientCampaign;
    }

    public ClientList getClientList() {
        return clientList;
    }

    public CampaignListContactResult clientList(ClientList clientList) {
        this.clientList = clientList;
        return this;
    }

    public void setClientList(ClientList clientList) {
        this.clientList = clientList;
    }

    public OutcomeSubReason getOutcomeSubReason() {
        return outcomeSubReason;
    }

    public CampaignListContactResult outcomeSubReason(OutcomeSubReason outcomeSubReason) {
        this.outcomeSubReason = outcomeSubReason;
        return this;
    }

    public void setOutcomeSubReason(OutcomeSubReason outcomeSubReason) {
        this.outcomeSubReason = outcomeSubReason;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    public Boolean getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(Boolean approved) {
        isApproved = approved;
    }

    public boolean getEmailIsSent() {
        return BooleanUtils.isTrue(emailIsSent);
    }

    public void setEmailIsSent(Boolean emailIsSent) {
        this.emailIsSent = emailIsSent;
    }

    public String getEmailTemplateJson() {
        return emailTemplateJson;
    }

    public void setEmailTemplateJson(String emailTemplateJson) {
        this.emailTemplateJson = emailTemplateJson;
    }

    public boolean getSmsIsSent() {
        return BooleanUtils.isTrue(smsIsSent);
    }

    public void setSmsIsSent(Boolean smsIsSent) {
        this.smsIsSent = smsIsSent;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public User getAssignUser() {
        return assignUser;
    }

    public void setAssignUser(User assignUser) {
        this.assignUser = assignUser;
    }

    public ZonedDateTime getAssignStamp() {
        return assignStamp;
    }

    public void setAssignStamp(ZonedDateTime assignStamp) {
        this.assignStamp = assignStamp;
    }

    public Outcome getClickedOutcome() {
        return clickedOutcome;
    }

    public void setClickedOutcome(Outcome clickedOutcome) {
        this.clickedOutcome = clickedOutcome;
    }

    public Long getOrgRowNum() {
        return orgRowNum;
    }

    public CampaignListContactResult setOrgRowNum(Long orgRowNum) {
        this.orgRowNum = orgRowNum;
        return this;
    }

    public User getUserClickedFinish() {
        return userClickedFinish;
    }

    public CampaignListContactResult setUserClickedFinish(User userClickedFinish) {
        this.userClickedFinish = userClickedFinish;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CampaignListContactResult campaignListContactResult = (CampaignListContactResult) o;
        if (campaignListContactResult.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), campaignListContactResult.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CampaignListContactResult{" +
            "id=" + getId() +
            ", contactRowNum=" + getContactRowNum() +
            "}";
    }
}
