package com.fmg.cma.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A CampaignPortalUser.
 */
@Entity
@Table(name = "campaign_portal_user")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CampaignPortalUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "sentNotification", nullable = true)
    private Boolean sentNotification;

    @ManyToOne
    private ClientCampaign clientCampaign;

    @ManyToOne
    private User user;


    public Boolean getSentNotification() {
        return sentNotification;
    }

    public void setSentNotification(Boolean aSentNotification) {
        sentNotification = aSentNotification;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public CampaignPortalUser email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ClientCampaign getClientCampaign() {
        return clientCampaign;
    }

    public CampaignPortalUser clientCampaign(ClientCampaign clientCampaign) {
        this.clientCampaign = clientCampaign;
        return this;
    }

    public void setClientCampaign(ClientCampaign clientCampaign) {
        this.clientCampaign = clientCampaign;
    }

    public User getUser() {
        return user;
    }

    public CampaignPortalUser user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CampaignPortalUser campaignPortalUser = (CampaignPortalUser) o;
        if (campaignPortalUser.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), campaignPortalUser.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CampaignPortalUser{" +
            "id=" + getId() +
            ", email='" + getEmail() + "'" +
            "}";
    }
}
