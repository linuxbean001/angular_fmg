package com.fmg.cma.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A CampaignPortalUserOutcome.
 */
@Entity
@Table(name = "campaign_portal_user_outcome")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CampaignPortalUserOutcome implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private CampaignPortalUser campaignPortalUser;

    @ManyToOne
    private Outcome outcome;

    @Column(name = "email_immediately")
    private Boolean emailImmediately;

    @Column(name = "email_daily")
    private Boolean emailDaily;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CampaignPortalUser getCampaignPortalUser() {
        return campaignPortalUser;
    }

    public CampaignPortalUserOutcome campaignPortalUser(CampaignPortalUser campaignPortalUser) {
        this.campaignPortalUser = campaignPortalUser;
        return this;
    }

    public void setCampaignPortalUser(CampaignPortalUser campaignPortalUser) {
        this.campaignPortalUser = campaignPortalUser;
    }

    public Outcome getOutcome() {
        return outcome;
    }

    public CampaignPortalUserOutcome outcome(Outcome outcome) {
        this.outcome = outcome;
        return this;
    }

    public void setOutcome(Outcome outcome) {
        this.outcome = outcome;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    public Boolean getEmailImmediately() {
        return emailImmediately;
    }

    public void setEmailImmediately(Boolean emailImmediately) {
        this.emailImmediately = emailImmediately;
    }

    public Boolean getEmailDaily() {
        return emailDaily;
    }

    public void setEmailDaily(Boolean emailDaily) {
        this.emailDaily = emailDaily;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CampaignPortalUserOutcome campaignPortalUserOutcome = (CampaignPortalUserOutcome) o;
        if (campaignPortalUserOutcome.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), campaignPortalUserOutcome.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CampaignPortalUserOutcome{" +
            "id=" + getId() +
            "}";
    }
}
