package com.fmg.cma.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * A ClientCampaign.
 */
@Entity
@Table(name = "client_campaign")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ClientCampaign implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "created_date")
    private ZonedDateTime createdDate;

    @ManyToOne
    private User createdBy;

    @ManyToOne
    private ClientCompany clientCompany;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "client_campaign_managers",
        joinColumns = @JoinColumn(name = "client_campaigns_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "managers_id", referencedColumnName = "id"))
    private Set<User> managers = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "client_campaign_users",
        joinColumns = @JoinColumn(name = "client_campaigns_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "users_id", referencedColumnName = "id"))
    private Set<User> users = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "client_campaign_qa_users",
        joinColumns = @JoinColumn(name = "client_campaigns_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "qa_users_id", referencedColumnName = "id"))
    private Set<User> qaUsers = new HashSet<>();

    @OneToMany(mappedBy = "clientCampaign")
    private Set<EmailTemplate> emailTemplates = new HashSet<>();

    @OneToMany(mappedBy = "clientCampaign")
    private Set<ClientList> clientLists = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove

    @OneToMany(mappedBy = "clientCampaign")
    private Set<Outcome> outcomes = new HashSet<>();

    @OneToMany(mappedBy = "clientCampaign")
    private Set<ClientCampaignScript> scripts = new HashSet<>();

    @OneToMany(mappedBy = "clientCampaign")
    private Set<ClientCampaignSms> smss = new HashSet<>();

    @OneToMany(mappedBy = "clientCampaign")
    private Set<CampaignPortalUser> portalUsers = new HashSet<>();

    @OneToMany(mappedBy = "clientCampaign")
    private Set<AppointmentAttendee> appointmentAttendees = new HashSet<>();

    @OneToMany(mappedBy = "clientCampaign")
    private Set<SurveyQuestion> surveyQuestions = new HashSet<>();

    @OneToMany(mappedBy = "clientCampaign")
    private Set<BookingQuestion> bookingQuestions = new HashSet<>();

    private Boolean weeklyHeadlineStats;

    private Boolean allowedExportData;

    private String sarReportIdent;

    private LocalTime sarReportTime;

    private String statsSummaryIdent;

    private LocalTime statsSummaryTime;

    @ElementCollection
    @CollectionTable(name = "client_campaign_distr_methods", joinColumns = @JoinColumn(name = "client_campaign_id"))
    @Column(name = "method_ident")
    private List<String> reportDistributionMethods;

    @ElementCollection
    @CollectionTable(name = "client_campaign_export_prtl_users", joinColumns = @JoinColumn(name = "client_campaign_id"))
    @Column(name = "portal_user_id")
    private List<Long> portalUsersAllowedToExport;

    @ElementCollection
    @CollectionTable(name = "client_campaign_delivered_prtl_users", joinColumns = @JoinColumn(name = "client_campaign_id"))
    @Column(name = "portal_user_id")
    private List<Long> reportsDeliveredToUsers;

    private String website;

    private String meetingDuration;

    private String specialRequests;

    private String resultsPerHour;

    private String qualityControl;

    private String comments;

    private String appointmentType;

    private String surveyName;

    private String bookingName;

    private Boolean smsTabNotRequired;

    private String controllerFullName;

    private String controllerJobRole;

    private String controllerEmail;

    private String controllerPhone;

    private String controllerCompanyName;

    private String controllerCompanyReg;

    private String controllerCompanyAddress;

    private String sarsFullName;

    private String sarsJobRole;

    private String sarsEmail;

    private String sarsPhone;

    private String sarsCompanyName;

    private String sarsCompanyReg;

    private String sarsCompanyAddress;

    private String liaHyperlink;

    private Integer callPerHour;

    private Integer callPerHourLow;

    private Integer callPerHourHigh;

    private Integer conversationsPerHour;

    private Integer conversationsPerHourLow;

    private Integer conversationsPerHourHigh;

    private Double contactRate;

    private Double contactRateLow;

    private Double contactRateHigh;

    private Integer hoursPerLead;

    private Integer hoursPerLeadLow;

    private Integer hoursPerLeadHigh;

    private Double conversionRate;

    private Double conversionRateLow;

    private Double conversionRateHigh;

    private Double leadToSaleConversion;

    private Double leadToSaleConversionLow;

    private Double leadToSaleConversionHigh;

    private BigDecimal averageSaleValue;

    private BigDecimal averageSaleValueLow;

    private BigDecimal averageSaleValueHigh;

    private String strategyDocument;

    @OneToMany(mappedBy = "clientCampaign")
    private Set<ClientCampaignAddDoc> addDocs = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public ClientCampaign name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public ClientCampaign createdDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public ClientCampaign createdBy(User user) {
        this.createdBy = user;
        return this;
    }

    public void setCreatedBy(User user) {
        this.createdBy = user;
    }

    public ClientCompany getClientCompany() {
        return clientCompany;
    }

    public ClientCampaign clientCompany(ClientCompany clientCompany) {
        this.clientCompany = clientCompany;
        return this;
    }

    public void setClientCompany(ClientCompany clientCompany) {
        this.clientCompany = clientCompany;
    }

    public Set<User> getManagers() {
        return managers;
    }

    public ClientCampaign managers(Set<User> managers) {
        this.managers = managers;
        return this;
    }

    public void setManagers(Set<User> managers) {
        this.managers = managers;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Set<User> getQaUsers() {
        return qaUsers;
    }

    public void setQaUsers(Set<User> qaUsers) {
        this.qaUsers = qaUsers;
    }

    public Set<EmailTemplate> getEmailTemplates() {
        return emailTemplates;
    }

    public ClientCampaign emailTemplates(Set<EmailTemplate> emailTemplates) {
        this.emailTemplates = emailTemplates;
        return this;
    }

    public void setEmailTemplates(Set<EmailTemplate> emailTemplates) {
        this.emailTemplates = emailTemplates;
    }

    public Set<ClientList> getClientLists() {
        return clientLists;
    }

    public ClientCampaign clientLists(Set<ClientList> clientLists) {
        this.clientLists = clientLists;
        return this;
    }

    public ClientCampaign addClientList(ClientList clientList) {
        this.clientLists.add(clientList);
//        clientList.setClientCampaign(this);
        return this;
    }

    public ClientCampaign removeClientList(ClientList clientList) {
        this.clientLists.remove(clientList);
//        clientList.setClientCampaign(null);
        return this;
    }

    public void setClientLists(Set<ClientList> clientLists) {
        this.clientLists = clientLists;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    public Set<Outcome> getOutcomes() {
        return outcomes;
    }

    public void setOutcomes(Set<Outcome> outcomes) {
        this.outcomes = outcomes;
    }

    public Set<ClientCampaignScript> getScripts() {
        return scripts;
    }

    public void setScripts(Set<ClientCampaignScript> scripts) {
        this.scripts = scripts;
    }

    public Set<ClientCampaignSms> getSmss() {
        return smss;
    }

    public void setSmss(Set<ClientCampaignSms> smss) {
        this.smss = smss;
    }

    public Set<CampaignPortalUser> getPortalUsers() {
        return portalUsers;
    }

    public void setPortalUsers(Set<CampaignPortalUser> portalUsers) {
        this.portalUsers = portalUsers;
    }

    public Set<AppointmentAttendee> getAppointmentAttendees() {
        return appointmentAttendees;
    }

    public void setAppointmentAttendees(Set<AppointmentAttendee> appointmentAttendees) {
        this.appointmentAttendees = appointmentAttendees;
    }

    public Set<SurveyQuestion> getSurveyQuestions() {
        return surveyQuestions;
    }

    public void setSurveyQuestions(Set<SurveyQuestion> surveyQuestions) {
        this.surveyQuestions = surveyQuestions;
    }

    public Set<BookingQuestion> getBookingQuestions() {
        return bookingQuestions;
    }

    public void setBookingQuestions(Set<BookingQuestion> bookingQuestions) {
        this.bookingQuestions = bookingQuestions;
    }

    public Boolean getWeeklyHeadlineStats() {
        return weeklyHeadlineStats;
    }

    public void setWeeklyHeadlineStats(Boolean weeklyHeadlineStats) {
        this.weeklyHeadlineStats = weeklyHeadlineStats;
    }

    public Boolean getAllowedExportData() {
        return allowedExportData;
    }

    public void setAllowedExportData(Boolean allowedExportData) {
        this.allowedExportData = allowedExportData;
    }

    public String getSarReportIdent() {
        return sarReportIdent;
    }

    public void setSarReportIdent(String sarReportIdent) {
        this.sarReportIdent = sarReportIdent;
    }

    public LocalTime getSarReportTime() {
        return sarReportTime;
    }

    public void setSarReportTime(LocalTime sarReportTime) {
        this.sarReportTime = sarReportTime;
    }

    public String getStatsSummaryIdent() {
        return statsSummaryIdent;
    }

    public void setStatsSummaryIdent(String statsSummaryIdent) {
        this.statsSummaryIdent = statsSummaryIdent;
    }

    public LocalTime getStatsSummaryTime() {
        return statsSummaryTime;
    }

    public void setStatsSummaryTime(LocalTime statsSummaryTime) {
        this.statsSummaryTime = statsSummaryTime;
    }

    public List<String> getReportDistributionMethods() {
        return reportDistributionMethods;
    }

    public void setReportDistributionMethods(List<String> reportDistributionMethods) {
        this.reportDistributionMethods = reportDistributionMethods;
    }

    public List<Long> getPortalUsersAllowedToExport() {
        return portalUsersAllowedToExport;
    }

    public void setPortalUsersAllowedToExport(List<Long> portalUsersAllowedToExport) {
        this.portalUsersAllowedToExport = portalUsersAllowedToExport;
    }

    public List<Long> getReportsDeliveredToUsers() {
        return reportsDeliveredToUsers;
    }

    public void setReportsDeliveredToUsers(List<Long> reportsDeliveredToUsers) {
        this.reportsDeliveredToUsers = reportsDeliveredToUsers;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getMeetingDuration() {
        return meetingDuration;
    }

    public void setMeetingDuration(String meetingDuration) {
        this.meetingDuration = meetingDuration;
    }

    public String getSpecialRequests() {
        return specialRequests;
    }

    public void setSpecialRequests(String specialRequests) {
        this.specialRequests = specialRequests;
    }

    public String getResultsPerHour() {
        return resultsPerHour;
    }

    public void setResultsPerHour(String resultsPerHour) {
        this.resultsPerHour = resultsPerHour;
    }

    public String getQualityControl() {
        return qualityControl;
    }

    public void setQualityControl(String qualityControl) {
        this.qualityControl = qualityControl;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getAppointmentType() {
        return appointmentType;
    }

    public void setAppointmentType(String appointmentType) {
        this.appointmentType = appointmentType;
    }

    public String getSurveyName() {
        return surveyName;
    }

    public void setSurveyName(String surveyName) {
        this.surveyName = surveyName;
    }

    public String getBookingName() {
        return bookingName;
    }

    public void setBookingName(String bookingName) {
        this.bookingName = bookingName;
    }

    public Boolean getSmsTabNotRequired() {
        return smsTabNotRequired;
    }

    public void setSmsTabNotRequired(Boolean smsTabNotRequired) {
        this.smsTabNotRequired = smsTabNotRequired;
    }

    public String getControllerFullName() {
        return controllerFullName;
    }

    public void setControllerFullName(String controllerFullName) {
        this.controllerFullName = controllerFullName;
    }

    public String getControllerJobRole() {
        return controllerJobRole;
    }

    public void setControllerJobRole(String controllerJobRole) {
        this.controllerJobRole = controllerJobRole;
    }

    public String getControllerEmail() {
        return controllerEmail;
    }

    public void setControllerEmail(String controllerEmail) {
        this.controllerEmail = controllerEmail;
    }

    public String getControllerPhone() {
        return controllerPhone;
    }

    public void setControllerPhone(String controllerPhone) {
        this.controllerPhone = controllerPhone;
    }

    public String getControllerCompanyName() {
        return controllerCompanyName;
    }

    public void setControllerCompanyName(String controllerCompanyName) {
        this.controllerCompanyName = controllerCompanyName;
    }

    public String getControllerCompanyReg() {
        return controllerCompanyReg;
    }

    public void setControllerCompanyReg(String controllerCompanyReg) {
        this.controllerCompanyReg = controllerCompanyReg;
    }

    public String getControllerCompanyAddress() {
        return controllerCompanyAddress;
    }

    public void setControllerCompanyAddress(String controllerCompanyAddress) {
        this.controllerCompanyAddress = controllerCompanyAddress;
    }

    public String getSarsFullName() {
        return sarsFullName;
    }

    public void setSarsFullName(String sarsFullName) {
        this.sarsFullName = sarsFullName;
    }

    public String getSarsJobRole() {
        return sarsJobRole;
    }

    public void setSarsJobRole(String sarsJobRole) {
        this.sarsJobRole = sarsJobRole;
    }

    public String getSarsEmail() {
        return sarsEmail;
    }

    public void setSarsEmail(String sarsEmail) {
        this.sarsEmail = sarsEmail;
    }

    public String getSarsPhone() {
        return sarsPhone;
    }

    public void setSarsPhone(String sarsPhone) {
        this.sarsPhone = sarsPhone;
    }

    public String getSarsCompanyName() {
        return sarsCompanyName;
    }

    public void setSarsCompanyName(String sarsCompanyName) {
        this.sarsCompanyName = sarsCompanyName;
    }

    public String getSarsCompanyReg() {
        return sarsCompanyReg;
    }

    public void setSarsCompanyReg(String sarsCompanyReg) {
        this.sarsCompanyReg = sarsCompanyReg;
    }

    public String getSarsCompanyAddress() {
        return sarsCompanyAddress;
    }

    public void setSarsCompanyAddress(String sarsCompanyAddress) {
        this.sarsCompanyAddress = sarsCompanyAddress;
    }

    public String getLiaHyperlink() {
        return liaHyperlink;
    }

    public void setLiaHyperlink(String liaHyperlink) {
        this.liaHyperlink = liaHyperlink;
    }

    public Integer getCallPerHour() {
        return callPerHour;
    }

    public void setCallPerHour(Integer callPerHour) {
        this.callPerHour = callPerHour;
    }

    public Integer getCallPerHourLow() {
        return callPerHourLow;
    }

    public void setCallPerHourLow(Integer callPerHourLow) {
        this.callPerHourLow = callPerHourLow;
    }

    public Integer getCallPerHourHigh() {
        return callPerHourHigh;
    }

    public void setCallPerHourHigh(Integer callPerHourHigh) {
        this.callPerHourHigh = callPerHourHigh;
    }

    public Integer getConversationsPerHour() {
        return conversationsPerHour;
    }

    public void setConversationsPerHour(Integer conversationsPerHour) {
        this.conversationsPerHour = conversationsPerHour;
    }

    public Integer getConversationsPerHourLow() {
        return conversationsPerHourLow;
    }

    public void setConversationsPerHourLow(Integer conversationsPerHourLow) {
        this.conversationsPerHourLow = conversationsPerHourLow;
    }

    public Integer getConversationsPerHourHigh() {
        return conversationsPerHourHigh;
    }

    public void setConversationsPerHourHigh(Integer conversationsPerHourHigh) {
        this.conversationsPerHourHigh = conversationsPerHourHigh;
    }

    public Double getContactRate() {
        return contactRate;
    }

    public void setContactRate(Double contactRate) {
        this.contactRate = contactRate;
    }

    public Double getContactRateLow() {
        return contactRateLow;
    }

    public void setContactRateLow(Double contactRateLow) {
        this.contactRateLow = contactRateLow;
    }

    public Double getContactRateHigh() {
        return contactRateHigh;
    }

    public void setContactRateHigh(Double contactRateHigh) {
        this.contactRateHigh = contactRateHigh;
    }

    public Integer getHoursPerLead() {
        return hoursPerLead;
    }

    public void setHoursPerLead(Integer hoursPerLead) {
        this.hoursPerLead = hoursPerLead;
    }

    public Integer getHoursPerLeadLow() {
        return hoursPerLeadLow;
    }

    public void setHoursPerLeadLow(Integer hoursPerLeadLow) {
        this.hoursPerLeadLow = hoursPerLeadLow;
    }

    public Integer getHoursPerLeadHigh() {
        return hoursPerLeadHigh;
    }

    public void setHoursPerLeadHigh(Integer hoursPerLeadHigh) {
        this.hoursPerLeadHigh = hoursPerLeadHigh;
    }

    public Double getConversionRate() {
        return conversionRate;
    }

    public void setConversionRate(Double conversionRate) {
        this.conversionRate = conversionRate;
    }

    public Double getConversionRateLow() {
        return conversionRateLow;
    }

    public void setConversionRateLow(Double conversionRateLow) {
        this.conversionRateLow = conversionRateLow;
    }

    public Double getConversionRateHigh() {
        return conversionRateHigh;
    }

    public void setConversionRateHigh(Double conversionRateHigh) {
        this.conversionRateHigh = conversionRateHigh;
    }

    public Double getLeadToSaleConversion() {
        return leadToSaleConversion;
    }

    public void setLeadToSaleConversion(Double leadToSaleConversion) {
        this.leadToSaleConversion = leadToSaleConversion;
    }

    public Double getLeadToSaleConversionLow() {
        return leadToSaleConversionLow;
    }

    public void setLeadToSaleConversionLow(Double leadToSaleConversionLow) {
        this.leadToSaleConversionLow = leadToSaleConversionLow;
    }

    public Double getLeadToSaleConversionHigh() {
        return leadToSaleConversionHigh;
    }

    public void setLeadToSaleConversionHigh(Double leadToSaleConversionHigh) {
        this.leadToSaleConversionHigh = leadToSaleConversionHigh;
    }

    public BigDecimal getAverageSaleValue() {
        return averageSaleValue;
    }

    public void setAverageSaleValue(BigDecimal averageSaleValue) {
        this.averageSaleValue = averageSaleValue;
    }

    public BigDecimal getAverageSaleValueLow() {
        return averageSaleValueLow;
    }

    public void setAverageSaleValueLow(BigDecimal averageSaleValueLow) {
        this.averageSaleValueLow = averageSaleValueLow;
    }

    public BigDecimal getAverageSaleValueHigh() {
        return averageSaleValueHigh;
    }

    public void setAverageSaleValueHigh(BigDecimal averageSaleValueHigh) {
        this.averageSaleValueHigh = averageSaleValueHigh;
    }

    public String getStrategyDocument() {
        return strategyDocument;
    }

    public void setStrategyDocument(String strategyDocument) {
        this.strategyDocument = strategyDocument;
    }

    public Set<ClientCampaignAddDoc> getAddDocs() {
        return addDocs;
    }

    public void setAddDocs(Set<ClientCampaignAddDoc> addDocs) {
        this.addDocs = addDocs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ClientCampaign clientCampaign = (ClientCampaign) o;
        if (clientCampaign.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clientCampaign.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClientCampaign{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            "}";
    }
}
