package com.fmg.cma.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

/**
 * A ClientCampaignAddDoc.
 */
@Entity
@Table(name = "client_campaign_add_doc")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ClientCampaignAddDoc implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "jhi_link")
    private String link;

    @Column(name = "text")
    private String text;

    @ManyToOne
    private ClientCampaign clientCampaign;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public ClientCampaignAddDoc link(String link) {
        this.link = link;
        return this;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getText() {
        return text;
    }

    public ClientCampaignAddDoc text(String text) {
        this.text = text;
        return this;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ClientCampaign getClientCampaign() {
        return clientCampaign;
    }

    public ClientCampaignAddDoc clientCampaign(ClientCampaign clientCampaign) {
        this.clientCampaign = clientCampaign;
        return this;
    }

    public void setClientCampaign(ClientCampaign clientCampaign) {
        this.clientCampaign = clientCampaign;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ClientCampaignAddDoc clientCampaignAddDoc = (ClientCampaignAddDoc) o;
        if (clientCampaignAddDoc.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clientCampaignAddDoc.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClientCampaignAddDoc{" +
            "id=" + getId() +
            ", link='" + getLink() + "'" +
            ", text='" + getText() + "'" +
            "}";
    }
}
