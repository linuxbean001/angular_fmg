package com.fmg.cma.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A ClientCompany.
 */
@Entity
@Table(name = "client_company")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ClientCompany implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "created_date")
    private ZonedDateTime createdDate;

    @ManyToOne
    private User createdBy;

    @ManyToOne(optional = false)
    @NotNull
    private ClientCompanyStatus status;

    @Column(name = "updated_date")
    private ZonedDateTime updatedDate;

    @ManyToOne
    private User updatedBy;

    @ManyToOne
    private Industry industry;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public ClientCompany name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public ClientCompany createdDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public ClientCompany createdBy(User user) {
        this.createdBy = user;
        return this;
    }

    public void setCreatedBy(User user) {
        this.createdBy = user;
    }

    public ClientCompanyStatus getStatus() {
        return status;
    }

    public ClientCompany status(ClientCompanyStatus clientCompanyStatus) {
        this.status = clientCompanyStatus;
        return this;
    }

    public void setStatus(ClientCompanyStatus clientCompanyStatus) {
        this.status = clientCompanyStatus;
    }

    public ZonedDateTime getUpdatedDate() {
        return updatedDate;
    }

    public ClientCompany setUpdatedDate(ZonedDateTime updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public ClientCompany setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    public Industry getIndustry() {
        return industry;
    }

    public ClientCompany industry(Industry industry) {
        this.industry = industry;
        return this;
    }

    public void setIndustry(Industry industry) {
        this.industry = industry;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ClientCompany clientCompany = (ClientCompany) o;
        if (clientCompany.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clientCompany.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClientCompany{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            "}";
    }
}
