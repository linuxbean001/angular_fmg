package com.fmg.cma.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

/**
 * A ClientField.
 */
@Entity
@Table(name = "client_field")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ClientField implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "visible")
    private Boolean visible;

    @Column(name = "editable")
    private Boolean editable;

    @Column(name = "is_org_information")
    private Boolean isOrgInformation;

    @ManyToOne
    private Field field;

    @ManyToOne

    private ClientList list;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isVisible() {
        return visible;
    }

    public ClientField visible(Boolean visible) {
        this.visible = visible;
        return this;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Boolean isEditable() {
        return editable;
    }

    public ClientField editable(Boolean editable) {
        this.editable = editable;
        return this;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }

    public Field getField() {
        return field;
    }

    public ClientField field(Field field) {
        this.field = field;
        return this;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public ClientList getList() {
        return list;
    }

    public ClientField list(ClientList clientList) {
        this.list = clientList;
        return this;
    }

    public void setList(ClientList clientList) {
        this.list = clientList;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    public Boolean getIsOrgInformation() {
        return isOrgInformation;
    }

    public ClientField setOrgInformation(Boolean orgInformation) {
        isOrgInformation = orgInformation;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ClientField clientField = (ClientField) o;
        if (clientField.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clientField.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClientField{" +
            "id=" + getId() +
            ", visible='" + isVisible() + "'" +
            ", editable='" + isEditable() + "'" +
            "}";
    }
}
