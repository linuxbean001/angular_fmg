package com.fmg.cma.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A ClientList.
 */
@Entity
@Table(name = "client_list")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ClientList implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    private Integer rowCount;

    @ManyToOne
    private ClientCampaign clientCampaign;


    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "client_list_portal_users",
        joinColumns = @JoinColumn(name = "client_lists_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "portal_users_id", referencedColumnName = "id"))
    private Set<CampaignPortalUser> portalUsers = new HashSet<>();




    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public ClientList name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    public Integer getRowCount() {
        return rowCount;
    }

    public void setRowCount(Integer rowCount) {
        this.rowCount = rowCount;
    }

    public ClientCampaign getClientCampaign() {
        return clientCampaign;
    }

    public ClientList setClientCampaign(ClientCampaign clientCampaign) {
        this.clientCampaign = clientCampaign;
        return this;
    }


    public Set<CampaignPortalUser> getPortalUsers() {
        return portalUsers;
    }

    public void setPortalUsers(Set<CampaignPortalUser> aPortalUsers) {
        this.portalUsers = aPortalUsers;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ClientList clientList = (ClientList) o;
        if (clientList.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clientList.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClientList{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
