package com.fmg.cma.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A ClientListData.
 */
@Entity
@Table(name = "client_list_data")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ClientListData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "row_num")
    private Long rowNum;

    @Column(name = "cvalue")
    private String cvalue;

    @ManyToOne
    private ClientField clientField;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRowNum() {
        return rowNum;
    }

    public ClientListData rowNum(Long rowNum) {
        this.rowNum = rowNum;
        return this;
    }

    public void setRowNum(Long rowNum) {
        this.rowNum = rowNum;
    }

    public String getCvalue() {
        return cvalue;
    }

    public ClientListData cvalue(String cvalue) {
        this.cvalue = cvalue;
        return this;
    }

    public void setCvalue(String cvalue) {
        this.cvalue = cvalue;
    }

    public ClientField getClientField() {
        return clientField;
    }

    public ClientListData clientField(ClientField clientField) {
        this.clientField = clientField;
        return this;
    }

    public void setClientField(ClientField clientField) {
        this.clientField = clientField;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ClientListData clientListData = (ClientListData) o;
        if (clientListData.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clientListData.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClientListData{" +
            "id=" + getId() +
            ", rowNum=" + getRowNum() +
            ", cvalue='" + getCvalue() + "'" +
            "}";
    }
}
