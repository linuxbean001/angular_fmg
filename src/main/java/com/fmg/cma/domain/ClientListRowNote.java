package com.fmg.cma.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "client_list_row_note")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ClientListRowNote implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private ClientList list;

    @Column(name = "row_num")
    private Long rowNum;

    @Column(name = "note")
    private String note;

    @Column(name = "contact_note")
    private String contactNote;

    @Column(name = "is_left_company")
    private Boolean isLeftCompany;

    public Long getId() {
        return id;
    }

    public ClientListRowNote setId(Long id) {
        this.id = id;
        return this;
    }

    public ClientList getList() {
        return list;
    }

    public ClientListRowNote setList(ClientList list) {
        this.list = list;
        return this;
    }

    public Long getRowNum() {
        return rowNum;
    }

    public ClientListRowNote setRowNum(Long rowNum) {
        this.rowNum = rowNum;
        return this;
    }

    public String getNote() {
        return note;
    }

    public ClientListRowNote setNote(String note) {
        this.note = note;
        return this;
    }

    public String getContactNote() {
        return contactNote;
    }

    public ClientListRowNote setContactNote(String contactNote) {
        this.contactNote = contactNote;
        return this;
    }

    public Boolean getIsLeftCompany() {
        return isLeftCompany;
    }

    public ClientListRowNote setIsLeftCompany(Boolean leftCompany) {
        isLeftCompany = leftCompany;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ClientListRowNote clientListRowNote = (ClientListRowNote) o;
        if (clientListRowNote.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clientListRowNote.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClientListRowNote{" +
            "id=" + id +
            ", list=" + list +
            ", rowNum=" + rowNum +
            ", note='" + note + '\'' +
            ", contactNote='" + contactNote + '\'' +
            ", isLeftCompany=" + isLeftCompany +
            '}';
    }
}
