package com.fmg.cma.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A ConsoleStats.
 */
@Entity
@Table(name = "console_stats")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ConsoleStats implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private User user;

    @Column(name = "stamp")
    private LocalDate stamp;

    @Column(name = "calls")
    private long calls;

    @Column(name = "skips")
    private long skips;

    @Column(name = "pending")
    private long pending;

    @Column(name = "dm_contact")
    private long dmContact;

    @Column(name = "lead_sales")
    private long leadSales;

    @Column(name = "pipeline_opps")
    private long pipelineOpps;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public ConsoleStats setUser(User user) {
        this.user = user;
        return this;
    }

    public LocalDate getStamp() {
        return stamp;
    }

    public ConsoleStats setStamp(LocalDate stamp) {
        this.stamp = stamp;
        return this;
    }

    public long getCalls() {
        return calls;
    }

    public ConsoleStats setCalls(long calls) {
        this.calls = calls;
        return this;
    }

    public ConsoleStats incrementCalls() {
        this.calls++;
        return this;
    }

    public long getSkips() {
        return skips;
    }

    public ConsoleStats setSkips(long skips) {
        this.skips = skips;
        return this;
    }

    public ConsoleStats incrementSkips() {
        this.skips++;
        return this;
    }

    public long getPending() {
        return pending;
    }

    public ConsoleStats setPending(long pending) {
        this.pending = pending;
        return this;
    }

    public long getDmContact() {
        return dmContact;
    }

    public ConsoleStats setDmContact(long dmContact) {
        this.dmContact = dmContact;
        return this;
    }

    public ConsoleStats incrementDMContact() {
        this.dmContact++;
        return this;
    }

    public long getLeadSales() {
        return leadSales;
    }

    public ConsoleStats setLeadSales(long leadSales) {
        this.leadSales = leadSales;
        return this;
    }

    public long getPipelineOpps() {
        return pipelineOpps;
    }

    public ConsoleStats setPipelineOpps(long pipelineOpps) {
        this.pipelineOpps = pipelineOpps;
        return this;
    }

    public ConsoleStats incrementPipelineOpps() {
        this.pipelineOpps++;
        return this;
    }

// jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ConsoleStats consoleStats = (ConsoleStats) o;
        if (consoleStats.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), consoleStats.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ConsoleStats{" +
            "id=" + getId() +
            ", calls=" + getCalls() +
            ", skips=" + getSkips() +
            ", pending=" + getPending() +
            ", stamp='" + getStamp() + "'" +
            ", dmContact=" + getDmContact() +
            ", leadSales=" + getLeadSales() +
            ", pipelineOpps=" + getPipelineOpps() +
            "}";
    }
}
