package com.fmg.cma.domain;

/**
 * See RecordState in campaign-list-contact-result.model.ts
 */
public enum DataListContactState {
    ACTIVE("ACTIVE"),
    COMPLETED("COMPLETED"),
    MY_LIST("MY_LIST"),
    PENDING("PENDING");

    private final String ident;

    DataListContactState(String ident) {
        this.ident = ident;
    }

    public String getIdent() {
        return ident;
    }
}
