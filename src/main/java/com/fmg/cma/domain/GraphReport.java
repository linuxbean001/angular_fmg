package com.fmg.cma.domain;

import java.util.List;

public class GraphReport {

    private String name;
    private List<Serie> series;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Serie> getSeries() {
        return series;
    }

    public void setSeries(List<Serie> series) {
        this.series = series;
    }
}
