package com.fmg.cma.domain;

import java.util.Date;

public class MyPipeline {

    private String actionDateStr;

    private Date actionDate;

    private String campaing;

    private String dataList;

    private String companyName;

    private String contactName;

    private String stateCountry;

    private String creationDateStr;

    private Date creationDate;

    private String comment;

    private String lastOutcom;

    private String priority;

    public MyPipeline() {
    }

    public MyPipeline(Date actionDate, String campaing, String dataList, String companyName, String contactName, String stateCountry, Date creationDate, String comment, String lastOutcom, String priority) {
        this.actionDate = actionDate;
        this.campaing = campaing;
        this.dataList = dataList;
        this.companyName = companyName;
        this.contactName = contactName;
        this.stateCountry = stateCountry;
        this.creationDate = creationDate;
        this.comment = comment;
        this.lastOutcom = lastOutcom;
        this.priority = priority;
    }

    public String getActionDateStr() {
        return actionDateStr;
    }

    public void setActionDateStr(String actionDateStr) {
        this.actionDateStr = actionDateStr;
    }

    public Date getActionDate() {
        return actionDate;
    }

    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }

    public String getCampaing() {
        return campaing;
    }

    public void setCampaing(String campaing) {
        this.campaing = campaing;
    }

    public String getDataList() {
        return dataList;
    }

    public void setDataList(String dataList) {
        this.dataList = dataList;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getStateCountry() {
        return stateCountry;
    }

    public void setStateCountry(String stateCountry) {
        this.stateCountry = stateCountry;
    }

    public String getCreationDateStr() {
        return creationDateStr;
    }

    public void setCreationDateStr(String creationDateStr) {
        this.creationDateStr = creationDateStr;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getLastOutcom() {
        return lastOutcom;
    }

    public void setLastOutcom(String lastOutcom) {
        this.lastOutcom = lastOutcom;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }
}
