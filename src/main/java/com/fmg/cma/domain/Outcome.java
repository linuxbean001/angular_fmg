package com.fmg.cma.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Outcome.
 */
@Entity
@Table(name = "outcome")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Outcome implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @ManyToOne
    private ClientCampaignTemplate template;

    private Boolean addSubReason;

    private String callPoolCycle;

    private Integer daysAfterCallBack;

    private String parked;

    /**
     * See {@link QcQa}.
     */
    private String qcqa;

    private String killRecord;

    private Integer killRecordAttempts;

    private Boolean enterNurtureStream;

    @ManyToOne
    private OutcomeType type;

    private String categoryIdent;

    private String ownership;

    private String ownershipDuration;

    private Integer ownershipDurationCount;

    private String emailToContact;

    private String sms;

    private Long smsId;

    private String calendarInvite;

    private String moveToAnotherCampaign;

    @ManyToOne
    private ClientCampaign moveToCampaign;

    @ManyToOne
    private ClientList clientList;

    @ManyToOne
    private ClientCampaign clientCampaign;

    @ManyToOne
    private EmailTemplate emailTemplate;

    private Boolean showCampaignBookingFormQuestions;

    @Column(name = "jhi_order")
    private Long order;

    @ManyToOne
    private OutcomeSubReason outcomeSubReason;

    private String linkToSchedule;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Outcome name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ClientCampaignTemplate getTemplate() {
        return template;
    }

    public Outcome template(ClientCampaignTemplate clientCampaignTemplate) {
        this.template = clientCampaignTemplate;
        return this;
    }

    public void setTemplate(ClientCampaignTemplate clientCampaignTemplate) {
        this.template = clientCampaignTemplate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    public Boolean getAddSubReason() {
        return addSubReason;
    }

    public void setAddSubReason(Boolean addSubReason) {
        this.addSubReason = addSubReason;
    }

    public String getCallPoolCycle() {
        return callPoolCycle;
    }

    public void setCallPoolCycle(String callPoolCycle) {
        this.callPoolCycle = callPoolCycle;
    }

    public Integer getDaysAfterCallBack() {
        return daysAfterCallBack;
    }

    public void setDaysAfterCallBack(Integer daysAfterCallBack) {
        this.daysAfterCallBack = daysAfterCallBack;
    }

    public String getParked() {
        return parked;
    }

    public void setParked(String parked) {
        this.parked = parked;
    }

    public String getQcqa() {
        return qcqa;
    }

    public void setQcqa(String qcqa) {
        this.qcqa = qcqa;
    }

    public String getKillRecord() {
        return killRecord;
    }

    public void setKillRecord(String killRecord) {
        this.killRecord = killRecord;
    }

    public Integer getKillRecordAttempts() {
        return killRecordAttempts;
    }

    public void setKillRecordAttempts(Integer killRecordAttempts) {
        this.killRecordAttempts = killRecordAttempts;
    }

    public Boolean getEnterNurtureStream() {
        return enterNurtureStream;
    }

    public void setEnterNurtureStream(Boolean enterNurtureStream) {
        this.enterNurtureStream = enterNurtureStream;
    }

    public OutcomeType getType() {
        return type;
    }

    public void setType(OutcomeType type) {
        this.type = type;
    }

    public String getCategoryIdent() {
        return categoryIdent;
    }

    public void setCategoryIdent(String categoryIdent) {
        this.categoryIdent = categoryIdent;
    }

    public String getOwnership() {
        return ownership;
    }

    public void setOwnership(String ownership) {
        this.ownership = ownership;
    }

    public String getOwnershipDuration() {
        return ownershipDuration;
    }

    public void setOwnershipDuration(String ownershipDuration) {
        this.ownershipDuration = ownershipDuration;
    }

    public Integer getOwnershipDurationCount() {
        return ownershipDurationCount;
    }

    public void setOwnershipDurationCount(Integer ownershipDurationCount) {
        this.ownershipDurationCount = ownershipDurationCount;
    }

    public String getEmailToContact() {
        return emailToContact;
    }

    public void setEmailToContact(String emailToContact) {
        this.emailToContact = emailToContact;
    }

    public String getSms() {
        return sms;
    }

    public void setSms(String sms) {
        this.sms = sms;
    }

    public Long getSmsId() {
        return smsId;
    }

    public void setSmsId(Long smsId) {
        this.smsId = smsId;
    }

    public String getCalendarInvite() {
        return calendarInvite;
    }

    public void setCalendarInvite(String calendarInvite) {
        this.calendarInvite = calendarInvite;
    }

    public String getMoveToAnotherCampaign() {
        return moveToAnotherCampaign;
    }

    public void setMoveToAnotherCampaign(String moveToAnotherCampaign) {
        this.moveToAnotherCampaign = moveToAnotherCampaign;
    }

    public ClientCampaign getMoveToCampaign() {
        return moveToCampaign;
    }

    public void setMoveToCampaign(ClientCampaign moveToCampaign) {
        this.moveToCampaign = moveToCampaign;
    }

    public ClientList getClientList() {
        return clientList;
    }

    public void setClientList(ClientList clientList) {
        this.clientList = clientList;
    }

    public ClientCampaign getClientCampaign() {
        return clientCampaign;
    }

    public void setClientCampaign(ClientCampaign clientCampaign) {
        this.clientCampaign = clientCampaign;
    }

    public EmailTemplate getEmailTemplate() {
        return emailTemplate;
    }

    public void setEmailTemplate(EmailTemplate emailTemplate) {
        this.emailTemplate = emailTemplate;
    }

    public Boolean getShowCampaignBookingFormQuestions() {
        return showCampaignBookingFormQuestions;
    }

    public void setShowCampaignBookingFormQuestions(Boolean showCampaignBookingFormQuestions) {
        this.showCampaignBookingFormQuestions = showCampaignBookingFormQuestions;
    }

    public Long getOrder() {
        return order;
    }

    public void setOrder(Long order) {
        this.order = order;
    }

    public OutcomeSubReason getOutcomeSubReason() {
        return outcomeSubReason;
    }

    public void setOutcomeSubReason(OutcomeSubReason outcomeSubReason) {
        this.outcomeSubReason = outcomeSubReason;
    }

    public String getLinkToSchedule() {
        return linkToSchedule;
    }

    public void setLinkToSchedule(String linkToSchedule) {
        this.linkToSchedule = linkToSchedule;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Outcome outcome = (Outcome) o;
        if (outcome.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), outcome.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Outcome{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", order=" + getOrder() +
            "}";
    }
}
