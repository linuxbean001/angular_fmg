package com.fmg.cma.domain;

public enum QcQa {
    NO("No", "NO"),
    URGENT("Urgent", "URGENT"),
    EYE_CHECK("Eye check", "EYE_CHECK");

    private final String name;
    private final String ident;

    QcQa(String name, String ident) {
        this.name = name;
        this.ident = ident;
    }

    public String getName() {
        return name;
    }

    public String getIdent() {
        return ident;
    }
}
