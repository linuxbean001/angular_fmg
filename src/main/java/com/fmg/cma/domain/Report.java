package com.fmg.cma.domain;

import java.util.List;

public class Report {

    private List<GraphReport> callPerHour;
    private List<GraphReport> conversationPerHour;
    private List<GraphReport> contactRate;
    private List<GraphReport> hoursPerLead;
    private List<GraphReport> conversionRate;
    private List<GraphReport> leadToSales;
    private List<GraphReport> averageSale;
    private List<GraphReport> costumePhase;

    public List<GraphReport> getCallPerHour() {
        return callPerHour;
    }

    public void setCallPerHour(List<GraphReport> callPerHour) {
        this.callPerHour = callPerHour;
    }

    public List<GraphReport> getConversationPerHour() {
        return conversationPerHour;
    }

    public void setConversationPerHour(List<GraphReport> conversationPerHour) {
        this.conversationPerHour = conversationPerHour;
    }

    public List<GraphReport> getContactRate() {
        return contactRate;
    }

    public void setContactRate(List<GraphReport> contactRate) {
        this.contactRate = contactRate;
    }

    public List<GraphReport> getHoursPerLead() {
        return hoursPerLead;
    }

    public void setHoursPerLead(List<GraphReport> hoursPerLead) {
        this.hoursPerLead = hoursPerLead;
    }

    public List<GraphReport> getConversionRate() {
        return conversionRate;
    }

    public void setConversionRate(List<GraphReport> conversionRate) {
        this.conversionRate = conversionRate;
    }

    public List<GraphReport> getLeadToSales() {
        return leadToSales;
    }

    public void setLeadToSales(List<GraphReport> leadToSales) {
        this.leadToSales = leadToSales;
    }

    public List<GraphReport> getAverageSale() {
        return averageSale;
    }

    public void setAverageSale(List<GraphReport> averageSale) {
        this.averageSale = averageSale;
    }

    public List<GraphReport> getCostumePhase() {
        return costumePhase;
    }

    public void setCostumePhase(List<GraphReport> costumePhase) {
        this.costumePhase = costumePhase;
    }
}
