package com.fmg.cma.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class Serie {

    private BigDecimal name;
    @JsonProperty("value")
    private BigDecimal val;

    public Serie(int name, BigDecimal val) {
        this.name = BigDecimal.valueOf(name);
        this.val = val;
    }

    public BigDecimal getName() {
        return name;
    }

    public void setName(BigDecimal name) {
        this.name = name;
    }

    public BigDecimal getVal() {
        return val;
    }

    public void setVal(BigDecimal val) {
        this.val = val;
    }
}
