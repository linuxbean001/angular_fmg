package com.fmg.cma.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A SurveyQuestion.
 */
@Entity
@Table(name = "survey_question")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SurveyQuestion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "question_text")
    private String questionText;

    @Column(name = "jhi_order")
    private Long order;

    @Column(name = "is_visible")
    private Boolean isVisible;

    @OneToMany(mappedBy = "surveyQuestion")
    // @OneToMany(mappedBy = "surveyQuestion", cascade = CascadeType.PERSIST)
    // @JsonIgnore
    // @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<SurveyQuestionField> fields = new HashSet<>();

    @ManyToOne
    private ClientCampaign clientCampaign;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public SurveyQuestion name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuestionText() {
        return questionText;
    }

    public SurveyQuestion questionText(String questionText) {
        this.questionText = questionText;
        return this;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public Long getOrder() {
        return order;
    }

    public SurveyQuestion order(Long order) {
        this.order = order;
        return this;
    }

    public void setOrder(Long order) {
        this.order = order;
    }

    public Boolean isIsVisible() {
        return isVisible;
    }

    public SurveyQuestion isVisible(Boolean isVisible) {
        this.isVisible = isVisible;
        return this;
    }

    public void setIsVisible(Boolean isVisible) {
        this.isVisible = isVisible;
    }

    public Set<SurveyQuestionField> getFields() {
        return fields;
    }

    public SurveyQuestion fields(Set<SurveyQuestionField> surveyQuestionFields) {
        this.fields = surveyQuestionFields;
        return this;
    }

    public SurveyQuestion addFields(SurveyQuestionField surveyQuestionField) {
        this.fields.add(surveyQuestionField);
        surveyQuestionField.setSurveyQuestion(this);
        return this;
    }

    public SurveyQuestion removeFields(SurveyQuestionField surveyQuestionField) {
        this.fields.remove(surveyQuestionField);
        surveyQuestionField.setSurveyQuestion(null);
        return this;
    }

    public void setFields(Set<SurveyQuestionField> surveyQuestionFields) {
        this.fields = surveyQuestionFields;

        // // re-apply owner
        // for (SurveyQuestionField field : fields) {
        //     field.setSurveyQuestion(this);
        // }
    }

    public ClientCampaign getClientCampaign() {
        return clientCampaign;
    }

    public SurveyQuestion clientCampaign(ClientCampaign clientCampaign) {
        this.clientCampaign = clientCampaign;
        return this;
    }

    public void setClientCampaign(ClientCampaign clientCampaign) {
        this.clientCampaign = clientCampaign;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SurveyQuestion surveyQuestion = (SurveyQuestion) o;
        if (surveyQuestion.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), surveyQuestion.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SurveyQuestion{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", questionText='" + getQuestionText() + "'" +
            ", order=" + getOrder() +
            ", isVisible='" + isIsVisible() + "'" +
            "}";
    }
}
