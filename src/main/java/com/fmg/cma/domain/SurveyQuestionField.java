package com.fmg.cma.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A SurveyQuestionField.
 */
@Entity
@Table(name = "survey_question_field")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SurveyQuestionField implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "data_type")
    private String dataType;

    @Column(name = "category")
    private String category;

    @Column(name = "pre_population_data")
    private String prePopulationData;

    @Column(name = "is_visible")
    private Boolean isVisible;

    @Column(name = "is_required")
    private Boolean isRequired;

    @ManyToOne
    private SurveyQuestion surveyQuestion;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public SurveyQuestionField name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDataType() {
        return dataType;
    }

    public SurveyQuestionField dataType(String dataType) {
        this.dataType = dataType;
        return this;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getCategory() {
        return category;
    }

    public SurveyQuestionField category(String category) {
        this.category = category;
        return this;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPrePopulationData() {
        return prePopulationData;
    }

    public SurveyQuestionField prePopulationData(String prePopulationData) {
        this.prePopulationData = prePopulationData;
        return this;
    }

    public void setPrePopulationData(String prePopulationData) {
        this.prePopulationData = prePopulationData;
    }

    public Boolean isIsVisible() {
        return isVisible;
    }

    public SurveyQuestionField isVisible(Boolean isVisible) {
        this.isVisible = isVisible;
        return this;
    }

    public void setIsVisible(Boolean isVisible) {
        this.isVisible = isVisible;
    }

    public Boolean isIsRequired() {
        return isRequired;
    }

    public SurveyQuestionField isRequired(Boolean isRequired) {
        this.isRequired = isRequired;
        return this;
    }

    public void setIsRequired(Boolean isRequired) {
        this.isRequired = isRequired;
    }

    public SurveyQuestion getSurveyQuestion() {
        return surveyQuestion;
    }

    public SurveyQuestionField surveyQuestion(SurveyQuestion surveyQuestion) {
        this.surveyQuestion = surveyQuestion;
        return this;
    }

    public void setSurveyQuestion(SurveyQuestion surveyQuestion) {
        this.surveyQuestion = surveyQuestion;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SurveyQuestionField surveyQuestionField = (SurveyQuestionField) o;
        if (surveyQuestionField.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), surveyQuestionField.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SurveyQuestionField{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", dataType='" + getDataType() + "'" +
            ", category='" + getCategory() + "'" +
            ", prePopulationData='" + getPrePopulationData() + "'" +
            ", isVisible='" + isIsVisible() + "'" +
            ", isRequired='" + isIsRequired() + "'" +
            "}";
    }
}
