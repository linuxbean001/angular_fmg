package com.fmg.cma.domain;

public class ToEmailTemplate extends EmailTemplate {

    private String to;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
