package com.fmg.cma.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fmg.cma.service.dto.CallHistoryDTO;

import java.time.LocalDateTime;

public class TransactionReport {

    private String campaignName;
    private String listName;
    private String companyName;
    private String contactType;
    private String firstName;
    private String phoneNumber;
    private String surname;
    private String address;
    private String address2;
    private String city;
    private String country;
    private String email;
    private String postcode;
    private String title;
    private LocalDateTime callerDate;
    private String callerId;
    private String callerType;
    private String notes;

    public TransactionReport(CallHistoryDTO history) {
        this.notes = history.getNote();
        this.callerType = history.getTypeIdent();
        this.callerId = history.getUserLogin();
        this.callerDate = history.getCreatedDate().toLocalDateTime();
        this.listName = history.getListName();
        this.campaignName = history.getCampaignName();
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getCallerDate() {
        return callerDate;
    }

    public void setCallerDate(LocalDateTime callerDate) {
        this.callerDate = callerDate;
    }

    public String getCallerId() {
        return callerId;
    }

    public void setCallerId(String callerId) {
        this.callerId = callerId;
    }

    public String getCallerType() {
        return callerType;
    }

    public void setCallerType(String callerType) {
        this.callerType = callerType;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public void of(Integer idField, String value) {
        switch (idField) {
            case 1:
                this.firstName = value;
                break;
            case 2:
                this.surname = value;
                break;
            case 3:
                this.companyName = value;
                break;
            case 4:
                this.phoneNumber = value;
                break;
            case 6:
                this.email = value;
                break;
            case 7:
                this.city = value;
                break;
            case 8:
                this.country = value;
                break;
            case 13:
                this.postcode = value;
                break;
            case 14:
                this.address = value;
                break;
            case 18:
                this.contactType = value;
                break;
            case 19:
                break;
            case 20:
                this.title = value;
                break;
            case 21:
                this.address2 = value;
                break;
            case 5:
            case 9:
            case 10:
            case 11:
            case 12:
            case 15:
            case 16:
            case 17:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            default:
                System.out.println("OK");
        }
    }
}
