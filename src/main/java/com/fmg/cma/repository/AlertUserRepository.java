package com.fmg.cma.repository;

import com.fmg.cma.domain.AlertUser;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the AlertUser entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AlertUserRepository extends JpaRepository<AlertUser, Long>, JpaSpecificationExecutor<AlertUser> {

    @Query("select alert_user from AlertUser alert_user where alert_user.user.login = ?#{principal.username}")
    List<AlertUser> findByUserIsCurrentUser();

}
