package com.fmg.cma.repository;

import com.fmg.cma.domain.AppointmentAttendee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the AppointmentAttendee entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AppointmentAttendeeRepository extends JpaRepository<AppointmentAttendee, Long>, JpaSpecificationExecutor<AppointmentAttendee> {

}
