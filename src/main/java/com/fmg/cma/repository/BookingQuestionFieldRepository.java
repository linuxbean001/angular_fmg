package com.fmg.cma.repository;

import com.fmg.cma.domain.BookingQuestionField;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data JPA repository for the BookingQuestionField entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BookingQuestionFieldRepository extends JpaRepository<BookingQuestionField, Long>, JpaSpecificationExecutor<BookingQuestionField> {

    List<BookingQuestionField> findByBookingQuestionId(Long id);
}
