package com.fmg.cma.repository;

import com.fmg.cma.domain.BookingQuestion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data JPA repository for the BookingQuestion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BookingQuestionRepository extends JpaRepository<BookingQuestion, Long>, JpaSpecificationExecutor<BookingQuestion> {

    @Query("select booking_question from BookingQuestion booking_question " +
        "left join fetch booking_question.fields " +
        "where booking_question.id = :id ")
    BookingQuestion findOneWithEagerRelationships(@Param("id") Long id);

}
