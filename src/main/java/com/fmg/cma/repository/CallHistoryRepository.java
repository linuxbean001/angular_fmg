package com.fmg.cma.repository;

import com.fmg.cma.domain.CallHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository for the CallHistory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CallHistoryRepository extends JpaRepository<CallHistory, Long> {

    @Query("select call_history from CallHistory call_history where call_history.user.login = ?#{principal.username}")
    List<CallHistory> findByUserIsCurrentUser();

    List<CallHistory> findByListId(long listId);

    List<CallHistory> findByListIdAndOrgRowNum(long listId, long orgRowNum);

}
