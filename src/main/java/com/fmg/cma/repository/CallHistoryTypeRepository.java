package com.fmg.cma.repository;

import com.fmg.cma.domain.CallHistoryType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data JPA repository for the CallHistoryType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CallHistoryTypeRepository extends JpaRepository<CallHistoryType, Long> {

    CallHistoryType getByIdent(String ident);
}
