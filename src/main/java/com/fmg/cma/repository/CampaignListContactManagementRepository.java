package com.fmg.cma.repository;

import com.fmg.cma.domain.CampaignListContactManagement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


/**
 * Spring Data JPA repository for the CampaignListContactManagement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CampaignListContactManagementRepository extends JpaRepository<CampaignListContactManagement, Long>, JpaSpecificationExecutor<CampaignListContactManagement> {

}
