package com.fmg.cma.repository;

import com.fmg.cma.domain.CampaignListContactQuestionResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


/**
 * Spring Data JPA repository for the CampaignListContactQuestionResult entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CampaignListContactQuestionResultRepository extends JpaRepository<CampaignListContactQuestionResult, Long>, JpaSpecificationExecutor<CampaignListContactQuestionResult> {

}
