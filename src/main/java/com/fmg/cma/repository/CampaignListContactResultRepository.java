package com.fmg.cma.repository;

import com.fmg.cma.domain.CampaignListContactResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data JPA repository for the CampaignListContactResult entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CampaignListContactResultRepository extends JpaRepository<CampaignListContactResult, Long>, JpaSpecificationExecutor<CampaignListContactResult> {

    Optional<CampaignListContactResult> findByClientCampaignIdAndClientListIdAndContactRowNum(Long clientCampaignId, Long clientListId, Long contactRowNum);

    List<CampaignListContactResult> findByClientListId(Long clientListId);

    List<CampaignListContactResult> findByStateAndAssignUserIdIsNotNull(String state);

}
