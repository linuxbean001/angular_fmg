package com.fmg.cma.repository;

import com.fmg.cma.domain.CampaignPortalUserOutcome;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data JPA repository for the CampaignPortalUserOutcome entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CampaignPortalUserOutcomeRepository extends JpaRepository<CampaignPortalUserOutcome, Long>, JpaSpecificationExecutor<CampaignPortalUserOutcome> {

    List<CampaignPortalUserOutcome> findAllByCampaignPortalUserId(long campaignPortalUserId);

}
