package com.fmg.cma.repository;

import com.fmg.cma.domain.CampaignPortalUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository for the CampaignPortalUser entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CampaignPortalUserRepository extends JpaRepository<CampaignPortalUser, Long>,
    JpaSpecificationExecutor<CampaignPortalUser> {

    @Query("select campaign_portal_user from CampaignPortalUser campaign_portal_user where campaign_portal_user.user" +
        ".login = ?#{principal.username}")
    List<CampaignPortalUser> findByUserIsCurrentUser();

}
