package com.fmg.cma.repository;

import com.fmg.cma.domain.ClientCampaignAddDoc;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


/**
 * Spring Data JPA repository for the ClientCampaignAddDoc entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientCampaignAddDocRepository extends JpaRepository<ClientCampaignAddDoc, Long>, JpaSpecificationExecutor<ClientCampaignAddDoc> {

}
