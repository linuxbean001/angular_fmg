package com.fmg.cma.repository;

import com.fmg.cma.domain.ClientCampaign;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository for the ClientCampaign entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientCampaignRepository extends JpaRepository<ClientCampaign, Long>, JpaSpecificationExecutor<ClientCampaign> {

    @Query("select client_campaign from ClientCampaign client_campaign where client_campaign.createdBy.login = ?#{principal.username}")
    List<ClientCampaign> findByCreatedByIsCurrentUser();

    @Query("select distinct client_campaign from ClientCampaign client_campaign " +
        "left join fetch client_campaign.managers " +
        "left join fetch client_campaign.users " +
        "left join fetch client_campaign.qaUsers " +
        "left join fetch client_campaign.emailTemplates " +
        "left join fetch client_campaign.outcomes " +
        "left join fetch client_campaign.scripts " +
        "left join fetch client_campaign.smss " +
        "left join fetch client_campaign.portalUsers " +
        "left join fetch client_campaign.appointmentAttendees " +
        "left join fetch client_campaign.addDocs ")
    List<ClientCampaign> findAllWithEagerRelationships();

    @Query("select client_campaign from ClientCampaign client_campaign " +
        "left join fetch client_campaign.managers " +
        "left join fetch client_campaign.users " +
        "left join fetch client_campaign.qaUsers " +
        "left join fetch client_campaign.emailTemplates " +
        "left join fetch client_campaign.outcomes " +
        "left join fetch client_campaign.scripts " +
        "left join fetch client_campaign.smss " +
        "left join fetch client_campaign.portalUsers " +
        "left join fetch client_campaign.appointmentAttendees " +
        "left join fetch client_campaign.addDocs " +
        "where client_campaign.id = :id ")
    ClientCampaign findOneWithEagerRelationships(@Param("id") Long id);

    List<ClientCampaign> findAllByName(@Param("name") String name);

    List<ClientCampaign> findAllByNameAndIdNot(@Param("name") String name, @Param("id") Long id);

    List<ClientCampaign> findByUsers_Id(Long id);

    List<ClientCampaign> findByManagers_Id(Long id);

    List<ClientCampaign> findByQaUsers_Id(Long id);
}
