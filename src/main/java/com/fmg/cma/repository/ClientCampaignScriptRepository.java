package com.fmg.cma.repository;

import com.fmg.cma.domain.ClientCampaignScript;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ClientCampaignScript entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientCampaignScriptRepository extends JpaRepository<ClientCampaignScript, Long>, JpaSpecificationExecutor<ClientCampaignScript> {

}
