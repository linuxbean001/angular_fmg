package com.fmg.cma.repository;

import com.fmg.cma.domain.ClientCampaignSms;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


/**
 * Spring Data JPA repository for the ClientCampaignSms entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientCampaignSmsRepository extends JpaRepository<ClientCampaignSms, Long>, JpaSpecificationExecutor<ClientCampaignSms> {

}
