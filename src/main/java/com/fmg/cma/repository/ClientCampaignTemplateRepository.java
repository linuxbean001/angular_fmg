package com.fmg.cma.repository;

import com.fmg.cma.domain.ClientCampaignTemplate;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ClientCampaignTemplate entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientCampaignTemplateRepository extends JpaRepository<ClientCampaignTemplate, Long> {

}
