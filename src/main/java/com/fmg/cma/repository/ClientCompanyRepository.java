package com.fmg.cma.repository;

import com.fmg.cma.domain.ClientCompany;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository for the ClientCompany entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientCompanyRepository extends JpaRepository<ClientCompany, Long>, JpaSpecificationExecutor<ClientCompany> {

    @Query("select client_company from ClientCompany client_company where client_company.createdBy.login = ?#{principal.username}")
    List<ClientCompany> findByCreatedByIsCurrentUser();

    List<ClientCompany> findByNameIgnoreCase(String name);

    List<ClientCompany> findByNameIgnoreCaseAndIdNot(String name, long id);
}
