package com.fmg.cma.repository;

import com.fmg.cma.domain.ClientCompanyStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data JPA repository for the ClientCompanyStatus entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientCompanyStatusRepository extends JpaRepository<ClientCompanyStatus, Long> {

    ClientCompanyStatus getByName(String name);
}
