package com.fmg.cma.repository;

import com.fmg.cma.domain.ClientField;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data JPA repository for the ClientField entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientFieldRepository extends JpaRepository<ClientField, Long> {

    List<ClientField> getByListId(Long id);

    List<ClientField> getByListIdAndIsOrgInformation(Long id, Boolean isOrgInformation);
}
