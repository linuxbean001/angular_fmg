package com.fmg.cma.repository;

import com.fmg.cma.domain.ClientListData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data JPA repository for the ClientListData entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientListDataRepository extends JpaRepository<ClientListData, Long> {

//    List<ClientListData> getByClientFieldIn(Collection<ClientField> clientFields);
}
