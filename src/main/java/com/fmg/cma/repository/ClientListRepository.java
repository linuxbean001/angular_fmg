package com.fmg.cma.repository;

import com.fmg.cma.domain.ClientList;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the ClientList entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientListRepository extends JpaRepository<ClientList, Long> {

    Page<ClientList> findByClientCampaignId(Long id, Pageable pageable);
}
