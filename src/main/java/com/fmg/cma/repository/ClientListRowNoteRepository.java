package com.fmg.cma.repository;

import com.fmg.cma.domain.ClientListRowNote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@SuppressWarnings("unused")
@Repository
public interface ClientListRowNoteRepository extends JpaRepository<ClientListRowNote, Long> {

    List<ClientListRowNote> getByListId(Long id);

    ClientListRowNote getByListIdAndRowNum(Long id, Long rowNum);
}
