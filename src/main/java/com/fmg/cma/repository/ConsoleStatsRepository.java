package com.fmg.cma.repository;

import com.fmg.cma.domain.ConsoleStats;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

/**
 * Spring Data JPA repository for the ConsoleStats entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ConsoleStatsRepository extends JpaRepository<ConsoleStats, Long>, JpaSpecificationExecutor<ConsoleStats> {

    // @Query("select cs from ConsoleStats cs where cs.user.login = ?#{principal.username}")
    // List<ConsoleStats> findByUserIsCurrentUser();

    @Query("select cs from ConsoleStats cs where cs.user.login = ?#{principal.username} and cs.stamp = :stamp")
    @Nullable
    ConsoleStats getByUserIsCurrentUser(@Param("stamp") LocalDate stamp);

}
