package com.fmg.cma.repository;

import com.fmg.cma.domain.OutcomeSubReason;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


/**
 * Spring Data JPA repository for the OutcomeSubReason entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OutcomeSubReasonRepository extends JpaRepository<OutcomeSubReason, Long>, JpaSpecificationExecutor<OutcomeSubReason> {

}
