package com.fmg.cma.repository;

import com.fmg.cma.domain.OutcomeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


/**
 * Spring Data JPA repository for the OutcomeType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OutcomeTypeRepository extends JpaRepository<OutcomeType, Long>, JpaSpecificationExecutor<OutcomeType> {

}
