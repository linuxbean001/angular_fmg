package com.fmg.cma.repository;

import com.fmg.cma.domain.SurveyQuestionField;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data JPA repository for the SurveyQuestionField entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SurveyQuestionFieldRepository extends JpaRepository<SurveyQuestionField, Long>, JpaSpecificationExecutor<SurveyQuestionField> {

    List<SurveyQuestionField> findBySurveyQuestionId(Long id);
}
