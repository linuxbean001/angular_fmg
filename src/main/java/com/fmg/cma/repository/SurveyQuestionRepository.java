package com.fmg.cma.repository;

import com.fmg.cma.domain.SurveyQuestion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data JPA repository for the SurveyQuestion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SurveyQuestionRepository extends JpaRepository<SurveyQuestion, Long>, JpaSpecificationExecutor<SurveyQuestion> {

    @Query("select survey_question from SurveyQuestion survey_question " +
        "left join fetch survey_question.fields " +
        "where survey_question.id = :id ")
    SurveyQuestion findOneWithEagerRelationships(@Param("id") Long id);

}
