package com.fmg.cma.repository;

import com.fmg.cma.domain.UserCompany;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the UserCompany entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserCompanyRepository extends JpaRepository<UserCompany, Long> {

}
