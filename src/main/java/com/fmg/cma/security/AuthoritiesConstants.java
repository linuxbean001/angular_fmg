package com.fmg.cma.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String SYSTEM_ADMIN = "ROLE_SYSTEM_ADMIN";

    public static final String USER = "ROLE_USER";

    public static final String SUPER_USER = "ROLE_SUPER_USER";

    public static final String PORTAL_USER = "ROLE_PORTAL_USER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    public static final String QA = "ROLE_QA";

    private AuthoritiesConstants() {
    }
}
