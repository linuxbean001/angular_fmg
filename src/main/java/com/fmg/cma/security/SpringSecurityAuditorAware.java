package com.fmg.cma.security;

import com.fmg.cma.config.Constants;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

/**
 * Implementation of AuditorAware based on Spring Security.
 */
@Component
public class SpringSecurityAuditorAware implements AuditorAware<String> {

    @Override
    public String getCurrentAuditor() {
        return SecurityUtils.findCurrentUserLogin().orElse(Constants.SYSTEM_ACCOUNT);
    }
}
