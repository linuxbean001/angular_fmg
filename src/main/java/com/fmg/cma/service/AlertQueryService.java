package com.fmg.cma.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.fmg.cma.domain.Alert;
import com.fmg.cma.domain.*; // for static metamodels
import com.fmg.cma.repository.AlertRepository;
import com.fmg.cma.service.dto.AlertCriteria;

import com.fmg.cma.service.dto.AlertDTO;
import com.fmg.cma.service.mapper.AlertMapper;

/**
 * Service for executing complex queries for Alert entities in the database.
 * The main input is a {@link AlertCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AlertDTO} or a {@link Page} of {@link AlertDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AlertQueryService extends QueryService<Alert> {

    private final Logger log = LoggerFactory.getLogger(AlertQueryService.class);


    private final AlertRepository alertRepository;

    private final AlertMapper alertMapper;

    public AlertQueryService(AlertRepository alertRepository, AlertMapper alertMapper) {
        this.alertRepository = alertRepository;
        this.alertMapper = alertMapper;
    }

    /**
     * Return a {@link List} of {@link AlertDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AlertDTO> findByCriteria(AlertCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Alert> specification = createSpecification(criteria);
        return alertMapper.toDto(alertRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link AlertDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AlertDTO> findByCriteria(AlertCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Alert> specification = createSpecification(criteria);
        final Page<Alert> result = alertRepository.findAll(specification, page);
        return result.map(alertMapper::toDto);
    }

    /**
     * Function to convert AlertCriteria to a {@link Specifications}
     */
    private Specifications<Alert> createSpecification(AlertCriteria criteria) {
        Specifications<Alert> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Alert_.id));
            }
            if (criteria.getStamp() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStamp(), Alert_.stamp));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Alert_.description));
            }
            if (criteria.getType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getType(), Alert_.type));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatus(), Alert_.status));
            }
            if (criteria.getContactRowNums() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContactRowNums(), Alert_.contactRowNums));
            }
            if (criteria.getClientCampaignId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getClientCampaignId(), Alert_.clientCampaign, ClientCampaign_.id));
            }
            if (criteria.getClientListId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getClientListId(), Alert_.clientList, ClientList_.id));
            }
            if (criteria.getAlertUserId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getAlertUserId(), Alert_.alertUsers, AlertUser_.id));
            }
        }
        return specification;
    }

}
