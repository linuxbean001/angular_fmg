package com.fmg.cma.service;

import com.fmg.cma.domain.Alert;
import com.fmg.cma.repository.AlertRepository;
import com.fmg.cma.service.dto.AlertDTO;
import com.fmg.cma.service.mapper.AlertMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Alert.
 */
@Service
@Transactional
public class AlertService {

    private final Logger log = LoggerFactory.getLogger(AlertService.class);

    private final AlertRepository alertRepository;

    private final AlertMapper alertMapper;

    public AlertService(AlertRepository alertRepository, AlertMapper alertMapper) {
        this.alertRepository = alertRepository;
        this.alertMapper = alertMapper;
    }

    /**
     * Save a alert.
     *
     * @param alertDTO the entity to save
     * @return the persisted entity
     */
    public AlertDTO save(AlertDTO alertDTO) {
        log.debug("Request to save Alert : {}", alertDTO);
        Alert alert = alertMapper.toEntity(alertDTO);
        alert = alertRepository.save(alert);
        return alertMapper.toDto(alert);
    }

    /**
     * Get all the alerts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AlertDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Alerts");
        return alertRepository.findAll(pageable)
            .map(alertMapper::toDto);
    }

    /**
     * Get one alert by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public AlertDTO findOne(Long id) {
        log.debug("Request to get Alert : {}", id);
        Alert alert = alertRepository.findOne(id);
        return alertMapper.toDto(alert);
    }

    /**
     * Delete the alert by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Alert : {}", id);
        alertRepository.delete(id);
    }
}
