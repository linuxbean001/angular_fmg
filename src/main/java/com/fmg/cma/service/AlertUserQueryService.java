package com.fmg.cma.service;


import com.fmg.cma.domain.AlertUser;
import com.fmg.cma.domain.AlertUser_;
import com.fmg.cma.domain.Alert_;
import com.fmg.cma.domain.User_;
import com.fmg.cma.repository.AlertUserRepository;
import com.fmg.cma.service.dto.AlertUserCriteria;
import com.fmg.cma.service.dto.AlertUserDTO;
import com.fmg.cma.service.mapper.AlertUserMapper;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for AlertUser entities in the database.
 * The main input is a {@link AlertUserCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AlertUserDTO} or a {@link Page} of {@link AlertUserDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AlertUserQueryService extends QueryService<AlertUser> {

    private final Logger log = LoggerFactory.getLogger(AlertUserQueryService.class);


    private final AlertUserRepository alertUserRepository;

    private final AlertUserMapper alertUserMapper;

    public AlertUserQueryService(AlertUserRepository alertUserRepository, AlertUserMapper alertUserMapper) {
        this.alertUserRepository = alertUserRepository;
        this.alertUserMapper = alertUserMapper;
    }

    /**
     * Return a {@link List} of {@link AlertUserDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AlertUserDTO> findByCriteria(AlertUserCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<AlertUser> specification = createSpecification(criteria);
        return alertUserMapper.toDto(alertUserRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link AlertUserDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AlertUserDTO> findByCriteria(AlertUserCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<AlertUser> specification = createSpecification(criteria);
        final Page<AlertUser> result = alertUserRepository.findAll(specification, page);
        return result.map(alertUserMapper::toDto);
    }

    /**
     * Function to convert AlertUserCriteria to a {@link Specifications}
     */
    private Specifications<AlertUser> createSpecification(AlertUserCriteria criteria) {
        Specifications<AlertUser> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AlertUser_.id));
            }
            if (criteria.getIsRead() != null) {
                specification = specification.and(buildSpecification(criteria.getIsRead(), AlertUser_.isRead));
            }
            if (criteria.getReadDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getReadDate(), AlertUser_.readDate));
            }
            if (criteria.getAlertId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getAlertId(), AlertUser_.alert, Alert_.id));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getUserId(), AlertUser_.user, User_.id));
            }
        }
        return specification;
    }

}
