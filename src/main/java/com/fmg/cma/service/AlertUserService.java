package com.fmg.cma.service;

import com.fmg.cma.domain.AlertUser;
import com.fmg.cma.repository.AlertUserRepository;
import com.fmg.cma.service.dto.AlertUserDTO;
import com.fmg.cma.service.mapper.AlertUserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing AlertUser.
 */
@Service
@Transactional
public class AlertUserService {

    private final Logger log = LoggerFactory.getLogger(AlertUserService.class);

    private final AlertUserRepository alertUserRepository;

    private final AlertUserMapper alertUserMapper;

    public AlertUserService(AlertUserRepository alertUserRepository, AlertUserMapper alertUserMapper) {
        this.alertUserRepository = alertUserRepository;
        this.alertUserMapper = alertUserMapper;
    }

    /**
     * Save a alertUser.
     *
     * @param alertUserDTO the entity to save
     * @return the persisted entity
     */
    public AlertUserDTO save(AlertUserDTO alertUserDTO) {
        log.debug("Request to save AlertUser : {}", alertUserDTO);
        AlertUser alertUser = alertUserMapper.toEntity(alertUserDTO);
        alertUser = alertUserRepository.save(alertUser);
        return alertUserMapper.toDto(alertUser);
    }

    /**
     * Get all the alertUsers.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AlertUserDTO> findAll(Pageable pageable) {
        log.debug("Request to get all AlertUsers");
        return alertUserRepository.findAll(pageable)
            .map(alertUserMapper::toDto);
    }

    /**
     * Get one alertUser by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public AlertUserDTO findOne(Long id) {
        log.debug("Request to get AlertUser : {}", id);
        AlertUser alertUser = alertUserRepository.findOne(id);
        return alertUserMapper.toDto(alertUser);
    }

    /**
     * Delete the alertUser by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete AlertUser : {}", id);
        alertUserRepository.delete(id);
    }
}
