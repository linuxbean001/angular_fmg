package com.fmg.cma.service;


import com.fmg.cma.domain.AppointmentAttendee;
import com.fmg.cma.domain.AppointmentAttendee_;
import com.fmg.cma.domain.ClientCampaign_;
import com.fmg.cma.repository.AppointmentAttendeeRepository;
import com.fmg.cma.service.dto.AppointmentAttendeeCriteria;
import com.fmg.cma.service.dto.AppointmentAttendeeDTO;
import com.fmg.cma.service.mapper.AppointmentAttendeeMapper;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for AppointmentAttendee entities in the database.
 * The main input is a {@link AppointmentAttendeeCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AppointmentAttendeeDTO} or a {@link Page} of {@link AppointmentAttendeeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AppointmentAttendeeQueryService extends QueryService<AppointmentAttendee> {

    private final Logger log = LoggerFactory.getLogger(AppointmentAttendeeQueryService.class);


    private final AppointmentAttendeeRepository appointmentAttendeeRepository;

    private final AppointmentAttendeeMapper appointmentAttendeeMapper;

    public AppointmentAttendeeQueryService(AppointmentAttendeeRepository appointmentAttendeeRepository, AppointmentAttendeeMapper appointmentAttendeeMapper) {
        this.appointmentAttendeeRepository = appointmentAttendeeRepository;
        this.appointmentAttendeeMapper = appointmentAttendeeMapper;
    }

    /**
     * Return a {@link List} of {@link AppointmentAttendeeDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AppointmentAttendeeDTO> findByCriteria(AppointmentAttendeeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<AppointmentAttendee> specification = createSpecification(criteria);
        return appointmentAttendeeMapper.toDto(appointmentAttendeeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link AppointmentAttendeeDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AppointmentAttendeeDTO> findByCriteria(AppointmentAttendeeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<AppointmentAttendee> specification = createSpecification(criteria);
        final Page<AppointmentAttendee> result = appointmentAttendeeRepository.findAll(specification, page);
        return result.map(appointmentAttendeeMapper::toDto);
    }

    /**
     * Function to convert AppointmentAttendeeCriteria to a {@link Specifications}
     */
    private Specifications<AppointmentAttendee> createSpecification(AppointmentAttendeeCriteria criteria) {
        Specifications<AppointmentAttendee> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AppointmentAttendee_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), AppointmentAttendee_.name));
            }
            if (criteria.getContactDetail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContactDetail(), AppointmentAttendee_.contactDetail));
            }
            if (criteria.getDomain() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDomain(), AppointmentAttendee_.domain));
            }
            if (criteria.getUsername() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUsername(), AppointmentAttendee_.username));
            }
            if (criteria.getPassword() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPassword(), AppointmentAttendee_.password));
            }
            if (criteria.getAppointmentDays() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAppointmentDays(), AppointmentAttendee_.appointmentDays));
            }
            if (criteria.getClientCampaignId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getClientCampaignId(), AppointmentAttendee_.clientCampaign, ClientCampaign_.id));
            }
        }
        return specification;
    }

}
