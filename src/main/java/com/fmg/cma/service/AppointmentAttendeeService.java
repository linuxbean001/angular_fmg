package com.fmg.cma.service;

import com.fmg.cma.domain.AppointmentAttendee;
import com.fmg.cma.repository.AppointmentAttendeeRepository;
import com.fmg.cma.service.dto.AppointmentAttendeeDTO;
import com.fmg.cma.service.mapper.AppointmentAttendeeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing AppointmentAttendee.
 */
@Service
@Transactional
public class AppointmentAttendeeService {

    private final Logger log = LoggerFactory.getLogger(AppointmentAttendeeService.class);

    private final AppointmentAttendeeRepository appointmentAttendeeRepository;

    private final AppointmentAttendeeMapper appointmentAttendeeMapper;

    public AppointmentAttendeeService(AppointmentAttendeeRepository appointmentAttendeeRepository, AppointmentAttendeeMapper appointmentAttendeeMapper) {
        this.appointmentAttendeeRepository = appointmentAttendeeRepository;
        this.appointmentAttendeeMapper = appointmentAttendeeMapper;
    }

    /**
     * Save a appointmentAttendee.
     *
     * @param appointmentAttendeeDTO the entity to save
     * @return the persisted entity
     */
    public AppointmentAttendeeDTO save(AppointmentAttendeeDTO appointmentAttendeeDTO) {
        log.debug("Request to save AppointmentAttendee : {}", appointmentAttendeeDTO);
        AppointmentAttendee appointmentAttendee = appointmentAttendeeMapper.toEntity(appointmentAttendeeDTO);
        appointmentAttendee = appointmentAttendeeRepository.save(appointmentAttendee);
        return appointmentAttendeeMapper.toDto(appointmentAttendee);
    }

    /**
     * Get all the appointmentAttendees.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AppointmentAttendeeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all AppointmentAttendees");
        return appointmentAttendeeRepository.findAll(pageable)
            .map(appointmentAttendeeMapper::toDto);
    }

    /**
     * Get one appointmentAttendee by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public AppointmentAttendeeDTO findOne(Long id) {
        log.debug("Request to get AppointmentAttendee : {}", id);
        AppointmentAttendee appointmentAttendee = appointmentAttendeeRepository.findOne(id);
        return appointmentAttendeeMapper.toDto(appointmentAttendee);
    }

    /**
     * Delete the appointmentAttendee by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete AppointmentAttendee : {}", id);
        appointmentAttendeeRepository.delete(id);
    }
}
