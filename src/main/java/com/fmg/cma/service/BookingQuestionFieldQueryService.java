package com.fmg.cma.service;

import com.fmg.cma.domain.BookingQuestionField;
import com.fmg.cma.domain.BookingQuestionField_;
import com.fmg.cma.domain.BookingQuestion_;
import com.fmg.cma.repository.BookingQuestionFieldRepository;
import com.fmg.cma.service.dto.BookingQuestionFieldCriteria;
import com.fmg.cma.service.dto.BookingQuestionFieldDTO;
import com.fmg.cma.service.mapper.BookingQuestionFieldMapper;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for BookingQuestionField entities in the database.
 * The main input is a {@link BookingQuestionFieldCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link BookingQuestionFieldDTO} or a {@link Page} of {@link BookingQuestionFieldDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class BookingQuestionFieldQueryService extends QueryService<BookingQuestionField> {

    private final Logger log = LoggerFactory.getLogger(BookingQuestionFieldQueryService.class);


    private final BookingQuestionFieldRepository bookingQuestionFieldRepository;

    private final BookingQuestionFieldMapper bookingQuestionFieldMapper;

    public BookingQuestionFieldQueryService(BookingQuestionFieldRepository bookingQuestionFieldRepository, BookingQuestionFieldMapper bookingQuestionFieldMapper) {
        this.bookingQuestionFieldRepository = bookingQuestionFieldRepository;
        this.bookingQuestionFieldMapper = bookingQuestionFieldMapper;
    }

    /**
     * Return a {@link List} of {@link BookingQuestionFieldDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<BookingQuestionFieldDTO> findByCriteria(BookingQuestionFieldCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<BookingQuestionField> specification = createSpecification(criteria);
        return bookingQuestionFieldMapper.toDto(bookingQuestionFieldRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link BookingQuestionFieldDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<BookingQuestionFieldDTO> findByCriteria(BookingQuestionFieldCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<BookingQuestionField> specification = createSpecification(criteria);
        final Page<BookingQuestionField> result = bookingQuestionFieldRepository.findAll(specification, page);
        return result.map(bookingQuestionFieldMapper::toDto);
    }

    /**
     * Function to convert BookingQuestionFieldCriteria to a {@link Specifications}
     */
    private Specifications<BookingQuestionField> createSpecification(BookingQuestionFieldCriteria criteria) {
        Specifications<BookingQuestionField> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), BookingQuestionField_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), BookingQuestionField_.name));
            }
            if (criteria.getDataType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDataType(), BookingQuestionField_.dataType));
            }
            if (criteria.getCategory() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCategory(), BookingQuestionField_.category));
            }
            if (criteria.getPrePopulationData() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPrePopulationData(), BookingQuestionField_.prePopulationData));
            }
            if (criteria.getIsVisible() != null) {
                specification = specification.and(buildSpecification(criteria.getIsVisible(), BookingQuestionField_.isVisible));
            }
            if (criteria.getIsRequired() != null) {
                specification = specification.and(buildSpecification(criteria.getIsRequired(), BookingQuestionField_.isRequired));
            }
            if (criteria.getBookingQuestionId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getBookingQuestionId(), BookingQuestionField_.bookingQuestion, BookingQuestion_.id));
            }
        }
        return specification;
    }

}
