package com.fmg.cma.service;

import com.fmg.cma.domain.BookingQuestionField;
import com.fmg.cma.repository.BookingQuestionFieldRepository;
import com.fmg.cma.service.dto.BookingQuestionFieldDTO;
import com.fmg.cma.service.mapper.BookingQuestionFieldMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing BookingQuestionField.
 */
@Service
@Transactional
public class BookingQuestionFieldService {

    private final Logger log = LoggerFactory.getLogger(BookingQuestionFieldService.class);

    private final BookingQuestionFieldRepository bookingQuestionFieldRepository;

    private final BookingQuestionFieldMapper bookingQuestionFieldMapper;

    public BookingQuestionFieldService(BookingQuestionFieldRepository bookingQuestionFieldRepository, BookingQuestionFieldMapper bookingQuestionFieldMapper) {
        this.bookingQuestionFieldRepository = bookingQuestionFieldRepository;
        this.bookingQuestionFieldMapper = bookingQuestionFieldMapper;
    }

    /**
     * Save a bookingQuestionField.
     *
     * @param bookingQuestionFieldDTO the entity to save
     * @return the persisted entity
     */
    public BookingQuestionFieldDTO save(BookingQuestionFieldDTO bookingQuestionFieldDTO) {
        log.debug("Request to save BookingQuestionField : {}", bookingQuestionFieldDTO);
        BookingQuestionField bookingQuestionField = bookingQuestionFieldMapper.toEntity(bookingQuestionFieldDTO);
        bookingQuestionField = bookingQuestionFieldRepository.save(bookingQuestionField);
        return bookingQuestionFieldMapper.toDto(bookingQuestionField);
    }

    /**
     * Get all the bookingQuestionFields.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BookingQuestionFieldDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BookingQuestionFields");
        return bookingQuestionFieldRepository.findAll(pageable)
            .map(bookingQuestionFieldMapper::toDto);
    }

    /**
     * Get one bookingQuestionField by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public BookingQuestionFieldDTO findOne(Long id) {
        log.debug("Request to get BookingQuestionField : {}", id);
        BookingQuestionField bookingQuestionField = bookingQuestionFieldRepository.findOne(id);
        return bookingQuestionFieldMapper.toDto(bookingQuestionField);
    }

    /**
     * Delete the bookingQuestionField by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete BookingQuestionField : {}", id);
        bookingQuestionFieldRepository.delete(id);
    }
}
