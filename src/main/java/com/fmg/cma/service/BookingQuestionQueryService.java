package com.fmg.cma.service;

import com.fmg.cma.domain.BookingQuestion;
import com.fmg.cma.domain.BookingQuestionField_;
import com.fmg.cma.domain.BookingQuestion_;
import com.fmg.cma.domain.ClientCampaign_;
import com.fmg.cma.repository.BookingQuestionRepository;
import com.fmg.cma.service.dto.BookingQuestionCriteria;
import com.fmg.cma.service.dto.BookingQuestionDTO;
import com.fmg.cma.service.mapper.BookingQuestionMapper;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for BookingQuestion entities in the database.
 * The main input is a {@link BookingQuestionCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link BookingQuestionDTO} or a {@link Page} of {@link BookingQuestionDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class BookingQuestionQueryService extends QueryService<BookingQuestion> {

    private final Logger log = LoggerFactory.getLogger(BookingQuestionQueryService.class);


    private final BookingQuestionRepository bookingQuestionRepository;

    private final BookingQuestionMapper bookingQuestionMapper;

    public BookingQuestionQueryService(BookingQuestionRepository bookingQuestionRepository, BookingQuestionMapper bookingQuestionMapper) {
        this.bookingQuestionRepository = bookingQuestionRepository;
        this.bookingQuestionMapper = bookingQuestionMapper;
    }

    /**
     * Return a {@link List} of {@link BookingQuestionDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<BookingQuestionDTO> findByCriteria(BookingQuestionCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<BookingQuestion> specification = createSpecification(criteria);
        return bookingQuestionMapper.toDto(bookingQuestionRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link BookingQuestionDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<BookingQuestionDTO> findByCriteria(BookingQuestionCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<BookingQuestion> specification = createSpecification(criteria);
        final Page<BookingQuestion> result = bookingQuestionRepository.findAll(specification, page);
        return result.map(bookingQuestionMapper::toDto);
    }

    /**
     * Function to convert BookingQuestionCriteria to a {@link Specifications}
     */
    private Specifications<BookingQuestion> createSpecification(BookingQuestionCriteria criteria) {
        Specifications<BookingQuestion> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), BookingQuestion_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), BookingQuestion_.name));
            }
            if (criteria.getQuestionText() != null) {
                specification = specification.and(buildStringSpecification(criteria.getQuestionText(), BookingQuestion_.questionText));
            }
            if (criteria.getOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getOrder(), BookingQuestion_.order));
            }
            if (criteria.getIsVisible() != null) {
                specification = specification.and(buildSpecification(criteria.getIsVisible(), BookingQuestion_.isVisible));
            }
            if (criteria.getFieldsId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getFieldsId(), BookingQuestion_.fields, BookingQuestionField_.id));
            }
            if (criteria.getClientCampaignId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getClientCampaignId(), BookingQuestion_.clientCampaign, ClientCampaign_.id));
            }
        }
        return specification;
    }

}
