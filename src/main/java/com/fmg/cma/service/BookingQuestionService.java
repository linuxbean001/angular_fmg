package com.fmg.cma.service;

import com.fmg.cma.domain.BookingQuestion;
import com.fmg.cma.domain.BookingQuestionField;
import com.fmg.cma.repository.BookingQuestionFieldRepository;
import com.fmg.cma.repository.BookingQuestionRepository;
import com.fmg.cma.service.dto.BookingQuestionDTO;
import com.fmg.cma.service.mapper.BookingQuestionFieldMapper;
import com.fmg.cma.service.mapper.BookingQuestionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

/**
 * Service Implementation for managing BookingQuestion.
 */
@Service
@Transactional
public class BookingQuestionService {

    private final Logger log = LoggerFactory.getLogger(BookingQuestionService.class);

    private final BookingQuestionRepository bookingQuestionRepository;

    private final BookingQuestionFieldRepository bookingQuestionFieldRepository;

    private final BookingQuestionMapper bookingQuestionMapper;

    private final BookingQuestionFieldMapper bookingQuestionFieldMapper;

    public BookingQuestionService(BookingQuestionRepository bookingQuestionRepository,
                                  BookingQuestionFieldRepository bookingQuestionFieldRepository,
                                  BookingQuestionMapper bookingQuestionMapper,
                                  BookingQuestionFieldMapper bookingQuestionFieldMapper) {
        this.bookingQuestionRepository = bookingQuestionRepository;
        this.bookingQuestionFieldRepository = bookingQuestionFieldRepository;
        this.bookingQuestionMapper = bookingQuestionMapper;
        this.bookingQuestionFieldMapper = bookingQuestionFieldMapper;
    }

    /**
     * Save a bookingQuestion.
     *
     * @param bookingQuestionDTO the entity to save
     * @return the persisted entity
     */
    public BookingQuestionDTO save(BookingQuestionDTO bookingQuestionDTO) {
        log.debug("Request to save BookingQuestion : {}", bookingQuestionDTO);
        BookingQuestion bookingQuestion = bookingQuestionMapper.toEntity(bookingQuestionDTO);
        // bookingQuestion = bookingQuestionRepository.save(bookingQuestion);

        boolean isNew = bookingQuestion.getId() == null;
        bookingQuestion = bookingQuestionRepository.save(bookingQuestion);
        if (isNew) {
            // re-save to fill order equals to id
            bookingQuestion.setOrder(bookingQuestion.getId());
            bookingQuestion = bookingQuestionRepository.save(bookingQuestion);
        }

        Set<BookingQuestionField> fields = bookingQuestionFieldMapper.toEntitySet(bookingQuestionDTO.getFields());
        if (fields != null) {
            if (!isNew) {
                // clear removed fields from a question
                Set<Long> ids = fields.stream().map(BookingQuestionField::getId).filter(Objects::nonNull).collect(toSet());

                Set<BookingQuestionField> removeFields = bookingQuestionFieldRepository.findByBookingQuestionId(bookingQuestion.getId()).stream()
                    .filter(field -> !ids.contains(field.getId()))
                    .collect(toSet());

                bookingQuestionFieldRepository.delete(removeFields);
            }

            for (BookingQuestionField field : fields) {
                field.setBookingQuestion(bookingQuestion);
                bookingQuestionFieldRepository.save(field);
            }
        }

        return bookingQuestionMapper.toDto(bookingQuestion);
    }

    /**
     * Get all the bookingQuestions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BookingQuestionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BookingQuestions");
        return bookingQuestionRepository.findAll(pageable)
            .map(bookingQuestionMapper::toDto);
    }

    /**
     * Get one bookingQuestion by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public BookingQuestionDTO findOne(Long id) {
        log.debug("Request to get BookingQuestion : {}", id);
        BookingQuestion bookingQuestion = bookingQuestionRepository.findOneWithEagerRelationships(id);
        return bookingQuestionMapper.toDto(bookingQuestion);
    }

    /**
     * Delete the bookingQuestion by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete BookingQuestion : {}", id);
        bookingQuestionRepository.delete(id);
    }
}
