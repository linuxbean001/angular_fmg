package com.fmg.cma.service;

import com.fmg.cma.domain.CallHistory;
import com.fmg.cma.repository.CallHistoryRepository;
import com.fmg.cma.repository.UserRepository;
import com.fmg.cma.security.SecurityUtils;
import com.fmg.cma.service.dto.CallHistoryDTO;
import com.fmg.cma.service.mapper.CallHistoryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;


/**
 * Service Implementation for managing CallHistory.
 */
@Service
@Transactional
public class CallHistoryService {

    private final Logger log = LoggerFactory.getLogger(CallHistoryService.class);

    private final CallHistoryRepository callHistoryRepository;

    private final CallHistoryMapper callHistoryMapper;

    private final UserRepository userRepository;

    public CallHistoryService(CallHistoryRepository callHistoryRepository,
                              CallHistoryMapper callHistoryMapper,
                              UserRepository userRepository) {
        this.callHistoryRepository = callHistoryRepository;
        this.callHistoryMapper = callHistoryMapper;
        this.userRepository = userRepository;
    }

    /**
     * Save a callHistory.
     *
     * @param callHistoryDTO the entity to save
     * @return the persisted entity
     */
    public CallHistoryDTO save(CallHistoryDTO callHistoryDTO) {
        log.debug("Request to save CallHistory : {}", callHistoryDTO);
        CallHistory callHistory = callHistoryMapper.toEntity(callHistoryDTO);
        callHistory = save(callHistory);
        return callHistoryMapper.toDto(callHistory);
    }

    public CallHistory save(CallHistory callHistory) {
        fillCalHistoryDto(callHistory);
        return callHistoryRepository.save(callHistory);
    }

    private void fillCalHistoryDto(CallHistory callHistory) {
        if (callHistory.getId() != null) {
            return;
        }

        callHistory.setCreatedDate(ZonedDateTime.now());
        callHistory.setUser(userRepository.getByLogin(SecurityUtils.getCurrentUserLogin()));
    }

    /**
     * Get all the callHistories.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CallHistoryDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CallHistories");
        return callHistoryRepository.findAll(pageable)
            .map(callHistoryMapper::toDto);
    }

    /**
     * Get one callHistory by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public CallHistoryDTO findOne(Long id) {
        log.debug("Request to get CallHistory : {}", id);
        CallHistory callHistory = callHistoryRepository.findOne(id);
        return callHistoryMapper.toDto(callHistory);
    }

    /**
     * Delete the callHistory by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CallHistory : {}", id);
        callHistoryRepository.delete(id);
    }
}
