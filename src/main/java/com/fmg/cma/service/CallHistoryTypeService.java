package com.fmg.cma.service;

import com.fmg.cma.domain.CallHistoryType;
import com.fmg.cma.repository.CallHistoryTypeRepository;
import com.fmg.cma.service.dto.CallHistoryTypeDTO;
import com.fmg.cma.service.mapper.CallHistoryTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing CallHistoryType.
 */
@Service
@Transactional
public class CallHistoryTypeService {

    private final Logger log = LoggerFactory.getLogger(CallHistoryTypeService.class);

    private final CallHistoryTypeRepository callHistoryTypeRepository;

    private final CallHistoryTypeMapper callHistoryTypeMapper;

    public CallHistoryTypeService(CallHistoryTypeRepository callHistoryTypeRepository, CallHistoryTypeMapper callHistoryTypeMapper) {
        this.callHistoryTypeRepository = callHistoryTypeRepository;
        this.callHistoryTypeMapper = callHistoryTypeMapper;
    }

    /**
     * Save a callHistoryType.
     *
     * @param callHistoryTypeDTO the entity to save
     * @return the persisted entity
     */
    public CallHistoryTypeDTO save(CallHistoryTypeDTO callHistoryTypeDTO) {
        log.debug("Request to save CallHistoryType : {}", callHistoryTypeDTO);
        CallHistoryType callHistoryType = callHistoryTypeMapper.toEntity(callHistoryTypeDTO);
        callHistoryType = callHistoryTypeRepository.save(callHistoryType);
        return callHistoryTypeMapper.toDto(callHistoryType);
    }

    /**
     * Get all the callHistoryTypes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CallHistoryTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CallHistoryTypes");
        return callHistoryTypeRepository.findAll(pageable)
            .map(callHistoryTypeMapper::toDto);
    }

    /**
     * Get one callHistoryType by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public CallHistoryTypeDTO findOne(Long id) {
        log.debug("Request to get CallHistoryType : {}", id);
        CallHistoryType callHistoryType = callHistoryTypeRepository.findOne(id);
        return callHistoryTypeMapper.toDto(callHistoryType);
    }

    /**
     * Delete the callHistoryType by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CallHistoryType : {}", id);
        callHistoryTypeRepository.delete(id);
    }
}
