package com.fmg.cma.service;


import com.fmg.cma.domain.CampaignListContactManagement;
import com.fmg.cma.domain.CampaignListContactManagement_;
import com.fmg.cma.domain.ClientCampaign_;
import com.fmg.cma.domain.ClientList_;
import com.fmg.cma.repository.CampaignListContactManagementRepository;
import com.fmg.cma.service.dto.CampaignListContactManagementCriteria;
import com.fmg.cma.service.dto.CampaignListContactManagementDTO;
import com.fmg.cma.service.mapper.CampaignListContactManagementMapper;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for CampaignListContactManagement entities in the database.
 * The main input is a {@link CampaignListContactManagementCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CampaignListContactManagementDTO} or a {@link Page} of {@link CampaignListContactManagementDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CampaignListContactManagementQueryService extends QueryService<CampaignListContactManagement> {

    private final Logger log = LoggerFactory.getLogger(CampaignListContactManagementQueryService.class);


    private final CampaignListContactManagementRepository campaignListContactManagementRepository;

    private final CampaignListContactManagementMapper campaignListContactManagementMapper;

    public CampaignListContactManagementQueryService(CampaignListContactManagementRepository campaignListContactManagementRepository, CampaignListContactManagementMapper campaignListContactManagementMapper) {
        this.campaignListContactManagementRepository = campaignListContactManagementRepository;
        this.campaignListContactManagementMapper = campaignListContactManagementMapper;
    }

    /**
     * Return a {@link List} of {@link CampaignListContactManagementDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CampaignListContactManagementDTO> findByCriteria(CampaignListContactManagementCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<CampaignListContactManagement> specification = createSpecification(criteria);
        return campaignListContactManagementMapper.toDto(campaignListContactManagementRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link CampaignListContactManagementDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CampaignListContactManagementDTO> findByCriteria(CampaignListContactManagementCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<CampaignListContactManagement> specification = createSpecification(criteria);
        final Page<CampaignListContactManagement> result = campaignListContactManagementRepository.findAll(specification, page);
        return result.map(campaignListContactManagementMapper::toDto);
    }

    /**
     * Function to convert CampaignListContactManagementCriteria to a {@link Specifications}
     */
    private Specifications<CampaignListContactManagement> createSpecification(CampaignListContactManagementCriteria criteria) {
        Specifications<CampaignListContactManagement> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), CampaignListContactManagement_.id));
            }
            if (criteria.getContactRowNum() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getContactRowNum(), CampaignListContactManagement_.contactRowNum));
            }
            if (criteria.getHoldFromCallPool() != null) {
                specification = specification.and(buildSpecification(criteria.getHoldFromCallPool(), CampaignListContactManagement_.holdFromCallPool));
            }
            if (criteria.getStamp() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStamp(), CampaignListContactManagement_.stamp));
            }
            if (criteria.getClientCampaignId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getClientCampaignId(), CampaignListContactManagement_.clientCampaign, ClientCampaign_.id));
            }
            if (criteria.getClientListId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getClientListId(), CampaignListContactManagement_.clientList, ClientList_.id));
            }
        }
        return specification;
    }

}
