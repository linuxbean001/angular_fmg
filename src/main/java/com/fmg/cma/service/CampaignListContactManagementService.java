package com.fmg.cma.service;

import com.fmg.cma.domain.CampaignListContactManagement;
import com.fmg.cma.repository.CampaignListContactManagementRepository;
import com.fmg.cma.service.dto.CampaignListContactManagementDTO;
import com.fmg.cma.service.mapper.CampaignListContactManagementMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing CampaignListContactManagement.
 */
@Service
@Transactional
public class CampaignListContactManagementService {

    private final Logger log = LoggerFactory.getLogger(CampaignListContactManagementService.class);

    private final CampaignListContactManagementRepository campaignListContactManagementRepository;

    private final CampaignListContactManagementMapper campaignListContactManagementMapper;

    public CampaignListContactManagementService(CampaignListContactManagementRepository campaignListContactManagementRepository, CampaignListContactManagementMapper campaignListContactManagementMapper) {
        this.campaignListContactManagementRepository = campaignListContactManagementRepository;
        this.campaignListContactManagementMapper = campaignListContactManagementMapper;
    }

    /**
     * Save a campaignListContactManagement.
     *
     * @param campaignListContactManagementDTO the entity to save
     * @return the persisted entity
     */
    public CampaignListContactManagementDTO save(CampaignListContactManagementDTO campaignListContactManagementDTO) {
        log.debug("Request to save CampaignListContactManagement : {}", campaignListContactManagementDTO);
        CampaignListContactManagement campaignListContactManagement = campaignListContactManagementMapper.toEntity(campaignListContactManagementDTO);
        campaignListContactManagement = campaignListContactManagementRepository.save(campaignListContactManagement);
        return campaignListContactManagementMapper.toDto(campaignListContactManagement);
    }

    /**
     * Get all the campaignListContactManagements.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CampaignListContactManagementDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CampaignListContactManagements");
        return campaignListContactManagementRepository.findAll(pageable)
            .map(campaignListContactManagementMapper::toDto);
    }

    /**
     * Get one campaignListContactManagement by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public CampaignListContactManagementDTO findOne(Long id) {
        log.debug("Request to get CampaignListContactManagement : {}", id);
        CampaignListContactManagement campaignListContactManagement = campaignListContactManagementRepository.findOne(id);
        return campaignListContactManagementMapper.toDto(campaignListContactManagement);
    }

    /**
     * Delete the campaignListContactManagement by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CampaignListContactManagement : {}", id);
        campaignListContactManagementRepository.delete(id);
    }
}
