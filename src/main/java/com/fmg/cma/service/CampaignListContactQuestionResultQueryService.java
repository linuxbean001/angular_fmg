package com.fmg.cma.service;


import com.fmg.cma.domain.BookingQuestionField_;
import com.fmg.cma.domain.CampaignListContactQuestionResult;
import com.fmg.cma.domain.CampaignListContactQuestionResult_;
import com.fmg.cma.domain.ClientCampaign_;
import com.fmg.cma.domain.ClientList_;
import com.fmg.cma.repository.CampaignListContactQuestionResultRepository;
import com.fmg.cma.service.dto.CampaignListContactQuestionResultCriteria;
import com.fmg.cma.service.dto.CampaignListContactQuestionResultDTO;
import com.fmg.cma.service.mapper.CampaignListContactQuestionResultMapper;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for CampaignListContactQuestionResult entities in the database.
 * The main input is a {@link CampaignListContactQuestionResultCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CampaignListContactQuestionResultDTO} or a {@link Page} of {@link CampaignListContactQuestionResultDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CampaignListContactQuestionResultQueryService extends QueryService<CampaignListContactQuestionResult> {

    private final Logger log = LoggerFactory.getLogger(CampaignListContactQuestionResultQueryService.class);


    private final CampaignListContactQuestionResultRepository campaignListContactQuestionResultRepository;

    private final CampaignListContactQuestionResultMapper campaignListContactQuestionResultMapper;

    public CampaignListContactQuestionResultQueryService(CampaignListContactQuestionResultRepository campaignListContactQuestionResultRepository, CampaignListContactQuestionResultMapper campaignListContactQuestionResultMapper) {
        this.campaignListContactQuestionResultRepository = campaignListContactQuestionResultRepository;
        this.campaignListContactQuestionResultMapper = campaignListContactQuestionResultMapper;
    }

    /**
     * Return a {@link List} of {@link CampaignListContactQuestionResultDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CampaignListContactQuestionResultDTO> findByCriteria(CampaignListContactQuestionResultCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<CampaignListContactQuestionResult> specification = createSpecification(criteria);
        return campaignListContactQuestionResultMapper.toDto(campaignListContactQuestionResultRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link CampaignListContactQuestionResultDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CampaignListContactQuestionResultDTO> findByCriteria(CampaignListContactQuestionResultCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<CampaignListContactQuestionResult> specification = createSpecification(criteria);
        final Page<CampaignListContactQuestionResult> result = campaignListContactQuestionResultRepository.findAll(specification, page);
        return result.map(campaignListContactQuestionResultMapper::toDto);
    }

    /**
     * Function to convert CampaignListContactQuestionResultCriteria to a {@link Specifications}
     */
    private Specifications<CampaignListContactQuestionResult> createSpecification(CampaignListContactQuestionResultCriteria criteria) {
        Specifications<CampaignListContactQuestionResult> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), CampaignListContactQuestionResult_.id));
            }
            if (criteria.getContactRowNum() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getContactRowNum(), CampaignListContactQuestionResult_.contactRowNum));
            }
            if (criteria.getCvalue() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCvalue(), CampaignListContactQuestionResult_.cvalue));
            }
            if (criteria.getClientCampaignId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getClientCampaignId(), CampaignListContactQuestionResult_.clientCampaign, ClientCampaign_.id));
            }
            if (criteria.getClientListId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getClientListId(), CampaignListContactQuestionResult_.clientList, ClientList_.id));
            }
            if (criteria.getBookingQuestionFieldId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getBookingQuestionFieldId(), CampaignListContactQuestionResult_.bookingQuestionField, BookingQuestionField_.id));
            }
        }
        return specification;
    }

}
