package com.fmg.cma.service;

import com.fmg.cma.domain.CampaignListContactQuestionResult;
import com.fmg.cma.repository.CampaignListContactQuestionResultRepository;
import com.fmg.cma.service.dto.CampaignListContactQuestionResultDTO;
import com.fmg.cma.service.mapper.CampaignListContactQuestionResultMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


/**
 * Service Implementation for managing CampaignListContactQuestionResult.
 */
@Service
@Transactional
public class CampaignListContactQuestionResultService {

    private final Logger log = LoggerFactory.getLogger(CampaignListContactQuestionResultService.class);

    private final CampaignListContactQuestionResultRepository campaignListContactQuestionResultRepository;

    private final CampaignListContactQuestionResultMapper campaignListContactQuestionResultMapper;

    public CampaignListContactQuestionResultService(CampaignListContactQuestionResultRepository campaignListContactQuestionResultRepository, CampaignListContactQuestionResultMapper campaignListContactQuestionResultMapper) {
        this.campaignListContactQuestionResultRepository = campaignListContactQuestionResultRepository;
        this.campaignListContactQuestionResultMapper = campaignListContactQuestionResultMapper;
    }

    /**
     * Save a campaignListContactQuestionResult.
     *
     * @param campaignListContactQuestionResultDTO the entity to save
     * @return the persisted entity
     */
    public CampaignListContactQuestionResultDTO save(CampaignListContactQuestionResultDTO campaignListContactQuestionResultDTO) {
        log.debug("Request to save CampaignListContactQuestionResult : {}", campaignListContactQuestionResultDTO);
        CampaignListContactQuestionResult campaignListContactQuestionResult = campaignListContactQuestionResultMapper.toEntity(campaignListContactQuestionResultDTO);
        campaignListContactQuestionResult = campaignListContactQuestionResultRepository.save(campaignListContactQuestionResult);
        return campaignListContactQuestionResultMapper.toDto(campaignListContactQuestionResult);
    }

    public List<CampaignListContactQuestionResultDTO> save(List<CampaignListContactQuestionResultDTO> campaignListContactQuestionResultDTOs) {
        log.debug("Request to save CampaignListContactQuestionResults : {}", campaignListContactQuestionResultDTOs);
        return campaignListContactQuestionResultDTOs.stream()
            .map(this::save)
            .collect(Collectors.toList());
    }

    /**
     * Get all the campaignListContactQuestionResults.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CampaignListContactQuestionResultDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CampaignListContactQuestionResults");
        return campaignListContactQuestionResultRepository.findAll(pageable)
            .map(campaignListContactQuestionResultMapper::toDto);
    }

    /**
     * Get one campaignListContactQuestionResult by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public CampaignListContactQuestionResultDTO findOne(Long id) {
        log.debug("Request to get CampaignListContactQuestionResult : {}", id);
        CampaignListContactQuestionResult campaignListContactQuestionResult = campaignListContactQuestionResultRepository.findOne(id);
        return campaignListContactQuestionResultMapper.toDto(campaignListContactQuestionResult);
    }

    /**
     * Delete the campaignListContactQuestionResult by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CampaignListContactQuestionResult : {}", id);
        campaignListContactQuestionResultRepository.delete(id);
    }
}
