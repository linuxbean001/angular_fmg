package com.fmg.cma.service;


import com.fmg.cma.domain.CampaignListContactResult;
import com.fmg.cma.domain.CampaignListContactResult_;
import com.fmg.cma.domain.ClientCampaign_;
import com.fmg.cma.domain.ClientList_;
import com.fmg.cma.domain.OutcomeSubReason_;
import com.fmg.cma.repository.CampaignListContactResultRepository;
import com.fmg.cma.service.dto.CampaignListContactResultCriteria;
import com.fmg.cma.service.dto.CampaignListContactResultDTO;
import com.fmg.cma.service.mapper.CampaignListContactResultMapper;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for CampaignListContactResult entities in the database.
 * The main input is a {@link CampaignListContactResultCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CampaignListContactResultDTO} or a {@link Page} of {@link CampaignListContactResultDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CampaignListContactResultQueryService extends QueryService<CampaignListContactResult> {

    private final Logger log = LoggerFactory.getLogger(CampaignListContactResultQueryService.class);


    private final CampaignListContactResultRepository campaignListContactResultRepository;

    private final CampaignListContactResultMapper campaignListContactResultMapper;

    public CampaignListContactResultQueryService(CampaignListContactResultRepository campaignListContactResultRepository, CampaignListContactResultMapper campaignListContactResultMapper) {
        this.campaignListContactResultRepository = campaignListContactResultRepository;
        this.campaignListContactResultMapper = campaignListContactResultMapper;
    }

    /**
     * Return a {@link List} of {@link CampaignListContactResultDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CampaignListContactResultDTO> findByCriteria(CampaignListContactResultCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<CampaignListContactResult> specification = createSpecification(criteria);
        return campaignListContactResultMapper.toDto(campaignListContactResultRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link CampaignListContactResultDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CampaignListContactResultDTO> findByCriteria(CampaignListContactResultCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<CampaignListContactResult> specification = createSpecification(criteria);
        final Page<CampaignListContactResult> result = campaignListContactResultRepository.findAll(specification, page);
        return result.map(campaignListContactResultMapper::toDto);
    }

    /**
     * Function to convert CampaignListContactResultCriteria to a {@link Specifications}
     */
    private Specifications<CampaignListContactResult> createSpecification(CampaignListContactResultCriteria criteria) {
        Specifications<CampaignListContactResult> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), CampaignListContactResult_.id));
            }
            if (criteria.getContactRowNum() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getContactRowNum(), CampaignListContactResult_.contactRowNum));
            }
            if (criteria.getClientCampaignId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getClientCampaignId(), CampaignListContactResult_.clientCampaign, ClientCampaign_.id));
            }
            if (criteria.getClientListId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getClientListId(), CampaignListContactResult_.clientList, ClientList_.id));
            }
            if (criteria.getOutcomeSubReasonId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getOutcomeSubReasonId(), CampaignListContactResult_.outcomeSubReason, OutcomeSubReason_.id));
            }
        }
        return specification;
    }

}
