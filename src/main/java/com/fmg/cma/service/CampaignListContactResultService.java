package com.fmg.cma.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fmg.cma.domain.CallHistory;
import com.fmg.cma.domain.CallHistoryType;
import com.fmg.cma.domain.CampaignListContactResult;
import com.fmg.cma.domain.DataListContactState;
import com.fmg.cma.domain.Outcome;
import com.fmg.cma.domain.ToEmailTemplate;
import com.fmg.cma.domain.User;
import com.fmg.cma.repository.CallHistoryTypeRepository;
import com.fmg.cma.repository.CampaignListContactResultRepository;
import com.fmg.cma.repository.EmailTemplateRepository;
import com.fmg.cma.repository.UserRepository;
import com.fmg.cma.security.SecurityUtils;
import com.fmg.cma.service.dto.AlertElementDto;
import com.fmg.cma.service.dto.CampaignListContactResultDTO;
import com.fmg.cma.service.mapper.CampaignListContactResultMapper;
import com.fmg.cma.service.util.ObjectMapperUtils;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Service Implementation for managing CampaignListContactResult.
 */
@Service
@Transactional
public class CampaignListContactResultService {

    private final Logger log = LoggerFactory.getLogger(CampaignListContactResultService.class);

    private final CampaignListContactResultRepository campaignListContactResultRepository;

    private final CampaignListContactResultMapper campaignListContactResultMapper;

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private final UserRepository userRepository;

    private final JpaProperties jpaProperties;

    private final ObjectMapper objectMapper;

    private final MailService mailService;

    private final CallHistoryService callHistoryService;

    private final CallHistoryTypeRepository callHistoryTypeRepository;

    private final EmailTemplateRepository emailTemplateRepository;

    public CampaignListContactResultService(CampaignListContactResultRepository campaignListContactResultRepository,
                                            CampaignListContactResultMapper campaignListContactResultMapper,
                                            NamedParameterJdbcTemplate namedParameterJdbcTemplate,
                                            UserRepository userRepository,
                                            JpaProperties jpaProperties,
                                            ObjectMapper objectMapper,
                                            MailService mailService,
                                            CallHistoryService callHistoryService,
                                            CallHistoryTypeRepository callHistoryTypeRepository,
                                            EmailTemplateRepository emailTemplateRepository) {
        this.campaignListContactResultRepository = campaignListContactResultRepository;
        this.campaignListContactResultMapper = campaignListContactResultMapper;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
        this.userRepository = userRepository;
        this.jpaProperties = jpaProperties;
        this.objectMapper = objectMapper;
        this.mailService = mailService;
        this.callHistoryService = callHistoryService;
        this.callHistoryTypeRepository = callHistoryTypeRepository;
        this.emailTemplateRepository = emailTemplateRepository;
    }

    /**
     * Save a campaignListContactResult.
     *
     * @param dto the entity to save
     * @return the persisted entity
     */
    @Transactional
    public CampaignListContactResultDTO save(CampaignListContactResultDTO dto) {
        log.debug("Request to save CampaignListContactResult : {}", dto);
        CampaignListContactResult res = campaignListContactResultRepository.findByClientCampaignIdAndClientListIdAndContactRowNum(dto.getClientCampaignId(), dto.getClientListId(), dto.getContactRowNum())
            .map(result -> {
                campaignListContactResultMapper.updateEntity(dto, result);
                return result;
            })
            .orElseGet(() -> campaignListContactResultMapper.toEntity(dto));

        if (BooleanUtils.isTrue(res.getIsApproved())) {
            if (!res.getEmailIsSent() && res.getEmailTemplateJson() != null) {
                ToEmailTemplate toEmailTemplate = ObjectMapperUtils.readValue(objectMapper, res.getEmailTemplateJson(), ToEmailTemplate.class);

                CallHistory callHistory = new CallHistory()
                    .setType(callHistoryTypeRepository.getByIdent(CallHistoryType.CONTACT_CHANGE))
                    .setList(res.getClientList())
                    .setOrgRowNum(res.getOrgRowNum())
                    .setContactRowNum(res.getContactRowNum())
                    .setEmailTemplate(emailTemplateRepository.getOne(toEmailTemplate.getId()));
                callHistoryService.save(callHistory);

                sendEmail(toEmailTemplate);
                res.setEmailIsSent(true);
            }

            // if (!res.getSmsIsSent() && res.() != null) {
            //     // ToClientCampaignSms toClientCampaignSms = ObjectMapperUtils.readValue(objectMapper, res.getEmailTemplateJson(), Sms.class);
            //
            //     CallHistory callHistory = new CallHistory()
            //         .setType(callHistoryTypeRepository.getByIdent(CallHistoryType.CONTACT_CHANGE))
            //         .setList(res.getClientList())
            //         .setOrgRowNum(res.getOrgRowNum())
            //         .setContactRowNum(res.getContactRowNum())
            //         .setSmsText()
            //         .setSms();
            //     callHistoryService.save(callHistory);
            //
            //     // sendSms();
            //     res.setSmsIsSent(true);
            // }
        } else if (BooleanUtils.isFalse(res.getIsApproved())) {
            // if (res.getId() != null) {
            //     CampaignListContactResult oldResult = campaignListContactResultRepository.getOne(res.getId());
            //     if (!BooleanUtils.isFalse(oldResult.getIsApproved())) {
            //         // it's changed
            //         // If they Decline the Dialler receives an alert on Hamilton (via the Alert drop down) they can make changes and resubmit to QA/QC
            //         if (res.getUserClickedFinish() != null) {
            //             // do I need to send an email?
            //             // alert is already mentioned in com.fmg.cma.service.CampaignListContactResultService.getAlertsForCurrentUser
            //         }
            //     }
            // }
        }

        if (res.getId() == null || res.getUserClickedFinish() == null) {
            User currentUser = userRepository.getByLogin(SecurityUtils.getCurrentUserLogin());
            res.setUserClickedFinish(currentUser);
        }

        res = campaignListContactResultRepository.save(res);
        return campaignListContactResultMapper.toDto(res);
    }

    private void sendEmail(ToEmailTemplate t) {
        mailService.sendEmail(t.getTo(), t.getSubject(), t.getBody(), false, true, new SendMailOptions().setBcc(t.getBcc()));
    }

    /**
     * Get all the campaignListContactResults.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CampaignListContactResultDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CampaignListContactResults");
        return campaignListContactResultRepository.findAll(pageable)
            .map(campaignListContactResultMapper::toDto);
    }

    /**
     * Get one campaignListContactResult by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public CampaignListContactResultDTO findOne(Long id) {
        log.debug("Request to get CampaignListContactResult : {}", id);
        CampaignListContactResult campaignListContactResult = campaignListContactResultRepository.findOne(id);
        return campaignListContactResultMapper.toDto(campaignListContactResult);
    }

    /**
     * Delete the campaignListContactResult by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CampaignListContactResult : {}", id);
        campaignListContactResultRepository.delete(id);
    }

    @Scheduled(cron = "0 0 * * * ?") // every hour
    // @Scheduled(cron = "10 * * * * ?") // every 10 seconds
    public void convertMyListToActive() {
        List<CampaignListContactResult> results = campaignListContactResultRepository.findByStateAndAssignUserIdIsNotNull(DataListContactState.MY_LIST.getIdent());
        for (CampaignListContactResult result : results) {
            log.debug("Removing assignee for {}", result.getId());

            // TODO too many checks, what should I do without them?

            ZonedDateTime assignStamp = result.getAssignStamp();
            if (assignStamp == null) {
                continue;
            }

            Outcome clickedOutcome = result.getClickedOutcome();
            if (clickedOutcome == null) {
                continue;
            }

            Integer daysAfterCallBack = clickedOutcome.getDaysAfterCallBack();
            if (daysAfterCallBack == null) {
                continue;
            }

            if (assignStamp.plusDays(daysAfterCallBack).isAfter(ZonedDateTime.now())) {
                log.debug("Remove assignee for {}", result.getId());

                result.setState(DataListContactState.ACTIVE.getIdent());
                result.setAssignUser(null);
                result.setAssignStamp(null);
                campaignListContactResultRepository.save(result);
            }
        }
    }

    public List<AlertElementDto> getAlertsForCurrentUser() {
        User currentUser = userRepository.getByLogin(SecurityUtils.getCurrentUserLogin());
        // waiting for approve:
        String sql = "select clcr.client_campaign_id, " +
            "cc.name, " +
            "clcr.client_list_id, " +
            "cl.name, " +
            ClientListGridService.getConcatenateFunction(jpaProperties, "clcr.contact_row_num") + " contact_row_nums " +
            "from campaign_list_contact_result clcr " +
            "         join client_campaign cc on clcr.client_campaign_id = cc.ID " +
            "         join client_campaign_qa_users ccqu on clcr.client_campaign_id = ccqu.client_campaigns_id " +
            "         join client_list cl on clcr.client_list_id = cl.id " +
            "where clcr.state = '" + DataListContactState.PENDING.getIdent() + "' " +
            "  and ccqu.qa_users_id = :CURRENT_USER_ID " +
            "group by clcr.client_campaign_id, cc.name, clcr.client_list_id, cl.name, clcr.clicked_outcome_id ";
            // "union " +
            // // declined here:
            // "select clcr.client_campaign_id, " +
            // "cc.name, " +
            // "clcr.client_list_id, " +
            // "cl.name, " +
            // ClientListGridService.getConcatenateFunction(jpaProperties, "clcr.contact_row_num") + " contact_row_nums " +
            // "from campaign_list_contact_result clcr " +
            // "         join client_campaign cc on clcr.client_campaign_id = cc.ID " +
            // "         join client_list cl on clcr.client_list_id = cl.id " +
            // "where clcr.state = '" + DataListContactState.PENDING.getIdent() + "' " +
            // "  and clcr.user_clicked_finish_id = :CURRENT_USER_ID " +
            // "  and clcr.is_approved = 0 " +
            // "group by clcr.client_campaign_id, cc.name, clcr.client_list_id, cl.name, clcr.clicked_outcome_id";
        List<AlertElementDto> elements = new ArrayList<>();
        SqlRowSet rs = namedParameterJdbcTemplate.queryForRowSet(sql, ImmutableMap.of("CURRENT_USER_ID", currentUser.getId()));
        while (rs.next()) {
            long campaignId = rs.getLong(1);
            String campaignName = rs.getString(2);
            long listId = rs.getLong(3);
            String listName = rs.getString(4);
            Set<Long> contactRowNums = Stream.of(rs.getString(5).split(ClientListGridService.STRING_CONCATENATE_DELIMITER)).map(Long::parseLong).collect(Collectors.toSet());

            AlertElementDto alertElementDto = new AlertElementDto()
                .setCampaignId(campaignId)
                .setCampaignName(campaignName)
                .setListId(listId)
                .setListName(listName)
                .setContactRowNums(contactRowNums);
            elements.add(alertElementDto);
        }
        elements.forEach(alertElementDto -> alertElementDto.setDummy(alertElementDto));
        return elements;
    }
}
