package com.fmg.cma.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.fmg.cma.domain.CampaignPortalUserOutcome;
import com.fmg.cma.domain.*; // for static metamodels
import com.fmg.cma.repository.CampaignPortalUserOutcomeRepository;
import com.fmg.cma.service.dto.CampaignPortalUserOutcomeCriteria;

import com.fmg.cma.service.dto.CampaignPortalUserOutcomeDTO;
import com.fmg.cma.service.mapper.CampaignPortalUserOutcomeMapper;

/**
 * Service for executing complex queries for CampaignPortalUserOutcome entities in the database.
 * The main input is a {@link CampaignPortalUserOutcomeCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CampaignPortalUserOutcomeDTO} or a {@link Page} of {@link CampaignPortalUserOutcomeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CampaignPortalUserOutcomeQueryService extends QueryService<CampaignPortalUserOutcome> {

    private final Logger log = LoggerFactory.getLogger(CampaignPortalUserOutcomeQueryService.class);


    private final CampaignPortalUserOutcomeRepository campaignPortalUserOutcomeRepository;

    private final CampaignPortalUserOutcomeMapper campaignPortalUserOutcomeMapper;

    public CampaignPortalUserOutcomeQueryService(CampaignPortalUserOutcomeRepository campaignPortalUserOutcomeRepository, CampaignPortalUserOutcomeMapper campaignPortalUserOutcomeMapper) {
        this.campaignPortalUserOutcomeRepository = campaignPortalUserOutcomeRepository;
        this.campaignPortalUserOutcomeMapper = campaignPortalUserOutcomeMapper;
    }

    /**
     * Return a {@link List} of {@link CampaignPortalUserOutcomeDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CampaignPortalUserOutcomeDTO> findByCriteria(CampaignPortalUserOutcomeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<CampaignPortalUserOutcome> specification = createSpecification(criteria);
        return campaignPortalUserOutcomeMapper.toDto(campaignPortalUserOutcomeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link CampaignPortalUserOutcomeDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CampaignPortalUserOutcomeDTO> findByCriteria(CampaignPortalUserOutcomeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<CampaignPortalUserOutcome> specification = createSpecification(criteria);
        final Page<CampaignPortalUserOutcome> result = campaignPortalUserOutcomeRepository.findAll(specification, page);
        return result.map(campaignPortalUserOutcomeMapper::toDto);
    }

    /**
     * Function to convert CampaignPortalUserOutcomeCriteria to a {@link Specifications}
     */
    private Specifications<CampaignPortalUserOutcome> createSpecification(CampaignPortalUserOutcomeCriteria criteria) {
        Specifications<CampaignPortalUserOutcome> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), CampaignPortalUserOutcome_.id));
            }
            if (criteria.getCampaignPortalUserId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getCampaignPortalUserId(), CampaignPortalUserOutcome_.campaignPortalUser, CampaignPortalUser_.id));
            }
            if (criteria.getOutcomeId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getOutcomeId(), CampaignPortalUserOutcome_.outcome, Outcome_.id));
            }
        }
        return specification;
    }

}
