package com.fmg.cma.service;

import com.fmg.cma.domain.CampaignPortalUserOutcome;
import com.fmg.cma.repository.CampaignPortalUserOutcomeRepository;
import com.fmg.cma.service.dto.CampaignPortalUserOutcomeDTO;
import com.fmg.cma.service.mapper.CampaignPortalUserOutcomeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing CampaignPortalUserOutcome.
 */
@Service
@Transactional
public class CampaignPortalUserOutcomeService {

    private final Logger log = LoggerFactory.getLogger(CampaignPortalUserOutcomeService.class);

    private final CampaignPortalUserOutcomeRepository campaignPortalUserOutcomeRepository;

    private final CampaignPortalUserOutcomeMapper campaignPortalUserOutcomeMapper;

    public CampaignPortalUserOutcomeService(CampaignPortalUserOutcomeRepository campaignPortalUserOutcomeRepository, CampaignPortalUserOutcomeMapper campaignPortalUserOutcomeMapper) {
        this.campaignPortalUserOutcomeRepository = campaignPortalUserOutcomeRepository;
        this.campaignPortalUserOutcomeMapper = campaignPortalUserOutcomeMapper;
    }

    /**
     * Save a campaignPortalUserOutcome.
     *
     * @param campaignPortalUserOutcomeDTO the entity to save
     * @return the persisted entity
     */
    public CampaignPortalUserOutcomeDTO save(CampaignPortalUserOutcomeDTO campaignPortalUserOutcomeDTO) {
        log.debug("Request to save CampaignPortalUserOutcome : {}", campaignPortalUserOutcomeDTO);
        CampaignPortalUserOutcome campaignPortalUserOutcome = campaignPortalUserOutcomeMapper.toEntity(campaignPortalUserOutcomeDTO);
        campaignPortalUserOutcome = campaignPortalUserOutcomeRepository.save(campaignPortalUserOutcome);
        return campaignPortalUserOutcomeMapper.toDto(campaignPortalUserOutcome);
    }

    /**
     * Get all the campaignPortalUserOutcomes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CampaignPortalUserOutcomeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CampaignPortalUserOutcomes");
        return campaignPortalUserOutcomeRepository.findAll(pageable)
            .map(campaignPortalUserOutcomeMapper::toDto);
    }

    /**
     * Get one campaignPortalUserOutcome by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public CampaignPortalUserOutcomeDTO findOne(Long id) {
        log.debug("Request to get CampaignPortalUserOutcome : {}", id);
        CampaignPortalUserOutcome campaignPortalUserOutcome = campaignPortalUserOutcomeRepository.findOne(id);
        return campaignPortalUserOutcomeMapper.toDto(campaignPortalUserOutcome);
    }

    /**
     * Delete the campaignPortalUserOutcome by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CampaignPortalUserOutcome : {}", id);
        campaignPortalUserOutcomeRepository.delete(id);
    }
}
