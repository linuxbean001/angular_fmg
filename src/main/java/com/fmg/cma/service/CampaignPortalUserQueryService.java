package com.fmg.cma.service;


import com.fmg.cma.domain.CampaignPortalUser;
import com.fmg.cma.domain.CampaignPortalUser_;
import com.fmg.cma.domain.ClientCampaign_;
import com.fmg.cma.domain.User_;
import com.fmg.cma.repository.CampaignPortalUserRepository;
import com.fmg.cma.service.dto.CampaignPortalUserCriteria;
import com.fmg.cma.service.dto.CampaignPortalUserDTO;
import com.fmg.cma.service.mapper.CampaignPortalUserMapper;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for CampaignPortalUser entities in the database.
 * The main input is a {@link CampaignPortalUserCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CampaignPortalUserDTO} or a {@link Page} of {@link CampaignPortalUserDTO}
 * which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CampaignPortalUserQueryService extends QueryService<CampaignPortalUser> {

    private final Logger log = LoggerFactory.getLogger(CampaignPortalUserQueryService.class);


    private final CampaignPortalUserRepository campaignPortalUserRepository;

    private final CampaignPortalUserMapper campaignPortalUserMapper;

    public CampaignPortalUserQueryService(CampaignPortalUserRepository campaignPortalUserRepository,
                                          CampaignPortalUserMapper campaignPortalUserMapper) {
        this.campaignPortalUserRepository = campaignPortalUserRepository;
        this.campaignPortalUserMapper = campaignPortalUserMapper;
    }

    /**
     * Return a {@link List} of {@link CampaignPortalUserDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CampaignPortalUserDTO> findByCriteria(CampaignPortalUserCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<CampaignPortalUser> specification = createSpecification(criteria);
        return campaignPortalUserMapper.toDto(campaignPortalUserRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link CampaignPortalUserDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CampaignPortalUserDTO> findByCriteria(CampaignPortalUserCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<CampaignPortalUser> specification = createSpecification(criteria);
        final Page<CampaignPortalUser> result = campaignPortalUserRepository.findAll(specification, page);
        return result.map(campaignPortalUserMapper::toDto);
    }

    /**
     * Function to convert CampaignPortalUserCriteria to a {@link Specifications}
     */
    private Specifications<CampaignPortalUser> createSpecification(CampaignPortalUserCriteria criteria) {
        Specifications<CampaignPortalUser> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), CampaignPortalUser_.id));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), CampaignPortalUser_
                    .email));
            }
            if (criteria.getClientCampaignId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getClientCampaignId(),
                    CampaignPortalUser_.clientCampaign, ClientCampaign_.id));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getUserId(),
                    CampaignPortalUser_.user, User_.id));
            }
        }
        return specification;
    }

}
