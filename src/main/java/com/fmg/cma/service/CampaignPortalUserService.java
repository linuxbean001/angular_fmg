package com.fmg.cma.service;

import com.fmg.cma.domain.CampaignPortalUser;
import com.fmg.cma.domain.CampaignPortalUserOutcome;
import com.fmg.cma.repository.CampaignPortalUserOutcomeRepository;
import com.fmg.cma.repository.CampaignPortalUserRepository;
import com.fmg.cma.service.dto.CampaignPortalUserDTO;
import com.fmg.cma.service.dto.CampaignPortalUserSummaryDTO;
import com.fmg.cma.service.mapper.CampaignPortalUserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.apache.commons.lang3.BooleanUtils.isTrue;


/**
 * Service Implementation for managing CampaignPortalUser.
 */
@Service
@Transactional
public class CampaignPortalUserService {

    private final Logger log = LoggerFactory.getLogger(CampaignPortalUserService.class);

    private final CampaignPortalUserRepository campaignPortalUserRepository;

    private final CampaignPortalUserMapper campaignPortalUserMapper;

    private final CampaignPortalUserOutcomeRepository campaignPortalUserOutcomeRepository;

    public CampaignPortalUserService(CampaignPortalUserRepository campaignPortalUserRepository,
                                     CampaignPortalUserMapper campaignPortalUserMapper,
                                     CampaignPortalUserOutcomeRepository campaignPortalUserOutcomeService) {
        this.campaignPortalUserRepository = campaignPortalUserRepository;
        this.campaignPortalUserMapper = campaignPortalUserMapper;
        this.campaignPortalUserOutcomeRepository = campaignPortalUserOutcomeService;
    }

    /**
     * Save a campaignPortalUser.
     *
     * @param campaignPortalUserDTO the entity to save
     * @return the persisted entity
     */
    public CampaignPortalUserDTO save(CampaignPortalUserDTO campaignPortalUserDTO) {
        log.debug("Request to save CampaignPortalUser : {}", campaignPortalUserDTO);
        CampaignPortalUser campaignPortalUser = campaignPortalUserMapper.toEntity(campaignPortalUserDTO);
        campaignPortalUser = campaignPortalUserRepository.save(campaignPortalUser);
        return campaignPortalUserMapper.toDto(campaignPortalUser);
    }

    /**
     * Get all the campaignPortalUsers.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CampaignPortalUserDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CampaignPortalUsers");
        return campaignPortalUserRepository.findAll(pageable)
            .map(campaignPortalUserMapper::toDto);
    }

    /**
     * Get one campaignPortalUser by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public CampaignPortalUserDTO findOne(Long id) {
        log.debug("Request to get CampaignPortalUser : {}", id);
        CampaignPortalUser campaignPortalUser = campaignPortalUserRepository.findOne(id);
        return campaignPortalUserMapper.toDto(campaignPortalUser);
    }

    /**
     * Delete the campaignPortalUser by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CampaignPortalUser : {}", id);
        campaignPortalUserRepository.delete(id);
    }

    public CampaignPortalUserSummaryDTO getCampaignPortalUserSummary(Long id) {
        List<CampaignPortalUserOutcome> portalUserOutcomes = campaignPortalUserOutcomeRepository.findAllByCampaignPortalUserId(id);
        return new CampaignPortalUserSummaryDTO()
            .setEmailDaily(portalUserOutcomes.stream().anyMatch(x -> isTrue(x.getEmailDaily())))
            .setEmailImmediately(portalUserOutcomes.stream().anyMatch(x -> isTrue(x.getEmailImmediately())));
    }
}
