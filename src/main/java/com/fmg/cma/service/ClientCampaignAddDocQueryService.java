package com.fmg.cma.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.fmg.cma.domain.ClientCampaignAddDoc;
import com.fmg.cma.domain.*; // for static metamodels
import com.fmg.cma.repository.ClientCampaignAddDocRepository;
import com.fmg.cma.service.dto.ClientCampaignAddDocCriteria;

import com.fmg.cma.service.dto.ClientCampaignAddDocDTO;
import com.fmg.cma.service.mapper.ClientCampaignAddDocMapper;

/**
 * Service for executing complex queries for ClientCampaignAddDoc entities in the database.
 * The main input is a {@link ClientCampaignAddDocCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ClientCampaignAddDocDTO} or a {@link Page} of {@link ClientCampaignAddDocDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ClientCampaignAddDocQueryService extends QueryService<ClientCampaignAddDoc> {

    private final Logger log = LoggerFactory.getLogger(ClientCampaignAddDocQueryService.class);


    private final ClientCampaignAddDocRepository clientCampaignAddDocRepository;

    private final ClientCampaignAddDocMapper clientCampaignAddDocMapper;

    public ClientCampaignAddDocQueryService(ClientCampaignAddDocRepository clientCampaignAddDocRepository, ClientCampaignAddDocMapper clientCampaignAddDocMapper) {
        this.clientCampaignAddDocRepository = clientCampaignAddDocRepository;
        this.clientCampaignAddDocMapper = clientCampaignAddDocMapper;
    }

    /**
     * Return a {@link List} of {@link ClientCampaignAddDocDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ClientCampaignAddDocDTO> findByCriteria(ClientCampaignAddDocCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<ClientCampaignAddDoc> specification = createSpecification(criteria);
        return clientCampaignAddDocMapper.toDto(clientCampaignAddDocRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ClientCampaignAddDocDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ClientCampaignAddDocDTO> findByCriteria(ClientCampaignAddDocCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<ClientCampaignAddDoc> specification = createSpecification(criteria);
        final Page<ClientCampaignAddDoc> result = clientCampaignAddDocRepository.findAll(specification, page);
        return result.map(clientCampaignAddDocMapper::toDto);
    }

    /**
     * Function to convert ClientCampaignAddDocCriteria to a {@link Specifications}
     */
    private Specifications<ClientCampaignAddDoc> createSpecification(ClientCampaignAddDocCriteria criteria) {
        Specifications<ClientCampaignAddDoc> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ClientCampaignAddDoc_.id));
            }
            if (criteria.getLink() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLink(), ClientCampaignAddDoc_.link));
            }
            if (criteria.getText() != null) {
                specification = specification.and(buildStringSpecification(criteria.getText(), ClientCampaignAddDoc_.text));
            }
            if (criteria.getClientCampaignId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getClientCampaignId(), ClientCampaignAddDoc_.clientCampaign, ClientCampaign_.id));
            }
        }
        return specification;
    }

}
