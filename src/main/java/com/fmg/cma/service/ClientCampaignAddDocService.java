package com.fmg.cma.service;

import com.fmg.cma.domain.ClientCampaignAddDoc;
import com.fmg.cma.repository.ClientCampaignAddDocRepository;
import com.fmg.cma.service.dto.ClientCampaignAddDocDTO;
import com.fmg.cma.service.mapper.ClientCampaignAddDocMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing ClientCampaignAddDoc.
 */
@Service
@Transactional
public class ClientCampaignAddDocService {

    private final Logger log = LoggerFactory.getLogger(ClientCampaignAddDocService.class);

    private final ClientCampaignAddDocRepository clientCampaignAddDocRepository;

    private final ClientCampaignAddDocMapper clientCampaignAddDocMapper;

    public ClientCampaignAddDocService(ClientCampaignAddDocRepository clientCampaignAddDocRepository, ClientCampaignAddDocMapper clientCampaignAddDocMapper) {
        this.clientCampaignAddDocRepository = clientCampaignAddDocRepository;
        this.clientCampaignAddDocMapper = clientCampaignAddDocMapper;
    }

    /**
     * Save a clientCampaignAddDoc.
     *
     * @param clientCampaignAddDocDTO the entity to save
     * @return the persisted entity
     */
    public ClientCampaignAddDocDTO save(ClientCampaignAddDocDTO clientCampaignAddDocDTO) {
        log.debug("Request to save ClientCampaignAddDoc : {}", clientCampaignAddDocDTO);
        ClientCampaignAddDoc clientCampaignAddDoc = clientCampaignAddDocMapper.toEntity(clientCampaignAddDocDTO);
        clientCampaignAddDoc = clientCampaignAddDocRepository.save(clientCampaignAddDoc);
        return clientCampaignAddDocMapper.toDto(clientCampaignAddDoc);
    }

    /**
     * Get all the clientCampaignAddDocs.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ClientCampaignAddDocDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ClientCampaignAddDocs");
        return clientCampaignAddDocRepository.findAll(pageable)
            .map(clientCampaignAddDocMapper::toDto);
    }

    /**
     * Get one clientCampaignAddDoc by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ClientCampaignAddDocDTO findOne(Long id) {
        log.debug("Request to get ClientCampaignAddDoc : {}", id);
        ClientCampaignAddDoc clientCampaignAddDoc = clientCampaignAddDocRepository.findOne(id);
        return clientCampaignAddDocMapper.toDto(clientCampaignAddDoc);
    }

    /**
     * Delete the clientCampaignAddDoc by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ClientCampaignAddDoc : {}", id);
        clientCampaignAddDocRepository.delete(id);
    }
}
