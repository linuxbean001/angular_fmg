package com.fmg.cma.service;


import com.fmg.cma.domain.ClientCampaign;
import com.fmg.cma.domain.ClientCampaign_;
import com.fmg.cma.domain.ClientCompany_;
import com.fmg.cma.domain.ClientList_;
import com.fmg.cma.domain.EmailTemplate_;
import com.fmg.cma.domain.Outcome_;
import com.fmg.cma.domain.User_;
import com.fmg.cma.repository.ClientCampaignRepository;
import com.fmg.cma.service.dto.ClientCampaignCriteria;
import com.fmg.cma.service.dto.ClientCampaignDTO;
import com.fmg.cma.service.mapper.ClientCampaignMapper;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for ClientCampaign entities in the database.
 * The main input is a {@link ClientCampaignCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ClientCampaignDTO} or a {@link Page} of {@link ClientCampaignDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ClientCampaignQueryService extends QueryService<ClientCampaign> {

    private final Logger log = LoggerFactory.getLogger(ClientCampaignQueryService.class);


    private final ClientCampaignRepository clientCampaignRepository;

    private final ClientCampaignMapper clientCampaignMapper;

    public ClientCampaignQueryService(ClientCampaignRepository clientCampaignRepository, ClientCampaignMapper clientCampaignMapper) {
        this.clientCampaignRepository = clientCampaignRepository;
        this.clientCampaignMapper = clientCampaignMapper;
    }

    /**
     * Return a {@link List} of {@link ClientCampaignDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ClientCampaignDTO> findByCriteria(ClientCampaignCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<ClientCampaign> specification = createSpecification(criteria);
        return clientCampaignMapper.toDto(clientCampaignRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ClientCampaignDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ClientCampaignDTO> findByCriteria(ClientCampaignCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<ClientCampaign> specification = createSpecification(criteria);
        // do not sort by ManyToMany column - this will cause a duplicate entities
        final Page<ClientCampaign> result = clientCampaignRepository.findAll(specification, page);
        return result.map(clientCampaignMapper::toDto);
    }

    /**
     * Function to convert ClientCampaignCriteria to a {@link Specifications}
     */
    private Specifications<ClientCampaign> createSpecification(ClientCampaignCriteria criteria) {
        Specifications<ClientCampaign> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ClientCampaign_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), ClientCampaign_.name));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), ClientCampaign_.createdDate));
            }
            if (criteria.getCreatedById() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getCreatedById(), ClientCampaign_.createdBy, User_.id));
            }
            if (criteria.getClientCompanyId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getClientCompanyId(), ClientCampaign_.clientCompany, ClientCompany_.id));
            }
            if (criteria.getManagersId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getManagersId(), ClientCampaign_.managers, User_.id));
            }
            if (criteria.getEmailTemplateId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getEmailTemplateId(), ClientCampaign_.emailTemplates, EmailTemplate_.id));
            }
            if (criteria.getClientListId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getClientListId(), ClientCampaign_.clientLists, ClientList_.id));
            }
            if (criteria.getOutcomesId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getOutcomesId(), ClientCampaign_.outcomes, Outcome_.id));
            }
        }
        return specification;
    }

}
