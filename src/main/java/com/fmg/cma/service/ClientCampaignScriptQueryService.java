package com.fmg.cma.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.fmg.cma.domain.ClientCampaignScript;
import com.fmg.cma.domain.*; // for static metamodels
import com.fmg.cma.repository.ClientCampaignScriptRepository;
import com.fmg.cma.service.dto.ClientCampaignScriptCriteria;

import com.fmg.cma.service.dto.ClientCampaignScriptDTO;
import com.fmg.cma.service.mapper.ClientCampaignScriptMapper;

/**
 * Service for executing complex queries for ClientCampaignScript entities in the database.
 * The main input is a {@link ClientCampaignScriptCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ClientCampaignScriptDTO} or a {@link Page} of {@link ClientCampaignScriptDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ClientCampaignScriptQueryService extends QueryService<ClientCampaignScript> {

    private final Logger log = LoggerFactory.getLogger(ClientCampaignScriptQueryService.class);


    private final ClientCampaignScriptRepository clientCampaignScriptRepository;

    private final ClientCampaignScriptMapper clientCampaignScriptMapper;

    public ClientCampaignScriptQueryService(ClientCampaignScriptRepository clientCampaignScriptRepository, ClientCampaignScriptMapper clientCampaignScriptMapper) {
        this.clientCampaignScriptRepository = clientCampaignScriptRepository;
        this.clientCampaignScriptMapper = clientCampaignScriptMapper;
    }

    /**
     * Return a {@link List} of {@link ClientCampaignScriptDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ClientCampaignScriptDTO> findByCriteria(ClientCampaignScriptCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<ClientCampaignScript> specification = createSpecification(criteria);
        return clientCampaignScriptMapper.toDto(clientCampaignScriptRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ClientCampaignScriptDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ClientCampaignScriptDTO> findByCriteria(ClientCampaignScriptCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<ClientCampaignScript> specification = createSpecification(criteria);
        final Page<ClientCampaignScript> result = clientCampaignScriptRepository.findAll(specification, page);
        return result.map(clientCampaignScriptMapper::toDto);
    }

    /**
     * Function to convert ClientCampaignScriptCriteria to a {@link Specifications}
     */
    private Specifications<ClientCampaignScript> createSpecification(ClientCampaignScriptCriteria criteria) {
        Specifications<ClientCampaignScript> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ClientCampaignScript_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), ClientCampaignScript_.name));
            }
            if (criteria.getExcerpt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getExcerpt(), ClientCampaignScript_.excerpt));
            }
        }
        return specification;
    }

}
