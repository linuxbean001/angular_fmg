package com.fmg.cma.service;

import com.fmg.cma.domain.ClientCampaignScript;
import com.fmg.cma.repository.ClientCampaignScriptRepository;
import com.fmg.cma.service.dto.ClientCampaignScriptDTO;
import com.fmg.cma.service.mapper.ClientCampaignScriptMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing ClientCampaignScript.
 */
@Service
@Transactional
public class ClientCampaignScriptService {

    private final Logger log = LoggerFactory.getLogger(ClientCampaignScriptService.class);

    private final ClientCampaignScriptRepository clientCampaignScriptRepository;

    private final ClientCampaignScriptMapper clientCampaignScriptMapper;

    public ClientCampaignScriptService(ClientCampaignScriptRepository clientCampaignScriptRepository, ClientCampaignScriptMapper clientCampaignScriptMapper) {
        this.clientCampaignScriptRepository = clientCampaignScriptRepository;
        this.clientCampaignScriptMapper = clientCampaignScriptMapper;
    }

    /**
     * Save a clientCampaignScript.
     *
     * @param clientCampaignScriptDTO the entity to save
     * @return the persisted entity
     */
    public ClientCampaignScriptDTO save(ClientCampaignScriptDTO clientCampaignScriptDTO) {
        log.debug("Request to save ClientCampaignScript : {}", clientCampaignScriptDTO);
        ClientCampaignScript clientCampaignScript = clientCampaignScriptMapper.toEntity(clientCampaignScriptDTO);
        clientCampaignScript = clientCampaignScriptRepository.save(clientCampaignScript);
        return clientCampaignScriptMapper.toDto(clientCampaignScript);
    }

    /**
     * Get all the clientCampaignScripts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ClientCampaignScriptDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ClientCampaignScripts");
        return clientCampaignScriptRepository.findAll(pageable)
            .map(clientCampaignScriptMapper::toDto);
    }

    /**
     * Get one clientCampaignScript by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ClientCampaignScriptDTO findOne(Long id) {
        log.debug("Request to get ClientCampaignScript : {}", id);
        ClientCampaignScript clientCampaignScript = clientCampaignScriptRepository.findOne(id);
        return clientCampaignScriptMapper.toDto(clientCampaignScript);
    }

    /**
     * Delete the clientCampaignScript by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ClientCampaignScript : {}", id);
        clientCampaignScriptRepository.delete(id);
    }
}
