package com.fmg.cma.service;

import com.fmg.cma.domain.AppointmentAttendee;
import com.fmg.cma.domain.Authority;
import com.fmg.cma.domain.BookingQuestion;
import com.fmg.cma.domain.CampaignPortalUser;
import com.fmg.cma.domain.ClientCampaign;
import com.fmg.cma.domain.ClientCampaignScript;
import com.fmg.cma.domain.ClientCampaignSms;
import com.fmg.cma.domain.ClientList;
import com.fmg.cma.domain.EmailTemplate;
import com.fmg.cma.domain.GraphReport;
import com.fmg.cma.domain.Outcome;
import com.fmg.cma.domain.Report;
import com.fmg.cma.domain.Serie;
import com.fmg.cma.domain.SurveyQuestion;
import com.fmg.cma.domain.User;
import com.fmg.cma.domain.UserCompany;
import com.fmg.cma.repository.AppointmentAttendeeRepository;
import com.fmg.cma.repository.BookingQuestionRepository;
import com.fmg.cma.repository.CampaignPortalUserOutcomeRepository;
import com.fmg.cma.repository.CampaignPortalUserRepository;
import com.fmg.cma.repository.ClientCampaignRepository;
import com.fmg.cma.repository.ClientCampaignScriptRepository;
import com.fmg.cma.repository.ClientCampaignSmsRepository;
import com.fmg.cma.repository.ClientFieldRepository;
import com.fmg.cma.repository.ClientListDataRepository;
import com.fmg.cma.repository.ClientListRepository;
import com.fmg.cma.repository.EmailTemplateRepository;
import com.fmg.cma.repository.FieldRepository;
import com.fmg.cma.repository.OutcomeRepository;
import com.fmg.cma.repository.SurveyQuestionRepository;
import com.fmg.cma.repository.UserRepository;
import com.fmg.cma.security.AuthoritiesConstants;
import com.fmg.cma.security.SecurityUtils;
import com.fmg.cma.service.dto.CampaignPortalUserDTO;
import com.fmg.cma.service.dto.ClientCampaignDTO;
import com.fmg.cma.service.dto.ClientListDTO;
import com.fmg.cma.service.dto.EmailTemplateDTO;
import com.fmg.cma.service.dto.UserCompanyDTO;
import com.fmg.cma.service.dto.UserDTO;
import com.fmg.cma.service.mapper.ClientCampaignMapper;
import com.fmg.cma.web.rest.ClientCampaignResource;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.errors.EmailNotFoundException;
import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Optional.of;


/**
 * Service Implementation for managing ClientCampaign.
 */
@Service
@Transactional
public class ClientCampaignService {

    private final Logger log = LoggerFactory.getLogger(ClientCampaignService.class);

    private final String DEFAULT_NAME = "Campaign - Name TBD";

    private final ClientCampaignRepository clientCampaignRepository;

    private final ClientCampaignMapper clientCampaignMapper;

    private final UserRepository userRepository;

    private final ClientListRepository clientListRepository;

    private final FieldRepository fieldRepository;

    private final ClientFieldRepository clientFieldRepository;

    private final ClientListDataRepository clientListDataRepository;

    private final CampaignPortalUserService campaignPortalUserService;

    private final CampaignPortalUserRepository campaignPortalUserRepository;

    private final UserService userService;

    private final OutcomeRepository outcomeRepository;

    private final CampaignPortalUserOutcomeRepository campaignPortalUserOutcomeRepository;

    private final MailService mailService;

    private final EmailTemplateService emailTemplateService;

    private final EmailTemplateRepository emailTemplateRepository;

    private final ClientCampaignScriptRepository clientCampaignScriptRepository;

    private final ClientCampaignSmsRepository clientCampaignSmsRepository;

    private final AppointmentAttendeeRepository appointmentAttendeeRepository;

    private final SurveyQuestionRepository surveyQuestionRepository;

    private final BookingQuestionRepository bookingQuestionRepository;

    public ClientCampaignService(ClientCampaignRepository clientCampaignRepository,
                                 ClientCampaignMapper clientCampaignMapper,
                                 UserRepository userRepository,
                                 ClientListRepository clientListRepository,
                                 FieldRepository fieldRepository,
                                 ClientFieldRepository clientFieldRepository,
                                 ClientListDataRepository clientListDataRepository,
                                 CampaignPortalUserService campaignPortalUserService,
                                 CampaignPortalUserRepository campaignPortalUserRepository,
                                 UserService userService,
                                 OutcomeRepository outcomeRepository,
                                 CampaignPortalUserOutcomeRepository campaignPortalUserOutcomeRepository,
                                 MailService mailService,
                                 EmailTemplateService emailTemplateService,
                                 EmailTemplateRepository emailTemplateRepository,
                                 ClientCampaignScriptRepository clientCampaignScriptRepository,
                                 ClientCampaignSmsRepository clientCampaignSmsRepository,
                                 AppointmentAttendeeRepository appointmentAttendeeRepository,
                                 SurveyQuestionRepository surveyQuestionRepository,
                                 BookingQuestionRepository bookingQuestionRepository) {
        this.clientCampaignRepository = clientCampaignRepository;
        this.clientCampaignMapper = clientCampaignMapper;
        this.userRepository = userRepository;
        this.clientListRepository = clientListRepository;
        this.fieldRepository = fieldRepository;
        this.clientFieldRepository = clientFieldRepository;
        this.clientListDataRepository = clientListDataRepository;
        this.campaignPortalUserService = campaignPortalUserService;
        this.campaignPortalUserRepository = campaignPortalUserRepository;
        this.userService = userService;
        this.outcomeRepository = outcomeRepository;
        this.campaignPortalUserOutcomeRepository = campaignPortalUserOutcomeRepository;
        this.mailService = mailService;
        this.emailTemplateService = emailTemplateService;
        this.emailTemplateRepository = emailTemplateRepository;
        this.clientCampaignScriptRepository = clientCampaignScriptRepository;
        this.clientCampaignSmsRepository = clientCampaignSmsRepository;
        this.appointmentAttendeeRepository = appointmentAttendeeRepository;
        this.surveyQuestionRepository = surveyQuestionRepository;
        this.bookingQuestionRepository = bookingQuestionRepository;
    }

    private void fillNewlyCreatedAttributes(ClientCampaign clientCampaign) {
        clientCampaign.setCreatedDate(ZonedDateTime.now());

        User currentUser = userRepository.getByLogin(SecurityUtils.getCurrentUserLogin());
        clientCampaign.setCreatedBy(currentUser);

        // this is workaround to allow auto-save
        if (clientCampaign.getName() == null) {
            clientCampaign.setName(DEFAULT_NAME);
        }
    }

    /**
     * Save a clientCampaign.
     *
     * @param clientCampaignDTO the entity to save
     * @return the persisted entity
     */
    public ClientCampaignDTO save(ClientCampaignDTO clientCampaignDTO) {
        log.debug("Request to save ClientCampaign : {}", clientCampaignDTO);

        ClientCampaign clientCampaign;
        if (clientCampaignDTO.getId() == null) {
            clientCampaign = clientCampaignMapper.toEntity(clientCampaignDTO);
            fillNewlyCreatedAttributes(clientCampaign);

            assertNameIsUnique(clientCampaign.getName(), null);
        } else {
            clientCampaign = clientCampaignRepository.getOne(clientCampaignDTO.getId());
            clientCampaignMapper.update(clientCampaignDTO, clientCampaign);

            // mapStruct does not remove elements from updated one to many relations so we need to manually remove portal users if they have been removed
            ensureCampaignIsUpdatedWithAnyRemovedPortalUsers(clientCampaignDTO, clientCampaign);

            assertNameIsUnique(clientCampaign.getName(), clientCampaign.getId());
        }
        clientCampaign = clientCampaignRepository.save(clientCampaign);
        return clientCampaignMapper.toDto(clientCampaign);
    }


    private void ensureCampaignIsUpdatedWithAnyRemovedPortalUsers(ClientCampaignDTO clientCampaignDTO, ClientCampaign aClientCampaign) {
        List<CampaignPortalUserDTO> updatedPortalUsers = clientCampaignDTO.getPortalUsers();
        updatedPortalUsers = updatedPortalUsers == null ? new ArrayList<>() : updatedPortalUsers;
        Set<CampaignPortalUser> existingPortalUsers = aClientCampaign.getPortalUsers();
        if (existingPortalUsers != null) {
            for (Iterator<CampaignPortalUser> it = existingPortalUsers.iterator(); it.hasNext();) {
                CampaignPortalUser element = it.next();
                boolean found = false;
                for (CampaignPortalUserDTO updatedPortalUser : updatedPortalUsers) {
                    if (updatedPortalUser.getId().equals(element.getId())) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    log.info("Not found existing user {} so removing it from campaign", element.getEmail());
                    element.setClientCampaign(null);
                    it.remove();
                }
            }
        }
    }

    private void assertNameIsUnique(String name, Long id) {
        if (!nameIsUnique(name, id)) {
            String message = "Campaign with name \"" + name + "\" already exists";
            throw new BadRequestAlertException(message, ClientCampaignResource.ENTITY_NAME, "clientCampaign.name.exists");
        }
    }

    public boolean nameIsUnique(String name, Long id) {
        if (DEFAULT_NAME.equals(name)) {
            return true;
        }

        List<ClientCampaign> campaigns;
        if (id == null) {
            campaigns = clientCampaignRepository.findAllByName(name);
        } else {
            campaigns = clientCampaignRepository.findAllByNameAndIdNot(name, id);
        }

        return campaigns.isEmpty();
    }

    /**
     * Save a clientCampaign.
     *
     * @param clientCampaignDTO the entity to save
     * @return the persisted entity
     */
    public ClientCampaignDTO saveByWizard(ClientCampaignDTO clientCampaignDTO) {
        Set<EmailTemplateDTO> emailTemplates = clientCampaignDTO.getEmailTemplates();
        if (emailTemplates != null) {
            Set<EmailTemplateDTO> saveDmailTemplateDTOs = new HashSet<>();
            for (EmailTemplateDTO emailTemplateDTO : emailTemplates) {
                // this is a bad code, it will cause to duplicate email at each campaign's save
                // // make a copy, just in case (client already do it)
                // emailTemplateDTO.setId(null);

                EmailTemplateDTO savedEmailTemplateDTO = emailTemplateService.save(emailTemplateDTO);
                saveDmailTemplateDTOs.add(savedEmailTemplateDTO);
            }
            clientCampaignDTO.setEmailTemplates(saveDmailTemplateDTOs);
        }

        // save and replace variable
        ClientCampaignDTO newClientCampaignDTO = save(clientCampaignDTO);

        // this is not an error that I use clientCampaignDTO, not newClientCampaignDTO
        List<CampaignPortalUserDTO> portalUsers = clientCampaignDTO.getPortalUsers();
        if (portalUsers != null) {
            for (CampaignPortalUserDTO portalUser : portalUsers) {
                String email = portalUser.getEmail();
                Optional<User> userOptional = userRepository.findOneByEmailIgnoreCase(email);
                Authority onlyPortalUserRole = new Authority(AuthoritiesConstants.PORTAL_USER);
                User user;
                if (userOptional.isPresent()) {
                    user = userOptional.get();
                    boolean havePortalRole = user.getAuthorities().contains(onlyPortalUserRole);
                    if (!havePortalRole) {
                        // add the portal role to that user.
                        user.getAuthorities().add(onlyPortalUserRole);
                        userRepository.save(user);
                    }

                    // Ensuring that the portal user (user with portal role) is assigned to that campaign has already
                    // been done on the add portal user part

                    if (BooleanUtils.isNotTrue(portalUser.getSentNotification())) {
                        SendMailOptions options = new SendMailOptions()
                            .setBcc(user.getEmail());
                        mailService.sendExistingPortalUserAssignedToCampaign(user, clientCampaignDTO.getName(), options);
                        portalUser.setSentNotification(Boolean.TRUE);
                    }
                } else {
                    // create a new user
                    User currentUser = userRepository.getByLogin(SecurityUtils.getCurrentUserLogin());
                    UserCompany company = currentUser.getCompany();
                    Long companyId = company.getId();

                    UserDTO userDTO = new UserDTO()
                        .setEmail(email)
                        .setLogin(email)
                        .setCompany(new UserCompanyDTO(companyId));

                    userDTO.setAuthorities(Collections.singleton(onlyPortalUserRole.getName()));
                    user = userService.createUser(userDTO);

                    SendMailOptions options = new SendMailOptions()
                        .setBcc(currentUser.getEmail());

                    mailService.sendNewPortalAccountCreatedMail(
                        userService.requestPasswordReset(email)
                            .orElseThrow(EmailNotFoundException::new), clientCampaignDTO.getName(),
                        options
                    );
                    portalUser.setSentNotification(Boolean.TRUE);
                }

//                // todo no need to do this as it has already been done when portal user added
                portalUser.setClientCampaignId(newClientCampaignDTO.getId())
                    .setUserId(user.getId());

                CampaignPortalUserDTO newPortalUserDTO = campaignPortalUserService.save(portalUser);

                // List<OutcomeDTO> outcomeDTOS = portalUser.getOutcomes();
                // if (outcomeDTOS != null) {
                //     //TODO save outcomes
                //     for (OutcomeDTO outcomeDTO : outcomeDTOS) {
                //         CampaignPortalUserOutcome cpuo = new CampaignPortalUserOutcome();
                //         cpuo.outcome(outcomeRepository.getOne(outcomeDTO.getId()))
                //             .campaignPortalUser(campaignPortalUserRepository.getOne(newPortalUserDTO.getId()));
                //         campaignPortalUserOutcomeRepository.save(cpuo);
                //     }
                // }
            }
        }

        return newClientCampaignDTO;
    }

    /**
     * Get all the clientCampaigns.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ClientCampaignDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ClientCampaigns");
        return clientCampaignRepository.findAll(pageable)
            .map(clientCampaignMapper::toDto);
    }

    /**
     * Get one clientCampaign by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ClientCampaignDTO findOne(Long id) {
        log.debug("Request to get ClientCampaign : {}", id);
        ClientCampaign clientCampaign = clientCampaignRepository.findOneWithEagerRelationships(id);

//         log.debug("CAJUN GIRLS!! {}", clientCampaign);
//        Set<ClientList> clientLists = clientCampaign.getClientLists();
//        for (ClientList clientList : clientLists) {
//            log.debug("LIST!! {}", clientList);
//            clientList.setName("BOB" + clientList.getName());
//            Set<CampaignPortalUser> portalUsersTest = new HashSet<>();
//            CampaignPortalUser portalUser = new CampaignPortalUser();
//            portalUser.setEmail("BOB" + clientList.getName());
//            portalUsersTest.add(portalUser);
//            clientList.setPortalUsers(portalUsersTest);
//        }

//        clientLists = clientCampaign.getClientLists();
//        for (ClientList clientList : clientLists) {
//            log.debug("LIST!! NOW {}", clientList);
//            Set<CampaignPortalUser> portalUsers = clientList.getPortalUsers();
//            for (CampaignPortalUser portalUser : portalUsers) {
//                log.info("NOW portalUser.getEmail() :{}: ", portalUser.getEmail());
//            }
//        }

//
//        Set<CampaignPortalUser> portalUsers = clientList.getPortalUsers();
//        log.info("portalUsers :{}: ", portalUsers);
//        Set<CampaignPortalUser> portalUsersTest = new HashSet<>();
//        CampaignPortalUser portalUser = new CampaignPortalUser();
//        portalUser.setEmail("TESTTESTTEST@gmail.com");
//        portalUsersTest.add(portalUser);
//        clientList.setPortalUsers(portalUsersTest);
//        log.debug("updated List");
//        clientLists.add(clientList);
//


//        clientCampaign.setClientLists(clientLists);
//
//        clientLists = clientCampaign.getClientLists();
//        for (ClientList clientList : clientLists) {
//            log.debug("LIST IS NOW!! {}", clientList);
//            Set<CampaignPortalUser> portalUsers = clientList.getPortalUsers();
//            log.info("portalUsers IS NOW :{}: ", portalUsers);
//        }


//        log.debug("Request to get ClientCampaign : {}", id);
//
//        Set<CampaignPortalUser> portalUsersTest = new HashSet<>();
//        CampaignPortalUser portalUser = new CampaignPortalUser();
//        portalUser.setEmail("TESTTESTTEST@gmail.com");
//        portalUsersTest.add(portalUser);
//        Set<ClientList> clientLists2 = clientCampaign.getClientLists();
//        for (ClientList clientList : clientLists2) {
//            clientList.setPortalUsers(portalUsersTest);
//        }
//        clientCampaign.setClientLists(clientLists2);
//        //  .ssetPortalUsers(portalUsersTest);
//
       // ClientCampaignDTO clientCampaignDTO = clientCampaignMapper.toDto(clientCampaign);
     //   log.debug("PUERTO DTO : {}", clientCampaignDTO);
     //   List<ClientListDTO> clientLists1 = clientCampaignDTO.getClientLists();

       // for (ClientListDTO clientListDTO : clientLists1) {
       //     log.debug("PUERTO client list {} portal users {}", clientListDTO, clientListDTO.getPortalUsers());
     //   }


        return clientCampaignMapper.toDto(clientCampaign);
    }

    /**
     * Delete the clientCampaign by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ClientCampaign : {}", id);
        clientCampaignRepository.delete(id);
    }

    /**
     * Get reports clientCampaign by id.
     *
     * @param id the id of the entity
     * @return the report entity
     */
    @Transactional(readOnly = true)
    public Report getReport(Long id, Long idList) {

        Report response = new Report();
        log.debug("Request to get ClientCampaign : {}", id);
        ClientCampaignDTO clientCampaign = clientCampaignMapper.toDto(clientCampaignRepository.findOneWithEagerRelationships(id));
        ClientListDTO clientList = clientCampaign.getClientLists().stream().filter(clientListDTO -> clientListDTO.getId().equals(idList)).findFirst().orElseThrow(() -> new RuntimeException("Not Found Client ID"));

        response.setAverageSale(dummyData(Optional.ofNullable(clientCampaign.getAverageSaleValue()).orElse(BigDecimal.ZERO), clientList));
        response.setCallPerHour(dummyData(BigDecimal.valueOf(Optional.ofNullable(clientCampaign.getCallPerHour()).orElse(0)), clientList));
        response.setContactRate(dummyData(BigDecimal.valueOf(Optional.ofNullable(clientCampaign.getContactRate()).orElse(0.0)), clientList));
        response.setConversationPerHour(dummyData(BigDecimal.valueOf(Optional.ofNullable(clientCampaign.getConversationsPerHour()).orElse(0)), clientList));
        response.setHoursPerLead(dummyData(BigDecimal.valueOf(Optional.ofNullable(clientCampaign.getHoursPerLead()).orElse(0)), clientList));
        response.setLeadToSales(dummyData(BigDecimal.valueOf(Optional.ofNullable(clientCampaign.getLeadToSaleConversion()).orElse(0.0)), clientList));
        response.setConversionRate(dummyData(BigDecimal.valueOf(Optional.ofNullable(clientCampaign.getConversionRate()).orElse(0.0)), clientList));

        return response;
    }

    @Transactional(readOnly = true)
    public Report getReport(Long id) {

        Report response = new Report();
        log.debug("Request to get ClientCampaign : {}", id);
        ClientCampaignDTO clientCampaign = clientCampaignMapper.toDto(clientCampaignRepository.findOneWithEagerRelationships(id));

        response.setAverageSale(dummyData(Optional.ofNullable(clientCampaign.getAverageSaleValue()).orElse(BigDecimal.ZERO), clientCampaign.getClientLists()));
        response.setCallPerHour(dummyData(BigDecimal.valueOf(Optional.ofNullable(clientCampaign.getCallPerHour()).orElse(0)), clientCampaign.getClientLists()));
        response.setContactRate(dummyData(BigDecimal.valueOf(Optional.ofNullable(clientCampaign.getContactRate()).orElse(0.0)), clientCampaign.getClientLists()));
        response.setConversationPerHour(dummyData(BigDecimal.valueOf(Optional.ofNullable(clientCampaign.getConversationsPerHour()).orElse(0)), clientCampaign.getClientLists()));
        response.setHoursPerLead(dummyData(BigDecimal.valueOf(Optional.ofNullable(clientCampaign.getHoursPerLead()).orElse(0)), clientCampaign.getClientLists()));
        response.setLeadToSales(dummyData(BigDecimal.valueOf(Optional.ofNullable(clientCampaign.getLeadToSaleConversion()).orElse(0.0)), clientCampaign.getClientLists()));
        response.setConversionRate(dummyData(BigDecimal.valueOf(Optional.ofNullable(clientCampaign.getConversionRate()).orElse(0.0)), clientCampaign.getClientLists()));

        return response;
    }

    public List<GraphReport> dummyData(BigDecimal limit, ClientListDTO clientList) {
        List<GraphReport> response = new ArrayList<>();

        GraphReport dummyActual = new GraphReport();
        dummyActual.setName(clientList.getName());
        dummyActual.setSeries(generateSerie());

        GraphReport dummyTarget = new GraphReport();
        dummyTarget.setName("Target");
        dummyTarget.setSeries(generateSerie(limit));

        response.add(dummyTarget);
        response.add(dummyActual);

        return response;
    }

    public List<GraphReport> dummyData(BigDecimal limit, List<ClientListDTO> clientListLists) {
        List<GraphReport> response = new ArrayList<>();

        clientListLists.forEach(clientListDTO -> {
            GraphReport dummyActual = new GraphReport();
            dummyActual.setName(clientListDTO.getName());
            dummyActual.setSeries(generateSerie());
            response.add(dummyActual);
        });


        GraphReport dummyTarget = new GraphReport();
        dummyTarget.setName("Target");
        dummyTarget.setSeries(generateSerie(limit));

        response.add(dummyTarget);

        return response;
    }

    public List<Serie> generateSerie(BigDecimal limit) {
        List<Serie> dummy = new ArrayList<>();
        dummy.add(new Serie(0, limit));
        for (int i = 0; i < 10; i++) {
            dummy.add(new Serie(new Random().nextInt(101), limit));
        }
        dummy.add(new Serie(100, limit));
        return dummy;
    }

    public List<Serie> generateSerie() {
        List<Serie> dummy = new ArrayList<>();
        int min = 0;
        for (int i = 0; i < 10; i++) {
            BigDecimal limit = BigDecimal.valueOf(i * 10 + 1);
            dummy.add(new Serie(limit(new Random().nextInt(limit.intValue()) + min), BigDecimal.valueOf(limit(new Random().nextInt(limit.intValue()) + min))));
            min = limit.intValue();
        }
        return dummy;
    }

    public int limit(int numero) {
        return Math.min(numero, 101);
    }

    public void addClientList(long id, long clientListId) {
        ClientCampaign clientCampaign = clientCampaignRepository.getOne(id);
        ClientList clientList = clientListRepository.getOne(clientListId);
        // clientCampaign.getClientLists().add(clientList);
        // clientCampaignRepository.save(clientCampaign);
        clientList.setClientCampaign(clientCampaign);
        clientListRepository.save(clientList);
    }

    public void addEmailTemplate(long id, long emailTemplateId) {
        ClientCampaign clientCampaign = clientCampaignRepository.getOne(id);
        EmailTemplate emailTemplate = emailTemplateRepository.getOne(emailTemplateId);

        emailTemplate.setClientCampaign(clientCampaign);
        emailTemplateRepository.save(emailTemplate);
    }

    public void addOutcome(long id, long outcomeId) {
        ClientCampaign clientCampaign = clientCampaignRepository.getOne(id);
        Outcome outcome = outcomeRepository.getOne(outcomeId);

        outcome.setClientCampaign(clientCampaign);
        outcomeRepository.save(outcome);
    }

    public void addScript(long id, long scriptId) {
        ClientCampaign clientCampaign = clientCampaignRepository.getOne(id);
        ClientCampaignScript clientCampaignScript = clientCampaignScriptRepository.getOne(scriptId);

        clientCampaignScript.setClientCampaign(clientCampaign);
        clientCampaignScriptRepository.save(clientCampaignScript);
    }

    public void addSms(long id, long smsId) {
        ClientCampaign clientCampaign = clientCampaignRepository.getOne(id);
        ClientCampaignSms clientCampaignSms = clientCampaignSmsRepository.getOne(smsId);

        clientCampaignSms.setClientCampaign(clientCampaign);
        clientCampaignSmsRepository.save(clientCampaignSms);
    }

    public void addPortalUser(long id, long portalUserId) {
        ClientCampaign clientCampaign = clientCampaignRepository.getOne(id);
        CampaignPortalUser campaignPortalUser = campaignPortalUserRepository.getOne(portalUserId);

        //        clientCampaign.getPortalUsers().add(campaignPortalUser);1
        //        clientCampaignRepository.save(clientCampaign);

        campaignPortalUser.setClientCampaign(clientCampaign);
        campaignPortalUserRepository.save(campaignPortalUser);
    }

    public void addAppointmentAttendee(long id, long appointmentAttendeeId) {
        ClientCampaign clientCampaign = clientCampaignRepository.getOne(id);
        AppointmentAttendee appointmentAttendee = appointmentAttendeeRepository.getOne(appointmentAttendeeId);

        appointmentAttendee.setClientCampaign(clientCampaign);
        appointmentAttendeeRepository.save(appointmentAttendee);
    }

    public void addSurveyQuestion(long id, long surveyQuestionId) {
        ClientCampaign clientCampaign = clientCampaignRepository.getOne(id);
        SurveyQuestion surveyQuestion = surveyQuestionRepository.getOne(surveyQuestionId);

        surveyQuestion.setClientCampaign(clientCampaign);
        surveyQuestionRepository.save(surveyQuestion);
    }

    public void addBookingQuestion(long id, long bookingQuestionId) {
        ClientCampaign clientCampaign = clientCampaignRepository.getOne(id);
        BookingQuestion bookingQuestion = bookingQuestionRepository.getOne(bookingQuestionId);

        bookingQuestion.setClientCampaign(clientCampaign);
        bookingQuestionRepository.save(bookingQuestion);
    }

    public List<ClientCampaignDTO> getConsoleRelatedClientCampaigns() {
        LinkedHashSet<ClientCampaign> clientCampaigns = new LinkedHashSet<>();

        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            // admin role can see all campaigns and lists
            clientCampaigns.addAll(clientCampaignRepository.findAll());
        } else {
            User currentUser = userRepository.getByLogin(SecurityUtils.getCurrentUserLogin());

            clientCampaigns.addAll(clientCampaignRepository.findByUsers_Id(currentUser.getId()));
            clientCampaigns.addAll(clientCampaignRepository.findByManagers_Id(currentUser.getId()));
            clientCampaigns.addAll(clientCampaignRepository.findByQaUsers_Id(currentUser.getId()));
        }

        return clientCampaignMapper.toDto(new ArrayList<>(clientCampaigns));
    }
}
