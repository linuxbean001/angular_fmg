package com.fmg.cma.service;


import com.fmg.cma.domain.ClientCampaignSms;
import com.fmg.cma.domain.ClientCampaignSms_;
import com.fmg.cma.repository.ClientCampaignSmsRepository;
import com.fmg.cma.service.dto.ClientCampaignSmsCriteria;
import com.fmg.cma.service.dto.ClientCampaignSmsDTO;
import com.fmg.cma.service.mapper.ClientCampaignSmsMapper;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for ClientCampaignSms entities in the database.
 * The main input is a {@link ClientCampaignSmsCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ClientCampaignSmsDTO} or a {@link Page} of {@link ClientCampaignSmsDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ClientCampaignSmsQueryService extends QueryService<ClientCampaignSms> {

    private final Logger log = LoggerFactory.getLogger(ClientCampaignSmsQueryService.class);


    private final ClientCampaignSmsRepository clientCampaignSmsRepository;

    private final ClientCampaignSmsMapper clientCampaignSmsMapper;

    public ClientCampaignSmsQueryService(ClientCampaignSmsRepository clientCampaignSmsRepository, ClientCampaignSmsMapper clientCampaignSmsMapper) {
        this.clientCampaignSmsRepository = clientCampaignSmsRepository;
        this.clientCampaignSmsMapper = clientCampaignSmsMapper;
    }

    /**
     * Return a {@link List} of {@link ClientCampaignSmsDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ClientCampaignSmsDTO> findByCriteria(ClientCampaignSmsCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<ClientCampaignSms> specification = createSpecification(criteria);
        return clientCampaignSmsMapper.toDto(clientCampaignSmsRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ClientCampaignSmsDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ClientCampaignSmsDTO> findByCriteria(ClientCampaignSmsCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<ClientCampaignSms> specification = createSpecification(criteria);
        final Page<ClientCampaignSms> result = clientCampaignSmsRepository.findAll(specification, page);
        return result.map(clientCampaignSmsMapper::toDto);
    }

    /**
     * Function to convert ClientCampaignSmsCriteria to a {@link Specifications}
     */
    private Specifications<ClientCampaignSms> createSpecification(ClientCampaignSmsCriteria criteria) {
        Specifications<ClientCampaignSms> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ClientCampaignSms_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), ClientCampaignSms_.name));
            }
            if (criteria.getExcerpt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getExcerpt(), ClientCampaignSms_.excerpt));
            }
        }
        return specification;
    }

}
