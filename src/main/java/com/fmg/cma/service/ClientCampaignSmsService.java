package com.fmg.cma.service;

import com.fmg.cma.domain.ClientCampaignSms;
import com.fmg.cma.repository.ClientCampaignSmsRepository;
import com.fmg.cma.service.dto.ClientCampaignSmsDTO;
import com.fmg.cma.service.mapper.ClientCampaignSmsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing ClientCampaignSms.
 */
@Service
@Transactional
public class ClientCampaignSmsService {

    private final Logger log = LoggerFactory.getLogger(ClientCampaignSmsService.class);

    private final ClientCampaignSmsRepository clientCampaignSmsRepository;

    private final ClientCampaignSmsMapper clientCampaignSmsMapper;

    public ClientCampaignSmsService(ClientCampaignSmsRepository clientCampaignSmsRepository, ClientCampaignSmsMapper clientCampaignSmsMapper) {
        this.clientCampaignSmsRepository = clientCampaignSmsRepository;
        this.clientCampaignSmsMapper = clientCampaignSmsMapper;
    }

    /**
     * Save a clientCampaignSms.
     *
     * @param clientCampaignSmsDTO the entity to save
     * @return the persisted entity
     */
    public ClientCampaignSmsDTO save(ClientCampaignSmsDTO clientCampaignSmsDTO) {
        log.debug("Request to save ClientCampaignSms : {}", clientCampaignSmsDTO);
        ClientCampaignSms clientCampaignSms = clientCampaignSmsMapper.toEntity(clientCampaignSmsDTO);
        clientCampaignSms = clientCampaignSmsRepository.save(clientCampaignSms);
        return clientCampaignSmsMapper.toDto(clientCampaignSms);
    }

    /**
     * Get all the clientCampaignSms.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ClientCampaignSmsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ClientCampaignSms");
        return clientCampaignSmsRepository.findAll(pageable)
            .map(clientCampaignSmsMapper::toDto);
    }

    /**
     * Get one clientCampaignSms by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ClientCampaignSmsDTO findOne(Long id) {
        log.debug("Request to get ClientCampaignSms : {}", id);
        ClientCampaignSms clientCampaignSms = clientCampaignSmsRepository.findOne(id);
        return clientCampaignSmsMapper.toDto(clientCampaignSms);
    }

    /**
     * Delete the clientCampaignSms by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ClientCampaignSms : {}", id);
        clientCampaignSmsRepository.delete(id);
    }
}
