package com.fmg.cma.service;

import com.fmg.cma.domain.ClientCampaignTemplate;
import com.fmg.cma.repository.ClientCampaignTemplateRepository;
import com.fmg.cma.service.dto.ClientCampaignTemplateDTO;
import com.fmg.cma.service.mapper.ClientCampaignTemplateMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing ClientCampaignTemplate.
 */
@Service
@Transactional
public class ClientCampaignTemplateService {

    private final Logger log = LoggerFactory.getLogger(ClientCampaignTemplateService.class);

    private final ClientCampaignTemplateRepository clientCampaignTemplateRepository;

    private final ClientCampaignTemplateMapper clientCampaignTemplateMapper;

    public ClientCampaignTemplateService(ClientCampaignTemplateRepository clientCampaignTemplateRepository, ClientCampaignTemplateMapper clientCampaignTemplateMapper) {
        this.clientCampaignTemplateRepository = clientCampaignTemplateRepository;
        this.clientCampaignTemplateMapper = clientCampaignTemplateMapper;
    }

    /**
     * Save a clientCampaignTemplate.
     *
     * @param clientCampaignTemplateDTO the entity to save
     * @return the persisted entity
     */
    public ClientCampaignTemplateDTO save(ClientCampaignTemplateDTO clientCampaignTemplateDTO) {
        log.debug("Request to save ClientCampaignTemplate : {}", clientCampaignTemplateDTO);
        ClientCampaignTemplate clientCampaignTemplate = clientCampaignTemplateMapper.toEntity(clientCampaignTemplateDTO);
        clientCampaignTemplate = clientCampaignTemplateRepository.save(clientCampaignTemplate);
        return clientCampaignTemplateMapper.toDto(clientCampaignTemplate);
    }

    /**
     * Get all the clientCampaignTemplates.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ClientCampaignTemplateDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ClientCampaignTemplates");
        return clientCampaignTemplateRepository.findAll(pageable)
            .map(clientCampaignTemplateMapper::toDto);
    }

    /**
     * Get one clientCampaignTemplate by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ClientCampaignTemplateDTO findOne(Long id) {
        log.debug("Request to get ClientCampaignTemplate : {}", id);
        ClientCampaignTemplate clientCampaignTemplate = clientCampaignTemplateRepository.findOne(id);
        return clientCampaignTemplateMapper.toDto(clientCampaignTemplate);
    }

    /**
     * Delete the clientCampaignTemplate by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ClientCampaignTemplate : {}", id);
        clientCampaignTemplateRepository.delete(id);
    }
}
