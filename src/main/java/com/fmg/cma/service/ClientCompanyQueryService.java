package com.fmg.cma.service;


import com.fmg.cma.domain.ClientCompany;
import com.fmg.cma.domain.ClientCompanyStatus_;
import com.fmg.cma.domain.ClientCompany_;
import com.fmg.cma.domain.Industry_;
import com.fmg.cma.domain.User_;
import com.fmg.cma.repository.ClientCompanyRepository;
import com.fmg.cma.service.dto.ClientCompanyCriteria;
import com.fmg.cma.service.dto.ClientCompanyDTO;
import com.fmg.cma.service.mapper.ClientCompanyMapper;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for ClientCompany entities in the database.
 * The main input is a {@link ClientCompanyCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ClientCompanyDTO} or a {@link Page} of {@link ClientCompanyDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ClientCompanyQueryService extends QueryService<ClientCompany> {

    private final Logger log = LoggerFactory.getLogger(ClientCompanyQueryService.class);


    private final ClientCompanyRepository clientCompanyRepository;

    private final ClientCompanyMapper clientCompanyMapper;

    public ClientCompanyQueryService(ClientCompanyRepository clientCompanyRepository, ClientCompanyMapper clientCompanyMapper) {
        this.clientCompanyRepository = clientCompanyRepository;
        this.clientCompanyMapper = clientCompanyMapper;
    }

    /**
     * Return a {@link List} of {@link ClientCompanyDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ClientCompanyDTO> findByCriteria(ClientCompanyCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<ClientCompany> specification = createSpecification(criteria);
        return clientCompanyMapper.toDto(clientCompanyRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ClientCompanyDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ClientCompanyDTO> findByCriteria(ClientCompanyCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<ClientCompany> specification = createSpecification(criteria);
        final Page<ClientCompany> result = clientCompanyRepository.findAll(specification, page);
        return result.map(clientCompanyMapper::toDto);
    }

    /**
     * Function to convert ClientCompanyCriteria to a {@link Specifications}
     */
    private Specifications<ClientCompany> createSpecification(ClientCompanyCriteria criteria) {
        Specifications<ClientCompany> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ClientCompany_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), ClientCompany_.name));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), ClientCompany_.createdDate));
            }
            if (criteria.getCreatedById() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getCreatedById(), ClientCompany_.createdBy, User_.id));
            }
            if (criteria.getStatusId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getStatusId(), ClientCompany_.status, ClientCompanyStatus_.id));
            }
            if (criteria.getIndustryId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getIndustryId(), ClientCompany_.industry, Industry_.id));
            }
        }
        return specification;
    }

}
