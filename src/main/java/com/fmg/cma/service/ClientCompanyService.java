package com.fmg.cma.service;

import com.fmg.cma.domain.ClientCompany;
import com.fmg.cma.domain.ClientCompanyStatus;
import com.fmg.cma.domain.Industry;
import com.fmg.cma.domain.User;
import com.fmg.cma.repository.ClientCompanyRepository;
import com.fmg.cma.repository.ClientCompanyStatusRepository;
import com.fmg.cma.repository.IndustryRepository;
import com.fmg.cma.repository.UserRepository;
import com.fmg.cma.security.SecurityUtils;
import com.fmg.cma.service.dto.ClientCompanyDTO;
import com.fmg.cma.service.mapper.ClientCompanyMapper;
import com.fmg.cma.web.rest.errors.ValidationAlertException;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;


/**
 * Service Implementation for managing ClientCompany.
 */
@Service
@Transactional
public class ClientCompanyService {

    private final Logger log = LoggerFactory.getLogger(ClientCompanyService.class);

    private final ClientCompanyRepository clientCompanyRepository;

    private final ClientCompanyMapper clientCompanyMapper;

    private final ClientCompanyStatusRepository clientCompanyStatusRepository;

    private final UserRepository userRepository;

    private final IndustryRepository industryRepository;

    public ClientCompanyService(ClientCompanyRepository clientCompanyRepository,
                                ClientCompanyMapper clientCompanyMapper,
                                ClientCompanyStatusRepository clientCompanyStatusRepository,
                                UserRepository userRepository,
                                IndustryRepository industryRepository) {
        this.clientCompanyRepository = clientCompanyRepository;
        this.clientCompanyMapper = clientCompanyMapper;
        this.clientCompanyStatusRepository = clientCompanyStatusRepository;
        this.userRepository = userRepository;
        this.industryRepository = industryRepository;
    }

    /**
     * Save a clientCompany.
     *
     * @param clientCompanyDTO the entity to save
     * @return the persisted entity
     */
    public ClientCompanyDTO save(ClientCompanyDTO clientCompanyDTO) {
        log.debug("Request to save ClientCompany : {}", clientCompanyDTO);
        ClientCompany clientCompany = clientCompanyMapper.toEntity(clientCompanyDTO);

        assertCompanyDoesntExists(clientCompany.getName(), clientCompany.getId());

        User currentUser = userRepository.getByLogin(SecurityUtils.getCurrentUserLogin());
        if (clientCompany.getId() == null) {
            clientCompany.setCreatedDate(ZonedDateTime.now());

            clientCompany.setCreatedBy(currentUser);

            if (clientCompany.getStatus() == null) {
                ClientCompanyStatus activeStatus = clientCompanyStatusRepository.getByName(ClientCompanyStatus.ACTIVE);
                clientCompany.setStatus(activeStatus);
            }
        }
        clientCompany.setUpdatedDate(ZonedDateTime.now());
        clientCompany.setUpdatedBy(currentUser);

        clientCompany = clientCompanyRepository.save(clientCompany);
        return clientCompanyMapper.toDto(clientCompany);
    }

    /**
     * Assert that company doesn't exists, see mzdw4w/fmg#4.
     */
    private void assertCompanyDoesntExists(String name, @Nullable Long id) {
        name = name.trim();
        if (StringUtils.isEmpty(name)) {
            return;
        }

        List<ClientCompany> companies;
        if (id == null) {
            companies = clientCompanyRepository.findByNameIgnoreCase(name);
        } else {
            companies = clientCompanyRepository.findByNameIgnoreCaseAndIdNot(name, id);
        }

        if (companies.isEmpty()) {
            return;
        }

        Map<String, Object> parameters = ImmutableMap.of(
            "message", "error.company.nameexists",
            "params", ImmutableMap.of(
                "message", "error.company.nameexists",
                "name", name
            )
        );

        throw new ValidationAlertException("The Company \"" + name + "\" already exists", parameters);
    }

    /**
     * Get all the clientCompanies.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ClientCompanyDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ClientCompanies");
        return clientCompanyRepository.findAll(pageable)
            .map(clientCompanyMapper::toDto);
    }

    /**
     * Get one clientCompany by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ClientCompanyDTO findOne(Long id) {
        log.debug("Request to get ClientCompany : {}", id);
        ClientCompany clientCompany = clientCompanyRepository.findOne(id);
        return clientCompanyMapper.toDto(clientCompany);
    }

    /**
     * Delete the clientCompany by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ClientCompany : {}", id);
        clientCompanyRepository.delete(id);
    }

    public void changeIndustry(Long id, Long industryId) {
        ClientCompany clientCompany = clientCompanyRepository.getOne(id);
        if (industryId == null) {
            clientCompany.setIndustry(null);
        } else {
            Industry industry = industryRepository.getOne(industryId);
            clientCompany.setIndustry(industry);
        }
        clientCompanyRepository.save(clientCompany);
    }
}
