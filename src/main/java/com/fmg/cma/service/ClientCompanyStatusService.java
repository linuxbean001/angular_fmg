package com.fmg.cma.service;

import com.fmg.cma.domain.ClientCompanyStatus;
import com.fmg.cma.repository.ClientCompanyStatusRepository;
import com.fmg.cma.service.dto.ClientCompanyStatusDTO;
import com.fmg.cma.service.mapper.ClientCompanyStatusMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing ClientCompanyStatus.
 */
@Service
@Transactional
public class ClientCompanyStatusService {

    private final Logger log = LoggerFactory.getLogger(ClientCompanyStatusService.class);

    private final ClientCompanyStatusRepository clientCompanyStatusRepository;

    private final ClientCompanyStatusMapper clientCompanyStatusMapper;

    public ClientCompanyStatusService(ClientCompanyStatusRepository clientCompanyStatusRepository, ClientCompanyStatusMapper clientCompanyStatusMapper) {
        this.clientCompanyStatusRepository = clientCompanyStatusRepository;
        this.clientCompanyStatusMapper = clientCompanyStatusMapper;
    }

    /**
     * Save a clientCompanyStatus.
     *
     * @param clientCompanyStatusDTO the entity to save
     * @return the persisted entity
     */
    public ClientCompanyStatusDTO save(ClientCompanyStatusDTO clientCompanyStatusDTO) {
        log.debug("Request to save ClientCompanyStatus : {}", clientCompanyStatusDTO);
        ClientCompanyStatus clientCompanyStatus = clientCompanyStatusMapper.toEntity(clientCompanyStatusDTO);
        clientCompanyStatus = clientCompanyStatusRepository.save(clientCompanyStatus);
        return clientCompanyStatusMapper.toDto(clientCompanyStatus);
    }

    /**
     * Get all the clientCompanyStatuses.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ClientCompanyStatusDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ClientCompanyStatuses");
        return clientCompanyStatusRepository.findAll(pageable)
            .map(clientCompanyStatusMapper::toDto);
    }

    /**
     * Get one clientCompanyStatus by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ClientCompanyStatusDTO findOne(Long id) {
        log.debug("Request to get ClientCompanyStatus : {}", id);
        ClientCompanyStatus clientCompanyStatus = clientCompanyStatusRepository.findOne(id);
        return clientCompanyStatusMapper.toDto(clientCompanyStatus);
    }

    /**
     * Delete the clientCompanyStatus by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ClientCompanyStatus : {}", id);
        clientCompanyStatusRepository.delete(id);
    }
}
