package com.fmg.cma.service;

import com.fmg.cma.domain.ClientField;
import com.fmg.cma.repository.ClientFieldRepository;
import com.fmg.cma.service.dto.ClientFieldDTO;
import com.fmg.cma.service.mapper.ClientFieldMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing ClientField.
 */
@Service
@Transactional
public class ClientFieldService {

    private final Logger log = LoggerFactory.getLogger(ClientFieldService.class);

    private final ClientFieldRepository clientFieldRepository;

    private final ClientFieldMapper clientFieldMapper;

    public ClientFieldService(ClientFieldRepository clientFieldRepository, ClientFieldMapper clientFieldMapper) {
        this.clientFieldRepository = clientFieldRepository;
        this.clientFieldMapper = clientFieldMapper;
    }

    /**
     * Save a clientField.
     *
     * @param clientFieldDTO the entity to save
     * @return the persisted entity
     */
    public ClientFieldDTO save(ClientFieldDTO clientFieldDTO) {
        log.debug("Request to save ClientField : {}", clientFieldDTO);
        ClientField clientField = clientFieldMapper.toEntity(clientFieldDTO);
        clientField = clientFieldRepository.save(clientField);
        return clientFieldMapper.toDto(clientField);
    }

    /**
     * Get all the clientFields.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ClientFieldDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ClientFields");
        return clientFieldRepository.findAll(pageable)
            .map(clientFieldMapper::toDto);
    }

    /**
     * Get one clientField by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ClientFieldDTO findOne(Long id) {
        log.debug("Request to get ClientField : {}", id);
        ClientField clientField = clientFieldRepository.findOne(id);
        return clientFieldMapper.toDto(clientField);
    }

    /**
     * Delete the clientField by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ClientField : {}", id);
        clientFieldRepository.delete(id);
    }
}
