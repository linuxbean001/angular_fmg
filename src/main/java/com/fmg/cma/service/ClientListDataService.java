package com.fmg.cma.service;

import com.fmg.cma.domain.ClientListData;
import com.fmg.cma.repository.ClientListDataRepository;
import com.fmg.cma.service.dto.ClientListDataDTO;
import com.fmg.cma.service.mapper.ClientListDataMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing ClientListData.
 */
@Service
@Transactional
public class ClientListDataService {

    private final Logger log = LoggerFactory.getLogger(ClientListDataService.class);

    private final ClientListDataRepository clientListDataRepository;

    private final ClientListDataMapper clientListDataMapper;

    public ClientListDataService(ClientListDataRepository clientListDataRepository, ClientListDataMapper clientListDataMapper) {
        this.clientListDataRepository = clientListDataRepository;
        this.clientListDataMapper = clientListDataMapper;
    }

    /**
     * Save a clientListData.
     *
     * @param clientListDataDTO the entity to save
     * @return the persisted entity
     */
    public ClientListDataDTO save(ClientListDataDTO clientListDataDTO) {
        log.debug("Request to save ClientListData : {}", clientListDataDTO);
        ClientListData clientListData = clientListDataMapper.toEntity(clientListDataDTO);
        clientListData = clientListDataRepository.save(clientListData);
        return clientListDataMapper.toDto(clientListData);
    }

    /**
     * Get all the clientListData.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ClientListDataDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ClientListData");
        return clientListDataRepository.findAll(pageable)
            .map(clientListDataMapper::toDto);
    }

    /**
     * Get one clientListData by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ClientListDataDTO findOne(Long id) {
        log.debug("Request to get ClientListData : {}", id);
        ClientListData clientListData = clientListDataRepository.findOne(id);
        return clientListDataMapper.toDto(clientListData);
    }

    /**
     * Delete the clientListData by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ClientListData : {}", id);
        clientListDataRepository.delete(id);
    }
}
