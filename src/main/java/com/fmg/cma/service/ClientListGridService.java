package com.fmg.cma.service;

import com.fmg.cma.domain.CallHistory;
import com.fmg.cma.domain.CallHistoryType;
import com.fmg.cma.domain.CampaignListContactResult;
import com.fmg.cma.domain.ClientCampaign;
import com.fmg.cma.domain.ClientField;
import com.fmg.cma.domain.ClientList;
import com.fmg.cma.domain.ClientListRowNote;
import com.fmg.cma.domain.ConsoleStats;
import com.fmg.cma.domain.DataListContactState;
import com.fmg.cma.domain.Field;
import com.fmg.cma.domain.Outcome;
import com.fmg.cma.domain.QcQa;
import com.fmg.cma.domain.TransactionReport;
import com.fmg.cma.domain.User;
import com.fmg.cma.repository.CallHistoryRepository;
import com.fmg.cma.repository.CallHistoryTypeRepository;
import com.fmg.cma.repository.CampaignListContactResultRepository;
import com.fmg.cma.repository.ClientFieldRepository;
import com.fmg.cma.repository.ClientListDataRepository;
import com.fmg.cma.repository.ClientListRepository;
import com.fmg.cma.repository.ClientListRowNoteRepository;
import com.fmg.cma.repository.FieldRepository;
import com.fmg.cma.repository.OutcomeRepository;
import com.fmg.cma.repository.UserRepository;
import com.fmg.cma.security.SecurityUtils;
import com.fmg.cma.service.dto.CallHistoryDTO;
import com.fmg.cma.service.dto.ClientCampaignDTO;
import com.fmg.cma.service.dto.ClientListDataDTO;
import com.fmg.cma.service.dto.ClientListRowDto;
import com.fmg.cma.service.dto.DataListCompany;
import com.fmg.cma.service.dto.DataListCompanyPk;
import com.fmg.cma.service.dto.DataListContact;
import com.fmg.cma.service.dto.DataListSummary;
import com.fmg.cma.service.dto.RowValueDto;
import com.fmg.cma.service.mapper.CallHistoryMapper;
import com.fmg.cma.service.mapper.ClientFieldMapper;
import com.fmg.cma.service.util.SqlUtils;
import com.google.common.base.MoreObjects;
import com.google.common.base.Suppliers;
import com.google.common.collect.ImmutableMap;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Collections.singletonMap;

@Service
@Transactional
public class ClientListGridService {

    /**
     * See AlertsComponent.CONTACT_TYPE_FIELD_NAME.
     */
    private static final String CONTACT_TYPE_FIELD_NAME = "Contact Type";
    public static final String STRING_CONCATENATE_DELIMITER = ";";

    private final Logger log = LoggerFactory.getLogger(ClientListGridService.class);

    private final ClientFieldRepository clientFieldRepository;
    private final ClientListDataRepository clientListDataRepository;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private final ClientFieldMapper clientFieldMapper;
    private final FieldRepository fieldRepository;
    private final ClientListRowNoteRepository clientListRowNoteRepository;
    private final ClientListRepository clientListRepository;
    private final CallHistoryRepository callHistoryRepository;
    private final CallHistoryMapper callHistoryMapper;
    private final CallHistoryService callHistoryService;
    private final CallHistoryTypeRepository callHistoryTypeRepository;
    private final CampaignListContactResultRepository campaignListContactResultRepository;
    private final UserRepository userRepository;
    private final OutcomeRepository outcomeRepository;
    private final JpaProperties jpaProperties;
    private final MailService mailService;
    private final ConsoleStatsService consoleStatsService;
    private ClientCampaignService clientCampaignService;

    public ClientListGridService(ClientFieldRepository clientFieldRepository,
                                 ClientListDataRepository clientListDataRepository,
                                 NamedParameterJdbcTemplate namedParameterJdbcTemplate,
                                 ClientFieldMapper clientFieldMapper,
                                 FieldRepository fieldRepository,
                                 ClientListRowNoteRepository clientListRowNoteRepository,
                                 ClientListRepository clientListRepository,
                                 CallHistoryRepository callHistoryRepository,
                                 CallHistoryMapper callHistoryMapper,
                                 CallHistoryService callHistoryService,
                                 CallHistoryTypeRepository callHistoryTypeRepository,
                                 CampaignListContactResultRepository campaignListContactResultRepository,
                                 UserRepository userRepository,
                                 OutcomeRepository outcomeRepository,
                                 JpaProperties jpaProperties,
                                 MailService mailService,
                                 ConsoleStatsService consoleStatsService,
                                 ClientCampaignService clientCampaignService) {
        this.clientFieldRepository = clientFieldRepository;
        this.clientListDataRepository = clientListDataRepository;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
        this.clientFieldMapper = clientFieldMapper;
        this.fieldRepository = fieldRepository;
        this.clientListRowNoteRepository = clientListRowNoteRepository;
        this.clientListRepository = clientListRepository;
        this.callHistoryRepository = callHistoryRepository;
        this.callHistoryMapper = callHistoryMapper;
        this.callHistoryService = callHistoryService;
        this.callHistoryTypeRepository = callHistoryTypeRepository;
        this.campaignListContactResultRepository = campaignListContactResultRepository;
        this.userRepository = userRepository;
        this.outcomeRepository = outcomeRepository;
        this.jpaProperties = jpaProperties;
        this.mailService = mailService;
        this.consoleStatsService = consoleStatsService;
        this.clientCampaignService = clientCampaignService;
    }

    public List<ClientField> getFields(long id) {
        return clientFieldRepository.getByListId(id);
    }

    public Page<ClientListRowDto> getRows(long id, Pageable pageable) {
        List<ClientField> fields = getFields(id);
//        List<ClientListData> byClientFieldIn = clientListDataRepository.getByClientFieldIn(fields);

        Set<String> availableFieldNames = new HashSet<>();
        availableFieldNames.add("row_num");

        final String prefix = "field_";
        StringBuilder sb = new StringBuilder();
        for (ClientField field : fields) {
            Long fieldId = field.getId();
            sb.append(", max(case when cld.client_field_id = ")
                .append(fieldId)
                .append(" then cvalue else null end) ")
                .append(prefix)
                .append(fieldId);
            availableFieldNames.add(prefix + fieldId);
        }

        StringBuilder orderSb = new StringBuilder();
        if (pageable != null && pageable.getSort() != null) {
            Sort sort = pageable.getSort();
            String comma = "";
            for (Sort.Order order : sort) {
                String property = order.getProperty();
                if (availableFieldNames.contains(property)) {
                    orderSb.append(comma)
                        .append(property)
                        .append(" ")
                        .append(order.getDirection());
                    comma = ", ";
                }
            }
        }

        if (orderSb.length() == 0) {
            orderSb.append("row_num");
        }

        String sql = "select * " +
            "from (select cld.row_num " + sb.toString() +
            "      from client_list_data cld " +
            "             join client_field cf on cf.id = cld.client_field_id " +
            "      where cf.list_id = :list_id " +
            "      group by cld.row_num) a " +
            "order by " + orderSb.toString();

        int offset = pageable.getPageNumber() * pageable.getPageSize();
        SqlUtils.SqlWithParameters paginated = SqlUtils.createPaginatedSql(sql, offset, pageable.getPageSize());
        ImmutableMap<String, Object> params = ImmutableMap.<String, Object>builder()
            .put("list_id", id)
            .putAll(paginated.getParams())
            .build();

        List<ClientListRowDto> rows = new ArrayList<>();

        SqlRowSet rs = namedParameterJdbcTemplate.queryForRowSet(paginated.getSql(), params);
        while (rs.next()) {
            ClientListRowDto row = new ClientListRowDto(rs.getLong(1));

            int i = 2;
            for (ClientField field : fields) {
                RowValueDto rowValueDto = new RowValueDto()
                    .setCvalue(rs.getString(i++));
                row.getValues().put(field.getId(), rowValueDto);
            }

            rows.add(row);
        }

        return new PageImpl<>(rows, pageable, getTotal(id));
    }

    public List<TransactionReport> getTransactionReport() {

        List<ClientCampaignDTO> campaigns = clientCampaignService.getConsoleRelatedClientCampaigns();
        List<TransactionReport> reports = new ArrayList<>();

        campaigns.forEach(campaign -> {
            campaign.getClientLists().forEach(clientList -> {
                Long id = clientList.getId();
                List<ClientField> fields = getFields(id);

                Page<ClientListRowDto> rows = getRows(id, new PageRequest(0, Integer.MAX_VALUE));
                List<CallHistoryDTO> callReport = callHistoryRepository.findByListId(id).stream().map(callHistoryMapper::toDto).collect(Collectors.toList());
                AtomicInteger i = new AtomicInteger();
                callReport.stream().forEach(call -> {
                    ClientListRowDto row = rows.getContent().stream().filter(listRowDto -> listRowDto.getRowNum() == Optional.ofNullable(call.getContactRowNum()).orElse(0L)).findFirst().orElse(null);
                    if (row != null) {
                        TransactionReport r = new TransactionReport(call);
                        fields.forEach(clientField -> {
                            r.of(clientField.getField().getId().intValue(), row.getValues().get(clientField.getId()).getCvalue());
                        });
                        reports.add(r);
                    }
                });
            });
        });

        return reports;
    }

    private long getTotal(long id) {
        String sql = "select count(1) " +
            "from (select cld.row_num " +
            "      from client_list_data cld " +
            "             join client_field cf on cf.id = cld.client_field_id " +
            "      where cf.list_id = :list_id " +
            "      group by cld.row_num) a";

        return namedParameterJdbcTemplate.queryForObject(sql, singletonMap("list_id", id), Long.class);
    }

    public void deleteRow(long listId, long rowNum) {
        String sql = "delete " +
            "from client_list_data " +
            "where id in (select id " +
            "             from (select cld.id " +
            "                   from client_list_data cld " +
            "                          join client_field cf on cf.id = cld.client_field_id " +
            "                   where cf.list_id = :list_id " +
            "                     and cld.row_num = :row_num) as id)";

        ImmutableMap<String, Object> map = ImmutableMap.of(
            "list_id", listId,
            "row_num", rowNum
        );

        namedParameterJdbcTemplate.update(sql, map);

        actualizeRowCount(listId);
    }

    public void update(ClientListDataDTO clientListDataDTO) {
        String sql = "update client_list_data " +
            "set cvalue = :cvalue " +
            "where client_field_id = :client_field_id " +
            "  and row_num = :row_num";

        ImmutableMap<String, Object> map = ImmutableMap.of(
            "cvalue", clientListDataDTO.getCvalue(),
            "client_field_id", clientListDataDTO.getClientFieldId(),
            "row_num", clientListDataDTO.getRowNum()
        );

        int updated = namedParameterJdbcTemplate.update(sql, map);
        if (updated == 0) {
            // it's a new value, we need to insert it
            sql = "insert into client_list_data (cvalue, client_field_id, row_num) " +
                "values (:cvalue, :client_field_id, :row_num)";
            namedParameterJdbcTemplate.update(sql, map);
        }
    }

    public void actualizeRowCount(long listId) {
        String sql = "update client_list cl " +
            "set cl.row_count = (select row_count " +
            "                    from (select client_list_id, count(1) row_count " +
            "                          from (select distinct d.row_num, l.id client_list_id " +
            "                                from client_list l " +
            "                                       join client_field f on f.LIST_ID = l.ID " +
            "                                       join client_list_data d on d.CLIENT_FIELD_ID = f.ID) a " +
            "                          group by client_list_id) b " +
            "                    where client_list_id = cl.id) " +
            "where cl.id = :list_id";

        namedParameterJdbcTemplate.update(sql, singletonMap("list_id", listId));
    }

    /**
     * Cross-database.
     */
    public static String getConcatenateFunction(JpaProperties jpaProperties, String fieldName) {
        // if (env.acceptsProfiles(JHipsterConstants.SPRING_PROFILE_DEVELOPMENT)) {
        if (Database.H2.equals(jpaProperties.getDatabase())) {
            // h2 introduces LISTAGG in Version 1.4.198, see here http://www.h2database.com/html/changelog.html
            return "LISTAGG(" + fieldName + ", '" + STRING_CONCATENATE_DELIMITER + "')";
        } else {
            // at production we are using mariadb
            return "GROUP_CONCAT(" + fieldName + " SEPARATOR '" + STRING_CONCATENATE_DELIMITER + "')";
        }
    }

    public DataListSummary getSummary(long listId) {
        List<ClientField> all = clientFieldRepository.getByListId(listId);
        List<ClientField> pkClientFields = clientFieldRepository.getByListIdAndIsOrgInformation(listId, true);

        DataListSummary dataListSummary = new DataListSummary();

        SummaryCache summaryCache = new SummaryCache(listId);

        if (!pkClientFields.isEmpty()) {
            // Map<Long, ClientListRowNote> notePerRowNum = getNotePerRowNum(listId);
            Map<DataListCompanyPk, List<DataListContact>> mapContacts = getCompanyContacts(listId, pkClientFields, summaryCache);

            StringBuilder sb = new StringBuilder("cld.row_num");
            StringBuilder selectSb = new StringBuilder(getConcatenateFunction(jpaProperties, "row_num") + " all_row_nums, count(*)");
            StringBuilder groupBySb = new StringBuilder();
            String groupByPrefix = "";
            StringBuilder orderBySb = new StringBuilder("all_row_nums");
            for (ClientField field : pkClientFields) {
                Long fieldId = field.getId();
                String fieldName = "field_" + fieldId;

                sb.append(", max(case when cld.client_field_id = ")
                    .append(fieldId)
                    .append(" then cvalue else null end) ")
                    .append(fieldName);

                selectSb.append(", ")
                    .append(fieldName);

                groupBySb.append(groupByPrefix)
                    .append(fieldName);
                groupByPrefix = ", ";

                orderBySb.append(", ")
                    .append(fieldName);
            }

            String sql = "select " + selectSb.toString() + " " +
                "from (select " + sb.toString() +
                "      from client_list_data cld " +
                "             join client_field cf on cf.id = cld.client_field_id " +
                "      where cf.list_id = :list_id " +
                "      group by cld.row_num) a " +
                " group by " + groupBySb.toString() +
                " order by " + orderBySb.toString();

            SqlRowSet rs = namedParameterJdbcTemplate.queryForRowSet(sql, ImmutableMap.of("list_id", listId));
            while (rs.next()) {
                List<Long> rowNums = (Stream.of(rs.getString(1).split(STRING_CONCATENATE_DELIMITER))).map(Long::parseLong).collect(Collectors.toList());
                long minRowNum = rowNums.stream().min(Long::compare).orElseThrow(() -> new IllegalArgumentException("Could not get minimum row num"));
                long count = rs.getLong(2); // number of contacts in this company
                // String note = getCompanyNote(notePerRowNum, rowNum);

                DataListCompany dataListCompany = new DataListCompany()
                    .setRowNum(minRowNum)
                    .setRowNums(rowNums);
                // .setNote(note);
                int i = 3; // row_num is the first column, count is the second column
                for (ClientField field : pkClientFields) {
                    String cvalue = rs.getString(i++);
                    dataListCompany.getValueMap().put(field.getId(), cvalue);
                }
                dataListSummary.getCompanies().add(dataListCompany);

                // load all contacts
                DataListCompanyPk pk = new DataListCompanyPk(dataListCompany.getValueMap());
                List<DataListContact> contacts = mapContacts.getOrDefault(pk, Collections.emptyList());
                dataListCompany.setContacts(contacts);

                // load full history
                List<CallHistoryDTO> histories = summaryCache.orgRowNumCallHistoriesSupplier.get().getOrDefault(minRowNum, Collections.emptyList());
                dataListCompany.setHistories(histories);
            }
        }

        dataListSummary.setPkFields(pkClientFields.stream()
            .sorted(Comparator.comparingLong(o -> MoreObjects.firstNonNull(o.getField().getOrder(), o.getField().getId())))
            .map(clientFieldMapper::toDto).collect(Collectors.toList()));
        // apply non-pk fields
        all.removeAll(pkClientFields);
        dataListSummary.setFields(all.parallelStream()
            .sorted(Comparator.comparingLong(o -> MoreObjects.firstNonNull(o.getField().getOrder(), o.getField().getId())))
            .map(clientFieldMapper::toDto).collect(Collectors.toList()));


        // clientListRowNoteRepository.getByListIdAndRowNum(listId, )
        // dataListSummary.setNote()

        return dataListSummary;
    }

    public Field getContactTypeField() {
        // select * from FIELD where name = 'Contact Type'
        return Optional.ofNullable(fieldRepository.getByName(CONTACT_TYPE_FIELD_NAME))
            .orElseThrow(() -> new RuntimeException("Could not find field " + CONTACT_TYPE_FIELD_NAME + ", check it in the table FIELD"));
    }

    public Map<DataListCompanyPk, List<DataListContact>> getCompanyContacts(long listId, List<ClientField> pkClientFields, SummaryCache summaryCache) {
        Field contactTypeField = summaryCache.contactTypeFieldSupplier.get();

        // ClientList clientList = clientListRepository.getOne(listId);
        List<ClientField> all = summaryCache.clientFieldsSupplier.get();
        // nullable
        Long contactTypeClientFieldId = all.stream()
            .filter(clientField -> clientField.getField().getId().equals(contactTypeField.getId()))
            .findFirst()
            .map(ClientField::getId)
            .orElse(null);

        StringBuilder select = new StringBuilder("cld.row_num");
        for (ClientField field : all) {
            Long fieldId = field.getId();
            String fieldName = "field_" + fieldId;

            select.append(", ")
                .append("max(case when cld.client_field_id = ")
                .append(fieldId)
                .append(" then cvalue else null end) ")
                .append(fieldName);
        }

        String sql = "select " + select.toString() +
            "      from client_list_data cld " +
            "             join client_field cf on cf.id = cld.client_field_id " +
            "      where cf.list_id = :list_id " +
            "      group by cld.row_num " +
            "      order by cld.row_num";

        Map<Long, ClientListRowNote> notePerRowNum = summaryCache.notePerRowNumSupplier.get();
        Map<DataListCompanyPk, List<DataListContact>> map = new HashMap<>();

        // FIXME here is a problem if a list contains multiple rows with same contact type - we'll use the latest of it
        // see for example: /home/nev/Pictures/Selection_231.png
        Map<String, Long> params = singletonMap("list_id", listId);
        log.debug("Execute: {}; {}", sql, params);
        SqlRowSet rs = namedParameterJdbcTemplate.queryForRowSet(sql, params);
        while (rs.next()) {
            long rowNum = rs.getLong(1);

            Map<Long, String> valueMap = new HashMap<>();
            Map<Long, String> pkValueMap = new HashMap<>();
            int i = 2;
            for (ClientField field : all) {
                String value = rs.getString(i++);
                if (pkClientFields.contains(field)) {
                    pkValueMap.put(field.getId(), value);
                } else {
                    valueMap.put(field.getId(), value);
                }
            }

            // nullable
            String contactType = valueMap.get(contactTypeClientFieldId);
            Boolean contactIsLeftCompany = getContactIsLeftCompany(notePerRowNum, rowNum);

            DataListContact dataListContact = new DataListContact()
                .setContactType(contactType)
                .setRowNum(rowNum)
                // .setNote(note)
                .setIsLeftCompany(contactIsLeftCompany)
                // .setFields(clientFieldMapper.toDto(regularFields))
                .setValueMap(valueMap);

            // TODO optimize or refactor
            CampaignListContactResult result = summaryCache.contactRowNumCampaignListContactResultSupplier.get().get(rowNum);
            if (result != null) {
                dataListContact.setState(result.getState());
                dataListContact.setAssignStamp(result.getAssignStamp());
                dataListContact.setClickedOutcomeId(Optional.ofNullable(result.getClickedOutcome()).map(Outcome::getId).orElse(null));
                if (result.getAssignUser() != null) {
                    dataListContact.setAssignUserId(result.getAssignUser().getId());
                    dataListContact.setAssignUserLogin(result.getAssignUser().getLogin());
                }
            } else {
                dataListContact.setState(DataListContactState.ACTIVE.getIdent());
            }

            DataListCompanyPk pk = new DataListCompanyPk(pkValueMap);
            List<DataListContact> dataListContacts = map.computeIfAbsent(pk, x -> new ArrayList<>());
            dataListContacts.add(dataListContact);
        }

        return map;
    }

    public List<CallHistoryDTO> getCallHistories(long listId) {
        return callHistoryRepository.findByListId(listId).stream()
            .map(callHistoryMapper::toDto)
            .collect(Collectors.toList());
    }

    public List<CallHistoryDTO> getCallHistories(long listId, long orgRowNum) {
        return callHistoryRepository.findByListIdAndOrgRowNum(listId, orgRowNum).stream()
            .map(callHistoryMapper::toDto)
            .collect(Collectors.toList());
    }

    @NotNull
    private Map<Long, ClientListRowNote> getNotePerRowNum(long listId) {
        return clientListRowNoteRepository.getByListId(listId).stream()
            .collect(Collectors.toMap(ClientListRowNote::getRowNum, x -> x));
    }

    private String getCompanyNote(Map<Long, ClientListRowNote> notePerRowNum, Long rowNum) {
        return Optional.ofNullable(notePerRowNum.get(rowNum)).map(ClientListRowNote::getNote).orElse(null);
    }

    private String getContactNote(Map<Long, ClientListRowNote> notePerRowNum, Long rowNum) {
        return Optional.ofNullable(notePerRowNum.get(rowNum)).map(ClientListRowNote::getContactNote).orElse(null);
    }

    private Boolean getContactIsLeftCompany(Map<Long, ClientListRowNote> notePerRowNum, Long rowNum) {
        return Optional.ofNullable(notePerRowNum.get(rowNum)).map(ClientListRowNote::getIsLeftCompany).orElse(null);
    }

    private boolean isContactTypeField(ClientField field, Field contactTypeField) {
        return field.getField().getId().equals(contactTypeField.getId());
    }

    public DataListContact saveCompanyContact(long listId, DataListCompany company, DataListContact contact) {
        if (DataListContactState.MY_LIST.getIdent().equals(contact.getState())) {
            User currentUser = userRepository.getByLogin(SecurityUtils.getCurrentUserLogin());
            contact.setAssignUserId(currentUser.getId());
            contact.setAssignUserLogin(currentUser.getLogin());
            contact.setAssignStamp(ZonedDateTime.now());
        }

        String sql;
        if (contact.getRowNum() == null) {
            contact.setRowNum(getMaxRowNum(listId) + 1); // new row
            sql = "insert into client_list_data (cvalue, row_num, client_field_id) " +
                "values (:cvalue, :row_num, :client_field_id)";

            for (Map.Entry<Long, String> e : company.getValueMap().entrySet()) {
                String cvalue = e.getValue();
                Long clientFieldId = e.getKey();

                Map<String, Object> map = new HashMap<>();
                map.put("cvalue", cvalue);
                map.put("row_num", contact.getRowNum());
                map.put("client_field_id", clientFieldId);

                namedParameterJdbcTemplate.update(sql, map);
            }
        } else {
            sql = "update client_list_data " +
                "set cvalue = :cvalue " +
                "where row_num = :row_num " +
                "  and client_field_id = :client_field_id";

            boolean contactAlreadyBelongsToCompany = company.getRowNums().contains(contact.getRowNum());
            if (!contactAlreadyBelongsToCompany) {
                // contact is moved to this company from another
                for (Map.Entry<Long, String> e : company.getValueMap().entrySet()) {
                    String cvalue = e.getValue();
                    Long clientFieldId = e.getKey();

                    Map<String, Object> map = new HashMap<>();
                    map.put("cvalue", cvalue);
                    map.put("row_num", contact.getRowNum());
                    map.put("client_field_id", clientFieldId);

                    namedParameterJdbcTemplate.update(sql, map);
                }
            }
        }

        for (Map.Entry<Long, String> e : contact.getValueMap().entrySet()) {
            String cvalue = e.getValue();
            Long clientFieldId = e.getKey();

            Map<String, Object> map = new HashMap<>();
            map.put("cvalue", cvalue);
            map.put("row_num", contact.getRowNum());
            map.put("client_field_id", clientFieldId);

            namedParameterJdbcTemplate.update(sql, map);
        }

        ClientListRowNote clientListRowNote = getClientListRowNote(listId, contact.getRowNum());
        clientListRowNote.setContactNote(contact.getNote());
        clientListRowNote.setIsLeftCompany(contact.getIsLeftCompany());
        clientListRowNoteRepository.save(clientListRowNote);

        ClientList clientList = clientListRepository.getOne(listId);
        ClientCampaign clientCampaign = clientList.getClientCampaign();
        CampaignListContactResult campaignListContactResult = campaignListContactResultRepository.findByClientCampaignIdAndClientListIdAndContactRowNum(clientCampaign.getId(), listId, contact.getRowNum())
            .orElseGet(() -> new CampaignListContactResult(clientCampaign, clientList, contact.getRowNum()));
        campaignListContactResult.setState(contact.getState());
        campaignListContactResult.setAssignStamp(contact.getAssignStamp());
        campaignListContactResult.setAssignUser(Optional.ofNullable(contact.getAssignUserId()).map(userRepository::getOne).orElse(null));
        Outcome clickedOutcome = Optional.ofNullable(contact.getClickedOutcomeId()).map(outcomeRepository::getOne).orElse(null);
        campaignListContactResult.setClickedOutcome(clickedOutcome);
        campaignListContactResultRepository.save(campaignListContactResult);

        CallHistory callHistory = new CallHistory()
            .setType(callHistoryTypeRepository.getByIdent(CallHistoryType.CONTACT_CHANGE))
            .setList(clientList)
            .setOrgRowNum(company.getRowNum())
            .setContactRowNum(contact.getRowNum())
            .setNote(contact.getNote());
        callHistoryService.save(callHistory);

        // an alert and email will be sent to the Quality assurance Person  (x2)
        if (DataListContactState.PENDING.getIdent().equals(contact.getState()) && clickedOutcome != null) {
            String qcqaString = clickedOutcome.getQcqa();
            if (QcQa.URGENT.getIdent().equals(qcqaString) || QcQa.EYE_CHECK.getIdent().equals(qcqaString)) {
                QcQa qcQa = QcQa.valueOf(qcqaString);
                Set<User> qaUsers = clientCampaign.getQaUsers();
                for (User qaUser : qaUsers) {
                    mailService.sendEmail(qaUser.getEmail(), "Quality Assurance Alert",
                        "There is an \"" + qcQa.getName() + "\" Quality Assurance Alert for you to check in Hamilton",
                        false, false, null);
                }
            }
            // increment pending
            consoleStatsService.applyChanges(new ConsoleStats().setPending(1));
        }

        return contact;
    }

    Long getMaxRowNum(long listId) {
        String sql = "select max(cld.ROW_NUM) " +
            "from client_list_data cld " +
            "         join client_field cf on cf.id = cld.client_field_id " +
            "where cf.list_id = :list_id";
        return namedParameterJdbcTemplate.queryForObject(sql, ImmutableMap.of("list_id", listId), Long.class);
    }

    public DataListCompany saveCompany(long listId, DataListCompany company) {
        List<ClientField> pkClientFields = clientFieldRepository.getByListIdAndIsOrgInformation(listId, true);

        HashMap<Long, String> pkValueMap = pkClientFields.stream()
            .map(ClientField::getId)
            .collect(
                HashMap::new,
                (map, clientFieldId) -> map.put(clientFieldId, company.getValueMap().get(clientFieldId)),
                Map::putAll
            );

        boolean isNewCompany = company.getRowNum() == null;

        String sql;
        if (isNewCompany) {
            // new row
            long rowNum = getMaxRowNum(listId) + 1;
            company.setRowNum(rowNum);
            company.setRowNums(Collections.singletonList(rowNum));

            sql = "insert into client_list_data (cvalue, row_num, client_field_id) " +
                "values (:cvalue, :row_num, :client_field_id)";
        } else {
            sql = "update client_list_data " +
                "set cvalue = :cvalue " +
                "where row_num = :row_num " +
                "  and client_field_id = :client_field_id";
        }

        for (Long rowNum : company.getRowNums()) {
            for (Map.Entry<Long, String> e : pkValueMap.entrySet()) {
                String cvalue = e.getValue();
                Long clientFieldId = e.getKey();

                ImmutableMap<String, Object> map = ImmutableMap.of(
                    "cvalue", cvalue,
                    "row_num", rowNum,
                    "client_field_id", clientFieldId
                );

                namedParameterJdbcTemplate.update(sql, map);
            }
        }

        // save all contacts
        if (!company.getContacts().isEmpty()) {
            // List<ClientField> regularClientFields = clientFieldRepository.getByListIdAndIsOrgInformation(listId, false);
            company.getContacts().forEach(contact -> {
                // HashMap<Long, String> dataValueMap = regularClientFields.stream()
                //     .map(ClientField::getId)
                //     .collect(
                //         HashMap::new,
                //         (map, clientFieldId) -> map.put(clientFieldId, contact.getValueMap().get(clientFieldId)),
                //         Map::putAll
                //     );
                //
                // DataListCompanyPk data = new DataListCompanyPk(dataValueMap);
                // saveCompanyContact(listId, pk, data, company.getRowNum());
                saveCompanyContact(listId, company, contact);
            });
        }

        ClientListRowNote clientListRowNote = getClientListRowNote(listId, company.getRowNum());
        clientListRowNote.setNote(company.getNote());
        clientListRowNoteRepository.save(clientListRowNote);

        CallHistory callHistory = new CallHistory()
            .setType(callHistoryTypeRepository.getByIdent(CallHistoryType.ORGANIZATION_CHANGE))
            .setList(clientListRepository.getOne(listId))
            .setOrgRowNum(company.getRowNum())
            .setNote(company.getNote());
        callHistoryService.save(callHistory);

        return company;
    }

    public void saveCompanyNote(long listId, Long rowNum, String note) {
        ClientListRowNote clientListRowNote = getClientListRowNote(listId, rowNum);
        clientListRowNote.setNote(note);
        clientListRowNoteRepository.save(clientListRowNote);
    }

    private ClientListRowNote getClientListRowNote(long listId, Long rowNum) {
        return Optional.ofNullable(clientListRowNoteRepository.getByListIdAndRowNum(listId, rowNum))
            .orElseGet(() -> new ClientListRowNote()
                .setList(clientListRepository.getOne(listId))
                .setRowNum(rowNum));
    }

    public void moveContactToAnotherList(long fromListId, long contactRowNum, long toListId) {
        // TODO what should I do with field that doesn't exists in toListId? maybe remove???
        //  currently I don't do anything - just skipping them

        long newRowNum = getMaxRowNum(toListId) + 1; // new row num
        String sql = "update client_list_data cld " +
            "set cld.client_field_id = (select cf_to.id " +
            "                           from client_field cf_from, " +
            "                                client_field cf_to " +
            "                           where cf_from.field_id = cf_to.field_id " +
            "                             and cf_to.list_id = :to_list_id " +
            "                             and cf_from.id = cld.CLIENT_FIELD_ID), " +
            " cld.row_num = :new_row_num " +
            "where cld.row_num = :contact_row_num " +
            "  and cld.client_field_id in (select cf_from.id " +
            "                              from client_field cf_from, " +
            "                                   client_field cf_to " +
            "                              where cf_from.field_id = cf_to.field_id " +
            "                                and cf_from.list_id = :from_list_id " +
            "                                and cf_to.list_id = :to_list_id)";

        ImmutableMap<String, Object> map = ImmutableMap.of(
            "to_list_id", toListId,
            "new_row_num", newRowNum,
            "contact_row_num", contactRowNum,
            "from_list_id", fromListId
        );

        namedParameterJdbcTemplate.update(sql, map);
    }

    class SummaryCache {
        final Supplier<Field> contactTypeFieldSupplier;
        final Supplier<List<ClientField>> clientFieldsSupplier;
        final Supplier<Map<Long, ClientListRowNote>> notePerRowNumSupplier;
        final Supplier<List<CallHistoryDTO>> callHistoriesSupplier;
        final Supplier<Map<Long, List<CallHistoryDTO>>> orgRowNumCallHistoriesSupplier;
        final Supplier<List<CampaignListContactResult>> campaignListContactResultsSupplier;
        final Supplier<Map<Long, CampaignListContactResult>> contactRowNumCampaignListContactResultSupplier;

        SummaryCache(long listId) {
            this.contactTypeFieldSupplier = Suppliers.memoize(ClientListGridService.this::getContactTypeField);
            this.clientFieldsSupplier = Suppliers.memoize(() -> getFields(listId));
            this.notePerRowNumSupplier = Suppliers.memoize(() -> getNotePerRowNum(listId));
            this.callHistoriesSupplier = Suppliers.memoize(() -> getCallHistories(listId));
            this.orgRowNumCallHistoriesSupplier = Suppliers.memoize(() -> {
                HashMap<Long, List<CallHistoryDTO>> map = new HashMap<>();
                for (CallHistoryDTO callHistoryDTO : callHistoriesSupplier.get()) {
                    List<CallHistoryDTO> list = map.computeIfAbsent(callHistoryDTO.getOrgRowNum(), k -> new ArrayList<>());
                    list.add(callHistoryDTO);
                }
                return map;
            });
            this.campaignListContactResultsSupplier = Suppliers.memoize(() -> campaignListContactResultRepository.findByClientListId(listId));
            this.contactRowNumCampaignListContactResultSupplier = Suppliers.memoize(() -> campaignListContactResultsSupplier.get().stream()
                .collect(Collectors.toMap(CampaignListContactResult::getContactRowNum, x -> x)));
        }
    }
}
