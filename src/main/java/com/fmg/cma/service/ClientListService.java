package com.fmg.cma.service;

import com.fmg.cma.domain.ClientField;
import com.fmg.cma.domain.ClientList;
import com.fmg.cma.domain.ClientListData;
import com.fmg.cma.domain.Field;
import com.fmg.cma.repository.ClientFieldRepository;
import com.fmg.cma.repository.ClientListDataRepository;
import com.fmg.cma.repository.ClientListRepository;
import com.fmg.cma.repository.FieldRepository;
import com.fmg.cma.service.dto.ClientListDTO;
import com.fmg.cma.service.dto.ClientListMappingDto;
import com.fmg.cma.service.dto.CsvCell;
import com.fmg.cma.service.dto.NewFieldDto;
import com.fmg.cma.service.dto.NewFieldNoMappingDto;
import com.fmg.cma.service.dto.OldFieldDto;
import com.fmg.cma.service.mapper.ClientListMapper;
import com.opencsv.CSVReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Service Implementation for managing ClientList.
 */
@Service
@Transactional
public class ClientListService {

    private final Logger log = LoggerFactory.getLogger(ClientListService.class);

    private final ClientListRepository clientListRepository;

    private final ClientListMapper clientListMapper;

    private final FieldRepository fieldRepository;

    private final ClientFieldRepository clientFieldRepository;

    private final ClientListDataRepository clientListDataRepository;

    public ClientListService(ClientListRepository clientListRepository,
                             ClientListMapper clientListMapper,
                             FieldRepository fieldRepository,
                             ClientFieldRepository clientFieldRepository,
                             ClientListDataRepository clientListDataRepository) {
        this.clientListRepository = clientListRepository;
        this.clientListMapper = clientListMapper;
        this.fieldRepository = fieldRepository;
        this.clientFieldRepository = clientFieldRepository;
        this.clientListDataRepository = clientListDataRepository;
    }

    /**
     * Save a clientList.
     *
     * @param clientListDTO the entity to save
     * @return the persisted entity
     */
    public ClientListDTO save(ClientListDTO clientListDTO, MultipartFile file) {
        log.debug("Request to save ClientList : {}", clientListDTO);

        List<String[]> lines = Collections.emptyList();
        if (file != null) {
            lines = readAllLines(file);
        }

        ClientList clientList = clientListMapper.toEntity(clientListDTO);
        clientList.setRowCount(lines.size());
        clientList = clientListRepository.save(clientList);

        if (!lines.isEmpty()) {
            Map<CsvCell, ClientField> clientFieldByIndex = new HashMap<>();
            ClientListMappingDto mapping = clientListDTO.getMapping();
            List<NewFieldNoMappingDto> newFieldsNoMapping = mapping.getNewFieldsNoMapping();
            for (NewFieldNoMappingDto newFieldNoMapping : newFieldsNoMapping) {
                Field field = fieldRepository.getByName(newFieldNoMapping.getName());
                if (field == null) {
                    field = new Field()
                        .name(newFieldNoMapping.getName())
                        .mandatory(false);
                    field = fieldRepository.save(field);
                }

                ClientField clientField = new ClientField()
                    .field(field)
                    .list(clientList)
                    .editable(newFieldNoMapping.isEditable())
                    .visible(newFieldNoMapping.isVisible())
                    .setOrgInformation(newFieldNoMapping.isOrgInformation());
                clientField = clientFieldRepository.save(clientField);
            }

            List<NewFieldDto> newFields = mapping.getNewFields();
            for (NewFieldDto newField : newFields) {
                Field field = fieldRepository.getByName((String) newField.getHeader().getValue());
                if (field == null) {
                    field = new Field()
                        .name((String) newField.getHeader().getValue())
                        .mandatory(false);
                    field = fieldRepository.save(field);
                }

                ClientField clientField = new ClientField()
                    .field(field)
                    .list(clientList)
                    .editable(newField.isEditable())
                    .visible(newField.isVisible())
                    .setOrgInformation(newField.isOrgInformation());
                clientField = clientFieldRepository.save(clientField);

                clientFieldByIndex.put(newField.getHeader(), clientField);
            }

            List<OldFieldDto> oldFields = mapping.getOldFields();
            for (OldFieldDto oldField : oldFields) {
                Field field = fieldRepository.getOne(oldField.getField().getId());

                ClientField clientField = new ClientField()
                    .field(field)
                    .list(clientList)
                    .editable(oldField.isEditable())
                    .visible(oldField.isVisible())
                    .setOrgInformation(oldField.isOrgInformation());
                clientField = clientFieldRepository.save(clientField);

                clientFieldByIndex.put(oldField.getHeader(), clientField);
            }

            List<CsvCell> orderedHeaders = clientFieldByIndex.keySet().stream()
                .sorted(Comparator.comparingInt(CsvCell::getIndex))
                .collect(Collectors.toList());

            long i = 0;
            for (String[] line : lines) {
                for (CsvCell header : orderedHeaders) {
                    try {
                        String value = line[header.getIndex()];
                        ClientField clientField = clientFieldByIndex.get(header);

                        ClientListData clientListData = new ClientListData()
                            .rowNum(i)
                            .clientField(clientField)
                            .cvalue(value);

                        clientListDataRepository.save(clientListData);
                    } catch (IndexOutOfBoundsException e) {
                        String message = "Could not get \"" + header.getValue() + "\" " +
                            "at index " + header.getIndex() + ". Total length is " + line.length;
                        throw new RuntimeException(message, e);
                    }
                }
                i++;
            }
        }

        return clientListMapper.toDto(clientList);
    }

    private List<String[]> readAllLines(MultipartFile file) {
        try (Reader reader = new InputStreamReader(file.getInputStream())) {
            try (CSVReader csvReader = new CSVReader(reader)) {
                // skip first line with headers
                csvReader.readNext();

                return csvReader.readAll();
            }
        } catch (IOException e) {
            throw new RuntimeException("Could not read file", e);
        }
    }

    /**
     * Get all the clientLists.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ClientListDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ClientLists");
        return clientListRepository.findAll(pageable)
            .map(clientListMapper::toDto);
    }

    public Page<ClientListDTO> getClientCampaignClientLists(long clientCampaignId, Pageable pageable) {
        log.debug("Request to get ClientLists by ClientCampaign {}", clientCampaignId);
        return clientListRepository.findByClientCampaignId(clientCampaignId, pageable)
            .map(clientListMapper::toDto);
    }

    /**
     * Get one clientList by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ClientListDTO findOne(Long id) {
        log.debug("Request to get ClientList : {}", id);
        ClientList clientList = clientListRepository.findOne(id);
        return clientListMapper.toDto(clientList);
    }

    /**
     * Delete the clientList by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ClientList : {}", id);
        clientListRepository.delete(id);
    }
}
