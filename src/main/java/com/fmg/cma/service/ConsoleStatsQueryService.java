package com.fmg.cma.service;

import com.fmg.cma.domain.ConsoleStats;
import com.fmg.cma.domain.ConsoleStats_;
import com.fmg.cma.domain.User;
import com.fmg.cma.domain.User_;
import com.fmg.cma.repository.ConsoleStatsRepository;
import com.fmg.cma.repository.UserRepository;
import com.fmg.cma.security.SecurityUtils;
import com.fmg.cma.service.dto.ConsoleStatsCriteria;
import com.fmg.cma.service.dto.ConsoleStatsDTO;
import com.fmg.cma.service.mapper.ConsoleStatsMapper;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

/**
 * Service for executing complex queries for ConsoleStats entities in the database.
 * The main input is a {@link ConsoleStatsCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ConsoleStatsDTO} or a {@link Page} of {@link ConsoleStatsDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ConsoleStatsQueryService extends QueryService<ConsoleStats> {

    private final Logger log = LoggerFactory.getLogger(ConsoleStatsQueryService.class);


    private final ConsoleStatsRepository consoleStatsRepository;

    private final ConsoleStatsMapper consoleStatsMapper;

    public ConsoleStatsQueryService(ConsoleStatsRepository consoleStatsRepository,
                                    ConsoleStatsMapper consoleStatsMapper) {
        this.consoleStatsRepository = consoleStatsRepository;
        this.consoleStatsMapper = consoleStatsMapper;
    }

    /**
     * Return a {@link List} of {@link ConsoleStatsDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ConsoleStatsDTO> findByCriteria(ConsoleStatsCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<ConsoleStats> specification = createSpecification(criteria);
        return consoleStatsMapper.toDto(consoleStatsRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ConsoleStatsDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ConsoleStatsDTO> findByCriteria(ConsoleStatsCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<ConsoleStats> specification = createSpecification(criteria);
        final Page<ConsoleStats> result = consoleStatsRepository.findAll(specification, page);
        return result.map(consoleStatsMapper::toDto);
    }

    /**
     * Function to convert ConsoleStatsCriteria to a {@link Specifications}
     */
    private Specifications<ConsoleStats> createSpecification(ConsoleStatsCriteria criteria) {
        Specifications<ConsoleStats> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ConsoleStats_.id));
            }
            if (criteria.getCalls() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCalls(), ConsoleStats_.calls));
            }
            if (criteria.getSkips() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSkips(), ConsoleStats_.skips));
            }
            if (criteria.getPending() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPending(), ConsoleStats_.pending));
            }
            if (criteria.getStamp() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStamp(), ConsoleStats_.stamp));
            }
            if (criteria.getDmContact() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDmContact(), ConsoleStats_.dmContact));
            }
            if (criteria.getLeadSales() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLeadSales(), ConsoleStats_.leadSales));
            }
            if (criteria.getPipelineOpps() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPipelineOpps(), ConsoleStats_.pipelineOpps));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getUserId(), ConsoleStats_.user, User_.id));
            }
        }
        return specification;
    }
}
