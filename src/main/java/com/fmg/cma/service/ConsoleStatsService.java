package com.fmg.cma.service;

import com.fmg.cma.domain.ConsoleStats;
import com.fmg.cma.domain.User;
import com.fmg.cma.repository.ConsoleStatsRepository;
import com.fmg.cma.repository.UserRepository;
import com.fmg.cma.security.SecurityUtils;
import com.fmg.cma.service.dto.ConsoleStatsDTO;
import com.fmg.cma.service.mapper.ConsoleStatsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.function.Function;


/**
 * Service Implementation for managing ConsoleStats.
 */
@Service
@Transactional
public class ConsoleStatsService {

    private final Logger log = LoggerFactory.getLogger(ConsoleStatsService.class);

    private final ConsoleStatsRepository consoleStatsRepository;

    private final ConsoleStatsMapper consoleStatsMapper;

    private final UserRepository userRepository;

    public ConsoleStatsService(ConsoleStatsRepository consoleStatsRepository,
                               ConsoleStatsMapper consoleStatsMapper,
                               UserRepository userRepository) {
        this.consoleStatsRepository = consoleStatsRepository;
        this.consoleStatsMapper = consoleStatsMapper;
        this.userRepository = userRepository;
    }

    /**
     * Save a consoleStats.
     *
     * @param consoleStatsDTO the entity to save
     * @return the persisted entity
     */
    public ConsoleStatsDTO save(ConsoleStatsDTO consoleStatsDTO) {
        log.debug("Request to save ConsoleStats : {}", consoleStatsDTO);
        ConsoleStats consoleStats = consoleStatsMapper.toEntity(consoleStatsDTO);
        consoleStats = consoleStatsRepository.save(consoleStats);
        return consoleStatsMapper.toDto(consoleStats);
    }

    /**
     * Get all the consoleStats.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ConsoleStatsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ConsoleStats");
        return consoleStatsRepository.findAll(pageable)
            .map(consoleStatsMapper::toDto);
    }

    /**
     * Get one consoleStats by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ConsoleStatsDTO findOne(Long id) {
        log.debug("Request to get ConsoleStats : {}", id);
        ConsoleStats consoleStats = consoleStatsRepository.findOne(id);
        return consoleStatsMapper.toDto(consoleStats);
    }

    /**
     * Delete the consoleStats by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ConsoleStats : {}", id);
        consoleStatsRepository.delete(id);
    }

    public ConsoleStatsDTO getByUserIsCurrentUserAndStampIsCurrentDate() {
        ConsoleStats stats = consoleStatsRepository.getByUserIsCurrentUser(LocalDate.now());
        if (stats == null) {
            stats = modifyAndSave(consoleStats -> consoleStats);
        }
        return consoleStatsMapper.toDto(stats);
    }

    public ConsoleStats modifyAndSave(Function<ConsoleStats, ConsoleStats> modifier) {
        User currentUser = userRepository.getByLogin(SecurityUtils.getCurrentUserLogin());
        synchronized (currentUser.getId()) {
            LocalDate now = LocalDate.now();
            ConsoleStats stats = consoleStatsRepository.getByUserIsCurrentUser(now);
            if (stats == null) {
                stats = new ConsoleStats()
                    .setUser(currentUser)
                    .setStamp(now);
            }

            stats = modifier.apply(stats);
            return consoleStatsRepository.save(stats);
        }
    }

    /**
     * @deprecated use {@link #applyChanges(ConsoleStats)}.
     */
    public ConsoleStats incrementSkips() {
        return modifyAndSave(ConsoleStats::incrementSkips);
    }

    /**
     * @deprecated use {@link #applyChanges(ConsoleStats)}.
     */
    public ConsoleStats incrementCalls() {
        return modifyAndSave(ConsoleStats::incrementCalls);
    }

    /**
     * @deprecated use {@link #applyChanges(ConsoleStats)}.
     */
    public ConsoleStats incrementPipelineOpps() {
        return modifyAndSave(ConsoleStats::incrementPipelineOpps);
    }

    /**
     * @deprecated use {@link #applyChanges(ConsoleStats)}.
     */
    public ConsoleStats incrementDMContact() {
        return modifyAndSave(ConsoleStats::incrementDMContact);
    }

    public ConsoleStats applyChanges(ConsoleStats changes) {
        return modifyAndSave(consoleStats -> {
            consoleStats.setSkips(consoleStats.getSkips() + changes.getSkips());
            consoleStats.setCalls(consoleStats.getCalls() + changes.getCalls());
            consoleStats.setPipelineOpps(consoleStats.getPipelineOpps() + changes.getPipelineOpps());
            consoleStats.setDmContact(consoleStats.getDmContact() + changes.getDmContact());
            consoleStats.setPending(consoleStats.getPending() + changes.getPending());
            return consoleStats;
        });
    }
}
