package com.fmg.cma.service;


import com.fmg.cma.domain.EmailTemplate;
import com.fmg.cma.domain.EmailTemplate_;
import com.fmg.cma.repository.EmailTemplateRepository;
import com.fmg.cma.service.dto.EmailTemplateCriteria;
import com.fmg.cma.service.dto.EmailTemplateDTO;
import com.fmg.cma.service.mapper.EmailTemplateMapper;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for EmailTemplate entities in the database.
 * The main input is a {@link EmailTemplateCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EmailTemplateDTO} or a {@link Page} of {@link EmailTemplateDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EmailTemplateQueryService extends QueryService<EmailTemplate> {

    private final Logger log = LoggerFactory.getLogger(EmailTemplateQueryService.class);


    private final EmailTemplateRepository emailTemplateRepository;

    private final EmailTemplateMapper emailTemplateMapper;

    public EmailTemplateQueryService(EmailTemplateRepository emailTemplateRepository, EmailTemplateMapper emailTemplateMapper) {
        this.emailTemplateRepository = emailTemplateRepository;
        this.emailTemplateMapper = emailTemplateMapper;
    }

    /**
     * Return a {@link List} of {@link EmailTemplateDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EmailTemplateDTO> findByCriteria(EmailTemplateCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<EmailTemplate> specification = createSpecification(criteria);
        return emailTemplateMapper.toDto(emailTemplateRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link EmailTemplateDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EmailTemplateDTO> findByCriteria(EmailTemplateCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<EmailTemplate> specification = createSpecification(criteria);
        final Page<EmailTemplate> result = emailTemplateRepository.findAll(specification, page);
        return result.map(emailTemplateMapper::toDto);
    }

    /**
     * Function to convert EmailTemplateCriteria to a {@link Specifications}
     */
    private Specifications<EmailTemplate> createSpecification(EmailTemplateCriteria criteria) {
        Specifications<EmailTemplate> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), EmailTemplate_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), EmailTemplate_.name));
            }
            if (criteria.getFrom() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFrom(), EmailTemplate_.from));
            }
            if (criteria.getSubject() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubject(), EmailTemplate_.subject));
            }
            if (criteria.getCc() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCc(), EmailTemplate_.cc));
            }
            if (criteria.getBcc() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBcc(), EmailTemplate_.bcc));
            }
            if (criteria.getReplyTo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getReplyTo(), EmailTemplate_.replyTo));
            }
            if (criteria.getBody() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBody(), EmailTemplate_.body));
            }
        }
        return specification;
    }
}
