package com.fmg.cma.service;

import com.fmg.cma.domain.EmailTemplate;
import com.fmg.cma.repository.EmailTemplateRepository;
import com.fmg.cma.service.dto.EmailTemplateDTO;
import com.fmg.cma.service.mapper.EmailTemplateMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing EmailTemplate.
 */
@Service
@Transactional
public class EmailTemplateService {

    private final Logger log = LoggerFactory.getLogger(EmailTemplateService.class);

    private final EmailTemplateRepository emailTemplateRepository;

    private final EmailTemplateMapper emailTemplateMapper;

    public EmailTemplateService(EmailTemplateRepository emailTemplateRepository, EmailTemplateMapper emailTemplateMapper) {
        this.emailTemplateRepository = emailTemplateRepository;
        this.emailTemplateMapper = emailTemplateMapper;
    }

    /**
     * Save a emailTemplate.
     *
     * @param emailTemplateDTO the entity to save
     * @return the persisted entity
     */
    public EmailTemplateDTO save(EmailTemplateDTO emailTemplateDTO) {
        log.debug("Request to save EmailTemplate : {}", emailTemplateDTO);
        EmailTemplate emailTemplate = emailTemplateMapper.toEntity(emailTemplateDTO);
        emailTemplate = emailTemplateRepository.save(emailTemplate);
        return emailTemplateMapper.toDto(emailTemplate);
    }

    /**
     * Get all the emailTemplates.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<EmailTemplateDTO> findAll(Pageable pageable) {
        log.debug("Request to get all EmailTemplates");
        return emailTemplateRepository.findAll(pageable)
            .map(emailTemplateMapper::toDto);
    }

    /**
     * Get one emailTemplate by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public EmailTemplateDTO findOne(Long id) {
        log.debug("Request to get EmailTemplate : {}", id);
        EmailTemplate emailTemplate = emailTemplateRepository.findOne(id);
        return emailTemplateMapper.toDto(emailTemplate);
    }

    /**
     * Delete the emailTemplate by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete EmailTemplate : {}", id);
        emailTemplateRepository.delete(id);
    }
}
