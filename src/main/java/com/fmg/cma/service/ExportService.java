package com.fmg.cma.service;

import com.fmg.cma.domain.MyPipeline;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.export.SimpleCsvExporterConfiguration;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.*;

@Service
public class ExportService {

    public byte[] exportPdf() throws JRException {
        InputStream employeeReportStream = getClass().getResourceAsStream("/templates/my-pipeline.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(employeeReportStream);

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("totalAmount", "$ 1324123");

        List<MyPipeline> documents = Arrays.asList(new MyPipeline(new Date(), "Just Do iT !", "List A", "Microsoft", "Bob Fish", "London", new Date(), "Loved the product we check that and is awesome for all !", "DM Contacted", "High"));

        JRBeanCollectionDataSource listDataSource = new JRBeanCollectionDataSource(documents, false);

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, listDataSource);

        return JasperExportManager.exportReportToPdf(jasperPrint);
    }

    public byte[] exportCsv() throws JRException {
        InputStream employeeReportStream = getClass().getResourceAsStream("/templates/my-pipeline-csv.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(employeeReportStream);
        JRCsvExporter exporter = new JRCsvExporter();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("totalAmount", "$ 1324123");

        List<MyPipeline> documents = Arrays.asList(new MyPipeline(new Date(), "Fancy a Chat", "Regional Office", "Sainsbury", "Rey Hedge", "Edinburgh", new Date(), "Checking budget I deposit the amount pending.", "Hot Lead", "Low"));

        JRBeanCollectionDataSource listDataSource = new JRBeanCollectionDataSource(documents, false);

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, listDataSource);
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleWriterExporterOutput(bos));
        SimpleCsvExporterConfiguration configuration = new SimpleCsvExporterConfiguration();
        configuration.setWriteBOM(Boolean.TRUE);
        configuration.setRecordDelimiter("\r\n");
        configuration.setFieldDelimiter(";");
        exporter.setConfiguration(configuration);
        exporter.exportReport();

        return bos.toByteArray();
    }

}
