package com.fmg.cma.service;

import com.fmg.cma.service.dto.CsvCell;
import com.opencsv.CSVReader;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.fmg.cma.service.util.FileUtils.removeBom;

@Service
public class FileService {

    public List<CsvCell> readLine(MultipartFile file) {
        try (Reader reader = new InputStreamReader(file.getInputStream())) {
            try (CSVReader csvReader = new CSVReader(reader)) {
                String[] strings = csvReader.readNext();
                removeBom(strings);

                return IntStream.range(0, strings.length)
                    .mapToObj(i -> new CsvCell(i, strings[i]))
                    .collect(Collectors.toList());
            }
        } catch (IOException e) {
            throw new RuntimeException("Could not read line", e);
        }
    }

}
