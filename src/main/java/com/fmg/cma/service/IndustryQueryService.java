package com.fmg.cma.service;


import com.fmg.cma.domain.Industry;
import com.fmg.cma.domain.Industry_;
import com.fmg.cma.repository.IndustryRepository;
import com.fmg.cma.service.dto.IndustryCriteria;
import com.fmg.cma.service.dto.IndustryDTO;
import com.fmg.cma.service.mapper.IndustryMapper;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for Industry entities in the database.
 * The main input is a {@link IndustryCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link IndustryDTO} or a {@link Page} of {@link IndustryDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class IndustryQueryService extends QueryService<Industry> {

    private final Logger log = LoggerFactory.getLogger(IndustryQueryService.class);


    private final IndustryRepository industryRepository;

    private final IndustryMapper industryMapper;

    public IndustryQueryService(IndustryRepository industryRepository, IndustryMapper industryMapper) {
        this.industryRepository = industryRepository;
        this.industryMapper = industryMapper;
    }

    /**
     * Return a {@link List} of {@link IndustryDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<IndustryDTO> findByCriteria(IndustryCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Industry> specification = createSpecification(criteria);
        return industryMapper.toDto(industryRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link IndustryDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<IndustryDTO> findByCriteria(IndustryCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Industry> specification = createSpecification(criteria);
        final Page<Industry> result = industryRepository.findAll(specification, page);
        return result.map(industryMapper::toDto);
    }

    /**
     * Function to convert IndustryCriteria to a {@link Specifications}
     */
    private Specifications<Industry> createSpecification(IndustryCriteria criteria) {
        Specifications<Industry> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Industry_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Industry_.name));
            }
        }
        return specification;
    }

}
