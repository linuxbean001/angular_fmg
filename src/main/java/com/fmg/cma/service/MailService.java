package com.fmg.cma.service;

import com.fmg.cma.domain.User;
import io.github.jhipster.config.JHipsterProperties;
import org.apache.commons.lang3.CharEncoding;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import javax.mail.internet.MimeMessage;
import java.util.Locale;

/**
 * Service for sending emails.
 * <p>
 * We use the @Async annotation to send emails asynchronously.
 */
@Service
public class MailService {

    private final Logger log = LoggerFactory.getLogger(MailService.class);

    private static final String USER = "user";

    private static final String CAMPAIGN = "campaign";

    private static final String BASE_URL = "baseUrl";

    private final JHipsterProperties jHipsterProperties;

    private final JavaMailSender javaMailSender;

    private final MessageSource messageSource;

    private final SpringTemplateEngine templateEngine;

    public MailService(JHipsterProperties jHipsterProperties, JavaMailSender javaMailSender,
                       MessageSource messageSource, SpringTemplateEngine templateEngine) {

        this.jHipsterProperties = jHipsterProperties;
        this.javaMailSender = javaMailSender;
        this.messageSource = messageSource;
        this.templateEngine = templateEngine;
    }

    @Async
    public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml,
                          SendMailOptions options) {
        log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
            isMultipart, isHtml, to, subject, content);

        // Prepare message using a Spring helper
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
            message.setTo(to);
            message.setFrom(jHipsterProperties.getMail().getFrom());
            message.setSubject(subject);
            message.setText(content, isHtml);

            if (options != null) {
                if (options.getBcc() != null) {
                    message.setBcc(options.getBcc());
                }
            }

            javaMailSender.send(mimeMessage);
            log.debug("Sent email to User '{}'", to);
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.warn("Email could not be sent to user '{}'", to, e);
            } else {
                log.warn("Email could not be sent to user '{}': {}", to, e.getMessage());
            }
        }
    }

    @Async
    public void sendEmailFromTemplate(User user, String aCampaignName, String templateName, String titleKey) {
        sendEmailFromTemplate(user, aCampaignName, templateName, titleKey, null);
    }

    @Async
    public void sendEmailFromTemplate(User user, String aCampaignName, String templateName, String titleKey, SendMailOptions options) {
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        if (StringUtils.isNotBlank(aCampaignName)) {
            log.info("Set campaign variable {}", aCampaignName);
            context.setVariable(CAMPAIGN, aCampaignName);
        }
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process(templateName, context);
        String subject = messageSource.getMessage(titleKey, null, locale);
        sendEmail(user.getEmail(), subject, content, false, true, options);
    }

    @Async
    public void sendActivationEmail(User user) {
        log.debug("Sending activation email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, null, "activationEmail", "email.activation.title");
    }

    @Async
    public void sendCreationEmail(User user) {
        log.debug("Sending creation email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, null, "creationEmail", "email.activation.title");
    }

    @Async
    public void sendPasswordResetMail(User user) {
        sendPasswordResetMail(user, null);
    }

    @Async
    public void sendPasswordResetMail(User user, SendMailOptions options) {
        log.debug("Sending password reset email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, null, "passwordResetEmail", "email.reset.title", options);
    }

    @Async
    public void sendNewPortalAccountCreatedMail(User user, String aCampaignName, SendMailOptions options) {
        log.debug("Sending password reset email to new portal account '{}' for Campaign '{}'", user.getEmail(), aCampaignName);
        sendEmailFromTemplate(user, aCampaignName, "portalAccountCreated", "email.portal_account_created.title", options);
    }


    @Async
    public void sendExistingPortalUserAssignedToCampaign(User aUser, String aCampaignName, SendMailOptions aOptions) {
        log.debug("Sending email to existing user {} they have been assigned to a campaign {}", aUser.getEmail(), aCampaignName);
        sendEmailFromTemplate(aUser, aCampaignName, "portalAccountAssignedToCampaign", "email.portal_account_assigned.title", aOptions);
    }
}
