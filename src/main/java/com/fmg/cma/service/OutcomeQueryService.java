package com.fmg.cma.service;


import com.fmg.cma.domain.ClientCampaignTemplate_;
import com.fmg.cma.domain.Outcome;
import com.fmg.cma.domain.OutcomeSubReason_;
import com.fmg.cma.domain.Outcome_;
import com.fmg.cma.repository.OutcomeRepository;
import com.fmg.cma.service.dto.OutcomeCriteria;
import com.fmg.cma.service.dto.OutcomeDTO;
import com.fmg.cma.service.mapper.OutcomeMapper;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for Outcome entities in the database.
 * The main input is a {@link OutcomeCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link OutcomeDTO} or a {@link Page} of {@link OutcomeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class OutcomeQueryService extends QueryService<Outcome> {

    private final Logger log = LoggerFactory.getLogger(OutcomeQueryService.class);


    private final OutcomeRepository outcomeRepository;

    private final OutcomeMapper outcomeMapper;

    public OutcomeQueryService(OutcomeRepository outcomeRepository, OutcomeMapper outcomeMapper) {
        this.outcomeRepository = outcomeRepository;
        this.outcomeMapper = outcomeMapper;
    }

    /**
     * Return a {@link List} of {@link OutcomeDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<OutcomeDTO> findByCriteria(OutcomeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Outcome> specification = createSpecification(criteria);
        return outcomeMapper.toDto(outcomeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link OutcomeDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<OutcomeDTO> findByCriteria(OutcomeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Outcome> specification = createSpecification(criteria);
        final Page<Outcome> result = outcomeRepository.findAll(specification, page);
        return result.map(outcomeMapper::toDto);
    }

    /**
     * Function to convert OutcomeCriteria to a {@link Specifications}
     */
    private Specifications<Outcome> createSpecification(OutcomeCriteria criteria) {
        Specifications<Outcome> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Outcome_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Outcome_.name));
            }
            if (criteria.getTemplateId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getTemplateId(), Outcome_.template, ClientCampaignTemplate_.id));
            }
            if (criteria.getOutcomeSubReasonId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getOutcomeSubReasonId(), Outcome_.outcomeSubReason, OutcomeSubReason_.id));
            }
        }
        return specification;
    }

}
