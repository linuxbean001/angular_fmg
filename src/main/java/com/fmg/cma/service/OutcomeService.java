package com.fmg.cma.service;

import com.fmg.cma.domain.Outcome;
import com.fmg.cma.repository.OutcomeRepository;
import com.fmg.cma.service.dto.OutcomeDTO;
import com.fmg.cma.service.mapper.OutcomeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Outcome.
 */
@Service
@Transactional
public class OutcomeService {

    private final Logger log = LoggerFactory.getLogger(OutcomeService.class);

    private final OutcomeRepository outcomeRepository;

    private final OutcomeMapper outcomeMapper;

    public OutcomeService(OutcomeRepository outcomeRepository, OutcomeMapper outcomeMapper) {
        this.outcomeRepository = outcomeRepository;
        this.outcomeMapper = outcomeMapper;
    }

    /**
     * Save a outcome.
     *
     * @param outcomeDTO the entity to save
     * @return the persisted entity
     */
    public OutcomeDTO save(OutcomeDTO outcomeDTO) {
        log.debug("Request to save Outcome : {}", outcomeDTO);
        Outcome outcome = outcomeMapper.toEntity(outcomeDTO);

        boolean isNew = outcome.getId() == null;
        outcome = outcomeRepository.save(outcome);
        if (isNew) {
            // re-save to fill order equals to id
            outcome.setOrder(outcome.getId());
            outcome = outcomeRepository.save(outcome);
        }

        return outcomeMapper.toDto(outcome);
    }

    /**
     * Get all the outcomes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<OutcomeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Outcomes");
        return outcomeRepository.findAll(pageable)
            .map(outcomeMapper::toDto);
    }

    /**
     * Get one outcome by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public OutcomeDTO findOne(Long id) {
        log.debug("Request to get Outcome : {}", id);
        Outcome outcome = outcomeRepository.findOne(id);
        return outcomeMapper.toDto(outcome);
    }

    /**
     * Delete the outcome by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Outcome : {}", id);
        outcomeRepository.delete(id);
    }
}
