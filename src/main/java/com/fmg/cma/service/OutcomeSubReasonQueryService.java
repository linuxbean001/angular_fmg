package com.fmg.cma.service;


import com.fmg.cma.domain.OutcomeSubReason;
import com.fmg.cma.domain.OutcomeSubReason_;
import com.fmg.cma.repository.OutcomeSubReasonRepository;
import com.fmg.cma.service.dto.OutcomeSubReasonCriteria;
import com.fmg.cma.service.dto.OutcomeSubReasonDTO;
import com.fmg.cma.service.mapper.OutcomeSubReasonMapper;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for OutcomeSubReason entities in the database.
 * The main input is a {@link OutcomeSubReasonCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link OutcomeSubReasonDTO} or a {@link Page} of {@link OutcomeSubReasonDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class OutcomeSubReasonQueryService extends QueryService<OutcomeSubReason> {

    private final Logger log = LoggerFactory.getLogger(OutcomeSubReasonQueryService.class);


    private final OutcomeSubReasonRepository outcomeSubReasonRepository;

    private final OutcomeSubReasonMapper outcomeSubReasonMapper;

    public OutcomeSubReasonQueryService(OutcomeSubReasonRepository outcomeSubReasonRepository, OutcomeSubReasonMapper outcomeSubReasonMapper) {
        this.outcomeSubReasonRepository = outcomeSubReasonRepository;
        this.outcomeSubReasonMapper = outcomeSubReasonMapper;
    }

    /**
     * Return a {@link List} of {@link OutcomeSubReasonDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<OutcomeSubReasonDTO> findByCriteria(OutcomeSubReasonCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<OutcomeSubReason> specification = createSpecification(criteria);
        return outcomeSubReasonMapper.toDto(outcomeSubReasonRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link OutcomeSubReasonDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<OutcomeSubReasonDTO> findByCriteria(OutcomeSubReasonCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<OutcomeSubReason> specification = createSpecification(criteria);
        final Page<OutcomeSubReason> result = outcomeSubReasonRepository.findAll(specification, page);
        return result.map(outcomeSubReasonMapper::toDto);
    }

    /**
     * Function to convert OutcomeSubReasonCriteria to a {@link Specifications}
     */
    private Specifications<OutcomeSubReason> createSpecification(OutcomeSubReasonCriteria criteria) {
        Specifications<OutcomeSubReason> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), OutcomeSubReason_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), OutcomeSubReason_.name));
            }
        }
        return specification;
    }

}
