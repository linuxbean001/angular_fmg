package com.fmg.cma.service;

import com.fmg.cma.domain.OutcomeSubReason;
import com.fmg.cma.repository.OutcomeSubReasonRepository;
import com.fmg.cma.service.dto.OutcomeSubReasonDTO;
import com.fmg.cma.service.mapper.OutcomeSubReasonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing OutcomeSubReason.
 */
@Service
@Transactional
public class OutcomeSubReasonService {

    private final Logger log = LoggerFactory.getLogger(OutcomeSubReasonService.class);

    private final OutcomeSubReasonRepository outcomeSubReasonRepository;

    private final OutcomeSubReasonMapper outcomeSubReasonMapper;

    public OutcomeSubReasonService(OutcomeSubReasonRepository outcomeSubReasonRepository, OutcomeSubReasonMapper outcomeSubReasonMapper) {
        this.outcomeSubReasonRepository = outcomeSubReasonRepository;
        this.outcomeSubReasonMapper = outcomeSubReasonMapper;
    }

    /**
     * Save a outcomeSubReason.
     *
     * @param outcomeSubReasonDTO the entity to save
     * @return the persisted entity
     */
    public OutcomeSubReasonDTO save(OutcomeSubReasonDTO outcomeSubReasonDTO) {
        log.debug("Request to save OutcomeSubReason : {}", outcomeSubReasonDTO);
        OutcomeSubReason outcomeSubReason = outcomeSubReasonMapper.toEntity(outcomeSubReasonDTO);
        outcomeSubReason = outcomeSubReasonRepository.save(outcomeSubReason);
        return outcomeSubReasonMapper.toDto(outcomeSubReason);
    }

    /**
     * Get all the outcomeSubReasons.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<OutcomeSubReasonDTO> findAll(Pageable pageable) {
        log.debug("Request to get all OutcomeSubReasons");
        return outcomeSubReasonRepository.findAll(pageable)
            .map(outcomeSubReasonMapper::toDto);
    }

    /**
     * Get one outcomeSubReason by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public OutcomeSubReasonDTO findOne(Long id) {
        log.debug("Request to get OutcomeSubReason : {}", id);
        OutcomeSubReason outcomeSubReason = outcomeSubReasonRepository.findOne(id);
        return outcomeSubReasonMapper.toDto(outcomeSubReason);
    }

    /**
     * Delete the outcomeSubReason by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete OutcomeSubReason : {}", id);
        outcomeSubReasonRepository.delete(id);
    }
}
