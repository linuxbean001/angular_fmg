package com.fmg.cma.service;


import com.fmg.cma.domain.OutcomeType;
import com.fmg.cma.domain.OutcomeType_;
import com.fmg.cma.repository.OutcomeTypeRepository;
import com.fmg.cma.service.dto.OutcomeTypeCriteria;
import com.fmg.cma.service.dto.OutcomeTypeDTO;
import com.fmg.cma.service.mapper.OutcomeTypeMapper;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for OutcomeType entities in the database.
 * The main input is a {@link OutcomeTypeCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link OutcomeTypeDTO} or a {@link Page} of {@link OutcomeTypeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class OutcomeTypeQueryService extends QueryService<OutcomeType> {

    private final Logger log = LoggerFactory.getLogger(OutcomeTypeQueryService.class);


    private final OutcomeTypeRepository outcomeTypeRepository;

    private final OutcomeTypeMapper outcomeTypeMapper;

    public OutcomeTypeQueryService(OutcomeTypeRepository outcomeTypeRepository, OutcomeTypeMapper outcomeTypeMapper) {
        this.outcomeTypeRepository = outcomeTypeRepository;
        this.outcomeTypeMapper = outcomeTypeMapper;
    }

    /**
     * Return a {@link List} of {@link OutcomeTypeDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<OutcomeTypeDTO> findByCriteria(OutcomeTypeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<OutcomeType> specification = createSpecification(criteria);
        return outcomeTypeMapper.toDto(outcomeTypeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link OutcomeTypeDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<OutcomeTypeDTO> findByCriteria(OutcomeTypeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<OutcomeType> specification = createSpecification(criteria);
        final Page<OutcomeType> result = outcomeTypeRepository.findAll(specification, page);
        return result.map(outcomeTypeMapper::toDto);
    }

    /**
     * Function to convert OutcomeTypeCriteria to a {@link Specifications}
     */
    private Specifications<OutcomeType> createSpecification(OutcomeTypeCriteria criteria) {
        Specifications<OutcomeType> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), OutcomeType_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), OutcomeType_.name));
            }
        }
        return specification;
    }

}
