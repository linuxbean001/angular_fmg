package com.fmg.cma.service;

import com.fmg.cma.domain.OutcomeType;
import com.fmg.cma.repository.OutcomeTypeRepository;
import com.fmg.cma.service.dto.OutcomeTypeDTO;
import com.fmg.cma.service.mapper.OutcomeTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing OutcomeType.
 */
@Service
@Transactional
public class OutcomeTypeService {

    private final Logger log = LoggerFactory.getLogger(OutcomeTypeService.class);

    private final OutcomeTypeRepository outcomeTypeRepository;

    private final OutcomeTypeMapper outcomeTypeMapper;

    public OutcomeTypeService(OutcomeTypeRepository outcomeTypeRepository, OutcomeTypeMapper outcomeTypeMapper) {
        this.outcomeTypeRepository = outcomeTypeRepository;
        this.outcomeTypeMapper = outcomeTypeMapper;
    }

    /**
     * Save a outcomeType.
     *
     * @param outcomeTypeDTO the entity to save
     * @return the persisted entity
     */
    public OutcomeTypeDTO save(OutcomeTypeDTO outcomeTypeDTO) {
        log.debug("Request to save OutcomeType : {}", outcomeTypeDTO);
        OutcomeType outcomeType = outcomeTypeMapper.toEntity(outcomeTypeDTO);
        outcomeType = outcomeTypeRepository.save(outcomeType);
        return outcomeTypeMapper.toDto(outcomeType);
    }

    /**
     * Get all the outcomeTypes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<OutcomeTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all OutcomeTypes");
        return outcomeTypeRepository.findAll(pageable)
            .map(outcomeTypeMapper::toDto);
    }

    /**
     * Get one outcomeType by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public OutcomeTypeDTO findOne(Long id) {
        log.debug("Request to get OutcomeType : {}", id);
        OutcomeType outcomeType = outcomeTypeRepository.findOne(id);
        return outcomeTypeMapper.toDto(outcomeType);
    }

    /**
     * Delete the outcomeType by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete OutcomeType : {}", id);
        outcomeTypeRepository.delete(id);
    }
}
