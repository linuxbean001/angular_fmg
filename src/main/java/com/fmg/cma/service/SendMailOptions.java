package com.fmg.cma.service;

public class SendMailOptions {

    private String bcc;

    public String getBcc() {
        return bcc;
    }

    public SendMailOptions setBcc(String bcc) {
        this.bcc = bcc;
        return this;
    }
}
