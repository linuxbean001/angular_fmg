package com.fmg.cma.service;

import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManagerFactory;

@Service
public class SessionFactoryService {

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    private JpaProperties jpaProperties;

    public SessionFactoryImplementor getSessionFactoryImplementor() {
        return entityManagerFactory.unwrap(SessionFactoryImplementor.class);
    }

    public String getSchemaName() {
        return jpaProperties.getProperties().get("hibernate.default_schema");
    }
}
