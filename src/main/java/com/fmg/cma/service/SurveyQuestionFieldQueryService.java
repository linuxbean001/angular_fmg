package com.fmg.cma.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.fmg.cma.domain.SurveyQuestionField;
import com.fmg.cma.domain.*; // for static metamodels
import com.fmg.cma.repository.SurveyQuestionFieldRepository;
import com.fmg.cma.service.dto.SurveyQuestionFieldCriteria;

import com.fmg.cma.service.dto.SurveyQuestionFieldDTO;
import com.fmg.cma.service.mapper.SurveyQuestionFieldMapper;

/**
 * Service for executing complex queries for SurveyQuestionField entities in the database.
 * The main input is a {@link SurveyQuestionFieldCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SurveyQuestionFieldDTO} or a {@link Page} of {@link SurveyQuestionFieldDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SurveyQuestionFieldQueryService extends QueryService<SurveyQuestionField> {

    private final Logger log = LoggerFactory.getLogger(SurveyQuestionFieldQueryService.class);


    private final SurveyQuestionFieldRepository surveyQuestionFieldRepository;

    private final SurveyQuestionFieldMapper surveyQuestionFieldMapper;

    public SurveyQuestionFieldQueryService(SurveyQuestionFieldRepository surveyQuestionFieldRepository, SurveyQuestionFieldMapper surveyQuestionFieldMapper) {
        this.surveyQuestionFieldRepository = surveyQuestionFieldRepository;
        this.surveyQuestionFieldMapper = surveyQuestionFieldMapper;
    }

    /**
     * Return a {@link List} of {@link SurveyQuestionFieldDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SurveyQuestionFieldDTO> findByCriteria(SurveyQuestionFieldCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<SurveyQuestionField> specification = createSpecification(criteria);
        return surveyQuestionFieldMapper.toDto(surveyQuestionFieldRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link SurveyQuestionFieldDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SurveyQuestionFieldDTO> findByCriteria(SurveyQuestionFieldCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<SurveyQuestionField> specification = createSpecification(criteria);
        final Page<SurveyQuestionField> result = surveyQuestionFieldRepository.findAll(specification, page);
        return result.map(surveyQuestionFieldMapper::toDto);
    }

    /**
     * Function to convert SurveyQuestionFieldCriteria to a {@link Specifications}
     */
    private Specifications<SurveyQuestionField> createSpecification(SurveyQuestionFieldCriteria criteria) {
        Specifications<SurveyQuestionField> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), SurveyQuestionField_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), SurveyQuestionField_.name));
            }
            if (criteria.getDataType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDataType(), SurveyQuestionField_.dataType));
            }
            if (criteria.getCategory() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCategory(), SurveyQuestionField_.category));
            }
            if (criteria.getPrePopulationData() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPrePopulationData(), SurveyQuestionField_.prePopulationData));
            }
            if (criteria.getIsVisible() != null) {
                specification = specification.and(buildSpecification(criteria.getIsVisible(), SurveyQuestionField_.isVisible));
            }
            if (criteria.getIsRequired() != null) {
                specification = specification.and(buildSpecification(criteria.getIsRequired(), SurveyQuestionField_.isRequired));
            }
            if (criteria.getSurveyQuestionId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getSurveyQuestionId(), SurveyQuestionField_.surveyQuestion, SurveyQuestion_.id));
            }
        }
        return specification;
    }

}
