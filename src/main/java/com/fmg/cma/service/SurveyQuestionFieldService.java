package com.fmg.cma.service;

import com.fmg.cma.domain.SurveyQuestionField;
import com.fmg.cma.repository.SurveyQuestionFieldRepository;
import com.fmg.cma.service.dto.SurveyQuestionFieldDTO;
import com.fmg.cma.service.mapper.SurveyQuestionFieldMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing SurveyQuestionField.
 */
@Service
@Transactional
public class SurveyQuestionFieldService {

    private final Logger log = LoggerFactory.getLogger(SurveyQuestionFieldService.class);

    private final SurveyQuestionFieldRepository surveyQuestionFieldRepository;

    private final SurveyQuestionFieldMapper surveyQuestionFieldMapper;

    public SurveyQuestionFieldService(SurveyQuestionFieldRepository surveyQuestionFieldRepository, SurveyQuestionFieldMapper surveyQuestionFieldMapper) {
        this.surveyQuestionFieldRepository = surveyQuestionFieldRepository;
        this.surveyQuestionFieldMapper = surveyQuestionFieldMapper;
    }

    /**
     * Save a surveyQuestionField.
     *
     * @param surveyQuestionFieldDTO the entity to save
     * @return the persisted entity
     */
    public SurveyQuestionFieldDTO save(SurveyQuestionFieldDTO surveyQuestionFieldDTO) {
        log.debug("Request to save SurveyQuestionField : {}", surveyQuestionFieldDTO);
        SurveyQuestionField surveyQuestionField = surveyQuestionFieldMapper.toEntity(surveyQuestionFieldDTO);
        surveyQuestionField = surveyQuestionFieldRepository.save(surveyQuestionField);
        return surveyQuestionFieldMapper.toDto(surveyQuestionField);
    }

    /**
     * Get all the surveyQuestionFields.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SurveyQuestionFieldDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SurveyQuestionFields");
        return surveyQuestionFieldRepository.findAll(pageable)
            .map(surveyQuestionFieldMapper::toDto);
    }

    /**
     * Get one surveyQuestionField by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public SurveyQuestionFieldDTO findOne(Long id) {
        log.debug("Request to get SurveyQuestionField : {}", id);
        SurveyQuestionField surveyQuestionField = surveyQuestionFieldRepository.findOne(id);
        return surveyQuestionFieldMapper.toDto(surveyQuestionField);
    }

    /**
     * Delete the surveyQuestionField by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SurveyQuestionField : {}", id);
        surveyQuestionFieldRepository.delete(id);
    }
}
