package com.fmg.cma.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.fmg.cma.domain.SurveyQuestion;
import com.fmg.cma.domain.*; // for static metamodels
import com.fmg.cma.repository.SurveyQuestionRepository;
import com.fmg.cma.service.dto.SurveyQuestionCriteria;

import com.fmg.cma.service.dto.SurveyQuestionDTO;
import com.fmg.cma.service.mapper.SurveyQuestionMapper;

/**
 * Service for executing complex queries for SurveyQuestion entities in the database.
 * The main input is a {@link SurveyQuestionCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SurveyQuestionDTO} or a {@link Page} of {@link SurveyQuestionDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SurveyQuestionQueryService extends QueryService<SurveyQuestion> {

    private final Logger log = LoggerFactory.getLogger(SurveyQuestionQueryService.class);


    private final SurveyQuestionRepository surveyQuestionRepository;

    private final SurveyQuestionMapper surveyQuestionMapper;

    public SurveyQuestionQueryService(SurveyQuestionRepository surveyQuestionRepository, SurveyQuestionMapper surveyQuestionMapper) {
        this.surveyQuestionRepository = surveyQuestionRepository;
        this.surveyQuestionMapper = surveyQuestionMapper;
    }

    /**
     * Return a {@link List} of {@link SurveyQuestionDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SurveyQuestionDTO> findByCriteria(SurveyQuestionCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<SurveyQuestion> specification = createSpecification(criteria);
        return surveyQuestionMapper.toDto(surveyQuestionRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link SurveyQuestionDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SurveyQuestionDTO> findByCriteria(SurveyQuestionCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<SurveyQuestion> specification = createSpecification(criteria);
        final Page<SurveyQuestion> result = surveyQuestionRepository.findAll(specification, page);
        return result.map(surveyQuestionMapper::toDto);
    }

    /**
     * Function to convert SurveyQuestionCriteria to a {@link Specifications}
     */
    private Specifications<SurveyQuestion> createSpecification(SurveyQuestionCriteria criteria) {
        Specifications<SurveyQuestion> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), SurveyQuestion_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), SurveyQuestion_.name));
            }
            if (criteria.getQuestionText() != null) {
                specification = specification.and(buildStringSpecification(criteria.getQuestionText(), SurveyQuestion_.questionText));
            }
            if (criteria.getOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getOrder(), SurveyQuestion_.order));
            }
            if (criteria.getIsVisible() != null) {
                specification = specification.and(buildSpecification(criteria.getIsVisible(), SurveyQuestion_.isVisible));
            }
            if (criteria.getFieldsId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getFieldsId(), SurveyQuestion_.fields, SurveyQuestionField_.id));
            }
            if (criteria.getClientCampaignId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getClientCampaignId(), SurveyQuestion_.clientCampaign, ClientCampaign_.id));
            }
        }
        return specification;
    }

}
