package com.fmg.cma.service;

import com.fmg.cma.domain.SurveyQuestion;
import com.fmg.cma.domain.SurveyQuestionField;
import com.fmg.cma.repository.SurveyQuestionFieldRepository;
import com.fmg.cma.repository.SurveyQuestionRepository;
import com.fmg.cma.service.dto.SurveyQuestionDTO;
import com.fmg.cma.service.mapper.SurveyQuestionFieldMapper;
import com.fmg.cma.service.mapper.SurveyQuestionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Set;

import static java.util.stream.Collectors.toSet;


/**
 * Service Implementation for managing SurveyQuestion.
 */
@Service
@Transactional
public class SurveyQuestionService {

    private final Logger log = LoggerFactory.getLogger(SurveyQuestionService.class);

    private final SurveyQuestionRepository surveyQuestionRepository;

    private final SurveyQuestionFieldRepository surveyQuestionFieldRepository;

    private final SurveyQuestionMapper surveyQuestionMapper;

    private final SurveyQuestionFieldMapper surveyQuestionFieldMapper;

    public SurveyQuestionService(SurveyQuestionRepository surveyQuestionRepository,
                                 SurveyQuestionFieldRepository surveyQuestionFieldRepository,
                                 SurveyQuestionMapper surveyQuestionMapper,
                                 SurveyQuestionFieldMapper surveyQuestionFieldMapper) {
        this.surveyQuestionRepository = surveyQuestionRepository;
        this.surveyQuestionFieldRepository = surveyQuestionFieldRepository;
        this.surveyQuestionMapper = surveyQuestionMapper;
        this.surveyQuestionFieldMapper = surveyQuestionFieldMapper;
    }

    /**
     * Save a surveyQuestion.
     *
     * @param surveyQuestionDTO the entity to save
     * @return the persisted entity
     */
    public SurveyQuestionDTO save(SurveyQuestionDTO surveyQuestionDTO) {
        log.debug("Request to save SurveyQuestion : {}", surveyQuestionDTO);
        SurveyQuestion surveyQuestion = surveyQuestionMapper.toEntity(surveyQuestionDTO);

        boolean isNew = surveyQuestion.getId() == null;
        surveyQuestion = surveyQuestionRepository.save(surveyQuestion);
        if (isNew) {
            // re-save to fill order equals to id
            surveyQuestion.setOrder(surveyQuestion.getId());
            surveyQuestion = surveyQuestionRepository.save(surveyQuestion);
        }

        Set<SurveyQuestionField> fields = surveyQuestionFieldMapper.toEntitySet(surveyQuestionDTO.getFields());
        if (fields != null) {
            if (!isNew) {
                // clear removed fields from a question
                Set<Long> ids = fields.stream().map(SurveyQuestionField::getId).filter(Objects::nonNull).collect(toSet());

                Set<SurveyQuestionField> removeFields = surveyQuestionFieldRepository.findBySurveyQuestionId(surveyQuestion.getId()).stream()
                    .filter(field -> !ids.contains(field.getId()))
                    .collect(toSet());

                surveyQuestionFieldRepository.delete(removeFields);
            }

            for (SurveyQuestionField field : fields) {
                field.setSurveyQuestion(surveyQuestion);
                surveyQuestionFieldRepository.save(field);
            }
        }

        return surveyQuestionMapper.toDto(surveyQuestion);
    }

    /**
     * Get all the surveyQuestions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SurveyQuestionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SurveyQuestions");
        return surveyQuestionRepository.findAll(pageable)
            .map(surveyQuestionMapper::toDto);
    }

    /**
     * Get one surveyQuestion by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public SurveyQuestionDTO findOne(Long id) {
        log.debug("Request to get SurveyQuestion : {}", id);
        SurveyQuestion surveyQuestion = surveyQuestionRepository.findOneWithEagerRelationships(id);
        return surveyQuestionMapper.toDto(surveyQuestion);
    }

    /**
     * Delete the surveyQuestion by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SurveyQuestion : {}", id);
        surveyQuestionRepository.delete(id);
    }
}
