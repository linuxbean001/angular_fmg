package com.fmg.cma.service;

import com.fmg.cma.domain.UserCompany;
import com.fmg.cma.repository.UserCompanyRepository;
import com.fmg.cma.service.dto.UserCompanyDTO;
import com.fmg.cma.service.mapper.UserCompanyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing UserCompany.
 */
@Service
@Transactional
public class UserCompanyService {

    private final Logger log = LoggerFactory.getLogger(UserCompanyService.class);

    private final UserCompanyRepository userCompanyRepository;

    private final UserCompanyMapper userCompanyMapper;

    public UserCompanyService(UserCompanyRepository userCompanyRepository, UserCompanyMapper userCompanyMapper) {
        this.userCompanyRepository = userCompanyRepository;
        this.userCompanyMapper = userCompanyMapper;
    }

    /**
     * Save a userCompany.
     *
     * @param userCompanyDTO the entity to save
     * @return the persisted entity
     */
    public UserCompanyDTO save(UserCompanyDTO userCompanyDTO) {
        log.debug("Request to save UserCompany : {}", userCompanyDTO);
        UserCompany userCompany = userCompanyMapper.toEntity(userCompanyDTO);
        userCompany = userCompanyRepository.save(userCompany);
        return userCompanyMapper.toDto(userCompany);
    }

    /**
     * Get all the userCompanies.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<UserCompanyDTO> findAll() {
        log.debug("Request to get all UserCompanies");
        return userCompanyRepository.findAll().stream()
            .map(userCompanyMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one userCompany by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public UserCompany findOne(Long id) {
        log.debug("Request to get UserCompany : {}", id);
        return userCompanyRepository.findOne(id);
    }

    /**
     * Delete the userCompany by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete UserCompany : {}", id);
        userCompanyRepository.delete(id);
    }
}
