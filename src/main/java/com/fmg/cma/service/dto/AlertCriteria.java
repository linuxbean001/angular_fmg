package com.fmg.cma.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;



import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the Alert entity. This class is used in AlertResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /alerts?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AlertCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private ZonedDateTimeFilter stamp;

    private StringFilter description;

    private StringFilter type;

    private StringFilter status;

    private StringFilter contactRowNums;

    private LongFilter clientCampaignId;

    private LongFilter clientListId;

    private LongFilter alertUserId;

    public AlertCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public ZonedDateTimeFilter getStamp() {
        return stamp;
    }

    public void setStamp(ZonedDateTimeFilter stamp) {
        this.stamp = stamp;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public StringFilter getType() {
        return type;
    }

    public void setType(StringFilter type) {
        this.type = type;
    }

    public StringFilter getStatus() {
        return status;
    }

    public void setStatus(StringFilter status) {
        this.status = status;
    }

    public StringFilter getContactRowNums() {
        return contactRowNums;
    }

    public void setContactRowNums(StringFilter contactRowNums) {
        this.contactRowNums = contactRowNums;
    }

    public LongFilter getClientCampaignId() {
        return clientCampaignId;
    }

    public void setClientCampaignId(LongFilter clientCampaignId) {
        this.clientCampaignId = clientCampaignId;
    }

    public LongFilter getClientListId() {
        return clientListId;
    }

    public void setClientListId(LongFilter clientListId) {
        this.clientListId = clientListId;
    }

    public LongFilter getAlertUserId() {
        return alertUserId;
    }

    public void setAlertUserId(LongFilter alertUserId) {
        this.alertUserId = alertUserId;
    }

    @Override
    public String toString() {
        return "AlertCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (stamp != null ? "stamp=" + stamp + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (type != null ? "type=" + type + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (contactRowNums != null ? "contactRowNums=" + contactRowNums + ", " : "") +
                (clientCampaignId != null ? "clientCampaignId=" + clientCampaignId + ", " : "") +
                (clientListId != null ? "clientListId=" + clientListId + ", " : "") +
                (alertUserId != null ? "alertUserId=" + alertUserId + ", " : "") +
            "}";
    }

}
