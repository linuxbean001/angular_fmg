package com.fmg.cma.service.dto;


import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Alert entity.
 */
public class AlertDTO implements Serializable {

    private Long id;

    private ZonedDateTime stamp;

    private String description;

    private String type;

    private String status;

    private String contactRowNums;

    private Long clientCampaignId;

    private String clientCampaignName;

    private Long clientListId;

    private String clientListName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getStamp() {
        return stamp;
    }

    public void setStamp(ZonedDateTime stamp) {
        this.stamp = stamp;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContactRowNums() {
        return contactRowNums;
    }

    public void setContactRowNums(String contactRowNums) {
        this.contactRowNums = contactRowNums;
    }

    public Long getClientCampaignId() {
        return clientCampaignId;
    }

    public void setClientCampaignId(Long clientCampaignId) {
        this.clientCampaignId = clientCampaignId;
    }

    public String getClientCampaignName() {
        return clientCampaignName;
    }

    public void setClientCampaignName(String clientCampaignName) {
        this.clientCampaignName = clientCampaignName;
    }

    public Long getClientListId() {
        return clientListId;
    }

    public void setClientListId(Long clientListId) {
        this.clientListId = clientListId;
    }

    public String getClientListName() {
        return clientListName;
    }

    public void setClientListName(String clientListName) {
        this.clientListName = clientListName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AlertDTO alertDTO = (AlertDTO) o;
        if(alertDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), alertDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AlertDTO{" +
            "id=" + getId() +
            ", stamp='" + getStamp() + "'" +
            ", description='" + getDescription() + "'" +
            ", type='" + getType() + "'" +
            ", status='" + getStatus() + "'" +
            ", contactRowNums='" + getContactRowNums() + "'" +
            "}";
    }
}
