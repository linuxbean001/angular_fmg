package com.fmg.cma.service.dto;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

public class AlertElementDto {
    private LocalDateTime dateTime;
    private String label;
    private Long campaignId;
    private String campaignName;
    private Long listId;
    private String listName;
    private Set<Long> contactRowNums;

    public Long getCampaignId() {
        return campaignId;
    }

    public AlertElementDto setCampaignId(Long campaignId) {
        this.campaignId = campaignId;
        return this;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public AlertElementDto setCampaignName(String campaignName) {
        this.campaignName = campaignName;
        return this;
    }

    public Long getListId() {
        return listId;
    }

    public AlertElementDto setListId(Long listId) {
        this.listId = listId;
        return this;
    }

    public String getListName() {
        return listName;
    }

    public AlertElementDto setListName(String listName) {
        this.listName = listName;
        return this;
    }

    public Set<Long> getContactRowNums() {
        return contactRowNums;
    }

    public AlertElementDto setContactRowNums(Set<Long> contactRowNums) {
        this.contactRowNums = contactRowNums;
        return this;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setDummy(AlertElementDto dto){
        dto.setDateTime(LocalDateTime.ofInstant(Instant.ofEpochMilli(ThreadLocalRandom.current().nextInt() * 1000L), ZoneId.systemDefault()));
        dto.setLabel("QA");
    }
}
