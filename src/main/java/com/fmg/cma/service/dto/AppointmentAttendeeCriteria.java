package com.fmg.cma.service.dto;

import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

import java.io.Serializable;

/**
 * Criteria class for the AppointmentAttendee entity. This class is used in AppointmentAttendeeResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /appointment-attendees?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AppointmentAttendeeCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter name;

    private StringFilter contactDetail;

    private StringFilter domain;

    private StringFilter username;

    private StringFilter password;

    private StringFilter appointmentDays;

    private LongFilter clientCampaignId;

    public AppointmentAttendeeCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getContactDetail() {
        return contactDetail;
    }

    public void setContactDetail(StringFilter contactDetail) {
        this.contactDetail = contactDetail;
    }

    public StringFilter getDomain() {
        return domain;
    }

    public void setDomain(StringFilter domain) {
        this.domain = domain;
    }

    public StringFilter getUsername() {
        return username;
    }

    public void setUsername(StringFilter username) {
        this.username = username;
    }

    public StringFilter getPassword() {
        return password;
    }

    public void setPassword(StringFilter password) {
        this.password = password;
    }

    public StringFilter getAppointmentDays() {
        return appointmentDays;
    }

    public void setAppointmentDays(StringFilter appointmentDays) {
        this.appointmentDays = appointmentDays;
    }

    public LongFilter getClientCampaignId() {
        return clientCampaignId;
    }

    public void setClientCampaignId(LongFilter clientCampaignId) {
        this.clientCampaignId = clientCampaignId;
    }

    @Override
    public String toString() {
        return "AppointmentAttendeeCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (contactDetail != null ? "contactDetail=" + contactDetail + ", " : "") +
            (domain != null ? "domain=" + domain + ", " : "") +
            (username != null ? "username=" + username + ", " : "") +
            (password != null ? "password=" + password + ", " : "") +
            (appointmentDays != null ? "appointmentDays=" + appointmentDays + ", " : "") +
            (clientCampaignId != null ? "clientCampaignId=" + clientCampaignId + ", " : "") +
            "}";
    }

}
