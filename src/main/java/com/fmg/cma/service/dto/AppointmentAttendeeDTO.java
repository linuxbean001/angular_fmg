package com.fmg.cma.service.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the AppointmentAttendee entity.
 */
public class AppointmentAttendeeDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private String contactDetail;

    private String domain;

    private String username;

    private String password;

    private String appointmentDays;

    private Long clientCampaignId;

    private String clientCampaignName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactDetail() {
        return contactDetail;
    }

    public void setContactDetail(String contactDetail) {
        this.contactDetail = contactDetail;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAppointmentDays() {
        return appointmentDays;
    }

    public void setAppointmentDays(String appointmentDays) {
        this.appointmentDays = appointmentDays;
    }

    public Long getClientCampaignId() {
        return clientCampaignId;
    }

    public void setClientCampaignId(Long clientCampaignId) {
        this.clientCampaignId = clientCampaignId;
    }

    public String getClientCampaignName() {
        return clientCampaignName;
    }

    public void setClientCampaignName(String clientCampaignName) {
        this.clientCampaignName = clientCampaignName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AppointmentAttendeeDTO appointmentAttendeeDTO = (AppointmentAttendeeDTO) o;
        if (appointmentAttendeeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), appointmentAttendeeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AppointmentAttendeeDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", contactDetail='" + getContactDetail() + "'" +
            ", domain='" + getDomain() + "'" +
            ", username='" + getUsername() + "'" +
            ", password='" + getPassword() + "'" +
            ", appointmentDays='" + getAppointmentDays() + "'" +
            "}";
    }
}
