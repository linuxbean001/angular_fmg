package com.fmg.cma.service.dto;

import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

import java.io.Serializable;

/**
 * Criteria class for the BookingQuestionField entity. This class is used in BookingQuestionFieldResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /booking-question-fields?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class BookingQuestionFieldCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter name;

    private StringFilter dataType;

    private StringFilter category;

    private StringFilter prePopulationData;

    private BooleanFilter isVisible;

    private BooleanFilter isRequired;

    private LongFilter bookingQuestionId;

    public BookingQuestionFieldCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getDataType() {
        return dataType;
    }

    public void setDataType(StringFilter dataType) {
        this.dataType = dataType;
    }

    public StringFilter getCategory() {
        return category;
    }

    public void setCategory(StringFilter category) {
        this.category = category;
    }

    public StringFilter getPrePopulationData() {
        return prePopulationData;
    }

    public void setPrePopulationData(StringFilter prePopulationData) {
        this.prePopulationData = prePopulationData;
    }

    public BooleanFilter getIsVisible() {
        return isVisible;
    }

    public void setIsVisible(BooleanFilter isVisible) {
        this.isVisible = isVisible;
    }

    public BooleanFilter getIsRequired() {
        return isRequired;
    }

    public void setIsRequired(BooleanFilter isRequired) {
        this.isRequired = isRequired;
    }

    public LongFilter getBookingQuestionId() {
        return bookingQuestionId;
    }

    public void setBookingQuestionId(LongFilter bookingQuestionId) {
        this.bookingQuestionId = bookingQuestionId;
    }

    @Override
    public String toString() {
        return "BookingQuestionFieldCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (dataType != null ? "dataType=" + dataType + ", " : "") +
            (category != null ? "category=" + category + ", " : "") +
            (prePopulationData != null ? "prePopulationData=" + prePopulationData + ", " : "") +
            (isVisible != null ? "isVisible=" + isVisible + ", " : "") +
            (isRequired != null ? "isRequired=" + isRequired + ", " : "") +
            (bookingQuestionId != null ? "bookingQuestionId=" + bookingQuestionId + ", " : "") +
            "}";
    }

}
