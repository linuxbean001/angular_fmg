package com.fmg.cma.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the BookingQuestionField entity.
 */
public class BookingQuestionFieldDTO implements Serializable {

    private Long id;

    private String name;

    private String dataType;

    private String category;

    private String prePopulationData;

    private Boolean isVisible;

    private Boolean isRequired;

    private Long bookingQuestionId;

    private String bookingQuestionName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPrePopulationData() {
        return prePopulationData;
    }

    public void setPrePopulationData(String prePopulationData) {
        this.prePopulationData = prePopulationData;
    }

    public Boolean isIsVisible() {
        return isVisible;
    }

    public void setIsVisible(Boolean isVisible) {
        this.isVisible = isVisible;
    }

    public Boolean isIsRequired() {
        return isRequired;
    }

    public void setIsRequired(Boolean isRequired) {
        this.isRequired = isRequired;
    }

    public Long getBookingQuestionId() {
        return bookingQuestionId;
    }

    public void setBookingQuestionId(Long bookingQuestionId) {
        this.bookingQuestionId = bookingQuestionId;
    }

    public String getBookingQuestionName() {
        return bookingQuestionName;
    }

    public void setBookingQuestionName(String bookingQuestionName) {
        this.bookingQuestionName = bookingQuestionName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BookingQuestionFieldDTO bookingQuestionFieldDTO = (BookingQuestionFieldDTO) o;
        if (bookingQuestionFieldDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bookingQuestionFieldDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BookingQuestionFieldDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", dataType='" + getDataType() + "'" +
            ", category='" + getCategory() + "'" +
            ", prePopulationData='" + getPrePopulationData() + "'" +
            ", isVisible='" + isIsVisible() + "'" +
            ", isRequired='" + isIsRequired() + "'" +
            "}";
    }
}
