package com.fmg.cma.service.dto;


import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A DTO for the CallHistory entity.
 */
public class CallHistoryDTO implements Serializable {

    private Long id;

    private ZonedDateTime createdDate;

    private String details;

    private String note;

    private Long orgRowNum;

    private Long contactRowNum;

    private Long userId;

    private String userLogin;

    private Long typeId;

    private String typeName;

    private String typeIdent;

    private Long listId;

    private String listName;

    private String campaignName;

    private String smsText;

    private Long smsId;

    private String smsName;

    private Long emailTemplateId;

    private String emailTemplateName;

    private Boolean isNoContactHoldFromCallPool;

    private ZonedDateTime noContactStamp;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getOrgRowNum() {
        return orgRowNum;
    }

    public void setOrgRowNum(Long orgRowNum) {
        this.orgRowNum = orgRowNum;
    }

    public Long getContactRowNum() {
        return contactRowNum;
    }

    public void setContactRowNum(Long contactRowNum) {
        this.contactRowNum = contactRowNum;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long callHistoryTypeId) {
        this.typeId = callHistoryTypeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String callHistoryTypeName) {
        this.typeName = callHistoryTypeName;
    }

    public Long getListId() {
        return listId;
    }

    public String getTypeIdent() {
        return typeIdent;
    }

    public void setTypeIdent(String typeIdent) {
        this.typeIdent = typeIdent;
    }

    public void setListId(Long clientListId) {
        this.listId = clientListId;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String clientListName) {
        this.listName = clientListName;
    }

    public String getSmsText() {
        return smsText;
    }

    public void setSmsText(String smsText) {
        this.smsText = smsText;
    }

    public Long getSmsId() {
        return smsId;
    }

    public void setSmsId(Long smsId) {
        this.smsId = smsId;
    }

    public String getSmsName() {
        return smsName;
    }

    public void setSmsName(String smsName) {
        this.smsName = smsName;
    }

    public Long getEmailTemplateId() {
        return emailTemplateId;
    }

    public void setEmailTemplateId(Long emailTemplateId) {
        this.emailTemplateId = emailTemplateId;
    }

    public String getEmailTemplateName() {
        return emailTemplateName;
    }

    public void setEmailTemplateName(String emailTemplateName) {
        this.emailTemplateName = emailTemplateName;
    }

    public Boolean getIsNoContactHoldFromCallPool() {
        return isNoContactHoldFromCallPool;
    }

    public void setIsNoContactHoldFromCallPool(Boolean noContactHoldFromCallPool) {
        isNoContactHoldFromCallPool = noContactHoldFromCallPool;
    }

    public ZonedDateTime getNoContactStamp() {
        return noContactStamp;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public void setNoContactStamp(ZonedDateTime noContactStamp) {
        this.noContactStamp = noContactStamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CallHistoryDTO callHistoryDTO = (CallHistoryDTO) o;
        if (callHistoryDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), callHistoryDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CallHistoryDTO{" +
            "id=" + getId() +
            ", createdDate='" + getCreatedDate() + "'" +
            ", details='" + getDetails() + "'" +
            ", note='" + getNote() + "'" +
            ", orgRowNum=" + getOrgRowNum() +
            ", contactRowNum=" + getContactRowNum() +
            "}";
    }
}
