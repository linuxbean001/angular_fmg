package com.fmg.cma.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the CallHistoryType entity.
 */
public class CallHistoryTypeDTO implements Serializable {

    private Long id;

    private String name;

    private String ident;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdent() {
        return ident;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CallHistoryTypeDTO callHistoryTypeDTO = (CallHistoryTypeDTO) o;
        if (callHistoryTypeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), callHistoryTypeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CallHistoryTypeDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", ident='" + getIdent() + "'" +
            "}";
    }
}
