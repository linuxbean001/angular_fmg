package com.fmg.cma.service.dto;

import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;

import java.io.Serializable;


/**
 * Criteria class for the CampaignListContactManagement entity. This class is used in CampaignListContactManagementResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /campaign-list-contact-managements?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CampaignListContactManagementCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private LongFilter contactRowNum;

    private BooleanFilter holdFromCallPool;

    private ZonedDateTimeFilter stamp;

    private LongFilter clientCampaignId;

    private LongFilter clientListId;

    public CampaignListContactManagementCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getContactRowNum() {
        return contactRowNum;
    }

    public void setContactRowNum(LongFilter contactRowNum) {
        this.contactRowNum = contactRowNum;
    }

    public BooleanFilter getHoldFromCallPool() {
        return holdFromCallPool;
    }

    public void setHoldFromCallPool(BooleanFilter holdFromCallPool) {
        this.holdFromCallPool = holdFromCallPool;
    }

    public ZonedDateTimeFilter getStamp() {
        return stamp;
    }

    public void setStamp(ZonedDateTimeFilter stamp) {
        this.stamp = stamp;
    }

    public LongFilter getClientCampaignId() {
        return clientCampaignId;
    }

    public void setClientCampaignId(LongFilter clientCampaignId) {
        this.clientCampaignId = clientCampaignId;
    }

    public LongFilter getClientListId() {
        return clientListId;
    }

    public void setClientListId(LongFilter clientListId) {
        this.clientListId = clientListId;
    }

    @Override
    public String toString() {
        return "CampaignListContactManagementCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (contactRowNum != null ? "contactRowNum=" + contactRowNum + ", " : "") +
            (holdFromCallPool != null ? "holdFromCallPool=" + holdFromCallPool + ", " : "") +
            (stamp != null ? "stamp=" + stamp + ", " : "") +
            (clientCampaignId != null ? "clientCampaignId=" + clientCampaignId + ", " : "") +
            (clientListId != null ? "clientListId=" + clientListId + ", " : "") +
            "}";
    }

}
