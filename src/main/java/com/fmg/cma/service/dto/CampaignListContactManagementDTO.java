package com.fmg.cma.service.dto;


import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A DTO for the CampaignListContactManagement entity.
 */
public class CampaignListContactManagementDTO implements Serializable {

    private Long id;

    private Long contactRowNum;

    private Boolean holdFromCallPool;

    private ZonedDateTime stamp;

    private Long clientCampaignId;

    private String clientCampaignName;

    private Long clientListId;

    private String clientListName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getContactRowNum() {
        return contactRowNum;
    }

    public void setContactRowNum(Long contactRowNum) {
        this.contactRowNum = contactRowNum;
    }

    public Boolean isHoldFromCallPool() {
        return holdFromCallPool;
    }

    public void setHoldFromCallPool(Boolean holdFromCallPool) {
        this.holdFromCallPool = holdFromCallPool;
    }

    public ZonedDateTime getStamp() {
        return stamp;
    }

    public void setStamp(ZonedDateTime stamp) {
        this.stamp = stamp;
    }

    public Long getClientCampaignId() {
        return clientCampaignId;
    }

    public void setClientCampaignId(Long clientCampaignId) {
        this.clientCampaignId = clientCampaignId;
    }

    public String getClientCampaignName() {
        return clientCampaignName;
    }

    public void setClientCampaignName(String clientCampaignName) {
        this.clientCampaignName = clientCampaignName;
    }

    public Long getClientListId() {
        return clientListId;
    }

    public void setClientListId(Long clientListId) {
        this.clientListId = clientListId;
    }

    public String getClientListName() {
        return clientListName;
    }

    public void setClientListName(String clientListName) {
        this.clientListName = clientListName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CampaignListContactManagementDTO campaignListContactManagementDTO = (CampaignListContactManagementDTO) o;
        if (campaignListContactManagementDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), campaignListContactManagementDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CampaignListContactManagementDTO{" +
            "id=" + getId() +
            ", contactRowNum=" + getContactRowNum() +
            ", holdFromCallPool='" + isHoldFromCallPool() + "'" +
            ", stamp='" + getStamp() + "'" +
            "}";
    }
}
