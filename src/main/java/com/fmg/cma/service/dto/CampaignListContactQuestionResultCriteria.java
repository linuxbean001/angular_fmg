package com.fmg.cma.service.dto;

import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

import java.io.Serializable;


/**
 * Criteria class for the CampaignListContactQuestionResult entity. This class is used in CampaignListContactQuestionResultResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /campaign-list-contact-question-results?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CampaignListContactQuestionResultCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private LongFilter contactRowNum;

    private StringFilter cvalue;

    private LongFilter clientCampaignId;

    private LongFilter clientListId;

    private LongFilter bookingQuestionFieldId;

    public CampaignListContactQuestionResultCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getContactRowNum() {
        return contactRowNum;
    }

    public void setContactRowNum(LongFilter contactRowNum) {
        this.contactRowNum = contactRowNum;
    }

    public StringFilter getCvalue() {
        return cvalue;
    }

    public void setCvalue(StringFilter cvalue) {
        this.cvalue = cvalue;
    }

    public LongFilter getClientCampaignId() {
        return clientCampaignId;
    }

    public void setClientCampaignId(LongFilter clientCampaignId) {
        this.clientCampaignId = clientCampaignId;
    }

    public LongFilter getClientListId() {
        return clientListId;
    }

    public void setClientListId(LongFilter clientListId) {
        this.clientListId = clientListId;
    }

    public LongFilter getBookingQuestionFieldId() {
        return bookingQuestionFieldId;
    }

    public void setBookingQuestionFieldId(LongFilter bookingQuestionFieldId) {
        this.bookingQuestionFieldId = bookingQuestionFieldId;
    }

    @Override
    public String toString() {
        return "CampaignListContactQuestionResultCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (contactRowNum != null ? "contactRowNum=" + contactRowNum + ", " : "") +
            (cvalue != null ? "cvalue=" + cvalue + ", " : "") +
            (clientCampaignId != null ? "clientCampaignId=" + clientCampaignId + ", " : "") +
            (clientListId != null ? "clientListId=" + clientListId + ", " : "") +
            (bookingQuestionFieldId != null ? "bookingQuestionFieldId=" + bookingQuestionFieldId + ", " : "") +
            "}";
    }

}
