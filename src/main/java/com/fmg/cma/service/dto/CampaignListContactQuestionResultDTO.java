package com.fmg.cma.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the CampaignListContactQuestionResult entity.
 */
public class CampaignListContactQuestionResultDTO implements Serializable {

    private Long id;

    private Long contactRowNum;

    private String cvalue;

    private Long clientCampaignId;

    private String clientCampaignName;

    private Long clientListId;

    private String clientListName;

    private Long bookingQuestionFieldId;

    private String bookingQuestionFieldName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getContactRowNum() {
        return contactRowNum;
    }

    public void setContactRowNum(Long contactRowNum) {
        this.contactRowNum = contactRowNum;
    }

    public String getCvalue() {
        return cvalue;
    }

    public void setCvalue(String cvalue) {
        this.cvalue = cvalue;
    }

    public Long getClientCampaignId() {
        return clientCampaignId;
    }

    public void setClientCampaignId(Long clientCampaignId) {
        this.clientCampaignId = clientCampaignId;
    }

    public String getClientCampaignName() {
        return clientCampaignName;
    }

    public void setClientCampaignName(String clientCampaignName) {
        this.clientCampaignName = clientCampaignName;
    }

    public Long getClientListId() {
        return clientListId;
    }

    public void setClientListId(Long clientListId) {
        this.clientListId = clientListId;
    }

    public String getClientListName() {
        return clientListName;
    }

    public void setClientListName(String clientListName) {
        this.clientListName = clientListName;
    }

    public Long getBookingQuestionFieldId() {
        return bookingQuestionFieldId;
    }

    public void setBookingQuestionFieldId(Long bookingQuestionFieldId) {
        this.bookingQuestionFieldId = bookingQuestionFieldId;
    }

    public String getBookingQuestionFieldName() {
        return bookingQuestionFieldName;
    }

    public void setBookingQuestionFieldName(String bookingQuestionFieldName) {
        this.bookingQuestionFieldName = bookingQuestionFieldName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CampaignListContactQuestionResultDTO campaignListContactQuestionResultDTO = (CampaignListContactQuestionResultDTO) o;
        if (campaignListContactQuestionResultDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), campaignListContactQuestionResultDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CampaignListContactQuestionResultDTO{" +
            "id=" + getId() +
            ", contactRowNum=" + getContactRowNum() +
            ", cvalue='" + getCvalue() + "'" +
            "}";
    }
}
