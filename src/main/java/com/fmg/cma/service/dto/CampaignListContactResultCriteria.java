package com.fmg.cma.service.dto;

import java.io.Serializable;

import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;


/**
 * Criteria class for the CampaignListContactResult entity. This class is used in CampaignListContactResultResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /campaign-list-contact-results?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CampaignListContactResultCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private LongFilter contactRowNum;

    private LongFilter clientCampaignId;

    private LongFilter clientListId;

    private LongFilter outcomeSubReasonId;

    public CampaignListContactResultCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getContactRowNum() {
        return contactRowNum;
    }

    public void setContactRowNum(LongFilter contactRowNum) {
        this.contactRowNum = contactRowNum;
    }

    public LongFilter getClientCampaignId() {
        return clientCampaignId;
    }

    public void setClientCampaignId(LongFilter clientCampaignId) {
        this.clientCampaignId = clientCampaignId;
    }

    public LongFilter getClientListId() {
        return clientListId;
    }

    public void setClientListId(LongFilter clientListId) {
        this.clientListId = clientListId;
    }

    public LongFilter getOutcomeSubReasonId() {
        return outcomeSubReasonId;
    }

    public void setOutcomeSubReasonId(LongFilter outcomeSubReasonId) {
        this.outcomeSubReasonId = outcomeSubReasonId;
    }

    @Override
    public String toString() {
        return "CampaignListContactResultCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (contactRowNum != null ? "contactRowNum=" + contactRowNum + ", " : "") +
            (clientCampaignId != null ? "clientCampaignId=" + clientCampaignId + ", " : "") +
            (clientListId != null ? "clientListId=" + clientListId + ", " : "") +
            (outcomeSubReasonId != null ? "outcomeSubReasonId=" + outcomeSubReasonId + ", " : "") +
            "}";
    }

}
