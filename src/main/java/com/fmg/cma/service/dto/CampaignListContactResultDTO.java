package com.fmg.cma.service.dto;


import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A DTO for the CampaignListContactResult entity.
 */
public class CampaignListContactResultDTO implements Serializable {

    private Long id;

    private Long contactRowNum;

    private Long clientCampaignId;

    private String clientCampaignName;

    private Long clientListId;

    private String clientListName;

    private Long outcomeSubReasonId;

    private String outcomeSubReasonName;

    private Boolean isApproved;

    private Boolean emailIsSent;

    private String emailTemplateJson;

    private Boolean smsIsSent;

    private String state;

    private Long assignUserId;

    private String assignUserLogin;

    private ZonedDateTime assignStamp;

    private Long clickedOutcomeId;

    private String clickedOutcomeName;

    private Long orgRowNum;

    private Long userClickedFinishId;

    private String userClickedFinishLogin;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getContactRowNum() {
        return contactRowNum;
    }

    public void setContactRowNum(Long contactRowNum) {
        this.contactRowNum = contactRowNum;
    }

    public Long getClientCampaignId() {
        return clientCampaignId;
    }

    public void setClientCampaignId(Long clientCampaignId) {
        this.clientCampaignId = clientCampaignId;
    }

    public String getClientCampaignName() {
        return clientCampaignName;
    }

    public void setClientCampaignName(String clientCampaignName) {
        this.clientCampaignName = clientCampaignName;
    }

    public Long getClientListId() {
        return clientListId;
    }

    public void setClientListId(Long clientListId) {
        this.clientListId = clientListId;
    }

    public String getClientListName() {
        return clientListName;
    }

    public void setClientListName(String clientListName) {
        this.clientListName = clientListName;
    }

    public Long getOutcomeSubReasonId() {
        return outcomeSubReasonId;
    }

    public void setOutcomeSubReasonId(Long outcomeSubReasonId) {
        this.outcomeSubReasonId = outcomeSubReasonId;
    }

    public String getOutcomeSubReasonName() {
        return outcomeSubReasonName;
    }

    public void setOutcomeSubReasonName(String outcomeSubReasonName) {
        this.outcomeSubReasonName = outcomeSubReasonName;
    }

    public Boolean getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(Boolean approved) {
        isApproved = approved;
    }

    public Boolean getEmailIsSent() {
        return emailIsSent;
    }

    public void setEmailIsSent(Boolean emailIsSent) {
        this.emailIsSent = emailIsSent;
    }

    public String getEmailTemplateJson() {
        return emailTemplateJson;
    }

    public void setEmailTemplateJson(String emailTemplateJson) {
        this.emailTemplateJson = emailTemplateJson;
    }

    public Boolean getSmsIsSent() {
        return smsIsSent;
    }

    public void setSmsIsSent(Boolean smsIsSent) {
        this.smsIsSent = smsIsSent;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getAssignUserId() {
        return assignUserId;
    }

    public void setAssignUserId(Long assignUserId) {
        this.assignUserId = assignUserId;
    }

    public String getAssignUserLogin() {
        return assignUserLogin;
    }

    public void setAssignUserLogin(String assignUserLogin) {
        this.assignUserLogin = assignUserLogin;
    }

    public ZonedDateTime getAssignStamp() {
        return assignStamp;
    }

    public void setAssignStamp(ZonedDateTime assignStamp) {
        this.assignStamp = assignStamp;
    }

    public Long getClickedOutcomeId() {
        return clickedOutcomeId;
    }

    public void setClickedOutcomeId(Long clickedOutcomeId) {
        this.clickedOutcomeId = clickedOutcomeId;
    }

    public String getClickedOutcomeName() {
        return clickedOutcomeName;
    }

    public void setClickedOutcomeName(String clickedOutcomeName) {
        this.clickedOutcomeName = clickedOutcomeName;
    }

    public Long getOrgRowNum() {
        return orgRowNum;
    }

    public CampaignListContactResultDTO setOrgRowNum(Long orgRowNum) {
        this.orgRowNum = orgRowNum;
        return this;
    }

    public Long getUserClickedFinishId() {
        return userClickedFinishId;
    }

    public CampaignListContactResultDTO setUserClickedFinishId(Long userClickedFinishId) {
        this.userClickedFinishId = userClickedFinishId;
        return this;
    }

    public String getUserClickedFinishLogin() {
        return userClickedFinishLogin;
    }

    public CampaignListContactResultDTO setUserClickedFinishLogin(String userClickedFinishLogin) {
        this.userClickedFinishLogin = userClickedFinishLogin;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CampaignListContactResultDTO campaignListContactResultDTO = (CampaignListContactResultDTO) o;
        if (campaignListContactResultDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), campaignListContactResultDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CampaignListContactResultDTO{" +
            "id=" + getId() +
            ", contactRowNum=" + getContactRowNum() +
            "}";
    }
}
