package com.fmg.cma.service.dto;

import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

import java.io.Serializable;


/**
 * Criteria class for the CampaignPortalUser entity. This class is used in CampaignPortalUserResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /campaign-portal-users?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CampaignPortalUserCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter email;

    private BooleanFilter emailOutcome;

    private BooleanFilter emailDaily;

    private LongFilter clientCampaignId;

    private LongFilter userId;

    public CampaignPortalUserCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public BooleanFilter getEmailOutcome() {
        return emailOutcome;
    }

    public void setEmailOutcome(BooleanFilter emailOutcome) {
        this.emailOutcome = emailOutcome;
    }

    public BooleanFilter getEmailDaily() {
        return emailDaily;
    }

    public void setEmailDaily(BooleanFilter emailDaily) {
        this.emailDaily = emailDaily;
    }

    public LongFilter getClientCampaignId() {
        return clientCampaignId;
    }

    public void setClientCampaignId(LongFilter clientCampaignId) {
        this.clientCampaignId = clientCampaignId;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "CampaignPortalUserCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (email != null ? "email=" + email + ", " : "") +
            (emailOutcome != null ? "emailOutcome=" + emailOutcome + ", " : "") +
            (emailDaily != null ? "emailDaily=" + emailDaily + ", " : "") +
            (clientCampaignId != null ? "clientCampaignId=" + clientCampaignId + ", " : "") +
            (userId != null ? "userId=" + userId + ", " : "") +
            "}";
    }

}
