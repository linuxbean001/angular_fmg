package com.fmg.cma.service.dto;


import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the CampaignPortalUser entity.
 */
public class CampaignPortalUserDTO implements Serializable {

    private Long id;

    @NotNull
    private String email;

    private Long clientCampaignId;

    private String clientCampaignName;

    private Long userId;

    private String userLogin;

    private Boolean sentNotification;

    public CampaignPortalUserDTO() {
    }

    public CampaignPortalUserDTO(Long aId) {
        id = aId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getClientCampaignId() {
        return clientCampaignId;
    }

    public Boolean getSentNotification() {
        return sentNotification;
    }

    public void setSentNotification(Boolean aSentNotification) {
        sentNotification = aSentNotification;
    }

    public CampaignPortalUserDTO setClientCampaignId(Long clientCampaignId) {
        this.clientCampaignId = clientCampaignId;
        return this;
    }

    public String getClientCampaignName() {
        return clientCampaignName;
    }

    public void setClientCampaignName(String clientCampaignName) {
        this.clientCampaignName = clientCampaignName;
    }

    public Long getUserId() {
        return userId;
    }

    public CampaignPortalUserDTO setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CampaignPortalUserDTO campaignPortalUserDTO = (CampaignPortalUserDTO) o;
        if (campaignPortalUserDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), campaignPortalUserDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CampaignPortalUserDTO{" +
            "id=" + getId() +
            ", email='" + getEmail() + "'" +
            ", send Notification email to user ='" + getSentNotification() + "'" +
            "}";
    }
}
