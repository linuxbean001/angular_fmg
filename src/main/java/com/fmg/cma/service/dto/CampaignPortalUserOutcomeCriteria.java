package com.fmg.cma.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the CampaignPortalUserOutcome entity. This class is used in CampaignPortalUserOutcomeResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /campaign-portal-user-outcomes?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CampaignPortalUserOutcomeCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private LongFilter campaignPortalUserId;

    private LongFilter outcomeId;

    public CampaignPortalUserOutcomeCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getCampaignPortalUserId() {
        return campaignPortalUserId;
    }

    public void setCampaignPortalUserId(LongFilter campaignPortalUserId) {
        this.campaignPortalUserId = campaignPortalUserId;
    }

    public LongFilter getOutcomeId() {
        return outcomeId;
    }

    public void setOutcomeId(LongFilter outcomeId) {
        this.outcomeId = outcomeId;
    }

    @Override
    public String toString() {
        return "CampaignPortalUserOutcomeCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (campaignPortalUserId != null ? "campaignPortalUserId=" + campaignPortalUserId + ", " : "") +
                (outcomeId != null ? "outcomeId=" + outcomeId + ", " : "") +
            "}";
    }

}
