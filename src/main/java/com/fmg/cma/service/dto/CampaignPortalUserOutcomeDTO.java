package com.fmg.cma.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the CampaignPortalUserOutcome entity.
 */
public class CampaignPortalUserOutcomeDTO implements Serializable {

    private Long id;

    private Long campaignPortalUserId;

    private String campaignPortalUserEmail;

    private Long outcomeId;

    private String outcomeName;

    private Boolean emailImmediately;

    private Boolean emailDaily;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCampaignPortalUserId() {
        return campaignPortalUserId;
    }

    public void setCampaignPortalUserId(Long campaignPortalUserId) {
        this.campaignPortalUserId = campaignPortalUserId;
    }

    public String getCampaignPortalUserEmail() {
        return campaignPortalUserEmail;
    }

    public void setCampaignPortalUserEmail(String campaignPortalUserEmail) {
        this.campaignPortalUserEmail = campaignPortalUserEmail;
    }

    public Long getOutcomeId() {
        return outcomeId;
    }

    public void setOutcomeId(Long outcomeId) {
        this.outcomeId = outcomeId;
    }

    public String getOutcomeName() {
        return outcomeName;
    }

    public void setOutcomeName(String outcomeName) {
        this.outcomeName = outcomeName;
    }

    public Boolean getEmailImmediately() {
        return emailImmediately;
    }

    public void setEmailImmediately(Boolean emailImmediately) {
        this.emailImmediately = emailImmediately;
    }

    public Boolean getEmailDaily() {
        return emailDaily;
    }

    public void setEmailDaily(Boolean emailDaily) {
        this.emailDaily = emailDaily;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CampaignPortalUserOutcomeDTO campaignPortalUserOutcomeDTO = (CampaignPortalUserOutcomeDTO) o;
        if (campaignPortalUserOutcomeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), campaignPortalUserOutcomeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CampaignPortalUserOutcomeDTO{" +
            "id=" + getId() +
            "}";
    }
}
