package com.fmg.cma.service.dto;

import java.io.Serializable;

public class CampaignPortalUserSummaryDTO implements Serializable {

    private Boolean emailImmediately;

    private Boolean emailDaily;

    public Boolean getEmailImmediately() {
        return emailImmediately;
    }

    public CampaignPortalUserSummaryDTO setEmailImmediately(Boolean emailImmediately) {
        this.emailImmediately = emailImmediately;
        return this;
    }

    public Boolean isEmailDaily() {
        return emailDaily;
    }

    public CampaignPortalUserSummaryDTO setEmailDaily(Boolean emailDaily) {
        this.emailDaily = emailDaily;
        return this;
    }

    @Override
    public String toString() {
        return "CampaignPortalUserSummaryDTO{" +
            ", emailImmediately='" + emailImmediately + "'" +
            ", emailDaily='" + emailDaily + "'" +
            "}";
    }
}
