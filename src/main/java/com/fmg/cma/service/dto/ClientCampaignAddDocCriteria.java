package com.fmg.cma.service.dto;

import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

import java.io.Serializable;


/**
 * Criteria class for the ClientCampaignAddDoc entity. This class is used in ClientCampaignAddDocResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /client-campaign-add-docs?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ClientCampaignAddDocCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter link;

    private StringFilter text;

    private LongFilter clientCampaignId;

    public ClientCampaignAddDocCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getLink() {
        return link;
    }

    public void setLink(StringFilter link) {
        this.link = link;
    }

    public StringFilter getText() {
        return text;
    }

    public void setText(StringFilter text) {
        this.text = text;
    }

    public LongFilter getClientCampaignId() {
        return clientCampaignId;
    }

    public void setClientCampaignId(LongFilter clientCampaignId) {
        this.clientCampaignId = clientCampaignId;
    }

    @Override
    public String toString() {
        return "ClientCampaignAddDocCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (link != null ? "link=" + link + ", " : "") +
            (text != null ? "text=" + text + ", " : "") +
            (clientCampaignId != null ? "clientCampaignId=" + clientCampaignId + ", " : "") +
            "}";
    }

}
