package com.fmg.cma.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the ClientCampaignAddDoc entity.
 */
public class ClientCampaignAddDocDTO implements Serializable {

    private Long id;

    private String link;

    private String text;

    private Long clientCampaignId;

    private String clientCampaignName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getClientCampaignId() {
        return clientCampaignId;
    }

    public void setClientCampaignId(Long clientCampaignId) {
        this.clientCampaignId = clientCampaignId;
    }

    public String getClientCampaignName() {
        return clientCampaignName;
    }

    public void setClientCampaignName(String clientCampaignName) {
        this.clientCampaignName = clientCampaignName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ClientCampaignAddDocDTO clientCampaignAddDocDTO = (ClientCampaignAddDocDTO) o;
        if (clientCampaignAddDocDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clientCampaignAddDocDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClientCampaignAddDocDTO{" +
            "id=" + getId() +
            ", link='" + getLink() + "'" +
            ", text='" + getText() + "'" +
            "}";
    }
}
