package com.fmg.cma.service.dto;

import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;

import java.io.Serializable;


/**
 * Criteria class for the ClientCampaign entity. This class is used in ClientCampaignResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /client-campaigns?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ClientCampaignCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter name;

    private ZonedDateTimeFilter createdDate;

    private LongFilter createdById;

    private LongFilter templateId;

    private LongFilter clientCompanyId;

    private LongFilter managersId;

    private LongFilter clientListId;

    private LongFilter emailTemplateId;

    private LongFilter outcomesId;

    public ClientCampaignCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public ZonedDateTimeFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(ZonedDateTimeFilter createdDate) {
        this.createdDate = createdDate;
    }

    public LongFilter getCreatedById() {
        return createdById;
    }

    public void setCreatedById(LongFilter createdById) {
        this.createdById = createdById;
    }

    public LongFilter getTemplateId() {
        return templateId;
    }

    public void setTemplateId(LongFilter templateId) {
        this.templateId = templateId;
    }

    public LongFilter getClientCompanyId() {
        return clientCompanyId;
    }

    public void setClientCompanyId(LongFilter clientCompanyId) {
        this.clientCompanyId = clientCompanyId;
    }

    public LongFilter getManagersId() {
        return managersId;
    }

    public void setManagersId(LongFilter managersId) {
        this.managersId = managersId;
    }

    public LongFilter getClientListId() {
        return clientListId;
    }

    public void setClientListId(LongFilter clientListId) {
        this.clientListId = clientListId;
    }

    public LongFilter getEmailTemplateId() {
        return emailTemplateId;
    }

    public void setEmailTemplateId(LongFilter emailTemplateId) {
        this.emailTemplateId = emailTemplateId;
    }

    public LongFilter getOutcomesId() {
        return outcomesId;
    }

    public void setOutcomesId(LongFilter outcomesId) {
        this.outcomesId = outcomesId;
    }

    @Override
    public String toString() {
        return "ClientCampaignCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
            (createdById != null ? "createdById=" + createdById + ", " : "") +
            (templateId != null ? "templateId=" + templateId + ", " : "") +
            (clientCompanyId != null ? "clientCompanyId=" + clientCompanyId + ", " : "") +
            (managersId != null ? "managersId=" + managersId + ", " : "") +
            (clientListId != null ? "clientListId=" + clientListId + ", " : "") +
            (emailTemplateId != null ? "emailTemplateId=" + emailTemplateId + ", " : "") +
            (outcomesId != null ? "outcomesId=" + outcomesId + ", " : "") +
            "}";
    }

}
