package com.fmg.cma.service.dto;


import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the ClientCampaign entity.
 */
public class ClientCampaignDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private ZonedDateTime createdDate;

    private Long createdById;

    private String createdByLogin;

    private Long templateId;

    private Long clientCompanyId;

    private String clientCompanyName;

    private Long clientCompanyIndustryId;

    private Set<UserDTO> managers = new HashSet<>();

    private Set<UserDTO> users = new HashSet<>();

    private Set<UserDTO> qaUsers = new HashSet<>();

    private Set<EmailTemplateDTO> emailTemplates = new HashSet<>();

    private List<CampaignPortalUserDTO> portalUsers;

    private List<AppointmentAttendeeDTO> appointmentAttendees;

    private List<ClientListDTO> clientLists;

    private Set<OutcomeDTO> outcomes = new HashSet<>();

    private Set<ClientCampaignScriptDTO> scripts = new HashSet<>();

    private Set<ClientCampaignSmsDTO> smss = new HashSet<>();

    private Boolean weeklyHeadlineStats;

    private Boolean allowedExportData;

    private String sarReportIdent;

    private LocalTime sarReportTime;

    private String statsSummaryIdent;

    private LocalTime statsSummaryTime;

    private List<String> reportDistributionMethods;

    private List<Long> portalUsersAllowedToExport;

    private List<Long> reportsDeliveredToUsers;

    private String website;

    private String meetingDuration;

    private String specialRequests;

    private String resultsPerHour;

    private String qualityControl;

    private String comments;

    private String appointmentType;

    private String surveyName;

    private String bookingName;

    private Set<SurveyQuestionDTO> surveyQuestions = new HashSet<>();

    private Set<BookingQuestionDTO> bookingQuestions = new HashSet<>();

    private Boolean smsTabNotRequired;

    private String controllerFullName;

    private String controllerJobRole;

    private String controllerEmail;

    private String controllerPhone;

    private String controllerCompanyName;

    private String controllerCompanyReg;

    private String controllerCompanyAddress;

    private String sarsFullName;

    private String sarsJobRole;

    private String sarsEmail;

    private String sarsPhone;

    private String sarsCompanyName;

    private String sarsCompanyReg;

    private String sarsCompanyAddress;

    private String liaHyperlink;

    private Integer callPerHour;

    private Integer callPerHourLow;

    private Integer callPerHourHigh;

    private Integer conversationsPerHour;

    private Integer conversationsPerHourLow;

    private Integer conversationsPerHourHigh;

    private Double contactRate;

    private Double contactRateLow;

    private Double contactRateHigh;

    private Integer hoursPerLead;

    private Integer hoursPerLeadLow;

    private Integer hoursPerLeadHigh;

    private Double conversionRate;

    private Double conversionRateLow;

    private Double conversionRateHigh;

    private Double leadToSaleConversion;

    private Double leadToSaleConversionLow;

    private Double leadToSaleConversionHigh;

    private BigDecimal averageSaleValue;

    private BigDecimal averageSaleValueLow;

    private BigDecimal averageSaleValueHigh;

    private String strategyDocument;

    private Set<ClientCampaignAddDocDTO> addDocs = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public Long getCreatedById() {
        return createdById;
    }

    public void setCreatedById(Long userId) {
        this.createdById = userId;
    }

    public String getCreatedByLogin() {
        return createdByLogin;
    }

    public void setCreatedByLogin(String userLogin) {
        this.createdByLogin = userLogin;
    }

    public Long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Long clientCampaignTemplateId) {
        this.templateId = clientCampaignTemplateId;
    }

    public Long getClientCompanyId() {
        return clientCompanyId;
    }

    public void setClientCompanyId(Long clientCompanyId) {
        this.clientCompanyId = clientCompanyId;
    }

    public String getClientCompanyName() {
        return clientCompanyName;
    }

    public void setClientCompanyName(String clientCompanyName) {
        this.clientCompanyName = clientCompanyName;
    }

    public Long getClientCompanyIndustryId() {
        return clientCompanyIndustryId;
    }

    public void setClientCompanyIndustryId(Long clientCompanyIndustryId) {
        this.clientCompanyIndustryId = clientCompanyIndustryId;
    }

    public Set<UserDTO> getManagers() {
        return managers;
    }

    public void setManagers(Set<UserDTO> managers) {
        this.managers = managers;
    }

    public Set<UserDTO> getUsers() {
        return users;
    }

    public void setUsers(Set<UserDTO> users) {
        this.users = users;
    }

    public Set<UserDTO> getQaUsers() {
        return qaUsers;
    }

    public void setQaUsers(Set<UserDTO> qaUsers) {
        this.qaUsers = qaUsers;
    }

    public Set<EmailTemplateDTO> getEmailTemplates() {
        return emailTemplates;
    }

    public void setEmailTemplates(Set<EmailTemplateDTO> emailTemplates) {
        this.emailTemplates = emailTemplates;
    }

    public List<CampaignPortalUserDTO> getPortalUsers() {
        return portalUsers;
    }

    public void setPortalUsers(List<CampaignPortalUserDTO> portalUsers) {
        this.portalUsers = portalUsers;
    }

    public List<AppointmentAttendeeDTO> getAppointmentAttendees() {
        return appointmentAttendees;
    }

    public void setAppointmentAttendees(List<AppointmentAttendeeDTO> appointmentAttendees) {
        this.appointmentAttendees = appointmentAttendees;
    }

    public List<ClientListDTO> getClientLists() {
        return clientLists;
    }

    public void setClientLists(List<ClientListDTO> clientLists) {
        this.clientLists = clientLists;
    }

    public Set<OutcomeDTO> getOutcomes() {
        return outcomes;
    }

    public void setOutcomes(Set<OutcomeDTO> outcomes) {
        this.outcomes = outcomes;
    }

    public Set<ClientCampaignScriptDTO> getScripts() {
        return scripts;
    }

    public void setScripts(Set<ClientCampaignScriptDTO> scripts) {
        this.scripts = scripts;
    }

    public Set<ClientCampaignSmsDTO> getSmss() {
        return smss;
    }

    public void setSmss(Set<ClientCampaignSmsDTO> smss) {
        this.smss = smss;
    }

    public Boolean getWeeklyHeadlineStats() {
        return weeklyHeadlineStats;
    }

    public void setWeeklyHeadlineStats(Boolean weeklyHeadlineStats) {
        this.weeklyHeadlineStats = weeklyHeadlineStats;
    }

    public Boolean getAllowedExportData() {
        return allowedExportData;
    }

    public void setAllowedExportData(Boolean allowedExportData) {
        this.allowedExportData = allowedExportData;
    }

    public String getSarReportIdent() {
        return sarReportIdent;
    }

    public void setSarReportIdent(String sarReportIdent) {
        this.sarReportIdent = sarReportIdent;
    }

    public LocalTime getSarReportTime() {
        return sarReportTime;
    }

    public void setSarReportTime(LocalTime sarReportTime) {
        this.sarReportTime = sarReportTime;
    }

    public String getStatsSummaryIdent() {
        return statsSummaryIdent;
    }

    public void setStatsSummaryIdent(String statsSummaryIdent) {
        this.statsSummaryIdent = statsSummaryIdent;
    }

    public LocalTime getStatsSummaryTime() {
        return statsSummaryTime;
    }

    public void setStatsSummaryTime(LocalTime statsSummaryTime) {
        this.statsSummaryTime = statsSummaryTime;
    }

    public List<String> getReportDistributionMethods() {
        return reportDistributionMethods;
    }

    public void setReportDistributionMethods(List<String> reportDistributionMethods) {
        this.reportDistributionMethods = reportDistributionMethods;
    }

    public List<Long> getPortalUsersAllowedToExport() {
        return portalUsersAllowedToExport;
    }

    public void setPortalUsersAllowedToExport(List<Long> portalUsersAllowedToExport) {
        this.portalUsersAllowedToExport = portalUsersAllowedToExport;
    }

    public List<Long> getReportsDeliveredToUsers() {
        return reportsDeliveredToUsers;
    }

    public void setReportsDeliveredToUsers(List<Long> reportsDeliveredToUsers) {
        this.reportsDeliveredToUsers = reportsDeliveredToUsers;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getMeetingDuration() {
        return meetingDuration;
    }

    public void setMeetingDuration(String meetingDuration) {
        this.meetingDuration = meetingDuration;
    }

    public String getSpecialRequests() {
        return specialRequests;
    }

    public void setSpecialRequests(String specialRequests) {
        this.specialRequests = specialRequests;
    }

    public String getResultsPerHour() {
        return resultsPerHour;
    }

    public void setResultsPerHour(String resultsPerHour) {
        this.resultsPerHour = resultsPerHour;
    }

    public String getQualityControl() {
        return qualityControl;
    }

    public void setQualityControl(String qualityControl) {
        this.qualityControl = qualityControl;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getAppointmentType() {
        return appointmentType;
    }

    public void setAppointmentType(String appointmentType) {
        this.appointmentType = appointmentType;
    }

    public String getSurveyName() {
        return surveyName;
    }

    public void setSurveyName(String surveyName) {
        this.surveyName = surveyName;
    }

    public String getBookingName() {
        return bookingName;
    }

    public void setBookingName(String bookingName) {
        this.bookingName = bookingName;
    }

    public Set<SurveyQuestionDTO> getSurveyQuestions() {
        return surveyQuestions;
    }

    public void setSurveyQuestions(Set<SurveyQuestionDTO> surveyQuestions) {
        this.surveyQuestions = surveyQuestions;
    }

    public Set<BookingQuestionDTO> getBookingQuestions() {
        return bookingQuestions;
    }

    public void setBookingQuestions(Set<BookingQuestionDTO> bookingQuestions) {
        this.bookingQuestions = bookingQuestions;
    }

    public Boolean getSmsTabNotRequired() {
        return smsTabNotRequired;
    }

    public void setSmsTabNotRequired(Boolean smsTabNotRequired) {
        this.smsTabNotRequired = smsTabNotRequired;
    }

    public String getControllerFullName() {
        return controllerFullName;
    }

    public void setControllerFullName(String controllerFullName) {
        this.controllerFullName = controllerFullName;
    }

    public String getControllerJobRole() {
        return controllerJobRole;
    }

    public void setControllerJobRole(String controllerJobRole) {
        this.controllerJobRole = controllerJobRole;
    }

    public String getControllerEmail() {
        return controllerEmail;
    }

    public void setControllerEmail(String controllerEmail) {
        this.controllerEmail = controllerEmail;
    }

    public String getControllerPhone() {
        return controllerPhone;
    }

    public void setControllerPhone(String controllerPhone) {
        this.controllerPhone = controllerPhone;
    }

    public String getControllerCompanyName() {
        return controllerCompanyName;
    }

    public void setControllerCompanyName(String controllerCompanyName) {
        this.controllerCompanyName = controllerCompanyName;
    }

    public String getControllerCompanyReg() {
        return controllerCompanyReg;
    }

    public void setControllerCompanyReg(String controllerCompanyReg) {
        this.controllerCompanyReg = controllerCompanyReg;
    }

    public String getControllerCompanyAddress() {
        return controllerCompanyAddress;
    }

    public void setControllerCompanyAddress(String controllerCompanyAddress) {
        this.controllerCompanyAddress = controllerCompanyAddress;
    }

    public String getSarsFullName() {
        return sarsFullName;
    }

    public void setSarsFullName(String sarsFullName) {
        this.sarsFullName = sarsFullName;
    }

    public String getSarsJobRole() {
        return sarsJobRole;
    }

    public void setSarsJobRole(String sarsJobRole) {
        this.sarsJobRole = sarsJobRole;
    }

    public String getSarsEmail() {
        return sarsEmail;
    }

    public void setSarsEmail(String sarsEmail) {
        this.sarsEmail = sarsEmail;
    }

    public String getSarsPhone() {
        return sarsPhone;
    }

    public void setSarsPhone(String sarsPhone) {
        this.sarsPhone = sarsPhone;
    }

    public String getSarsCompanyName() {
        return sarsCompanyName;
    }

    public void setSarsCompanyName(String sarsCompanyName) {
        this.sarsCompanyName = sarsCompanyName;
    }

    public String getSarsCompanyReg() {
        return sarsCompanyReg;
    }

    public void setSarsCompanyReg(String sarsCompanyReg) {
        this.sarsCompanyReg = sarsCompanyReg;
    }

    public String getSarsCompanyAddress() {
        return sarsCompanyAddress;
    }

    public void setSarsCompanyAddress(String sarsCompanyAddress) {
        this.sarsCompanyAddress = sarsCompanyAddress;
    }

    public String getLiaHyperlink() {
        return liaHyperlink;
    }

    public void setLiaHyperlink(String liaHyperlink) {
        this.liaHyperlink = liaHyperlink;
    }

    public Integer getCallPerHour() {
        return callPerHour;
    }

    public void setCallPerHour(Integer callPerHour) {
        this.callPerHour = callPerHour;
    }

    public Integer getCallPerHourLow() {
        return callPerHourLow;
    }

    public void setCallPerHourLow(Integer callPerHourLow) {
        this.callPerHourLow = callPerHourLow;
    }

    public Integer getCallPerHourHigh() {
        return callPerHourHigh;
    }

    public void setCallPerHourHigh(Integer callPerHourHigh) {
        this.callPerHourHigh = callPerHourHigh;
    }

    public Integer getConversationsPerHour() {
        return conversationsPerHour;
    }

    public void setConversationsPerHour(Integer conversationsPerHour) {
        this.conversationsPerHour = conversationsPerHour;
    }

    public Integer getConversationsPerHourLow() {
        return conversationsPerHourLow;
    }

    public void setConversationsPerHourLow(Integer conversationsPerHourLow) {
        this.conversationsPerHourLow = conversationsPerHourLow;
    }

    public Integer getConversationsPerHourHigh() {
        return conversationsPerHourHigh;
    }

    public void setConversationsPerHourHigh(Integer conversationsPerHourHigh) {
        this.conversationsPerHourHigh = conversationsPerHourHigh;
    }

    public Double getContactRate() {
        return contactRate;
    }

    public void setContactRate(Double contactRate) {
        this.contactRate = contactRate;
    }

    public Double getContactRateLow() {
        return contactRateLow;
    }

    public void setContactRateLow(Double contactRateLow) {
        this.contactRateLow = contactRateLow;
    }

    public Double getContactRateHigh() {
        return contactRateHigh;
    }

    public void setContactRateHigh(Double contactRateHigh) {
        this.contactRateHigh = contactRateHigh;
    }

    public Integer getHoursPerLead() {
        return hoursPerLead;
    }

    public void setHoursPerLead(Integer hoursPerLead) {
        this.hoursPerLead = hoursPerLead;
    }

    public Integer getHoursPerLeadLow() {
        return hoursPerLeadLow;
    }

    public void setHoursPerLeadLow(Integer hoursPerLeadLow) {
        this.hoursPerLeadLow = hoursPerLeadLow;
    }

    public Integer getHoursPerLeadHigh() {
        return hoursPerLeadHigh;
    }

    public void setHoursPerLeadHigh(Integer hoursPerLeadHigh) {
        this.hoursPerLeadHigh = hoursPerLeadHigh;
    }

    public Double getConversionRate() {
        return conversionRate;
    }

    public void setConversionRate(Double conversionRate) {
        this.conversionRate = conversionRate;
    }

    public Double getConversionRateLow() {
        return conversionRateLow;
    }

    public void setConversionRateLow(Double conversionRateLow) {
        this.conversionRateLow = conversionRateLow;
    }

    public Double getConversionRateHigh() {
        return conversionRateHigh;
    }

    public void setConversionRateHigh(Double conversionRateHigh) {
        this.conversionRateHigh = conversionRateHigh;
    }

    public Double getLeadToSaleConversion() {
        return leadToSaleConversion;
    }

    public void setLeadToSaleConversion(Double leadToSaleConversion) {
        this.leadToSaleConversion = leadToSaleConversion;
    }

    public Double getLeadToSaleConversionLow() {
        return leadToSaleConversionLow;
    }

    public void setLeadToSaleConversionLow(Double leadToSaleConversionLow) {
        this.leadToSaleConversionLow = leadToSaleConversionLow;
    }

    public Double getLeadToSaleConversionHigh() {
        return leadToSaleConversionHigh;
    }

    public void setLeadToSaleConversionHigh(Double leadToSaleConversionHigh) {
        this.leadToSaleConversionHigh = leadToSaleConversionHigh;
    }

    public BigDecimal getAverageSaleValue() {
        return averageSaleValue;
    }

    public void setAverageSaleValue(BigDecimal averageSaleValue) {
        this.averageSaleValue = averageSaleValue;
    }

    public BigDecimal getAverageSaleValueLow() {
        return averageSaleValueLow;
    }

    public void setAverageSaleValueLow(BigDecimal averageSaleValueLow) {
        this.averageSaleValueLow = averageSaleValueLow;
    }

    public BigDecimal getAverageSaleValueHigh() {
        return averageSaleValueHigh;
    }

    public void setAverageSaleValueHigh(BigDecimal averageSaleValueHigh) {
        this.averageSaleValueHigh = averageSaleValueHigh;
    }

    public String getStrategyDocument() {
        return strategyDocument;
    }

    public void setStrategyDocument(String strategyDocument) {
        this.strategyDocument = strategyDocument;
    }

    public Set<ClientCampaignAddDocDTO> getAddDocs() {
        return addDocs;
    }

    public void setAddDocs(Set<ClientCampaignAddDocDTO> addDocs) {
        this.addDocs = addDocs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ClientCampaignDTO clientCampaignDTO = (ClientCampaignDTO) o;
        if (clientCampaignDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clientCampaignDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this)
            .append("id", id)
            .append("name", name)
            .append("createdDate", createdDate)
            .append("createdById", createdById)
            .append("createdByLogin", createdByLogin)
            .append("templateId", templateId)
            .append("clientCompanyId", clientCompanyId)
            .append("clientCompanyName", clientCompanyName)
            .append("clientCompanyIndustryId", clientCompanyIndustryId)
            .append("managers", managers)
            .append("users", users)
            .append("qaUsers", qaUsers)
            .append("emailTemplates", emailTemplates)
            .append("portalUsers", portalUsers)
            .append("appointmentAttendees", appointmentAttendees)
            .append("clientLists", clientLists)
            .append("outcomes", outcomes)
            .append("scripts", scripts)
            .append("smss", smss)
            .append("weeklyHeadlineStats", weeklyHeadlineStats)
            .append("allowedExportData", allowedExportData)
            .append("sarReportIdent", sarReportIdent)
            .append("sarReportTime", sarReportTime)
            .append("statsSummaryIdent", statsSummaryIdent)
            .append("statsSummaryTime", statsSummaryTime)
            .append("reportDistributionMethods", reportDistributionMethods)
            .append("portalUsersAllowedToExport", portalUsersAllowedToExport)
            .append("reportsDeliveredToUsers", reportsDeliveredToUsers)
            .append("website", website)
            .append("meetingDuration", meetingDuration)
            .append("specialRequests", specialRequests)
            .append("resultsPerHour", resultsPerHour)
            .append("qualityControl", qualityControl)
            .append("comments", comments)
            .append("appointmentType", appointmentType)
            .append("surveyName", surveyName)
            .append("bookingName", bookingName)
            .append("surveyQuestions", surveyQuestions)
            .append("bookingQuestions", bookingQuestions)
            .append("smsTabNotRequired", smsTabNotRequired)
            .append("controllerFullName", controllerFullName)
            .append("controllerJobRole", controllerJobRole)
            .append("controllerEmail", controllerEmail)
            .append("controllerPhone", controllerPhone)
            .append("controllerCompanyName", controllerCompanyName)
            .append("controllerCompanyReg", controllerCompanyReg)
            .append("controllerCompanyAddress", controllerCompanyAddress)
            .append("sarsFullName", sarsFullName)
            .append("sarsJobRole", sarsJobRole)
            .append("sarsEmail", sarsEmail)
            .append("sarsPhone", sarsPhone)
            .append("sarsCompanyName", sarsCompanyName)
            .append("sarsCompanyReg", sarsCompanyReg)
            .append("sarsCompanyAddress", sarsCompanyAddress)
            .append("liaHyperlink", liaHyperlink)
            .append("callPerHour", callPerHour)
            .append("callPerHourLow", callPerHourLow)
            .append("callPerHourHigh", callPerHourHigh)
            .append("conversationsPerHour", conversationsPerHour)
            .append("conversationsPerHourLow", conversationsPerHourLow)
            .append("conversationsPerHourHigh", conversationsPerHourHigh)
            .append("contactRate", contactRate)
            .append("contactRateLow", contactRateLow)
            .append("contactRateHigh", contactRateHigh)
            .append("hoursPerLead", hoursPerLead)
            .append("hoursPerLeadLow", hoursPerLeadLow)
            .append("hoursPerLeadHigh", hoursPerLeadHigh)
            .append("conversionRate", conversionRate)
            .append("conversionRateLow", conversionRateLow)
            .append("conversionRateHigh", conversionRateHigh)
            .append("leadToSaleConversion", leadToSaleConversion)
            .append("leadToSaleConversionLow", leadToSaleConversionLow)
            .append("leadToSaleConversionHigh", leadToSaleConversionHigh)
            .append("averageSaleValue", averageSaleValue)
            .append("averageSaleValueLow", averageSaleValueLow)
            .append("averageSaleValueHigh", averageSaleValueHigh)
            .append("strategyDocument", strategyDocument)
            .append("addDocs", addDocs)
            .toString();
    }
}
