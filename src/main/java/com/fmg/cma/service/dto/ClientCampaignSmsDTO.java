package com.fmg.cma.service.dto;


import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the ClientCampaignSms entity.
 */
public class ClientCampaignSmsDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private String excerpt;

    private Long clientCampaignId;

    private String clientCampaignName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public Long getClientCampaignId() {
        return clientCampaignId;
    }

    public void setClientCampaignId(Long clientCampaignId) {
        this.clientCampaignId = clientCampaignId;
    }

    public String getClientCampaignName() {
        return clientCampaignName;
    }

    public void setClientCampaignName(String clientCampaignName) {
        this.clientCampaignName = clientCampaignName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ClientCampaignSmsDTO clientCampaignSmsDTO = (ClientCampaignSmsDTO) o;
        if (clientCampaignSmsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clientCampaignSmsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClientCampaignSmsDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", excerpt='" + getExcerpt() + "'" +
            "}";
    }
}
