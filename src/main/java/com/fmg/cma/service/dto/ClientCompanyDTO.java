package com.fmg.cma.service.dto;


import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A DTO for the ClientCompany entity.
 */
public class ClientCompanyDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private ZonedDateTime createdDate;

    private Long createdById;

    private String createdByLogin;

    private Long statusId;

    private String statusName;

    private ZonedDateTime updatedDate;

    private Long updatedById;

    private String updatedByLogin;

    private Long industryId;

    private String industryName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public Long getCreatedById() {
        return createdById;
    }

    public void setCreatedById(Long userId) {
        this.createdById = userId;
    }

    public String getCreatedByLogin() {
        return createdByLogin;
    }

    public void setCreatedByLogin(String userLogin) {
        this.createdByLogin = userLogin;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long clientCompanyStatusId) {
        this.statusId = clientCompanyStatusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String clientCompanyStatusName) {
        this.statusName = clientCompanyStatusName;
    }

    public ZonedDateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(ZonedDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Long getUpdatedById() {
        return updatedById;
    }

    public void setUpdatedById(Long updatedById) {
        this.updatedById = updatedById;
    }

    public String getUpdatedByLogin() {
        return updatedByLogin;
    }

    public void setUpdatedByLogin(String updatedByLogin) {
        this.updatedByLogin = updatedByLogin;
    }

    public Long getIndustryId() {
        return industryId;
    }

    public void setIndustryId(Long industryId) {
        this.industryId = industryId;
    }

    public String getIndustryName() {
        return industryName;
    }

    public void setIndustryName(String industryName) {
        this.industryName = industryName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ClientCompanyDTO clientCompanyDTO = (ClientCompanyDTO) o;
        if (clientCompanyDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clientCompanyDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClientCompanyDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            "}";
    }
}
