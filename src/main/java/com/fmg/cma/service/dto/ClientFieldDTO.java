package com.fmg.cma.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the ClientField entity.
 */
public class ClientFieldDTO implements Serializable {

    private Long id;

    private Boolean visible;

    private Boolean editable;

    private Boolean isOrgInformation;

    private Long fieldId;

    private String fieldName;

    private Long fieldOrder;

    private Long listId;

    private String listName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Boolean isEditable() {
        return editable;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }

    public Boolean getIsOrgInformation() {
        return isOrgInformation;
    }

    public void setIsOrgInformation(boolean orgInformation) {
        isOrgInformation = orgInformation;
    }

    public Long getFieldId() {
        return fieldId;
    }

    public void setFieldId(Long fieldId) {
        this.fieldId = fieldId;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public Long getFieldOrder() {
        return fieldOrder;
    }

    public void setFieldOrder(Long fieldOrder) {
        this.fieldOrder = fieldOrder;
    }

    public Long getListId() {
        return listId;
    }

    public void setListId(Long clientListId) {
        this.listId = clientListId;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String clientListName) {
        this.listName = clientListName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ClientFieldDTO clientFieldDTO = (ClientFieldDTO) o;
        if(clientFieldDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clientFieldDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClientFieldDTO{" +
            "id=" + getId() +
            ", visible='" + isVisible() + "'" +
            ", editable='" + isEditable() + "'" +
            "}";
    }
}
