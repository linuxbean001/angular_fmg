package com.fmg.cma.service.dto;


import com.fmg.cma.domain.CampaignPortalUser;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the ClientList entity.
 */
public class ClientListDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private Long clientCampaignId;

    private String clientCampaignName;

    private ClientListMappingDto mapping;

    private Integer rowCount;

    private Set<CampaignPortalUserDTO> portalUsers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getClientCampaignId() {
        return clientCampaignId;
    }

    public void setClientCampaignId(Long clientCampaignId) {
        this.clientCampaignId = clientCampaignId;
    }

    public String getClientCampaignName() {
        return clientCampaignName;
    }

    public void setClientCampaignName(String clientCampaignName) {
        this.clientCampaignName = clientCampaignName;
    }

    public ClientListMappingDto getMapping() {
        return mapping;
    }

    public void setMapping(ClientListMappingDto mapping) {
        this.mapping = mapping;
    }

    public Integer getRowCount() {
        return rowCount;
    }

    public void setRowCount(Integer rowCount) {
        this.rowCount = rowCount;
    }

    public Set<CampaignPortalUserDTO> getPortalUsers() {
        return portalUsers;
    }

    public void setPortalUsers(Set<CampaignPortalUserDTO> aPortalUsers) {
        portalUsers = aPortalUsers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ClientListDTO clientListDTO = (ClientListDTO) o;
        if (clientListDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clientListDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClientListDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
