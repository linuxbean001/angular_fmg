package com.fmg.cma.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the ClientListData entity.
 */
public class ClientListDataDTO implements Serializable {

    private Long id;

    private Long rowNum;

    private String cvalue;

    private Long clientFieldId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRowNum() {
        return rowNum;
    }

    public void setRowNum(Long rowNum) {
        this.rowNum = rowNum;
    }

    public String getCvalue() {
        return cvalue;
    }

    public void setCvalue(String cvalue) {
        this.cvalue = cvalue;
    }

    public Long getClientFieldId() {
        return clientFieldId;
    }

    public void setClientFieldId(Long clientFieldId) {
        this.clientFieldId = clientFieldId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ClientListDataDTO clientListDataDTO = (ClientListDataDTO) o;
        if(clientListDataDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clientListDataDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClientListDataDTO{" +
            "id=" + getId() +
            ", rowNum=" + getRowNum() +
            ", cvalue='" + getCvalue() + "'" +
            "}";
    }
}
