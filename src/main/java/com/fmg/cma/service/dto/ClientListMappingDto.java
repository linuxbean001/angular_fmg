package com.fmg.cma.service.dto;

import java.util.List;

public class ClientListMappingDto {
    private List<OldFieldDto> oldFields;
    private List<NewFieldDto> newFields;
    private List<NewFieldNoMappingDto> newFieldsNoMapping;

    public List<OldFieldDto> getOldFields() {
        return oldFields;
    }

    public void setOldFields(List<OldFieldDto> oldFields) {
        this.oldFields = oldFields;
    }

    public List<NewFieldDto> getNewFields() {
        return newFields;
    }

    public void setNewFields(List<NewFieldDto> newFields) {
        this.newFields = newFields;
    }

    public List<NewFieldNoMappingDto> getNewFieldsNoMapping() {
        return newFieldsNoMapping;
    }

    public void setNewFieldsNoMapping(List<NewFieldNoMappingDto> newFieldsNoMapping) {
        this.newFieldsNoMapping = newFieldsNoMapping;
    }

    @Override
    public String toString() {
        return "ClientListMappingDto{" +
            "oldFields=" + oldFields +
            ", newFields=" + newFields +
            ", newFieldsNoMapping=" + newFieldsNoMapping +
            '}';
    }
}
