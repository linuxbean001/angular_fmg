package com.fmg.cma.service.dto;

import java.util.HashMap;
import java.util.Map;

public class ClientListRowDto {

    private long rowNum;
    private Map<Long, RowValueDto> values = new HashMap<>();

    public ClientListRowDto(long rowNum) {
        this.rowNum = rowNum;
    }

    public long getRowNum() {
        return rowNum;
    }

    public Map<Long, RowValueDto> getValues() {
        return values;
    }

    @Override
    public String toString() {
        return "ClientListRowDto{" +
            "rowNum=" + rowNum +
            ", values=" + values +
            '}';
    }
}
