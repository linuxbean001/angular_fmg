package com.fmg.cma.service.dto;

import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LocalDateFilter;
import io.github.jhipster.service.filter.LongFilter;

import java.io.Serializable;


/**
 * Criteria class for the ConsoleStats entity. This class is used in ConsoleStatsResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /console-stats?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ConsoleStatsCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private LongFilter calls;

    private LongFilter skips;

    private LongFilter pending;

    private LocalDateFilter stamp;

    private LongFilter dmContact;

    private LongFilter leadSales;

    private LongFilter pipelineOpps;

    private LongFilter userId;

    public ConsoleStatsCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getCalls() {
        return calls;
    }

    public void setCalls(LongFilter calls) {
        this.calls = calls;
    }

    public LongFilter getSkips() {
        return skips;
    }

    public void setSkips(LongFilter skips) {
        this.skips = skips;
    }

    public LongFilter getPending() {
        return pending;
    }

    public void setPending(LongFilter pending) {
        this.pending = pending;
    }

    public LocalDateFilter getStamp() {
        return stamp;
    }

    public void setStamp(LocalDateFilter stamp) {
        this.stamp = stamp;
    }

    public LongFilter getDmContact() {
        return dmContact;
    }

    public void setDmContact(LongFilter dmContact) {
        this.dmContact = dmContact;
    }

    public LongFilter getLeadSales() {
        return leadSales;
    }

    public void setLeadSales(LongFilter leadSales) {
        this.leadSales = leadSales;
    }

    public LongFilter getPipelineOpps() {
        return pipelineOpps;
    }

    public void setPipelineOpps(LongFilter pipelineOpps) {
        this.pipelineOpps = pipelineOpps;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "ConsoleStatsCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (calls != null ? "calls=" + calls + ", " : "") +
            (skips != null ? "skips=" + skips + ", " : "") +
            (pending != null ? "pending=" + pending + ", " : "") +
            (stamp != null ? "stamp=" + stamp + ", " : "") +
            (dmContact != null ? "dmContact=" + dmContact + ", " : "") +
            (leadSales != null ? "leadSales=" + leadSales + ", " : "") +
            (pipelineOpps != null ? "pipelineOpps=" + pipelineOpps + ", " : "") +
            (userId != null ? "userId=" + userId + ", " : "") +
            "}";
    }

}
