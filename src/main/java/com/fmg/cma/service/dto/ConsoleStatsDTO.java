package com.fmg.cma.service.dto;


import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the ConsoleStats entity.
 */
public class ConsoleStatsDTO implements Serializable {

    private Long id;

    private long calls;

    private long skips;

    private long pending;

    private LocalDate stamp;

    private long dmContact;

    private long leadSales;

    private long pipelineOpps;

    private Long userId;

    private String userLogin;

    public ConsoleStatsDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getCalls() {
        return calls;
    }

    public void setCalls(long calls) {
        this.calls = calls;
    }

    public long getSkips() {
        return skips;
    }

    public void setSkips(long skips) {
        this.skips = skips;
    }

    public long getPending() {
        return pending;
    }

    public void setPending(long pending) {
        this.pending = pending;
    }

    public LocalDate getStamp() {
        return stamp;
    }

    public void setStamp(LocalDate stamp) {
        this.stamp = stamp;
    }

    public long getDmContact() {
        return dmContact;
    }

    public void setDmContact(long dmContact) {
        this.dmContact = dmContact;
    }

    public long getLeadSales() {
        return leadSales;
    }

    public void setLeadSales(long leadSales) {
        this.leadSales = leadSales;
    }

    public long getPipelineOpps() {
        return pipelineOpps;
    }

    public void setPipelineOpps(long pipelineOpps) {
        this.pipelineOpps = pipelineOpps;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ConsoleStatsDTO consoleStatsDTO = (ConsoleStatsDTO) o;
        if (consoleStatsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), consoleStatsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ConsoleStatsDTO{" +
            "id=" + getId() +
            ", calls=" + getCalls() +
            ", skips=" + getSkips() +
            ", pending=" + getPending() +
            ", stamp='" + getStamp() + "'" +
            ", dmContact=" + getDmContact() +
            ", leadSales=" + getLeadSales() +
            ", pipelineOpps=" + getPipelineOpps() +
            "}";
    }
}
