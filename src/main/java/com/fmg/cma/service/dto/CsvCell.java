package com.fmg.cma.service.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class CsvCell {

    private int index;
    private Object value;

    @JsonCreator
    public CsvCell(@JsonProperty("index") int index, @JsonProperty("value") Object value) {
        this.index = index;
        this.value = value;
    }

    public int getIndex() {
        return index;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CsvCell csvCell = (CsvCell) o;
        return index == csvCell.index &&
            Objects.equals(value, csvCell.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(index, value);
    }

    @Override
    public String toString() {
        return "CsvCell{" +
            "index=" + index +
            ", value='" + value + '\'' +
            '}';
    }
}
