package com.fmg.cma.service.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataListCompany {

    /**
     * The first (minimum) row num of the that company (there are multiple rows with same fields values).
     */
    private Long rowNum;

    private List<Long> rowNums;

    private String note;

    /**
     * Map of {@link com.fmg.cma.domain.ClientField#id} - {@link RowValueDto#cvalue}.
     */
    private Map<Long, String> valueMap = new HashMap<>();

    private List<DataListContact> contacts;

    private List<CallHistoryDTO> histories;

    public Long getRowNum() {
        return rowNum;
    }

    public DataListCompany setRowNum(Long rowNum) {
        this.rowNum = rowNum;
        return this;
    }

    public List<Long> getRowNums() {
        return rowNums;
    }

    public DataListCompany setRowNums(List<Long> rowNums) {
        this.rowNums = rowNums;
        return this;
    }

    public String getNote() {
        return note;
    }

    public DataListCompany setNote(String note) {
        this.note = note;
        return this;
    }

    public Map<Long, String> getValueMap() {
        return valueMap;
    }

    public DataListCompany setValueMap(Map<Long, String> valueMap) {
        this.valueMap = valueMap;
        return this;
    }

    public List<DataListContact> getContacts() {
        return contacts;
    }

    public DataListCompany setContacts(List<DataListContact> contacts) {
        this.contacts = contacts;
        return this;
    }

    public List<CallHistoryDTO> getHistories() {
        return histories;
    }

    public DataListCompany setHistories(List<CallHistoryDTO> histories) {
        this.histories = histories;
        return this;
    }
}
