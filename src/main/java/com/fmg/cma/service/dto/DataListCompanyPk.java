package com.fmg.cma.service.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;
import java.util.Objects;

public class DataListCompanyPk {

    private final Map<Long, String> valueMap;

    @JsonCreator
    public DataListCompanyPk(@JsonProperty("valueMap") Map<Long, String> valueMap) {
        this.valueMap = valueMap;
    }

    public Map<Long, String> getValueMap() {
        return valueMap;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataListCompanyPk that = (DataListCompanyPk) o;
        return Objects.equals(valueMap, that.valueMap);
    }

    @Override
    public int hashCode() {
        return Objects.hash(valueMap);
    }

    @Override
    public String toString() {
        return "DataListCompanyPk{" +
            "valueMap=" + valueMap +
            '}';
    }
}
