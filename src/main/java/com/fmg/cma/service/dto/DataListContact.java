package com.fmg.cma.service.dto;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

public class DataListContact {

    /**
     * Tab header name.
     */
    private String contactType;

    private Long rowNum;

    private String note;

    private Boolean isLeftCompany;

    // private List<ClientFieldDTO> fields;

    /**
     * Map of {@link com.fmg.cma.domain.ClientField#id} - {@link RowValueDto#cvalue}.
     */
    private Map<Long, String> valueMap = new HashMap<>();

    /**
     * See {@link com.fmg.cma.domain.CampaignListContactResult#state} & {@link com.fmg.cma.domain.DataListContactState}.
     */
    private String state;

    private Long assignUserId;

    private String assignUserLogin;

    private ZonedDateTime assignStamp;

    private Long clickedOutcomeId;

    public String getContactType() {
        return contactType;
    }

    public DataListContact setContactType(String contactType) {
        this.contactType = contactType;
        return this;
    }

    public Long getRowNum() {
        return rowNum;
    }

    public DataListContact setRowNum(Long rowNum) {
        this.rowNum = rowNum;
        return this;
    }

    public String getNote() {
        return note;
    }

    public DataListContact setNote(String note) {
        this.note = note;
        return this;
    }

    public Boolean getIsLeftCompany() {
        return isLeftCompany;
    }

    public DataListContact setIsLeftCompany(Boolean leftCompany) {
        isLeftCompany = leftCompany;
        return this;
    }

    // public List<ClientFieldDTO> getFields() {
    //     return fields;
    // }
    //
    // public DataListContact setFields(List<ClientFieldDTO> fields) {
    //     this.fields = fields;
    //     return this;
    // }

    public Map<Long, String> getValueMap() {
        return valueMap;
    }

    public DataListContact setValueMap(Map<Long, String> valueMap) {
        this.valueMap = valueMap;
        return this;
    }

    public String getState() {
        return state;
    }

    public DataListContact setState(String state) {
        this.state = state;
        return this;
    }

    public Long getAssignUserId() {
        return assignUserId;
    }

    public void setAssignUserId(Long assignUserId) {
        this.assignUserId = assignUserId;
    }

    public String getAssignUserLogin() {
        return assignUserLogin;
    }

    public void setAssignUserLogin(String assignUserLogin) {
        this.assignUserLogin = assignUserLogin;
    }

    public ZonedDateTime getAssignStamp() {
        return assignStamp;
    }

    public void setAssignStamp(ZonedDateTime assignStamp) {
        this.assignStamp = assignStamp;
    }

    public Long getClickedOutcomeId() {
        return clickedOutcomeId;
    }

    public void setClickedOutcomeId(Long clickedOutcomeId) {
        this.clickedOutcomeId = clickedOutcomeId;
    }

    @Override
    public String toString() {
        return "DataListContact{" +
            "contactType='" + contactType + '\'' +
            ", rowNum=" + rowNum +
            ", note='" + note + '\'' +
            ", isLeftCompany=" + isLeftCompany +
            ", valueMap=" + valueMap +
            '}';
    }
}
