package com.fmg.cma.service.dto;

import java.util.ArrayList;
import java.util.List;

public class DataListSummary {

    /**
     * Fields with {@link com.fmg.cma.domain.ClientField#isOrgInformation} = true.
     */
    private List<ClientFieldDTO> pkFields;

    /**
     * Companies.
     */
    private List<DataListCompany> companies = new ArrayList<>();

    /**
     * Other non-pk fields.
     */
    private List<ClientFieldDTO> fields;

    public List<ClientFieldDTO> getPkFields() {
        return pkFields;
    }

    public DataListSummary setPkFields(List<ClientFieldDTO> pkFields) {
        this.pkFields = pkFields;
        return this;
    }

    public List<DataListCompany> getCompanies() {
        return companies;
    }

    public DataListSummary setCompanies(List<DataListCompany> companies) {
        this.companies = companies;
        return this;
    }

    public List<ClientFieldDTO> getFields() {
        return fields;
    }

    public DataListSummary setFields(List<ClientFieldDTO> fields) {
        this.fields = fields;
        return this;
    }
}
