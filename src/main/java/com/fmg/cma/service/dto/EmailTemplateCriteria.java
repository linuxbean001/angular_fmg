package com.fmg.cma.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the EmailTemplate entity. This class is used in EmailTemplateResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /email-templates?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EmailTemplateCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter name;

    private StringFilter from;

    private StringFilter subject;

    private StringFilter cc;

    private StringFilter bcc;

    private StringFilter replyTo;

    private StringFilter body;

    public EmailTemplateCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getFrom() {
        return from;
    }

    public void setFrom(StringFilter from) {
        this.from = from;
    }

    public StringFilter getSubject() {
        return subject;
    }

    public void setSubject(StringFilter subject) {
        this.subject = subject;
    }

    public StringFilter getCc() {
        return cc;
    }

    public void setCc(StringFilter cc) {
        this.cc = cc;
    }

    public StringFilter getBcc() {
        return bcc;
    }

    public void setBcc(StringFilter bcc) {
        this.bcc = bcc;
    }

    public StringFilter getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(StringFilter replyTo) {
        this.replyTo = replyTo;
    }

    public StringFilter getBody() {
        return body;
    }

    public void setBody(StringFilter body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "EmailTemplateCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (from != null ? "from=" + from + ", " : "") +
                (subject != null ? "subject=" + subject + ", " : "") +
                (cc != null ? "cc=" + cc + ", " : "") +
                (bcc != null ? "bcc=" + bcc + ", " : "") +
                (replyTo != null ? "replyTo=" + replyTo + ", " : "") +
                (body != null ? "body=" + body + ", " : "") +
            "}";
    }

}
