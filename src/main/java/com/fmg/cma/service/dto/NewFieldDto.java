package com.fmg.cma.service.dto;

public class NewFieldDto {
    private CsvCell header;
    private boolean editable;
    private boolean visible;
    private boolean isOrgInformation;

    public CsvCell getHeader() {
        return header;
    }

    public void setHeader(CsvCell header) {
        this.header = header;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isOrgInformation() {
        return isOrgInformation;
    }

    public void setIsOrgInformation(boolean orgInformation) {
        isOrgInformation = orgInformation;
    }

    @Override
    public String toString() {
        return "NewFieldDto{" +
            "header=" + header +
            ", editable=" + editable +
            ", visible=" + visible +
            '}';
    }
}
