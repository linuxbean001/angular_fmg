package com.fmg.cma.service.dto;

public class NewFieldNoMappingDto {
    private String name;
    private boolean editable;
    private boolean visible;
    private boolean isOrgInformation;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isOrgInformation() {
        return isOrgInformation;
    }

    public void setIsOrgInformation(boolean orgInformation) {
        isOrgInformation = orgInformation;
    }

    @Override
    public String toString() {
        return "NewFieldNoMappingDto{" +
            "name='" + name + '\'' +
            ", editable=" + editable +
            ", visible=" + visible +
            '}';
    }
}
