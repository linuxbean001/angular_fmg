package com.fmg.cma.service.dto;

import com.fmg.cma.domain.Field;

public class OldFieldDto {
    private Field field;
    private CsvCell header;
    private boolean editable;
    private boolean visible;
    private boolean isOrgInformation;

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public CsvCell getHeader() {
        return header;
    }

    public void setHeader(CsvCell header) {
        this.header = header;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isOrgInformation() {
        return isOrgInformation;
    }

    public void setIsOrgInformation(boolean orgInformation) {
        isOrgInformation = orgInformation;
    }

    @Override
    public String toString() {
        return "OldFieldDto{" +
            "field=" + field +
            ", header=" + header +
            ", editable=" + editable +
            ", visible=" + visible +
            '}';
    }
}
