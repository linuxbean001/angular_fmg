package com.fmg.cma.service.dto;

import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

import java.io.Serializable;


/**
 * Criteria class for the Outcome entity. This class is used in OutcomeResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /outcomes?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class OutcomeCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter name;

    private LongFilter templateId;

    private BooleanFilter addSubReason;

    private BooleanFilter callPoolCycle;

    private LongFilter daysAfterCallBack;

    private BooleanFilter parked;

    private BooleanFilter qcqa;

    private BooleanFilter killRecord;

    private LongFilter killRecordAttempts;

    private BooleanFilter enterNurtureStream;

    private LongFilter outcomeSubReasonId;

    public OutcomeCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public LongFilter getTemplateId() {
        return templateId;
    }

    public void setTemplateId(LongFilter templateId) {
        this.templateId = templateId;
    }

    public BooleanFilter getAddSubReason() {
        return addSubReason;
    }

    public void setAddSubReason(BooleanFilter addSubReason) {
        this.addSubReason = addSubReason;
    }

    public BooleanFilter getCallPoolCycle() {
        return callPoolCycle;
    }

    public void setCallPoolCycle(BooleanFilter callPoolCycle) {
        this.callPoolCycle = callPoolCycle;
    }

    public LongFilter getDaysAfterCallBack() {
        return daysAfterCallBack;
    }

    public void setDaysAfterCallBack(LongFilter daysAfterCallBack) {
        this.daysAfterCallBack = daysAfterCallBack;
    }

    public BooleanFilter getParked() {
        return parked;
    }

    public void setParked(BooleanFilter parked) {
        this.parked = parked;
    }

    public BooleanFilter getQcqa() {
        return qcqa;
    }

    public void setQcqa(BooleanFilter qcqa) {
        this.qcqa = qcqa;
    }

    public BooleanFilter getKillRecord() {
        return killRecord;
    }

    public void setKillRecord(BooleanFilter killRecord) {
        this.killRecord = killRecord;
    }

    public LongFilter getKillRecordAttempts() {
        return killRecordAttempts;
    }

    public void setKillRecordAttempts(LongFilter killRecordAttempts) {
        this.killRecordAttempts = killRecordAttempts;
    }

    public BooleanFilter getEnterNurtureStream() {
        return enterNurtureStream;
    }

    public void setEnterNurtureStream(BooleanFilter enterNurtureStream) {
        this.enterNurtureStream = enterNurtureStream;
    }

    public LongFilter getOutcomeSubReasonId() {
        return outcomeSubReasonId;
    }

    public void setOutcomeSubReasonId(LongFilter outcomeSubReasonId) {
        this.outcomeSubReasonId = outcomeSubReasonId;
    }

    @Override
    public String toString() {
        return "OutcomeCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (templateId != null ? "templateId=" + templateId + ", " : "") +
            "}";
    }

}
