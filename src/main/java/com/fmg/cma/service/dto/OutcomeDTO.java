package com.fmg.cma.service.dto;


import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Outcome entity.
 */
public class OutcomeDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private Long templateId;

    private String templateName;

    private Boolean addSubReason;

    private String callPoolCycle;

    private Integer daysAfterCallBack;

    private String parked;

    private String qcqa;

    private String killRecord;

    private Integer killRecordAttempts;

    private Boolean enterNurtureStream;

    private Long typeId;

    private String typeName;

    private String categoryIdent;

    private String ownership;

    private String ownershipDuration;

    private Integer ownershipDurationCount;

    private String emailToContact;

    private String sms;

    private Long smsId;

    private String calendarInvite;

    private String moveToAnotherCampaign;

    private Long moveToCampaignId;

    private String moveToCampaignName;

    private Long clientCampaignId;

    private String clientCampaignName;

    private Long clientListId;

    private String clientListName;

    private Long emailTemplateId;

    private String emailTemplateName;

    private Boolean showCampaignBookingFormQuestions;

    private Long order;

    private Long outcomeSubReasonId;

    private String outcomeSubReasonName;

    private String linkToSchedule;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Long clientCampaignTemplateId) {
        this.templateId = clientCampaignTemplateId;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String clientCampaignTemplateName) {
        this.templateName = clientCampaignTemplateName;
    }

    public Boolean getAddSubReason() {
        return addSubReason;
    }

    public void setAddSubReason(Boolean addSubReason) {
        this.addSubReason = addSubReason;
    }

    public String getCallPoolCycle() {
        return callPoolCycle;
    }

    public void setCallPoolCycle(String callPoolCycle) {
        this.callPoolCycle = callPoolCycle;
    }

    public Integer getDaysAfterCallBack() {
        return daysAfterCallBack;
    }

    public void setDaysAfterCallBack(Integer daysAfterCallBack) {
        this.daysAfterCallBack = daysAfterCallBack;
    }

    public String getParked() {
        return parked;
    }

    public void setParked(String parked) {
        this.parked = parked;
    }

    public String getQcqa() {
        return qcqa;
    }

    public void setQcqa(String qcqa) {
        this.qcqa = qcqa;
    }

    public String getKillRecord() {
        return killRecord;
    }

    public void setKillRecord(String killRecord) {
        this.killRecord = killRecord;
    }

    public Integer getKillRecordAttempts() {
        return killRecordAttempts;
    }

    public void setKillRecordAttempts(Integer killRecordAttempts) {
        this.killRecordAttempts = killRecordAttempts;
    }

    public Boolean getEnterNurtureStream() {
        return enterNurtureStream;
    }

    public void setEnterNurtureStream(Boolean enterNurtureStream) {
        this.enterNurtureStream = enterNurtureStream;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getCategoryIdent() {
        return categoryIdent;
    }

    public void setCategoryIdent(String categoryIdent) {
        this.categoryIdent = categoryIdent;
    }

    public String getOwnership() {
        return ownership;
    }

    public void setOwnership(String ownership) {
        this.ownership = ownership;
    }

    public String getOwnershipDuration() {
        return ownershipDuration;
    }

    public void setOwnershipDuration(String ownershipDuration) {
        this.ownershipDuration = ownershipDuration;
    }

    public Integer getOwnershipDurationCount() {
        return ownershipDurationCount;
    }

    public void setOwnershipDurationCount(Integer ownershipDurationCount) {
        this.ownershipDurationCount = ownershipDurationCount;
    }

    public String getEmailToContact() {
        return emailToContact;
    }

    public void setEmailToContact(String emailToContact) {
        this.emailToContact = emailToContact;
    }

    public String getSms() {
        return sms;
    }

    public void setSms(String sms) {
        this.sms = sms;
    }

    public Long getSmsId() {
        return smsId;
    }

    public void setSmsId(Long smsId) {
        this.smsId = smsId;
    }

    public String getCalendarInvite() {
        return calendarInvite;
    }

    public void setCalendarInvite(String calendarInvite) {
        this.calendarInvite = calendarInvite;
    }

    public String getMoveToAnotherCampaign() {
        return moveToAnotherCampaign;
    }

    public void setMoveToAnotherCampaign(String moveToAnotherCampaign) {
        this.moveToAnotherCampaign = moveToAnotherCampaign;
    }

    public Long getMoveToCampaignId() {
        return moveToCampaignId;
    }

    public void setMoveToCampaignId(Long moveToCampaignId) {
        this.moveToCampaignId = moveToCampaignId;
    }

    public String getMoveToCampaignName() {
        return moveToCampaignName;
    }

    public void setMoveToCampaignName(String moveToCampaignName) {
        this.moveToCampaignName = moveToCampaignName;
    }

    public Long getClientCampaignId() {
        return clientCampaignId;
    }

    public void setClientCampaignId(Long clientCampaignId) {
        this.clientCampaignId = clientCampaignId;
    }

    public String getClientCampaignName() {
        return clientCampaignName;
    }

    public void setClientCampaignName(String clientCampaignName) {
        this.clientCampaignName = clientCampaignName;
    }

    public Long getClientListId() {
        return clientListId;
    }

    public void setClientListId(Long clientListId) {
        this.clientListId = clientListId;
    }

    public String getClientListName() {
        return clientListName;
    }

    public void setClientListName(String clientListName) {
        this.clientListName = clientListName;
    }

    public Long getEmailTemplateId() {
        return emailTemplateId;
    }

    public void setEmailTemplateId(Long emailTemplateId) {
        this.emailTemplateId = emailTemplateId;
    }

    public String getEmailTemplateName() {
        return emailTemplateName;
    }

    public void setEmailTemplateName(String emailTemplateName) {
        this.emailTemplateName = emailTemplateName;
    }

    public Boolean getShowCampaignBookingFormQuestions() {
        return showCampaignBookingFormQuestions;
    }

    public void setShowCampaignBookingFormQuestions(Boolean showCampaignBookingFormQuestions) {
        this.showCampaignBookingFormQuestions = showCampaignBookingFormQuestions;
    }

    public Long getOrder() {
        return order;
    }

    public void setOrder(Long order) {
        this.order = order;
    }

    public Long getOutcomeSubReasonId() {
        return outcomeSubReasonId;
    }

    public void setOutcomeSubReasonId(Long outcomeSubReasonId) {
        this.outcomeSubReasonId = outcomeSubReasonId;
    }

    public String getOutcomeSubReasonName() {
        return outcomeSubReasonName;
    }

    public void setOutcomeSubReasonName(String outcomeSubReasonName) {
        this.outcomeSubReasonName = outcomeSubReasonName;
    }

    public String getLinkToSchedule() {
        return linkToSchedule;
    }

    public void setLinkToSchedule(String linkToSchedule) {
        this.linkToSchedule = linkToSchedule;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OutcomeDTO outcomeDTO = (OutcomeDTO) o;
        if (outcomeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), outcomeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OutcomeDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
