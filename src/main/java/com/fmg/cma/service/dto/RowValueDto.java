package com.fmg.cma.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import static com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_DEFAULT)
public class RowValueDto {

    private String cvalue;

    public String getCvalue() {
        return cvalue;
    }

    public RowValueDto setCvalue(String cvalue) {
        this.cvalue = cvalue;
        return this;
    }

    @Override
    public String toString() {
        return "RowValueDto{" +
            "cvalue='" + cvalue + '\'' +
            '}';
    }
}
