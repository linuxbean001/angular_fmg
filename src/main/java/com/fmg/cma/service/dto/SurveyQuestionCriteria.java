package com.fmg.cma.service.dto;

import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

import java.io.Serializable;






/**
 * Criteria class for the SurveyQuestion entity. This class is used in SurveyQuestionResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /survey-questions?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class SurveyQuestionCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter name;

    private StringFilter questionText;

    private LongFilter order;

    private BooleanFilter isVisible;

    private LongFilter fieldsId;

    private LongFilter clientCampaignId;

    public SurveyQuestionCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getQuestionText() {
        return questionText;
    }

    public void setQuestionText(StringFilter questionText) {
        this.questionText = questionText;
    }

    public LongFilter getOrder() {
        return order;
    }

    public void setOrder(LongFilter order) {
        this.order = order;
    }

    public BooleanFilter getIsVisible() {
        return isVisible;
    }

    public void setIsVisible(BooleanFilter isVisible) {
        this.isVisible = isVisible;
    }

    public LongFilter getFieldsId() {
        return fieldsId;
    }

    public void setFieldsId(LongFilter fieldsId) {
        this.fieldsId = fieldsId;
    }

    public LongFilter getClientCampaignId() {
        return clientCampaignId;
    }

    public void setClientCampaignId(LongFilter clientCampaignId) {
        this.clientCampaignId = clientCampaignId;
    }

    @Override
    public String toString() {
        return "SurveyQuestionCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (questionText != null ? "questionText=" + questionText + ", " : "") +
                (order != null ? "order=" + order + ", " : "") +
                (isVisible != null ? "isVisible=" + isVisible + ", " : "") +
                (fieldsId != null ? "fieldsId=" + fieldsId + ", " : "") +
                (clientCampaignId != null ? "clientCampaignId=" + clientCampaignId + ", " : "") +
            "}";
    }

}
