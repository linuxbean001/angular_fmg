package com.fmg.cma.service.dto;


import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the SurveyQuestion entity.
 */
public class SurveyQuestionDTO implements Serializable {

    private Long id;

    private String name;

    private String questionText;

    private Long order;

    private Boolean isVisible;

    private Long clientCampaignId;

    private String clientCampaignName;

    private Set<SurveyQuestionFieldDTO> fields;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public Long getOrder() {
        return order;
    }

    public void setOrder(Long order) {
        this.order = order;
    }

    public Boolean isIsVisible() {
        return isVisible;
    }

    public void setIsVisible(Boolean isVisible) {
        this.isVisible = isVisible;
    }

    public Long getClientCampaignId() {
        return clientCampaignId;
    }

    public void setClientCampaignId(Long clientCampaignId) {
        this.clientCampaignId = clientCampaignId;
    }

    public String getClientCampaignName() {
        return clientCampaignName;
    }

    public void setClientCampaignName(String clientCampaignName) {
        this.clientCampaignName = clientCampaignName;
    }

    public Set<SurveyQuestionFieldDTO> getFields() {
        return fields;
    }

    public void setFields(Set<SurveyQuestionFieldDTO> fields) {
        this.fields = fields;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SurveyQuestionDTO surveyQuestionDTO = (SurveyQuestionDTO) o;
        if(surveyQuestionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), surveyQuestionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SurveyQuestionDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", questionText='" + getQuestionText() + "'" +
            ", order=" + getOrder() +
            ", isVisible='" + isIsVisible() + "'" +
            "}";
    }
}
