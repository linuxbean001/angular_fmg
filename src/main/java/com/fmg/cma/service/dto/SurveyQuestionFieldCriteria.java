package com.fmg.cma.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the SurveyQuestionField entity. This class is used in SurveyQuestionFieldResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /survey-question-fields?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class SurveyQuestionFieldCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter name;

    private StringFilter dataType;

    private StringFilter category;

    private StringFilter prePopulationData;

    private BooleanFilter isVisible;

    private BooleanFilter isRequired;

    private LongFilter surveyQuestionId;

    public SurveyQuestionFieldCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getDataType() {
        return dataType;
    }

    public void setDataType(StringFilter dataType) {
        this.dataType = dataType;
    }

    public StringFilter getCategory() {
        return category;
    }

    public void setCategory(StringFilter category) {
        this.category = category;
    }

    public StringFilter getPrePopulationData() {
        return prePopulationData;
    }

    public void setPrePopulationData(StringFilter prePopulationData) {
        this.prePopulationData = prePopulationData;
    }

    public BooleanFilter getIsVisible() {
        return isVisible;
    }

    public void setIsVisible(BooleanFilter isVisible) {
        this.isVisible = isVisible;
    }

    public BooleanFilter getIsRequired() {
        return isRequired;
    }

    public void setIsRequired(BooleanFilter isRequired) {
        this.isRequired = isRequired;
    }

    public LongFilter getSurveyQuestionId() {
        return surveyQuestionId;
    }

    public void setSurveyQuestionId(LongFilter surveyQuestionId) {
        this.surveyQuestionId = surveyQuestionId;
    }

    @Override
    public String toString() {
        return "SurveyQuestionFieldCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (dataType != null ? "dataType=" + dataType + ", " : "") +
                (category != null ? "category=" + category + ", " : "") +
                (prePopulationData != null ? "prePopulationData=" + prePopulationData + ", " : "") +
                (isVisible != null ? "isVisible=" + isVisible + ", " : "") +
                (isRequired != null ? "isRequired=" + isRequired + ", " : "") +
                (surveyQuestionId != null ? "surveyQuestionId=" + surveyQuestionId + ", " : "") +
            "}";
    }

}
