package com.fmg.cma.service.dto;


import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the UserCompany entity.
 */
public class UserCompanyDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    public UserCompanyDTO() {
    }

    public UserCompanyDTO(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserCompanyDTO userCompanyDTO = (UserCompanyDTO) o;
        if (userCompanyDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userCompanyDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserCompanyDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
