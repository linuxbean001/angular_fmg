package com.fmg.cma.service.dto;

import io.github.jhipster.service.filter.StringFilter;

import java.io.Serializable;

public class UserCriteria implements Serializable {
    private static final long serialVersionUID = 1L;

    private StringFilter email;

    public UserCriteria() {
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "ClientCampaignCriteria{" +
            (email != null ? "email=" + email + ", " : "") +
            "}";
    }

}
