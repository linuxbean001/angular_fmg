package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.*;
import com.fmg.cma.service.dto.AlertDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Alert and its DTO AlertDTO.
 */
@Mapper(componentModel = "spring", uses = {ClientCampaignMapper.class, ClientListMapper.class})
public interface AlertMapper extends EntityMapper<AlertDTO, Alert> {

    @Mapping(source = "clientCampaign.id", target = "clientCampaignId")
    @Mapping(source = "clientCampaign.name", target = "clientCampaignName")
    @Mapping(source = "clientList.id", target = "clientListId")
    @Mapping(source = "clientList.name", target = "clientListName")
    AlertDTO toDto(Alert alert);

    @Mapping(source = "clientCampaignId", target = "clientCampaign")
    @Mapping(source = "clientListId", target = "clientList")
    @Mapping(target = "alertUsers", ignore = true)
    Alert toEntity(AlertDTO alertDTO);

    default Alert fromId(Long id) {
        if (id == null) {
            return null;
        }
        Alert alert = new Alert();
        alert.setId(id);
        return alert;
    }
}
