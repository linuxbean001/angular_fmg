package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.*;
import com.fmg.cma.service.dto.AlertUserDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity AlertUser and its DTO AlertUserDTO.
 */
@Mapper(componentModel = "spring", uses = {AlertMapper.class, UserMapper.class})
public interface AlertUserMapper extends EntityMapper<AlertUserDTO, AlertUser> {

    @Mapping(source = "alert.id", target = "alertId")
    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.login", target = "userLogin")
    AlertUserDTO toDto(AlertUser alertUser);

    @Mapping(source = "alertId", target = "alert")
    @Mapping(source = "userId", target = "user")
    AlertUser toEntity(AlertUserDTO alertUserDTO);

    default AlertUser fromId(Long id) {
        if (id == null) {
            return null;
        }
        AlertUser alertUser = new AlertUser();
        alertUser.setId(id);
        return alertUser;
    }
}
