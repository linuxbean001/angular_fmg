package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.AppointmentAttendee;
import com.fmg.cma.service.dto.AppointmentAttendeeDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity AppointmentAttendee and its DTO AppointmentAttendeeDTO.
 */
@Mapper(componentModel = "spring", uses = {ClientCampaignMapper.class})
public interface AppointmentAttendeeMapper extends EntityMapper<AppointmentAttendeeDTO, AppointmentAttendee> {

    @Mapping(source = "clientCampaign.id", target = "clientCampaignId")
    @Mapping(source = "clientCampaign.name", target = "clientCampaignName")
    AppointmentAttendeeDTO toDto(AppointmentAttendee appointmentAttendee);

    @Mapping(source = "clientCampaignId", target = "clientCampaign")
    AppointmentAttendee toEntity(AppointmentAttendeeDTO appointmentAttendeeDTO);

    default AppointmentAttendee fromId(Long id) {
        if (id == null) {
            return null;
        }
        AppointmentAttendee appointmentAttendee = new AppointmentAttendee();
        appointmentAttendee.setId(id);
        return appointmentAttendee;
    }
}
