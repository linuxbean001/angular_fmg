package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.BookingQuestionField;
import com.fmg.cma.service.dto.BookingQuestionFieldDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Set;

/**
 * Mapper for the entity BookingQuestionField and its DTO BookingQuestionFieldDTO.
 */
@Mapper(componentModel = "spring", uses = {BookingQuestionMapper.class})
public interface BookingQuestionFieldMapper extends EntityMapper<BookingQuestionFieldDTO, BookingQuestionField> {

    @Mapping(source = "bookingQuestion.id", target = "bookingQuestionId")
    @Mapping(source = "bookingQuestion.name", target = "bookingQuestionName")
    BookingQuestionFieldDTO toDto(BookingQuestionField bookingQuestionField);

    @Mapping(source = "bookingQuestionId", target = "bookingQuestion")
    BookingQuestionField toEntity(BookingQuestionFieldDTO bookingQuestionFieldDTO);

    Set<BookingQuestionField> toEntitySet(Set<BookingQuestionFieldDTO> dtoList);

    default BookingQuestionField fromId(Long id) {
        if (id == null) {
            return null;
        }
        BookingQuestionField bookingQuestionField = new BookingQuestionField();
        bookingQuestionField.setId(id);
        return bookingQuestionField;
    }
}
