package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.BookingQuestion;
import com.fmg.cma.repository.BookingQuestionRepository;
import com.fmg.cma.repository.ClientCampaignRepository;
import com.fmg.cma.service.dto.BookingQuestionDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

/**
 * Mapper for the entity BookingQuestion and its DTO BookingQuestionDTO.
 */
@Mapper(componentModel = "spring", uses = {ClientCampaignMapper.class})
public abstract class BookingQuestionMapper implements EntityMapper<BookingQuestionDTO, BookingQuestion> {

    @Autowired
    private BookingQuestionRepository bookingQuestionRepository;

    @Autowired
    private ClientCampaignRepository clientCampaignRepository;

    @Mapping(source = "clientCampaign.id", target = "clientCampaignId")
    @Mapping(source = "clientCampaign.name", target = "clientCampaignName")
    public abstract BookingQuestionDTO toDto(BookingQuestion bookingQuestion);

    // @Mapping(target = "fields", ignore = true)
    // @Mapping(source = "clientCampaignId", target = "clientCampaign")
    @Mapping(ignore = true, target = "clientCampaign") // this is required to prevent issue with zeroing
    public abstract void update(BookingQuestionDTO bookingQuestionDTO, @MappingTarget BookingQuestion bookingQuestion);

    @Mapping(source = "clientCampaignId", target = "clientCampaign")
    public BookingQuestion toEntity(BookingQuestionDTO bookingQuestionDTO) {
        // this required to fix issue with full entity update in save
        BookingQuestion bookingQuestion = Optional.ofNullable(bookingQuestionDTO.getId())
            .map(bookingQuestionRepository::findOneWithEagerRelationships)
            .orElseGet(BookingQuestion::new);
        update(bookingQuestionDTO, bookingQuestion);

        // overwrite campaign only if it's not null
        Long clientCampaignId = bookingQuestionDTO.getClientCampaignId();
        if (clientCampaignId != null) {
            bookingQuestion.setClientCampaign(clientCampaignRepository.getOne(clientCampaignId));
        }

        return bookingQuestion;
    }

    public BookingQuestion fromId(Long id) {
        if (id == null) {
            return null;
        }
        BookingQuestion bookingQuestion = new BookingQuestion();
        bookingQuestion.setId(id);
        return bookingQuestion;
    }
}
