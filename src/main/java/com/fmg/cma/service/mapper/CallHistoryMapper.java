package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.CallHistory;
import com.fmg.cma.service.dto.CallHistoryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Mapper for the entity CallHistory and its DTO CallHistoryDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, ClientListMapper.class, EmailTemplateMapper.class})
public abstract class CallHistoryMapper {

    @Autowired
    private CallHistoryTypeMapper callHistoryTypeMapper;

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.login", target = "userLogin")
    @Mapping(source = "type.id", target = "typeId")
    @Mapping(source = "type.name", target = "typeName")
    @Mapping(source = "type.ident", target = "typeIdent")
    @Mapping(source = "list.id", target = "listId")
    @Mapping(source = "list.name", target = "listName")
    @Mapping(source = "list.clientCampaign.name", target = "campaignName")
    @Mapping(source = "emailTemplate.id", target = "emailTemplateId")
    @Mapping(source = "emailTemplate.name", target = "emailTemplateName")
    public abstract CallHistoryDTO toDto(CallHistory callHistory);

    @Mapping(ignore = true, target = "type")
    public CallHistory toEntity(CallHistoryDTO callHistoryDTO) {
        if (callHistoryDTO == null) {
            return null;
        }

        CallHistory callHistory = new CallHistory();

        updateEntity(callHistory, callHistoryDTO);

        if (callHistoryDTO.getTypeId() != null) {
            callHistory.setType(callHistoryTypeMapper.fromId(callHistoryDTO.getTypeId()));
        } else if (callHistoryDTO.getTypeIdent() != null) {
            callHistory.setType(callHistoryTypeMapper.fromIdent(callHistoryDTO.getTypeIdent()));
        }

        return callHistory;
    }

    @Mapping(source = "userId", target = "user")
    @Mapping(source = "listId", target = "list")
    @Mapping(source = "emailTemplateId", target = "emailTemplate")
    @Mapping(ignore = true, target = "type")
    public abstract void updateEntity(@MappingTarget CallHistory callHistory, CallHistoryDTO callHistoryDTO);

    public CallHistory fromId(Long id) {
        if (id == null) {
            return null;
        }
        CallHistory callHistory = new CallHistory();
        callHistory.setId(id);
        return callHistory;
    }
}
