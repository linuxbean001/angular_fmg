package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.CallHistoryType;
import com.fmg.cma.repository.CallHistoryTypeRepository;
import com.fmg.cma.service.dto.CallHistoryTypeDTO;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Mapper for the entity CallHistoryType and its DTO CallHistoryTypeDTO.
 */
@Mapper(componentModel = "spring")
public abstract class CallHistoryTypeMapper implements EntityMapper<CallHistoryTypeDTO, CallHistoryType> {

    @Autowired
    private CallHistoryTypeRepository callHistoryTypeRepository;

    public CallHistoryType fromId(Long id) {
        if (id == null) {
            return null;
        }
        CallHistoryType callHistoryType = new CallHistoryType();
        callHistoryType.setId(id);
        return callHistoryType;
    }

    public CallHistoryType fromIdent(String ident) {
        if (ident == null) {
            return null;
        }
        return callHistoryTypeRepository.getByIdent(ident);
    }
}
