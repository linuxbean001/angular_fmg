package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.CampaignListContactManagement;
import com.fmg.cma.service.dto.CampaignListContactManagementDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity CampaignListContactManagement and its DTO CampaignListContactManagementDTO.
 */
@Mapper(componentModel = "spring", uses = {ClientCampaignMapper.class, ClientListMapper.class})
public interface CampaignListContactManagementMapper extends EntityMapper<CampaignListContactManagementDTO, CampaignListContactManagement> {

    @Mapping(source = "clientCampaign.id", target = "clientCampaignId")
    @Mapping(source = "clientCampaign.name", target = "clientCampaignName")
    @Mapping(source = "clientList.id", target = "clientListId")
    @Mapping(source = "clientList.name", target = "clientListName")
    CampaignListContactManagementDTO toDto(CampaignListContactManagement campaignListContactManagement);

    @Mapping(source = "clientCampaignId", target = "clientCampaign")
    @Mapping(source = "clientListId", target = "clientList")
    CampaignListContactManagement toEntity(CampaignListContactManagementDTO campaignListContactManagementDTO);

    default CampaignListContactManagement fromId(Long id) {
        if (id == null) {
            return null;
        }
        CampaignListContactManagement campaignListContactManagement = new CampaignListContactManagement();
        campaignListContactManagement.setId(id);
        return campaignListContactManagement;
    }
}
