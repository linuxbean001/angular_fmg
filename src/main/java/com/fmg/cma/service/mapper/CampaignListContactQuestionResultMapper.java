package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.CampaignListContactQuestionResult;
import com.fmg.cma.service.dto.CampaignListContactQuestionResultDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity CampaignListContactQuestionResult and its DTO CampaignListContactQuestionResultDTO.
 */
@Mapper(componentModel = "spring", uses = {ClientCampaignMapper.class, ClientListMapper.class, BookingQuestionFieldMapper.class})
public interface CampaignListContactQuestionResultMapper extends EntityMapper<CampaignListContactQuestionResultDTO, CampaignListContactQuestionResult> {

    @Mapping(source = "clientCampaign.id", target = "clientCampaignId")
    @Mapping(source = "clientCampaign.name", target = "clientCampaignName")
    @Mapping(source = "clientList.id", target = "clientListId")
    @Mapping(source = "clientList.name", target = "clientListName")
    @Mapping(source = "bookingQuestionField.id", target = "bookingQuestionFieldId")
    @Mapping(source = "bookingQuestionField.name", target = "bookingQuestionFieldName")
    CampaignListContactQuestionResultDTO toDto(CampaignListContactQuestionResult campaignListContactQuestionResult);

    @Mapping(source = "clientCampaignId", target = "clientCampaign")
    @Mapping(source = "clientListId", target = "clientList")
    @Mapping(source = "bookingQuestionFieldId", target = "bookingQuestionField")
    CampaignListContactQuestionResult toEntity(CampaignListContactQuestionResultDTO campaignListContactQuestionResultDTO);

    default CampaignListContactQuestionResult fromId(Long id) {
        if (id == null) {
            return null;
        }
        CampaignListContactQuestionResult campaignListContactQuestionResult = new CampaignListContactQuestionResult();
        campaignListContactQuestionResult.setId(id);
        return campaignListContactQuestionResult;
    }
}
