package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.CampaignListContactResult;
import com.fmg.cma.service.dto.CampaignListContactResultDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

/**
 * Mapper for the entity CampaignListContactResult and its DTO CampaignListContactResultDTO.
 */
@Mapper(componentModel = "spring", uses = {
    ClientCampaignMapper.class,
    ClientListMapper.class,
    OutcomeSubReasonMapper.class,
    UserMapper.class,
    OutcomeMapper.class
})
public interface CampaignListContactResultMapper extends EntityMapper<CampaignListContactResultDTO, CampaignListContactResult> {

    @Mapping(source = "clientCampaign.id", target = "clientCampaignId")
    @Mapping(source = "clientCampaign.name", target = "clientCampaignName")
    @Mapping(source = "clientList.id", target = "clientListId")
    @Mapping(source = "clientList.name", target = "clientListName")
    @Mapping(source = "outcomeSubReason.id", target = "outcomeSubReasonId")
    @Mapping(source = "outcomeSubReason.name", target = "outcomeSubReasonName")
    @Mapping(source = "assignUser.id", target = "assignUserId")
    @Mapping(source = "assignUser.login", target = "assignUserLogin")
    @Mapping(source = "clickedOutcome.id", target = "clickedOutcomeId")
    @Mapping(source = "clickedOutcome.name", target = "clickedOutcomeName")
    @Mapping(source = "userClickedFinish.id", target = "userClickedFinishId")
    @Mapping(source = "userClickedFinish.login", target = "userClickedFinishLogin")
    CampaignListContactResultDTO toDto(CampaignListContactResult campaignListContactResult);

    @Mapping(source = "clientCampaignId", target = "clientCampaign")
    @Mapping(source = "clientListId", target = "clientList")
    @Mapping(source = "outcomeSubReasonId", target = "outcomeSubReason")
    @Mapping(source = "assignUserId", target = "assignUser")
    @Mapping(source = "clickedOutcomeId", target = "clickedOutcome")
    @Mapping(source = "userClickedFinishId", target = "userClickedFinish")
    CampaignListContactResult toEntity(CampaignListContactResultDTO campaignListContactResultDTO);

    @Mapping(ignore = true, target = "id")
    @Mapping(source = "clientCampaignId", target = "clientCampaign")
    @Mapping(source = "clientListId", target = "clientList")
    @Mapping(source = "outcomeSubReasonId", target = "outcomeSubReason")
    @Mapping(source = "assignUserId", target = "assignUser")
    @Mapping(source = "clickedOutcomeId", target = "clickedOutcome")
    @Mapping(source = "userClickedFinishId", target = "userClickedFinish")
    void updateEntity(CampaignListContactResultDTO campaignListContactResultDTO, @MappingTarget CampaignListContactResult result);

    default CampaignListContactResult fromId(Long id) {
        if (id == null) {
            return null;
        }
        CampaignListContactResult campaignListContactResult = new CampaignListContactResult();
        campaignListContactResult.setId(id);
        return campaignListContactResult;
    }
}
