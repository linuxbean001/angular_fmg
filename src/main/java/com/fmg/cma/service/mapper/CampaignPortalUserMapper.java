package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.CampaignPortalUser;
import com.fmg.cma.service.dto.CampaignPortalUserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity CampaignPortalUser and its DTO CampaignPortalUserDTO.
 */
@Mapper(componentModel = "spring", uses = {ClientCampaignMapper.class, UserMapper.class})
public interface CampaignPortalUserMapper extends EntityMapper<CampaignPortalUserDTO, CampaignPortalUser> {

    @Mapping(source = "clientCampaign.id", target = "clientCampaignId")
    @Mapping(source = "clientCampaign.name", target = "clientCampaignName")
    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.login", target = "userLogin")
    CampaignPortalUserDTO toDto(CampaignPortalUser campaignPortalUser);

    @Mapping(source = "clientCampaignId", target = "clientCampaign")
    @Mapping(source = "userId", target = "user")
    CampaignPortalUser toEntity(CampaignPortalUserDTO campaignPortalUserDTO);

    default CampaignPortalUser fromId(Long id) {
        if (id == null) {
            return null;
        }
        CampaignPortalUser campaignPortalUser = new CampaignPortalUser();
        campaignPortalUser.setId(id);
        return campaignPortalUser;
    }
}
