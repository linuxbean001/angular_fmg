package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.*;
import com.fmg.cma.service.dto.CampaignPortalUserOutcomeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity CampaignPortalUserOutcome and its DTO CampaignPortalUserOutcomeDTO.
 */
@Mapper(componentModel = "spring", uses = {CampaignPortalUserMapper.class, OutcomeMapper.class})
public interface CampaignPortalUserOutcomeMapper extends EntityMapper<CampaignPortalUserOutcomeDTO, CampaignPortalUserOutcome> {

    @Mapping(source = "campaignPortalUser.id", target = "campaignPortalUserId")
    @Mapping(source = "campaignPortalUser.email", target = "campaignPortalUserEmail")
    @Mapping(source = "outcome.id", target = "outcomeId")
    @Mapping(source = "outcome.name", target = "outcomeName")
    CampaignPortalUserOutcomeDTO toDto(CampaignPortalUserOutcome campaignPortalUserOutcome);

    @Mapping(source = "campaignPortalUserId", target = "campaignPortalUser")
    @Mapping(source = "outcomeId", target = "outcome")
    CampaignPortalUserOutcome toEntity(CampaignPortalUserOutcomeDTO campaignPortalUserOutcomeDTO);

    default CampaignPortalUserOutcome fromId(Long id) {
        if (id == null) {
            return null;
        }
        CampaignPortalUserOutcome campaignPortalUserOutcome = new CampaignPortalUserOutcome();
        campaignPortalUserOutcome.setId(id);
        return campaignPortalUserOutcome;
    }
}
