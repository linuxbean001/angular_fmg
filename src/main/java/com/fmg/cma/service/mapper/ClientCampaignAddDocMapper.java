package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.ClientCampaignAddDoc;
import com.fmg.cma.repository.ClientCampaignAddDocRepository;
import com.fmg.cma.repository.ClientCampaignRepository;
import com.fmg.cma.service.dto.ClientCampaignAddDocDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

/**
 * Mapper for the entity ClientCampaignAddDoc and its DTO ClientCampaignAddDocDTO.
 */
@Mapper(componentModel = "spring", uses = {ClientCampaignMapper.class})
public abstract class ClientCampaignAddDocMapper implements EntityMapper<ClientCampaignAddDocDTO, ClientCampaignAddDoc> {

    @Autowired
    private ClientCampaignAddDocRepository clientCampaignAddDocRepository;

    @Autowired
    private ClientCampaignRepository clientCampaignRepository;

    @Mapping(source = "clientCampaign.id", target = "clientCampaignId")
    @Mapping(source = "clientCampaign.name", target = "clientCampaignName")
    public abstract ClientCampaignAddDocDTO toDto(ClientCampaignAddDoc clientCampaignAddDoc);

    @Mapping(ignore = true, target = "clientCampaign") // this is required to prevent issue with zeroing
    public abstract void update(ClientCampaignAddDocDTO clientCampaignAddDocDTO, @MappingTarget ClientCampaignAddDoc clientCampaignAddDoc);

    @Mapping(source = "clientCampaignId", target = "clientCampaign")
    public ClientCampaignAddDoc toEntity(ClientCampaignAddDocDTO clientCampaignAddDocDTO) {
        // this required to fix issue with full entity update in save
        ClientCampaignAddDoc clientCampaignAddDoc = Optional.ofNullable(clientCampaignAddDocDTO.getId())
            .map(clientCampaignAddDocRepository::findOne)
            .orElseGet(ClientCampaignAddDoc::new);
        update(clientCampaignAddDocDTO, clientCampaignAddDoc);

        // overwrite campaign only if it's not null
        Long clientCampaignId = clientCampaignAddDocDTO.getClientCampaignId();
        if (clientCampaignId != null) {
            clientCampaignAddDoc.setClientCampaign(clientCampaignRepository.getOne(clientCampaignId));
        }

        return clientCampaignAddDoc;
    }

    public ClientCampaignAddDoc fromId(Long id) {
        if (id == null) {
            return null;
        }
        ClientCampaignAddDoc clientCampaignAddDoc = new ClientCampaignAddDoc();
        clientCampaignAddDoc.setId(id);
        return clientCampaignAddDoc;
    }
}
