package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.ClientCampaign;
import com.fmg.cma.repository.ClientCampaignRepository;
import com.fmg.cma.service.dto.ClientCampaignDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Mapper for the entity ClientCampaign and its DTO ClientCampaignDTO.
 */
@Mapper(componentModel = "spring", uses = {
    UserMapper.class,
    ClientCampaignTemplateMapper.class,
    ClientCompanyMapper.class,
    ClientListMapper.class,
    EmailTemplateMapper.class,
    OutcomeMapper.class,
    ClientCampaignScriptMapper.class,
    AppointmentAttendeeMapper.class,
    ClientCampaignAddDocMapper.class
})
public abstract class ClientCampaignMapper implements EntityMapper<ClientCampaignDTO, ClientCampaign> {

    @Autowired
    private ClientCampaignRepository clientCampaignRepository;

    @Mapping(source = "createdBy.id", target = "createdById")
    @Mapping(source = "createdBy.login", target = "createdByLogin")
    @Mapping(source = "clientCompany.id", target = "clientCompanyId")
    @Mapping(source = "clientCompany.name", target = "clientCompanyName")
    @Mapping(source = "clientCompany.industry.id", target = "clientCompanyIndustryId")
    public abstract ClientCampaignDTO toDto(ClientCampaign clientCampaign);

    @Mapping(source = "clientCompanyId", target = "clientCompany")
    @Mapping(ignore = true, target = "createdDate")
    @Mapping(ignore = true, target = "createdBy")
    public abstract ClientCampaign toEntity(ClientCampaignDTO clientCampaignDTO);

    @Mapping(source = "clientCompanyId", target = "clientCompany")
    @Mapping(ignore = true, target = "createdDate")
    @Mapping(ignore = true, target = "createdBy")
    public abstract void update(ClientCampaignDTO from, @MappingTarget ClientCampaign to);

    public ClientCampaign fromId(Long id) {
        if (id == null) {
            return null;
        }
        // this is slow, but "name" is not missed
        return clientCampaignRepository.getOne(id);
        // ClientCampaign clientCampaign = new ClientCampaign();
        // clientCampaign.setId(id);
        // return clientCampaign;
    }
}
