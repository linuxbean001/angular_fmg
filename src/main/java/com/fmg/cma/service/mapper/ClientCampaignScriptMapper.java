package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.ClientCampaignScript;
import com.fmg.cma.repository.ClientCampaignRepository;
import com.fmg.cma.repository.ClientCampaignScriptRepository;
import com.fmg.cma.service.dto.ClientCampaignScriptDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

/**
 * Mapper for the entity ClientCampaignScript and its DTO ClientCampaignScriptDTO.
 */
@Mapper(componentModel = "spring", uses = {ClientCampaignMapper.class})
public abstract class ClientCampaignScriptMapper implements EntityMapper<ClientCampaignScriptDTO, ClientCampaignScript> {

    @Autowired
    private ClientCampaignScriptRepository clientCampaignScriptRepository;

    @Autowired
    private ClientCampaignRepository clientCampaignRepository;

    @Mapping(source = "clientCampaign.id", target = "clientCampaignId")
    @Mapping(source = "clientCampaign.name", target = "clientCampaignName")
    public abstract ClientCampaignScriptDTO toDto(ClientCampaignScript clientCampaignScript);

    @Mapping(ignore = true, target = "clientCampaign") // this is required to prevent issue with zeroing
    public abstract void update(ClientCampaignScriptDTO clientCampaignScriptDTO, @MappingTarget ClientCampaignScript clientCampaignScript);

    @Mapping(source = "clientCampaignId", target = "clientCampaign")
    public ClientCampaignScript toEntity(ClientCampaignScriptDTO clientCampaignScriptDTO) {
        // this required to fix issue with full entity update in save
        ClientCampaignScript clientCampaignScript = Optional.ofNullable(clientCampaignScriptDTO.getId())
            .map(clientCampaignScriptRepository::findOne)
            .orElseGet(ClientCampaignScript::new);
        update(clientCampaignScriptDTO, clientCampaignScript);

        // overwrite campaign only if it's not null
        Long clientCampaignId = clientCampaignScriptDTO.getClientCampaignId();
        if (clientCampaignId != null) {
            clientCampaignScript.setClientCampaign(clientCampaignRepository.getOne(clientCampaignId));
        }

        return clientCampaignScript;
    }

    public ClientCampaignScript fromId(Long id) {
        if (id == null) {
            return null;
        }
        ClientCampaignScript clientCampaignScript = new ClientCampaignScript();
        clientCampaignScript.setId(id);
        return clientCampaignScript;
    }
}
