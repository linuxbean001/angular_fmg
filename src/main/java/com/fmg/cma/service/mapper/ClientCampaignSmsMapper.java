package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.ClientCampaignSms;
import com.fmg.cma.repository.ClientCampaignRepository;
import com.fmg.cma.repository.ClientCampaignSmsRepository;
import com.fmg.cma.service.dto.ClientCampaignSmsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

/**
 * Mapper for the entity ClientCampaignSms and its DTO ClientCampaignSmsDTO.
 */
@Mapper(componentModel = "spring", uses = {ClientCampaignMapper.class})
public abstract class ClientCampaignSmsMapper implements EntityMapper<ClientCampaignSmsDTO, ClientCampaignSms> {

    @Autowired
    private ClientCampaignSmsRepository clientCampaignSmsRepository;

    @Autowired
    private ClientCampaignRepository clientCampaignRepository;

    @Mapping(source = "clientCampaign.id", target = "clientCampaignId")
    @Mapping(source = "clientCampaign.name", target = "clientCampaignName")
    public abstract ClientCampaignSmsDTO toDto(ClientCampaignSms clientCampaignSms);

    @Mapping(ignore = true, target = "clientCampaign") // this is required to prevent issue with zeroing
    public abstract void update(ClientCampaignSmsDTO clientCampaignSmsDTO, @MappingTarget ClientCampaignSms clientCampaignSms);

    @Mapping(source = "clientCampaignId", target = "clientCampaign")
    public ClientCampaignSms toEntity(ClientCampaignSmsDTO clientCampaignSmsDTO) {
        // this required to fix issue with full entity update in save
        ClientCampaignSms clientCampaignSms = Optional.ofNullable(clientCampaignSmsDTO.getId())
            .map(clientCampaignSmsRepository::findOne)
            .orElseGet(ClientCampaignSms::new);
        update(clientCampaignSmsDTO, clientCampaignSms);

        // overwrite campaign only if it's not null
        Long clientCampaignId = clientCampaignSmsDTO.getClientCampaignId();
        if (clientCampaignId != null) {
            clientCampaignSms.setClientCampaign(clientCampaignRepository.getOne(clientCampaignId));
        }

        return clientCampaignSms;
    }


    public ClientCampaignSms fromId(Long id) {
        if (id == null) {
            return null;
        }
        ClientCampaignSms clientCampaignSms = new ClientCampaignSms();
        clientCampaignSms.setId(id);
        return clientCampaignSms;
    }
}
