package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.ClientCampaignTemplate;
import com.fmg.cma.repository.ClientCampaignTemplateRepository;
import com.fmg.cma.service.dto.ClientCampaignTemplateDTO;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Mapper for the entity ClientCampaignTemplate and its DTO ClientCampaignTemplateDTO.
 */
@Mapper(componentModel = "spring")
public abstract class ClientCampaignTemplateMapper implements EntityMapper<ClientCampaignTemplateDTO, ClientCampaignTemplate> {

    @Autowired
    private ClientCampaignTemplateRepository clientCampaignTemplateRepository;

    public ClientCampaignTemplate fromId(Long id) {
        if (id == null) {
            return null;
        }
        // this is slow, but "name" is not missed
        return clientCampaignTemplateRepository.getOne(id);
        // ClientCampaignTemplate clientCampaignTemplate = new ClientCampaignTemplate();
        // clientCampaignTemplate.setId(id);
        // return clientCampaignTemplate;
    }
}
