package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.ClientCompany;
import com.fmg.cma.repository.ClientCompanyRepository;
import com.fmg.cma.service.dto.ClientCompanyDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Mapper for the entity ClientCompany and its DTO ClientCompanyDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, ClientCompanyStatusMapper.class, IndustryMapper.class})
public abstract class ClientCompanyMapper implements EntityMapper<ClientCompanyDTO, ClientCompany> {

    @Autowired
    private ClientCompanyRepository clientCompanyRepository;

    @Mapping(source = "createdBy.id", target = "createdById")
    @Mapping(source = "createdBy.login", target = "createdByLogin")
    @Mapping(source = "updatedBy.id", target = "updatedById")
    @Mapping(source = "updatedBy.login", target = "updatedByLogin")
    @Mapping(source = "status.id", target = "statusId")
    @Mapping(source = "status.name", target = "statusName")
    @Mapping(source = "industry.id", target = "industryId")
    @Mapping(source = "industry.name", target = "industryName")
    public abstract ClientCompanyDTO toDto(ClientCompany clientCompany);

    @Mapping(source = "createdById", target = "createdBy")
    @Mapping(source = "updatedById", target = "updatedBy")
    @Mapping(source = "statusId", target = "status")
    @Mapping(source = "industryId", target = "industry")
    public abstract ClientCompany toEntity(ClientCompanyDTO clientCompanyDTO);

    public ClientCompany fromId(Long id) {
        if (id == null) {
            return null;
        }
        // this is slow, but "name" is not missed
        return clientCompanyRepository.getOne(id);
        //clientCampaignClientCompany clientCompany = new ClientCompany();
        //clientCompany.setId(id);
        //return clientCompany;
    }
}
