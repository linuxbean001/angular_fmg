package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.ClientCompanyStatus;
import com.fmg.cma.service.dto.ClientCompanyStatusDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity ClientCompanyStatus and its DTO ClientCompanyStatusDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ClientCompanyStatusMapper extends EntityMapper<ClientCompanyStatusDTO, ClientCompanyStatus> {


    default ClientCompanyStatus fromId(Long id) {
        if (id == null) {
            return null;
        }
        ClientCompanyStatus clientCompanyStatus = new ClientCompanyStatus();
        clientCompanyStatus.setId(id);
        return clientCompanyStatus;
    }
}
