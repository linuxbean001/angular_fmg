package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.*;
import com.fmg.cma.service.dto.ClientFieldDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ClientField and its DTO ClientFieldDTO.
 */
@Mapper(componentModel = "spring", uses = {FieldMapper.class, ClientListMapper.class})
public interface ClientFieldMapper extends EntityMapper<ClientFieldDTO, ClientField> {

    @Mapping(source = "field.id", target = "fieldId")
    @Mapping(source = "field.name", target = "fieldName")
    @Mapping(source = "field.order", target = "fieldOrder")
    @Mapping(source = "list.id", target = "listId")
    @Mapping(source = "list.name", target = "listName")
    ClientFieldDTO toDto(ClientField clientField);

    @Mapping(source = "fieldId", target = "field")
    @Mapping(source = "listId", target = "list")
    ClientField toEntity(ClientFieldDTO clientFieldDTO);

    default ClientField fromId(Long id) {
        if (id == null) {
            return null;
        }
        ClientField clientField = new ClientField();
        clientField.setId(id);
        return clientField;
    }
}
