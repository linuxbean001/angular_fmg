package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.*;
import com.fmg.cma.service.dto.ClientListDataDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ClientListData and its DTO ClientListDataDTO.
 */
@Mapper(componentModel = "spring", uses = {ClientFieldMapper.class})
public interface ClientListDataMapper extends EntityMapper<ClientListDataDTO, ClientListData> {

    @Mapping(source = "clientField.id", target = "clientFieldId")
    ClientListDataDTO toDto(ClientListData clientListData);

    @Mapping(source = "clientFieldId", target = "clientField")
    ClientListData toEntity(ClientListDataDTO clientListDataDTO);

    default ClientListData fromId(Long id) {
        if (id == null) {
            return null;
        }
        ClientListData clientListData = new ClientListData();
        clientListData.setId(id);
        return clientListData;
    }
}
