package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.ClientList;
import com.fmg.cma.repository.ClientListRepository;
import com.fmg.cma.service.dto.ClientListDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Mapper for the entity ClientList and its DTO ClientListDTO.
 */
@Mapper(componentModel = "spring", uses = {ClientCampaignMapper.class, CampaignPortalUserMapper.class})
public abstract class ClientListMapper implements EntityMapper<ClientListDTO, ClientList> {

    @Autowired
    private ClientListRepository clientListRepository;

    @Mapping(source = "clientCampaign.id", target = "clientCampaignId")
    @Mapping(source = "clientCampaign.name", target = "clientCampaignName")
    public abstract ClientListDTO toDto(ClientList clientList);

    @Mapping(source = "clientCampaignId", target = "clientCampaign")
    public abstract ClientList toEntity(ClientListDTO clientListDTO);

    public ClientList fromId(Long id) {
        if (id == null) {
            return null;
        }
        // this is slow, but "name" is not missed
        return clientListRepository.getOne(id);
        // ClientList clientList = new ClientList();
        // clientList.setId(id);
        // return clientList;
    }
}
