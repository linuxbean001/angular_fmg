package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.ConsoleStats;
import com.fmg.cma.service.dto.ConsoleStatsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity ConsoleStats and its DTO ConsoleStatsDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface ConsoleStatsMapper extends EntityMapper<ConsoleStatsDTO, ConsoleStats> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.login", target = "userLogin")
    ConsoleStatsDTO toDto(ConsoleStats consoleStats);

    @Mapping(source = "userId", target = "user")
    ConsoleStats toEntity(ConsoleStatsDTO consoleStatsDTO);

    default ConsoleStats fromId(Long id) {
        if (id == null) {
            return null;
        }
        ConsoleStats consoleStats = new ConsoleStats();
        consoleStats.setId(id);
        return consoleStats;
    }
}
