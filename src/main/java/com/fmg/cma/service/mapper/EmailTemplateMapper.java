package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.EmailTemplate;
import com.fmg.cma.repository.ClientCampaignRepository;
import com.fmg.cma.repository.EmailTemplateRepository;
import com.fmg.cma.service.dto.EmailTemplateDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

/**
 * Mapper for the entity EmailTemplate and its DTO EmailTemplateDTO.
 */
@Mapper(componentModel = "spring", uses = {ClientCampaignMapper.class})
public abstract class EmailTemplateMapper implements EntityMapper<EmailTemplateDTO, EmailTemplate> {

    @Autowired
    private EmailTemplateRepository emailTemplateRepository;

    @Autowired
    private ClientCampaignRepository clientCampaignRepository;

    @Mapping(source = "clientCampaign.id", target = "clientCampaignId")
    @Mapping(source = "clientCampaign.name", target = "clientCampaignName")
    public abstract EmailTemplateDTO toDto(EmailTemplate emailTemplate);

    @Mapping(ignore = true, target = "clientCampaign") // this is required to prevent issue with zeroing
    public abstract void update(EmailTemplateDTO emailTemplateDTO, @MappingTarget EmailTemplate emailTemplate);

    @Mapping(source = "clientCampaignId", target = "clientCampaign")
    public EmailTemplate toEntity(EmailTemplateDTO emailTemplateDTO) {
        // this required to fix issue with full entity update in save
        EmailTemplate emailTemplate = Optional.ofNullable(emailTemplateDTO.getId())
            .map(emailTemplateRepository::findOne)
            .orElseGet(EmailTemplate::new);
        update(emailTemplateDTO, emailTemplate);

        // overwrite campaign only if it's not null
        Long clientCampaignId = emailTemplateDTO.getClientCampaignId();
        if (clientCampaignId != null) {
            emailTemplate.setClientCampaign(clientCampaignRepository.getOne(clientCampaignId));
        }

        return emailTemplate;
    }

    public EmailTemplate fromId(Long id) {
        if (id == null) {
            return null;
        }
        // this is slow, but "name" is not missed
        return emailTemplateRepository.getOne(id);
        // EmailTemplate emailTemplate = new EmailTemplate();
        // emailTemplate.setId(id);
        // return emailTemplate;
    }
}
