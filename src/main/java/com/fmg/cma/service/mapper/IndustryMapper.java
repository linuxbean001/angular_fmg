package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.Industry;
import com.fmg.cma.service.dto.IndustryDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity Industry and its DTO IndustryDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface IndustryMapper extends EntityMapper<IndustryDTO, Industry> {


    default Industry fromId(Long id) {
        if (id == null) {
            return null;
        }
        Industry industry = new Industry();
        industry.setId(id);
        return industry;
    }
}
