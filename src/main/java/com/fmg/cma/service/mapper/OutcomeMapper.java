package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.Outcome;
import com.fmg.cma.repository.ClientCampaignRepository;
import com.fmg.cma.repository.OutcomeRepository;
import com.fmg.cma.service.dto.OutcomeDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

/**
 * Mapper for the entity Outcome and its DTO OutcomeDTO.
 */
@Mapper(componentModel = "spring", uses = {
    ClientCampaignTemplateMapper.class,
    OutcomeTypeMapper.class,
    ClientCampaignMapper.class,
    EmailTemplateMapper.class,
    ClientListMapper.class,
    OutcomeSubReasonMapper.class
})
public abstract class OutcomeMapper implements EntityMapper<OutcomeDTO, Outcome> {

    @Autowired
    private OutcomeRepository outcomeRepository;

    @Autowired
    private ClientCampaignRepository clientCampaignRepository;

    @Mapping(source = "template.id", target = "templateId")
    @Mapping(source = "template.name", target = "templateName")
    @Mapping(source = "type.id", target = "typeId")
    @Mapping(source = "type.name", target = "typeName")
    @Mapping(source = "moveToCampaign.id", target = "moveToCampaignId")
    @Mapping(source = "moveToCampaign.name", target = "moveToCampaignName")
    @Mapping(source = "clientCampaign.id", target = "clientCampaignId")
    @Mapping(source = "clientCampaign.name", target = "clientCampaignName")
    @Mapping(source = "clientList.id", target = "clientListId")
    @Mapping(source = "clientList.name", target = "clientListName")
    @Mapping(source = "emailTemplate.id", target = "emailTemplateId")
    @Mapping(source = "emailTemplate.name", target = "emailTemplateName")
    @Mapping(source = "outcomeSubReason.id", target = "outcomeSubReasonId")
    @Mapping(source = "outcomeSubReason.name", target = "outcomeSubReasonName")
    public abstract OutcomeDTO toDto(Outcome outcome);

    @Mapping(source = "templateId", target = "template")
    @Mapping(source = "typeId", target = "type")
    @Mapping(source = "moveToCampaignId", target = "moveToCampaign")
    @Mapping(source = "clientListId", target = "clientList")
    @Mapping(source = "emailTemplateId", target = "emailTemplate")
    @Mapping(source = "outcomeSubReasonId", target = "outcomeSubReason")
    @Mapping(ignore = true, target = "clientCampaign") // this is required to prevent issue with zeroing
    public abstract void update(OutcomeDTO outcomeDTO, @MappingTarget Outcome outcome);

    @Mapping(source = "clientCampaignId", target = "clientCampaign")
    public Outcome toEntity(OutcomeDTO outcomeDTO) {
        // this required to fix issue with full entity update in save
        Outcome clientCampaignSms = Optional.ofNullable(outcomeDTO.getId())
            .map(outcomeRepository::findOne)
            .orElseGet(Outcome::new);
        update(outcomeDTO, clientCampaignSms);

        // overwrite campaign only if it's not null
        Long clientCampaignId = outcomeDTO.getClientCampaignId();
        if (clientCampaignId != null) {
            clientCampaignSms.setClientCampaign(clientCampaignRepository.getOne(clientCampaignId));
        }

        return clientCampaignSms;
    }

    public Outcome fromId(Long id) {
        if (id == null) {
            return null;
        }
        // this is slow, but "name" is not missed
        return outcomeRepository.getOne(id);
        // Outcome outcome = new Outcome();
        // outcome.setId(id);
        // return outcome;
    }
}
