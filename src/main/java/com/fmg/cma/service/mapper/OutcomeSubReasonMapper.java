package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.OutcomeSubReason;
import com.fmg.cma.service.dto.OutcomeSubReasonDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity OutcomeSubReason and its DTO OutcomeSubReasonDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface OutcomeSubReasonMapper extends EntityMapper<OutcomeSubReasonDTO, OutcomeSubReason> {


    default OutcomeSubReason fromId(Long id) {
        if (id == null) {
            return null;
        }
        OutcomeSubReason outcomeSubReason = new OutcomeSubReason();
        outcomeSubReason.setId(id);
        return outcomeSubReason;
    }
}
