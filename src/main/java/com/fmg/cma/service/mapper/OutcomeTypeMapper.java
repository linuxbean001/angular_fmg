package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.OutcomeType;
import com.fmg.cma.repository.OutcomeTypeRepository;
import com.fmg.cma.service.dto.OutcomeTypeDTO;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Mapper for the entity OutcomeType and its DTO OutcomeTypeDTO.
 */
@Mapper(componentModel = "spring")
public abstract class OutcomeTypeMapper implements EntityMapper<OutcomeTypeDTO, OutcomeType> {

    @Autowired
    private OutcomeTypeRepository outcomeTypeRepository;

    public OutcomeType fromId(Long id) {
        if (id == null) {
            return null;
        }
        // this is slow, but "name" is not missed
        return outcomeTypeRepository.getOne(id);
        // OutcomeType outcomeType = new OutcomeType();
        // outcomeType.setId(id);
        // return outcomeType;
    }
}
