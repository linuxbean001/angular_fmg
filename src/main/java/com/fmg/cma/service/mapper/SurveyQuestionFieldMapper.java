package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.SurveyQuestionField;
import com.fmg.cma.service.dto.SurveyQuestionFieldDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Set;

/**
 * Mapper for the entity SurveyQuestionField and its DTO SurveyQuestionFieldDTO.
 */
@Mapper(componentModel = "spring", uses = {SurveyQuestionMapper.class})
public interface SurveyQuestionFieldMapper extends EntityMapper<SurveyQuestionFieldDTO, SurveyQuestionField> {

    @Mapping(source = "surveyQuestion.id", target = "surveyQuestionId")
    @Mapping(source = "surveyQuestion.name", target = "surveyQuestionName")
    SurveyQuestionFieldDTO toDto(SurveyQuestionField surveyQuestionField);

    @Mapping(source = "surveyQuestionId", target = "surveyQuestion")
    SurveyQuestionField toEntity(SurveyQuestionFieldDTO surveyQuestionFieldDTO);

    Set<SurveyQuestionField> toEntitySet(Set<SurveyQuestionFieldDTO> dtoList);

    default SurveyQuestionField fromId(Long id) {
        if (id == null) {
            return null;
        }
        SurveyQuestionField surveyQuestionField = new SurveyQuestionField();
        surveyQuestionField.setId(id);
        return surveyQuestionField;
    }
}
