package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.SurveyQuestion;
import com.fmg.cma.repository.ClientCampaignRepository;
import com.fmg.cma.repository.SurveyQuestionRepository;
import com.fmg.cma.service.dto.SurveyQuestionDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

/**
 * Mapper for the entity SurveyQuestion and its DTO SurveyQuestionDTO.
 */
@Mapper(componentModel = "spring", uses = {ClientCampaignMapper.class, SurveyQuestionFieldMapper.class})
public abstract class SurveyQuestionMapper implements EntityMapper<SurveyQuestionDTO, SurveyQuestion> {

    @Autowired
    private SurveyQuestionRepository surveyQuestionRepository;

    @Autowired
    private ClientCampaignRepository clientCampaignRepository;

    @Mapping(source = "clientCampaign.id", target = "clientCampaignId")
    @Mapping(source = "clientCampaign.name", target = "clientCampaignName")
    public abstract SurveyQuestionDTO toDto(SurveyQuestion surveyQuestion);

    // @Mapping(target = "fields", ignore = true)
    // @Mapping(source = "clientCampaignId", target = "clientCampaign")
    @Mapping(ignore = true, target = "clientCampaign") // this is required to prevent issue with zeroing
    public abstract void update(SurveyQuestionDTO surveyQuestionDTO, @MappingTarget SurveyQuestion surveyQuestion);

    @Mapping(source = "clientCampaignId", target = "clientCampaign")
    public SurveyQuestion toEntity(SurveyQuestionDTO surveyQuestionDTO) {
        // this required to fix issue with full entity update in save
        SurveyQuestion surveyQuestion = Optional.ofNullable(surveyQuestionDTO.getId())
            .map(surveyQuestionRepository::findOneWithEagerRelationships)
            .orElseGet(SurveyQuestion::new);
        update(surveyQuestionDTO, surveyQuestion);

        // overwrite campaign only if it's not null
        Long clientCampaignId = surveyQuestionDTO.getClientCampaignId();
        if (clientCampaignId != null) {
            surveyQuestion.setClientCampaign(clientCampaignRepository.getOne(clientCampaignId));
        }

        return surveyQuestion;
    }

    public SurveyQuestion fromId(Long id) {
        if (id == null) {
            return null;
        }
        SurveyQuestion surveyQuestion = new SurveyQuestion();
        surveyQuestion.setId(id);
        return surveyQuestion;
    }
}
