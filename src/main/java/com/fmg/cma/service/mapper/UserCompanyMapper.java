package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.UserCompany;
import com.fmg.cma.service.dto.UserCompanyDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity UserCompany and its DTO UserCompanyDTO.
 */
@Mapper(componentModel = "spring")
public interface UserCompanyMapper extends EntityMapper<UserCompanyDTO, UserCompany> {

    default UserCompany fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserCompany userCompany = new UserCompany();
        userCompany.setId(id);
        return userCompany;
    }
}
