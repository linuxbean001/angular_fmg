package com.fmg.cma.service.util;

import com.fmg.cma.service.SessionFactoryService;
import org.hibernate.dialect.Dialect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DatabaseUtils {
    private static final Logger logger = LoggerFactory.getLogger(DatabaseUtils.class);

    private static SessionFactoryService sessionFactoryService;

    @Autowired
    public DatabaseUtils(SessionFactoryService sessionFactoryService) {
        DatabaseUtils.sessionFactoryService = sessionFactoryService;
    }

    public static Dialect getCurrentDialect() {
        return sessionFactoryService.getSessionFactoryImplementor().getDialect();
    }
}
