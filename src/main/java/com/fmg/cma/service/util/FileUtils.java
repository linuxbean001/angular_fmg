package com.fmg.cma.service.util;

public class FileUtils {

    /**
     * Workaround to remove BOM.
     */
    public static void removeBom(String[] strings) {
        if (strings.length > 0) {
            strings[0] = strings[0].replace("\uFEFF", "");
        }
    }
}
