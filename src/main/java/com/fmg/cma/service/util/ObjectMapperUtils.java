package com.fmg.cma.service.util;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public final class ObjectMapperUtils {

    public static <T> T readValue(ObjectMapper objectMapper, String content, Class<T> valueType) {
        try {
            return objectMapper.readValue(content, valueType);
        } catch (IOException e) {
            throw new RuntimeException("Could not read", e);
        }
    }
}
