package com.fmg.cma.service.util;

import org.hibernate.dialect.Dialect;
import org.hibernate.dialect.pagination.LimitHandler;
import org.hibernate.engine.spi.RowSelection;

import java.util.HashMap;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

public final class SqlUtils {

    public static SqlWithParameters createPaginatedSql(String sql, int firstRow, int maxRows) {
        Dialect dialect = DatabaseUtils.getCurrentDialect();
        return createPaginatedSql(sql, firstRow, maxRows, dialect);
    }

    public static SqlWithParameters createPaginatedSql(String sql, int firstRow, int maxRows, Dialect dialect) {
        checkNotNull(dialect, "dialect must not be null");

        RowSelection rowSelection = new RowSelection();
        rowSelection.setMaxRows(maxRows);
        rowSelection.setFetchSize(maxRows);
        rowSelection.setFirstRow(firstRow);
        LimitHandler limitHandler = dialect.getLimitHandler();
        String sqlLimit = limitHandler.processSql(sql, rowSelection);

        SqlWithParameters sqlWithParameters = new SqlWithParameters();

        //if (((AbstractLimitHandler) limitHandler).bindLimitParametersInReverseOrder()) {
        //  //limit,offset
        //} else {
        //  //offset,limit
        //}
        //TODO: make it cross database
        //mariadb
        if (sqlLimit.contains("limit ?, ?")) {
            sqlWithParameters.setParameter("limit1", firstRow);
            sqlWithParameters.setParameter("limit2", maxRows);
            sqlLimit = sqlLimit.replace("limit ?, ?", "limit :limit1, :limit2");
        }
        //postgre
        if (sqlLimit.contains("limit ?")) {
            sqlWithParameters.setParameter("limit", maxRows);
            sqlLimit = sqlLimit.replace("limit ?", "limit :limit");
        }
        if (sqlLimit.contains("offset ?")) {
            sqlWithParameters.setParameter("offset", firstRow);
            sqlLimit = sqlLimit.replace("offset ?", "offset :offset");
        }
        //oracle
        if (sqlLimit.contains("rownum <= ?")) {
            sqlWithParameters.setParameter("limit", firstRow + maxRows);
            sqlLimit = sqlLimit.replace("rownum <= ?", "rownum <= :limit");
        }
        if (sqlLimit.contains("rownum_ > ?")) {
            sqlWithParameters.setParameter("offset", firstRow);
            sqlLimit = sqlLimit.replace("rownum_ > ?", "rownum_ > :offset");
        }
        //SQL server
        if (sqlLimit.contains("TOP(?)")) {
            sqlWithParameters.setParameter("fetchSize", firstRow + maxRows);
            sqlLimit = sqlLimit.replace("TOP(?)", "TOP(:fetchSize)");
        }
        if (sqlLimit.contains("__hibernate_row_nr__ < ?")) {
            sqlWithParameters.setParameter("limit", firstRow + maxRows + 1);
            sqlLimit = sqlLimit.replace("__hibernate_row_nr__ < ?", "__hibernate_row_nr__ < :limit");
        }
        if (sqlLimit.contains("__hibernate_row_nr__ >= ?")) {
            sqlWithParameters.setParameter("offset", firstRow + 1);
            sqlLimit = sqlLimit.replace("__hibernate_row_nr__ >= ?", "__hibernate_row_nr__ >= :offset");
        }

        return sqlWithParameters.setSql(sqlLimit);
    }

    public static class SqlWithParameters {
        private String sql;
        private Map<String, Object> params = new HashMap<>();

        public SqlWithParameters() {

        }

        public SqlWithParameters(String sql) {
            this.sql = sql;
        }

        public SqlWithParameters(String sql, Map<String, Object> params) {
            this.sql = sql;
            this.params = params;
        }

        public String getSql() {
            return sql;
        }

        public SqlWithParameters setSql(String sql) {
            this.sql = sql;
            return this;
        }

        public Map<String, Object> getParams() {
            return params;
        }

        public SqlWithParameters setParams(Map<String, Object> params) {
            this.params = params;
            return this;
        }

        public SqlWithParameters setParameter(String name, Object value) {
            params.put(name, value);
            return this;
        }
    }
}
