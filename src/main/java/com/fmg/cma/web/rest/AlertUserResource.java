package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.service.AlertUserService;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import com.fmg.cma.web.rest.util.PaginationUtil;
import com.fmg.cma.service.dto.AlertUserDTO;
import com.fmg.cma.service.dto.AlertUserCriteria;
import com.fmg.cma.service.AlertUserQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AlertUser.
 */
@RestController
@RequestMapping("/api")
public class AlertUserResource {

    private final Logger log = LoggerFactory.getLogger(AlertUserResource.class);

    private static final String ENTITY_NAME = "alertUser";

    private final AlertUserService alertUserService;

    private final AlertUserQueryService alertUserQueryService;

    public AlertUserResource(AlertUserService alertUserService, AlertUserQueryService alertUserQueryService) {
        this.alertUserService = alertUserService;
        this.alertUserQueryService = alertUserQueryService;
    }

    /**
     * POST  /alert-users : Create a new alertUser.
     *
     * @param alertUserDTO the alertUserDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new alertUserDTO, or with status 400 (Bad Request) if the alertUser has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/alert-users")
    @Timed
    public ResponseEntity<AlertUserDTO> createAlertUser(@RequestBody AlertUserDTO alertUserDTO) throws URISyntaxException {
        log.debug("REST request to save AlertUser : {}", alertUserDTO);
        if (alertUserDTO.getId() != null) {
            throw new BadRequestAlertException("A new alertUser cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AlertUserDTO result = alertUserService.save(alertUserDTO);
        return ResponseEntity.created(new URI("/api/alert-users/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /alert-users : Updates an existing alertUser.
     *
     * @param alertUserDTO the alertUserDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated alertUserDTO,
     * or with status 400 (Bad Request) if the alertUserDTO is not valid,
     * or with status 500 (Internal Server Error) if the alertUserDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/alert-users")
    @Timed
    public ResponseEntity<AlertUserDTO> updateAlertUser(@RequestBody AlertUserDTO alertUserDTO) throws URISyntaxException {
        log.debug("REST request to update AlertUser : {}", alertUserDTO);
        if (alertUserDTO.getId() == null) {
            return createAlertUser(alertUserDTO);
        }
        AlertUserDTO result = alertUserService.save(alertUserDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, alertUserDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /alert-users : get all the alertUsers.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of alertUsers in body
     */
    @GetMapping("/alert-users")
    @Timed
    public ResponseEntity<List<AlertUserDTO>> getAllAlertUsers(AlertUserCriteria criteria, Pageable pageable) {
        log.debug("REST request to get AlertUsers by criteria: {}", criteria);
        Page<AlertUserDTO> page = alertUserQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/alert-users");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /alert-users/:id : get the "id" alertUser.
     *
     * @param id the id of the alertUserDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the alertUserDTO, or with status 404 (Not Found)
     */
    @GetMapping("/alert-users/{id}")
    @Timed
    public ResponseEntity<AlertUserDTO> getAlertUser(@PathVariable Long id) {
        log.debug("REST request to get AlertUser : {}", id);
        AlertUserDTO alertUserDTO = alertUserService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(alertUserDTO));
    }

    /**
     * DELETE  /alert-users/:id : delete the "id" alertUser.
     *
     * @param id the id of the alertUserDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/alert-users/{id}")
    @Timed
    public ResponseEntity<Void> deleteAlertUser(@PathVariable Long id) {
        log.debug("REST request to delete AlertUser : {}", id);
        alertUserService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
