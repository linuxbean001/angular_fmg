package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.service.AppointmentAttendeeService;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import com.fmg.cma.web.rest.util.PaginationUtil;
import com.fmg.cma.service.dto.AppointmentAttendeeDTO;
import com.fmg.cma.service.dto.AppointmentAttendeeCriteria;
import com.fmg.cma.service.AppointmentAttendeeQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AppointmentAttendee.
 */
@RestController
@RequestMapping("/api")
public class AppointmentAttendeeResource {

    private final Logger log = LoggerFactory.getLogger(AppointmentAttendeeResource.class);

    private static final String ENTITY_NAME = "appointmentAttendee";

    private final AppointmentAttendeeService appointmentAttendeeService;

    private final AppointmentAttendeeQueryService appointmentAttendeeQueryService;

    public AppointmentAttendeeResource(AppointmentAttendeeService appointmentAttendeeService, AppointmentAttendeeQueryService appointmentAttendeeQueryService) {
        this.appointmentAttendeeService = appointmentAttendeeService;
        this.appointmentAttendeeQueryService = appointmentAttendeeQueryService;
    }

    /**
     * POST  /appointment-attendees : Create a new appointmentAttendee.
     *
     * @param appointmentAttendeeDTO the appointmentAttendeeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new appointmentAttendeeDTO, or with status 400 (Bad Request) if the appointmentAttendee has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/appointment-attendees")
    @Timed
    public ResponseEntity<AppointmentAttendeeDTO> createAppointmentAttendee(@Valid @RequestBody AppointmentAttendeeDTO appointmentAttendeeDTO) throws URISyntaxException {
        log.debug("REST request to save AppointmentAttendee : {}", appointmentAttendeeDTO);
        if (appointmentAttendeeDTO.getId() != null) {
            throw new BadRequestAlertException("A new appointmentAttendee cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AppointmentAttendeeDTO result = appointmentAttendeeService.save(appointmentAttendeeDTO);
        return ResponseEntity.created(new URI("/api/appointment-attendees/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /appointment-attendees : Updates an existing appointmentAttendee.
     *
     * @param appointmentAttendeeDTO the appointmentAttendeeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated appointmentAttendeeDTO,
     * or with status 400 (Bad Request) if the appointmentAttendeeDTO is not valid,
     * or with status 500 (Internal Server Error) if the appointmentAttendeeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/appointment-attendees")
    @Timed
    public ResponseEntity<AppointmentAttendeeDTO> updateAppointmentAttendee(@Valid @RequestBody AppointmentAttendeeDTO appointmentAttendeeDTO) throws URISyntaxException {
        log.debug("REST request to update AppointmentAttendee : {}", appointmentAttendeeDTO);
        if (appointmentAttendeeDTO.getId() == null) {
            return createAppointmentAttendee(appointmentAttendeeDTO);
        }
        AppointmentAttendeeDTO result = appointmentAttendeeService.save(appointmentAttendeeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, appointmentAttendeeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /appointment-attendees : get all the appointmentAttendees.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of appointmentAttendees in body
     */
    @GetMapping("/appointment-attendees")
    @Timed
    public ResponseEntity<List<AppointmentAttendeeDTO>> getAllAppointmentAttendees(AppointmentAttendeeCriteria criteria, Pageable pageable) {
        log.debug("REST request to get AppointmentAttendees by criteria: {}", criteria);
        Page<AppointmentAttendeeDTO> page = appointmentAttendeeQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/appointment-attendees");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /appointment-attendees/:id : get the "id" appointmentAttendee.
     *
     * @param id the id of the appointmentAttendeeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the appointmentAttendeeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/appointment-attendees/{id}")
    @Timed
    public ResponseEntity<AppointmentAttendeeDTO> getAppointmentAttendee(@PathVariable Long id) {
        log.debug("REST request to get AppointmentAttendee : {}", id);
        AppointmentAttendeeDTO appointmentAttendeeDTO = appointmentAttendeeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(appointmentAttendeeDTO));
    }

    /**
     * DELETE  /appointment-attendees/:id : delete the "id" appointmentAttendee.
     *
     * @param id the id of the appointmentAttendeeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/appointment-attendees/{id}")
    @Timed
    public ResponseEntity<Void> deleteAppointmentAttendee(@PathVariable Long id) {
        log.debug("REST request to delete AppointmentAttendee : {}", id);
        appointmentAttendeeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
