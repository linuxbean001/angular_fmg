package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.service.BookingQuestionFieldQueryService;
import com.fmg.cma.service.BookingQuestionFieldService;
import com.fmg.cma.service.dto.BookingQuestionFieldCriteria;
import com.fmg.cma.service.dto.BookingQuestionFieldDTO;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import com.fmg.cma.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing BookingQuestionField.
 */
@RestController
@RequestMapping("/api")
public class BookingQuestionFieldResource {

    private final Logger log = LoggerFactory.getLogger(BookingQuestionFieldResource.class);

    private static final String ENTITY_NAME = "bookingQuestionField";

    private final BookingQuestionFieldService bookingQuestionFieldService;

    private final BookingQuestionFieldQueryService bookingQuestionFieldQueryService;

    public BookingQuestionFieldResource(BookingQuestionFieldService bookingQuestionFieldService, BookingQuestionFieldQueryService bookingQuestionFieldQueryService) {
        this.bookingQuestionFieldService = bookingQuestionFieldService;
        this.bookingQuestionFieldQueryService = bookingQuestionFieldQueryService;
    }

    /**
     * POST  /booking-question-fields : Create a new bookingQuestionField.
     *
     * @param bookingQuestionFieldDTO the bookingQuestionFieldDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new bookingQuestionFieldDTO, or with status 400 (Bad Request) if the bookingQuestionField has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/booking-question-fields")
    @Timed
    public ResponseEntity<BookingQuestionFieldDTO> createBookingQuestionField(@RequestBody BookingQuestionFieldDTO bookingQuestionFieldDTO) throws URISyntaxException {
        log.debug("REST request to save BookingQuestionField : {}", bookingQuestionFieldDTO);
        if (bookingQuestionFieldDTO.getId() != null) {
            throw new BadRequestAlertException("A new bookingQuestionField cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BookingQuestionFieldDTO result = bookingQuestionFieldService.save(bookingQuestionFieldDTO);
        return ResponseEntity.created(new URI("/api/booking-question-fields/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /booking-question-fields : Updates an existing bookingQuestionField.
     *
     * @param bookingQuestionFieldDTO the bookingQuestionFieldDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated bookingQuestionFieldDTO,
     * or with status 400 (Bad Request) if the bookingQuestionFieldDTO is not valid,
     * or with status 500 (Internal Server Error) if the bookingQuestionFieldDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/booking-question-fields")
    @Timed
    public ResponseEntity<BookingQuestionFieldDTO> updateBookingQuestionField(@RequestBody BookingQuestionFieldDTO bookingQuestionFieldDTO) throws URISyntaxException {
        log.debug("REST request to update BookingQuestionField : {}", bookingQuestionFieldDTO);
        if (bookingQuestionFieldDTO.getId() == null) {
            return createBookingQuestionField(bookingQuestionFieldDTO);
        }
        BookingQuestionFieldDTO result = bookingQuestionFieldService.save(bookingQuestionFieldDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, bookingQuestionFieldDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /booking-question-fields : get all the bookingQuestionFields.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of bookingQuestionFields in body
     */
    @GetMapping("/booking-question-fields")
    @Timed
    public ResponseEntity<List<BookingQuestionFieldDTO>> getAllBookingQuestionFields(BookingQuestionFieldCriteria criteria, Pageable pageable) {
        log.debug("REST request to get BookingQuestionFields by criteria: {}", criteria);
        Page<BookingQuestionFieldDTO> page = bookingQuestionFieldQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/booking-question-fields");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /booking-question-fields/:id : get the "id" bookingQuestionField.
     *
     * @param id the id of the bookingQuestionFieldDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the bookingQuestionFieldDTO, or with status 404 (Not Found)
     */
    @GetMapping("/booking-question-fields/{id}")
    @Timed
    public ResponseEntity<BookingQuestionFieldDTO> getBookingQuestionField(@PathVariable Long id) {
        log.debug("REST request to get BookingQuestionField : {}", id);
        BookingQuestionFieldDTO bookingQuestionFieldDTO = bookingQuestionFieldService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(bookingQuestionFieldDTO));
    }

    /**
     * DELETE  /booking-question-fields/:id : delete the "id" bookingQuestionField.
     *
     * @param id the id of the bookingQuestionFieldDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/booking-question-fields/{id}")
    @Timed
    public ResponseEntity<Void> deleteBookingQuestionField(@PathVariable Long id) {
        log.debug("REST request to delete BookingQuestionField : {}", id);
        bookingQuestionFieldService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
