package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.service.BookingQuestionQueryService;
import com.fmg.cma.service.BookingQuestionService;
import com.fmg.cma.service.dto.BookingQuestionCriteria;
import com.fmg.cma.service.dto.BookingQuestionDTO;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import com.fmg.cma.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing BookingQuestion.
 */
@RestController
@RequestMapping("/api")
public class BookingQuestionResource {

    private final Logger log = LoggerFactory.getLogger(BookingQuestionResource.class);

    private static final String ENTITY_NAME = "bookingQuestion";

    private final BookingQuestionService bookingQuestionService;

    private final BookingQuestionQueryService bookingQuestionQueryService;

    public BookingQuestionResource(BookingQuestionService bookingQuestionService, BookingQuestionQueryService bookingQuestionQueryService) {
        this.bookingQuestionService = bookingQuestionService;
        this.bookingQuestionQueryService = bookingQuestionQueryService;
    }

    /**
     * POST  /booking-questions : Create a new bookingQuestion.
     *
     * @param bookingQuestionDTO the bookingQuestionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new bookingQuestionDTO, or with status 400 (Bad Request) if the bookingQuestion has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/booking-questions")
    @Timed
    public ResponseEntity<BookingQuestionDTO> createBookingQuestion(@RequestBody BookingQuestionDTO bookingQuestionDTO) throws URISyntaxException {
        log.debug("REST request to save BookingQuestion : {}", bookingQuestionDTO);
        if (bookingQuestionDTO.getId() != null) {
            throw new BadRequestAlertException("A new bookingQuestion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BookingQuestionDTO result = bookingQuestionService.save(bookingQuestionDTO);
        return ResponseEntity.created(new URI("/api/booking-questions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /booking-questions : Updates an existing bookingQuestion.
     *
     * @param bookingQuestionDTO the bookingQuestionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated bookingQuestionDTO,
     * or with status 400 (Bad Request) if the bookingQuestionDTO is not valid,
     * or with status 500 (Internal Server Error) if the bookingQuestionDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/booking-questions")
    @Timed
    public ResponseEntity<BookingQuestionDTO> updateBookingQuestion(@RequestBody BookingQuestionDTO bookingQuestionDTO) throws URISyntaxException {
        log.debug("REST request to update BookingQuestion : {}", bookingQuestionDTO);
        if (bookingQuestionDTO.getId() == null) {
            return createBookingQuestion(bookingQuestionDTO);
        }
        BookingQuestionDTO result = bookingQuestionService.save(bookingQuestionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, bookingQuestionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /booking-questions : get all the bookingQuestions.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of bookingQuestions in body
     */
    @GetMapping("/booking-questions")
    @Timed
    public ResponseEntity<List<BookingQuestionDTO>> getAllBookingQuestions(BookingQuestionCriteria criteria, Pageable pageable) {
        log.debug("REST request to get BookingQuestions by criteria: {}", criteria);
        Page<BookingQuestionDTO> page = bookingQuestionQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/booking-questions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /booking-questions/:id : get the "id" bookingQuestion.
     *
     * @param id the id of the bookingQuestionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the bookingQuestionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/booking-questions/{id}")
    @Timed
    public ResponseEntity<BookingQuestionDTO> getBookingQuestion(@PathVariable Long id) {
        log.debug("REST request to get BookingQuestion : {}", id);
        BookingQuestionDTO bookingQuestionDTO = bookingQuestionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(bookingQuestionDTO));
    }

    /**
     * DELETE  /booking-questions/:id : delete the "id" bookingQuestion.
     *
     * @param id the id of the bookingQuestionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/booking-questions/{id}")
    @Timed
    public ResponseEntity<Void> deleteBookingQuestion(@PathVariable Long id) {
        log.debug("REST request to delete BookingQuestion : {}", id);
        bookingQuestionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
