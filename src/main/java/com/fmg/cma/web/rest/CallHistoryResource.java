package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.service.CallHistoryService;
import com.fmg.cma.service.dto.CallHistoryDTO;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import com.fmg.cma.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CallHistory.
 */
@RestController
@RequestMapping("/api")
public class CallHistoryResource {

    private final Logger log = LoggerFactory.getLogger(CallHistoryResource.class);

    private static final String ENTITY_NAME = "callHistory";

    private final CallHistoryService callHistoryService;

    public CallHistoryResource(CallHistoryService callHistoryService) {
        this.callHistoryService = callHistoryService;
    }

    /**
     * POST  /call-histories : Create a new callHistory.
     *
     * @param callHistoryDTO the callHistoryDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new callHistoryDTO, or with status 400 (Bad Request) if the callHistory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/call-histories")
    @Timed
    public ResponseEntity<CallHistoryDTO> createCallHistory(@RequestBody CallHistoryDTO callHistoryDTO) throws URISyntaxException {
        log.debug("REST request to save CallHistory : {}", callHistoryDTO);
        if (callHistoryDTO.getId() != null) {
            throw new BadRequestAlertException("A new callHistory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CallHistoryDTO result = callHistoryService.save(callHistoryDTO);
        return ResponseEntity.created(new URI("/api/call-histories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /call-histories : Updates an existing callHistory.
     *
     * @param callHistoryDTO the callHistoryDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated callHistoryDTO,
     * or with status 400 (Bad Request) if the callHistoryDTO is not valid,
     * or with status 500 (Internal Server Error) if the callHistoryDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/call-histories")
    @Timed
    public ResponseEntity<CallHistoryDTO> updateCallHistory(@RequestBody CallHistoryDTO callHistoryDTO) throws URISyntaxException {
        log.debug("REST request to update CallHistory : {}", callHistoryDTO);
        if (callHistoryDTO.getId() == null) {
            return createCallHistory(callHistoryDTO);
        }
        CallHistoryDTO result = callHistoryService.save(callHistoryDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, callHistoryDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /call-histories : get all the callHistories.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of callHistories in body
     */
    @GetMapping("/call-histories")
    @Timed
    public ResponseEntity<List<CallHistoryDTO>> getAllCallHistories(Pageable pageable) {
        log.debug("REST request to get a page of CallHistories");
        Page<CallHistoryDTO> page = callHistoryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/call-histories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /call-histories/:id : get the "id" callHistory.
     *
     * @param id the id of the callHistoryDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the callHistoryDTO, or with status 404 (Not Found)
     */
    @GetMapping("/call-histories/{id}")
    @Timed
    public ResponseEntity<CallHistoryDTO> getCallHistory(@PathVariable Long id) {
        log.debug("REST request to get CallHistory : {}", id);
        CallHistoryDTO callHistoryDTO = callHistoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(callHistoryDTO));
    }

    /**
     * DELETE  /call-histories/:id : delete the "id" callHistory.
     *
     * @param id the id of the callHistoryDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/call-histories/{id}")
    @Timed
    public ResponseEntity<Void> deleteCallHistory(@PathVariable Long id) {
        log.debug("REST request to delete CallHistory : {}", id);
        callHistoryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
