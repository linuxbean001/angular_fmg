package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.service.CallHistoryTypeService;
import com.fmg.cma.service.dto.CallHistoryTypeDTO;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import com.fmg.cma.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CallHistoryType.
 */
@RestController
@RequestMapping("/api")
public class CallHistoryTypeResource {

    private final Logger log = LoggerFactory.getLogger(CallHistoryTypeResource.class);

    private static final String ENTITY_NAME = "callHistoryType";

    private final CallHistoryTypeService callHistoryTypeService;

    public CallHistoryTypeResource(CallHistoryTypeService callHistoryTypeService) {
        this.callHistoryTypeService = callHistoryTypeService;
    }

    /**
     * POST  /call-history-types : Create a new callHistoryType.
     *
     * @param callHistoryTypeDTO the callHistoryTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new callHistoryTypeDTO, or with status 400 (Bad Request) if the callHistoryType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/call-history-types")
    @Timed
    public ResponseEntity<CallHistoryTypeDTO> createCallHistoryType(@RequestBody CallHistoryTypeDTO callHistoryTypeDTO) throws URISyntaxException {
        log.debug("REST request to save CallHistoryType : {}", callHistoryTypeDTO);
        if (callHistoryTypeDTO.getId() != null) {
            throw new BadRequestAlertException("A new callHistoryType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CallHistoryTypeDTO result = callHistoryTypeService.save(callHistoryTypeDTO);
        return ResponseEntity.created(new URI("/api/call-history-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /call-history-types : Updates an existing callHistoryType.
     *
     * @param callHistoryTypeDTO the callHistoryTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated callHistoryTypeDTO,
     * or with status 400 (Bad Request) if the callHistoryTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the callHistoryTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/call-history-types")
    @Timed
    public ResponseEntity<CallHistoryTypeDTO> updateCallHistoryType(@RequestBody CallHistoryTypeDTO callHistoryTypeDTO) throws URISyntaxException {
        log.debug("REST request to update CallHistoryType : {}", callHistoryTypeDTO);
        if (callHistoryTypeDTO.getId() == null) {
            return createCallHistoryType(callHistoryTypeDTO);
        }
        CallHistoryTypeDTO result = callHistoryTypeService.save(callHistoryTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, callHistoryTypeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /call-history-types : get all the callHistoryTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of callHistoryTypes in body
     */
    @GetMapping("/call-history-types")
    @Timed
    public ResponseEntity<List<CallHistoryTypeDTO>> getAllCallHistoryTypes(Pageable pageable) {
        log.debug("REST request to get a page of CallHistoryTypes");
        Page<CallHistoryTypeDTO> page = callHistoryTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/call-history-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /call-history-types/:id : get the "id" callHistoryType.
     *
     * @param id the id of the callHistoryTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the callHistoryTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/call-history-types/{id}")
    @Timed
    public ResponseEntity<CallHistoryTypeDTO> getCallHistoryType(@PathVariable Long id) {
        log.debug("REST request to get CallHistoryType : {}", id);
        CallHistoryTypeDTO callHistoryTypeDTO = callHistoryTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(callHistoryTypeDTO));
    }

    /**
     * DELETE  /call-history-types/:id : delete the "id" callHistoryType.
     *
     * @param id the id of the callHistoryTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/call-history-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteCallHistoryType(@PathVariable Long id) {
        log.debug("REST request to delete CallHistoryType : {}", id);
        callHistoryTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
