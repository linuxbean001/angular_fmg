package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.service.CampaignListContactManagementQueryService;
import com.fmg.cma.service.CampaignListContactManagementService;
import com.fmg.cma.service.dto.CampaignListContactManagementCriteria;
import com.fmg.cma.service.dto.CampaignListContactManagementDTO;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import com.fmg.cma.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CampaignListContactManagement.
 */
@RestController
@RequestMapping("/api")
public class CampaignListContactManagementResource {

    private final Logger log = LoggerFactory.getLogger(CampaignListContactManagementResource.class);

    private static final String ENTITY_NAME = "campaignListContactManagement";

    private final CampaignListContactManagementService campaignListContactManagementService;

    private final CampaignListContactManagementQueryService campaignListContactManagementQueryService;

    public CampaignListContactManagementResource(CampaignListContactManagementService campaignListContactManagementService, CampaignListContactManagementQueryService campaignListContactManagementQueryService) {
        this.campaignListContactManagementService = campaignListContactManagementService;
        this.campaignListContactManagementQueryService = campaignListContactManagementQueryService;
    }

    /**
     * POST  /campaign-list-contact-managements : Create a new campaignListContactManagement.
     *
     * @param campaignListContactManagementDTO the campaignListContactManagementDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new campaignListContactManagementDTO, or with status 400 (Bad Request) if the campaignListContactManagement has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/campaign-list-contact-managements")
    @Timed
    public ResponseEntity<CampaignListContactManagementDTO> createCampaignListContactManagement(@RequestBody CampaignListContactManagementDTO campaignListContactManagementDTO) throws URISyntaxException {
        log.debug("REST request to save CampaignListContactManagement : {}", campaignListContactManagementDTO);
        if (campaignListContactManagementDTO.getId() != null) {
            throw new BadRequestAlertException("A new campaignListContactManagement cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CampaignListContactManagementDTO result = campaignListContactManagementService.save(campaignListContactManagementDTO);
        return ResponseEntity.created(new URI("/api/campaign-list-contact-managements/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /campaign-list-contact-managements : Updates an existing campaignListContactManagement.
     *
     * @param campaignListContactManagementDTO the campaignListContactManagementDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated campaignListContactManagementDTO,
     * or with status 400 (Bad Request) if the campaignListContactManagementDTO is not valid,
     * or with status 500 (Internal Server Error) if the campaignListContactManagementDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/campaign-list-contact-managements")
    @Timed
    public ResponseEntity<CampaignListContactManagementDTO> updateCampaignListContactManagement(@RequestBody CampaignListContactManagementDTO campaignListContactManagementDTO) throws URISyntaxException {
        log.debug("REST request to update CampaignListContactManagement : {}", campaignListContactManagementDTO);
        if (campaignListContactManagementDTO.getId() == null) {
            return createCampaignListContactManagement(campaignListContactManagementDTO);
        }
        CampaignListContactManagementDTO result = campaignListContactManagementService.save(campaignListContactManagementDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, campaignListContactManagementDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /campaign-list-contact-managements : get all the campaignListContactManagements.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of campaignListContactManagements in body
     */
    @GetMapping("/campaign-list-contact-managements")
    @Timed
    public ResponseEntity<List<CampaignListContactManagementDTO>> getAllCampaignListContactManagements(CampaignListContactManagementCriteria criteria, Pageable pageable) {
        log.debug("REST request to get CampaignListContactManagements by criteria: {}", criteria);
        Page<CampaignListContactManagementDTO> page = campaignListContactManagementQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/campaign-list-contact-managements");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /campaign-list-contact-managements/:id : get the "id" campaignListContactManagement.
     *
     * @param id the id of the campaignListContactManagementDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the campaignListContactManagementDTO, or with status 404 (Not Found)
     */
    @GetMapping("/campaign-list-contact-managements/{id}")
    @Timed
    public ResponseEntity<CampaignListContactManagementDTO> getCampaignListContactManagement(@PathVariable Long id) {
        log.debug("REST request to get CampaignListContactManagement : {}", id);
        CampaignListContactManagementDTO campaignListContactManagementDTO = campaignListContactManagementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(campaignListContactManagementDTO));
    }

    /**
     * DELETE  /campaign-list-contact-managements/:id : delete the "id" campaignListContactManagement.
     *
     * @param id the id of the campaignListContactManagementDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/campaign-list-contact-managements/{id}")
    @Timed
    public ResponseEntity<Void> deleteCampaignListContactManagement(@PathVariable Long id) {
        log.debug("REST request to delete CampaignListContactManagement : {}", id);
        campaignListContactManagementService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
