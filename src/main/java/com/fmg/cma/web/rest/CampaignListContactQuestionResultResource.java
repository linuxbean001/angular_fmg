package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.service.CampaignListContactQuestionResultQueryService;
import com.fmg.cma.service.CampaignListContactQuestionResultService;
import com.fmg.cma.service.dto.CampaignListContactQuestionResultCriteria;
import com.fmg.cma.service.dto.CampaignListContactQuestionResultDTO;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import com.fmg.cma.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CampaignListContactQuestionResult.
 */
@RestController
@RequestMapping("/api")
public class CampaignListContactQuestionResultResource {

    private final Logger log = LoggerFactory.getLogger(CampaignListContactQuestionResultResource.class);

    private static final String ENTITY_NAME = "campaignListContactQuestionResult";

    private final CampaignListContactQuestionResultService campaignListContactQuestionResultService;

    private final CampaignListContactQuestionResultQueryService campaignListContactQuestionResultQueryService;

    public CampaignListContactQuestionResultResource(CampaignListContactQuestionResultService campaignListContactQuestionResultService, CampaignListContactQuestionResultQueryService campaignListContactQuestionResultQueryService) {
        this.campaignListContactQuestionResultService = campaignListContactQuestionResultService;
        this.campaignListContactQuestionResultQueryService = campaignListContactQuestionResultQueryService;
    }

    /**
     * POST  /campaign-list-contact-question-results : Create a new campaignListContactQuestionResult.
     *
     * @param campaignListContactQuestionResultDTO the campaignListContactQuestionResultDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new campaignListContactQuestionResultDTO, or with status 400 (Bad Request) if the campaignListContactQuestionResult has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/campaign-list-contact-question-results")
    @Timed
    public ResponseEntity<CampaignListContactQuestionResultDTO> createCampaignListContactQuestionResult(@RequestBody CampaignListContactQuestionResultDTO campaignListContactQuestionResultDTO) throws URISyntaxException {
        log.debug("REST request to save CampaignListContactQuestionResult : {}", campaignListContactQuestionResultDTO);
        if (campaignListContactQuestionResultDTO.getId() != null) {
            throw new BadRequestAlertException("A new campaignListContactQuestionResult cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CampaignListContactQuestionResultDTO result = campaignListContactQuestionResultService.save(campaignListContactQuestionResultDTO);
        return ResponseEntity.created(new URI("/api/campaign-list-contact-question-results/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /campaign-list-contact-question-results : Updates an existing campaignListContactQuestionResult.
     *
     * @param campaignListContactQuestionResultDTO the campaignListContactQuestionResultDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated campaignListContactQuestionResultDTO,
     * or with status 400 (Bad Request) if the campaignListContactQuestionResultDTO is not valid,
     * or with status 500 (Internal Server Error) if the campaignListContactQuestionResultDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/campaign-list-contact-question-results")
    @Timed
    public ResponseEntity<CampaignListContactQuestionResultDTO> updateCampaignListContactQuestionResult(@RequestBody CampaignListContactQuestionResultDTO campaignListContactQuestionResultDTO) throws URISyntaxException {
        log.debug("REST request to update CampaignListContactQuestionResult : {}", campaignListContactQuestionResultDTO);
        if (campaignListContactQuestionResultDTO.getId() == null) {
            return createCampaignListContactQuestionResult(campaignListContactQuestionResultDTO);
        }
        CampaignListContactQuestionResultDTO result = campaignListContactQuestionResultService.save(campaignListContactQuestionResultDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, campaignListContactQuestionResultDTO.getId().toString()))
            .body(result);
    }

    @PutMapping("/campaign-list-contact-question-results-multiple")
    @Timed
    public List<CampaignListContactQuestionResultDTO> updateCampaignListContactQuestionResult(@RequestBody List<CampaignListContactQuestionResultDTO> campaignListContactQuestionResultDTOs) throws URISyntaxException {
        log.debug("REST request to update CampaignListContactQuestionResults : {}", campaignListContactQuestionResultDTOs);
        return campaignListContactQuestionResultService.save(campaignListContactQuestionResultDTOs);
    }

    /**
     * GET  /campaign-list-contact-question-results : get all the campaignListContactQuestionResults.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of campaignListContactQuestionResults in body
     */
    @GetMapping("/campaign-list-contact-question-results")
    @Timed
    public ResponseEntity<List<CampaignListContactQuestionResultDTO>> getAllCampaignListContactQuestionResults(CampaignListContactQuestionResultCriteria criteria, Pageable pageable) {
        log.debug("REST request to get CampaignListContactQuestionResults by criteria: {}", criteria);
        Page<CampaignListContactQuestionResultDTO> page = campaignListContactQuestionResultQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/campaign-list-contact-question-results");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /campaign-list-contact-question-results/:id : get the "id" campaignListContactQuestionResult.
     *
     * @param id the id of the campaignListContactQuestionResultDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the campaignListContactQuestionResultDTO, or with status 404 (Not Found)
     */
    @GetMapping("/campaign-list-contact-question-results/{id}")
    @Timed
    public ResponseEntity<CampaignListContactQuestionResultDTO> getCampaignListContactQuestionResult(@PathVariable Long id) {
        log.debug("REST request to get CampaignListContactQuestionResult : {}", id);
        CampaignListContactQuestionResultDTO campaignListContactQuestionResultDTO = campaignListContactQuestionResultService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(campaignListContactQuestionResultDTO));
    }

    /**
     * DELETE  /campaign-list-contact-question-results/:id : delete the "id" campaignListContactQuestionResult.
     *
     * @param id the id of the campaignListContactQuestionResultDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/campaign-list-contact-question-results/{id}")
    @Timed
    public ResponseEntity<Void> deleteCampaignListContactQuestionResult(@PathVariable Long id) {
        log.debug("REST request to delete CampaignListContactQuestionResult : {}", id);
        campaignListContactQuestionResultService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
