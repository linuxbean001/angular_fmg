package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.service.CampaignListContactResultQueryService;
import com.fmg.cma.service.CampaignListContactResultService;
import com.fmg.cma.service.dto.AlertElementDto;
import com.fmg.cma.service.dto.CampaignListContactResultCriteria;
import com.fmg.cma.service.dto.CampaignListContactResultDTO;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import com.fmg.cma.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CampaignListContactResult.
 */
@RestController
@RequestMapping("/api")
public class CampaignListContactResultResource {

    private final Logger log = LoggerFactory.getLogger(CampaignListContactResultResource.class);

    private static final String ENTITY_NAME = "campaignListContactResult";

    private final CampaignListContactResultService campaignListContactResultService;

    private final CampaignListContactResultQueryService campaignListContactResultQueryService;

    public CampaignListContactResultResource(CampaignListContactResultService campaignListContactResultService, CampaignListContactResultQueryService campaignListContactResultQueryService) {
        this.campaignListContactResultService = campaignListContactResultService;
        this.campaignListContactResultQueryService = campaignListContactResultQueryService;
    }

    /**
     * POST  /campaign-list-contact-results : Create a new campaignListContactResult.
     *
     * @param campaignListContactResultDTO the campaignListContactResultDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new campaignListContactResultDTO, or with status 400 (Bad Request) if the campaignListContactResult has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/campaign-list-contact-results")
    @Timed
    public ResponseEntity<CampaignListContactResultDTO> createCampaignListContactResult(@RequestBody CampaignListContactResultDTO campaignListContactResultDTO) throws URISyntaxException {
        log.debug("REST request to save CampaignListContactResult : {}", campaignListContactResultDTO);
        if (campaignListContactResultDTO.getId() != null) {
            throw new BadRequestAlertException("A new campaignListContactResult cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CampaignListContactResultDTO result = campaignListContactResultService.save(campaignListContactResultDTO);
        return ResponseEntity.created(new URI("/api/campaign-list-contact-results/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /campaign-list-contact-results : Updates an existing campaignListContactResult.
     *
     * @param campaignListContactResultDTO the campaignListContactResultDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated campaignListContactResultDTO,
     * or with status 400 (Bad Request) if the campaignListContactResultDTO is not valid,
     * or with status 500 (Internal Server Error) if the campaignListContactResultDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/campaign-list-contact-results")
    @Timed
    public ResponseEntity<CampaignListContactResultDTO> updateCampaignListContactResult(@RequestBody CampaignListContactResultDTO campaignListContactResultDTO) throws URISyntaxException {
        log.debug("REST request to update CampaignListContactResult : {}", campaignListContactResultDTO);
        if (campaignListContactResultDTO.getId() == null) {
            return createCampaignListContactResult(campaignListContactResultDTO);
        }
        CampaignListContactResultDTO result = campaignListContactResultService.save(campaignListContactResultDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, campaignListContactResultDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /campaign-list-contact-results : get all the campaignListContactResults.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of campaignListContactResults in body
     */
    @GetMapping("/campaign-list-contact-results")
    @Timed
    public ResponseEntity<List<CampaignListContactResultDTO>> getAllCampaignListContactResults(CampaignListContactResultCriteria criteria, Pageable pageable) {
        log.debug("REST request to get CampaignListContactResults by criteria: {}", criteria);
        Page<CampaignListContactResultDTO> page = campaignListContactResultQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/campaign-list-contact-results");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /campaign-list-contact-results/:id : get the "id" campaignListContactResult.
     *
     * @param id the id of the campaignListContactResultDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the campaignListContactResultDTO, or with status 404 (Not Found)
     */
    @GetMapping("/campaign-list-contact-results/{id}")
    @Timed
    public ResponseEntity<CampaignListContactResultDTO> getCampaignListContactResult(@PathVariable Long id) {
        log.debug("REST request to get CampaignListContactResult : {}", id);
        CampaignListContactResultDTO campaignListContactResultDTO = campaignListContactResultService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(campaignListContactResultDTO));
    }

    /**
     * DELETE  /campaign-list-contact-results/:id : delete the "id" campaignListContactResult.
     *
     * @param id the id of the campaignListContactResultDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/campaign-list-contact-results/{id}")
    @Timed
    public ResponseEntity<Void> deleteCampaignListContactResult(@PathVariable Long id) {
        log.debug("REST request to delete CampaignListContactResult : {}", id);
        campaignListContactResultService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/current-user-alerts")
    @Timed
    public List<AlertElementDto> getAlertsForCurrentUser() {
        return campaignListContactResultService.getAlertsForCurrentUser();
    }
}
