package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.service.CampaignPortalUserOutcomeService;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import com.fmg.cma.web.rest.util.PaginationUtil;
import com.fmg.cma.service.dto.CampaignPortalUserOutcomeDTO;
import com.fmg.cma.service.dto.CampaignPortalUserOutcomeCriteria;
import com.fmg.cma.service.CampaignPortalUserOutcomeQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CampaignPortalUserOutcome.
 */
@RestController
@RequestMapping("/api")
public class CampaignPortalUserOutcomeResource {

    private final Logger log = LoggerFactory.getLogger(CampaignPortalUserOutcomeResource.class);

    private static final String ENTITY_NAME = "campaignPortalUserOutcome";

    private final CampaignPortalUserOutcomeService campaignPortalUserOutcomeService;

    private final CampaignPortalUserOutcomeQueryService campaignPortalUserOutcomeQueryService;

    public CampaignPortalUserOutcomeResource(CampaignPortalUserOutcomeService campaignPortalUserOutcomeService, CampaignPortalUserOutcomeQueryService campaignPortalUserOutcomeQueryService) {
        this.campaignPortalUserOutcomeService = campaignPortalUserOutcomeService;
        this.campaignPortalUserOutcomeQueryService = campaignPortalUserOutcomeQueryService;
    }

    /**
     * POST  /campaign-portal-user-outcomes : Create a new campaignPortalUserOutcome.
     *
     * @param campaignPortalUserOutcomeDTO the campaignPortalUserOutcomeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new campaignPortalUserOutcomeDTO, or with status 400 (Bad Request) if the campaignPortalUserOutcome has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/campaign-portal-user-outcomes")
    @Timed
    public ResponseEntity<CampaignPortalUserOutcomeDTO> createCampaignPortalUserOutcome(@RequestBody CampaignPortalUserOutcomeDTO campaignPortalUserOutcomeDTO) throws URISyntaxException {
        log.debug("REST request to save CampaignPortalUserOutcome : {}", campaignPortalUserOutcomeDTO);
        if (campaignPortalUserOutcomeDTO.getId() != null) {
            throw new BadRequestAlertException("A new campaignPortalUserOutcome cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CampaignPortalUserOutcomeDTO result = campaignPortalUserOutcomeService.save(campaignPortalUserOutcomeDTO);
        return ResponseEntity.created(new URI("/api/campaign-portal-user-outcomes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /campaign-portal-user-outcomes : Updates an existing campaignPortalUserOutcome.
     *
     * @param campaignPortalUserOutcomeDTO the campaignPortalUserOutcomeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated campaignPortalUserOutcomeDTO,
     * or with status 400 (Bad Request) if the campaignPortalUserOutcomeDTO is not valid,
     * or with status 500 (Internal Server Error) if the campaignPortalUserOutcomeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/campaign-portal-user-outcomes")
    @Timed
    public ResponseEntity<CampaignPortalUserOutcomeDTO> updateCampaignPortalUserOutcome(@RequestBody CampaignPortalUserOutcomeDTO campaignPortalUserOutcomeDTO) throws URISyntaxException {
        log.debug("REST request to update CampaignPortalUserOutcome : {}", campaignPortalUserOutcomeDTO);
        if (campaignPortalUserOutcomeDTO.getId() == null) {
            return createCampaignPortalUserOutcome(campaignPortalUserOutcomeDTO);
        }
        CampaignPortalUserOutcomeDTO result = campaignPortalUserOutcomeService.save(campaignPortalUserOutcomeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, campaignPortalUserOutcomeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /campaign-portal-user-outcomes : get all the campaignPortalUserOutcomes.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of campaignPortalUserOutcomes in body
     */
    @GetMapping("/campaign-portal-user-outcomes")
    @Timed
    public ResponseEntity<List<CampaignPortalUserOutcomeDTO>> getAllCampaignPortalUserOutcomes(CampaignPortalUserOutcomeCriteria criteria, Pageable pageable) {
        log.debug("REST request to get CampaignPortalUserOutcomes by criteria: {}", criteria);
        Page<CampaignPortalUserOutcomeDTO> page = campaignPortalUserOutcomeQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/campaign-portal-user-outcomes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /campaign-portal-user-outcomes/:id : get the "id" campaignPortalUserOutcome.
     *
     * @param id the id of the campaignPortalUserOutcomeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the campaignPortalUserOutcomeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/campaign-portal-user-outcomes/{id}")
    @Timed
    public ResponseEntity<CampaignPortalUserOutcomeDTO> getCampaignPortalUserOutcome(@PathVariable Long id) {
        log.debug("REST request to get CampaignPortalUserOutcome : {}", id);
        CampaignPortalUserOutcomeDTO campaignPortalUserOutcomeDTO = campaignPortalUserOutcomeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(campaignPortalUserOutcomeDTO));
    }

    /**
     * DELETE  /campaign-portal-user-outcomes/:id : delete the "id" campaignPortalUserOutcome.
     *
     * @param id the id of the campaignPortalUserOutcomeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/campaign-portal-user-outcomes/{id}")
    @Timed
    public ResponseEntity<Void> deleteCampaignPortalUserOutcome(@PathVariable Long id) {
        log.debug("REST request to delete CampaignPortalUserOutcome : {}", id);
        campaignPortalUserOutcomeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
