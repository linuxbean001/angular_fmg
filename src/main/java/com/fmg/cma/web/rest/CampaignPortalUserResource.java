package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.service.CampaignPortalUserQueryService;
import com.fmg.cma.service.CampaignPortalUserService;
import com.fmg.cma.service.dto.CampaignPortalUserCriteria;
import com.fmg.cma.service.dto.CampaignPortalUserDTO;
import com.fmg.cma.service.dto.CampaignPortalUserSummaryDTO;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import com.fmg.cma.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CampaignPortalUser.
 */
@RestController
@RequestMapping("/api")
public class CampaignPortalUserResource {

    private final Logger log = LoggerFactory.getLogger(CampaignPortalUserResource.class);

    private static final String ENTITY_NAME = "campaignPortalUser";

    private final CampaignPortalUserService campaignPortalUserService;

    private final CampaignPortalUserQueryService campaignPortalUserQueryService;

    public CampaignPortalUserResource(CampaignPortalUserService campaignPortalUserService,
                                      CampaignPortalUserQueryService campaignPortalUserQueryService) {
        this.campaignPortalUserService = campaignPortalUserService;
        this.campaignPortalUserQueryService = campaignPortalUserQueryService;
    }

    /**
     * POST  /campaign-portal-users : Create a new campaignPortalUser.
     *
     * @param campaignPortalUserDTO the campaignPortalUserDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new campaignPortalUserDTO, or with
     * status 400 (Bad Request) if the campaignPortalUser has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/campaign-portal-users")
    @Timed
    public ResponseEntity<CampaignPortalUserDTO> createCampaignPortalUser(@Valid @RequestBody CampaignPortalUserDTO
                                                                              campaignPortalUserDTO) throws
        URISyntaxException {
        log.info("CREATE CALLED!!!");
        log.debug("REST request to save CampaignPortalUser : {}", campaignPortalUserDTO);
        if (campaignPortalUserDTO.getId() != null) {
            throw new BadRequestAlertException("A new campaignPortalUser cannot already have an ID", ENTITY_NAME,
                "idexists");
        }
        CampaignPortalUserDTO result = campaignPortalUserService.save(campaignPortalUserDTO);
        return ResponseEntity.created(new URI("/api/campaign-portal-users/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /campaign-portal-users : Updates an existing campaignPortalUser.
     *
     * @param campaignPortalUserDTO the campaignPortalUserDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated campaignPortalUserDTO,
     * or with status 400 (Bad Request) if the campaignPortalUserDTO is not valid,
     * or with status 500 (Internal Server Error) if the campaignPortalUserDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/campaign-portal-users")
    @Timed
    public ResponseEntity<CampaignPortalUserDTO> updateCampaignPortalUser(@Valid @RequestBody CampaignPortalUserDTO
                                                                              campaignPortalUserDTO) throws
        URISyntaxException {
        log.info("JON12 WOW IN THS UPDATE!");
        log.debug("REST request to update CampaignPortalUser : {}", campaignPortalUserDTO);
        if (campaignPortalUserDTO.getId() == null) {
            return createCampaignPortalUser(campaignPortalUserDTO);
        }
        CampaignPortalUserDTO result = campaignPortalUserService.save(campaignPortalUserDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, campaignPortalUserDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /campaign-portal-users : get all the campaignPortalUsers.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of campaignPortalUsers in body
     */
    @GetMapping("/campaign-portal-users")
    @Timed
    public ResponseEntity<List<CampaignPortalUserDTO>> getAllCampaignPortalUsers(CampaignPortalUserCriteria criteria,
                                                                                 Pageable pageable) {
        log.debug("REST request to get CampaignPortalUsers by criteria: {}", criteria);
        Page<CampaignPortalUserDTO> page = campaignPortalUserQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/campaign-portal-users");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /campaign-portal-users/:id : get the "id" campaignPortalUser.
     *
     * @param id the id of the campaignPortalUserDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the campaignPortalUserDTO, or with status 404
     * (Not Found)
     */
    @GetMapping("/campaign-portal-users/{id}")
    @Timed
    public ResponseEntity<CampaignPortalUserDTO> getCampaignPortalUser(@PathVariable Long id) {
        log.debug("REST request to get CampaignPortalUser : {}", id);
        CampaignPortalUserDTO campaignPortalUserDTO = campaignPortalUserService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(campaignPortalUserDTO));
    }

    /**
     * DELETE  /campaign-portal-users/:id : delete the "id" campaignPortalUser.
     *
     * @param id the id of the campaignPortalUserDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/campaign-portal-users/{id}")
    @Timed
    public ResponseEntity<Void> deleteCampaignPortalUser(@PathVariable Long id) {
        log.debug("REST request to delete CampaignPortalUser : {}", id);
        campaignPortalUserService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/campaign-portal-users/{id}/summary")
    @Timed
    public ResponseEntity<CampaignPortalUserSummaryDTO> getCampaignPortalUserSummary(@PathVariable Long id) {
        log.debug("REST request to get CampaignPortalUserSummary : {}", id);
        CampaignPortalUserSummaryDTO campaignPortalUserSummaryDTO = campaignPortalUserService.getCampaignPortalUserSummary(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(campaignPortalUserSummaryDTO));
    }
}
