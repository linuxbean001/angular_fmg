package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.service.ClientCampaignAddDocQueryService;
import com.fmg.cma.service.ClientCampaignAddDocService;
import com.fmg.cma.service.dto.ClientCampaignAddDocCriteria;
import com.fmg.cma.service.dto.ClientCampaignAddDocDTO;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import com.fmg.cma.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ClientCampaignAddDoc.
 */
@RestController
@RequestMapping("/api")
public class ClientCampaignAddDocResource {

    private final Logger log = LoggerFactory.getLogger(ClientCampaignAddDocResource.class);

    private static final String ENTITY_NAME = "clientCampaignAddDoc";

    private final ClientCampaignAddDocService clientCampaignAddDocService;

    private final ClientCampaignAddDocQueryService clientCampaignAddDocQueryService;

    public ClientCampaignAddDocResource(ClientCampaignAddDocService clientCampaignAddDocService, ClientCampaignAddDocQueryService clientCampaignAddDocQueryService) {
        this.clientCampaignAddDocService = clientCampaignAddDocService;
        this.clientCampaignAddDocQueryService = clientCampaignAddDocQueryService;
    }

    /**
     * POST  /client-campaign-add-docs : Create a new clientCampaignAddDoc.
     *
     * @param clientCampaignAddDocDTO the clientCampaignAddDocDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new clientCampaignAddDocDTO, or with status 400 (Bad Request) if the clientCampaignAddDoc has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/client-campaign-add-docs")
    @Timed
    public ResponseEntity<ClientCampaignAddDocDTO> createClientCampaignAddDoc(@RequestBody ClientCampaignAddDocDTO clientCampaignAddDocDTO) throws URISyntaxException {
        log.debug("REST request to save ClientCampaignAddDoc : {}", clientCampaignAddDocDTO);
        if (clientCampaignAddDocDTO.getId() != null) {
            throw new BadRequestAlertException("A new clientCampaignAddDoc cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClientCampaignAddDocDTO result = clientCampaignAddDocService.save(clientCampaignAddDocDTO);
        return ResponseEntity.created(new URI("/api/client-campaign-add-docs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /client-campaign-add-docs : Updates an existing clientCampaignAddDoc.
     *
     * @param clientCampaignAddDocDTO the clientCampaignAddDocDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated clientCampaignAddDocDTO,
     *         or with status 400 (Bad Request) if the clientCampaignAddDocDTO is not valid,
     *         or with status 500 (Internal Server Error) if the clientCampaignAddDocDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/client-campaign-add-docs")
    @Timed
    public ResponseEntity<ClientCampaignAddDocDTO> updateClientCampaignAddDoc(@RequestBody ClientCampaignAddDocDTO clientCampaignAddDocDTO) throws URISyntaxException {
        log.debug("REST request to update ClientCampaignAddDoc : {}", clientCampaignAddDocDTO);
        if (clientCampaignAddDocDTO.getId() == null) {
            return createClientCampaignAddDoc(clientCampaignAddDocDTO);
        }
        ClientCampaignAddDocDTO result = clientCampaignAddDocService.save(clientCampaignAddDocDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, clientCampaignAddDocDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /client-campaign-add-docs : get all the clientCampaignAddDocs.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of clientCampaignAddDocs in body
     */
    @GetMapping("/client-campaign-add-docs")
    @Timed
    public ResponseEntity<List<ClientCampaignAddDocDTO>> getAllClientCampaignAddDocs(ClientCampaignAddDocCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ClientCampaignAddDocs by criteria: {}", criteria);
        Page<ClientCampaignAddDocDTO> page = clientCampaignAddDocQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/client-campaign-add-docs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /client-campaign-add-docs/:id : get the "id" clientCampaignAddDoc.
     *
     * @param id the id of the clientCampaignAddDocDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the clientCampaignAddDocDTO, or with status 404 (Not Found)
     */
    @GetMapping("/client-campaign-add-docs/{id}")
    @Timed
    public ResponseEntity<ClientCampaignAddDocDTO> getClientCampaignAddDoc(@PathVariable Long id) {
        log.debug("REST request to get ClientCampaignAddDoc : {}", id);
        ClientCampaignAddDocDTO clientCampaignAddDocDTO = clientCampaignAddDocService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(clientCampaignAddDocDTO));
    }

    /**
     * DELETE  /client-campaign-add-docs/:id : delete the "id" clientCampaignAddDoc.
     *
     * @param id the id of the clientCampaignAddDocDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/client-campaign-add-docs/{id}")
    @Timed
    public ResponseEntity<Void> deleteClientCampaignAddDoc(@PathVariable Long id) {
        log.debug("REST request to delete ClientCampaignAddDoc : {}", id);
        clientCampaignAddDocService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
