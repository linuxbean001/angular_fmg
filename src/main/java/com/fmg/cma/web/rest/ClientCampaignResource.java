package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.domain.Report;
import com.fmg.cma.service.ClientCampaignQueryService;
import com.fmg.cma.service.ClientCampaignService;
import com.fmg.cma.service.dto.ClientCampaignCriteria;
import com.fmg.cma.service.dto.ClientCampaignDTO;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import com.fmg.cma.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ClientCampaign.
 */
@RestController
@RequestMapping("/api")
public class ClientCampaignResource {

    private final Logger log = LoggerFactory.getLogger(ClientCampaignResource.class);

    public static final String ENTITY_NAME = "clientCampaign";

    private final ClientCampaignService clientCampaignService;

    private final ClientCampaignQueryService clientCampaignQueryService;

    public ClientCampaignResource(ClientCampaignService clientCampaignService, ClientCampaignQueryService clientCampaignQueryService) {
        this.clientCampaignService = clientCampaignService;
        this.clientCampaignQueryService = clientCampaignQueryService;
    }

    /**
     * POST  /client-campaigns : Create a new clientCampaign.
     *
     * @param clientCampaignDTO the clientCampaignDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new clientCampaignDTO, or with status 400 (Bad Request) if the clientCampaign has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/client-campaigns")
    @Timed
    public ResponseEntity<ClientCampaignDTO> createClientCampaign(@Valid @RequestBody ClientCampaignDTO clientCampaignDTO) throws URISyntaxException {
        log.debug("REST request to save ClientCampaign : {}", clientCampaignDTO);
        if (clientCampaignDTO.getId() != null) {
            throw new BadRequestAlertException("A new clientCampaign cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClientCampaignDTO result = clientCampaignService.save(clientCampaignDTO);
        return ResponseEntity.created(new URI("/api/client-campaigns/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PostMapping("/client-campaigns-wizard")
    @Timed
    public ResponseEntity<ClientCampaignDTO> createClientCampaignByWizard(
        //@RequestParam("campaign") ClientCampaignDTO clientCampaignDTO
        @RequestPart("campaign") ClientCampaignDTO clientCampaignDTO
    ) throws URISyntaxException {
        log.debug("REST request to save ClientCampaign : {}", clientCampaignDTO);
//        if (clientCampaignDTO.getId() != null) {
//            throw new BadRequestAlertException("A new clientCampaign cannot already have an ID", ENTITY_NAME, "idexists");
//        }
        ClientCampaignDTO result = clientCampaignService.saveByWizard(clientCampaignDTO);
        return ResponseEntity.created(new URI("/api/client-campaigns/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /client-campaigns : Updates an existing clientCampaign.
     *
     * @param clientCampaignDTO the clientCampaignDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated clientCampaignDTO,
     * or with status 400 (Bad Request) if the clientCampaignDTO is not valid,
     * or with status 500 (Internal Server Error) if the clientCampaignDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/client-campaigns")
    @Timed
    public ResponseEntity<ClientCampaignDTO> updateClientCampaign(@Valid @RequestBody ClientCampaignDTO clientCampaignDTO) throws URISyntaxException {
        log.debug("REST request to update ClientCampaign : {}", clientCampaignDTO);
        if (clientCampaignDTO.getId() == null) {
            return createClientCampaign(clientCampaignDTO);
        }
        log.info("JON12 111 GOIING TO SAVE ClientCampaign : {} with portal users {}", clientCampaignDTO.getId(), clientCampaignDTO.getPortalUsers());

        ClientCampaignDTO result = clientCampaignService.save(clientCampaignDTO);

        log.debug("JON12 1111 UPDATED TO NOW ClientCampaign : {} with portal users {}", result.getId(), result.getPortalUsers());


        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, clientCampaignDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /client-campaigns : get all the clientCampaigns.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of clientCampaigns in body
     */
    @GetMapping("/client-campaigns")
    @Timed
    public ResponseEntity<List<ClientCampaignDTO>> getAllClientCampaigns(ClientCampaignCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ClientCampaigns by criteria: {}", criteria);
        Page<ClientCampaignDTO> page = clientCampaignQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/client-campaigns");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /client-campaigns/:id : get the "id" clientCampaign.
     *
     * @param id the id of the clientCampaignDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the clientCampaignDTO, or with status 404 (Not Found)
     */
    @GetMapping("/client-campaigns/{id}")
    @Timed
    public ResponseEntity<ClientCampaignDTO> getClientCampaign(@PathVariable Long id) {
        log.debug("REST request to get ClientCampaign : {}", id);
        log.debug("CAJUN request to get ClientCampaign : {}", id);
        ClientCampaignDTO clientCampaignDTO = clientCampaignService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(clientCampaignDTO));
    }

    /**
     * DELETE  /client-campaigns/:id : delete the "id" clientCampaign.
     *
     * @param id the id of the clientCampaignDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/client-campaigns/{id}")
    @Timed
    public ResponseEntity<Void> deleteClientCampaign(@PathVariable Long id) {
        log.debug("REST request to delete ClientCampaign : {}", id);
        clientCampaignService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @PutMapping("/client-campaigns/{id}/add-client-list/{clientListId}")
    @Timed
    public void addClientList(@PathVariable long id, @PathVariable long clientListId) {
        clientCampaignService.addClientList(id, clientListId);
    }

    @PutMapping("/client-campaigns/{id}/add-email-template/{emailTemplateId}")
    @Timed
    public void addEmailTemplate(@PathVariable long id, @PathVariable long emailTemplateId) {
        clientCampaignService.addEmailTemplate(id, emailTemplateId);
    }

    @PutMapping("/client-campaigns/{id}/add-outcome/{outcomeId}")
    @Timed
    public void addOutcome(@PathVariable long id, @PathVariable long outcomeId) {
        clientCampaignService.addOutcome(id, outcomeId);
    }

    @PutMapping("/client-campaigns/{id}/add-script/{scriptId}")
    @Timed
    public void addScript(@PathVariable long id, @PathVariable long scriptId) {
        clientCampaignService.addScript(id, scriptId);
    }

    @PutMapping("/client-campaigns/{id}/add-sms/{smsId}")
    @Timed
    public void addSms(@PathVariable long id, @PathVariable long smsId) {
        clientCampaignService.addSms(id, smsId);
    }

    @PutMapping("/client-campaigns/{id}/add-portal-user/{portalUserId}")
    @Timed
    public void addPortalUser(@PathVariable long id, @PathVariable long portalUserId) {
        clientCampaignService.addPortalUser(id, portalUserId);
    }

    @PutMapping("/client-campaigns/{id}/appointment-attendee/{appointmentAttendeeId}")
    @Timed
    public void addAppointmentAttendee(@PathVariable long id, @PathVariable long appointmentAttendeeId) {
        clientCampaignService.addAppointmentAttendee(id, appointmentAttendeeId);
    }

    @PutMapping("/client-campaigns/{id}/add-survey-question/{surveyQuestionId}")
    @Timed
    public void addSurveyQuestion(@PathVariable long id, @PathVariable long surveyQuestionId) {
        clientCampaignService.addSurveyQuestion(id, surveyQuestionId);
    }

    @PutMapping("/client-campaigns/{id}/add-booking-question/{bookingQuestionId}")
    @Timed
    public void addBookingQuestion(@PathVariable long id, @PathVariable long bookingQuestionId) {
        clientCampaignService.addBookingQuestion(id, bookingQuestionId);
    }

    @GetMapping("/client-campaigns/nameIsUnique")
    @Timed
    public boolean nameIsUnique(@RequestParam String name, @RequestParam(required = false) Long id) {
        return clientCampaignService.nameIsUnique(name, id);
    }

    @GetMapping("/client-campaigns/console/related-client-campaigns")
    @Timed
    public List<ClientCampaignDTO> getConsoleRelatedClientCampaigns() {
        return clientCampaignService.getConsoleRelatedClientCampaigns();
    }

    /**
     * GET  /client-campaigns/report/:id/:idList : get the "id" clientCampaign.
     *
     * @param id the id of the clientCampaignDTO to retrieve
     * @param idList the id of the clientList in clientCampaignDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the clientCampaignDTO, or with status 404 (Not Found)
     */
    @GetMapping("/client-campaigns/report/{id}/{idList}")
    @Timed
    public ResponseEntity<Report> getReportByCampaign(@PathVariable Long id, @PathVariable Long idList) {
        log.debug("REST request to get ClientCampaign : Campaign {} ClientList {} ", id, idList);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(clientCampaignService.getReport(id, idList)));
    }

    /**
     * GET  /client-campaigns/:id : get the "id" clientCampaign.
     *
     * @param id the id of the clientCampaignDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the clientCampaignDTO, or with status 404 (Not Found)
     */
    @GetMapping("/client-campaigns/report/{id}")
    @Timed
    public ResponseEntity<Report> getReportByCampaign(@PathVariable Long id) {
        log.debug("REST request to get ClientCampaign : Campaign {} ", id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(clientCampaignService.getReport(id)));
    }
}
