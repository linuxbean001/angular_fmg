package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.service.ClientCampaignScriptService;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import com.fmg.cma.web.rest.util.PaginationUtil;
import com.fmg.cma.service.dto.ClientCampaignScriptDTO;
import com.fmg.cma.service.dto.ClientCampaignScriptCriteria;
import com.fmg.cma.service.ClientCampaignScriptQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ClientCampaignScript.
 */
@RestController
@RequestMapping("/api")
public class ClientCampaignScriptResource {

    private final Logger log = LoggerFactory.getLogger(ClientCampaignScriptResource.class);

    private static final String ENTITY_NAME = "clientCampaignScript";

    private final ClientCampaignScriptService clientCampaignScriptService;

    private final ClientCampaignScriptQueryService clientCampaignScriptQueryService;

    public ClientCampaignScriptResource(ClientCampaignScriptService clientCampaignScriptService, ClientCampaignScriptQueryService clientCampaignScriptQueryService) {
        this.clientCampaignScriptService = clientCampaignScriptService;
        this.clientCampaignScriptQueryService = clientCampaignScriptQueryService;
    }

    /**
     * POST  /client-campaign-scripts : Create a new clientCampaignScript.
     *
     * @param clientCampaignScriptDTO the clientCampaignScriptDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new clientCampaignScriptDTO, or with status 400 (Bad Request) if the clientCampaignScript has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/client-campaign-scripts")
    @Timed
    public ResponseEntity<ClientCampaignScriptDTO> createClientCampaignScript(@Valid @RequestBody ClientCampaignScriptDTO clientCampaignScriptDTO) throws URISyntaxException {
        log.debug("REST request to save ClientCampaignScript : {}", clientCampaignScriptDTO);
        if (clientCampaignScriptDTO.getId() != null) {
            throw new BadRequestAlertException("A new clientCampaignScript cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClientCampaignScriptDTO result = clientCampaignScriptService.save(clientCampaignScriptDTO);
        return ResponseEntity.created(new URI("/api/client-campaign-scripts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /client-campaign-scripts : Updates an existing clientCampaignScript.
     *
     * @param clientCampaignScriptDTO the clientCampaignScriptDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated clientCampaignScriptDTO,
     * or with status 400 (Bad Request) if the clientCampaignScriptDTO is not valid,
     * or with status 500 (Internal Server Error) if the clientCampaignScriptDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/client-campaign-scripts")
    @Timed
    public ResponseEntity<ClientCampaignScriptDTO> updateClientCampaignScript(@Valid @RequestBody ClientCampaignScriptDTO clientCampaignScriptDTO) throws URISyntaxException {
        log.debug("REST request to update ClientCampaignScript : {}", clientCampaignScriptDTO);
        if (clientCampaignScriptDTO.getId() == null) {
            return createClientCampaignScript(clientCampaignScriptDTO);
        }
        ClientCampaignScriptDTO result = clientCampaignScriptService.save(clientCampaignScriptDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, clientCampaignScriptDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /client-campaign-scripts : get all the clientCampaignScripts.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of clientCampaignScripts in body
     */
    @GetMapping("/client-campaign-scripts")
    @Timed
    public ResponseEntity<List<ClientCampaignScriptDTO>> getAllClientCampaignScripts(ClientCampaignScriptCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ClientCampaignScripts by criteria: {}", criteria);
        Page<ClientCampaignScriptDTO> page = clientCampaignScriptQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/client-campaign-scripts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /client-campaign-scripts/:id : get the "id" clientCampaignScript.
     *
     * @param id the id of the clientCampaignScriptDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the clientCampaignScriptDTO, or with status 404 (Not Found)
     */
    @GetMapping("/client-campaign-scripts/{id}")
    @Timed
    public ResponseEntity<ClientCampaignScriptDTO> getClientCampaignScript(@PathVariable Long id) {
        log.debug("REST request to get ClientCampaignScript : {}", id);
        ClientCampaignScriptDTO clientCampaignScriptDTO = clientCampaignScriptService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(clientCampaignScriptDTO));
    }

    /**
     * DELETE  /client-campaign-scripts/:id : delete the "id" clientCampaignScript.
     *
     * @param id the id of the clientCampaignScriptDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/client-campaign-scripts/{id}")
    @Timed
    public ResponseEntity<Void> deleteClientCampaignScript(@PathVariable Long id) {
        log.debug("REST request to delete ClientCampaignScript : {}", id);
        clientCampaignScriptService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
