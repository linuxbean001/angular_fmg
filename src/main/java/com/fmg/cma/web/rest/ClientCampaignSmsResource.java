package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.service.ClientCampaignSmsQueryService;
import com.fmg.cma.service.ClientCampaignSmsService;
import com.fmg.cma.service.dto.ClientCampaignSmsCriteria;
import com.fmg.cma.service.dto.ClientCampaignSmsDTO;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import com.fmg.cma.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ClientCampaignSms.
 */
@RestController
@RequestMapping("/api")
public class ClientCampaignSmsResource {

    private final Logger log = LoggerFactory.getLogger(ClientCampaignSmsResource.class);

    private static final String ENTITY_NAME = "clientCampaignSms";

    private final ClientCampaignSmsService clientCampaignSmsService;

    private final ClientCampaignSmsQueryService clientCampaignSmsQueryService;

    public ClientCampaignSmsResource(ClientCampaignSmsService clientCampaignSmsService, ClientCampaignSmsQueryService clientCampaignSmsQueryService) {
        this.clientCampaignSmsService = clientCampaignSmsService;
        this.clientCampaignSmsQueryService = clientCampaignSmsQueryService;
    }

    /**
     * POST  /client-campaign-sms : Create a new clientCampaignSms.
     *
     * @param clientCampaignSmsDTO the clientCampaignSmsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new clientCampaignSmsDTO, or with status 400 (Bad Request) if the clientCampaignSms has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/client-campaign-sms")
    @Timed
    public ResponseEntity<ClientCampaignSmsDTO> createClientCampaignSms(@Valid @RequestBody ClientCampaignSmsDTO clientCampaignSmsDTO) throws URISyntaxException {
        log.debug("REST request to save ClientCampaignSms : {}", clientCampaignSmsDTO);
        if (clientCampaignSmsDTO.getId() != null) {
            throw new BadRequestAlertException("A new clientCampaignSms cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClientCampaignSmsDTO result = clientCampaignSmsService.save(clientCampaignSmsDTO);
        return ResponseEntity.created(new URI("/api/client-campaign-sms/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /client-campaign-sms : Updates an existing clientCampaignSms.
     *
     * @param clientCampaignSmsDTO the clientCampaignSmsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated clientCampaignSmsDTO,
     * or with status 400 (Bad Request) if the clientCampaignSmsDTO is not valid,
     * or with status 500 (Internal Server Error) if the clientCampaignSmsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/client-campaign-sms")
    @Timed
    public ResponseEntity<ClientCampaignSmsDTO> updateClientCampaignSms(@Valid @RequestBody ClientCampaignSmsDTO clientCampaignSmsDTO) throws URISyntaxException {
        log.debug("REST request to update ClientCampaignSms : {}", clientCampaignSmsDTO);
        if (clientCampaignSmsDTO.getId() == null) {
            return createClientCampaignSms(clientCampaignSmsDTO);
        }
        ClientCampaignSmsDTO result = clientCampaignSmsService.save(clientCampaignSmsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, clientCampaignSmsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /client-campaign-sms : get all the clientCampaignSms.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of clientCampaignSms in body
     */
    @GetMapping("/client-campaign-sms")
    @Timed
    public ResponseEntity<List<ClientCampaignSmsDTO>> getAllClientCampaignSms(ClientCampaignSmsCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ClientCampaignSms by criteria: {}", criteria);
        Page<ClientCampaignSmsDTO> page = clientCampaignSmsQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/client-campaign-sms");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /client-campaign-sms/:id : get the "id" clientCampaignSms.
     *
     * @param id the id of the clientCampaignSmsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the clientCampaignSmsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/client-campaign-sms/{id}")
    @Timed
    public ResponseEntity<ClientCampaignSmsDTO> getClientCampaignSms(@PathVariable Long id) {
        log.debug("REST request to get ClientCampaignSms : {}", id);
        ClientCampaignSmsDTO clientCampaignSmsDTO = clientCampaignSmsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(clientCampaignSmsDTO));
    }

    /**
     * DELETE  /client-campaign-sms/:id : delete the "id" clientCampaignSms.
     *
     * @param id the id of the clientCampaignSmsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/client-campaign-sms/{id}")
    @Timed
    public ResponseEntity<Void> deleteClientCampaignSms(@PathVariable Long id) {
        log.debug("REST request to delete ClientCampaignSms : {}", id);
        clientCampaignSmsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
