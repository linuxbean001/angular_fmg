package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.service.ClientCampaignTemplateService;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import com.fmg.cma.web.rest.util.PaginationUtil;
import com.fmg.cma.service.dto.ClientCampaignTemplateDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ClientCampaignTemplate.
 */
@RestController
@RequestMapping("/api")
public class ClientCampaignTemplateResource {

    private final Logger log = LoggerFactory.getLogger(ClientCampaignTemplateResource.class);

    private static final String ENTITY_NAME = "clientCampaignTemplate";

    private final ClientCampaignTemplateService clientCampaignTemplateService;

    public ClientCampaignTemplateResource(ClientCampaignTemplateService clientCampaignTemplateService) {
        this.clientCampaignTemplateService = clientCampaignTemplateService;
    }

    /**
     * POST  /client-campaign-templates : Create a new clientCampaignTemplate.
     *
     * @param clientCampaignTemplateDTO the clientCampaignTemplateDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new clientCampaignTemplateDTO, or with status 400 (Bad Request) if the clientCampaignTemplate has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/client-campaign-templates")
    @Timed
    public ResponseEntity<ClientCampaignTemplateDTO> createClientCampaignTemplate(@Valid @RequestBody ClientCampaignTemplateDTO clientCampaignTemplateDTO) throws URISyntaxException {
        log.debug("REST request to save ClientCampaignTemplate : {}", clientCampaignTemplateDTO);
        if (clientCampaignTemplateDTO.getId() != null) {
            throw new BadRequestAlertException("A new clientCampaignTemplate cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClientCampaignTemplateDTO result = clientCampaignTemplateService.save(clientCampaignTemplateDTO);
        return ResponseEntity.created(new URI("/api/client-campaign-templates/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /client-campaign-templates : Updates an existing clientCampaignTemplate.
     *
     * @param clientCampaignTemplateDTO the clientCampaignTemplateDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated clientCampaignTemplateDTO,
     * or with status 400 (Bad Request) if the clientCampaignTemplateDTO is not valid,
     * or with status 500 (Internal Server Error) if the clientCampaignTemplateDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/client-campaign-templates")
    @Timed
    public ResponseEntity<ClientCampaignTemplateDTO> updateClientCampaignTemplate(@Valid @RequestBody ClientCampaignTemplateDTO clientCampaignTemplateDTO) throws URISyntaxException {
        log.debug("REST request to update ClientCampaignTemplate : {}", clientCampaignTemplateDTO);
        if (clientCampaignTemplateDTO.getId() == null) {
            return createClientCampaignTemplate(clientCampaignTemplateDTO);
        }
        ClientCampaignTemplateDTO result = clientCampaignTemplateService.save(clientCampaignTemplateDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, clientCampaignTemplateDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /client-campaign-templates : get all the clientCampaignTemplates.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of clientCampaignTemplates in body
     */
    @GetMapping("/client-campaign-templates")
    @Timed
    public ResponseEntity<List<ClientCampaignTemplateDTO>> getAllClientCampaignTemplates(Pageable pageable) {
        log.debug("REST request to get a page of ClientCampaignTemplates");
        Page<ClientCampaignTemplateDTO> page = clientCampaignTemplateService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/client-campaign-templates");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /client-campaign-templates/:id : get the "id" clientCampaignTemplate.
     *
     * @param id the id of the clientCampaignTemplateDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the clientCampaignTemplateDTO, or with status 404 (Not Found)
     */
    @GetMapping("/client-campaign-templates/{id}")
    @Timed
    public ResponseEntity<ClientCampaignTemplateDTO> getClientCampaignTemplate(@PathVariable Long id) {
        log.debug("REST request to get ClientCampaignTemplate : {}", id);
        ClientCampaignTemplateDTO clientCampaignTemplateDTO = clientCampaignTemplateService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(clientCampaignTemplateDTO));
    }

    /**
     * DELETE  /client-campaign-templates/:id : delete the "id" clientCampaignTemplate.
     *
     * @param id the id of the clientCampaignTemplateDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/client-campaign-templates/{id}")
    @Timed
    public ResponseEntity<Void> deleteClientCampaignTemplate(@PathVariable Long id) {
        log.debug("REST request to delete ClientCampaignTemplate : {}", id);
        clientCampaignTemplateService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
