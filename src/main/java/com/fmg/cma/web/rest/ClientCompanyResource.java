package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.service.ClientCompanyQueryService;
import com.fmg.cma.service.ClientCompanyService;
import com.fmg.cma.service.dto.ClientCompanyCriteria;
import com.fmg.cma.service.dto.ClientCompanyDTO;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import com.fmg.cma.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ClientCompany.
 */
@RestController
@RequestMapping("/api")
public class ClientCompanyResource {

    private final Logger log = LoggerFactory.getLogger(ClientCompanyResource.class);

    public static final String ENTITY_NAME = "clientCompany";

    private final ClientCompanyService clientCompanyService;

    private final ClientCompanyQueryService clientCompanyQueryService;

    public ClientCompanyResource(ClientCompanyService clientCompanyService, ClientCompanyQueryService clientCompanyQueryService) {
        this.clientCompanyService = clientCompanyService;
        this.clientCompanyQueryService = clientCompanyQueryService;
    }

    /**
     * POST  /client-companies : Create a new clientCompany.
     *
     * @param clientCompanyDTO the clientCompanyDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new clientCompanyDTO, or with status 400 (Bad Request) if the clientCompany has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/client-companies")
    @Timed
    public ResponseEntity<ClientCompanyDTO> createClientCompany(@Valid @RequestBody ClientCompanyDTO clientCompanyDTO) throws URISyntaxException {
        log.debug("REST request to save ClientCompany : {}", clientCompanyDTO);
        if (clientCompanyDTO.getId() != null) {
            throw new BadRequestAlertException("A new clientCompany cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClientCompanyDTO result = clientCompanyService.save(clientCompanyDTO);
        return ResponseEntity.created(new URI("/api/client-companies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /client-companies : Updates an existing clientCompany.
     *
     * @param clientCompanyDTO the clientCompanyDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated clientCompanyDTO,
     * or with status 400 (Bad Request) if the clientCompanyDTO is not valid,
     * or with status 500 (Internal Server Error) if the clientCompanyDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/client-companies")
    @Timed
    public ResponseEntity<ClientCompanyDTO> updateClientCompany(@Valid @RequestBody ClientCompanyDTO clientCompanyDTO) throws URISyntaxException {
        log.debug("REST request to update ClientCompany : {}", clientCompanyDTO);
        if (clientCompanyDTO.getId() == null) {
            return createClientCompany(clientCompanyDTO);
        }
        ClientCompanyDTO result = clientCompanyService.save(clientCompanyDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, clientCompanyDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /client-companies : get all the clientCompanies.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of clientCompanies in body
     */
    @GetMapping("/client-companies")
    @Timed
    public ResponseEntity<List<ClientCompanyDTO>> getAllClientCompanies(ClientCompanyCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ClientCompanies by criteria: {}", criteria);
        Page<ClientCompanyDTO> page = clientCompanyQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/client-companies");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /client-companies/:id : get the "id" clientCompany.
     *
     * @param id the id of the clientCompanyDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the clientCompanyDTO, or with status 404 (Not Found)
     */
    @GetMapping("/client-companies/{id}")
    @Timed
    public ResponseEntity<ClientCompanyDTO> getClientCompany(@PathVariable Long id) {
        log.debug("REST request to get ClientCompany : {}", id);
        ClientCompanyDTO clientCompanyDTO = clientCompanyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(clientCompanyDTO));
    }

    /**
     * DELETE  /client-companies/:id : delete the "id" clientCompany.
     *
     * @param id the id of the clientCompanyDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/client-companies/{id}")
    @Timed
    public ResponseEntity<Void> deleteClientCompany(@PathVariable Long id) {
        log.debug("REST request to delete ClientCompany : {}", id);
        clientCompanyService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @PutMapping("/client-companies-update-industry/{id}")
    public void changeIndustry(@PathVariable Long id, @RequestParam(required = false) Long industryId) {
        clientCompanyService.changeIndustry(id, industryId);
    }
}
