package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.service.ClientCompanyStatusService;
import com.fmg.cma.service.dto.ClientCompanyStatusDTO;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import com.fmg.cma.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ClientCompanyStatus.
 */
@RestController
@RequestMapping("/api")
public class ClientCompanyStatusResource {

    private final Logger log = LoggerFactory.getLogger(ClientCompanyStatusResource.class);

    private static final String ENTITY_NAME = "clientCompanyStatus";

    private final ClientCompanyStatusService clientCompanyStatusService;

    public ClientCompanyStatusResource(ClientCompanyStatusService clientCompanyStatusService) {
        this.clientCompanyStatusService = clientCompanyStatusService;
    }

    /**
     * POST  /client-company-statuses : Create a new clientCompanyStatus.
     *
     * @param clientCompanyStatusDTO the clientCompanyStatusDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new clientCompanyStatusDTO, or with status 400 (Bad Request) if the clientCompanyStatus has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/client-company-statuses")
    @Timed
    public ResponseEntity<ClientCompanyStatusDTO> createClientCompanyStatus(@Valid @RequestBody ClientCompanyStatusDTO clientCompanyStatusDTO) throws URISyntaxException {
        log.debug("REST request to save ClientCompanyStatus : {}", clientCompanyStatusDTO);
        if (clientCompanyStatusDTO.getId() != null) {
            throw new BadRequestAlertException("A new clientCompanyStatus cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClientCompanyStatusDTO result = clientCompanyStatusService.save(clientCompanyStatusDTO);
        return ResponseEntity.created(new URI("/api/client-company-statuses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /client-company-statuses : Updates an existing clientCompanyStatus.
     *
     * @param clientCompanyStatusDTO the clientCompanyStatusDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated clientCompanyStatusDTO,
     * or with status 400 (Bad Request) if the clientCompanyStatusDTO is not valid,
     * or with status 500 (Internal Server Error) if the clientCompanyStatusDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/client-company-statuses")
    @Timed
    public ResponseEntity<ClientCompanyStatusDTO> updateClientCompanyStatus(@Valid @RequestBody ClientCompanyStatusDTO clientCompanyStatusDTO) throws URISyntaxException {
        log.debug("REST request to update ClientCompanyStatus : {}", clientCompanyStatusDTO);
        if (clientCompanyStatusDTO.getId() == null) {
            return createClientCompanyStatus(clientCompanyStatusDTO);
        }
        ClientCompanyStatusDTO result = clientCompanyStatusService.save(clientCompanyStatusDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, clientCompanyStatusDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /client-company-statuses : get all the clientCompanyStatuses.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of clientCompanyStatuses in body
     */
    @GetMapping("/client-company-statuses")
    @Timed
    public ResponseEntity<List<ClientCompanyStatusDTO>> getAllClientCompanyStatuses(Pageable pageable) {
        log.debug("REST request to get a page of ClientCompanyStatuses");
        Page<ClientCompanyStatusDTO> page = clientCompanyStatusService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/client-company-statuses");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /client-company-statuses/:id : get the "id" clientCompanyStatus.
     *
     * @param id the id of the clientCompanyStatusDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the clientCompanyStatusDTO, or with status 404 (Not Found)
     */
    @GetMapping("/client-company-statuses/{id}")
    @Timed
    public ResponseEntity<ClientCompanyStatusDTO> getClientCompanyStatus(@PathVariable Long id) {
        log.debug("REST request to get ClientCompanyStatus : {}", id);
        ClientCompanyStatusDTO clientCompanyStatusDTO = clientCompanyStatusService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(clientCompanyStatusDTO));
    }

    /**
     * DELETE  /client-company-statuses/:id : delete the "id" clientCompanyStatus.
     *
     * @param id the id of the clientCompanyStatusDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/client-company-statuses/{id}")
    @Timed
    public ResponseEntity<Void> deleteClientCompanyStatus(@PathVariable Long id) {
        log.debug("REST request to delete ClientCompanyStatus : {}", id);
        clientCompanyStatusService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
