package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.service.ClientFieldService;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import com.fmg.cma.web.rest.util.PaginationUtil;
import com.fmg.cma.service.dto.ClientFieldDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ClientField.
 */
@RestController
@RequestMapping("/api")
public class ClientFieldResource {

    private final Logger log = LoggerFactory.getLogger(ClientFieldResource.class);

    private static final String ENTITY_NAME = "clientField";

    private final ClientFieldService clientFieldService;

    public ClientFieldResource(ClientFieldService clientFieldService) {
        this.clientFieldService = clientFieldService;
    }

    /**
     * POST  /client-fields : Create a new clientField.
     *
     * @param clientFieldDTO the clientFieldDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new clientFieldDTO, or with status 400 (Bad Request) if the clientField has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/client-fields")
    @Timed
    public ResponseEntity<ClientFieldDTO> createClientField(@RequestBody ClientFieldDTO clientFieldDTO) throws URISyntaxException {
        log.debug("REST request to save ClientField : {}", clientFieldDTO);
        if (clientFieldDTO.getId() != null) {
            throw new BadRequestAlertException("A new clientField cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClientFieldDTO result = clientFieldService.save(clientFieldDTO);
        return ResponseEntity.created(new URI("/api/client-fields/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /client-fields : Updates an existing clientField.
     *
     * @param clientFieldDTO the clientFieldDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated clientFieldDTO,
     * or with status 400 (Bad Request) if the clientFieldDTO is not valid,
     * or with status 500 (Internal Server Error) if the clientFieldDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/client-fields")
    @Timed
    public ResponseEntity<ClientFieldDTO> updateClientField(@RequestBody ClientFieldDTO clientFieldDTO) throws URISyntaxException {
        log.debug("REST request to update ClientField : {}", clientFieldDTO);
        if (clientFieldDTO.getId() == null) {
            return createClientField(clientFieldDTO);
        }
        ClientFieldDTO result = clientFieldService.save(clientFieldDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, clientFieldDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /client-fields : get all the clientFields.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of clientFields in body
     */
    @GetMapping("/client-fields")
    @Timed
    public ResponseEntity<List<ClientFieldDTO>> getAllClientFields(Pageable pageable) {
        log.debug("REST request to get a page of ClientFields");
        Page<ClientFieldDTO> page = clientFieldService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/client-fields");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /client-fields/:id : get the "id" clientField.
     *
     * @param id the id of the clientFieldDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the clientFieldDTO, or with status 404 (Not Found)
     */
    @GetMapping("/client-fields/{id}")
    @Timed
    public ResponseEntity<ClientFieldDTO> getClientField(@PathVariable Long id) {
        log.debug("REST request to get ClientField : {}", id);
        ClientFieldDTO clientFieldDTO = clientFieldService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(clientFieldDTO));
    }

    /**
     * DELETE  /client-fields/:id : delete the "id" clientField.
     *
     * @param id the id of the clientFieldDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/client-fields/{id}")
    @Timed
    public ResponseEntity<Void> deleteClientField(@PathVariable Long id) {
        log.debug("REST request to delete ClientField : {}", id);
        clientFieldService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
