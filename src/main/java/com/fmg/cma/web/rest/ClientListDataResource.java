package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.service.ClientListDataService;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import com.fmg.cma.web.rest.util.PaginationUtil;
import com.fmg.cma.service.dto.ClientListDataDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ClientListData.
 */
@RestController
@RequestMapping("/api")
public class ClientListDataResource {

    private final Logger log = LoggerFactory.getLogger(ClientListDataResource.class);

    private static final String ENTITY_NAME = "clientListData";

    private final ClientListDataService clientListDataService;

    public ClientListDataResource(ClientListDataService clientListDataService) {
        this.clientListDataService = clientListDataService;
    }

    /**
     * POST  /client-list-data : Create a new clientListData.
     *
     * @param clientListDataDTO the clientListDataDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new clientListDataDTO, or with status 400 (Bad Request) if the clientListData has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/client-list-data")
    @Timed
    public ResponseEntity<ClientListDataDTO> createClientListData(@RequestBody ClientListDataDTO clientListDataDTO) throws URISyntaxException {
        log.debug("REST request to save ClientListData : {}", clientListDataDTO);
        if (clientListDataDTO.getId() != null) {
            throw new BadRequestAlertException("A new clientListData cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClientListDataDTO result = clientListDataService.save(clientListDataDTO);
        return ResponseEntity.created(new URI("/api/client-list-data/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /client-list-data : Updates an existing clientListData.
     *
     * @param clientListDataDTO the clientListDataDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated clientListDataDTO,
     * or with status 400 (Bad Request) if the clientListDataDTO is not valid,
     * or with status 500 (Internal Server Error) if the clientListDataDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/client-list-data")
    @Timed
    public ResponseEntity<ClientListDataDTO> updateClientListData(@RequestBody ClientListDataDTO clientListDataDTO) throws URISyntaxException {
        log.debug("REST request to update ClientListData : {}", clientListDataDTO);
        if (clientListDataDTO.getId() == null) {
            return createClientListData(clientListDataDTO);
        }
        ClientListDataDTO result = clientListDataService.save(clientListDataDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, clientListDataDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /client-list-data : get all the clientListData.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of clientListData in body
     */
    @GetMapping("/client-list-data")
    @Timed
    public ResponseEntity<List<ClientListDataDTO>> getAllClientListData(Pageable pageable) {
        log.debug("REST request to get a page of ClientListData");
        Page<ClientListDataDTO> page = clientListDataService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/client-list-data");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /client-list-data/:id : get the "id" clientListData.
     *
     * @param id the id of the clientListDataDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the clientListDataDTO, or with status 404 (Not Found)
     */
    @GetMapping("/client-list-data/{id}")
    @Timed
    public ResponseEntity<ClientListDataDTO> getClientListData(@PathVariable Long id) {
        log.debug("REST request to get ClientListData : {}", id);
        ClientListDataDTO clientListDataDTO = clientListDataService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(clientListDataDTO));
    }

    /**
     * DELETE  /client-list-data/:id : delete the "id" clientListData.
     *
     * @param id the id of the clientListDataDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/client-list-data/{id}")
    @Timed
    public ResponseEntity<Void> deleteClientListData(@PathVariable Long id) {
        log.debug("REST request to delete ClientListData : {}", id);
        clientListDataService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
