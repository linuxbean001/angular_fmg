package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.domain.ClientField;
import com.fmg.cma.domain.TransactionReport;
import com.fmg.cma.service.ClientListGridService;
import com.fmg.cma.service.dto.CallHistoryDTO;
import com.fmg.cma.service.dto.ClientFieldDTO;
import com.fmg.cma.service.dto.ClientListDataDTO;
import com.fmg.cma.service.dto.ClientListRowDto;
import com.fmg.cma.service.dto.DataListCompany;
import com.fmg.cma.service.dto.DataListContact;
import com.fmg.cma.service.dto.DataListSummary;
import com.fmg.cma.service.mapper.ClientFieldMapper;
import com.fmg.cma.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ClientList.
 */
@RestController
@RequestMapping("/api")
public class ClientListGridResource {

    private final Logger log = LoggerFactory.getLogger(ClientListGridResource.class);

    private static final String ENTITY_NAME = "clientList";

    private final ClientListGridService clientListGridService;

    private final ClientFieldMapper clientFieldMapper;

    public ClientListGridResource(ClientListGridService clientListGridService,
                                  ClientFieldMapper clientFieldMapper) {
        this.clientListGridService = clientListGridService;
        this.clientFieldMapper = clientFieldMapper;
    }

    @GetMapping("/client-lists/{id}/fields")
    @Timed
    public List<ClientFieldDTO> getFields(@PathVariable long id) {
        List<ClientField> fields = clientListGridService.getFields(id);
        return clientFieldMapper.toDto(fields);
    }

    @GetMapping(value = "/client-lists/{id}/rows")
    @Timed
    public ResponseEntity<List<ClientListRowDto>> getRows(@PathVariable long id, Pageable pageable) {
        Page<ClientListRowDto> page = clientListGridService.getRows(id, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/client-lists/" + id + "/rows");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @DeleteMapping(value = "/client-lists/{id}/rows/{rowNum}")
    @Timed
    public void deleteRow(@PathVariable long id, @PathVariable long rowNum) {
        clientListGridService.deleteRow(id, rowNum);
    }

    @PutMapping(value = "/client-list-data-by-coord")
    @Timed
    public void update(@RequestBody ClientListDataDTO clientListDataDTO) {
        clientListGridService.update(clientListDataDTO);
    }

    @GetMapping("/client-lists/{id}/summary")
    @Timed
    public DataListSummary getSummary(@PathVariable long id) {
        return clientListGridService.getSummary(id);
    }

    // @PostMapping("/client-lists/{id}/company-contacts")
    // @Timed
    // public List<DataListContact> getCompanyContacts(@PathVariable long id,
    //                                                 @RequestBody DataListCompanyPk dataListCompanyPk) {
    //     return clientListGridService.getCompanyContacts(id, dataListCompanyPk);
    // }

    @PostMapping("/client-lists/{id}/save-company-contact")
    @Timed
    public DataListContact saveCompanyContact(@PathVariable long id,
                                              @RequestPart("company") DataListCompany company,
                                              @RequestPart("contact") DataListContact contact) {
        // why I return row num? the better way is return whole contact
        return clientListGridService.saveCompanyContact(id, company, contact);
    }

    @PostMapping("/client-lists/{id}/save-company")
    @Timed
    public DataListCompany saveCompany(@PathVariable long id,
                                       @RequestBody DataListCompany company) {
        return clientListGridService.saveCompany(id, company);
    }

    @PostMapping("/client-lists/{id}/save-company-note")
    @Timed
    public void saveCompanyNote(@PathVariable long id,
                                @RequestPart("rowNum") Long rowNum,
                                @RequestPart("note") String note) {
        clientListGridService.saveCompanyNote(id, rowNum, note);
    }

    @GetMapping("/client-lists/{id}/row-nums/{orgRowNum}/histories")
    @Timed
    public List<CallHistoryDTO> saveCompanyNote(@PathVariable long id, @PathVariable long orgRowNum) {
        return clientListGridService.getCallHistories(id, orgRowNum);
    }

    @PostMapping("/client-lists/{listId}/move-contact/{contactRowNum}/to/{newListId}")
    @Timed
    public void moveContactToAnotherList(@PathVariable long listId,
                                         @PathVariable long contactRowNum,
                                         @PathVariable long newListId) {
        clientListGridService.moveContactToAnotherList(listId, contactRowNum, newListId);
    }

    @GetMapping("/transaction-report/")
    @Timed
    public ResponseEntity<List<TransactionReport>> getTransactionReport() {
        List<TransactionReport> reports = clientListGridService.getTransactionReport();
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(reports));
    }
}
