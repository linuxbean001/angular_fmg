package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.service.ClientListService;
import com.fmg.cma.service.dto.ClientListDTO;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import com.fmg.cma.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ClientList.
 */
@RestController
@RequestMapping("/api")
public class ClientListResource {

    private final Logger log = LoggerFactory.getLogger(ClientListResource.class);

    private static final String ENTITY_NAME = "clientList";

    private final ClientListService clientListService;

    public ClientListResource(ClientListService clientListService) {
        this.clientListService = clientListService;
    }

    /**
     * POST  /client-lists : Create a new clientList.
     *
     * @param clientListDTO the clientListDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new clientListDTO, or with status 400 (Bad Request) if the clientList has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/client-lists")
    @Timed
    public ResponseEntity<ClientListDTO> createClientList(
        //@Valid @RequestBody ClientListDTO clientListDTO,
        @Valid @RequestPart("clientList") ClientListDTO clientListDTO,
        @RequestPart("file") MultipartFile file) throws URISyntaxException {
        log.debug("REST request to save ClientList : {}", clientListDTO);
        if (clientListDTO.getId() != null) {
            throw new BadRequestAlertException("A new clientList cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClientListDTO result = clientListService.save(clientListDTO, file);
        return ResponseEntity.created(new URI("/api/client-lists/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /client-lists : Updates an existing clientList.
     *
     * @param clientListDTO the clientListDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated clientListDTO,
     * or with status 400 (Bad Request) if the clientListDTO is not valid,
     * or with status 500 (Internal Server Error) if the clientListDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/client-lists")
    @Timed
    public ResponseEntity<ClientListDTO> updateClientList(
        //@Valid @RequestBody ClientListDTO clientListDTO
        @Valid @RequestPart("clientList") ClientListDTO clientListDTO,
        @RequestPart(value = "file", required = false) MultipartFile file) throws URISyntaxException {
        log.debug("REST request to update ClientList : {}", clientListDTO);
        if (clientListDTO.getId() == null) {
            return createClientList(clientListDTO, file);
        }
        ClientListDTO result = clientListService.save(clientListDTO, file);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, clientListDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /client-lists : get all the clientLists.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of clientLists in body
     */
    @GetMapping("/client-lists")
    @Timed
    public ResponseEntity<List<ClientListDTO>> getAllClientLists(Pageable pageable) {
        log.debug("REST request to get a page of ClientLists");
        Page<ClientListDTO> page = clientListService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/client-lists");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /client-lists/:id : get the "id" clientList.
     *
     * @param id the id of the clientListDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the clientListDTO, or with status 404 (Not Found)
     */
    @GetMapping("/client-lists/{id}")
    @Timed
    public ResponseEntity<ClientListDTO> getClientList(@PathVariable Long id) {
        log.debug("REST request to get ClientList : {}", id);
        ClientListDTO clientListDTO = clientListService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(clientListDTO));
    }

    /**
     * DELETE  /client-lists/:id : delete the "id" clientList.
     *
     * @param id the id of the clientListDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/client-lists/{id}")
    @Timed
    public ResponseEntity<Void> deleteClientList(@PathVariable Long id) {
        log.debug("REST request to delete ClientList : {}", id);
        clientListService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/client-campaigns/{clientCampaignId}/client-lists")
    @Timed
    public ResponseEntity<List<ClientListDTO>> getClientCampaignClientLists(@PathVariable long clientCampaignId, Pageable pageable) {
        log.debug("REST request to get a page of ClientLists");
        Page<ClientListDTO> page = clientListService.getClientCampaignClientLists(clientCampaignId, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/client-lists");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
