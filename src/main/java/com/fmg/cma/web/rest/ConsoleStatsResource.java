package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.domain.ConsoleStats;
import com.fmg.cma.service.ConsoleStatsQueryService;
import com.fmg.cma.service.ConsoleStatsService;
import com.fmg.cma.service.dto.ConsoleStatsCriteria;
import com.fmg.cma.service.dto.ConsoleStatsDTO;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import com.fmg.cma.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ConsoleStats.
 */
@RestController
@RequestMapping("/api")
public class ConsoleStatsResource {

    private final Logger log = LoggerFactory.getLogger(ConsoleStatsResource.class);

    private static final String ENTITY_NAME = "consoleStats";

    private final ConsoleStatsService consoleStatsService;

    private final ConsoleStatsQueryService consoleStatsQueryService;

    public ConsoleStatsResource(ConsoleStatsService consoleStatsService, ConsoleStatsQueryService consoleStatsQueryService) {
        this.consoleStatsService = consoleStatsService;
        this.consoleStatsQueryService = consoleStatsQueryService;
    }

    /**
     * POST  /console-stats : Create a new consoleStats.
     *
     * @param consoleStatsDTO the consoleStatsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new consoleStatsDTO, or with status 400 (Bad Request) if the consoleStats has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/console-stats")
    @Timed
    public ResponseEntity<ConsoleStatsDTO> createConsoleStats(@RequestBody ConsoleStatsDTO consoleStatsDTO) throws URISyntaxException {
        log.debug("REST request to save ConsoleStats : {}", consoleStatsDTO);
        if (consoleStatsDTO.getId() != null) {
            throw new BadRequestAlertException("A new consoleStats cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ConsoleStatsDTO result = consoleStatsService.save(consoleStatsDTO);
        return ResponseEntity.created(new URI("/api/console-stats/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /console-stats : Updates an existing consoleStats.
     *
     * @param consoleStatsDTO the consoleStatsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated consoleStatsDTO,
     * or with status 400 (Bad Request) if the consoleStatsDTO is not valid,
     * or with status 500 (Internal Server Error) if the consoleStatsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/console-stats")
    @Timed
    public ResponseEntity<ConsoleStatsDTO> updateConsoleStats(@RequestBody ConsoleStatsDTO consoleStatsDTO) throws URISyntaxException {
        log.debug("REST request to update ConsoleStats : {}", consoleStatsDTO);
        if (consoleStatsDTO.getId() == null) {
            return createConsoleStats(consoleStatsDTO);
        }
        ConsoleStatsDTO result = consoleStatsService.save(consoleStatsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, consoleStatsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /console-stats : get all the consoleStats.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of consoleStats in body
     */
    @GetMapping("/console-stats")
    @Timed
    public ResponseEntity<List<ConsoleStatsDTO>> getAllConsoleStats(ConsoleStatsCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ConsoleStats by criteria: {}", criteria);
        Page<ConsoleStatsDTO> page = consoleStatsQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/console-stats");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /console-stats/:id : get the "id" consoleStats.
     *
     * @param id the id of the consoleStatsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the consoleStatsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/console-stats/{id}")
    @Timed
    public ResponseEntity<ConsoleStatsDTO> getConsoleStats(@PathVariable Long id) {
        log.debug("REST request to get ConsoleStats : {}", id);
        ConsoleStatsDTO consoleStatsDTO = consoleStatsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(consoleStatsDTO));
    }

    /**
     * DELETE  /console-stats/:id : delete the "id" consoleStats.
     *
     * @param id the id of the consoleStatsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/console-stats/{id}")
    @Timed
    public ResponseEntity<Void> deleteConsoleStats(@PathVariable Long id) {
        log.debug("REST request to delete ConsoleStats : {}", id);
        consoleStatsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/console-stats/current-date")
    @Timed
    public ConsoleStatsDTO getByUserIsCurrentUserAndStampIsCurrentDate() {
        return consoleStatsService.getByUserIsCurrentUserAndStampIsCurrentDate();
    }

    @PostMapping("/console-stats/incrementSkips")
    @Timed
    public ConsoleStats incrementSkips() {
        return consoleStatsService.incrementSkips();
    }

    @PostMapping("/console-stats/incrementCalls")
    @Timed
    public ConsoleStats incrementCalls() {
        return consoleStatsService.incrementCalls();
    }

    @PostMapping("/console-stats/incrementPipelineOpps")
    @Timed
    public ConsoleStats incrementPipelineOpps() {
        return consoleStatsService.incrementPipelineOpps();
    }

    @PostMapping("/console-stats/incrementDMContact")
    @Timed
    public ConsoleStats incrementDMContact() {
        return consoleStatsService.incrementDMContact();
    }
}
