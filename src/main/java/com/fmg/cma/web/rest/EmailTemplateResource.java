package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.service.EmailTemplateQueryService;
import com.fmg.cma.service.EmailTemplateService;
import com.fmg.cma.service.dto.EmailTemplateCriteria;
import com.fmg.cma.service.dto.EmailTemplateDTO;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import com.fmg.cma.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing EmailTemplate.
 */
@RestController
@RequestMapping("/api")
public class EmailTemplateResource {

    private final Logger log = LoggerFactory.getLogger(EmailTemplateResource.class);

    private static final String ENTITY_NAME = "emailTemplate";

    private final EmailTemplateService emailTemplateService;

    private final EmailTemplateQueryService emailTemplateQueryService;

    public EmailTemplateResource(EmailTemplateService emailTemplateService, EmailTemplateQueryService emailTemplateQueryService) {
        this.emailTemplateService = emailTemplateService;
        this.emailTemplateQueryService = emailTemplateQueryService;
    }

    /**
     * POST  /email-templates : Create a new emailTemplate.
     *
     * @param emailTemplateDTO the emailTemplateDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new emailTemplateDTO, or with status 400 (Bad Request) if the emailTemplate has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/email-templates")
    @Timed
    public ResponseEntity<EmailTemplateDTO> createEmailTemplate(@Valid @RequestBody EmailTemplateDTO emailTemplateDTO) throws URISyntaxException {
        log.debug("REST request to save EmailTemplate : {}", emailTemplateDTO);
        if (emailTemplateDTO.getId() != null) {
            throw new BadRequestAlertException("A new emailTemplate cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EmailTemplateDTO result = emailTemplateService.save(emailTemplateDTO);
        return ResponseEntity.created(new URI("/api/email-templates/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /email-templates : Updates an existing emailTemplate.
     *
     * @param emailTemplateDTO the emailTemplateDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated emailTemplateDTO,
     * or with status 400 (Bad Request) if the emailTemplateDTO is not valid,
     * or with status 500 (Internal Server Error) if the emailTemplateDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/email-templates")
    @Timed
    public ResponseEntity<EmailTemplateDTO> updateEmailTemplate(@Valid @RequestBody EmailTemplateDTO emailTemplateDTO) throws URISyntaxException {
        log.debug("REST request to update EmailTemplate : {}", emailTemplateDTO);
        if (emailTemplateDTO.getId() == null) {
            return createEmailTemplate(emailTemplateDTO);
        }
        EmailTemplateDTO result = emailTemplateService.save(emailTemplateDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, emailTemplateDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /email-templates : get all the emailTemplates.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of emailTemplates in body
     */
    @GetMapping("/email-templates")
    @Timed
    public ResponseEntity<List<EmailTemplateDTO>> getAllEmailTemplates(EmailTemplateCriteria criteria, Pageable pageable) {
        log.debug("REST request to get EmailTemplates by criteria: {}", criteria);
        Page<EmailTemplateDTO> page = emailTemplateQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/email-templates");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /email-templates/:id : get the "id" emailTemplate.
     *
     * @param id the id of the emailTemplateDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the emailTemplateDTO, or with status 404 (Not Found)
     */
    @GetMapping("/email-templates/{id}")
    @Timed
    public ResponseEntity<EmailTemplateDTO> getEmailTemplate(@PathVariable Long id) {
        log.debug("REST request to get EmailTemplate : {}", id);
        EmailTemplateDTO emailTemplateDTO = emailTemplateService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(emailTemplateDTO));
    }

    /**
     * DELETE  /email-templates/:id : delete the "id" emailTemplate.
     *
     * @param id the id of the emailTemplateDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/email-templates/{id}")
    @Timed
    public ResponseEntity<Void> deleteEmailTemplate(@PathVariable Long id) {
        log.debug("REST request to delete EmailTemplate : {}", id);
        emailTemplateService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
