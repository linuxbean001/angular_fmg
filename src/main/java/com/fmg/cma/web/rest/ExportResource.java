package com.fmg.cma.web.rest;

import com.fmg.cma.service.ExportService;
import net.sf.jasperreports.engine.JRException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/export")
public class ExportResource {

    private ExportService export;

    public ExportResource(ExportService export) {
        this.export = export;
    }

    @GetMapping("/pdf")
    public ResponseEntity<byte[]> pdf() throws JRException {
        byte[] bytes = export.exportPdf();
        String filename = "pdf_file2export";
        return ResponseEntity.ok()
            .header("Content-Type", "application/pdf; charset=UTF-8")
            .header("Content-Disposition", "inline; filename=\"" + filename + ".pdf\"")
            .body(bytes);
    }

    @GetMapping("/csv")
    public ResponseEntity<byte[]> csv() throws JRException {
        byte[] bytes = export.exportCsv();
        String filename = "csv_file2export";
        return ResponseEntity.ok()
            .header("Content-Type", "text/csv; charset=UTF-8")
            .header("Content-Disposition", "inline; filename=\"" + filename + ".csv\"")
            .body(bytes);
    }

    @GetMapping("/pdf/email")
    public ResponseEntity<byte[]> pdfEmail() throws JRException {
        byte[] bytes = export.exportPdf();
        String filename = "email_pdf_file2export";
        return ResponseEntity.ok()
            .header("Content-Type", "application/pdf; charset=UTF-8")
            .header("Content-Disposition", "inline; filename=\"" + filename + ".pdf\"")
            .body(bytes);
    }

    @GetMapping("/csv/email")
    public ResponseEntity<byte[]> csvEmail() throws JRException {
        byte[] bytes = export.exportCsv();
        String filename = "email_csv_file2export";
        return ResponseEntity.ok()
            .header("Content-Type", "text/csv; charset=UTF-8")
            .header("Content-Disposition", "inline; filename=\"" + filename + ".csv\"")
            .body(bytes);
    }

}
