package com.fmg.cma.web.rest;

public enum Kind {
    ALL_EXCEPT_PORTAL_USER(0),
    CONTAINS_PORTAL_USER(1);

    private final int number;

    Kind(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public static Kind forValue(Integer value) {
        if (value == null) {
            return null;
        }

        for (Kind item : Kind.values()) {
            if (item.getNumber() == value) {
                return item;
            }
        }

        throw new IllegalArgumentException(
            String.format("Unable to cast \"" + value + "\" to %s ", Kind.class.getName())
        );
    }
}
