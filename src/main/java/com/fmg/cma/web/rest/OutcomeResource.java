package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.service.OutcomeQueryService;
import com.fmg.cma.service.OutcomeService;
import com.fmg.cma.service.dto.OutcomeCriteria;
import com.fmg.cma.service.dto.OutcomeDTO;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import com.fmg.cma.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing Outcome.
 */
@RestController
@RequestMapping("/api")
public class OutcomeResource {

    private final Logger log = LoggerFactory.getLogger(OutcomeResource.class);

    private static final String ENTITY_NAME = "outcome";

    private final OutcomeService outcomeService;

    private final OutcomeQueryService outcomeQueryService;

    public OutcomeResource(OutcomeService outcomeService, OutcomeQueryService outcomeQueryService) {
        this.outcomeService = outcomeService;
        this.outcomeQueryService = outcomeQueryService;
    }

    /**
     * POST  /outcomes : Create a new outcome.
     *
     * @param outcomeDTO the outcomeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new outcomeDTO, or with status 400 (Bad Request) if the outcome has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/outcomes")
    @Timed
    public ResponseEntity<OutcomeDTO> createOutcome(@Valid @RequestBody OutcomeDTO outcomeDTO) throws URISyntaxException {
        log.debug("REST request to save Outcome : {}", outcomeDTO);
        if (outcomeDTO.getId() != null) {
            throw new BadRequestAlertException("A new outcome cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OutcomeDTO result = outcomeService.save(outcomeDTO);
        return ResponseEntity.created(new URI("/api/outcomes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PostMapping("/outcomes-multiple")
    @Timed
    public List<OutcomeDTO> createOutcomes(@Valid @RequestBody List<OutcomeDTO> outcomeDTOs) {
        log.debug("REST request to save Outcomes : {}", outcomeDTOs);

        return outcomeDTOs.stream()
            .map(outcomeDTO -> {
                OutcomeDTO resultOutcomeDTO = outcomeService.save(outcomeDTO);
                // FIXME this is a workaround, API return object without names
                resultOutcomeDTO.setTypeName(outcomeDTO.getTypeName());
                resultOutcomeDTO.setTemplateName(outcomeDTO.getTemplateName());
                return resultOutcomeDTO;
            })
            .collect(Collectors.toList());
    }

    /**
     * PUT  /outcomes : Updates an existing outcome.
     *
     * @param outcomeDTO the outcomeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated outcomeDTO,
     * or with status 400 (Bad Request) if the outcomeDTO is not valid,
     * or with status 500 (Internal Server Error) if the outcomeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/outcomes")
    @Timed
    public ResponseEntity<OutcomeDTO> updateOutcome(@Valid @RequestBody OutcomeDTO outcomeDTO) throws URISyntaxException {
        log.debug("REST request to update Outcome : {}", outcomeDTO);
        if (outcomeDTO.getId() == null) {
            return createOutcome(outcomeDTO);
        }
        OutcomeDTO result = outcomeService.save(outcomeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, outcomeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /outcomes : get all the outcomes.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of outcomes in body
     */
    @GetMapping("/outcomes")
    @Timed
    public ResponseEntity<List<OutcomeDTO>> getAllOutcomes(OutcomeCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Outcomes by criteria: {}", criteria);
        Page<OutcomeDTO> page = outcomeQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/outcomes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /outcomes/:id : get the "id" outcome.
     *
     * @param id the id of the outcomeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the outcomeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/outcomes/{id}")
    @Timed
    public ResponseEntity<OutcomeDTO> getOutcome(@PathVariable Long id) {
        log.debug("REST request to get Outcome : {}", id);
        OutcomeDTO outcomeDTO = outcomeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(outcomeDTO));
    }

    /**
     * DELETE  /outcomes/:id : delete the "id" outcome.
     *
     * @param id the id of the outcomeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/outcomes/{id}")
    @Timed
    public ResponseEntity<Void> deleteOutcome(@PathVariable Long id) {
        log.debug("REST request to delete Outcome : {}", id);
        outcomeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
