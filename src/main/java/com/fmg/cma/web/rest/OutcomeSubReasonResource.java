package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.service.OutcomeSubReasonQueryService;
import com.fmg.cma.service.OutcomeSubReasonService;
import com.fmg.cma.service.dto.OutcomeSubReasonCriteria;
import com.fmg.cma.service.dto.OutcomeSubReasonDTO;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import com.fmg.cma.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing OutcomeSubReason.
 */
@RestController
@RequestMapping("/api")
public class OutcomeSubReasonResource {

    private final Logger log = LoggerFactory.getLogger(OutcomeSubReasonResource.class);

    private static final String ENTITY_NAME = "outcomeSubReason";

    private final OutcomeSubReasonService outcomeSubReasonService;

    private final OutcomeSubReasonQueryService outcomeSubReasonQueryService;

    public OutcomeSubReasonResource(OutcomeSubReasonService outcomeSubReasonService, OutcomeSubReasonQueryService outcomeSubReasonQueryService) {
        this.outcomeSubReasonService = outcomeSubReasonService;
        this.outcomeSubReasonQueryService = outcomeSubReasonQueryService;
    }

    /**
     * POST  /outcome-sub-reasons : Create a new outcomeSubReason.
     *
     * @param outcomeSubReasonDTO the outcomeSubReasonDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new outcomeSubReasonDTO, or with status 400 (Bad Request) if the outcomeSubReason has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/outcome-sub-reasons")
    @Timed
    public ResponseEntity<OutcomeSubReasonDTO> createOutcomeSubReason(@RequestBody OutcomeSubReasonDTO outcomeSubReasonDTO) throws URISyntaxException {
        log.debug("REST request to save OutcomeSubReason : {}", outcomeSubReasonDTO);
        if (outcomeSubReasonDTO.getId() != null) {
            throw new BadRequestAlertException("A new outcomeSubReason cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OutcomeSubReasonDTO result = outcomeSubReasonService.save(outcomeSubReasonDTO);
        return ResponseEntity.created(new URI("/api/outcome-sub-reasons/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /outcome-sub-reasons : Updates an existing outcomeSubReason.
     *
     * @param outcomeSubReasonDTO the outcomeSubReasonDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated outcomeSubReasonDTO,
     * or with status 400 (Bad Request) if the outcomeSubReasonDTO is not valid,
     * or with status 500 (Internal Server Error) if the outcomeSubReasonDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/outcome-sub-reasons")
    @Timed
    public ResponseEntity<OutcomeSubReasonDTO> updateOutcomeSubReason(@RequestBody OutcomeSubReasonDTO outcomeSubReasonDTO) throws URISyntaxException {
        log.debug("REST request to update OutcomeSubReason : {}", outcomeSubReasonDTO);
        if (outcomeSubReasonDTO.getId() == null) {
            return createOutcomeSubReason(outcomeSubReasonDTO);
        }
        OutcomeSubReasonDTO result = outcomeSubReasonService.save(outcomeSubReasonDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, outcomeSubReasonDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /outcome-sub-reasons : get all the outcomeSubReasons.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of outcomeSubReasons in body
     */
    @GetMapping("/outcome-sub-reasons")
    @Timed
    public ResponseEntity<List<OutcomeSubReasonDTO>> getAllOutcomeSubReasons(OutcomeSubReasonCriteria criteria, Pageable pageable) {
        log.debug("REST request to get OutcomeSubReasons by criteria: {}", criteria);
        Page<OutcomeSubReasonDTO> page = outcomeSubReasonQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/outcome-sub-reasons");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /outcome-sub-reasons/:id : get the "id" outcomeSubReason.
     *
     * @param id the id of the outcomeSubReasonDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the outcomeSubReasonDTO, or with status 404 (Not Found)
     */
    @GetMapping("/outcome-sub-reasons/{id}")
    @Timed
    public ResponseEntity<OutcomeSubReasonDTO> getOutcomeSubReason(@PathVariable Long id) {
        log.debug("REST request to get OutcomeSubReason : {}", id);
        OutcomeSubReasonDTO outcomeSubReasonDTO = outcomeSubReasonService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(outcomeSubReasonDTO));
    }

    /**
     * DELETE  /outcome-sub-reasons/:id : delete the "id" outcomeSubReason.
     *
     * @param id the id of the outcomeSubReasonDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/outcome-sub-reasons/{id}")
    @Timed
    public ResponseEntity<Void> deleteOutcomeSubReason(@PathVariable Long id) {
        log.debug("REST request to delete OutcomeSubReason : {}", id);
        outcomeSubReasonService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
