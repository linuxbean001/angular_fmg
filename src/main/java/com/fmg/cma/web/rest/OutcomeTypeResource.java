package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.service.OutcomeTypeQueryService;
import com.fmg.cma.service.OutcomeTypeService;
import com.fmg.cma.service.dto.OutcomeTypeCriteria;
import com.fmg.cma.service.dto.OutcomeTypeDTO;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import com.fmg.cma.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing OutcomeType.
 */
@RestController
@RequestMapping("/api")
public class OutcomeTypeResource {

    private final Logger log = LoggerFactory.getLogger(OutcomeTypeResource.class);

    private static final String ENTITY_NAME = "outcomeType";

    private final OutcomeTypeService outcomeTypeService;

    private final OutcomeTypeQueryService outcomeTypeQueryService;

    public OutcomeTypeResource(OutcomeTypeService outcomeTypeService, OutcomeTypeQueryService outcomeTypeQueryService) {
        this.outcomeTypeService = outcomeTypeService;
        this.outcomeTypeQueryService = outcomeTypeQueryService;
    }

    /**
     * POST  /outcome-types : Create a new outcomeType.
     *
     * @param outcomeTypeDTO the outcomeTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new outcomeTypeDTO, or with status 400 (Bad Request) if the outcomeType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/outcome-types")
    @Timed
    public ResponseEntity<OutcomeTypeDTO> createOutcomeType(@Valid @RequestBody OutcomeTypeDTO outcomeTypeDTO) throws URISyntaxException {
        log.debug("REST request to save OutcomeType : {}", outcomeTypeDTO);
        if (outcomeTypeDTO.getId() != null) {
            throw new BadRequestAlertException("A new outcomeType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OutcomeTypeDTO result = outcomeTypeService.save(outcomeTypeDTO);
        return ResponseEntity.created(new URI("/api/outcome-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /outcome-types : Updates an existing outcomeType.
     *
     * @param outcomeTypeDTO the outcomeTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated outcomeTypeDTO,
     * or with status 400 (Bad Request) if the outcomeTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the outcomeTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/outcome-types")
    @Timed
    public ResponseEntity<OutcomeTypeDTO> updateOutcomeType(@Valid @RequestBody OutcomeTypeDTO outcomeTypeDTO) throws URISyntaxException {
        log.debug("REST request to update OutcomeType : {}", outcomeTypeDTO);
        if (outcomeTypeDTO.getId() == null) {
            return createOutcomeType(outcomeTypeDTO);
        }
        OutcomeTypeDTO result = outcomeTypeService.save(outcomeTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, outcomeTypeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /outcome-types : get all the outcomeTypes.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of outcomeTypes in body
     */
    @GetMapping("/outcome-types")
    @Timed
    public ResponseEntity<List<OutcomeTypeDTO>> getAllOutcomeTypes(OutcomeTypeCriteria criteria, Pageable pageable) {
        log.debug("REST request to get OutcomeTypes by criteria: {}", criteria);
        Page<OutcomeTypeDTO> page = outcomeTypeQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/outcome-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /outcome-types/:id : get the "id" outcomeType.
     *
     * @param id the id of the outcomeTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the outcomeTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/outcome-types/{id}")
    @Timed
    public ResponseEntity<OutcomeTypeDTO> getOutcomeType(@PathVariable Long id) {
        log.debug("REST request to get OutcomeType : {}", id);
        OutcomeTypeDTO outcomeTypeDTO = outcomeTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(outcomeTypeDTO));
    }

    /**
     * DELETE  /outcome-types/:id : delete the "id" outcomeType.
     *
     * @param id the id of the outcomeTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/outcome-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteOutcomeType(@PathVariable Long id) {
        log.debug("REST request to delete OutcomeType : {}", id);
        outcomeTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
