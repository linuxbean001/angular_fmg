package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.service.SurveyQuestionFieldQueryService;
import com.fmg.cma.service.SurveyQuestionFieldService;
import com.fmg.cma.service.dto.SurveyQuestionFieldCriteria;
import com.fmg.cma.service.dto.SurveyQuestionFieldDTO;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import com.fmg.cma.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SurveyQuestionField.
 */
@RestController
@RequestMapping("/api")
public class SurveyQuestionFieldResource {

    private final Logger log = LoggerFactory.getLogger(SurveyQuestionFieldResource.class);

    private static final String ENTITY_NAME = "surveyQuestionField";

    private final SurveyQuestionFieldService surveyQuestionFieldService;

    private final SurveyQuestionFieldQueryService surveyQuestionFieldQueryService;

    public SurveyQuestionFieldResource(SurveyQuestionFieldService surveyQuestionFieldService, SurveyQuestionFieldQueryService surveyQuestionFieldQueryService) {
        this.surveyQuestionFieldService = surveyQuestionFieldService;
        this.surveyQuestionFieldQueryService = surveyQuestionFieldQueryService;
    }

    /**
     * POST  /survey-question-fields : Create a new surveyQuestionField.
     *
     * @param surveyQuestionFieldDTO the surveyQuestionFieldDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new surveyQuestionFieldDTO, or with status 400 (Bad Request) if the surveyQuestionField has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/survey-question-fields")
    @Timed
    public ResponseEntity<SurveyQuestionFieldDTO> createSurveyQuestionField(@RequestBody SurveyQuestionFieldDTO surveyQuestionFieldDTO) throws URISyntaxException {
        log.debug("REST request to save SurveyQuestionField : {}", surveyQuestionFieldDTO);
        if (surveyQuestionFieldDTO.getId() != null) {
            throw new BadRequestAlertException("A new surveyQuestionField cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SurveyQuestionFieldDTO result = surveyQuestionFieldService.save(surveyQuestionFieldDTO);
        return ResponseEntity.created(new URI("/api/survey-question-fields/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /survey-question-fields : Updates an existing surveyQuestionField.
     *
     * @param surveyQuestionFieldDTO the surveyQuestionFieldDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated surveyQuestionFieldDTO,
     * or with status 400 (Bad Request) if the surveyQuestionFieldDTO is not valid,
     * or with status 500 (Internal Server Error) if the surveyQuestionFieldDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/survey-question-fields")
    @Timed
    public ResponseEntity<SurveyQuestionFieldDTO> updateSurveyQuestionField(@RequestBody SurveyQuestionFieldDTO surveyQuestionFieldDTO) throws URISyntaxException {
        log.debug("REST request to update SurveyQuestionField : {}", surveyQuestionFieldDTO);
        if (surveyQuestionFieldDTO.getId() == null) {
            return createSurveyQuestionField(surveyQuestionFieldDTO);
        }
        SurveyQuestionFieldDTO result = surveyQuestionFieldService.save(surveyQuestionFieldDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, surveyQuestionFieldDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /survey-question-fields : get all the surveyQuestionFields.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of surveyQuestionFields in body
     */
    @GetMapping("/survey-question-fields")
    @Timed
    public ResponseEntity<List<SurveyQuestionFieldDTO>> getAllSurveyQuestionFields(SurveyQuestionFieldCriteria criteria, Pageable pageable) {
        log.debug("REST request to get SurveyQuestionFields by criteria: {}", criteria);
        Page<SurveyQuestionFieldDTO> page = surveyQuestionFieldQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/survey-question-fields");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /survey-question-fields/:id : get the "id" surveyQuestionField.
     *
     * @param id the id of the surveyQuestionFieldDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the surveyQuestionFieldDTO, or with status 404 (Not Found)
     */
    @GetMapping("/survey-question-fields/{id}")
    @Timed
    public ResponseEntity<SurveyQuestionFieldDTO> getSurveyQuestionField(@PathVariable Long id) {
        log.debug("REST request to get SurveyQuestionField : {}", id);
        SurveyQuestionFieldDTO surveyQuestionFieldDTO = surveyQuestionFieldService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(surveyQuestionFieldDTO));
    }

    /**
     * DELETE  /survey-question-fields/:id : delete the "id" surveyQuestionField.
     *
     * @param id the id of the surveyQuestionFieldDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/survey-question-fields/{id}")
    @Timed
    public ResponseEntity<Void> deleteSurveyQuestionField(@PathVariable Long id) {
        log.debug("REST request to delete SurveyQuestionField : {}", id);
        surveyQuestionFieldService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
