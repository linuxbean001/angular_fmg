package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fmg.cma.domain.UserCompany;
import com.fmg.cma.service.UserCompanyService;
import com.fmg.cma.service.dto.UserCompanyDTO;
import com.fmg.cma.service.mapper.UserCompanyMapper;
import com.fmg.cma.web.rest.errors.BadRequestAlertException;
import com.fmg.cma.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing UserCompany.
 */
@RestController
@RequestMapping("/api")
public class UserCompanyResource {

    private final Logger log = LoggerFactory.getLogger(UserCompanyResource.class);

    private static final String ENTITY_NAME = "userCompany";

    private final UserCompanyService userCompanyService;

    private final UserCompanyMapper userCompanyMapper;

    public UserCompanyResource(UserCompanyService userCompanyService, UserCompanyMapper userCompanyMapper) {
        this.userCompanyService = userCompanyService;
        this.userCompanyMapper = userCompanyMapper;
    }

    /**
     * POST  /user-companies : Create a new userCompany.
     *
     * @param userCompanyDTO the userCompanyDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new userCompanyDTO, or with status 400 (Bad Request) if the userCompany has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/user-companies")
    @Timed
    public ResponseEntity<UserCompanyDTO> createUserCompany(@Valid @RequestBody UserCompanyDTO userCompanyDTO) throws URISyntaxException {
        log.debug("REST request to save UserCompany : {}", userCompanyDTO);
        if (userCompanyDTO.getId() != null) {
            throw new BadRequestAlertException("A new userCompany cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserCompanyDTO result = userCompanyService.save(userCompanyDTO);
        return ResponseEntity.created(new URI("/api/user-companies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /user-companies : Updates an existing userCompany.
     *
     * @param userCompanyDTO the userCompanyDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated userCompanyDTO,
     * or with status 400 (Bad Request) if the userCompanyDTO is not valid,
     * or with status 500 (Internal Server Error) if the userCompanyDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/user-companies")
    @Timed
    public ResponseEntity<UserCompanyDTO> updateUserCompany(@Valid @RequestBody UserCompanyDTO userCompanyDTO) throws URISyntaxException {
        log.debug("REST request to update UserCompany : {}", userCompanyDTO);
        if (userCompanyDTO.getId() == null) {
            return createUserCompany(userCompanyDTO);
        }
        UserCompanyDTO result = userCompanyService.save(userCompanyDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, userCompanyDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /user-companies : get all the userCompanies.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of userCompanies in body
     */
    @GetMapping("/user-companies")
    @Timed
    public List<UserCompanyDTO> getAllUserCompanies() {
        log.debug("REST request to get all UserCompanies");
        return userCompanyService.findAll();
    }

    /**
     * GET  /user-companies/:id : get the "id" userCompany.
     *
     * @param id the id of the userCompanyDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userCompanyDTO, or with status 404 (Not Found)
     */
    @GetMapping("/user-companies/{id}")
    @Timed
    public ResponseEntity<UserCompanyDTO> getUserCompany(@PathVariable Long id) {
        log.debug("REST request to get UserCompany : {}", id);
        UserCompany userCompany = userCompanyService.findOne(id);
        UserCompanyDTO userCompanyDTO = userCompanyMapper.toDto(userCompany);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(userCompanyDTO));
    }

    /**
     * DELETE  /user-companies/:id : delete the "id" userCompany.
     *
     * @param id the id of the userCompanyDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/user-companies/{id}")
    @Timed
    public ResponseEntity<Void> deleteUserCompany(@PathVariable Long id) {
        log.debug("REST request to delete UserCompany : {}", id);
        userCompanyService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
