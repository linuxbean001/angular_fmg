package com.fmg.cma.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fmg.cma.security.jwt.JWTConfigurer;
import com.fmg.cma.security.jwt.TokenProvider;
import com.fmg.cma.web.rest.vm.LoginVM;
import com.fmg.cma.web.rest.vm.RefreshDto;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Date;

/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping("/api")
public class UserJWTController {

    private final TokenProvider tokenProvider;

    private final AuthenticationManager authenticationManager;

    public UserJWTController(TokenProvider tokenProvider, AuthenticationManager authenticationManager) {
        this.tokenProvider = tokenProvider;
        this.authenticationManager = authenticationManager;
    }

    @PostMapping("/authenticate")
    @Timed
    public ResponseEntity<JWTToken> authorize(@Valid @RequestBody LoginVM loginVM) {
        UsernamePasswordAuthenticationToken authenticationToken =
            new UsernamePasswordAuthenticationToken(loginVM.getUsername(), loginVM.getPassword());

        Authentication authentication = this.authenticationManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        return getJwtTokenResponseEntity(authentication, loginVM.isRememberMe());
    }

    @PostMapping("/refresh")
    @Timed
    public ResponseEntity<JWTToken> refresh(@RequestBody RefreshDto refreshDto) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        return getJwtTokenResponseEntity(authentication, refreshDto.isRememberMe());
    }

    @NotNull
    private ResponseEntity<JWTToken> getJwtTokenResponseEntity(Authentication authentication, Boolean rememberMe) {
        Date expiration = tokenProvider.getExpiration((rememberMe == null) ? false : rememberMe);
        String jwt = tokenProvider.createToken(authentication)
            .setExpiration(expiration)
            .compact();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTConfigurer.AUTHORIZATION_HEADER, "Bearer " + jwt);
        return new ResponseEntity<>(new JWTToken(jwt, expiration), httpHeaders, HttpStatus.OK);
    }

    /**
     * Object to return as body in JWT Authentication.
     */
    static class JWTToken {

        private String idToken;
        private Date expiration;

        JWTToken(String idToken, Date expiration) {
            this.idToken = idToken;
            this.expiration = expiration;
        }

        @JsonProperty("id_token")
        String getIdToken() {
            return idToken;
        }

        void setIdToken(String idToken) {
            this.idToken = idToken;
        }

        public Date getExpiration() {
            return expiration;
        }
    }
}
