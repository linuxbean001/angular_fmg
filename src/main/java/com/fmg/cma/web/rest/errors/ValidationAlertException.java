package com.fmg.cma.web.rest.errors;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.util.Map;

public class ValidationAlertException extends AbstractThrowableProblem {

    public ValidationAlertException(String defaultMessage, Map<String, Object> parameters) {
        super(ErrorConstants.CONSTRAINT_VIOLATION_TYPE, defaultMessage, Status.BAD_REQUEST, null, null, null, parameters);
    }
}
