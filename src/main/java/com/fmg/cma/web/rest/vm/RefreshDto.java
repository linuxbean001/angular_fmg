package com.fmg.cma.web.rest.vm;

public class RefreshDto {

    private Boolean rememberMe;

    public Boolean isRememberMe() {
        return rememberMe;
    }

    public void setRememberMe(Boolean rememberMe) {
        this.rememberMe = rememberMe;
    }
}
