/**
 * View Models used by Spring MVC REST controllers.
 */
package com.fmg.cma.web.rest.vm;
