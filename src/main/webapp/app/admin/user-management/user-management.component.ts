import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {JhiAlertService} from 'ng-jhipster';
import {TabClickEvent, Tabs} from '../../layouts/tabs/tabs.component';

@Component({
    selector: 'jhi-user-mgmt',
    templateUrl: './user-management.component.html'
})
export class UserMgmtComponent implements OnInit, OnDestroy {

    KIND: typeof KIND = KIND;

    @ViewChild(Tabs)
    tabs: Tabs;

    constructor(
        private alertService: JhiAlertService,
    ) {
    }

    ngOnInit() {
    }

    ngOnDestroy() {
    }

    private onError(error) {
        this.alertService.error(error.error, error.message, null);
    }

    onTabClick(event: TabClickEvent) {
        this.tabs.selectTab(event.to);
    }
}

export enum KIND {
    ALL_EXCEPT_PORTAL_USER,
    CONTAINS_PORTAL_USER
}
