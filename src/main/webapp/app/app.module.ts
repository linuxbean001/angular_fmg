import './vendor.ts';

import {Injector, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {LocalStorageService, Ng2Webstorage, SessionStorageService} from 'ngx-webstorage';
import {JhiEventManager} from 'ng-jhipster';

import {AuthInterceptor} from './blocks/interceptor/auth.interceptor';
import {AuthExpiredInterceptor} from './blocks/interceptor/auth-expired.interceptor';
import {ErrorHandlerInterceptor} from './blocks/interceptor/errorhandler.interceptor';
import {NotificationInterceptor} from './blocks/interceptor/notification.interceptor';
import {CmaSharedModule, UserRouteAccessService} from './shared';
import {CmaAppRoutingModule} from './app-routing.module';
import {CmaHomeModule} from './home';
import {CmaAdminModule} from './admin/admin.module';
import {CmaAccountModule} from './account/account.module';
import {CmaEntityModule} from './entities/entity.module';
import {PaginationConfig} from './blocks/config/uib-pagination.config';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import {
    ActiveMenuDirective,
    ErrorComponent,
    FooterComponent,
    JhiMainComponent,
    NavbarComponent,
    PageRibbonComponent,
    ProfileService
} from './layouts';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {CmaClientFeedbackModule} from './client-feedback/client-feedback.module';
import {CmaPersonalBestskModule} from './home/personal-bests/personal-bests.module';
import {CmaEmailSmsModule} from './home/email-sms/email-sms.module';
import {CmaTransactionReportModule} from './home/transaction-report/transaction-report.module';

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        CmaAppRoutingModule,
        Ng2Webstorage.forRoot({prefix: 'jhi', separator: '-'}),
        CmaSharedModule,
        CmaHomeModule,
        CmaAdminModule,
        CmaAccountModule,
        CmaEntityModule,
        CmaClientFeedbackModule,
        CmaPersonalBestskModule,
        CmaEmailSmsModule,
        CmaTransactionReportModule,
        NgbModule
        // jhipster-needle-angular-add-module JHipster will add new module here
    ],
    declarations: [
        JhiMainComponent,
        NavbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        ActiveMenuDirective,
        FooterComponent,
    ],
    providers: [
        ProfileService,
        PaginationConfig,
        UserRouteAccessService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true,
            deps: [
                LocalStorageService,
                SessionStorageService
            ]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthExpiredInterceptor,
            multi: true,
            deps: [
                Injector
            ]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorHandlerInterceptor,
            multi: true,
            deps: [
                JhiEventManager
            ]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: NotificationInterceptor,
            multi: true,
            deps: [
                Injector
            ]
        }
    ],
    bootstrap: [JhiMainComponent]
})
export class CmaAppModule {
}
