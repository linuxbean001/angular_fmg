import {Component} from '@angular/core';
import {ICellRendererAngularComp} from '@ag-grid-community/angular';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-buttons-client-feedback',
    template: `
        <div class="btn-group">
            <a class="btn btn-outline-info">Console</a>
            <a class="btn btn-outline-info">Survey</a>
        </div>`,
    styles: [
            `.btn {
            line-height: 0.5
        }`
    ]
})
export class ClientFeedbackButtonsComponent implements ICellRendererAngularComp {
    public params: any;
    modalRef: NgbModalRef;

    agInit(params: any): void {
        this.params = params;
    }

    refresh(): boolean {
        return false;
    }
}
