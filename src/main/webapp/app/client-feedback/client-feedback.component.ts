import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {Observable} from 'rxjs';
import {Columns, ColumnSearch, TypeColumn} from '../entities/my-pipeline/domain/ColumnSearch';
import {FormControl} from '@angular/forms';
import {FeedBack} from './domain/FeedBack';
import {NgbdSortableHeaderDirective, SortEvent} from '../entities/my-pipeline/table/directives/sortable.directive';
import {Searching} from '../entities/my-pipeline/table/domain/Searching';
import {TypeSearch, TypesSearch} from '../entities/my-pipeline/table/domain/TypeSearch';
import {ClientFeedbackService} from './client-feedback.service';
import {NgbDateStruct, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {GridOptions, Module} from '@ag-grid-community/core';
import {AllCommunityModules} from '@ag-grid-community/all-modules';
import {PipelineButtonsComponent} from '../entities/my-pipeline/pipeline/pipeline-buttons.component';
import * as moment from 'moment';
import {ClientFeedbackButtonsComponent} from './client-feedback-buttons.component';

@Component({
    selector: 'jhi-client-feedback',
    templateUrl: './client-feedback.component.html',
    styleUrls: ['client-feedback.component.css']
})
export class ClientFeedbackComponent implements OnInit {

    filter = new FormControl('');
    columns = Columns;
    modalRef: NgbModalRef;
    today: NgbDateStruct;

    gridOptions: GridOptions;
    private gridApi;

    public modules: Module[] = AllCommunityModules;

    columnDefs = [

        {headerName: 'Appointment Date', field: 'appointment', type: 'dateColumn'},
        {headerName: 'Campaign', field: 'campaign'},
        {headerName: 'Campaign Date', field: 'campaignDate', type: 'dateColumn'},
        {headerName: 'Company Name', field: 'companyName'},
        {headerName: 'Question 1', field: 'question1', filter: 'agNumberColumnFilter'},
        {headerName: 'Question 2', field: 'question2', filter: 'agNumberColumnFilter'},
        {headerName: 'Question 3', field: 'question3', filter: 'agNumberColumnFilter'},
        {headerName: 'Question 4', field: 'question4', filter: 'agNumberColumnFilter'},
        {headerName: 'Question 5', field: 'question5', filter: 'agNumberColumnFilter'},
        {headerName: 'View', field: '', cellRenderer: 'clientFeedbackButtonsComponent', suppressMenu: true}
    ];

    private frameworkComponents;

    @ViewChildren(NgbdSortableHeaderDirective) headers: QueryList<NgbdSortableHeaderDirective>;

    constructor(public service: ClientFeedbackService) {
        this.frameworkComponents = {
            clientFeedbackButtonsComponent: ClientFeedbackButtonsComponent
        };
        this.gridOptions = <GridOptions>{
            rowHeight: 35,
            enableRangeSelection: true,
            columnDefs: this.columnDefs,
            onGridReady: (params) => {
                service.countries$.subscribe(
                    (rowData) => {
                        if (this.gridOptions.api) { // can be null when tabbing between the examples
                            this.gridOptions.api.setRowData(rowData);
                        }
                    }
                );
                this.gridApi = params.api;
            },
            pagination: true,
            paginationAutoPageSize: true,
            defaultColDef: {
                sortable: true,
                filter: true,
                resizable: true
            },
            columnTypes: {
                'dateColumn': {
                    filter: 'agDateColumnFilter',
                    filterParams: {
                        comparator: (filterLocalDateAtMidnight, cellValue) => {
                            const dateParts = moment(cellValue).format('DD/MM/YYYY').split('/');
                            const day = Number(dateParts[0]);
                            const month = Number(dateParts[1]) - 1;
                            const year = Number(dateParts[2]);
                            const cellDate = new Date(year, month, day);
                            if (cellDate < filterLocalDateAtMidnight) {
                                return -1;
                            } else if (cellDate > filterLocalDateAtMidnight) {
                                return 1;
                            } else {
                                return 0;
                            }
                        }
                    },
                    valueFormatter: (params) => {
                        return moment(params.value).format('DD/MM/YYYY');
                    }
                }
            },
            frameworkComponents: this.frameworkComponents
        };
    }

    ngOnInit() {
    }

}
