import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {clientFeedbackRoute} from './client-feedback.route';
import {ClientFeedbackComponent} from './client-feedback.component';
import {ClientFeedbackService} from './client-feedback.service';
import {CmaPipelineModule} from '../entities/my-pipeline/pipeline.module';
import {NgbdSortableHeaderDirective} from '../entities/my-pipeline/table/directives/sortable.directive';
import {CmaHeaderConsoleModule} from '../entities/header-console/header-console.module';
import {AgGridModule} from '@ag-grid-community/angular';
import {ClientFeedbackButtonsComponent} from './client-feedback-buttons.component';

const ENTITY_STATES = [
    ...clientFeedbackRoute,
];

@NgModule({
    imports: [
        RouterModule.forChild(ENTITY_STATES),
        BrowserModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        CmaPipelineModule,
        CmaHeaderConsoleModule,
        AgGridModule.withComponents(
            [
                ClientFeedbackComponent,
                ClientFeedbackButtonsComponent
            ]
        )
    ],
    declarations: [
        ClientFeedbackComponent,
        ClientFeedbackButtonsComponent
    ],
    entryComponents: [
        ClientFeedbackComponent
    ],
    providers: [ClientFeedbackService, NgbdSortableHeaderDirective],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaClientFeedbackModule {
}
