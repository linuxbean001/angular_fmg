import {Routes} from '@angular/router';
import {UserRouteAccessService} from '../shared';
import {ClientFeedbackComponent} from './client-feedback.component';

export const clientFeedbackRoute: Routes = [
    {
        path: 'client-feedback',
        component: ClientFeedbackComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'client-feedback.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
