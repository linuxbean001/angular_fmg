import {Injectable} from '@angular/core';

import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {debounceTime, switchMap, tap} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {DatePipe} from '@angular/common';
import {FeedBack} from './domain/FeedBack';
import {SortDirection} from '../entities/my-pipeline/table/directives/sortable.directive';
import {Searching} from '../entities/my-pipeline/table/domain/Searching';
import {TypesSearch} from '../entities/my-pipeline/table/domain/TypeSearch';
import {CustomNgbDateParserFormatter} from '../utils/CustomNgbDateParserFormatter';
import {Columns, ColumnSearch, TypeColumn} from '../entities/my-pipeline/domain/ColumnSearch';
import {FEEDBACKS} from './domain/dummyData';

interface SearchResult {
    feedBackList: FeedBack[];
    total: number;
}

interface State {
    page: number;
    pageSize: number;
    searchTerm: Searching;
    sortColumn: string;
    sortDirection: SortDirection;
}

@Injectable()
export class ClientFeedbackService {

    private _loading$ = new BehaviorSubject<boolean>(true);
    private _search$ = new Subject<void>();
    private _countries$ = new BehaviorSubject<FeedBack[]>([]);
    private _total$ = new BehaviorSubject<number>(0);

    private _state: State = {
        page: 1,
        pageSize: 4,
        searchTerm: new Searching('', new Date(), new ColumnSearch('', TypeColumn.STRING, Columns.ALL), TypesSearch.SEARCH),
        sortColumn: '',
        sortDirection: ''
    };

    constructor(private datePipe: DatePipe, private customDatePipe: CustomNgbDateParserFormatter) {
        this._search$.pipe(
            tap(() => this._loading$.next(true)),
            debounceTime(200),
            switchMap(() => this._search()),
            tap(() => this._loading$.next(false))
        ).subscribe((result) => {
            this._countries$.next(result.feedBackList);
            this._total$.next(result.total);
        });

        this._search$.next();
    }

    get countries$() {
        return this._countries$;
    }

    get total$() {
        return this._total$.asObservable();
    }

    get loading$() {
        return this._loading$.asObservable();
    }

    get page() {
        return this._state.page;
    }

    get pageSize() {
        return this._state.pageSize;
    }

    get searchTerm() {
        return this._state.searchTerm;
    }

    set page(page: number) {
        this._set({page});
    }

    set pageSize(pageSize: number) {
        this._set({pageSize});
    }

    set searchTerm(searchTerm: Searching) {
        this._set({searchTerm});
    }

    set sortColumn(sortColumn: string) {
        this._set({sortColumn});
    }

    set sortDirection(sortDirection: SortDirection) {
        this._set({sortDirection});
    }

    private _set(patch: Partial<State>) {
        Object.assign(this._state, patch);
        this._search$.next();
    }

    private _search(): Observable<SearchResult> {
        const {sortColumn, sortDirection, pageSize, page, searchTerm} = this._state;

        // 1. sort
        const feedBackList = FEEDBACKS;

        const total = feedBackList.length;

        // 3. paginate
        return of({feedBackList, total});
    }

}
