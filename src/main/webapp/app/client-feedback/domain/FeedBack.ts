export class FeedBack {
    constructor(public appointmentDate: Date,
                public campaign: string,
                public campaignDate: Date,
                public companyName: string,
                public question1: string,
                public question2: string,
                public question3: string,
                public question4: string,
                public question5: string) {
    }
}
