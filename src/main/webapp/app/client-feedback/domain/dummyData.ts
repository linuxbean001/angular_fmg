import {FeedBack} from './FeedBack';

export const FEEDBACKS: FeedBack[] = [
    {
        appointmentDate: new Date(1992, 0, 21),
        campaign: 'Just Do iT !',
        campaignDate: new Date(2012, 0, 21),
        companyName: 'Microsoft',
        question1: '5',
        question2: '1',
        question3: '4',
        question4: '2',
        question5: '8'
    },
    {
        appointmentDate: new Date(1992, 0, 15),
        campaign: 'Can you try ?',
        campaignDate: new Date(2012, 0, 22),
        companyName: 'Microsoft',
        question1: '5',
        question2: '1',
        question3: '4',
        question4: '2',
        question5: '8'
    },
    {
        appointmentDate: new Date(1992, 0, 21),
        campaign: 'Are you ready ?',
        campaignDate: new Date(2012, 0, 21),
        companyName: 'UPS',
        question1: '5',
        question2: '1',
        question3: '4',
        question4: '2',
        question5: '8'
    },
    {
        appointmentDate: new Date(1980, 0, 21),
        campaign: 'Just Do iT !',
        campaignDate: new Date(2010, 0, 21),
        companyName: 'Apple',
        question1: '5',
        question2: '1',
        question3: '4',
        question4: '2',
        question5: '8'
    }
];
