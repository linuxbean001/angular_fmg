import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AlertUser } from './alert-user.model';
import { AlertUserPopupService } from './alert-user-popup.service';
import { AlertUserService } from './alert-user.service';

@Component({
    selector: 'jhi-alert-user-delete-dialog',
    templateUrl: './alert-user-delete-dialog.component.html'
})
export class AlertUserDeleteDialogComponent {

    alertUser: AlertUser;

    constructor(
        private alertUserService: AlertUserService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.alertUserService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'alertUserListModification',
                content: 'Deleted an alertUser'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-alert-user-delete-popup',
    template: ''
})
export class AlertUserDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private alertUserPopupService: AlertUserPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.alertUserPopupService
                .open(AlertUserDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
