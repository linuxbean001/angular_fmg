import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { AlertUser } from './alert-user.model';
import { AlertUserService } from './alert-user.service';

@Component({
    selector: 'jhi-alert-user-detail',
    templateUrl: './alert-user-detail.component.html'
})
export class AlertUserDetailComponent implements OnInit, OnDestroy {

    alertUser: AlertUser;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private alertUserService: AlertUserService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAlertUsers();
    }

    load(id) {
        this.alertUserService.find(id)
            .subscribe((alertUserResponse: HttpResponse<AlertUser>) => {
                this.alertUser = alertUserResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAlertUsers() {
        this.eventSubscriber = this.eventManager.subscribe(
            'alertUserListModification',
            (response) => this.load(this.alertUser.id)
        );
    }
}
