import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { AlertUser } from './alert-user.model';
import { AlertUserPopupService } from './alert-user-popup.service';
import { AlertUserService } from './alert-user.service';
import { Alert, AlertService } from '../alert';
import { User, UserService } from '../../shared';

@Component({
    selector: 'jhi-alert-user-dialog',
    templateUrl: './alert-user-dialog.component.html'
})
export class AlertUserDialogComponent implements OnInit {

    alertUser: AlertUser;
    isSaving: boolean;

    alerts: Alert[];

    users: User[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private alertUserService: AlertUserService,
        private alertService: AlertService,
        private userService: UserService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.alertService.query()
            .subscribe((res: HttpResponse<Alert[]>) => { this.alerts = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.userService.query()
            .subscribe((res: HttpResponse<User[]>) => { this.users = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.alertUser.id !== undefined) {
            this.subscribeToSaveResponse(
                this.alertUserService.update(this.alertUser));
        } else {
            this.subscribeToSaveResponse(
                this.alertUserService.create(this.alertUser));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<AlertUser>>) {
        result.subscribe((res: HttpResponse<AlertUser>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: AlertUser) {
        this.eventManager.broadcast({ name: 'alertUserListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackAlertById(index: number, item: Alert) {
        return item.id;
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-alert-user-popup',
    template: ''
})
export class AlertUserPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private alertUserPopupService: AlertUserPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.alertUserPopupService
                    .open(AlertUserDialogComponent as Component, params['id']);
            } else {
                this.alertUserPopupService
                    .open(AlertUserDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
