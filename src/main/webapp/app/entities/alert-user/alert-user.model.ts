import { BaseEntity } from './../../shared';

export class AlertUser implements BaseEntity {
    constructor(
        public id?: number,
        public isRead?: boolean,
        public readDate?: any,
        public alertId?: number,
        public userLogin?: string,
        public userId?: number,
    ) {
        this.isRead = false;
    }
}
