import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CmaSharedModule } from '../../shared';
import { CmaAdminModule } from '../../admin/admin.module';
import {
    AlertUserService,
    AlertUserPopupService,
    AlertUserComponent,
    AlertUserDetailComponent,
    AlertUserDialogComponent,
    AlertUserPopupComponent,
    AlertUserDeletePopupComponent,
    AlertUserDeleteDialogComponent,
    alertUserRoute,
    alertUserPopupRoute,
    AlertUserResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...alertUserRoute,
    ...alertUserPopupRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        CmaAdminModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        AlertUserComponent,
        AlertUserDetailComponent,
        AlertUserDialogComponent,
        AlertUserDeleteDialogComponent,
        AlertUserPopupComponent,
        AlertUserDeletePopupComponent,
    ],
    entryComponents: [
        AlertUserComponent,
        AlertUserDialogComponent,
        AlertUserPopupComponent,
        AlertUserDeleteDialogComponent,
        AlertUserDeletePopupComponent,
    ],
    providers: [
        AlertUserService,
        AlertUserPopupService,
        AlertUserResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaAlertUserModule {}
