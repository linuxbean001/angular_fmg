import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { AlertUserComponent } from './alert-user.component';
import { AlertUserDetailComponent } from './alert-user-detail.component';
import { AlertUserPopupComponent } from './alert-user-dialog.component';
import { AlertUserDeletePopupComponent } from './alert-user-delete-dialog.component';

@Injectable()
export class AlertUserResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const alertUserRoute: Routes = [
    {
        path: 'alert-user',
        component: AlertUserComponent,
        resolve: {
            'pagingParams': AlertUserResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER', 'ROLE_PORTAL_USER'],
            pageTitle: 'cmaApp.alertUser.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'alert-user/:id',
        component: AlertUserDetailComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_PORTAL_USER'],
            pageTitle: 'cmaApp.alertUser.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const alertUserPopupRoute: Routes = [
    {
        path: 'alert-user-new',
        component: AlertUserPopupComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_PORTAL_USER'],
            pageTitle: 'cmaApp.alertUser.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'alert-user/:id/edit',
        component: AlertUserPopupComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_PORTAL_USER'],
            pageTitle: 'cmaApp.alertUser.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'alert-user/:id/delete',
        component: AlertUserDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_PORTAL_USER'],
            pageTitle: 'cmaApp.alertUser.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
