import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { AlertUser } from './alert-user.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<AlertUser>;

@Injectable()
export class AlertUserService {

    private resourceUrl =  SERVER_API_URL + 'api/alert-users';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(alertUser: AlertUser): Observable<EntityResponseType> {
        const copy = this.convert(alertUser);
        return this.http.post<AlertUser>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(alertUser: AlertUser): Observable<EntityResponseType> {
        const copy = this.convert(alertUser);
        return this.http.put<AlertUser>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<AlertUser>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<AlertUser[]>> {
        const options = createRequestOption(req);
        return this.http.get<AlertUser[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<AlertUser[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: AlertUser = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<AlertUser[]>): HttpResponse<AlertUser[]> {
        const jsonResponse: AlertUser[] = res.body;
        const body: AlertUser[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to AlertUser.
     */
    private convertItemFromServer(alertUser: AlertUser): AlertUser {
        const copy: AlertUser = Object.assign({}, alertUser);
        copy.readDate = this.dateUtils
            .convertDateTimeFromServer(alertUser.readDate);
        return copy;
    }

    /**
     * Convert a AlertUser to a JSON which can be sent to the server.
     */
    private convert(alertUser: AlertUser): AlertUser {
        const copy: AlertUser = Object.assign({}, alertUser);

        copy.readDate = this.dateUtils.toDate(alertUser.readDate);
        return copy;
    }
}
