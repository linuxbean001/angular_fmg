import { BaseEntity } from './../../shared';

export class Alert implements BaseEntity {
    constructor(
        public id?: number,
        public stamp?: any,
        public description?: string,
        public type?: string,
        public status?: string,
        public contactRowNums?: string,
        public clientCampaignName?: string,
        public clientCampaignId?: number,
        public clientListName?: string,
        public clientListId?: number,
        public alertUsers?: BaseEntity[],
    ) {
    }
}
