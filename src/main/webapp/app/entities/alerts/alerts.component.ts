import {Component, OnDestroy, OnInit, QueryList, ViewChildren} from '@angular/core';
import {AlertElement, CampaignListContactResultService} from '../campaign-list-contact-result';
import {BehaviorSubject} from 'rxjs';
import {FormControl} from '@angular/forms';
import {Columns} from '../my-pipeline/domain/ColumnSearch';
import {
    NgbdSortableHeaderDirective,
    SortDirection,
    SortEvent
} from '../my-pipeline/table/directives/sortable.directive';
import {ActivatedRoute, Router} from '@angular/router';

interface State {
    page: number;
    pageSize: number;
    sortColumn: string;
    sortDirection: SortDirection;
}

function compare(v1, v2) {
    return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
}

function sort(alertsNotify: AlertElement[], column: string, direction: string) {
    if (direction === '') {
        return alertsNotify;
    } else {
        return [...alertsNotify].sort((a, b) => {
            const res = compare(a[column], b[column]);
            return direction === 'asc' ? res : -res;
        });
    }
}

@Component({
    selector: 'jhi-alerts',
    templateUrl: './alerts.component.html',
    styleUrls: ['alerts.component.css']
})
export class AlertsComponent implements OnInit, OnDestroy {

    alertElements: AlertElement[];
    alertElementsList = new BehaviorSubject<AlertElement[]>([]);
    filter = new FormControl('');
    columns = Columns;
    private _state: State = {
        page: 1,
        pageSize: 4,
        sortColumn: '',
        sortDirection: ''
    };
    pointerClass: string;

    @ViewChildren(NgbdSortableHeaderDirective) headers: QueryList<NgbdSortableHeaderDirective>;

    constructor(
        private campaignListContactResultService: CampaignListContactResultService,
        private _route: ActivatedRoute,
        private _router: Router
    ) {
    }

    ngOnInit() {
        this.loadAlertElements();
    }

    private loadAlertElements() {
        this.campaignListContactResultService.getAlertsForCurrentUser().subscribe((elemnts) => {
            this.alertElements = elemnts.body;
            this.alertElementsList.next(elemnts.body);
        });
    }

    onSort({column, direction}: SortEvent) {
        // resetting other headers
        this.headers.forEach((header) => {
            if (header.jhiSortable !== column) {
                header.direction = '';
            }
        });

        this._state.sortColumn = column;
        this._state.sortDirection = direction;

        this.alertElements = sort(this.alertElements, this._state.sortColumn, this._state.sortDirection);
        this.alertElementsList.next(this.alertElements);
    }

    setPointer(pointer: string) {
        this.pointerClass = pointer;
    }

    ngOnDestroy(): void {
    }

    navigate(alertElement: AlertElement) {
        const parameters = {
            method: 'APPROVE_ONLY',
            campaignId: alertElement.campaignId,
            listId: alertElement.listId,
            contactRowNums: alertElement.contactRowNums
        };
        this._router.navigate(['../console'], {queryParams: parameters});
    }

}
