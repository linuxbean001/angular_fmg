import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {CmaSharedModule} from '../../shared';
import {TabModule} from '../../layouts/tabs/tab.module';
import {AngularSplitModule} from 'angular-split';
import {CmaClientCampaignModule} from '../client-campaign/client-campaign.module';
import {CmaEmailTemplateModule} from '../email-template/email-template.module';
import {CmaClientCampaignSmsModule} from '../client-campaign-sms/client-campaign-sms.module';
import {alertsRoute} from './alerts.route';
import {AlertsComponent} from './alerts.component';
import {CmaPipelineModule} from '../my-pipeline/pipeline.module';

const ENTITY_STATES = [
    ...alertsRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        TabModule,
        RouterModule.forChild(ENTITY_STATES),
        AngularSplitModule,
        CmaClientCampaignModule,
        CmaEmailTemplateModule,
        CmaClientCampaignSmsModule,
        CmaPipelineModule
    ],
    declarations: [
        AlertsComponent,
    ],
    entryComponents: [
        AlertsComponent,
    ],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaAlertsModule {
}
