import {Routes} from '@angular/router';

import {UserRouteAccessService} from '../../shared';
import {AlertsComponent} from './alerts.component';

export const alertsRoute: Routes = [
    {
        path: 'alerts-notifications',
        component: AlertsComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_PORTAL_USER'],
            pageTitle: 'alerts.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
