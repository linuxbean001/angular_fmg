export class AlertNotification {
    constructor(public dateAlert: Date,
                public typeAlert: string,
                public  campaing: string,
                public descriptionAlert: string) {
    }
}
