import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AppointmentAttendee } from './appointment-attendee.model';
import { AppointmentAttendeePopupService } from './appointment-attendee-popup.service';
import { AppointmentAttendeeService } from './appointment-attendee.service';

@Component({
    selector: 'jhi-appointment-attendee-delete-dialog',
    templateUrl: './appointment-attendee-delete-dialog.component.html'
})
export class AppointmentAttendeeDeleteDialogComponent {

    appointmentAttendee: AppointmentAttendee;

    constructor(
        private appointmentAttendeeService: AppointmentAttendeeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.appointmentAttendeeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'appointmentAttendeeListModification',
                content: 'Deleted an appointmentAttendee',
                id,
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-appointment-attendee-delete-popup',
    template: ''
})
export class AppointmentAttendeeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private appointmentAttendeePopupService: AppointmentAttendeePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.appointmentAttendeePopupService
                .open(AppointmentAttendeeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
