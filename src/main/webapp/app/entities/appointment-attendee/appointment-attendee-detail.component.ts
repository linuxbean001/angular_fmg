import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { AppointmentAttendee } from './appointment-attendee.model';
import { AppointmentAttendeeService } from './appointment-attendee.service';

@Component({
    selector: 'jhi-appointment-attendee-detail',
    templateUrl: './appointment-attendee-detail.component.html'
})
export class AppointmentAttendeeDetailComponent implements OnInit, OnDestroy {

    appointmentAttendee: AppointmentAttendee;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private appointmentAttendeeService: AppointmentAttendeeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAppointmentAttendees();
    }

    load(id) {
        this.appointmentAttendeeService.find(id)
            .subscribe((appointmentAttendeeResponse: HttpResponse<AppointmentAttendee>) => {
                this.appointmentAttendee = appointmentAttendeeResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAppointmentAttendees() {
        this.eventSubscriber = this.eventManager.subscribe(
            'appointmentAttendeeListModification',
            (response) => this.load(this.appointmentAttendee.id)
        );
    }
}
