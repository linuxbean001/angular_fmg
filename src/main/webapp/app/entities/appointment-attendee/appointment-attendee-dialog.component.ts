import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { AppointmentAttendee } from './appointment-attendee.model';
import { AppointmentAttendeePopupService } from './appointment-attendee-popup.service';
import { AppointmentAttendeeService } from './appointment-attendee.service';
import { ClientCampaign, ClientCampaignService } from '../client-campaign';

@Component({
    selector: 'jhi-appointment-attendee-dialog',
    templateUrl: './appointment-attendee-dialog.component.html'
})
export class AppointmentAttendeeDialogComponent implements OnInit {

    appointmentAttendee: AppointmentAttendee;
    isSaving: boolean;

    clientcampaigns: ClientCampaign[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private appointmentAttendeeService: AppointmentAttendeeService,
        private clientCampaignService: ClientCampaignService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.clientCampaignService.query()
            .subscribe((res: HttpResponse<ClientCampaign[]>) => { this.clientcampaigns = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.appointmentAttendee.id !== undefined) {
            this.subscribeToSaveResponse(
                this.appointmentAttendeeService.update(this.appointmentAttendee));
        } else {
            this.subscribeToSaveResponse(
                this.appointmentAttendeeService.create(this.appointmentAttendee));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<AppointmentAttendee>>) {
        result.subscribe((res: HttpResponse<AppointmentAttendee>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: AppointmentAttendee) {
        this.eventManager.broadcast({ name: 'appointmentAttendeeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackClientCampaignById(index: number, item: ClientCampaign) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-appointment-attendee-popup',
    template: ''
})
export class AppointmentAttendeePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private appointmentAttendeePopupService: AppointmentAttendeePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.appointmentAttendeePopupService
                    .open(AppointmentAttendeeDialogComponent as Component, params['id']);
            } else {
                this.appointmentAttendeePopupService
                    .open(AppointmentAttendeeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
