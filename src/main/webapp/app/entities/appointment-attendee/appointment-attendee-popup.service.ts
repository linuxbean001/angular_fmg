import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { AppointmentAttendee } from './appointment-attendee.model';
import { AppointmentAttendeeService } from './appointment-attendee.service';

@Injectable()
export class AppointmentAttendeePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private appointmentAttendeeService: AppointmentAttendeeService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.appointmentAttendeeService.find(id)
                    .subscribe((appointmentAttendeeResponse: HttpResponse<AppointmentAttendee>) => {
                        const appointmentAttendee: AppointmentAttendee = appointmentAttendeeResponse.body;
                        this.ngbModalRef = this.appointmentAttendeeModalRef(component, appointmentAttendee);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.appointmentAttendeeModalRef(component, new AppointmentAttendee());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    appointmentAttendeeModalRef(component: Component, appointmentAttendee: AppointmentAttendee): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.appointmentAttendee = appointmentAttendee;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
