import {BaseEntity} from './../../shared';
import {EventFrequencyRate} from '../client-campaign';

export class AppointmentAttendee implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public clientCampaignId?: number,
        public clientCampaignName?: string,
        public contactDetail?: string,
        public domain?: string,
        public username?: string,
        public password?: string,
        public appointmentDays?: string,
    ) {
    }
}

export class AppointmentDays {
    static ITEMS: EventFrequencyRate[] = [
        new AppointmentDays('1 day', '1_DAY'),
        new AppointmentDays('2 days', '2_DAYS'),
        new AppointmentDays('3 days', '3_DAYS'),
        new AppointmentDays('4 days', '4_DAYS'),
        new AppointmentDays('5 days', '5_DAYS'),
        new AppointmentDays('6 days', '6_DAYS'),
        new AppointmentDays('1 week', '1_WEEK'),
        new AppointmentDays('2 weeks', '2_WEEKS'),
        new AppointmentDays('3 weeks', '3_WEEKS'),
        new AppointmentDays('4 weeks', '4_WEEKS'),
        new AppointmentDays('5 weeks', '5_WEEKS'),
        new AppointmentDays('6 weeks', '6_WEEKS'),
    ];

    public constructor(
        public name: string,
        public ident: string,
    ) {
    }
}
