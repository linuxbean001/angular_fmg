import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CmaSharedModule } from '../../shared';
import {
    AppointmentAttendeeService,
    AppointmentAttendeePopupService,
    AppointmentAttendeeComponent,
    AppointmentAttendeeDetailComponent,
    AppointmentAttendeeDialogComponent,
    AppointmentAttendeePopupComponent,
    AppointmentAttendeeDeletePopupComponent,
    AppointmentAttendeeDeleteDialogComponent,
    appointmentAttendeeRoute,
    appointmentAttendeePopupRoute,
    AppointmentAttendeeResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...appointmentAttendeeRoute,
    ...appointmentAttendeePopupRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        AppointmentAttendeeComponent,
        AppointmentAttendeeDetailComponent,
        AppointmentAttendeeDialogComponent,
        AppointmentAttendeeDeleteDialogComponent,
        AppointmentAttendeePopupComponent,
        AppointmentAttendeeDeletePopupComponent,
    ],
    entryComponents: [
        AppointmentAttendeeComponent,
        AppointmentAttendeeDialogComponent,
        AppointmentAttendeePopupComponent,
        AppointmentAttendeeDeleteDialogComponent,
        AppointmentAttendeeDeletePopupComponent,
    ],
    providers: [
        AppointmentAttendeeService,
        AppointmentAttendeePopupService,
        AppointmentAttendeeResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaAppointmentAttendeeModule {}
