import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { AppointmentAttendeeComponent } from './appointment-attendee.component';
import { AppointmentAttendeeDetailComponent } from './appointment-attendee-detail.component';
import { AppointmentAttendeePopupComponent } from './appointment-attendee-dialog.component';
import { AppointmentAttendeeDeletePopupComponent } from './appointment-attendee-delete-dialog.component';

@Injectable()
export class AppointmentAttendeeResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const appointmentAttendeeRoute: Routes = [
    {
        path: 'appointment-attendee',
        component: AppointmentAttendeeComponent,
        resolve: {
            'pagingParams': AppointmentAttendeeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.appointmentAttendee.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'appointment-attendee/:id',
        component: AppointmentAttendeeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.appointmentAttendee.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const appointmentAttendeePopupRoute: Routes = [
    {
        path: 'appointment-attendee-new',
        component: AppointmentAttendeePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.appointmentAttendee.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'appointment-attendee/:id/edit',
        component: AppointmentAttendeePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.appointmentAttendee.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'appointment-attendee/:id/delete',
        component: AppointmentAttendeeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.appointmentAttendee.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
