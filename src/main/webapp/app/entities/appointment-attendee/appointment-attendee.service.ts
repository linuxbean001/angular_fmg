import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { AppointmentAttendee } from './appointment-attendee.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<AppointmentAttendee>;

@Injectable()
export class AppointmentAttendeeService {

    private resourceUrl =  SERVER_API_URL + 'api/appointment-attendees';

    constructor(private http: HttpClient) { }

    create(appointmentAttendee: AppointmentAttendee): Observable<EntityResponseType> {
        const copy = this.convert(appointmentAttendee);
        return this.http.post<AppointmentAttendee>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(appointmentAttendee: AppointmentAttendee): Observable<EntityResponseType> {
        const copy = this.convert(appointmentAttendee);
        return this.http.put<AppointmentAttendee>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<AppointmentAttendee>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<AppointmentAttendee[]>> {
        const options = createRequestOption(req);
        return this.http.get<AppointmentAttendee[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<AppointmentAttendee[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: AppointmentAttendee = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<AppointmentAttendee[]>): HttpResponse<AppointmentAttendee[]> {
        const jsonResponse: AppointmentAttendee[] = res.body;
        const body: AppointmentAttendee[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to AppointmentAttendee.
     */
    private convertItemFromServer(appointmentAttendee: AppointmentAttendee): AppointmentAttendee {
        const copy: AppointmentAttendee = Object.assign({}, appointmentAttendee);
        return copy;
    }

    /**
     * Convert a AppointmentAttendee to a JSON which can be sent to the server.
     */
    private convert(appointmentAttendee: AppointmentAttendee): AppointmentAttendee {
        const copy: AppointmentAttendee = Object.assign({}, appointmentAttendee);
        return copy;
    }
}
