import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager} from 'ng-jhipster';

import {BookingQuestionField} from './booking-question-field.model';
import {BookingQuestionFieldPopupService} from './booking-question-field-popup.service';
import {BookingQuestionFieldService} from './booking-question-field.service';

@Component({
    selector: 'jhi-booking-question-field-delete-dialog',
    templateUrl: './booking-question-field-delete-dialog.component.html'
})
export class BookingQuestionFieldDeleteDialogComponent {

    bookingQuestionField: BookingQuestionField;

    constructor(
        private bookingQuestionFieldService: BookingQuestionFieldService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.bookingQuestionFieldService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'bookingQuestionFieldListModification',
                content: 'Deleted an bookingQuestionField',
                id
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-booking-question-field-delete-popup',
    template: ''
})
export class BookingQuestionFieldDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private bookingQuestionFieldPopupService: BookingQuestionFieldPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.bookingQuestionFieldPopupService
                .open(BookingQuestionFieldDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
