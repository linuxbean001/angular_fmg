import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { BookingQuestionField } from './booking-question-field.model';
import { BookingQuestionFieldService } from './booking-question-field.service';

@Component({
    selector: 'jhi-booking-question-field-detail',
    templateUrl: './booking-question-field-detail.component.html'
})
export class BookingQuestionFieldDetailComponent implements OnInit, OnDestroy {

    bookingQuestionField: BookingQuestionField;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private bookingQuestionFieldService: BookingQuestionFieldService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInBookingQuestionFields();
    }

    load(id) {
        this.bookingQuestionFieldService.find(id)
            .subscribe((bookingQuestionFieldResponse: HttpResponse<BookingQuestionField>) => {
                this.bookingQuestionField = bookingQuestionFieldResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInBookingQuestionFields() {
        this.eventSubscriber = this.eventManager.subscribe(
            'bookingQuestionFieldListModification',
            (response) => this.load(this.bookingQuestionField.id)
        );
    }
}
