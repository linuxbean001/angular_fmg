import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';

import {BookingQuestionField} from './booking-question-field.model';
import {BookingQuestionFieldPopupService} from './booking-question-field-popup.service';
import {BookingQuestionFieldService} from './booking-question-field.service';
import {BookingQuestion, BookingQuestionService} from '../booking-question';

@Component({
    selector: 'jhi-booking-question-field-dialog',
    templateUrl: './booking-question-field-dialog.component.html'
})
export class BookingQuestionFieldDialogComponent implements OnInit {

    bookingQuestionField: BookingQuestionField;
    isSaving: boolean;

    bookingquestions: BookingQuestion[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private bookingQuestionFieldService: BookingQuestionFieldService,
        private bookingQuestionService: BookingQuestionService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.bookingQuestionService.query()
            .subscribe((res: HttpResponse<BookingQuestion[]>) => { this.bookingquestions = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.bookingQuestionField.id !== undefined) {
            this.subscribeToSaveResponse(
                this.bookingQuestionFieldService.update(this.bookingQuestionField));
        } else {
            this.subscribeToSaveResponse(
                this.bookingQuestionFieldService.create(this.bookingQuestionField));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<BookingQuestionField>>) {
        result.subscribe((res: HttpResponse<BookingQuestionField>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: BookingQuestionField) {
        this.eventManager.broadcast({
            name: 'bookingQuestionFieldListModification',
            content: 'OK',
            bookingQuestionField: result,
        });
        this.isSaving = false;
        this.activeModal.close(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackBookingQuestionById(index: number, item: BookingQuestion) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-booking-question-field-popup',
    template: ''
})
export class BookingQuestionFieldPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private bookingQuestionFieldPopupService: BookingQuestionFieldPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.bookingQuestionFieldPopupService
                    .open(BookingQuestionFieldDialogComponent as Component, params['id']);
            } else {
                this.bookingQuestionFieldPopupService
                    .open(BookingQuestionFieldDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
