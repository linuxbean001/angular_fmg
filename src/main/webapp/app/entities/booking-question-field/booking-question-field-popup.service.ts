import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { BookingQuestionField } from './booking-question-field.model';
import { BookingQuestionFieldService } from './booking-question-field.service';

@Injectable()
export class BookingQuestionFieldPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private bookingQuestionFieldService: BookingQuestionFieldService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.bookingQuestionFieldService.find(id)
                    .subscribe((bookingQuestionFieldResponse: HttpResponse<BookingQuestionField>) => {
                        const bookingQuestionField: BookingQuestionField = bookingQuestionFieldResponse.body;
                        this.ngbModalRef = this.bookingQuestionFieldModalRef(component, bookingQuestionField);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.bookingQuestionFieldModalRef(component, new BookingQuestionField());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    bookingQuestionFieldModalRef(component: Component, bookingQuestionField: BookingQuestionField): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.bookingQuestionField = bookingQuestionField;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
