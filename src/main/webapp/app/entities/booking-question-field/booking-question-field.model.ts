import {BaseEntity} from './../../shared';

export class BookingQuestionField implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public dataType?: string,
        public category?: string,
        public prePopulationData?: string,
        public isVisible?: boolean,
        public isRequired?: boolean,
        public bookingQuestionName?: string,
        public bookingQuestionId?: number,
    ) {
        this.isVisible = false;
        this.isRequired = false;
    }
}

export class BookingQuestionFieldDataType {
    static APPOINTMENT_DATE = new BookingQuestionFieldDataType('Appointment date', 'APPOINTMENT_DATE');
    static APPOINTMENT_TIME = new BookingQuestionFieldDataType('Appointment time', 'APPOINTMENT_TIME');
    static SHORT_TEXT = new BookingQuestionFieldDataType('Short text', 'SHORT_TEX');
    static NUMBER = new BookingQuestionFieldDataType('Number', 'NUMBER');
    static CHECKBOX = new BookingQuestionFieldDataType('Checkbox', 'CHECKBOX');
    static LONG_TEXT = new BookingQuestionFieldDataType('Long text', 'LONG_TEXT');
    static DROPDOWN = new BookingQuestionFieldDataType('Dropdown', 'DROPDOWN');
    static DATE = new BookingQuestionFieldDataType('Date', 'DATE');
    static TIME = new BookingQuestionFieldDataType('Time', 'TIME');
    static NON_EDITABLE = new BookingQuestionFieldDataType('Non-editable', 'NON_EDITABLE');

    static ITEMS: BookingQuestionFieldDataType[] = [
        BookingQuestionFieldDataType.APPOINTMENT_DATE,
        BookingQuestionFieldDataType.APPOINTMENT_TIME,
        BookingQuestionFieldDataType.SHORT_TEXT,
        BookingQuestionFieldDataType.NUMBER,
        BookingQuestionFieldDataType.CHECKBOX,
        BookingQuestionFieldDataType.DROPDOWN,
        BookingQuestionFieldDataType.DATE,
        BookingQuestionFieldDataType.TIME,
        BookingQuestionFieldDataType.LONG_TEXT,
        BookingQuestionFieldDataType.NON_EDITABLE,
    ];

    public constructor(
        public name: string,
        public ident: string,
    ) {
    }
}

export class BookingQuestionFieldCategory {
    static ITEMS: BookingQuestionFieldCategory[] = [
        new BookingQuestionFieldCategory('None', 'NONE'),
        new BookingQuestionFieldCategory('Agents', 'AGENTS'),
        new BookingQuestionFieldCategory('Contacts', 'CONTACTS'),
        new BookingQuestionFieldCategory('Sites', 'SITES'),
    ];

    public constructor(
        public name: string,
        public ident: string,
    ) {
    }
}
