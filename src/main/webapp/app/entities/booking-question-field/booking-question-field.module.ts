import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CmaSharedModule } from '../../shared';
import {
    BookingQuestionFieldService,
    BookingQuestionFieldPopupService,
    BookingQuestionFieldComponent,
    BookingQuestionFieldDetailComponent,
    BookingQuestionFieldDialogComponent,
    BookingQuestionFieldPopupComponent,
    BookingQuestionFieldDeletePopupComponent,
    BookingQuestionFieldDeleteDialogComponent,
    bookingQuestionFieldRoute,
    bookingQuestionFieldPopupRoute,
    BookingQuestionFieldResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...bookingQuestionFieldRoute,
    ...bookingQuestionFieldPopupRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        BookingQuestionFieldComponent,
        BookingQuestionFieldDetailComponent,
        BookingQuestionFieldDialogComponent,
        BookingQuestionFieldDeleteDialogComponent,
        BookingQuestionFieldPopupComponent,
        BookingQuestionFieldDeletePopupComponent,
    ],
    entryComponents: [
        BookingQuestionFieldComponent,
        BookingQuestionFieldDialogComponent,
        BookingQuestionFieldPopupComponent,
        BookingQuestionFieldDeleteDialogComponent,
        BookingQuestionFieldDeletePopupComponent,
    ],
    providers: [
        BookingQuestionFieldService,
        BookingQuestionFieldPopupService,
        BookingQuestionFieldResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaBookingQuestionFieldModule {}
