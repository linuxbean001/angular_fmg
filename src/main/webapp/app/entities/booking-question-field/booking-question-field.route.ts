import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { BookingQuestionFieldComponent } from './booking-question-field.component';
import { BookingQuestionFieldDetailComponent } from './booking-question-field-detail.component';
import { BookingQuestionFieldPopupComponent } from './booking-question-field-dialog.component';
import { BookingQuestionFieldDeletePopupComponent } from './booking-question-field-delete-dialog.component';

@Injectable()
export class BookingQuestionFieldResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const bookingQuestionFieldRoute: Routes = [
    {
        path: 'booking-question-field',
        component: BookingQuestionFieldComponent,
        resolve: {
            'pagingParams': BookingQuestionFieldResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.bookingQuestionField.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'booking-question-field/:id',
        component: BookingQuestionFieldDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.bookingQuestionField.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const bookingQuestionFieldPopupRoute: Routes = [
    {
        path: 'booking-question-field-new',
        component: BookingQuestionFieldPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.bookingQuestionField.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'booking-question-field/:id/edit',
        component: BookingQuestionFieldPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.bookingQuestionField.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'booking-question-field/:id/delete',
        component: BookingQuestionFieldDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.bookingQuestionField.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
