import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { BookingQuestionField } from './booking-question-field.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<BookingQuestionField>;

@Injectable()
export class BookingQuestionFieldService {

    private resourceUrl =  SERVER_API_URL + 'api/booking-question-fields';

    constructor(private http: HttpClient) { }

    create(bookingQuestionField: BookingQuestionField): Observable<EntityResponseType> {
        const copy = this.convert(bookingQuestionField);
        return this.http.post<BookingQuestionField>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(bookingQuestionField: BookingQuestionField): Observable<EntityResponseType> {
        const copy = this.convert(bookingQuestionField);
        return this.http.put<BookingQuestionField>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<BookingQuestionField>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<BookingQuestionField[]>> {
        const options = createRequestOption(req);
        return this.http.get<BookingQuestionField[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<BookingQuestionField[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: BookingQuestionField = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<BookingQuestionField[]>): HttpResponse<BookingQuestionField[]> {
        const jsonResponse: BookingQuestionField[] = res.body;
        const body: BookingQuestionField[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to BookingQuestionField.
     */
    private convertItemFromServer(bookingQuestionField: BookingQuestionField): BookingQuestionField {
        const copy: BookingQuestionField = Object.assign({}, bookingQuestionField);
        return copy;
    }

    /**
     * Convert a BookingQuestionField to a JSON which can be sent to the server.
     */
    private convert(bookingQuestionField: BookingQuestionField): BookingQuestionField {
        const copy: BookingQuestionField = Object.assign({}, bookingQuestionField);
        return copy;
    }
}
