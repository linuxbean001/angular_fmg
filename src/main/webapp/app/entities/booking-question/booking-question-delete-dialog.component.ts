import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager} from 'ng-jhipster';

import {BookingQuestion} from './booking-question.model';
import {BookingQuestionPopupService} from './booking-question-popup.service';
import {BookingQuestionService} from './booking-question.service';

@Component({
    selector: 'jhi-booking-question-delete-dialog',
    templateUrl: './booking-question-delete-dialog.component.html'
})
export class BookingQuestionDeleteDialogComponent {

    bookingQuestion: BookingQuestion;

    constructor(
        private bookingQuestionService: BookingQuestionService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.bookingQuestionService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'bookingQuestionListModification',
                content: 'Deleted an bookingQuestion',
                id
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-booking-question-delete-popup',
    template: ''
})
export class BookingQuestionDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private bookingQuestionPopupService: BookingQuestionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.bookingQuestionPopupService
                .open(BookingQuestionDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
