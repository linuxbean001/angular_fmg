import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { BookingQuestion } from './booking-question.model';
import { BookingQuestionService } from './booking-question.service';

@Component({
    selector: 'jhi-booking-question-detail',
    templateUrl: './booking-question-detail.component.html'
})
export class BookingQuestionDetailComponent implements OnInit, OnDestroy {

    bookingQuestion: BookingQuestion;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private bookingQuestionService: BookingQuestionService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInBookingQuestions();
    }

    load(id) {
        this.bookingQuestionService.find(id)
            .subscribe((bookingQuestionResponse: HttpResponse<BookingQuestion>) => {
                this.bookingQuestion = bookingQuestionResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInBookingQuestions() {
        this.eventSubscriber = this.eventManager.subscribe(
            'bookingQuestionListModification',
            (response) => this.load(this.bookingQuestion.id)
        );
    }
}
