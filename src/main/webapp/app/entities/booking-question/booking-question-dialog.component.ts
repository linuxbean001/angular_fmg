import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';

import {BookingQuestion} from './booking-question.model';
import {BookingQuestionPopupService} from './booking-question-popup.service';
import {BookingQuestionService} from './booking-question.service';
import {ClientCampaign, ClientCampaignService} from '../client-campaign';
import {
    BookingQuestionField,
    BookingQuestionFieldCategory,
    BookingQuestionFieldDataType,
    BookingQuestionFieldService
} from '../booking-question-field';
import {ConfirmService} from '../../shared';

@Component({
    selector: 'jhi-booking-question-dialog',
    templateUrl: './booking-question-dialog.component.html'
})
export class BookingQuestionDialogComponent implements OnInit {

    bookingQuestion: BookingQuestion;
    isSaving: boolean;

    BookingQuestionFieldDataType: typeof BookingQuestionFieldDataType = BookingQuestionFieldDataType;
    BookingQuestionFieldCategory: typeof BookingQuestionFieldCategory = BookingQuestionFieldCategory;

    clientCampaigns: ClientCampaign[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private bookingQuestionService: BookingQuestionService,
        private bookingQuestionFieldService: BookingQuestionFieldService,
        private eventManager: JhiEventManager,
        private confirmService: ConfirmService,
        private clientCampaignService: ClientCampaignService,
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.clientCampaignService.query()
            .subscribe((res: HttpResponse<ClientCampaign[]>) => {
                this.clientCampaigns = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.bookingQuestion.id !== undefined) {
            this.subscribeToSaveResponse(
                this.bookingQuestionService.update(this.bookingQuestion));
        } else {
            this.subscribeToSaveResponse(
                this.bookingQuestionService.create(this.bookingQuestion));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<BookingQuestion>>) {
        result.subscribe((res: HttpResponse<BookingQuestion>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: BookingQuestion) {
        this.eventManager.broadcast({
            name: 'bookingQuestionListModification',
            content: 'OK',
            bookingQuestion: result,
        });
        this.isSaving = false;
        this.activeModal.close(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackClientCampaignById(index: number, item: ClientCampaign) {
        return item.id;
    }

    createNewField() {
        const bookingQuestionField = new BookingQuestionField();
        this.bookingQuestion.fields.push(bookingQuestionField);
    }

    removeField(item: BookingQuestionField) {
        this.removeFieldInternal(item);
    }

    removeFieldInternal(item: BookingQuestionField) {
        const fields = this.bookingQuestion.fields;
        fields.splice(fields.indexOf(item), 1);
    }
}

@Component({
    selector: 'jhi-booking-question-popup',
    template: ''
})
export class BookingQuestionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private bookingQuestionPopupService: BookingQuestionPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            // const displayCampaign = params['displayCampaign'] === undefined ? true : params['displayCampaign'] === 'true';

            if (params['id']) {
                this.bookingQuestionPopupService
                    .open(BookingQuestionDialogComponent as Component, params['id']);
            } else {
                this.bookingQuestionPopupService
                    .open(BookingQuestionDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
