import {BaseEntity} from './../../shared';
import {BookingQuestionField} from '../booking-question-field';

export class BookingQuestion implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public questionText?: string,
        public order?: number,
        public isVisible?: boolean,
        public fields: BookingQuestionField[] = [],
        public clientCampaignName?: string,
        public clientCampaignId?: number,
    ) {
        this.isVisible = true;
    }
}
