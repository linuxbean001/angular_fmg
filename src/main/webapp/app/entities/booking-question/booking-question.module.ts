import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CmaSharedModule } from '../../shared';
import {
    BookingQuestionService,
    BookingQuestionPopupService,
    BookingQuestionComponent,
    BookingQuestionDetailComponent,
    BookingQuestionDialogComponent,
    BookingQuestionPopupComponent,
    BookingQuestionDeletePopupComponent,
    BookingQuestionDeleteDialogComponent,
    bookingQuestionRoute,
    bookingQuestionPopupRoute,
    BookingQuestionResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...bookingQuestionRoute,
    ...bookingQuestionPopupRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        BookingQuestionComponent,
        BookingQuestionDetailComponent,
        BookingQuestionDialogComponent,
        BookingQuestionDeleteDialogComponent,
        BookingQuestionPopupComponent,
        BookingQuestionDeletePopupComponent,
    ],
    entryComponents: [
        BookingQuestionComponent,
        BookingQuestionDialogComponent,
        BookingQuestionPopupComponent,
        BookingQuestionDeleteDialogComponent,
        BookingQuestionDeletePopupComponent,
    ],
    providers: [
        BookingQuestionService,
        BookingQuestionPopupService,
        BookingQuestionResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaBookingQuestionModule {}
