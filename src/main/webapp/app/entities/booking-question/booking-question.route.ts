import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { BookingQuestionComponent } from './booking-question.component';
import { BookingQuestionDetailComponent } from './booking-question-detail.component';
import { BookingQuestionPopupComponent } from './booking-question-dialog.component';
import { BookingQuestionDeletePopupComponent } from './booking-question-delete-dialog.component';

@Injectable()
export class BookingQuestionResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const bookingQuestionRoute: Routes = [
    {
        path: 'booking-question',
        component: BookingQuestionComponent,
        resolve: {
            'pagingParams': BookingQuestionResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.bookingQuestion.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'booking-question/:id',
        component: BookingQuestionDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.bookingQuestion.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const bookingQuestionPopupRoute: Routes = [
    {
        path: 'booking-question-new',
        component: BookingQuestionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.bookingQuestion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'booking-question/:id/edit',
        component: BookingQuestionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.bookingQuestion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'booking-question/:id/delete',
        component: BookingQuestionDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.bookingQuestion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
