import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { BookingQuestion } from './booking-question.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<BookingQuestion>;

@Injectable()
export class BookingQuestionService {

    private resourceUrl =  SERVER_API_URL + 'api/booking-questions';

    constructor(private http: HttpClient) { }

    create(bookingQuestion: BookingQuestion): Observable<EntityResponseType> {
        const copy = this.convert(bookingQuestion);
        return this.http.post<BookingQuestion>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(bookingQuestion: BookingQuestion): Observable<EntityResponseType> {
        const copy = this.convert(bookingQuestion);
        return this.http.put<BookingQuestion>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<BookingQuestion>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<BookingQuestion[]>> {
        const options = createRequestOption(req);
        return this.http.get<BookingQuestion[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<BookingQuestion[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: BookingQuestion = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<BookingQuestion[]>): HttpResponse<BookingQuestion[]> {
        const jsonResponse: BookingQuestion[] = res.body;
        const body: BookingQuestion[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to BookingQuestion.
     */
    private convertItemFromServer(bookingQuestion: BookingQuestion): BookingQuestion {
        const copy: BookingQuestion = Object.assign({}, bookingQuestion);
        return copy;
    }

    /**
     * Convert a BookingQuestion to a JSON which can be sent to the server.
     */
    private convert(bookingQuestion: BookingQuestion): BookingQuestion {
        const copy: BookingQuestion = Object.assign({}, bookingQuestion);
        return copy;
    }
}
