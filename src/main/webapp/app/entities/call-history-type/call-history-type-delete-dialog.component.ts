import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CallHistoryType } from './call-history-type.model';
import { CallHistoryTypePopupService } from './call-history-type-popup.service';
import { CallHistoryTypeService } from './call-history-type.service';

@Component({
    selector: 'jhi-call-history-type-delete-dialog',
    templateUrl: './call-history-type-delete-dialog.component.html'
})
export class CallHistoryTypeDeleteDialogComponent {

    callHistoryType: CallHistoryType;

    constructor(
        private callHistoryTypeService: CallHistoryTypeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.callHistoryTypeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'callHistoryTypeListModification',
                content: 'Deleted an callHistoryType'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-call-history-type-delete-popup',
    template: ''
})
export class CallHistoryTypeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private callHistoryTypePopupService: CallHistoryTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.callHistoryTypePopupService
                .open(CallHistoryTypeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
