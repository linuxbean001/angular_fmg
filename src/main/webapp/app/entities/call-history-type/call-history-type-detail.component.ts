import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { CallHistoryType } from './call-history-type.model';
import { CallHistoryTypeService } from './call-history-type.service';

@Component({
    selector: 'jhi-call-history-type-detail',
    templateUrl: './call-history-type-detail.component.html'
})
export class CallHistoryTypeDetailComponent implements OnInit, OnDestroy {

    callHistoryType: CallHistoryType;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private callHistoryTypeService: CallHistoryTypeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCallHistoryTypes();
    }

    load(id) {
        this.callHistoryTypeService.find(id)
            .subscribe((callHistoryTypeResponse: HttpResponse<CallHistoryType>) => {
                this.callHistoryType = callHistoryTypeResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCallHistoryTypes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'callHistoryTypeListModification',
            (response) => this.load(this.callHistoryType.id)
        );
    }
}
