import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CallHistoryType } from './call-history-type.model';
import { CallHistoryTypePopupService } from './call-history-type-popup.service';
import { CallHistoryTypeService } from './call-history-type.service';

@Component({
    selector: 'jhi-call-history-type-dialog',
    templateUrl: './call-history-type-dialog.component.html'
})
export class CallHistoryTypeDialogComponent implements OnInit {

    callHistoryType: CallHistoryType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private callHistoryTypeService: CallHistoryTypeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.callHistoryType.id !== undefined) {
            this.subscribeToSaveResponse(
                this.callHistoryTypeService.update(this.callHistoryType));
        } else {
            this.subscribeToSaveResponse(
                this.callHistoryTypeService.create(this.callHistoryType));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<CallHistoryType>>) {
        result.subscribe((res: HttpResponse<CallHistoryType>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: CallHistoryType) {
        this.eventManager.broadcast({ name: 'callHistoryTypeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-call-history-type-popup',
    template: ''
})
export class CallHistoryTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private callHistoryTypePopupService: CallHistoryTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.callHistoryTypePopupService
                    .open(CallHistoryTypeDialogComponent as Component, params['id']);
            } else {
                this.callHistoryTypePopupService
                    .open(CallHistoryTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
