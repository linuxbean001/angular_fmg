import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { CallHistoryType } from './call-history-type.model';
import { CallHistoryTypeService } from './call-history-type.service';

@Injectable()
export class CallHistoryTypePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private callHistoryTypeService: CallHistoryTypeService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.callHistoryTypeService.find(id)
                    .subscribe((callHistoryTypeResponse: HttpResponse<CallHistoryType>) => {
                        const callHistoryType: CallHistoryType = callHistoryTypeResponse.body;
                        this.ngbModalRef = this.callHistoryTypeModalRef(component, callHistoryType);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.callHistoryTypeModalRef(component, new CallHistoryType());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    callHistoryTypeModalRef(component: Component, callHistoryType: CallHistoryType): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.callHistoryType = callHistoryType;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
