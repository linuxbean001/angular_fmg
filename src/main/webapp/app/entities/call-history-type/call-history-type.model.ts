import {BaseEntity} from '../../shared';
import {NoContact} from '../console/pages/no-contact.component';

export class CallHistoryType implements BaseEntity {
    static SMS = 'SMS';
    static EMAIL = 'EMAIL';
    static CALL = 'CALL';
    static ORGANIZATION_CHANGE = 'ORGANIZATION_CHANGE';
    static CONTACT_CHANGE = 'CONTACT_CHANGE';
    static QA = 'QA';
    static NO_CONTACT_IS_VOICE_MAIL = NoContact.IS_VOICE_MAIL;
    static NO_CONTACT_IS_AWAY = NoContact.IS_AWAY;
    static NO_CONTACT_IS_IN_MEETING = NoContact.IS_IN_MEETING;
    static NO_CONTACT_IS_ENGAGED = NoContact.IS_ENGAGED;

    constructor(
        public id?: number,
        public name?: string,
        public ident?: string,
    ) {
    }
}
