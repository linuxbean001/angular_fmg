import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CmaSharedModule } from '../../shared';
import {
    CallHistoryTypeService,
    CallHistoryTypePopupService,
    CallHistoryTypeComponent,
    CallHistoryTypeDetailComponent,
    CallHistoryTypeDialogComponent,
    CallHistoryTypePopupComponent,
    CallHistoryTypeDeletePopupComponent,
    CallHistoryTypeDeleteDialogComponent,
    callHistoryTypeRoute,
    callHistoryTypePopupRoute,
    CallHistoryTypeResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...callHistoryTypeRoute,
    ...callHistoryTypePopupRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        CallHistoryTypeComponent,
        CallHistoryTypeDetailComponent,
        CallHistoryTypeDialogComponent,
        CallHistoryTypeDeleteDialogComponent,
        CallHistoryTypePopupComponent,
        CallHistoryTypeDeletePopupComponent,
    ],
    entryComponents: [
        CallHistoryTypeComponent,
        CallHistoryTypeDialogComponent,
        CallHistoryTypePopupComponent,
        CallHistoryTypeDeleteDialogComponent,
        CallHistoryTypeDeletePopupComponent,
    ],
    providers: [
        CallHistoryTypeService,
        CallHistoryTypePopupService,
        CallHistoryTypeResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaCallHistoryTypeModule {}
