import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { CallHistoryTypeComponent } from './call-history-type.component';
import { CallHistoryTypeDetailComponent } from './call-history-type-detail.component';
import { CallHistoryTypePopupComponent } from './call-history-type-dialog.component';
import { CallHistoryTypeDeletePopupComponent } from './call-history-type-delete-dialog.component';

@Injectable()
export class CallHistoryTypeResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const callHistoryTypeRoute: Routes = [
    {
        path: 'call-history-type',
        component: CallHistoryTypeComponent,
        resolve: {
            'pagingParams': CallHistoryTypeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.callHistoryType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'call-history-type/:id',
        component: CallHistoryTypeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.callHistoryType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const callHistoryTypePopupRoute: Routes = [
    {
        path: 'call-history-type-new',
        component: CallHistoryTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.callHistoryType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'call-history-type/:id/edit',
        component: CallHistoryTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.callHistoryType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'call-history-type/:id/delete',
        component: CallHistoryTypeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.callHistoryType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
