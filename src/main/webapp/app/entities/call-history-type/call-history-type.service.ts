import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { CallHistoryType } from './call-history-type.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<CallHistoryType>;

@Injectable()
export class CallHistoryTypeService {

    private resourceUrl =  SERVER_API_URL + 'api/call-history-types';

    constructor(private http: HttpClient) { }

    create(callHistoryType: CallHistoryType): Observable<EntityResponseType> {
        const copy = this.convert(callHistoryType);
        return this.http.post<CallHistoryType>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(callHistoryType: CallHistoryType): Observable<EntityResponseType> {
        const copy = this.convert(callHistoryType);
        return this.http.put<CallHistoryType>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<CallHistoryType>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<CallHistoryType[]>> {
        const options = createRequestOption(req);
        return this.http.get<CallHistoryType[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<CallHistoryType[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: CallHistoryType = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<CallHistoryType[]>): HttpResponse<CallHistoryType[]> {
        const jsonResponse: CallHistoryType[] = res.body;
        const body: CallHistoryType[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to CallHistoryType.
     */
    private convertItemFromServer(callHistoryType: CallHistoryType): CallHistoryType {
        const copy: CallHistoryType = Object.assign({}, callHistoryType);
        return copy;
    }

    /**
     * Convert a CallHistoryType to a JSON which can be sent to the server.
     */
    private convert(callHistoryType: CallHistoryType): CallHistoryType {
        const copy: CallHistoryType = Object.assign({}, callHistoryType);
        return copy;
    }
}
