export * from './call-history-type.model';
export * from './call-history-type-popup.service';
export * from './call-history-type.service';
export * from './call-history-type-dialog.component';
export * from './call-history-type-delete-dialog.component';
export * from './call-history-type-detail.component';
export * from './call-history-type.component';
export * from './call-history-type.route';
