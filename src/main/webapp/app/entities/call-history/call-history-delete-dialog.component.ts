import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager} from 'ng-jhipster';

import {CallHistory} from './call-history.model';
import {CallHistoryPopupService} from './call-history-popup.service';
import {CallHistoryService} from './call-history.service';

@Component({
    selector: 'jhi-call-history-delete-dialog',
    templateUrl: './call-history-delete-dialog.component.html'
})
export class CallHistoryDeleteDialogComponent {

    callHistory: CallHistory;

    constructor(
        private callHistoryService: CallHistoryService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.callHistoryService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'callHistoryListModification',
                content: 'Deleted an callHistory'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-call-history-delete-popup',
    template: ''
})
export class CallHistoryDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private callHistoryPopupService: CallHistoryPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.callHistoryPopupService
                .open(CallHistoryDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
