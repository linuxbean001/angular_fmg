import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpResponse} from '@angular/common/http';
import {Subscription} from 'rxjs/Subscription';
import {JhiEventManager} from 'ng-jhipster';

import {CallHistory} from './call-history.model';
import {CallHistoryService} from './call-history.service';

@Component({
    selector: 'jhi-call-history-detail',
    templateUrl: './call-history-detail.component.html'
})
export class CallHistoryDetailComponent implements OnInit, OnDestroy {

    callHistory: CallHistory;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private callHistoryService: CallHistoryService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCallHistories();
    }

    load(id) {
        this.callHistoryService.find(id)
            .subscribe((callHistoryResponse: HttpResponse<CallHistory>) => {
                this.callHistory = callHistoryResponse.body;
            });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCallHistories() {
        this.eventSubscriber = this.eventManager.subscribe(
            'callHistoryListModification',
            (response) => this.load(this.callHistory.id)
        );
    }
}
