import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';

import {CallHistory} from './call-history.model';
import {CallHistoryPopupService} from './call-history-popup.service';
import {CallHistoryService} from './call-history.service';
import {User, UserService} from '../../shared';
import {CallHistoryType, CallHistoryTypeService} from '../call-history-type';
import {ClientList, ClientListService} from '../client-list';

@Component({
    selector: 'jhi-call-history-dialog',
    templateUrl: './call-history-dialog.component.html'
})
export class CallHistoryDialogComponent implements OnInit {

    callHistory: CallHistory;
    isSaving: boolean;

    users: User[];

    callhistorytypes: CallHistoryType[];

    clientlists: ClientList[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private callHistoryService: CallHistoryService,
        private userService: UserService,
        private callHistoryTypeService: CallHistoryTypeService,
        private clientListService: ClientListService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.userService.query()
            .subscribe((res: HttpResponse<User[]>) => {
                this.users = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.callHistoryTypeService.query()
            .subscribe((res: HttpResponse<CallHistoryType[]>) => {
                this.callhistorytypes = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.clientListService.query()
            .subscribe((res: HttpResponse<ClientList[]>) => {
                this.clientlists = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.callHistory.id !== undefined) {
            this.subscribeToSaveResponse(
                this.callHistoryService.update(this.callHistory));
        } else {
            this.subscribeToSaveResponse(
                this.callHistoryService.create(this.callHistory));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<CallHistory>>) {
        result.subscribe((res: HttpResponse<CallHistory>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: CallHistory) {
        this.eventManager.broadcast({name: 'callHistoryListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }

    trackCallHistoryTypeById(index: number, item: CallHistoryType) {
        return item.id;
    }

    trackClientListById(index: number, item: ClientList) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-call-history-popup',
    template: ''
})
export class CallHistoryPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private callHistoryPopupService: CallHistoryPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.callHistoryPopupService
                    .open(CallHistoryDialogComponent as Component, params['id']);
            } else {
                this.callHistoryPopupService
                    .open(CallHistoryDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
