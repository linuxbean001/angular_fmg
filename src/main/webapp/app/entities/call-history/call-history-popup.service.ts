import {Component, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {HttpResponse} from '@angular/common/http';
import {DatePipe} from '@angular/common';
import {CallHistory} from './call-history.model';
import {CallHistoryService} from './call-history.service';

@Injectable()
export class CallHistoryPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private callHistoryService: CallHistoryService
    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.callHistoryService.find(id)
                    .subscribe((callHistoryResponse: HttpResponse<CallHistory>) => {
                        const callHistory: CallHistory = callHistoryResponse.body;
                        callHistory.createdDate = this.datePipe
                            .transform(callHistory.createdDate, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.callHistoryModalRef(component, callHistory);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.callHistoryModalRef(component, new CallHistory());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    callHistoryModalRef(component: Component, callHistory: CallHistory): NgbModalRef {
        const modalRef = this.modalService.open(component, {size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.callHistory = callHistory;
        modalRef.result.then((result) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
