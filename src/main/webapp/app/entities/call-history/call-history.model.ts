import {BaseEntity} from '../../shared';

export class CallHistory implements BaseEntity {
    public id: number;
    public createdDate: any;
    public details: string;
    public note: string;
    public orgRowNum: number;
    public contactRowNum: number;
    public userLogin: string;
    public userId: number;
    public typeName: string;
    public typeId: number;
    /**
     * See {@link CallHistoryType}.
     */
    public typeIdent: string;
    public listName: string;
    public listId: number;
    // ---
    public smsText: string;
    public smsId: number;
    public smsName: string;
    // ---
    public emailTemplateId: number;
    public emailTemplateName: string;
    // ---
    public isNoContactHoldFromCallPool: boolean;
    public noContactStamp: any;

    constructor(callHistory?: Partial<CallHistory>) {
        if (callHistory) {
            Object.assign(this, callHistory);
        }
    }
}
