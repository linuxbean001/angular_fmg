import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {CmaSharedModule} from '../../shared';
import {CmaAdminModule} from '../../admin/admin.module';
import {
    CallHistoryComponent,
    CallHistoryDeleteDialogComponent,
    CallHistoryDeletePopupComponent,
    CallHistoryDetailComponent,
    CallHistoryDialogComponent,
    CallHistoryPopupComponent,
    callHistoryPopupRoute,
    CallHistoryPopupService,
    CallHistoryResolvePagingParams,
    callHistoryRoute,
    CallHistoryService,
} from './';

const ENTITY_STATES = [
    ...callHistoryRoute,
    ...callHistoryPopupRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        CmaAdminModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        CallHistoryComponent,
        CallHistoryDetailComponent,
        CallHistoryDialogComponent,
        CallHistoryDeleteDialogComponent,
        CallHistoryPopupComponent,
        CallHistoryDeletePopupComponent,
    ],
    entryComponents: [
        CallHistoryComponent,
        CallHistoryDialogComponent,
        CallHistoryPopupComponent,
        CallHistoryDeleteDialogComponent,
        CallHistoryDeletePopupComponent,
    ],
    providers: [
        CallHistoryService,
        CallHistoryPopupService,
        CallHistoryResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaCallHistoryModule {
}
