import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { CallHistoryComponent } from './call-history.component';
import { CallHistoryDetailComponent } from './call-history-detail.component';
import { CallHistoryPopupComponent } from './call-history-dialog.component';
import { CallHistoryDeletePopupComponent } from './call-history-delete-dialog.component';

@Injectable()
export class CallHistoryResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const callHistoryRoute: Routes = [
    {
        path: 'call-history',
        component: CallHistoryComponent,
        resolve: {
            'pagingParams': CallHistoryResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.callHistory.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'call-history/:id',
        component: CallHistoryDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.callHistory.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const callHistoryPopupRoute: Routes = [
    {
        path: 'call-history-new',
        component: CallHistoryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.callHistory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'call-history/:id/edit',
        component: CallHistoryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.callHistory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'call-history/:id/delete',
        component: CallHistoryDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.callHistory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
