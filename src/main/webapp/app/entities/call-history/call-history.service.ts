import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';

import {JhiDateUtils} from 'ng-jhipster';

import {CallHistory} from './call-history.model';
import {createRequestOption} from '../../shared';

export type EntityResponseType = HttpResponse<CallHistory>;

@Injectable()
export class CallHistoryService {

    private resourceUrl = SERVER_API_URL + 'api/call-histories';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) {
    }

    create(callHistory: CallHistory): Observable<EntityResponseType> {
        const copy = this.convert(callHistory);
        return this.http.post<CallHistory>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(callHistory: CallHistory): Observable<EntityResponseType> {
        const copy = this.convert(callHistory);
        return this.http.put<CallHistory>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<CallHistory>(`${this.resourceUrl}/${id}`, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<CallHistory[]>> {
        const options = createRequestOption(req);
        return this.http.get<CallHistory[]>(this.resourceUrl, {params: options, observe: 'response'})
            .map((res: HttpResponse<CallHistory[]>) => this.convertArrayResponse(res));
    }

    getCallHistories(listId, orgRowNum: number): Observable<HttpResponse<CallHistory[]>> {
        const url = `${SERVER_API_URL}api/client-lists/${listId}/row-nums/${orgRowNum}/histories`;
        return this.http.get<CallHistory[]>(url, {observe: 'response'});
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: CallHistory = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<CallHistory[]>): HttpResponse<CallHistory[]> {
        const jsonResponse: CallHistory[] = res.body;
        const body: CallHistory[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to CallHistory.
     */
    private convertItemFromServer(callHistory: CallHistory): CallHistory {
        const copy: CallHistory = Object.assign({}, callHistory);
        copy.createdDate = this.dateUtils
            .convertDateTimeFromServer(callHistory.createdDate);
        copy.noContactStamp = this.dateUtils
            .convertDateTimeFromServer(callHistory.noContactStamp);
        return copy;
    }

    /**
     * Convert a CallHistory to a JSON which can be sent to the server.
     */
    private convert(callHistory: CallHistory): CallHistory {
        const copy: CallHistory = Object.assign({}, callHistory);

        copy.createdDate = this.dateUtils.toDate(callHistory.createdDate);
        // parsing something like "2019-09-13", should I worry about timezone (convert to UTC)?
        copy.noContactStamp = callHistory.noContactStamp ? new Date(callHistory.noContactStamp) : null;
        return copy;
    }
}
