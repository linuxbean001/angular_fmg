import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager} from 'ng-jhipster';

import {CampaignListContactManagement} from './campaign-list-contact-management.model';
import {CampaignListContactManagementPopupService} from './campaign-list-contact-management-popup.service';
import {CampaignListContactManagementService} from './campaign-list-contact-management.service';

@Component({
    selector: 'jhi-campaign-list-contact-management-delete-dialog',
    templateUrl: './campaign-list-contact-management-delete-dialog.component.html'
})
export class CampaignListContactManagementDeleteDialogComponent {

    campaignListContactManagement: CampaignListContactManagement;

    constructor(
        private campaignListContactManagementService: CampaignListContactManagementService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.campaignListContactManagementService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'campaignListContactManagementListModification',
                content: 'Deleted an campaignListContactManagement'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-campaign-list-contact-management-delete-popup',
    template: ''
})
export class CampaignListContactManagementDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private campaignListContactManagementPopupService: CampaignListContactManagementPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.campaignListContactManagementPopupService
                .open(CampaignListContactManagementDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
