import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpResponse} from '@angular/common/http';
import {Subscription} from 'rxjs/Subscription';
import {JhiEventManager} from 'ng-jhipster';

import {CampaignListContactManagement} from './campaign-list-contact-management.model';
import {CampaignListContactManagementService} from './campaign-list-contact-management.service';

@Component({
    selector: 'jhi-campaign-list-contact-management-detail',
    templateUrl: './campaign-list-contact-management-detail.component.html'
})
export class CampaignListContactManagementDetailComponent implements OnInit, OnDestroy {

    campaignListContactManagement: CampaignListContactManagement;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private campaignListContactManagementService: CampaignListContactManagementService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCampaignListContactManagements();
    }

    load(id) {
        this.campaignListContactManagementService.find(id)
            .subscribe((campaignListContactManagementResponse: HttpResponse<CampaignListContactManagement>) => {
                this.campaignListContactManagement = campaignListContactManagementResponse.body;
            });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCampaignListContactManagements() {
        this.eventSubscriber = this.eventManager.subscribe(
            'campaignListContactManagementListModification',
            (response) => this.load(this.campaignListContactManagement.id)
        );
    }
}
