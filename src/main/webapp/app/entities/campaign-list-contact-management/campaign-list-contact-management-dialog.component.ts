import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';

import {CampaignListContactManagement} from './campaign-list-contact-management.model';
import {CampaignListContactManagementPopupService} from './campaign-list-contact-management-popup.service';
import {CampaignListContactManagementService} from './campaign-list-contact-management.service';
import {ClientCampaign, ClientCampaignService} from '../client-campaign';
import {ClientList, ClientListService} from '../client-list';

@Component({
    selector: 'jhi-campaign-list-contact-management-dialog',
    templateUrl: './campaign-list-contact-management-dialog.component.html'
})
export class CampaignListContactManagementDialogComponent implements OnInit {

    campaignListContactManagement: CampaignListContactManagement;
    isSaving: boolean;

    clientcampaigns: ClientCampaign[];

    clientlists: ClientList[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private campaignListContactManagementService: CampaignListContactManagementService,
        private clientCampaignService: ClientCampaignService,
        private clientListService: ClientListService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.clientCampaignService.query()
            .subscribe((res: HttpResponse<ClientCampaign[]>) => {
                this.clientcampaigns = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.clientListService.query()
            .subscribe((res: HttpResponse<ClientList[]>) => {
                this.clientlists = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.campaignListContactManagement.id !== undefined) {
            this.subscribeToSaveResponse(
                this.campaignListContactManagementService.update(this.campaignListContactManagement));
        } else {
            this.subscribeToSaveResponse(
                this.campaignListContactManagementService.create(this.campaignListContactManagement));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<CampaignListContactManagement>>) {
        result.subscribe((res: HttpResponse<CampaignListContactManagement>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: CampaignListContactManagement) {
        this.eventManager.broadcast({name: 'campaignListContactManagementListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackClientCampaignById(index: number, item: ClientCampaign) {
        return item.id;
    }

    trackClientListById(index: number, item: ClientList) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-campaign-list-contact-management-popup',
    template: ''
})
export class CampaignListContactManagementPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private campaignListContactManagementPopupService: CampaignListContactManagementPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.campaignListContactManagementPopupService
                    .open(CampaignListContactManagementDialogComponent as Component, params['id']);
            } else {
                this.campaignListContactManagementPopupService
                    .open(CampaignListContactManagementDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
