import {Component, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {HttpResponse} from '@angular/common/http';
import {DatePipe} from '@angular/common';
import {CampaignListContactManagement} from './campaign-list-contact-management.model';
import {CampaignListContactManagementService} from './campaign-list-contact-management.service';

@Injectable()
export class CampaignListContactManagementPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private campaignListContactManagementService: CampaignListContactManagementService
    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.campaignListContactManagementService.find(id)
                    .subscribe((campaignListContactManagementResponse: HttpResponse<CampaignListContactManagement>) => {
                        const campaignListContactManagement: CampaignListContactManagement = campaignListContactManagementResponse.body;
                        campaignListContactManagement.stamp = this.datePipe
                            .transform(campaignListContactManagement.stamp, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.campaignListContactManagementModalRef(component, campaignListContactManagement);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.campaignListContactManagementModalRef(component, new CampaignListContactManagement());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    campaignListContactManagementModalRef(component: Component, campaignListContactManagement: CampaignListContactManagement): NgbModalRef {
        const modalRef = this.modalService.open(component, {size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.campaignListContactManagement = campaignListContactManagement;
        modalRef.result.then((result) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
