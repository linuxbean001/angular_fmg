import {BaseEntity} from '../../shared';

export class CampaignListContactManagement implements BaseEntity {
    constructor(
        public id?: number,
        public contactRowNum?: number,
        public clientCampaignName?: string,
        public clientCampaignId?: number,
        public clientListName?: string,
        public clientListId?: number,
        public holdFromCallPool?: boolean,
        public stamp?: any,
    ) {
        this.holdFromCallPool = false;
    }
}
