import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {CmaSharedModule} from '../../shared';
import {
    CampaignListContactManagementComponent,
    CampaignListContactManagementDeleteDialogComponent,
    CampaignListContactManagementDeletePopupComponent,
    CampaignListContactManagementDetailComponent,
    CampaignListContactManagementDialogComponent,
    CampaignListContactManagementPopupComponent,
    campaignListContactManagementPopupRoute,
    CampaignListContactManagementPopupService,
    CampaignListContactManagementResolvePagingParams,
    campaignListContactManagementRoute,
    CampaignListContactManagementService,
} from './';

const ENTITY_STATES = [
    ...campaignListContactManagementRoute,
    ...campaignListContactManagementPopupRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        CampaignListContactManagementComponent,
        CampaignListContactManagementDetailComponent,
        CampaignListContactManagementDialogComponent,
        CampaignListContactManagementDeleteDialogComponent,
        CampaignListContactManagementPopupComponent,
        CampaignListContactManagementDeletePopupComponent,
    ],
    entryComponents: [
        CampaignListContactManagementComponent,
        CampaignListContactManagementDialogComponent,
        CampaignListContactManagementPopupComponent,
        CampaignListContactManagementDeleteDialogComponent,
        CampaignListContactManagementDeletePopupComponent,
    ],
    providers: [
        CampaignListContactManagementService,
        CampaignListContactManagementPopupService,
        CampaignListContactManagementResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaCampaignListContactManagementModule {
}
