import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes} from '@angular/router';
import {JhiPaginationUtil} from 'ng-jhipster';

import {UserRouteAccessService} from '../../shared';
import {CampaignListContactManagementComponent} from './campaign-list-contact-management.component';
import {CampaignListContactManagementDetailComponent} from './campaign-list-contact-management-detail.component';
import {CampaignListContactManagementPopupComponent} from './campaign-list-contact-management-dialog.component';
import {CampaignListContactManagementDeletePopupComponent} from './campaign-list-contact-management-delete-dialog.component';

@Injectable()
export class CampaignListContactManagementResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const campaignListContactManagementRoute: Routes = [
    {
        path: 'campaign-list-contact-management',
        component: CampaignListContactManagementComponent,
        resolve: {
            'pagingParams': CampaignListContactManagementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.campaignListContactManagement.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'campaign-list-contact-management/:id',
        component: CampaignListContactManagementDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.campaignListContactManagement.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const campaignListContactManagementPopupRoute: Routes = [
    {
        path: 'campaign-list-contact-management-new',
        component: CampaignListContactManagementPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.campaignListContactManagement.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'campaign-list-contact-management/:id/edit',
        component: CampaignListContactManagementPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.campaignListContactManagement.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'campaign-list-contact-management/:id/delete',
        component: CampaignListContactManagementDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.campaignListContactManagement.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
