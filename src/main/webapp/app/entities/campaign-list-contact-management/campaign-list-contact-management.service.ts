import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';

import {JhiDateUtils} from 'ng-jhipster';

import {CampaignListContactManagement} from './campaign-list-contact-management.model';
import {createRequestOption} from '../../shared';
import {DatePipe} from '@angular/common';

export type EntityResponseType = HttpResponse<CampaignListContactManagement>;

@Injectable()
export class CampaignListContactManagementService {

    private resourceUrl = SERVER_API_URL + 'api/campaign-list-contact-managements';

    constructor(
        private http: HttpClient,
        private dateUtils: JhiDateUtils,
        private datePipe: DatePipe,
    ) {
    }

    create(campaignListContactManagement: CampaignListContactManagement): Observable<EntityResponseType> {
        const copy = this.convert(campaignListContactManagement);
        return this.http.post<CampaignListContactManagement>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(campaignListContactManagement: CampaignListContactManagement): Observable<EntityResponseType> {
        const copy = this.convert(campaignListContactManagement);
        return this.http.put<CampaignListContactManagement>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<CampaignListContactManagement>(`${this.resourceUrl}/${id}`, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<CampaignListContactManagement[]>> {
        const options = createRequestOption(req);
        return this.http.get<CampaignListContactManagement[]>(this.resourceUrl, {params: options, observe: 'response'})
            .map((res: HttpResponse<CampaignListContactManagement[]>) => this.convertArrayResponse(res));
    }

    queryByCampaignAndListAndRowNum(clientCampaignId, clientListId, contactRowNum: number): Observable<HttpResponse<CampaignListContactManagement[]>> {
        return this.query({
            'clientCampaignId.equals': clientCampaignId,
            'clientListId.equals': clientListId,
            'contactRowNum.equals': contactRowNum,
        });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: CampaignListContactManagement = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<CampaignListContactManagement[]>): HttpResponse<CampaignListContactManagement[]> {
        const jsonResponse: CampaignListContactManagement[] = res.body;
        const body: CampaignListContactManagement[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to CampaignListContactManagement.
     */
    private convertItemFromServer(campaignListContactManagement: CampaignListContactManagement): CampaignListContactManagement {
        const copy: CampaignListContactManagement = Object.assign({}, campaignListContactManagement);
        // copy.stamp = this.dateUtils
        //     .convertDateTimeFromServer(campaignListContactManagement.stamp);
        copy.stamp = this.datePipe
            .transform(campaignListContactManagement.stamp, 'yyyy-MM-ddTHH:mm:ss');
        return copy;
    }

    /**
     * Convert a CampaignListContactManagement to a JSON which can be sent to the server.
     */
    private convert(campaignListContactManagement: CampaignListContactManagement): CampaignListContactManagement {
        const copy: CampaignListContactManagement = Object.assign({}, campaignListContactManagement);

        // parsing something like "2019-09-13", should I worry about timezone (convert to UTC)?
        copy.stamp = campaignListContactManagement.stamp ? new Date(campaignListContactManagement.stamp) : null;
        // copy.stamp = this.dateUtils.toDate(campaignListContactManagement.stamp);

        return copy;
    }
}
