import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager} from 'ng-jhipster';

import {CampaignListContactQuestionResult} from './campaign-list-contact-question-result.model';
import {CampaignListContactQuestionResultPopupService} from './campaign-list-contact-question-result-popup.service';
import {CampaignListContactQuestionResultService} from './campaign-list-contact-question-result.service';

@Component({
    selector: 'jhi-campaign-list-contact-question-result-delete-dialog',
    templateUrl: './campaign-list-contact-question-result-delete-dialog.component.html'
})
export class CampaignListContactQuestionResultDeleteDialogComponent {

    campaignListContactQuestionResult: CampaignListContactQuestionResult;

    constructor(
        private campaignListContactQuestionResultService: CampaignListContactQuestionResultService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.campaignListContactQuestionResultService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'campaignListContactQuestionResultListModification',
                content: 'Deleted an campaignListContactQuestionResult'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-campaign-list-contact-question-result-delete-popup',
    template: ''
})
export class CampaignListContactQuestionResultDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private campaignListContactQuestionResultPopupService: CampaignListContactQuestionResultPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.campaignListContactQuestionResultPopupService
                .open(CampaignListContactQuestionResultDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
