import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpResponse} from '@angular/common/http';
import {Subscription} from 'rxjs/Subscription';
import {JhiEventManager} from 'ng-jhipster';

import {CampaignListContactQuestionResult} from './campaign-list-contact-question-result.model';
import {CampaignListContactQuestionResultService} from './campaign-list-contact-question-result.service';

@Component({
    selector: 'jhi-campaign-list-contact-question-result-detail',
    templateUrl: './campaign-list-contact-question-result-detail.component.html'
})
export class CampaignListContactQuestionResultDetailComponent implements OnInit, OnDestroy {

    campaignListContactQuestionResult: CampaignListContactQuestionResult;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private campaignListContactQuestionResultService: CampaignListContactQuestionResultService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCampaignListContactQuestionResults();
    }

    load(id) {
        this.campaignListContactQuestionResultService.find(id)
            .subscribe((campaignListContactQuestionResultResponse: HttpResponse<CampaignListContactQuestionResult>) => {
                this.campaignListContactQuestionResult = campaignListContactQuestionResultResponse.body;
            });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCampaignListContactQuestionResults() {
        this.eventSubscriber = this.eventManager.subscribe(
            'campaignListContactQuestionResultListModification',
            (response) => this.load(this.campaignListContactQuestionResult.id)
        );
    }
}
