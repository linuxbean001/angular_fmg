import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';

import {CampaignListContactQuestionResult} from './campaign-list-contact-question-result.model';
import {CampaignListContactQuestionResultPopupService} from './campaign-list-contact-question-result-popup.service';
import {CampaignListContactQuestionResultService} from './campaign-list-contact-question-result.service';
import {ClientCampaign, ClientCampaignService} from '../client-campaign';
import {ClientList, ClientListService} from '../client-list';
import {BookingQuestionField, BookingQuestionFieldService} from '../booking-question-field';

@Component({
    selector: 'jhi-campaign-list-contact-question-result-dialog',
    templateUrl: './campaign-list-contact-question-result-dialog.component.html'
})
export class CampaignListContactQuestionResultDialogComponent implements OnInit {

    campaignListContactQuestionResult: CampaignListContactQuestionResult;
    isSaving: boolean;

    clientcampaigns: ClientCampaign[];

    clientlists: ClientList[];

    bookingquestionfields: BookingQuestionField[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private campaignListContactQuestionResultService: CampaignListContactQuestionResultService,
        private clientCampaignService: ClientCampaignService,
        private clientListService: ClientListService,
        private bookingQuestionFieldService: BookingQuestionFieldService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.clientCampaignService.query()
            .subscribe((res: HttpResponse<ClientCampaign[]>) => {
                this.clientcampaigns = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.clientListService.query()
            .subscribe((res: HttpResponse<ClientList[]>) => {
                this.clientlists = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.bookingQuestionFieldService.query()
            .subscribe((res: HttpResponse<BookingQuestionField[]>) => {
                this.bookingquestionfields = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.campaignListContactQuestionResult.id !== undefined) {
            this.subscribeToSaveResponse(
                this.campaignListContactQuestionResultService.update(this.campaignListContactQuestionResult));
        } else {
            this.subscribeToSaveResponse(
                this.campaignListContactQuestionResultService.create(this.campaignListContactQuestionResult));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<CampaignListContactQuestionResult>>) {
        result.subscribe((res: HttpResponse<CampaignListContactQuestionResult>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: CampaignListContactQuestionResult) {
        this.eventManager.broadcast({name: 'campaignListContactQuestionResultListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackClientCampaignById(index: number, item: ClientCampaign) {
        return item.id;
    }

    trackClientListById(index: number, item: ClientList) {
        return item.id;
    }

    trackBookingQuestionFieldById(index: number, item: BookingQuestionField) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-campaign-list-contact-question-result-popup',
    template: ''
})
export class CampaignListContactQuestionResultPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private campaignListContactQuestionResultPopupService: CampaignListContactQuestionResultPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.campaignListContactQuestionResultPopupService
                    .open(CampaignListContactQuestionResultDialogComponent as Component, params['id']);
            } else {
                this.campaignListContactQuestionResultPopupService
                    .open(CampaignListContactQuestionResultDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
