import {Component, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {HttpResponse} from '@angular/common/http';
import {CampaignListContactQuestionResult} from './campaign-list-contact-question-result.model';
import {CampaignListContactQuestionResultService} from './campaign-list-contact-question-result.service';

@Injectable()
export class CampaignListContactQuestionResultPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private campaignListContactQuestionResultService: CampaignListContactQuestionResultService
    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.campaignListContactQuestionResultService.find(id)
                    .subscribe((campaignListContactQuestionResultResponse: HttpResponse<CampaignListContactQuestionResult>) => {
                        const campaignListContactQuestionResult: CampaignListContactQuestionResult = campaignListContactQuestionResultResponse.body;
                        this.ngbModalRef = this.campaignListContactQuestionResultModalRef(component, campaignListContactQuestionResult);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.campaignListContactQuestionResultModalRef(component, new CampaignListContactQuestionResult());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    campaignListContactQuestionResultModalRef(component: Component,
                                              campaignListContactQuestionResult: CampaignListContactQuestionResult): NgbModalRef {
        const modalRef = this.modalService.open(component, {size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.campaignListContactQuestionResult = campaignListContactQuestionResult;
        modalRef.result.then((result) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
