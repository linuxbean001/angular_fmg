import {BaseEntity} from '../../shared';

export class CampaignListContactQuestionResult implements BaseEntity {
    public id?: number;
    public contactRowNum?: number;
    public cvalue?: string;
    public clientCampaignName?: string;
    public clientCampaignId?: number;
    public clientListName?: string;
    public clientListId?: number;
    public bookingQuestionFieldName?: string;
    public bookingQuestionFieldId?: number;

    constructor(item?: Partial<CampaignListContactQuestionResult>) {
        if (item) {
            Object.assign(this, item);
        }
    }
}
