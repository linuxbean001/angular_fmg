import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {CmaSharedModule} from '../../shared';
import {
    CampaignListContactQuestionResultComponent,
    CampaignListContactQuestionResultDeleteDialogComponent,
    CampaignListContactQuestionResultDeletePopupComponent,
    CampaignListContactQuestionResultDetailComponent,
    CampaignListContactQuestionResultDialogComponent,
    CampaignListContactQuestionResultPopupComponent,
    campaignListContactQuestionResultPopupRoute,
    CampaignListContactQuestionResultPopupService,
    CampaignListContactQuestionResultResolvePagingParams,
    campaignListContactQuestionResultRoute,
    CampaignListContactQuestionResultService,
} from './';

const ENTITY_STATES = [
    ...campaignListContactQuestionResultRoute,
    ...campaignListContactQuestionResultPopupRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        CampaignListContactQuestionResultComponent,
        CampaignListContactQuestionResultDetailComponent,
        CampaignListContactQuestionResultDialogComponent,
        CampaignListContactQuestionResultDeleteDialogComponent,
        CampaignListContactQuestionResultPopupComponent,
        CampaignListContactQuestionResultDeletePopupComponent,
    ],
    entryComponents: [
        CampaignListContactQuestionResultComponent,
        CampaignListContactQuestionResultDialogComponent,
        CampaignListContactQuestionResultPopupComponent,
        CampaignListContactQuestionResultDeleteDialogComponent,
        CampaignListContactQuestionResultDeletePopupComponent,
    ],
    providers: [
        CampaignListContactQuestionResultService,
        CampaignListContactQuestionResultPopupService,
        CampaignListContactQuestionResultResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaCampaignListContactQuestionResultModule {
}
