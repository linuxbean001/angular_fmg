import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes} from '@angular/router';
import {JhiPaginationUtil} from 'ng-jhipster';

import {UserRouteAccessService} from '../../shared';
import {CampaignListContactQuestionResultComponent} from './campaign-list-contact-question-result.component';
import {CampaignListContactQuestionResultDetailComponent} from './campaign-list-contact-question-result-detail.component';
import {CampaignListContactQuestionResultPopupComponent} from './campaign-list-contact-question-result-dialog.component';
import {CampaignListContactQuestionResultDeletePopupComponent} from './campaign-list-contact-question-result-delete-dialog.component';

@Injectable()
export class CampaignListContactQuestionResultResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const campaignListContactQuestionResultRoute: Routes = [
    {
        path: 'campaign-list-contact-question-result',
        component: CampaignListContactQuestionResultComponent,
        resolve: {
            'pagingParams': CampaignListContactQuestionResultResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.campaignListContactQuestionResult.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'campaign-list-contact-question-result/:id',
        component: CampaignListContactQuestionResultDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.campaignListContactQuestionResult.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const campaignListContactQuestionResultPopupRoute: Routes = [
    {
        path: 'campaign-list-contact-question-result-new',
        component: CampaignListContactQuestionResultPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.campaignListContactQuestionResult.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'campaign-list-contact-question-result/:id/edit',
        component: CampaignListContactQuestionResultPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.campaignListContactQuestionResult.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'campaign-list-contact-question-result/:id/delete',
        component: CampaignListContactQuestionResultDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.campaignListContactQuestionResult.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
