import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';

import {CampaignListContactQuestionResult} from './campaign-list-contact-question-result.model';
import {createRequestOption} from '../../shared';

export type EntityResponseType = HttpResponse<CampaignListContactQuestionResult>;

@Injectable()
export class CampaignListContactQuestionResultService {

    private resourceUrl = SERVER_API_URL + 'api/campaign-list-contact-question-results';

    constructor(private http: HttpClient) {
    }

    create(campaignListContactQuestionResult: CampaignListContactQuestionResult):
        Observable<EntityResponseType> {
        const copy = this.convert(campaignListContactQuestionResult);
        return this.http.post<CampaignListContactQuestionResult>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(campaignListContactQuestionResult: CampaignListContactQuestionResult): Observable<EntityResponseType> {
        const copy = this.convert(campaignListContactQuestionResult);
        return this.http.put<CampaignListContactQuestionResult>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    updateAll(campaignListContactQuestionResults: CampaignListContactQuestionResult[]): Observable<HttpResponse<CampaignListContactQuestionResult[]>> {
        const copy = campaignListContactQuestionResults.map((campaignListContactQuestionResult) => this.convert(campaignListContactQuestionResult));
        const url = this.resourceUrl + '-multiple';
        return this.http.put<CampaignListContactQuestionResult[]>(url, copy, {observe: 'response'})
            .map((res: HttpResponse<CampaignListContactQuestionResult[]>) => this.convertArrayResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<CampaignListContactQuestionResult>(`${this.resourceUrl}/${id}`, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<CampaignListContactQuestionResult[]>> {
        const options = createRequestOption(req);
        return this.http.get<CampaignListContactQuestionResult[]>(this.resourceUrl, {
            params: options,
            observe: 'response'
        })
            .map((res: HttpResponse<CampaignListContactQuestionResult[]>) => this.convertArrayResponse(res));
    }

    queryByCampaignAndListAndRowNum(clientCampaignId, clientListId, contactRowNum: number): Observable<HttpResponse<CampaignListContactQuestionResult[]>> {
        return this.query({
            'clientCampaignId.equals': clientCampaignId,
            'clientListId.equals': clientListId,
            'contactRowNum.equals': contactRowNum,
        });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: CampaignListContactQuestionResult = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<CampaignListContactQuestionResult[]>): HttpResponse<CampaignListContactQuestionResult[]> {
        const jsonResponse: CampaignListContactQuestionResult[] = res.body;
        const body: CampaignListContactQuestionResult[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to CampaignListContactQuestionResult.
     */
    private convertItemFromServer(campaignListContactQuestionResult: CampaignListContactQuestionResult): CampaignListContactQuestionResult {
        const copy: CampaignListContactQuestionResult = Object.assign({}, campaignListContactQuestionResult);
        return copy;
    }

    /**
     * Convert a CampaignListContactQuestionResult to a JSON which can be sent to the server.
     */
    private convert(campaignListContactQuestionResult: CampaignListContactQuestionResult): CampaignListContactQuestionResult {
        const copy: CampaignListContactQuestionResult = Object.assign({}, campaignListContactQuestionResult);
        return copy;
    }
}
