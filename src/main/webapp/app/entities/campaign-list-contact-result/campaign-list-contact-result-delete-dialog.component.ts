import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager} from 'ng-jhipster';

import {CampaignListContactResult} from './campaign-list-contact-result.model';
import {CampaignListContactResultPopupService} from './campaign-list-contact-result-popup.service';
import {CampaignListContactResultService} from './campaign-list-contact-result.service';

@Component({
    selector: 'jhi-campaign-list-contact-result-delete-dialog',
    templateUrl: './campaign-list-contact-result-delete-dialog.component.html'
})
export class CampaignListContactResultDeleteDialogComponent {

    campaignListContactResult: CampaignListContactResult;

    constructor(
        private campaignListContactResultService: CampaignListContactResultService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.campaignListContactResultService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'campaignListContactResultListModification',
                content: 'Deleted an campaignListContactResult'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-campaign-list-contact-result-delete-popup',
    template: ''
})
export class CampaignListContactResultDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private campaignListContactResultPopupService: CampaignListContactResultPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.campaignListContactResultPopupService
                .open(CampaignListContactResultDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
