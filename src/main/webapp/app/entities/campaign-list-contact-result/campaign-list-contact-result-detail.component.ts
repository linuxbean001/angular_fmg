import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpResponse} from '@angular/common/http';
import {Subscription} from 'rxjs/Subscription';
import {JhiEventManager} from 'ng-jhipster';

import {CampaignListContactResult} from './campaign-list-contact-result.model';
import {CampaignListContactResultService} from './campaign-list-contact-result.service';

@Component({
    selector: 'jhi-campaign-list-contact-result-detail',
    templateUrl: './campaign-list-contact-result-detail.component.html'
})
export class CampaignListContactResultDetailComponent implements OnInit, OnDestroy {

    campaignListContactResult: CampaignListContactResult;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private campaignListContactResultService: CampaignListContactResultService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCampaignListContactResults();
    }

    load(id) {
        this.campaignListContactResultService.find(id)
            .subscribe((campaignListContactResultResponse: HttpResponse<CampaignListContactResult>) => {
                this.campaignListContactResult = campaignListContactResultResponse.body;
            });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCampaignListContactResults() {
        this.eventSubscriber = this.eventManager.subscribe(
            'campaignListContactResultListModification',
            (response) => this.load(this.campaignListContactResult.id)
        );
    }
}
