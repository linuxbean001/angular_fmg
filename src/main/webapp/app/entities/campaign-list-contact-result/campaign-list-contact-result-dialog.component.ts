import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';

import {CampaignListContactResult} from './campaign-list-contact-result.model';
import {CampaignListContactResultPopupService} from './campaign-list-contact-result-popup.service';
import {CampaignListContactResultService} from './campaign-list-contact-result.service';
import {ClientCampaign, ClientCampaignService} from '../client-campaign';
import {ClientList, ClientListService} from '../client-list';
import {OutcomeSubReason, OutcomeSubReasonService} from '../outcome-sub-reason';

@Component({
    selector: 'jhi-campaign-list-contact-result-dialog',
    templateUrl: './campaign-list-contact-result-dialog.component.html'
})
export class CampaignListContactResultDialogComponent implements OnInit {

    campaignListContactResult: CampaignListContactResult;
    isSaving: boolean;

    clientcampaigns: ClientCampaign[];

    clientlists: ClientList[];

    outcomesubreasons: OutcomeSubReason[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private campaignListContactResultService: CampaignListContactResultService,
        private clientCampaignService: ClientCampaignService,
        private clientListService: ClientListService,
        private outcomeSubReasonService: OutcomeSubReasonService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.clientCampaignService.query()
            .subscribe((res: HttpResponse<ClientCampaign[]>) => {
                this.clientcampaigns = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.clientListService.query()
            .subscribe((res: HttpResponse<ClientList[]>) => {
                this.clientlists = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.outcomeSubReasonService.query()
            .subscribe((res: HttpResponse<OutcomeSubReason[]>) => {
                this.outcomesubreasons = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.campaignListContactResult.id !== undefined) {
            this.subscribeToSaveResponse(
                this.campaignListContactResultService.update(this.campaignListContactResult));
        } else {
            this.subscribeToSaveResponse(
                this.campaignListContactResultService.create(this.campaignListContactResult));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<CampaignListContactResult>>) {
        result.subscribe((res: HttpResponse<CampaignListContactResult>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: CampaignListContactResult) {
        this.eventManager.broadcast({name: 'campaignListContactResultListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackClientCampaignById(index: number, item: ClientCampaign) {
        return item.id;
    }

    trackClientListById(index: number, item: ClientList) {
        return item.id;
    }

    trackOutcomeSubReasonById(index: number, item: OutcomeSubReason) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-campaign-list-contact-result-popup',
    template: ''
})
export class CampaignListContactResultPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private campaignListContactResultPopupService: CampaignListContactResultPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.campaignListContactResultPopupService
                    .open(CampaignListContactResultDialogComponent as Component, params['id']);
            } else {
                this.campaignListContactResultPopupService
                    .open(CampaignListContactResultDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
