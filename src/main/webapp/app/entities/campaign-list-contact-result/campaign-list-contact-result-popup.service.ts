import {Component, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {HttpResponse} from '@angular/common/http';
import {CampaignListContactResult} from './campaign-list-contact-result.model';
import {CampaignListContactResultService} from './campaign-list-contact-result.service';

@Injectable()
export class CampaignListContactResultPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private campaignListContactResultService: CampaignListContactResultService
    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.campaignListContactResultService.find(id)
                    .subscribe((campaignListContactResultResponse: HttpResponse<CampaignListContactResult>) => {
                        const campaignListContactResult: CampaignListContactResult = campaignListContactResultResponse.body;
                        this.ngbModalRef = this.campaignListContactResultModalRef(component, campaignListContactResult);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.campaignListContactResultModalRef(component, new CampaignListContactResult());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    campaignListContactResultModalRef(component: Component, campaignListContactResult: CampaignListContactResult): NgbModalRef {
        const modalRef = this.modalService.open(component, {size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.campaignListContactResult = campaignListContactResult;
        modalRef.result.then((result) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
