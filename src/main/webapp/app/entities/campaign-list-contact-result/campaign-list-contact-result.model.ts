import {BaseEntity} from '../../shared';

export class CampaignListContactResult implements BaseEntity {
    constructor(
        public id?: number,
        public orgRowNum?: number,
        public contactRowNum?: number,
        public clientCampaignName?: string,
        public clientCampaignId?: number,
        public clientListName?: string,
        public clientListId?: number,
        public outcomeSubReasonName?: string,
        public outcomeSubReasonId?: number,
        public isApproved?: boolean,
        public emailIsSent?: boolean,
        public emailTemplateJson?: string,
        public smsIsSent?: boolean,
        /**
         * One of the {@link RecordState#ITEMS}.
         */
        public state?: string,
        // temporary properties, no need to save them, right?
        public sendSMSWasClicked?: boolean,
        public assignUserLogin?: string,
        public assignUserId?: number,
        public assignStamp?: string,
        public clickedOutcomeName?: string,
        public clickedOutcomeId?: number,
    ) {
    }
}

export class AlertElement {
    constructor(
        public dateTime?: Date,
        public label?: string,
        public campaignId?: number,
        public campaignName?: string,
        public listId?: number,
        public listName?: string,
        public contactRowNums?: number[],
    ) {
    }
}

export class RecordState {
    static ACTIVE = new RecordState('Active', 'ACTIVE');
    static COMPLETED = new RecordState('Completed', 'COMPLETED');
    static MY_LIST = new RecordState('My list', 'MY_LIST');
    static PENDING = new RecordState('Pending', 'PENDING');

    static ITEMS: RecordState[] = [
        RecordState.ACTIVE,
        RecordState.COMPLETED,
        RecordState.MY_LIST,
        RecordState.PENDING,
    ];

    public constructor(
        public name: string,
        public ident: string,
    ) {
    }
}

export class ResultEmailState {
    static SEND = new ResultEmailState('Send', 'SEND');
    static SENT = new ResultEmailState('Sent', 'SENT');
    static APPROVAL = new ResultEmailState('Approval', 'APPROVAL');

    public constructor(
        public name: string,
        public ident: string,
    ) {
    }
}
