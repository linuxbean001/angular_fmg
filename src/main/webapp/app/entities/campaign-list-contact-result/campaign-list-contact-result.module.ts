import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {CmaSharedModule} from '../../shared';
import {
    CampaignListContactResultComponent,
    CampaignListContactResultDeleteDialogComponent,
    CampaignListContactResultDeletePopupComponent,
    CampaignListContactResultDetailComponent,
    CampaignListContactResultDialogComponent,
    CampaignListContactResultPopupComponent,
    campaignListContactResultPopupRoute,
    CampaignListContactResultPopupService,
    CampaignListContactResultResolvePagingParams,
    campaignListContactResultRoute,
    CampaignListContactResultService,
} from './';

const ENTITY_STATES = [
    ...campaignListContactResultRoute,
    ...campaignListContactResultPopupRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        CampaignListContactResultComponent,
        CampaignListContactResultDetailComponent,
        CampaignListContactResultDialogComponent,
        CampaignListContactResultDeleteDialogComponent,
        CampaignListContactResultPopupComponent,
        CampaignListContactResultDeletePopupComponent,
    ],
    entryComponents: [
        CampaignListContactResultComponent,
        CampaignListContactResultDialogComponent,
        CampaignListContactResultPopupComponent,
        CampaignListContactResultDeleteDialogComponent,
        CampaignListContactResultDeletePopupComponent,
    ],
    providers: [
        CampaignListContactResultService,
        CampaignListContactResultPopupService,
        CampaignListContactResultResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaCampaignListContactResultModule {
}
