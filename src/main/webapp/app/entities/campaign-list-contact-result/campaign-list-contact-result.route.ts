import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes} from '@angular/router';
import {JhiPaginationUtil} from 'ng-jhipster';

import {UserRouteAccessService} from '../../shared';
import {CampaignListContactResultComponent} from './campaign-list-contact-result.component';
import {CampaignListContactResultDetailComponent} from './campaign-list-contact-result-detail.component';
import {CampaignListContactResultPopupComponent} from './campaign-list-contact-result-dialog.component';
import {CampaignListContactResultDeletePopupComponent} from './campaign-list-contact-result-delete-dialog.component';

@Injectable()
export class CampaignListContactResultResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const campaignListContactResultRoute: Routes = [
    {
        path: 'campaign-list-contact-result',
        component: CampaignListContactResultComponent,
        resolve: {
            'pagingParams': CampaignListContactResultResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.campaignListContactResult.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'campaign-list-contact-result/:id',
        component: CampaignListContactResultDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.campaignListContactResult.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const campaignListContactResultPopupRoute: Routes = [
    {
        path: 'campaign-list-contact-result-new',
        component: CampaignListContactResultPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.campaignListContactResult.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'campaign-list-contact-result/:id/edit',
        component: CampaignListContactResultPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.campaignListContactResult.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'campaign-list-contact-result/:id/delete',
        component: CampaignListContactResultDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.campaignListContactResult.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
