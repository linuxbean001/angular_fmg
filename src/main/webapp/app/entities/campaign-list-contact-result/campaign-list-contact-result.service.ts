import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';

import {AlertElement, CampaignListContactResult} from './campaign-list-contact-result.model';
import {createRequestOption} from '../../shared';

export type EntityResponseType = HttpResponse<CampaignListContactResult>;

@Injectable()
export class CampaignListContactResultService {

    private resourceUrl = SERVER_API_URL + 'api/campaign-list-contact-results';

    constructor(private http: HttpClient) {
    }

    create(campaignListContactResult: CampaignListContactResult): Observable<EntityResponseType> {
        const copy = this.convert(campaignListContactResult);
        return this.http.post<CampaignListContactResult>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(campaignListContactResult: CampaignListContactResult): Observable<EntityResponseType> {
        const copy = this.convert(campaignListContactResult);
        return this.http.put<CampaignListContactResult>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<CampaignListContactResult>(`${this.resourceUrl}/${id}`, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<CampaignListContactResult[]>> {
        const options = createRequestOption(req);
        return this.http.get<CampaignListContactResult[]>(this.resourceUrl, {params: options, observe: 'response'})
            .map((res: HttpResponse<CampaignListContactResult[]>) => this.convertArrayResponse(res));
    }

    queryByCampaignAndListAndRowNum(clientCampaignId, clientListId, contactRowNum: number): Observable<HttpResponse<CampaignListContactResult[]>> {
        return this.query({
            'clientCampaignId.equals': clientCampaignId,
            'clientListId.equals': clientListId,
            'contactRowNum.equals': contactRowNum,
        });
    }

    getAlertsForCurrentUser(): Observable<HttpResponse<AlertElement[]>> {
        return this.http.get<AlertElement[]>(`${SERVER_API_URL}api/current-user-alerts`, {observe: 'response'});
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: CampaignListContactResult = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<CampaignListContactResult[]>): HttpResponse<CampaignListContactResult[]> {
        const jsonResponse: CampaignListContactResult[] = res.body;
        const body: CampaignListContactResult[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to CampaignListContactResult.
     */
    private convertItemFromServer(campaignListContactResult: CampaignListContactResult): CampaignListContactResult {
        const copy: CampaignListContactResult = Object.assign({}, campaignListContactResult);
        return copy;
    }

    /**
     * Convert a CampaignListContactResult to a JSON which can be sent to the server.
     */
    private convert(campaignListContactResult: CampaignListContactResult): CampaignListContactResult {
        const copy: CampaignListContactResult = Object.assign({}, campaignListContactResult);
        return copy;
    }
}
