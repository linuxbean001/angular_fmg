import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CampaignPortalUserOutcome } from './campaign-portal-user-outcome.model';
import { CampaignPortalUserOutcomePopupService } from './campaign-portal-user-outcome-popup.service';
import { CampaignPortalUserOutcomeService } from './campaign-portal-user-outcome.service';

@Component({
    selector: 'jhi-campaign-portal-user-outcome-delete-dialog',
    templateUrl: './campaign-portal-user-outcome-delete-dialog.component.html'
})
export class CampaignPortalUserOutcomeDeleteDialogComponent {

    campaignPortalUserOutcome: CampaignPortalUserOutcome;

    constructor(
        private campaignPortalUserOutcomeService: CampaignPortalUserOutcomeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.campaignPortalUserOutcomeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'campaignPortalUserOutcomeListModification',
                content: 'Deleted an campaignPortalUserOutcome',
                id,
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-campaign-portal-user-outcome-delete-popup',
    template: ''
})
export class CampaignPortalUserOutcomeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private campaignPortalUserOutcomePopupService: CampaignPortalUserOutcomePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.campaignPortalUserOutcomePopupService
                .open(CampaignPortalUserOutcomeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
