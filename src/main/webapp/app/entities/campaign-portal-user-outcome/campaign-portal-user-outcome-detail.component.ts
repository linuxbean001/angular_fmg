import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { CampaignPortalUserOutcome } from './campaign-portal-user-outcome.model';
import { CampaignPortalUserOutcomeService } from './campaign-portal-user-outcome.service';

@Component({
    selector: 'jhi-campaign-portal-user-outcome-detail',
    templateUrl: './campaign-portal-user-outcome-detail.component.html'
})
export class CampaignPortalUserOutcomeDetailComponent implements OnInit, OnDestroy {

    campaignPortalUserOutcome: CampaignPortalUserOutcome;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private campaignPortalUserOutcomeService: CampaignPortalUserOutcomeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCampaignPortalUserOutcomes();
    }

    load(id) {
        this.campaignPortalUserOutcomeService.find(id)
            .subscribe((campaignPortalUserOutcomeResponse: HttpResponse<CampaignPortalUserOutcome>) => {
                this.campaignPortalUserOutcome = campaignPortalUserOutcomeResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCampaignPortalUserOutcomes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'campaignPortalUserOutcomeListModification',
            (response) => this.load(this.campaignPortalUserOutcome.id)
        );
    }
}
