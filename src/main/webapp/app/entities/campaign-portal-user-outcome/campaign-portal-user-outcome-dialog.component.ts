import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CampaignPortalUserOutcome } from './campaign-portal-user-outcome.model';
import { CampaignPortalUserOutcomePopupService } from './campaign-portal-user-outcome-popup.service';
import { CampaignPortalUserOutcomeService } from './campaign-portal-user-outcome.service';
import { CampaignPortalUser, CampaignPortalUserService } from '../campaign-portal-user';
import { Outcome, OutcomeService } from '../outcome';

@Component({
    selector: 'jhi-campaign-portal-user-outcome-dialog',
    templateUrl: './campaign-portal-user-outcome-dialog.component.html'
})
export class CampaignPortalUserOutcomeDialogComponent implements OnInit {

    campaignPortalUserOutcome: CampaignPortalUserOutcome;
    isSaving: boolean;

    campaignportalusers: CampaignPortalUser[];

    outcomes: Outcome[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private campaignPortalUserOutcomeService: CampaignPortalUserOutcomeService,
        private campaignPortalUserService: CampaignPortalUserService,
        private outcomeService: OutcomeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.campaignPortalUserService.query()
            .subscribe((res: HttpResponse<CampaignPortalUser[]>) => { this.campaignportalusers = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.outcomeService.query()
            .subscribe((res: HttpResponse<Outcome[]>) => { this.outcomes = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.campaignPortalUserOutcome.id !== undefined) {
            this.subscribeToSaveResponse(
                this.campaignPortalUserOutcomeService.update(this.campaignPortalUserOutcome));
        } else {
            this.subscribeToSaveResponse(
                this.campaignPortalUserOutcomeService.create(this.campaignPortalUserOutcome));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<CampaignPortalUserOutcome>>) {
        result.subscribe((res: HttpResponse<CampaignPortalUserOutcome>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: CampaignPortalUserOutcome) {
        this.eventManager.broadcast({ name: 'campaignPortalUserOutcomeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackCampaignPortalUserById(index: number, item: CampaignPortalUser) {
        return item.id;
    }

    trackOutcomeById(index: number, item: Outcome) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-campaign-portal-user-outcome-popup',
    template: ''
})
export class CampaignPortalUserOutcomePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private campaignPortalUserOutcomePopupService: CampaignPortalUserOutcomePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.campaignPortalUserOutcomePopupService
                    .open(CampaignPortalUserOutcomeDialogComponent as Component, params['id']);
            } else {
                this.campaignPortalUserOutcomePopupService
                    .open(CampaignPortalUserOutcomeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
