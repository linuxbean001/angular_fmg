import {Component, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {HttpResponse} from '@angular/common/http';
import {CampaignPortalUserOutcome} from './campaign-portal-user-outcome.model';
import {CampaignPortalUserOutcomeService} from './campaign-portal-user-outcome.service';
import {CampaignPortalUser} from '../campaign-portal-user';
import {Outcome} from '../outcome';
import {CampaignPortalUserOutcomeSummaryComponent} from './campaign-portal-user-outcome-summary.component';

@Injectable()
export class CampaignPortalUserOutcomePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private campaignPortalUserOutcomeService: CampaignPortalUserOutcomeService
    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.campaignPortalUserOutcomeService.find(id)
                    .subscribe((campaignPortalUserOutcomeResponse: HttpResponse<CampaignPortalUserOutcome>) => {
                        const campaignPortalUserOutcome: CampaignPortalUserOutcome = campaignPortalUserOutcomeResponse.body;
                        this.ngbModalRef = this.campaignPortalUserOutcomeModalRef(component, campaignPortalUserOutcome);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.campaignPortalUserOutcomeModalRef(component, new CampaignPortalUserOutcome());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    campaignPortalUserOutcomeModalRef(component: Component, campaignPortalUserOutcome: CampaignPortalUserOutcome): NgbModalRef {
        const modalRef = this.modalService.open(component, {size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.campaignPortalUserOutcome = campaignPortalUserOutcome;
        modalRef.result.then((result) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        });
        return modalRef;
    }

    openSummary(component: Component,
                outcomes: Outcome[],
                campaignPortalUser: CampaignPortalUser): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
            setTimeout(() => {
                this.ngbModalRef = this.campaignPortalUserOutcomeSummaryModalRef(component, outcomes, campaignPortalUser);
                resolve(this.ngbModalRef);
            }, 0);
        });
    }

    campaignPortalUserOutcomeSummaryModalRef(component: Component,
                                             outcomes: Outcome[],
                                             campaignPortalUser: CampaignPortalUser): NgbModalRef {
        const modalRef = this.modalService.open(component, {size: 'lg', backdrop: 'static'});
        const summaryComponent = modalRef.componentInstance as CampaignPortalUserOutcomeSummaryComponent;
        summaryComponent.outcomes = outcomes;
        summaryComponent.campaignPortalUser = campaignPortalUser;
        // modalRef.componentInstance.outcomes = Outcome[];
        // modalRef.componentInstance.campaignPortalUser = campaignPortalUser;
        modalRef.result.then((result) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
