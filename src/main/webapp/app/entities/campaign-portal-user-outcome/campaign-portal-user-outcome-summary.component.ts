import {Component, OnInit} from '@angular/core';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';

import {CampaignPortalUserOutcome} from './campaign-portal-user-outcome.model';
import {CampaignPortalUserOutcomeService} from './campaign-portal-user-outcome.service';
import {CampaignPortalUser} from '../campaign-portal-user';
import {Outcome} from '../outcome';

@Component({
    selector: 'jhi-campaign-portal-user-outcome-summary',
    templateUrl: './campaign-portal-user-outcome-summary.component.html'
})
export class CampaignPortalUserOutcomeSummaryComponent implements OnInit {

    outcomes: Outcome[];
    campaignPortalUser: CampaignPortalUser;
    campaignPortalUserOutcomes: CampaignPortalUserOutcome[] = [];

    isSaving = false;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private campaignPortalUserOutcomeService: CampaignPortalUserOutcomeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        if (this.campaignPortalUser.id) {
            const req = {
                'campaignPortalUserId.equals': this.campaignPortalUser.id
            };

            this.campaignPortalUserOutcomeService.query(req)
                .subscribe((res: HttpResponse<CampaignPortalUser[]>) => {
                    this.campaignPortalUserOutcomes = res.body;
                }, (res: HttpErrorResponse) => this.onError(res.message));
        } else {
            this.campaignPortalUserOutcomes = [];
        }
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;

        if (this.campaignPortalUserOutcomes.length) {
            this.campaignPortalUserOutcomes
                .forEach((campaignPortalUserOutcome) => {
                    if (campaignPortalUserOutcome.id) {
                        this.subscribeToSaveResponse(
                            this.campaignPortalUserOutcomeService.update(campaignPortalUserOutcome));
                    } else {
                        this.subscribeToSaveResponse(
                            this.campaignPortalUserOutcomeService.create(campaignPortalUserOutcome));
                    }
                });
        } else {
            this.isSaving = false;
            this.activeModal.close(null);
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<CampaignPortalUserOutcome>>) {
        result.subscribe((res: HttpResponse<CampaignPortalUserOutcome>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: CampaignPortalUserOutcome) {
        this.eventManager.broadcast({name: 'campaignPortalUserOutcomeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.close(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    getPortalUserOutcome(outcomeId: number): CampaignPortalUserOutcome {
        return this.campaignPortalUserOutcomes
            .find((x) => x.outcomeId === outcomeId);
    }

    getOrCreatePortalUserOutcome(outcomeId: number): CampaignPortalUserOutcome {
        const campaignPortalUserOutcome = this.getPortalUserOutcome(outcomeId);
        if (campaignPortalUserOutcome) {
            return campaignPortalUserOutcome;
        }

        const newCampaignPortalUserOutcome = new CampaignPortalUserOutcome();
        newCampaignPortalUserOutcome.outcomeId = outcomeId;
        newCampaignPortalUserOutcome.campaignPortalUserId = this.campaignPortalUser.id;
        this.campaignPortalUserOutcomes.push(newCampaignPortalUserOutcome);
        return newCampaignPortalUserOutcome;
    }

    getEmailImmediately(outcome: Outcome) {
        const campaignPortalUserOutcome = this.getPortalUserOutcome(outcome.id);
        // TODO
        return campaignPortalUserOutcome && campaignPortalUserOutcome.emailImmediately; // || outcome.emailImmediately;
    }

    setEmailImmediately(outcome: Outcome, $event) {
        const campaignPortalUserOutcome = this.getOrCreatePortalUserOutcome(outcome.id);
        campaignPortalUserOutcome.emailImmediately = $event;
    }

    getEmailDaily(outcome: Outcome) {
        const campaignPortalUserOutcome = this.getPortalUserOutcome(outcome.id);
        // TODO
        return campaignPortalUserOutcome && campaignPortalUserOutcome.emailDaily; // || outcome.emailDaily;
    }

    setEmailDaily(outcome: Outcome, $event) {
        const campaignPortalUserOutcome = this.getOrCreatePortalUserOutcome(outcome.id);
        campaignPortalUserOutcome.emailDaily = $event;
    }
}
