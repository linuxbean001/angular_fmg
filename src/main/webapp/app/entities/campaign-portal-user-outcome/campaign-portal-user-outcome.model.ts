import {BaseEntity} from './../../shared';

export class CampaignPortalUserOutcome implements BaseEntity {
    constructor(
        public id?: number,
        public campaignPortalUserEmail?: string,
        public campaignPortalUserId?: number,
        public outcomeName?: string,
        public outcomeId?: number,
        public emailImmediately?: boolean,
        public emailDaily?: boolean,
    ) {
    }
}
