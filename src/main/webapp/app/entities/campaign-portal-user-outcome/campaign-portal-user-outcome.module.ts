import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {CmaSharedModule} from '../../shared';
import {
    CampaignPortalUserOutcomeComponent,
    CampaignPortalUserOutcomeDeleteDialogComponent,
    CampaignPortalUserOutcomeDeletePopupComponent,
    CampaignPortalUserOutcomeDetailComponent,
    CampaignPortalUserOutcomeDialogComponent,
    CampaignPortalUserOutcomePopupComponent,
    campaignPortalUserOutcomePopupRoute,
    CampaignPortalUserOutcomePopupService,
    CampaignPortalUserOutcomeResolvePagingParams,
    campaignPortalUserOutcomeRoute,
    CampaignPortalUserOutcomeService,
} from './';
import {CampaignPortalUserOutcomeSummaryComponent} from './campaign-portal-user-outcome-summary.component';

const ENTITY_STATES = [
    ...campaignPortalUserOutcomeRoute,
    ...campaignPortalUserOutcomePopupRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        CampaignPortalUserOutcomeComponent,
        CampaignPortalUserOutcomeDetailComponent,
        CampaignPortalUserOutcomeDialogComponent,
        CampaignPortalUserOutcomeSummaryComponent,
        CampaignPortalUserOutcomeDeleteDialogComponent,
        CampaignPortalUserOutcomePopupComponent,
        CampaignPortalUserOutcomeDeletePopupComponent,
    ],
    entryComponents: [
        CampaignPortalUserOutcomeComponent,
        CampaignPortalUserOutcomeDialogComponent,
        CampaignPortalUserOutcomeSummaryComponent,
        CampaignPortalUserOutcomePopupComponent,
        CampaignPortalUserOutcomeDeleteDialogComponent,
        CampaignPortalUserOutcomeDeletePopupComponent,
    ],
    providers: [
        CampaignPortalUserOutcomeService,
        CampaignPortalUserOutcomePopupService,
        CampaignPortalUserOutcomeResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaCampaignPortalUserOutcomeModule {
}
