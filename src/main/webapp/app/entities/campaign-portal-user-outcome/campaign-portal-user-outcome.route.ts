import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { CampaignPortalUserOutcomeComponent } from './campaign-portal-user-outcome.component';
import { CampaignPortalUserOutcomeDetailComponent } from './campaign-portal-user-outcome-detail.component';
import { CampaignPortalUserOutcomePopupComponent } from './campaign-portal-user-outcome-dialog.component';
import { CampaignPortalUserOutcomeDeletePopupComponent } from './campaign-portal-user-outcome-delete-dialog.component';

@Injectable()
export class CampaignPortalUserOutcomeResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const campaignPortalUserOutcomeRoute: Routes = [
    {
        path: 'campaign-portal-user-outcome',
        component: CampaignPortalUserOutcomeComponent,
        resolve: {
            'pagingParams': CampaignPortalUserOutcomeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.campaignPortalUserOutcome.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'campaign-portal-user-outcome/:id',
        component: CampaignPortalUserOutcomeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.campaignPortalUserOutcome.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const campaignPortalUserOutcomePopupRoute: Routes = [
    {
        path: 'campaign-portal-user-outcome-new',
        component: CampaignPortalUserOutcomePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.campaignPortalUserOutcome.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'campaign-portal-user-outcome/:id/edit',
        component: CampaignPortalUserOutcomePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.campaignPortalUserOutcome.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'campaign-portal-user-outcome/:id/delete',
        component: CampaignPortalUserOutcomeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.campaignPortalUserOutcome.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
