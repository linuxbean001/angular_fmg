import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { CampaignPortalUserOutcome } from './campaign-portal-user-outcome.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<CampaignPortalUserOutcome>;

@Injectable()
export class CampaignPortalUserOutcomeService {

    private resourceUrl =  SERVER_API_URL + 'api/campaign-portal-user-outcomes';

    constructor(private http: HttpClient) { }

    create(campaignPortalUserOutcome: CampaignPortalUserOutcome): Observable<EntityResponseType> {
        const copy = this.convert(campaignPortalUserOutcome);
        return this.http.post<CampaignPortalUserOutcome>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(campaignPortalUserOutcome: CampaignPortalUserOutcome): Observable<EntityResponseType> {
        const copy = this.convert(campaignPortalUserOutcome);
        return this.http.put<CampaignPortalUserOutcome>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<CampaignPortalUserOutcome>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<CampaignPortalUserOutcome[]>> {
        const options = createRequestOption(req);
        return this.http.get<CampaignPortalUserOutcome[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<CampaignPortalUserOutcome[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: CampaignPortalUserOutcome = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<CampaignPortalUserOutcome[]>): HttpResponse<CampaignPortalUserOutcome[]> {
        const jsonResponse: CampaignPortalUserOutcome[] = res.body;
        const body: CampaignPortalUserOutcome[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to CampaignPortalUserOutcome.
     */
    private convertItemFromServer(campaignPortalUserOutcome: CampaignPortalUserOutcome): CampaignPortalUserOutcome {
        const copy: CampaignPortalUserOutcome = Object.assign({}, campaignPortalUserOutcome);
        return copy;
    }

    /**
     * Convert a CampaignPortalUserOutcome to a JSON which can be sent to the server.
     */
    private convert(campaignPortalUserOutcome: CampaignPortalUserOutcome): CampaignPortalUserOutcome {
        const copy: CampaignPortalUserOutcome = Object.assign({}, campaignPortalUserOutcome);
        return copy;
    }
}
