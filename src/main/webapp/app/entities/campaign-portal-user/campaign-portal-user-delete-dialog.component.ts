import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CampaignPortalUser } from './campaign-portal-user.model';
import { CampaignPortalUserPopupService } from './campaign-portal-user-popup.service';
import { CampaignPortalUserService } from './campaign-portal-user.service';

@Component({
    selector: 'jhi-campaign-portal-user-delete-dialog',
    templateUrl: './campaign-portal-user-delete-dialog.component.html'
})
export class CampaignPortalUserDeleteDialogComponent {

    campaignPortalUser: CampaignPortalUser;

    constructor(
        private campaignPortalUserService: CampaignPortalUserService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.campaignPortalUserService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'campaignPortalUserListModification',
                content: 'Deleted an campaignPortalUser',
                id,
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-campaign-portal-user-delete-popup',
    template: ''
})
export class CampaignPortalUserDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private campaignPortalUserPopupService: CampaignPortalUserPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.campaignPortalUserPopupService
                .open(CampaignPortalUserDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
