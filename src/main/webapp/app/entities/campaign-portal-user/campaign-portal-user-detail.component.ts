import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { CampaignPortalUser } from './campaign-portal-user.model';
import { CampaignPortalUserService } from './campaign-portal-user.service';

@Component({
    selector: 'jhi-campaign-portal-user-detail',
    templateUrl: './campaign-portal-user-detail.component.html'
})
export class CampaignPortalUserDetailComponent implements OnInit, OnDestroy {

    campaignPortalUser: CampaignPortalUser;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private campaignPortalUserService: CampaignPortalUserService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCampaignPortalUsers();
    }

    load(id) {
        this.campaignPortalUserService.find(id)
            .subscribe((campaignPortalUserResponse: HttpResponse<CampaignPortalUser>) => {
                this.campaignPortalUser = campaignPortalUserResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCampaignPortalUsers() {
        this.eventSubscriber = this.eventManager.subscribe(
            'campaignPortalUserListModification',
            (response) => this.load(this.campaignPortalUser.id)
        );
    }
}
