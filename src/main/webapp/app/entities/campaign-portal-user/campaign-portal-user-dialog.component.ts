import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CampaignPortalUser } from './campaign-portal-user.model';
import { CampaignPortalUserPopupService } from './campaign-portal-user-popup.service';
import { CampaignPortalUserService } from './campaign-portal-user.service';
import { ClientCampaign, ClientCampaignService } from '../client-campaign';
import { User, UserService } from '../../shared';

@Component({
    selector: 'jhi-campaign-portal-user-dialog',
    templateUrl: './campaign-portal-user-dialog.component.html'
})
export class CampaignPortalUserDialogComponent implements OnInit {

    campaignPortalUser: CampaignPortalUser;
    isSaving: boolean;

    clientcampaigns: ClientCampaign[];

    users: User[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private campaignPortalUserService: CampaignPortalUserService,
        private clientCampaignService: ClientCampaignService,
        private userService: UserService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.clientCampaignService.query()
            .subscribe((res: HttpResponse<ClientCampaign[]>) => { this.clientcampaigns = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.userService.query()
            .subscribe((res: HttpResponse<User[]>) => { this.users = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.campaignPortalUser.id !== undefined) {
            this.subscribeToSaveResponse(
                this.campaignPortalUserService.update(this.campaignPortalUser));
        } else {
            this.subscribeToSaveResponse(
                this.campaignPortalUserService.create(this.campaignPortalUser));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<CampaignPortalUser>>) {
        result.subscribe((res: HttpResponse<CampaignPortalUser>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: CampaignPortalUser) {
        this.eventManager.broadcast({ name: 'campaignPortalUserListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackClientCampaignById(index: number, item: ClientCampaign) {
        return item.id;
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-campaign-portal-user-popup',
    template: ''
})
export class CampaignPortalUserPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private campaignPortalUserPopupService: CampaignPortalUserPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.campaignPortalUserPopupService
                    .open(CampaignPortalUserDialogComponent as Component, params['id']);
            } else {
                this.campaignPortalUserPopupService
                    .open(CampaignPortalUserDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
