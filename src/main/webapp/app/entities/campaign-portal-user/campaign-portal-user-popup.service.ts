import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { CampaignPortalUser } from './campaign-portal-user.model';
import { CampaignPortalUserService } from './campaign-portal-user.service';

@Injectable()
export class CampaignPortalUserPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private campaignPortalUserService: CampaignPortalUserService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.campaignPortalUserService.find(id)
                    .subscribe((campaignPortalUserResponse: HttpResponse<CampaignPortalUser>) => {
                        const campaignPortalUser: CampaignPortalUser = campaignPortalUserResponse.body;
                        this.ngbModalRef = this.campaignPortalUserModalRef(component, campaignPortalUser);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.campaignPortalUserModalRef(component, new CampaignPortalUser());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    campaignPortalUserModalRef(component: Component, campaignPortalUser: CampaignPortalUser): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.campaignPortalUser = campaignPortalUser;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
