import {BaseEntity} from './../../shared';

export class CampaignPortalUser implements BaseEntity {
    constructor(
        public id?: number,
        public email?: string,
        public clientCampaignName?: string,
        public clientCampaignId?: number,
        public userLogin?: string,
        public userId?: number,
    ) {
    }
}

// this is not an Entity!
export class CampaignPortalUserSummary {
    constructor(
        public emailImmediately?: boolean,
        public emailDaily?: boolean,
    ) {
    }
}
