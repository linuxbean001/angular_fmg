import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CmaSharedModule } from '../../shared';
import { CmaAdminModule } from '../../admin/admin.module';
import {
    CampaignPortalUserService,
    CampaignPortalUserPopupService,
    CampaignPortalUserComponent,
    CampaignPortalUserDetailComponent,
    CampaignPortalUserDialogComponent,
    CampaignPortalUserPopupComponent,
    CampaignPortalUserDeletePopupComponent,
    CampaignPortalUserDeleteDialogComponent,
    campaignPortalUserRoute,
    campaignPortalUserPopupRoute,
    CampaignPortalUserResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...campaignPortalUserRoute,
    ...campaignPortalUserPopupRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        CmaAdminModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        CampaignPortalUserComponent,
        CampaignPortalUserDetailComponent,
        CampaignPortalUserDialogComponent,
        CampaignPortalUserDeleteDialogComponent,
        CampaignPortalUserPopupComponent,
        CampaignPortalUserDeletePopupComponent,
    ],
    entryComponents: [
        CampaignPortalUserComponent,
        CampaignPortalUserDialogComponent,
        CampaignPortalUserPopupComponent,
        CampaignPortalUserDeleteDialogComponent,
        CampaignPortalUserDeletePopupComponent,
    ],
    providers: [
        CampaignPortalUserService,
        CampaignPortalUserPopupService,
        CampaignPortalUserResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaCampaignPortalUserModule {}
