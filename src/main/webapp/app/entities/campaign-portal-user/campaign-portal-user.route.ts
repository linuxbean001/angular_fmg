import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { CampaignPortalUserComponent } from './campaign-portal-user.component';
import { CampaignPortalUserDetailComponent } from './campaign-portal-user-detail.component';
import { CampaignPortalUserPopupComponent } from './campaign-portal-user-dialog.component';
import { CampaignPortalUserDeletePopupComponent } from './campaign-portal-user-delete-dialog.component';

@Injectable()
export class CampaignPortalUserResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const campaignPortalUserRoute: Routes = [
    {
        path: 'campaign-portal-user',
        component: CampaignPortalUserComponent,
        resolve: {
            'pagingParams': CampaignPortalUserResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.campaignPortalUser.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'campaign-portal-user/:id',
        component: CampaignPortalUserDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.campaignPortalUser.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const campaignPortalUserPopupRoute: Routes = [
    {
        path: 'campaign-portal-user-new',
        component: CampaignPortalUserPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.campaignPortalUser.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'campaign-portal-user/:id/edit',
        component: CampaignPortalUserPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.campaignPortalUser.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'campaign-portal-user/:id/delete',
        component: CampaignPortalUserDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.campaignPortalUser.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
