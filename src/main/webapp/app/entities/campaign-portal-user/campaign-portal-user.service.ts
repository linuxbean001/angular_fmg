import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';

import {CampaignPortalUser, CampaignPortalUserSummary} from './campaign-portal-user.model';
import {createRequestOption} from '../../shared';

export type EntityResponseType = HttpResponse<CampaignPortalUser>;

@Injectable()
export class CampaignPortalUserService {

    private resourceUrl = SERVER_API_URL + 'api/campaign-portal-users';

    constructor(private http: HttpClient) {
    }

    create(campaignPortalUser: CampaignPortalUser): Observable<EntityResponseType> {
        const copy = this.convert(campaignPortalUser);
        return this.http.post<CampaignPortalUser>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(campaignPortalUser: CampaignPortalUser): Observable<EntityResponseType> {
        const copy = this.convert(campaignPortalUser);
        return this.http.put<CampaignPortalUser>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<CampaignPortalUser>(`${this.resourceUrl}/${id}`, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    getSummary(id: number): Observable<HttpResponse<CampaignPortalUserSummary>> {
        return this.http.get<CampaignPortalUserSummary>(`${this.resourceUrl}/${id}/summary`, {observe: 'response'});
    }

    query(req?: any): Observable<HttpResponse<CampaignPortalUser[]>> {
        const options = createRequestOption(req);
        return this.http.get<CampaignPortalUser[]>(this.resourceUrl, {params: options, observe: 'response'})
            .map((res: HttpResponse<CampaignPortalUser[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: CampaignPortalUser = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<CampaignPortalUser[]>): HttpResponse<CampaignPortalUser[]> {
        const jsonResponse: CampaignPortalUser[] = res.body;
        const body: CampaignPortalUser[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to CampaignPortalUser.
     */
    private convertItemFromServer(campaignPortalUser: CampaignPortalUser): CampaignPortalUser {
        const copy: CampaignPortalUser = Object.assign({}, campaignPortalUser);
        return copy;
    }

    /**
     * Convert a CampaignPortalUser to a JSON which can be sent to the server.
     */
    private convert(campaignPortalUser: CampaignPortalUser): CampaignPortalUser {
        const copy: CampaignPortalUser = Object.assign({}, campaignPortalUser);
        return copy;
    }
}
