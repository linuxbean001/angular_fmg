import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ClientCampaignAddDoc } from './client-campaign-add-doc.model';
import { ClientCampaignAddDocPopupService } from './client-campaign-add-doc-popup.service';
import { ClientCampaignAddDocService } from './client-campaign-add-doc.service';

@Component({
    selector: 'jhi-client-campaign-add-doc-delete-dialog',
    templateUrl: './client-campaign-add-doc-delete-dialog.component.html'
})
export class ClientCampaignAddDocDeleteDialogComponent {

    clientCampaignAddDoc: ClientCampaignAddDoc;

    constructor(
        private clientCampaignAddDocService: ClientCampaignAddDocService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.clientCampaignAddDocService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'clientCampaignAddDocListModification',
                content: 'Deleted an clientCampaignAddDoc'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-client-campaign-add-doc-delete-popup',
    template: ''
})
export class ClientCampaignAddDocDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientCampaignAddDocPopupService: ClientCampaignAddDocPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.clientCampaignAddDocPopupService
                .open(ClientCampaignAddDocDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
