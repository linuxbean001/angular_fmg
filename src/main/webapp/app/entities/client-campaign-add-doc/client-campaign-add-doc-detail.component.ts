import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { ClientCampaignAddDoc } from './client-campaign-add-doc.model';
import { ClientCampaignAddDocService } from './client-campaign-add-doc.service';

@Component({
    selector: 'jhi-client-campaign-add-doc-detail',
    templateUrl: './client-campaign-add-doc-detail.component.html'
})
export class ClientCampaignAddDocDetailComponent implements OnInit, OnDestroy {

    clientCampaignAddDoc: ClientCampaignAddDoc;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private clientCampaignAddDocService: ClientCampaignAddDocService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInClientCampaignAddDocs();
    }

    load(id) {
        this.clientCampaignAddDocService.find(id)
            .subscribe((clientCampaignAddDocResponse: HttpResponse<ClientCampaignAddDoc>) => {
                this.clientCampaignAddDoc = clientCampaignAddDocResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInClientCampaignAddDocs() {
        this.eventSubscriber = this.eventManager.subscribe(
            'clientCampaignAddDocListModification',
            (response) => this.load(this.clientCampaignAddDoc.id)
        );
    }
}
