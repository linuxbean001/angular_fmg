import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ClientCampaignAddDoc } from './client-campaign-add-doc.model';
import { ClientCampaignAddDocPopupService } from './client-campaign-add-doc-popup.service';
import { ClientCampaignAddDocService } from './client-campaign-add-doc.service';
import { ClientCampaign, ClientCampaignService } from '../client-campaign';

@Component({
    selector: 'jhi-client-campaign-add-doc-dialog',
    templateUrl: './client-campaign-add-doc-dialog.component.html'
})
export class ClientCampaignAddDocDialogComponent implements OnInit {

    clientCampaignAddDoc: ClientCampaignAddDoc;
    isSaving: boolean;

    clientcampaigns: ClientCampaign[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private clientCampaignAddDocService: ClientCampaignAddDocService,
        private clientCampaignService: ClientCampaignService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.clientCampaignService.query()
            .subscribe((res: HttpResponse<ClientCampaign[]>) => { this.clientcampaigns = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.clientCampaignAddDoc.id !== undefined) {
            this.subscribeToSaveResponse(
                this.clientCampaignAddDocService.update(this.clientCampaignAddDoc));
        } else {
            this.subscribeToSaveResponse(
                this.clientCampaignAddDocService.create(this.clientCampaignAddDoc));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ClientCampaignAddDoc>>) {
        result.subscribe((res: HttpResponse<ClientCampaignAddDoc>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ClientCampaignAddDoc) {
        this.eventManager.broadcast({ name: 'clientCampaignAddDocListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackClientCampaignById(index: number, item: ClientCampaign) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-client-campaign-add-doc-popup',
    template: ''
})
export class ClientCampaignAddDocPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientCampaignAddDocPopupService: ClientCampaignAddDocPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.clientCampaignAddDocPopupService
                    .open(ClientCampaignAddDocDialogComponent as Component, params['id']);
            } else {
                this.clientCampaignAddDocPopupService
                    .open(ClientCampaignAddDocDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
