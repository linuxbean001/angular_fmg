import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { ClientCampaignAddDoc } from './client-campaign-add-doc.model';
import { ClientCampaignAddDocService } from './client-campaign-add-doc.service';

@Injectable()
export class ClientCampaignAddDocPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private clientCampaignAddDocService: ClientCampaignAddDocService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.clientCampaignAddDocService.find(id)
                    .subscribe((clientCampaignAddDocResponse: HttpResponse<ClientCampaignAddDoc>) => {
                        const clientCampaignAddDoc: ClientCampaignAddDoc = clientCampaignAddDocResponse.body;
                        this.ngbModalRef = this.clientCampaignAddDocModalRef(component, clientCampaignAddDoc);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.clientCampaignAddDocModalRef(component, new ClientCampaignAddDoc());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    clientCampaignAddDocModalRef(component: Component, clientCampaignAddDoc: ClientCampaignAddDoc): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.clientCampaignAddDoc = clientCampaignAddDoc;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
