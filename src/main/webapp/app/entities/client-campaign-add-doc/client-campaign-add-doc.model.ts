import {BaseEntity} from '../../shared';

export class ClientCampaignAddDoc implements BaseEntity {
    constructor(
        public id?: number,
        public link?: string,
        public text?: string,
        public clientCampaignId?: number,
        public clientCampaignName?: string,
    ) {
    }
}
