import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CmaSharedModule } from '../../shared';
import {
    ClientCampaignAddDocService,
    ClientCampaignAddDocPopupService,
    ClientCampaignAddDocComponent,
    ClientCampaignAddDocDetailComponent,
    ClientCampaignAddDocDialogComponent,
    ClientCampaignAddDocPopupComponent,
    ClientCampaignAddDocDeletePopupComponent,
    ClientCampaignAddDocDeleteDialogComponent,
    clientCampaignAddDocRoute,
    clientCampaignAddDocPopupRoute,
    ClientCampaignAddDocResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...clientCampaignAddDocRoute,
    ...clientCampaignAddDocPopupRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ClientCampaignAddDocComponent,
        ClientCampaignAddDocDetailComponent,
        ClientCampaignAddDocDialogComponent,
        ClientCampaignAddDocDeleteDialogComponent,
        ClientCampaignAddDocPopupComponent,
        ClientCampaignAddDocDeletePopupComponent,
    ],
    entryComponents: [
        ClientCampaignAddDocComponent,
        ClientCampaignAddDocDialogComponent,
        ClientCampaignAddDocPopupComponent,
        ClientCampaignAddDocDeleteDialogComponent,
        ClientCampaignAddDocDeletePopupComponent,
    ],
    providers: [
        ClientCampaignAddDocService,
        ClientCampaignAddDocPopupService,
        ClientCampaignAddDocResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaClientCampaignAddDocModule {}
