import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { ClientCampaignAddDocComponent } from './client-campaign-add-doc.component';
import { ClientCampaignAddDocDetailComponent } from './client-campaign-add-doc-detail.component';
import { ClientCampaignAddDocPopupComponent } from './client-campaign-add-doc-dialog.component';
import { ClientCampaignAddDocDeletePopupComponent } from './client-campaign-add-doc-delete-dialog.component';

@Injectable()
export class ClientCampaignAddDocResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const clientCampaignAddDocRoute: Routes = [
    {
        path: 'client-campaign-add-doc',
        component: ClientCampaignAddDocComponent,
        resolve: {
            'pagingParams': ClientCampaignAddDocResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCampaignAddDoc.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'client-campaign-add-doc/:id',
        component: ClientCampaignAddDocDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCampaignAddDoc.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const clientCampaignAddDocPopupRoute: Routes = [
    {
        path: 'client-campaign-add-doc-new',
        component: ClientCampaignAddDocPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCampaignAddDoc.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-campaign-add-doc/:id/edit',
        component: ClientCampaignAddDocPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCampaignAddDoc.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-campaign-add-doc/:id/delete',
        component: ClientCampaignAddDocDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCampaignAddDoc.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
