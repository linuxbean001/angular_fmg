import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';

import {ClientCampaignAddDoc} from './client-campaign-add-doc.model';
import {createRequestOption} from '../../shared';

export type EntityResponseType = HttpResponse<ClientCampaignAddDoc>;

@Injectable()
export class ClientCampaignAddDocService {

    private resourceUrl = SERVER_API_URL + 'api/client-campaign-add-docs';

    constructor(private http: HttpClient) {
    }

    create(clientCampaignAddDoc: ClientCampaignAddDoc): Observable<EntityResponseType> {
        const copy = this.convert(clientCampaignAddDoc);
        return this.http.post<ClientCampaignAddDoc>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(clientCampaignAddDoc: ClientCampaignAddDoc): Observable<EntityResponseType> {
        const copy = this.convert(clientCampaignAddDoc);
        return this.http.put<ClientCampaignAddDoc>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ClientCampaignAddDoc>(`${this.resourceUrl}/${id}`, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ClientCampaignAddDoc[]>> {
        const options = createRequestOption(req);
        return this.http.get<ClientCampaignAddDoc[]>(this.resourceUrl, {params: options, observe: 'response'})
            .map((res: HttpResponse<ClientCampaignAddDoc[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ClientCampaignAddDoc = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ClientCampaignAddDoc[]>): HttpResponse<ClientCampaignAddDoc[]> {
        const jsonResponse: ClientCampaignAddDoc[] = res.body;
        const body: ClientCampaignAddDoc[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ClientCampaignAddDoc.
     */
    private convertItemFromServer(clientCampaignAddDoc: ClientCampaignAddDoc): ClientCampaignAddDoc {
        const copy: ClientCampaignAddDoc = Object.assign({}, clientCampaignAddDoc);
        return copy;
    }

    /**
     * Convert a ClientCampaignAddDoc to a JSON which can be sent to the server.
     */
    private convert(clientCampaignAddDoc: ClientCampaignAddDoc): ClientCampaignAddDoc {
        const copy: ClientCampaignAddDoc = Object.assign({}, clientCampaignAddDoc);
        return copy;
    }
}
