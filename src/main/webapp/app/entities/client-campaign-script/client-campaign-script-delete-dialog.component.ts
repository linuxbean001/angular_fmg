import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager} from 'ng-jhipster';

import {ClientCampaignScript} from './client-campaign-script.model';
import {ClientCampaignScriptPopupService} from './client-campaign-script-popup.service';
import {ClientCampaignScriptService} from './client-campaign-script.service';

@Component({
    selector: 'jhi-client-campaign-script-delete-dialog',
    templateUrl: './client-campaign-script-delete-dialog.component.html'
})
export class ClientCampaignScriptDeleteDialogComponent {

    clientCampaignScript: ClientCampaignScript;

    constructor(
        private clientCampaignScriptService: ClientCampaignScriptService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.clientCampaignScriptService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'clientCampaignScriptListModification',
                content: 'Deleted an clientCampaignScript',
                id
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-client-campaign-script-delete-popup',
    template: ''
})
export class ClientCampaignScriptDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientCampaignScriptPopupService: ClientCampaignScriptPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.clientCampaignScriptPopupService
                .open(ClientCampaignScriptDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
