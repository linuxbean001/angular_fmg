import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { ClientCampaignScript } from './client-campaign-script.model';
import { ClientCampaignScriptService } from './client-campaign-script.service';

@Component({
    selector: 'jhi-client-campaign-script-detail',
    templateUrl: './client-campaign-script-detail.component.html'
})
export class ClientCampaignScriptDetailComponent implements OnInit, OnDestroy {

    clientCampaignScript: ClientCampaignScript;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private clientCampaignScriptService: ClientCampaignScriptService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInClientCampaignScripts();
    }

    load(id) {
        this.clientCampaignScriptService.find(id)
            .subscribe((clientCampaignScriptResponse: HttpResponse<ClientCampaignScript>) => {
                this.clientCampaignScript = clientCampaignScriptResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInClientCampaignScripts() {
        this.eventSubscriber = this.eventManager.subscribe(
            'clientCampaignScriptListModification',
            (response) => this.load(this.clientCampaignScript.id)
        );
    }
}
