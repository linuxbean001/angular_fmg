import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';

import {ClientCampaignScript} from './client-campaign-script.model';
import {ClientCampaignScriptPopupService} from './client-campaign-script-popup.service';
import {ClientCampaignScriptService} from './client-campaign-script.service';

import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {ClientCampaign, ClientCampaignService} from '../client-campaign';
import {CKEditor5} from '@ckeditor/ckeditor5-angular';
import MyCustomUploadAdapterPlugin from '../email-template/MyCustomUploadAdapterPlugin';

@Component({
    selector: 'jhi-client-campaign-script-dialog',
    templateUrl: './client-campaign-script-dialog.component.html'
})
export class ClientCampaignScriptDialogComponent implements OnInit {

    public Editor = ClassicEditor;
    public editorConfig: CKEditor5.Config = {
        extraPlugins: [MyCustomUploadAdapterPlugin]
    };

    clientCampaignScript: ClientCampaignScript;
    isSaving: boolean;

    clientCampaigns: ClientCampaign[];

    constructor(
        public activeModal: NgbActiveModal,
        private clientCampaignScriptService: ClientCampaignScriptService,
        private eventManager: JhiEventManager,
        private jhiAlertService: JhiAlertService,
        private clientCampaignService: ClientCampaignService,
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.clientCampaignService.query()
            .subscribe((res: HttpResponse<ClientCampaign[]>) => {
                this.clientCampaigns = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.clientCampaignScript.id !== undefined) {
            this.subscribeToSaveResponse(
                this.clientCampaignScriptService.update(this.clientCampaignScript));
        } else {
            this.subscribeToSaveResponse(
                this.clientCampaignScriptService.create(this.clientCampaignScript));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ClientCampaignScript>>) {
        result.subscribe((res: HttpResponse<ClientCampaignScript>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ClientCampaignScript) {
        this.eventManager.broadcast({
            name: 'clientCampaignScriptListModification',
            content: 'OK',
            script: result
        });
        this.isSaving = false;
        this.activeModal.close(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    getExcerpt() {
        return this.clientCampaignScript.excerpt || '';
    }

    setExcerpt($event) {
        this.clientCampaignScript.excerpt = $event;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackClientCampaignById(index: number, item: ClientCampaign) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-client-campaign-script-popup',
    template: ''
})
export class ClientCampaignScriptPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientCampaignScriptPopupService: ClientCampaignScriptPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.clientCampaignScriptPopupService
                    .open(ClientCampaignScriptDialogComponent as Component, params['id']);
            } else {
                this.clientCampaignScriptPopupService
                    .open(ClientCampaignScriptDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
