import {Component, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {HttpResponse} from '@angular/common/http';
import {ClientCampaignScript} from './client-campaign-script.model';
import {ClientCampaignScriptService} from './client-campaign-script.service';

@Injectable()
export class ClientCampaignScriptPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private clientCampaignScriptService: ClientCampaignScriptService
    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.clientCampaignScriptService.find(id)
                    .subscribe((clientCampaignScriptResponse: HttpResponse<ClientCampaignScript>) => {
                        const clientCampaignScript: ClientCampaignScript = clientCampaignScriptResponse.body;
                        this.ngbModalRef = this.clientCampaignScriptModalRef(component, clientCampaignScript);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.clientCampaignScriptModalRef(component, new ClientCampaignScript());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    openStandalone(component: Component, clientCampaignScript: ClientCampaignScript): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            this.ngbModalRef = this.clientCampaignScriptModalRef(component, clientCampaignScript);
            resolve(this.ngbModalRef);
        });
    }

    clientCampaignScriptModalRef(component: Component, clientCampaignScript: ClientCampaignScript): NgbModalRef {
        const modalRef = this.modalService.open(component, {size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.clientCampaignScript = clientCampaignScript;
        modalRef.result.then((result) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
