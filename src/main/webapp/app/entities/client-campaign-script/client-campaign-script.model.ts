import {BaseEntity} from './../../shared';

export class ClientCampaignScript implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public excerpt?: string,
        public clientCampaignName?: string,
        public clientCampaignId?: number,
    ) {
    }
}
