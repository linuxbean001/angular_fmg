import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {CmaSharedModule} from '../../shared';
import {
    ClientCampaignScriptComponent,
    ClientCampaignScriptDeleteDialogComponent,
    ClientCampaignScriptDeletePopupComponent,
    ClientCampaignScriptDetailComponent,
    ClientCampaignScriptDialogComponent,
    ClientCampaignScriptPopupComponent,
    clientCampaignScriptPopupRoute,
    ClientCampaignScriptPopupService,
    ClientCampaignScriptResolvePagingParams,
    clientCampaignScriptRoute,
    ClientCampaignScriptService,
} from './';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';

const ENTITY_STATES = [
    ...clientCampaignScriptRoute,
    ...clientCampaignScriptPopupRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        CKEditorModule
    ],
    declarations: [
        ClientCampaignScriptComponent,
        ClientCampaignScriptDetailComponent,
        ClientCampaignScriptDialogComponent,
        ClientCampaignScriptDeleteDialogComponent,
        ClientCampaignScriptPopupComponent,
        ClientCampaignScriptDeletePopupComponent,
    ],
    entryComponents: [
        ClientCampaignScriptComponent,
        ClientCampaignScriptDialogComponent,
        ClientCampaignScriptPopupComponent,
        ClientCampaignScriptDeleteDialogComponent,
        ClientCampaignScriptDeletePopupComponent,
    ],
    providers: [
        ClientCampaignScriptService,
        ClientCampaignScriptPopupService,
        ClientCampaignScriptResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaClientCampaignScriptModule {
}
