import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes} from '@angular/router';
import {JhiPaginationUtil} from 'ng-jhipster';

import {UserRouteAccessService} from '../../shared';
import {ClientCampaignScriptComponent} from './client-campaign-script.component';
import {ClientCampaignScriptDetailComponent} from './client-campaign-script-detail.component';
import {ClientCampaignScriptPopupComponent} from './client-campaign-script-dialog.component';
import {ClientCampaignScriptDeletePopupComponent} from './client-campaign-script-delete-dialog.component';

@Injectable()
export class ClientCampaignScriptResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const clientCampaignScriptRoute: Routes = [
    {
        path: 'client-campaign-script',
        component: ClientCampaignScriptComponent,
        resolve: {
            'pagingParams': ClientCampaignScriptResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCampaignScript.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'client-campaign-script/:id',
        component: ClientCampaignScriptDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCampaignScript.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const clientCampaignScriptPopupRoute: Routes = [
    {
        path: 'client-campaign-script-new',
        component: ClientCampaignScriptPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCampaignScript.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-campaign-script/:id/edit',
        component: ClientCampaignScriptPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCampaignScript.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-campaign-script/:id/delete',
        component: ClientCampaignScriptDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCampaignScript.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
