import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';

import {ClientCampaignScript} from './client-campaign-script.model';
import {createRequestOption} from '../../shared';

export type EntityResponseType = HttpResponse<ClientCampaignScript>;

@Injectable()
export class ClientCampaignScriptService {

    private resourceUrl = SERVER_API_URL + 'api/client-campaign-scripts';

    constructor(private http: HttpClient) {
    }

    create(clientCampaignScript: ClientCampaignScript): Observable<EntityResponseType> {
        const copy = this.convert(clientCampaignScript);
        return this.http.post<ClientCampaignScript>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(clientCampaignScript: ClientCampaignScript): Observable<EntityResponseType> {
        const copy = this.convert(clientCampaignScript);
        return this.http.put<ClientCampaignScript>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ClientCampaignScript>(`${this.resourceUrl}/${id}`, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ClientCampaignScript[]>> {
        const options = createRequestOption(req);
        return this.http.get<ClientCampaignScript[]>(this.resourceUrl, {params: options, observe: 'response'})
            .map((res: HttpResponse<ClientCampaignScript[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ClientCampaignScript = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ClientCampaignScript[]>): HttpResponse<ClientCampaignScript[]> {
        const jsonResponse: ClientCampaignScript[] = res.body;
        const body: ClientCampaignScript[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ClientCampaignScript.
     */
    private convertItemFromServer(clientCampaignScript: ClientCampaignScript): ClientCampaignScript {
        const copy: ClientCampaignScript = Object.assign({}, clientCampaignScript);
        return copy;
    }

    /**
     * Convert a ClientCampaignScript to a JSON which can be sent to the server.
     */
    private convert(clientCampaignScript: ClientCampaignScript): ClientCampaignScript {
        const copy: ClientCampaignScript = Object.assign({}, clientCampaignScript);
        return copy;
    }
}
