import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ClientCampaignSms } from './client-campaign-sms.model';
import { ClientCampaignSmsPopupService } from './client-campaign-sms-popup.service';
import { ClientCampaignSmsService } from './client-campaign-sms.service';

@Component({
    selector: 'jhi-client-campaign-sms-delete-dialog',
    templateUrl: './client-campaign-sms-delete-dialog.component.html'
})
export class ClientCampaignSmsDeleteDialogComponent {

    clientCampaignSms: ClientCampaignSms;

    constructor(
        private clientCampaignSmsService: ClientCampaignSmsService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.clientCampaignSmsService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'clientCampaignSmsListModification',
                content: 'Deleted an clientCampaignSms',
                id,
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-client-campaign-sms-delete-popup',
    template: ''
})
export class ClientCampaignSmsDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientCampaignSmsPopupService: ClientCampaignSmsPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.clientCampaignSmsPopupService
                .open(ClientCampaignSmsDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
