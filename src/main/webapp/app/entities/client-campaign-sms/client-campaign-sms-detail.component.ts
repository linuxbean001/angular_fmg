import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { ClientCampaignSms } from './client-campaign-sms.model';
import { ClientCampaignSmsService } from './client-campaign-sms.service';

@Component({
    selector: 'jhi-client-campaign-sms-detail',
    templateUrl: './client-campaign-sms-detail.component.html'
})
export class ClientCampaignSmsDetailComponent implements OnInit, OnDestroy {

    clientCampaignSms: ClientCampaignSms;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private clientCampaignSmsService: ClientCampaignSmsService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInClientCampaignSms();
    }

    load(id) {
        this.clientCampaignSmsService.find(id)
            .subscribe((clientCampaignSmsResponse: HttpResponse<ClientCampaignSms>) => {
                this.clientCampaignSms = clientCampaignSmsResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInClientCampaignSms() {
        this.eventSubscriber = this.eventManager.subscribe(
            'clientCampaignSmsListModification',
            (response) => this.load(this.clientCampaignSms.id)
        );
    }
}
