import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';

import {ClientCampaignSms} from './client-campaign-sms.model';
import {ClientCampaignSmsPopupService} from './client-campaign-sms-popup.service';
import {ClientCampaignSmsService} from './client-campaign-sms.service';
import {ClientCampaign, ClientCampaignService} from '../client-campaign';

@Component({
    selector: 'jhi-client-campaign-sms-dialog',
    templateUrl: './client-campaign-sms-dialog.component.html'
})
export class ClientCampaignSmsDialogComponent implements OnInit {

    clientCampaignSms: ClientCampaignSms;
    isSaving: boolean;

    clientCampaigns: ClientCampaign[];

    constructor(
        public activeModal: NgbActiveModal,
        private clientCampaignSmsService: ClientCampaignSmsService,
        private eventManager: JhiEventManager,
        private jhiAlertService: JhiAlertService,
        private clientCampaignService: ClientCampaignService,
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.clientCampaignService.query()
            .subscribe((res: HttpResponse<ClientCampaign[]>) => {
                this.clientCampaigns = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.clientCampaignSms.id !== undefined) {
            this.subscribeToSaveResponse(
                this.clientCampaignSmsService.update(this.clientCampaignSms));
        } else {
            this.subscribeToSaveResponse(
                this.clientCampaignSmsService.create(this.clientCampaignSms));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ClientCampaignSms>>) {
        result.subscribe((res: HttpResponse<ClientCampaignSms>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ClientCampaignSms) {
        this.eventManager.broadcast({
            name: 'clientCampaignSmsListModification',
            content: 'OK',
            sms: result,
        });
        this.isSaving = false;
        this.activeModal.close(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackClientCampaignById(index: number, item: ClientCampaign) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-client-campaign-sms-popup',
    template: ''
})
export class ClientCampaignSmsPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientCampaignSmsPopupService: ClientCampaignSmsPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.clientCampaignSmsPopupService
                    .open(ClientCampaignSmsDialogComponent as Component, params['id']);
            } else {
                this.clientCampaignSmsPopupService
                    .open(ClientCampaignSmsDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
