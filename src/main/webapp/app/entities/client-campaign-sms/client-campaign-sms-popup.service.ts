import {Component, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {HttpResponse} from '@angular/common/http';
import {ClientCampaignSms} from './client-campaign-sms.model';
import {ClientCampaignSmsService} from './client-campaign-sms.service';

@Injectable()
export class ClientCampaignSmsPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private clientCampaignSmsService: ClientCampaignSmsService
    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.clientCampaignSmsService.find(id)
                    .subscribe((clientCampaignSmsResponse: HttpResponse<ClientCampaignSms>) => {
                        const clientCampaignSms: ClientCampaignSms = clientCampaignSmsResponse.body;
                        this.ngbModalRef = this.clientCampaignSmsModalRef(component, clientCampaignSms);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.clientCampaignSmsModalRef(component, new ClientCampaignSms());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    openStandalone(component: Component, clientCampaignSms: ClientCampaignSms): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            this.ngbModalRef = this.clientCampaignSmsModalRef(component, clientCampaignSms);
            resolve(this.ngbModalRef);
        });
    }

    clientCampaignSmsModalRef(component: Component, clientCampaignSms: ClientCampaignSms): NgbModalRef {
        const modalRef = this.modalService.open(component, {size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.clientCampaignSms = clientCampaignSms;
        modalRef.result.then((result) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
