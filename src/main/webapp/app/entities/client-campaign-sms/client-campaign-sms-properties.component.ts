import {Component, Input, OnInit} from '@angular/core';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';

import {ClientCampaignSms, ClientCampaignSmsDialogComponentMode} from './client-campaign-sms.model';
import {ClientCampaignSmsService} from './client-campaign-sms.service';
import {ClientCampaign, ClientCampaignService} from '../client-campaign';
import {NgForm} from '@angular/forms';

@Component({
    selector: 'jhi-client-campaign-sms-properties',
    templateUrl: './client-campaign-sms-properties.component.html'
})
export class ClientCampaignSmsPropertiesComponent implements OnInit {

    @Input() editForm: NgForm;
    @Input() clientCampaignSms: ClientCampaignSms;
    @Input() mode = ClientCampaignSmsDialogComponentMode.CREATE_OR_EDIT;
    ClientCampaignSmsDialogComponentMode: typeof ClientCampaignSmsDialogComponentMode = ClientCampaignSmsDialogComponentMode;
    isSaving: boolean;
    clientCampaigns: ClientCampaign[];

    constructor(
        private clientCampaignSmsService: ClientCampaignSmsService,
        private eventManager: JhiEventManager,
        private jhiAlertService: JhiAlertService,
        private clientCampaignService: ClientCampaignService,
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.clientCampaignService.query()
            .subscribe((res: HttpResponse<ClientCampaign[]>) => {
                this.clientCampaigns = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    trackClientCampaignById(index: number, item: ClientCampaign) {
        return item.id;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
