import {BaseEntity} from '../../shared';

export class ClientCampaignSms implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public excerpt?: string,
        public clientCampaignName?: string,
        public clientCampaignId?: number,
    ) {
    }
}

export class ClientCampaignSmsDialogComponentMode {
    /**
     * This is default mode.
     */
    static CREATE_OR_EDIT = new ClientCampaignSmsDialogComponentMode();

    /**
     * This mode is used in Console to display the {@link EmailTemplate#to} property.
     */
    static SEND = new ClientCampaignSmsDialogComponentMode();
}
