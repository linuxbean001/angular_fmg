import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CmaSharedModule } from '../../shared';
import {
    ClientCampaignSmsService,
    ClientCampaignSmsPopupService,
    ClientCampaignSmsComponent,
    ClientCampaignSmsDetailComponent,
    ClientCampaignSmsDialogComponent,
    ClientCampaignSmsPopupComponent,
    ClientCampaignSmsDeletePopupComponent,
    ClientCampaignSmsDeleteDialogComponent,
    clientCampaignSmsRoute,
    clientCampaignSmsPopupRoute,
    ClientCampaignSmsResolvePagingParams,
} from './';
import {ClientCampaignSmsPropertiesComponent} from './client-campaign-sms-properties.component';

const ENTITY_STATES = [
    ...clientCampaignSmsRoute,
    ...clientCampaignSmsPopupRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ClientCampaignSmsComponent,
        ClientCampaignSmsDetailComponent,
        ClientCampaignSmsDialogComponent,
        ClientCampaignSmsDeleteDialogComponent,
        ClientCampaignSmsPopupComponent,
        ClientCampaignSmsDeletePopupComponent,
        ClientCampaignSmsPropertiesComponent,
    ],
    entryComponents: [
        ClientCampaignSmsComponent,
        ClientCampaignSmsDialogComponent,
        ClientCampaignSmsPopupComponent,
        ClientCampaignSmsDeleteDialogComponent,
        ClientCampaignSmsDeletePopupComponent,
        ClientCampaignSmsPropertiesComponent,
    ],
    providers: [
        ClientCampaignSmsService,
        ClientCampaignSmsPopupService,
        ClientCampaignSmsResolvePagingParams,
    ],
    exports: [
        ClientCampaignSmsPropertiesComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaClientCampaignSmsModule {}
