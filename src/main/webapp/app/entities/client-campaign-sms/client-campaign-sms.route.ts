import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { ClientCampaignSmsComponent } from './client-campaign-sms.component';
import { ClientCampaignSmsDetailComponent } from './client-campaign-sms-detail.component';
import { ClientCampaignSmsPopupComponent } from './client-campaign-sms-dialog.component';
import { ClientCampaignSmsDeletePopupComponent } from './client-campaign-sms-delete-dialog.component';

@Injectable()
export class ClientCampaignSmsResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const clientCampaignSmsRoute: Routes = [
    {
        path: 'client-campaign-sms',
        component: ClientCampaignSmsComponent,
        resolve: {
            'pagingParams': ClientCampaignSmsResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCampaignSms.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'client-campaign-sms/:id',
        component: ClientCampaignSmsDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCampaignSms.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const clientCampaignSmsPopupRoute: Routes = [
    {
        path: 'client-campaign-sms-new',
        component: ClientCampaignSmsPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCampaignSms.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-campaign-sms/:id/edit',
        component: ClientCampaignSmsPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCampaignSms.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-campaign-sms/:id/delete',
        component: ClientCampaignSmsDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCampaignSms.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
