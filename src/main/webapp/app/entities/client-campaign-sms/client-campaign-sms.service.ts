import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { ClientCampaignSms } from './client-campaign-sms.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<ClientCampaignSms>;

@Injectable()
export class ClientCampaignSmsService {

    private resourceUrl =  SERVER_API_URL + 'api/client-campaign-sms';

    constructor(private http: HttpClient) { }

    create(clientCampaignSms: ClientCampaignSms): Observable<EntityResponseType> {
        const copy = this.convert(clientCampaignSms);
        return this.http.post<ClientCampaignSms>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(clientCampaignSms: ClientCampaignSms): Observable<EntityResponseType> {
        const copy = this.convert(clientCampaignSms);
        return this.http.put<ClientCampaignSms>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ClientCampaignSms>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ClientCampaignSms[]>> {
        const options = createRequestOption(req);
        return this.http.get<ClientCampaignSms[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ClientCampaignSms[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ClientCampaignSms = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ClientCampaignSms[]>): HttpResponse<ClientCampaignSms[]> {
        const jsonResponse: ClientCampaignSms[] = res.body;
        const body: ClientCampaignSms[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ClientCampaignSms.
     */
    private convertItemFromServer(clientCampaignSms: ClientCampaignSms): ClientCampaignSms {
        const copy: ClientCampaignSms = Object.assign({}, clientCampaignSms);
        return copy;
    }

    /**
     * Convert a ClientCampaignSms to a JSON which can be sent to the server.
     */
    private convert(clientCampaignSms: ClientCampaignSms): ClientCampaignSms {
        const copy: ClientCampaignSms = Object.assign({}, clientCampaignSms);
        return copy;
    }
}
