import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ClientCampaignTemplate } from './client-campaign-template.model';
import { ClientCampaignTemplatePopupService } from './client-campaign-template-popup.service';
import { ClientCampaignTemplateService } from './client-campaign-template.service';

@Component({
    selector: 'jhi-client-campaign-template-delete-dialog',
    templateUrl: './client-campaign-template-delete-dialog.component.html'
})
export class ClientCampaignTemplateDeleteDialogComponent {

    clientCampaignTemplate: ClientCampaignTemplate;

    constructor(
        private clientCampaignTemplateService: ClientCampaignTemplateService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.clientCampaignTemplateService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'clientCampaignTemplateListModification',
                content: 'Deleted an clientCampaignTemplate',
                id,
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-client-campaign-template-delete-popup',
    template: ''
})
export class ClientCampaignTemplateDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientCampaignTemplatePopupService: ClientCampaignTemplatePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.clientCampaignTemplatePopupService
                .open(ClientCampaignTemplateDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
