import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { ClientCampaignTemplate } from './client-campaign-template.model';
import { ClientCampaignTemplateService } from './client-campaign-template.service';

@Component({
    selector: 'jhi-client-campaign-template-detail',
    templateUrl: './client-campaign-template-detail.component.html'
})
export class ClientCampaignTemplateDetailComponent implements OnInit, OnDestroy {

    clientCampaignTemplate: ClientCampaignTemplate;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private clientCampaignTemplateService: ClientCampaignTemplateService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInClientCampaignTemplates();
    }

    load(id) {
        this.clientCampaignTemplateService.find(id)
            .subscribe((clientCampaignTemplateResponse: HttpResponse<ClientCampaignTemplate>) => {
                this.clientCampaignTemplate = clientCampaignTemplateResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInClientCampaignTemplates() {
        this.eventSubscriber = this.eventManager.subscribe(
            'clientCampaignTemplateListModification',
            (response) => this.load(this.clientCampaignTemplate.id)
        );
    }
}
