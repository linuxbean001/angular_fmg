import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ClientCampaignTemplate } from './client-campaign-template.model';
import { ClientCampaignTemplatePopupService } from './client-campaign-template-popup.service';
import { ClientCampaignTemplateService } from './client-campaign-template.service';

@Component({
    selector: 'jhi-client-campaign-template-dialog',
    templateUrl: './client-campaign-template-dialog.component.html'
})
export class ClientCampaignTemplateDialogComponent implements OnInit {

    clientCampaignTemplate: ClientCampaignTemplate;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private clientCampaignTemplateService: ClientCampaignTemplateService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.clientCampaignTemplate.id !== undefined) {
            this.subscribeToSaveResponse(
                this.clientCampaignTemplateService.update(this.clientCampaignTemplate));
        } else {
            this.subscribeToSaveResponse(
                this.clientCampaignTemplateService.create(this.clientCampaignTemplate));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ClientCampaignTemplate>>) {
        result.subscribe((res: HttpResponse<ClientCampaignTemplate>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ClientCampaignTemplate) {
        this.eventManager.broadcast({ name: 'clientCampaignTemplateListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-client-campaign-template-popup',
    template: ''
})
export class ClientCampaignTemplatePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientCampaignTemplatePopupService: ClientCampaignTemplatePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.clientCampaignTemplatePopupService
                    .open(ClientCampaignTemplateDialogComponent as Component, params['id']);
            } else {
                this.clientCampaignTemplatePopupService
                    .open(ClientCampaignTemplateDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
