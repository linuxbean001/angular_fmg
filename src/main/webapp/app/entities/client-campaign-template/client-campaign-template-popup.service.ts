import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { ClientCampaignTemplate } from './client-campaign-template.model';
import { ClientCampaignTemplateService } from './client-campaign-template.service';

@Injectable()
export class ClientCampaignTemplatePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private clientCampaignTemplateService: ClientCampaignTemplateService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.clientCampaignTemplateService.find(id)
                    .subscribe((clientCampaignTemplateResponse: HttpResponse<ClientCampaignTemplate>) => {
                        const clientCampaignTemplate: ClientCampaignTemplate = clientCampaignTemplateResponse.body;
                        this.ngbModalRef = this.clientCampaignTemplateModalRef(component, clientCampaignTemplate);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.clientCampaignTemplateModalRef(component, new ClientCampaignTemplate());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    clientCampaignTemplateModalRef(component: Component, clientCampaignTemplate: ClientCampaignTemplate): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.clientCampaignTemplate = clientCampaignTemplate;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
