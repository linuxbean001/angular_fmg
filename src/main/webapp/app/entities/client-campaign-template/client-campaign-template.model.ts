import {BaseEntity} from './../../shared';

export class ClientCampaignTemplate implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public outcomes?: BaseEntity[],
    ) {
    }
}
