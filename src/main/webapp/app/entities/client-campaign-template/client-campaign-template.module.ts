import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CmaSharedModule } from '../../shared';
import {
    ClientCampaignTemplateService,
    ClientCampaignTemplatePopupService,
    ClientCampaignTemplateComponent,
    ClientCampaignTemplateDetailComponent,
    ClientCampaignTemplateDialogComponent,
    ClientCampaignTemplatePopupComponent,
    ClientCampaignTemplateDeletePopupComponent,
    ClientCampaignTemplateDeleteDialogComponent,
    clientCampaignTemplateRoute,
    clientCampaignTemplatePopupRoute,
    ClientCampaignTemplateResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...clientCampaignTemplateRoute,
    ...clientCampaignTemplatePopupRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ClientCampaignTemplateComponent,
        ClientCampaignTemplateDetailComponent,
        ClientCampaignTemplateDialogComponent,
        ClientCampaignTemplateDeleteDialogComponent,
        ClientCampaignTemplatePopupComponent,
        ClientCampaignTemplateDeletePopupComponent,
    ],
    entryComponents: [
        ClientCampaignTemplateComponent,
        ClientCampaignTemplateDialogComponent,
        ClientCampaignTemplatePopupComponent,
        ClientCampaignTemplateDeleteDialogComponent,
        ClientCampaignTemplateDeletePopupComponent,
    ],
    providers: [
        ClientCampaignTemplateService,
        ClientCampaignTemplatePopupService,
        ClientCampaignTemplateResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaClientCampaignTemplateModule {}
