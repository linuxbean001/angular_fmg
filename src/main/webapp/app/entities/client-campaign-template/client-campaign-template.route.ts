import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { ClientCampaignTemplateComponent } from './client-campaign-template.component';
import { ClientCampaignTemplateDetailComponent } from './client-campaign-template-detail.component';
import { ClientCampaignTemplatePopupComponent } from './client-campaign-template-dialog.component';
import { ClientCampaignTemplateDeletePopupComponent } from './client-campaign-template-delete-dialog.component';

@Injectable()
export class ClientCampaignTemplateResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const clientCampaignTemplateRoute: Routes = [
    {
        path: 'client-campaign-template',
        component: ClientCampaignTemplateComponent,
        resolve: {
            'pagingParams': ClientCampaignTemplateResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCampaignTemplate.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'client-campaign-template/:id',
        component: ClientCampaignTemplateDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCampaignTemplate.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const clientCampaignTemplatePopupRoute: Routes = [
    {
        path: 'client-campaign-template-new',
        component: ClientCampaignTemplatePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCampaignTemplate.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-campaign-template/:id/edit',
        component: ClientCampaignTemplatePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCampaignTemplate.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-campaign-template/:id/delete',
        component: ClientCampaignTemplateDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCampaignTemplate.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
