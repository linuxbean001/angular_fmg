import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { ClientCampaignTemplate } from './client-campaign-template.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<ClientCampaignTemplate>;

@Injectable()
export class ClientCampaignTemplateService {

    private resourceUrl =  SERVER_API_URL + 'api/client-campaign-templates';

    constructor(private http: HttpClient) { }

    create(clientCampaignTemplate: ClientCampaignTemplate): Observable<EntityResponseType> {
        const copy = this.convert(clientCampaignTemplate);
        return this.http.post<ClientCampaignTemplate>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(clientCampaignTemplate: ClientCampaignTemplate): Observable<EntityResponseType> {
        const copy = this.convert(clientCampaignTemplate);
        return this.http.put<ClientCampaignTemplate>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ClientCampaignTemplate>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ClientCampaignTemplate[]>> {
        const options = createRequestOption(req);
        return this.http.get<ClientCampaignTemplate[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ClientCampaignTemplate[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ClientCampaignTemplate = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ClientCampaignTemplate[]>): HttpResponse<ClientCampaignTemplate[]> {
        const jsonResponse: ClientCampaignTemplate[] = res.body;
        const body: ClientCampaignTemplate[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ClientCampaignTemplate.
     */
    private convertItemFromServer(clientCampaignTemplate: ClientCampaignTemplate): ClientCampaignTemplate {
        const copy: ClientCampaignTemplate = Object.assign({}, clientCampaignTemplate);
        return copy;
    }

    /**
     * Convert a ClientCampaignTemplate to a JSON which can be sent to the server.
     */
    private convert(clientCampaignTemplate: ClientCampaignTemplate): ClientCampaignTemplate {
        const copy: ClientCampaignTemplate = Object.assign({}, clientCampaignTemplate);
        return copy;
    }
}
