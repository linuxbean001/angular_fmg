export * from './client-campaign-template.model';
export * from './client-campaign-template-popup.service';
export * from './client-campaign-template.service';
export * from './client-campaign-template-dialog.component';
export * from './client-campaign-template-delete-dialog.component';
export * from './client-campaign-template-detail.component';
export * from './client-campaign-template.component';
export * from './client-campaign-template.route';
