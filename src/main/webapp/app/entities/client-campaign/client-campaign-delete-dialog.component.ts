import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager} from 'ng-jhipster';

import {ClientCampaign} from './client-campaign.model';
import {ClientCampaignPopupService} from './client-campaign-popup.service';
import {ClientCampaignService} from './client-campaign.service';

@Component({
    selector: 'jhi-client-campaign-delete-dialog',
    templateUrl: './client-campaign-delete-dialog.component.html'
})
export class ClientCampaignDeleteDialogComponent {

    clientCampaign: ClientCampaign;

    constructor(
        private clientCampaignService: ClientCampaignService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.clientCampaignService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'clientCampaignListModification',
                content: 'Deleted an clientCampaign',
                id,
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-client-campaign-delete-popup',
    template: ''
})
export class ClientCampaignDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientCampaignPopupService: ClientCampaignPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.clientCampaignPopupService
                .open(ClientCampaignDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
