import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { ClientCampaign } from './client-campaign.model';
import { ClientCampaignService } from './client-campaign.service';

@Component({
    selector: 'jhi-client-campaign-detail',
    templateUrl: './client-campaign-detail.component.html'
})
export class ClientCampaignDetailComponent implements OnInit, OnDestroy {

    clientCampaign: ClientCampaign;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private clientCampaignService: ClientCampaignService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInClientCampaigns();
    }

    load(id) {
        this.clientCampaignService.find(id)
            .subscribe((clientCampaignResponse: HttpResponse<ClientCampaign>) => {
                this.clientCampaign = clientCampaignResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInClientCampaigns() {
        this.eventSubscriber = this.eventManager.subscribe(
            'clientCampaignListModification',
            (response) => this.load(this.clientCampaign.id)
        );
    }
}
