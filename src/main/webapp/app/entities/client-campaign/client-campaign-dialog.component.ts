import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';

import {ClientCampaign, EventFrequencyRate, ReportDistributionMethod} from './client-campaign.model';
import {ClientCampaignPopupService} from './client-campaign-popup.service';
import {ClientCampaignService} from './client-campaign.service';
import {User, UserService} from '../../shared';
import {ClientCampaignTemplate, ClientCampaignTemplateService} from '../client-campaign-template';
import {ClientCompany, ClientCompanyService} from '../client-company';
import {ClientList, ClientListService} from '../client-list';
import {EmailTemplate, EmailTemplateService} from '../email-template';
import {Outcome, OutcomeService} from '../outcome';
import {ClientCampaignScript, ClientCampaignScriptService} from '../client-campaign-script';
import {CampaignPortalUser, CampaignPortalUserService} from '../campaign-portal-user';
import {ClientCampaignSms, ClientCampaignSmsService} from '../client-campaign-sms';

@Component({
    selector: 'jhi-client-campaign-dialog',
    templateUrl: './client-campaign-dialog.component.html'
})
export class ClientCampaignDialogComponent implements OnInit {

    clientCampaign: ClientCampaign;
    isSaving: boolean;

    users: User[];

    clientcampaigntemplates: ClientCampaignTemplate[];

    clientcompanies: ClientCompany[];

    clientLists: ClientList[];

    emailTemplates: EmailTemplate[];

    outcomes: Outcome[];

    scripts: ClientCampaignScript[];

    smss: ClientCampaignSms[];

    RATES = EventFrequencyRate.RATES;
    METHODS = ReportDistributionMethod.METHODS;

    portalUsers: CampaignPortalUser[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private clientCampaignService: ClientCampaignService,
        private userService: UserService,
        private clientCampaignTemplateService: ClientCampaignTemplateService,
        private clientCompanyService: ClientCompanyService,
        private clientListService: ClientListService,
        private emailTemplateService: EmailTemplateService,
        private outcomeService: OutcomeService,
        private clientCampaignScriptService: ClientCampaignScriptService,
        private clientCampaignSmsService: ClientCampaignSmsService,
        private campaignPortalUserService: CampaignPortalUserService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.userService.query()
            .subscribe((res: HttpResponse<User[]>) => {
                this.users = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.clientCampaignTemplateService.query()
            .subscribe((res: HttpResponse<ClientCampaignTemplate[]>) => {
                this.clientcampaigntemplates = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.clientCompanyService.query()
            .subscribe((res: HttpResponse<ClientCompany[]>) => {
                this.clientcompanies = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.clientListService.query()
            .subscribe((res: HttpResponse<ClientList[]>) => {
                this.clientLists = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.emailTemplateService.query()
            .subscribe((res: HttpResponse<EmailTemplate[]>) => {
                this.emailTemplates = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.outcomeService.query()
            .subscribe((res: HttpResponse<Outcome[]>) => {
                this.outcomes = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.clientCampaignScriptService.query()
            .subscribe((res: HttpResponse<ClientCampaignScript[]>) => {
                this.scripts = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.clientCampaignSmsService.query()
            .subscribe((res: HttpResponse<ClientCampaignSms[]>) => {
                this.smss = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.campaignPortalUserService.query()
            .subscribe((res: HttpResponse<CampaignPortalUser[]>) => {
                this.portalUsers = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.clientCampaign.id !== undefined) {
            this.subscribeToSaveResponse(
                this.clientCampaignService.update(this.clientCampaign));
        } else {
            this.subscribeToSaveResponse(
                this.clientCampaignService.create(this.clientCampaign));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ClientCampaign>>) {
        result.subscribe((res: HttpResponse<ClientCampaign>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ClientCampaign) {
        this.eventManager.broadcast({name: 'clientCampaignListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }

    trackClientCampaignTemplateById(index: number, item: ClientCampaignTemplate) {
        return item.id;
    }

    trackClientCompanyById(index: number, item: ClientCompany) {
        return item.id;
    }

    trackClientListById(index: number, item: ClientList) {
        return item.id;
    }

    trackEmailTemplateById(index: number, item: EmailTemplate) {
        return item.id;
    }

    trackOutcomeById(index: number, item: Outcome) {
        return item.id;
    }

    trackScriptById(index: number, item: ClientCampaignScript) {
        return item.id;
    }

    trackSmsById(index: number, item: ClientCampaignSms) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-client-campaign-popup',
    template: ''
})
export class ClientCampaignPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientCampaignPopupService: ClientCampaignPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.clientCampaignPopupService
                    .open(ClientCampaignDialogComponent as Component, params['id']);
            } else {
                this.clientCampaignPopupService
                    .open(ClientCampaignDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
