import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { ClientCampaign } from './client-campaign.model';
import { ClientCampaignService } from './client-campaign.service';

@Injectable()
export class ClientCampaignPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private clientCampaignService: ClientCampaignService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.clientCampaignService.find(id)
                    .subscribe((clientCampaignResponse: HttpResponse<ClientCampaign>) => {
                        const clientCampaign: ClientCampaign = clientCampaignResponse.body;
                        clientCampaign.createdDate = this.datePipe
                            .transform(clientCampaign.createdDate, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.clientCampaignModalRef(component, clientCampaign);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.clientCampaignModalRef(component, new ClientCampaign());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    clientCampaignModalRef(component: Component, clientCampaign: ClientCampaign): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.clientCampaign = clientCampaign;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
