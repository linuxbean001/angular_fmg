import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {JhiAlertService, JhiEventManager, JhiParseLinks} from 'ng-jhipster';
import {TranslateService} from '@ngx-translate/core';

import {ClientCampaign} from './client-campaign.model';
import {ClientCampaignService} from './client-campaign.service';
import {ConfirmOptions, ConfirmService, Principal} from '../../shared';
import {ClientCampaignWizardClientComponent} from './pages/client.component';
import {TabChangedEvent, TabClickEvent, Tabs} from '../../layouts/tabs/tabs.component';
import {ClientCampaignWizardCampaignComponent} from './pages/campaign.component';
import {ClientCampaignWizardAccessComponent} from './pages/access.component';
import {ClientCampaignWizardListComponent} from './pages/list.component';
import {FileService} from './file.service';
import {ClientCampaignWizardEmailComponent} from './pages/email.component';
import {ClientCampaignWizardReportingComponent} from './pages/reporting.component';
import {Tab} from '../../layouts/tabs/tab.component';
import {AppointmentAttendeeService} from '../appointment-attendee';
import {ClientCampaignWizardGdprComponent} from './pages/gdpr.component';
import {ClientCampaignWizardTargetComponent} from './pages/target.component';

@Component({
    selector: 'jhi-client-campaign-wizard',
    templateUrl: './client-campaign-wizard.component.html'
})
export class ClientCampaignWizardComponent implements OnInit, OnDestroy {

    WIZARD_LAST_STATE = 'wizard_last_state';

    currentAccount: any;
    // error: any;
    // success: any;
    // routeData: any;
    // links: any;
    // totalItems: any;
    // queryCount: any;
    // itemsPerPage: any;
    // page: any;
    // predicate: any;
    // previousPage: any;
    // reverse: any;

    wizardData: WizardState;
    @ViewChild(Tabs)
    tabs: Tabs;
    tabsViewChildReady = false;
    finishing = false;
    finished = false;
    routeSub: any;
    routeData: any;
    ClientCampaign: typeof ClientCampaign = ClientCampaign;
    @ViewChild(ClientCampaignWizardClientComponent)
    private clientComponent: ClientCampaignWizardClientComponent;

    // @ViewChild(ClientCampaignWizardMappingComponent)
    // private mappingComponent: ClientCampaignWizardMappingComponent;

    // @ViewChild(ClientCampaignWizardSummaryComponent)
    // private summaryComponent: ClientCampaignWizardSummaryComponent;
    @ViewChild(ClientCampaignWizardCampaignComponent)
    private campaignComponent: ClientCampaignWizardCampaignComponent;
    @ViewChild(ClientCampaignWizardAccessComponent)
    private accessComponent: ClientCampaignWizardAccessComponent;
    @ViewChild(ClientCampaignWizardGdprComponent)
    private gdprComponent: ClientCampaignWizardGdprComponent;
    @ViewChild(ClientCampaignWizardEmailComponent)
    private emailComponent: ClientCampaignWizardEmailComponent;
    @ViewChild(ClientCampaignWizardListComponent)
    private listComponent: ClientCampaignWizardListComponent;
    @ViewChild(ClientCampaignWizardTargetComponent)
    private targetComponent: ClientCampaignWizardTargetComponent;
    @ViewChild(ClientCampaignWizardReportingComponent)
    private reportingComponent: ClientCampaignWizardReportingComponent;

    constructor(
        private clientCampaignService: ClientCampaignService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private fileService: FileService,
        private confirmService: ConfirmService,
        private route: ActivatedRoute,
        private location: Location,
        private appointmentAttendeeService: AppointmentAttendeeService,
        private translateService: TranslateService,
    ) {
    }

    ngOnInit() {
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            const ident = data.wizardParams.tab;
            if (ident) {
                // const tab = this.tabs.find(ident);
                // if (tab) {
                //     this.tabs.selectTab(tab);
                // }
            }
        });

        this.routeSub = this.route.params.subscribe((params) => {
            const id = params['id'];
            if (id) {
                // TODO
                this.clientCampaignService.find(id).toPromise()
                    .then((response) => response.body)
                    .then((clientCampaign) => {
                        this.wizardData = new WizardState();
                        this.wizardData.clientCampaign = clientCampaign;
                        console.log('JON12 LOADED CAMPAIGN. Port users' + clientCampaign.portalUsers.length);
                        // workaround to prevent "Expression has changed after it was checked"
                        setTimeout(() => {
                            this.tabsViewChildReady = true;
                        });
                    });
            } else {
                this.wizardData = new WizardState();

                // workaround to prevent "Expression has changed after it was checked"
                setTimeout(() => {
                    this.tabsViewChildReady = true;
                });
            }
        });

        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });

        // // FIXME just for debug purposes, remove this!
        // let debugWizardState = new WizardState();
        // debugWizardState.clientPage.clientCompany = new ClientCompany(1);
        // debugWizardState.campaignPage.name = "test";
        // debugWizardState.campaignPage.campaignTemplate = new ClientCampaignTemplate(1);
        // debugWizardState.campaignPage.clientCampaign = new ClientCampaign(1);
        // const campaignPortalUser = new CampaignPortalUser(null, "a@a.ru", true, true);
        // campaignPortalUser.outcomes.push(new Outcome(1));
        // campaignPortalUser.outcomes.push(new Outcome(2));
        // debugWizardState.accessPage.portalUsers.push(campaignPortalUser);
        // debugWizardState.accessPage.users.push(new User(5));
        // debugWizardState.emailPage.newTemplate = new EmailTemplate();
        // debugWizardState.emailPage.templateWoCampaign = new EmailTemplate(2);
        // debugWizardState.emailPage.clientCampaignWithTemplate = new ClientCampaign(1);
        // debugWizardState.listPage.name = "my list";
        // this.saveToLocalStorage(debugWizardState);
        //
        // const wizardData = this.loadFromLocalStorage();
        // if (wizardData) {
        //     let options: ConfirmOptions = {
        //         title: "Confirmation",
        //         message: "We noticed that you did not finish last time. Therefore, uploaded the previous state. Want to start over?",
        //         yesCaption: "Start from scratch",
        //         noCaption: "Continue where finished",
        //     };
        //
        //     // why timeout?! see https://github.com/angular/angular/issues/15634
        //     setTimeout(() => {
        //         this.confirmService.confirm(options)
        //             .then((value) => {
        //                 this.wizardData = new WizardState();
        //                 this.removeFromLocalStorage();
        //             })
        //             .catch((reason) => {
        //                 this.wizardData = wizardData;
        //             });
        //     });
        // }
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }

    isDisabled(tab) {
        const currentTab = this.tabs.getCurrentTab();
    }

    validateClientPage(showError = true): TabLeavingData {
        // if (!this.wizardData.clientCampaign.clientCompanyId) {
        //     // if (showError) {
        //     //     this.displayError('error.campaign-wizard.client.empty');
        //     // }
        //     return TabLeavingData.VALIDATION_ISSUE;
        // }
        return TabLeavingData.CAN_LEAVE;
    }

    validateSurveyPage(showError = true): TabLeavingData {
        if (!this.wizardData.clientCampaign.surveyName) {
            // if (showError) {
            //     this.displayError('error.campaign-wizard.surveyName.empty');
            // }
            return TabLeavingData.VALIDATION_ISSUE;
        }
        return TabLeavingData.CAN_LEAVE;
    }

    validateBookingPage(showError = true): TabLeavingData {
        if (!this.wizardData.clientCampaign.bookingName) {
            // if (showError) {
            //     this.displayError('error.campaign-wizard.bookingName.empty');
            // }
            return TabLeavingData.VALIDATION_ISSUE;
        }
        return TabLeavingData.CAN_LEAVE;
    }

    validateCampaignPage(showError = true): Promise<TabLeavingData> {
        const name = this.wizardData.clientCampaign.name;
        if (!name) {
            // if (showError) {
            //     this.displayError('error.campaign-wizard.campaign.emptycampaignname');
            // }
            return Promise.resolve(TabLeavingData.VALIDATION_ISSUE);
        } else {
            const id = this.wizardData.clientCampaign.id;
            return this.clientCampaignService.nameIsUnique(name, id).toPromise()
                .then((response) => {
                    const nameAlreadyExists = response.body;
                    if (!nameAlreadyExists) {
                        this.displayError('error.clientCampaign.name.exists');
                        return TabLeavingData.VALIDATION_ISSUE;
                    }
                    return TabLeavingData.CAN_LEAVE;
                });
        }
    }

    validateAccessPage(showError = true): TabLeavingData {
        // const managersFilled = !!this.wizardData.clientCampaign.managers.length;
        // const portalUsersFilled = !!this.wizardData.clientCampaign.portalUsers.length;
        //
        // if (!managersFilled) {
        //     // if (showError) {
        //     //     this.displayError('error.campaign-wizard.access.emptyusers');
        //     // }
        //     return false;
        // }

        const someEmailNotFilled = this.wizardData.clientCampaign.portalUsers.some((value) => !value.email);
        if (someEmailNotFilled) {
            if (showError) {
                this.displayError('error.campaign-wizard.access.fillemailofportaluser');
            }
            return TabLeavingData.VALIDATION_ISSUE;
        }
        return TabLeavingData.CAN_LEAVE;
    }

    validateGDPRPage(showError = true): TabLeavingData {
        return TabLeavingData.CAN_LEAVE;
    }

    validateOutcomePage(showError = true): TabLeavingData {
        if (!this.wizardData.clientCampaign.outcomes.length) {
            // if (showError) {
            //     this.displayError('error.campaign-wizard.outcome.emptyoutcomes');
            // }
            return TabLeavingData.VALIDATION_ISSUE;
        }
        return TabLeavingData.CAN_LEAVE;
    }

    validateBriefingPage(showError = true): TabLeavingData {
        return TabLeavingData.CAN_LEAVE;
    }

    validateScriptPage(showError = true): TabLeavingData {
        if (!this.wizardData.clientCampaign.scripts.length) {
            // if (showError) {
            //     this.displayError('error.campaign-wizard.script.emptyscripts');
            // }
            return TabLeavingData.VALIDATION_ISSUE;
        }
        return TabLeavingData.CAN_LEAVE;
    }

    validateSmsPage(showError = true): TabLeavingData {
        if (!this.wizardData.clientCampaign.smss.length && !this.wizardData.clientCampaign.smsTabNotRequired) {
            // if (showError) {
            //     this.displayError('error.campaign-wizard.sms.emptysmss');
            // }
            return TabLeavingData.VALIDATION_ISSUE;
        }
        return TabLeavingData.CAN_LEAVE;
    }

    validateEmailPage(showError = true): TabLeavingData {
        if (!this.wizardData.clientCampaign.emailTemplates.length) {
            // if (showError) {
            //     this.displayError('error.campaign-wizard.email.emptytemplates');
            // }
            return TabLeavingData.VALIDATION_ISSUE;
        }
        return TabLeavingData.CAN_LEAVE;
    }

    validateListPage(showError = true): TabLeavingData {
        // if (!this.wizardData.listPage.name) {
        //     if (showError) {
        //         this.displayError("error.campaign-wizard.list.emptyname");
        //     }
        //     return false;
        // }
        //
        // if (!this.wizardData.listPage.csvFile) {
        //     if (showError) {
        //         this.displayError("error.campaign-wizard.list.emptyfile");
        //     }
        //     return false;
        // }
        return TabLeavingData.CAN_LEAVE;
    }

    validateTargetPage(showError = true): TabLeavingData {
        return TabLeavingData.CAN_LEAVE;
    }

    validateReportingPage(showError = true): TabLeavingData {
        return TabLeavingData.CAN_LEAVE;
    }

    validateMappingPage(showError = true): TabLeavingData {
        return TabLeavingData.CAN_LEAVE;
    }

    validatePage(tab: Tab): Promise<TabLeavingData> {
        switch (tab.ident) {
            case 'CLIENT':
                return Promise.resolve(this.validateClientPage());
            case 'CAMPAIGN':
                return this.validateCampaignPage();
            case 'ACCESS':
                return Promise.resolve(this.validateAccessPage());
            case 'GDPR':
                return Promise.resolve(this.validateGDPRPage());
            case 'OUTCOME':
                return Promise.resolve(this.validateOutcomePage());
            case 'BRIEFING':
                return Promise.resolve(this.validateBriefingPage());
            case 'SURVEY':
                return Promise.resolve(this.validateSurveyPage());
            case 'BOOKING':
                return Promise.resolve(this.validateBookingPage());
            case 'SCRIPT':
                return Promise.resolve(this.validateScriptPage());
            case 'SMS':
                return Promise.resolve(this.validateSmsPage());
            case 'EMAIL':
                return Promise.resolve(this.validateEmailPage());
            case 'LIST':
                return Promise.resolve(this.validateListPage());
            case 'TARGET':
                return Promise.resolve(this.validateTargetPage());
            case 'REPORTING':
                return Promise.resolve(this.validateReportingPage());
            // case "MAPPING":
            //     return !this.validateMappingPage();
            default:
                return Promise.resolve(TabLeavingData.CAN_LEAVE);
        }
    }

    removeFromLocalStorage() {
        localStorage.removeItem(this.WIZARD_LAST_STATE);
    }

    saveToLocalStorage(wizardState: WizardState) {
        // localStorage.setItem(this.WIZARD_LAST_STATE, JSON.stringify(wizardState));
    }

    finish() {
        // this.finished = true;
        // setTimeout(() => this.nextTab(), 1)
        // return;

        if (
            !this.validateClientPage().canLeave ||
            // !this.validateCampaignPage().canLeave || // FIXME fix this promise
            !this.validateAccessPage().canLeave ||
            !this.validateGDPRPage().canLeave ||
            !this.validateEmailPage().canLeave ||
            !this.validateScriptPage().canLeave ||
            !this.validateListPage().canLeave ||
            !this.validateTargetPage().canLeave ||
            !this.validateReportingPage().canLeave ||
            !this.validateMappingPage().canLeave
        ) {
            return;
        }

        // let file = this.wizardData.listPage.csvFile;

        const clientCampaign = this.wizardData.clientCampaign;
        // page Client List Mapping
        // let oldFields = this.mappingComponent.oldFields
        //     .filter(oldField => this.mappingComponent.elementByField[oldField.field.id].length > 0)
        //     .map(oldField => new OldField(oldField.field, this.mappingComponent.elementByField[oldField.field.id][0], oldField.editable, oldField.visible));
        //
        // let newFields = this.mappingComponent.newFields
        //     .map(newField => {
        //         let header = newField.header;
        //         let newName = this.mappingComponent.nameByIndex[header.index];
        //
        //         let newField1 = new NewField(header, newField.editable, newField.visible);
        //         newField1.header.value = newName || header.value;
        //         return newField1;
        //     });
        //
        // clientCampaign.mapping = new ClientListMapping(
        //     oldFields,
        //     newFields,
        //     this.mappingComponent.newFieldsNoMapping,
        // );

        this.finishing = true;
        try {
            this.clientCampaignService.createByWizard(clientCampaign).toPromise()
                .then((response) => {
                    this.wizardData.clientCampaign = response.body;

                    this.finishing = false;
                    this.finished = true;

                    // give a timeout
                    setTimeout(() => {
                        this.tabs.nextTab();
                        this.removeFromLocalStorage();
                    });
                })
                .catch((reason) => {
                    this.finishing = false;
                    this.removeFromLocalStorage();
                });
        } catch (e) {
            this.finishing = false;
            throw e;
        }
    }

    // loadFromLocalStorage(): WizardState {
    //     const wizardState = JSON.parse(localStorage.getItem(this.WIZARD_LAST_STATE));
    //     if (wizardState) {
    //         // clear it, it's not correctly serialized to string
    //         delete wizardState.listPage.csvFile;
    //     }
    //     return wizardState;
    // }

    reset() {
        this.wizardData = new WizardState();
        this.finished = false;

        // give a timeout
        setTimeout(() => {
            this.tabs.selectTab(this.tabs.tabs.first);
        });
    }

    onTabChanged($event: TabChangedEvent) {
        // skip first render
        if (!$event.from) {
            return;
        }

        const data = $event.data as TabLeavedData;
        if (data && data.hasValidationIssue) {
            // just leave, save not required
            return;
        }

        const clientCampaign = this.wizardData.clientCampaign;
        if (clientCampaign.id) {
            this.clientCampaignService.update(clientCampaign).toPromise()
                .then((response) => {

                    console.log('JON12 UPDATING IN THE WBIZARD');

                    const savedClientCampaign = response.body;
                    this.wizardData.clientCampaign = savedClientCampaign;

                    // // is this is correct place to do this?
                    // // client campaign already contains appointmentAttendees...
                    // if ($event.from.ident === 'BRIEFING') {
                    //     clientCampaign.appointmentAttendees.forEach((appointmentAttendee) => {
                    //         this.appointmentAttendeeService.update(appointmentAttendee).toPromise();
                    //     });
                    // }
                });
        } else {
            this.clientCampaignService.createByWizard(clientCampaign).toPromise()
                .then((response) => {
                    const savedClientCampaign = response.body;
                    this.wizardData.clientCampaign = savedClientCampaign;
                    this.location.replaceState('/client-campaign-wizard/' + savedClientCampaign.id + '/edit');
                });
        }
        // this.saveToLocalStorage(this.wizardData);
    }

    onTabClick(event: TabClickEvent) {
        if (!event.from) {
            this.tabs.selectTab(event.to);
        } else {
            this.validatePage(event.from)
                .then((tabLeavingData) => {
                    if (tabLeavingData.canLeave) {
                        this.tabs.selectTab(event.to, new TabLeavedData(tabLeavingData.tabLeavedData.hasValidationIssue));
                    } else {
                        // you can't leave
                    }
                });
        }
    }

    gdprClassName(cc: ClientCampaign) {
        return cc.controllerFullName &&
        cc.controllerJobRole &&
        cc.controllerEmail &&
        cc.controllerPhone &&
        cc.controllerCompanyName &&
        cc.controllerCompanyReg &&
        cc.controllerCompanyAddress &&
        cc.sarsFullName &&
        cc.sarsJobRole &&
        cc.sarsEmail &&
        cc.sarsPhone &&
        cc.sarsCompanyName &&
        cc.sarsCompanyReg &&
        cc.sarsCompanyAddress &&
        cc.liaHyperlink ? 'nav-item-state-success' : 'nav-item-state-secondary';
    }

    accessClassName(cc: ClientCampaign) {
        return ClientCampaign.accessIsComplete(cc) ? 'nav-item-state-success' : 'nav-item-state-secondary';
    }

    targetClassName(cc: ClientCampaign) {
        return ClientCampaign.targetIsComplete(cc) ? 'nav-item-state-success' : 'nav-item-state-secondary';
    }

    private displayError(msg: string) {
        // this.jhiAlertService.error(msg, null, null);

        const translatedMsg = this.translateService.instant(msg);

        const options: ConfirmOptions = {
            title: 'Warning',
            message: translatedMsg,
            noHidden: true,
            yesCaption: 'OK'
        };

        return this.confirmService.confirm(options);
    }
}

export class WizardState {
    public constructor(
        public version: WizardStateVersion = WizardStateVersion.V1,
        public clientCampaign = new ClientCampaign(),
        // public headers?: CsvCell[],
    ) {
    }
}

export enum WizardStateVersion {
    V1
}

export class TabLeavedData {
    public constructor(
        public hasValidationIssue: boolean,
    ) {
    }
}

export class TabLeavingData {
    static CAN_LEAVE = new TabLeavingData(true, new TabLeavedData(false));
    static VALIDATION_ISSUE = new TabLeavingData(true, new TabLeavedData(true));
    static CANT_LEAVE = new TabLeavingData(false, new TabLeavedData(true));

    public constructor(
        public canLeave: boolean,
        public tabLeavedData: TabLeavedData,
    ) {
    }
}
