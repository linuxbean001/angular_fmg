import {BaseEntity, User} from './../../shared';
import {NewField, NewFieldNoMapping, OldField} from './pages/mapping.component';
import {CampaignPortalUser} from '../campaign-portal-user/campaign-portal-user.model';
import {Outcome} from '../outcome';
import {ClientCampaignScript} from '../client-campaign-script';
import {ClientCampaignSms} from '../client-campaign-sms';
import {AppointmentAttendee} from '../appointment-attendee';
import {SurveyQuestion} from '../survey-question';
import {BookingQuestion} from '../booking-question';
import {ClientCampaignAddDoc} from '../client-campaign-add-doc';
import {ClientList} from '../client-list';

export class ClientListMapping {
    public constructor(
        public oldFields: OldField[],
        public newFields: NewField[],
        public newFieldsNoMapping: NewFieldNoMapping[],
    ) {
    }
}

export class ClientCampaign implements BaseEntity {

    static DEFAULT_NAME = 'Campaign - Name TBD';

    static accessIsComplete(cc: ClientCampaign): boolean {
        // access page
        // should I check cc.portalUsers.length ?
        // UPD: yes
        // one more question: should I check cc.users & cc.qaUsers ?
        // UPD: see here https://gitlab.com/mzdw4w/fmg/issues/67#note_171839407
        return cc.managers.length && cc.portalUsers.length && cc.qaUsers.length >= 2;
    }

    static targetIsComplete(cc: ClientCampaign) {
        return cc.callPerHour &&
            cc.conversationsPerHour &&
            cc.contactRate &&
            cc.hoursPerLead &&
            cc.conversionRate &&
            cc.leadToSaleConversion &&
            cc.averageSaleValue;
    }

    static getCompletePercent(cc: ClientCampaign): number {
        const tabCount = 14;
        const part = 100 / tabCount;

        let percent = 0;
        if (cc.clientCompanyId) {
            percent += part;
        }
        if (cc.name && cc.name !== ClientCampaign.DEFAULT_NAME) {
            percent += part;
        }
        if (this.accessIsComplete(cc)) {
            percent += part;
        }
        // GDPR page
        if (cc.controllerFullName &&
            cc.controllerJobRole &&
            cc.controllerEmail &&
            cc.controllerPhone &&
            cc.controllerCompanyName &&
            cc.controllerCompanyReg &&
            cc.controllerCompanyAddress &&
            cc.sarsFullName &&
            cc.sarsJobRole &&
            cc.sarsEmail &&
            cc.sarsPhone &&
            cc.sarsCompanyName &&
            cc.sarsCompanyReg &&
            cc.sarsCompanyAddress &&
            cc.liaHyperlink) {
            percent += part;
        }
        if (cc.scripts.length) {
            percent += part;
        }
        if (cc.smss.length || cc.smsTabNotRequired) {
            percent += part;
        }
        if (cc.emailTemplates.length) {
            percent += part;
        }
        if (cc.outcomes.length) {
            percent += part;
        }
        if (cc.appointmentAttendees.length) {
            percent += part;
        }
        if (cc.surveyQuestions.length) {
            percent += part;
        }
        if (cc.bookingQuestions.length) {
            percent += part;
        }
        if (cc.clientLists.length) {
            percent += part;
        }
        if (this.targetIsComplete(cc)) {
            percent += part;
        }
        // TODO reporting, how I can check reporting tab is complete?
        if (false) {
            // @ts-ignore
            percent += part;
        }

        // always round upper
        return Math.round(Math.min(100, Math.ceil(percent)));
    }

    constructor(
        public id?: number,
        public name?: string,
        public createdDate?: any,
        public createdByLogin?: string,
        public createdById?: number,
        public clientCompanyName?: string,
        public clientCompanyId?: number,
        public clientCompanyIndustryId?: number,
        public managers: User[] = [],
        public users: User[] = [],
        public qaUsers: User[] = [],
        public emailTemplates: BaseEntity[] = [], // or EmailTemplate[]?
        public mapping?: ClientListMapping,
        public portalUsers: CampaignPortalUser[] = [],
        public clientLists: ClientList[] = [],
        public outcomes: Outcome[] = [], // or BaseEntity[]?
        public scripts: ClientCampaignScript[] = [],
        public smss: ClientCampaignSms[] = [],
        public weeklyHeadlineStats?: boolean,
        public allowedExportData?: boolean,
        public sarReportIdent?: string,
        public sarReportTime?: any,
        public statsSummaryIdent?: string,
        public statsSummaryTime?: any,
        public reportDistributionMethods?: string[],
        public portalUsersAllowedToExport?: number[],
        public reportsDeliveredToUsers?: number[],
        public appointmentAttendees: AppointmentAttendee[] = [],
        public website?: string,
        public meetingDuration?: string,
        public specialRequests?: string,
        public resultsPerHour?: string,
        public qualityControl?: string,
        public comments?: string,
        public appointmentType?: string,
        public surveyName?: string,
        public surveyQuestions: SurveyQuestion[] = [],
        public bookingName?: string,
        public bookingQuestions: BookingQuestion[] = [],
        public smsTabNotRequired?: boolean,
        public controllerFullName?: string,
        public controllerJobRole?: string,
        public controllerEmail?: string,
        public controllerPhone?: string,
        public controllerCompanyName?: string,
        public controllerCompanyReg?: string,
        public controllerCompanyAddress?: string,
        public sarsFullName?: string,
        public sarsJobRole?: string,
        public sarsEmail?: string,
        public sarsPhone?: string,
        public sarsCompanyName?: string,
        public sarsCompanyReg?: string,
        public sarsCompanyAddress?: string,
        public liaHyperlink?: string,
        public callPerHour?: number,
        public callPerHourLow?: number,
        public callPerHourHigh?: number,
        public conversationsPerHour?: number,
        public conversationsPerHourLow?: number,
        public conversationsPerHourHigh?: number,
        public contactRate?: number,
        public contactRateLow?: number,
        public contactRateHigh?: number,
        public hoursPerLead?: number,
        public hoursPerLeadLow?: number,
        public hoursPerLeadHigh?: number,
        public conversionRate?: number,
        public conversionRateLow?: number,
        public conversionRateHigh?: number,
        public leadToSaleConversion?: number,
        public leadToSaleConversionLow?: number,
        public leadToSaleConversionHigh?: number,
        public averageSaleValue?: number,
        public averageSaleValueLow?: number,
        public averageSaleValueHigh?: number,
        public strategyDocument?: string,
        public addDocs: ClientCampaignAddDoc[] = [],
    ) {
    }
}

export class EventFrequencyRate {
    static RATES: EventFrequencyRate[] = [
        new EventFrequencyRate('Daily', 'DAILY'),
        new EventFrequencyRate('Weekly', 'WEEKLY'),
        new EventFrequencyRate('Monthly', 'MONTHLY'),
        new EventFrequencyRate('Quarterly', 'QUARTERLY'),
    ];

    static getByIdent(ident: string): EventFrequencyRate {
        return this.RATES.find((x) => x.ident === ident);
    }

    public constructor(
        public name: string,
        public ident: string,
    ) {
    }
}

export class ReportDistributionMethod {
    static METHODS: ReportDistributionMethod[] = [
        new ReportDistributionMethod('Email', 'EMAIL'),
        new ReportDistributionMethod('Sent Link To Reports Page', 'LINK'),
    ];

    static getByIdents(...idents: string[]): ReportDistributionMethod[] {
        return this.METHODS.filter((x) => idents.indexOf(x.ident) > -1);
    }

    public constructor(
        public name: string,
        public ident: string,
    ) {
    }
}
