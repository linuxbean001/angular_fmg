import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {CmaSharedModule} from '../../shared';
import {CmaAdminModule} from '../../admin/admin.module';

import {
    ClientCampaignComponent,
    ClientCampaignDeleteDialogComponent,
    ClientCampaignDeletePopupComponent,
    ClientCampaignDetailComponent,
    ClientCampaignDialogComponent,
    ClientCampaignPopupComponent,
    clientCampaignPopupRoute,
    ClientCampaignPopupService,
    ClientCampaignResolvePagingParams,
    clientCampaignRoute,
    ClientCampaignService,
    ClientCampaignWizardComponent,
    ClientCampaignWizardResolveParams,
    clientCampaignWizardRoute,
    FileService
} from './';

import {ClientCampaignWizardClientComponent} from './pages/client.component';
import {ClientCampaignWizardCampaignComponent} from './pages/campaign.component';
import {ClientCampaignWizardAccessComponent} from './pages/access.component';
import {ClientCampaignWizardEmailComponent} from './pages/email.component';
import {ClientCampaignWizardListComponent} from './pages/list.component';
import {ClientCampaignWizardMappingComponent} from './pages/mapping.component';
import {ClientCampaignWizardSummaryComponent} from './pages/summary.component';
import {DragulaModule} from 'ng2-dragula';
import {TabModule} from '../../layouts/tabs/tab.module';
import {Tab} from '../../layouts/tabs/tab.component';
import {Tabs} from '../../layouts/tabs/tabs.component';
import {ClientListMappingPopupService} from './pages/client-list-mapping-popup.service';
import {ClientCampaignWizardOutcomeComponent} from './pages/outcome.component';
import {ClientCampaignWizardScriptComponent} from './pages/script.component';
import {ClientCampaignWizardReportingComponent} from './pages/reporting.component';
import {ClientCampaignWizardSmsComponent} from './pages/sms.component';
import {ClientCampaignWizardBriefingComponent} from './pages/briefing.component';
import {ClientCampaignWizardSurveyComponent} from './pages/survey.component';
import {ClientCampaignWizardBookingComponent} from './pages/booking.component';
import {ClientCampaignWizardGdprComponent} from './pages/gdpr.component';
import {ClientCampaignWizardTargetComponent} from './pages/target.component';
import {ReactiveFormsModule} from '@angular/forms';
import {CustomFormsModule} from 'ng2-validation';

const ENTITY_STATES = [
    ...clientCampaignRoute,
    ...clientCampaignPopupRoute,
    ...clientCampaignWizardRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        CmaAdminModule,
        RouterModule.forChild(ENTITY_STATES),
        DragulaModule.forRoot(),
        TabModule,
        ReactiveFormsModule,
        CustomFormsModule,
    ],
    declarations: [
        ClientCampaignComponent,
        ClientCampaignDetailComponent,
        ClientCampaignDialogComponent,
        ClientCampaignDeleteDialogComponent,
        ClientCampaignPopupComponent,
        ClientCampaignDeletePopupComponent,
        ClientCampaignWizardComponent,
        ClientCampaignWizardClientComponent,
        ClientCampaignWizardCampaignComponent,
        ClientCampaignWizardAccessComponent,
        ClientCampaignWizardGdprComponent,
        ClientCampaignWizardTargetComponent,
        ClientCampaignWizardEmailComponent,
        ClientCampaignWizardListComponent,
        ClientCampaignWizardMappingComponent,
        ClientCampaignWizardSummaryComponent,
        ClientCampaignWizardOutcomeComponent,
        ClientCampaignWizardScriptComponent,
        ClientCampaignWizardSurveyComponent,
        ClientCampaignWizardBookingComponent,
        ClientCampaignWizardReportingComponent,
        ClientCampaignWizardSmsComponent,
        ClientCampaignWizardBriefingComponent,
    ],
    entryComponents: [
        ClientCampaignComponent,
        ClientCampaignDialogComponent,
        ClientCampaignPopupComponent,
        ClientCampaignDeleteDialogComponent,
        ClientCampaignDeletePopupComponent,
        ClientCampaignWizardComponent,
        ClientCampaignWizardClientComponent,
        ClientCampaignWizardCampaignComponent,
        ClientCampaignWizardAccessComponent,
        ClientCampaignWizardGdprComponent,
        ClientCampaignWizardTargetComponent,
        ClientCampaignWizardEmailComponent,
        ClientCampaignWizardListComponent,
        ClientCampaignWizardMappingComponent,
        ClientCampaignWizardSummaryComponent,
        ClientCampaignWizardOutcomeComponent,
        ClientCampaignWizardScriptComponent,
        ClientCampaignWizardSurveyComponent,
        ClientCampaignWizardBookingComponent,
        ClientCampaignWizardReportingComponent,
        ClientCampaignWizardSmsComponent,
        ClientCampaignWizardBriefingComponent,
        Tab,
        Tabs,
    ],
    providers: [
        ClientCampaignService,
        ClientCampaignPopupService,
        ClientCampaignResolvePagingParams,
        ClientCampaignWizardResolveParams,
        FileService,
        ClientListMappingPopupService,
    ],
    exports: [
        ClientCampaignWizardBriefingComponent,
        ClientCampaignWizardGdprComponent,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaClientCampaignModule {
}
