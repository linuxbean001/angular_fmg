import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes} from '@angular/router';
import {JhiPaginationUtil} from 'ng-jhipster';

import {UserRouteAccessService} from '../../shared';
import {ClientCampaignComponent} from './client-campaign.component';
import {ClientCampaignDetailComponent} from './client-campaign-detail.component';
import {ClientCampaignPopupComponent} from './client-campaign-dialog.component';
import {ClientCampaignDeletePopupComponent} from './client-campaign-delete-dialog.component';
import {ClientCampaignWizardComponent} from './client-campaign-wizard.component';

@Injectable()
export class ClientCampaignResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

@Injectable()
export class ClientCampaignWizardResolveParams implements Resolve<any> {

    constructor() {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const tab = route.queryParams['tab'] || 'CLIENT';
        return {
            tab,
        };
    }
}

export const clientCampaignRoute: Routes = [
    {
        path: 'client-campaign',
        component: ClientCampaignComponent,
        resolve: {
            'pagingParams': ClientCampaignResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCampaign.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'client-campaign/:id',
        component: ClientCampaignDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCampaign.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const clientCampaignPopupRoute: Routes = [
    {
        path: 'client-campaign-new',
        component: ClientCampaignPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCampaign.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-campaign/:id/edit',
        component: ClientCampaignPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCampaign.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-campaign/:id/delete',
        component: ClientCampaignDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCampaign.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];

export const clientCampaignWizardRoute: Routes = [
    {
        path: 'client-campaign-wizard',
        component: ClientCampaignWizardComponent,
        resolve: {
            'wizardParams': ClientCampaignWizardResolveParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCampaignWizard.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'client-campaign-wizard/:id/edit',
        component: ClientCampaignWizardComponent,
        resolve: {
            'wizardParams': ClientCampaignWizardResolveParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCampaignWizard.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
];
