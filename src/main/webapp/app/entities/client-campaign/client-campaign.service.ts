import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';

import {JhiDateUtils} from 'ng-jhipster';

import {ClientCampaign} from './client-campaign.model';
import {createRequestOption} from '../../shared';
import {DatePipe} from '@angular/common';
import {ReportCompaign} from '../reports/domain/ReportCompaign';

export type EntityResponseType = HttpResponse<ClientCampaign>;

@Injectable()
export class ClientCampaignService {

    private resourceUrl = SERVER_API_URL + 'api/client-campaigns';

    constructor(private http: HttpClient,
                private dateUtils: JhiDateUtils,
                private datePipe: DatePipe,
    ) {
    }

    create(clientCampaign: ClientCampaign): Observable<EntityResponseType> {
        const copy = this.convert(clientCampaign);
        return this.http.post<ClientCampaign>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    createByWizard(clientCampaign: ClientCampaign): Observable<EntityResponseType> {
        const formData = new FormData();
        // formData.append("file", file, file.name);
        formData.append('campaign', new Blob([JSON.stringify(this.convert(clientCampaign))], {
            type: 'application/json'
        }));

        return this.http.post<ClientCampaign>(SERVER_API_URL + 'api/client-campaigns-wizard', formData, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(clientCampaign: ClientCampaign): Observable<EntityResponseType> {
        const copy = this.convert(clientCampaign);
        return this.http.put<ClientCampaign>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    addClientList(id, clientListId: number): Observable<HttpResponse<any>> {
        const url = `${this.resourceUrl}/${id}/add-client-list/${clientListId}`;
        return this.http.put<void>(url, null, {observe: 'response'});
    }

    addEmailTemplate(id, clientListId: number): Observable<HttpResponse<any>> {
        const url = `${this.resourceUrl}/${id}/add-email-template/${clientListId}`;
        return this.http.put<void>(url, null, {observe: 'response'});
    }

    addOutcome(id, outcomeId: number): Observable<HttpResponse<any>> {
        const url = `${this.resourceUrl}/${id}/add-outcome/${outcomeId}`;
        return this.http.put<void>(url, null, {observe: 'response'});
    }

    addScript(id, scriptId: number): Observable<HttpResponse<any>> {
        const url = `${this.resourceUrl}/${id}/add-script/${scriptId}`;
        return this.http.put<void>(url, null, {observe: 'response'});
    }

    addSurveyQuestion(id, surveyQuestionId: number): Observable<HttpResponse<any>> {
        const url = `${this.resourceUrl}/${id}/add-survey-question/${surveyQuestionId}`;
        return this.http.put<void>(url, null, {observe: 'response'});
    }

    addBookingQuestion(id, bookingQuestionId: number): Observable<HttpResponse<any>> {
        const url = `${this.resourceUrl}/${id}/add-booking-question/${bookingQuestionId}`;
        return this.http.put<void>(url, null, {observe: 'response'});
    }

    addSms(id, smsId: number): Observable<HttpResponse<any>> {
        const url = `${this.resourceUrl}/${id}/add-sms/${smsId}`;
        return this.http.put<void>(url, null, {observe: 'response'});
    }

    addPortalUser(id, portalUserId: number): Observable<HttpResponse<any>> {
        console.log('JON12 called add portal user');
        const url = `${this.resourceUrl}/${id}/add-portal-user/${portalUserId}`;
        return this.http.put<void>(url, null, {observe: 'response'});
    }

    addAppointmentAttendee(id, attendeeId: number): Observable<HttpResponse<any>> {
        const url = `${this.resourceUrl}/${id}/appointment-attendee/${attendeeId}`;
        return this.http.put<void>(url, null, {observe: 'response'});
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ClientCampaign>(`${this.resourceUrl}/${id}`, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ClientCampaign[]>> {
        const options = createRequestOption(req);
        return this.http.get<ClientCampaign[]>(this.resourceUrl, {params: options, observe: 'response'})
            .map((res: HttpResponse<ClientCampaign[]>) => this.convertArrayResponse(res));
    }

    queryAll(): Observable<HttpResponse<ClientCampaign[]>> {
        return this.query({size: 2000});
    }

    consoleRelatedClientCampaigns(): Observable<HttpResponse<ClientCampaign[]>> {
        const url = `${this.resourceUrl}/console/related-client-campaigns`;
        return this.http.get<ClientCampaign[]>(url, {observe: 'response'})
            .map((res: HttpResponse<ClientCampaign[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }

    nameIsUnique(name: string, id?: number): Observable<HttpResponse<boolean>> {
        const params = {name};
        if (id) {
            params['id'] = id.toString();
        }

        return this.http.get<boolean>(`${this.resourceUrl}/nameIsUnique`, {
            observe: 'response', params
        });
    }

    getReportsCampaignById(clientCampaignId: number, req?: any): Observable<HttpResponse<ReportCompaign>> {
        const options = createRequestOption(req);
        const url = `${this.resourceUrl}/report/${clientCampaignId}`;
        return this.http.get<ReportCompaign>(url, {params: options, observe: 'response'});
    }

    getReportsCampaignByIdAndClientList(clientCampaignId: number, clientListId: number, req?: any): Observable<HttpResponse<ReportCompaign>> {
        const options = createRequestOption(req);
        const url = `${this.resourceUrl}/report/${clientCampaignId}/${clientListId}`;
        return this.http.get<ReportCompaign>(url, {params: options, observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ClientCampaign = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ClientCampaign[]>): HttpResponse<ClientCampaign[]> {
        const jsonResponse: ClientCampaign[] = res.body;
        const body: ClientCampaign[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ClientCampaign.
     */
    private convertItemFromServer(clientCampaign: ClientCampaign): ClientCampaign {
        const copy: ClientCampaign = Object.assign({}, clientCampaign);
        copy.createdDate = this.dateUtils
            .convertDateTimeFromServer(clientCampaign.createdDate);
        return copy;
    }

    /**
     * Convert a ClientCampaign to a JSON which can be sent to the server.
     */
    private convert(clientCampaign: ClientCampaign): ClientCampaign {
        const copy: ClientCampaign = Object.assign({}, clientCampaign);

        // workaround
        if (clientCampaign.createdDate instanceof Date) {
            clientCampaign.createdDate = this.datePipe
                .transform(clientCampaign.createdDate, 'yyyy-MM-ddTHH:mm:ss');
        }
        copy.createdDate = this.dateUtils.toDate(clientCampaign.createdDate);

        return copy;
    }
}
