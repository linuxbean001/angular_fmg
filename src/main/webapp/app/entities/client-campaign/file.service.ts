import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';

@Injectable()
export class FileService {

    private resourceUrl = SERVER_API_URL + 'api/file';

    constructor(private http: HttpClient) {
    }

    readLine(file: File): Observable<CsvCell[]> {
        const formData = new FormData();
        formData.append('file', file, file.name);

        return this.http.post<CsvCell[]>(this.resourceUrl + '/read-line', formData, {observe: 'response'})
            .map((res: HttpResponse<CsvCell[]>) => res.body);
    }
}

export class CsvCell {
    constructor(
        public index: number,
        public value?: any
    ) {
    }
}
