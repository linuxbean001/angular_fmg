import {Component, Input, OnInit} from '@angular/core';
import {ConfirmOptions, ConfirmService, User, UserService} from '../../../shared';
import {WizardState} from '../client-campaign-wizard.component';
import {OutcomeService} from '../../outcome';
import {CampaignPortalUser, CampaignPortalUserService} from '../../campaign-portal-user';
import {ClientCampaignService} from '../client-campaign.service';

@Component({
    selector: 'jhi-client-campaign-wizard-access',
    templateUrl: './access.component.html'
})
export class ClientCampaignWizardAccessComponent implements OnInit {

    _wizardState: WizardState;
    @Input()
    set wizardState(value: WizardState) {
        this._wizardState = value;
        this.populateUsersById();
    }

    managers: User[] = [];
    managersLoading = false;

    users: User[] = [];
    usersLoading = false;

    qaUsers: User[] = [];
    qaUsersLoading = false;

    constructor(
        private userService: UserService,
        private outcomeService: OutcomeService,
        private campaignPortalUserService: CampaignPortalUserService,
        private confirmService: ConfirmService,
        private clientCampaignService: ClientCampaignService,
    ) {
    }

    ngOnInit() {
        this.loadUsers();

        // just for test
        // this.addPortalUser();

        // this.portalUsers$ = concat(
        //     of([]), // default items
        //     this.portalUsersInput$.pipe(
        //         debounceTime(200),
        //         distinctUntilChanged(),
        //         tap(() => this.portalUsersLoading = true),
        //         // switchMap(name => this.campaignPortalUserService.query({"name.contains": name})
        //         switchMap(name => {
        //                 const req = {
        //                     //sort: this.sort(),
        //                     page: 0,
        //                     size: -1,
        //                     kind: KIND.CONTAINS_PORTAL_USER,
        //                     "email.contains": name,
        //                 };
        //                 return this.userService.query(req)
        //                     .map((response) => response.body)
        //                     .pipe(
        //                         catchError(() => of([])), // empty list on error
        //                         tap(() => this.portalUsersLoading = false)
        //                     )
        //             }
        //         )
        //     )
        // );
    }

    loadUsers() {
        this.userService.superUsers().toPromise()
            .then((response) => {
                this.managers = response.body;
                this.populateUsersById();
            });
        this.userService.query().toPromise()
            .then((response) => {
                this.users = response.body;
                this.populateUsersById();
            });
        this.userService.qaUsers().toPromise()
            .then((response) => {
                this.qaUsers = response.body;
                this.populateUsersById();
            });
    }

    populateUsersById() {
        // select by resolved value
        const managers = this._wizardState.clientCampaign.managers;
        if (managers.length) {
            const selectedUserIds: number[] = managers.map((x) => x.id);
            const suitable = this.managers.filter((x) => selectedUserIds.indexOf(x.id) > -1);
            if (suitable.length) {
                this._wizardState.clientCampaign.managers = suitable;
            }
        }

        const users = this._wizardState.clientCampaign.users;
        if (users.length) {
            const selectedUserIds: number[] = users.map((x) => x.id);
            const suitable = this.users.filter((x) => selectedUserIds.indexOf(x.id) > -1);
            if (suitable.length) {
                this._wizardState.clientCampaign.users = suitable;
            }
        }

        const qaUsers = this._wizardState.clientCampaign.qaUsers;
        if (qaUsers.length) {
            const selectedUserIds: number[] = qaUsers.map((x) => x.id);
            const suitable = this.qaUsers.filter((x) => selectedUserIds.indexOf(x.id) > -1);
            if (suitable.length) {
                this._wizardState.clientCampaign.qaUsers = suitable;
            }
        }
    }

    addPortalUser() {
        const campaignPortalUser = new CampaignPortalUser(null, null, null, null, null, null);
        this._wizardState.clientCampaign.portalUsers.push(campaignPortalUser);
    }

    removePortalUser(item: CampaignPortalUser) {
        const options = {
            title: 'Confirmation',
            message: `Are you sure you would like to remove portal user ${item.email ? '"' + item.email + '"' : 'Empty email'}?`
        };

        this.confirmService.confirm(options)
            .then((result) => this.removePortalUserInternal(item));
        // .catch(reason => {
        // });
    }

    removePortalUserInternal(item: CampaignPortalUser) {
        console.log('HELLO');
        const portalUsers = this._wizardState.clientCampaign.portalUsers;
        console.log('PORTAL USERS ' + portalUsers);

        portalUsers.splice(portalUsers.indexOf(item), 1);

        console.log('UPDATED PORTAL USERS ' + portalUsers);
        this._wizardState.clientCampaign.portalUsers = portalUsers;
    }

    exitEditingMode(item: CampaignPortalUser, $event: any) {
        const potentialEmail = ($event.target.value || '').trim();
        if (!potentialEmail) {
            if (!item.email) {
                return;
            }

            const opts: ConfirmOptions = {
                title: 'Confirmation',
                message: `Are you sure you would like to remove portal user?`
            };

            this.confirmService.confirm(opts)
                .then((result) => this.removePortalUserInternal(item))
                .catch((reason) => $event.target.value = item.email);

            return;
        }

        if (item.email === potentialEmail) {
            return;
        }

        if (potentialEmail.length < 3 || potentialEmail.indexOf('@') === -1) {
            this.warningIncorrectEmail(potentialEmail)
                .then((result) => $event.target.value = item.email)
                .catch((reason) => $event.target.value = item.email);
            return;
        }

        const options: ConfirmOptions = {
            title: 'Confirmation',
            message: `Are you sure you would like portal user with email address \"${potentialEmail}\"
            to be added to given access to campaign ${this._wizardState.clientCampaign.name || '\"not specified\"'}?`
        };

        this.confirmService.confirm(options)
            .then((result) => {
                item.email = potentialEmail;

                if (item.id) {
                    this.campaignPortalUserService.update(item).toPromise();
                } else {
                    const id = this._wizardState.clientCampaign.id;
                    this.campaignPortalUserService.create(item).toPromise()
                        .then((res) => {
                            item.id = res.body.id;

                            this.clientCampaignService.addPortalUser(id, item.id).toPromise();
                        });
                }
            })
            .catch((reason) => $event.target.value = item.email);
    }

    warningIncorrectEmail(email: string) {
        const options: ConfirmOptions = {
            title: 'Warning',
            message: `Incorrect email \"${email}\"`,
            noHidden: true,
            yesCaption: 'OK'
        };

        return this.confirmService.confirm(options);
    }
}
