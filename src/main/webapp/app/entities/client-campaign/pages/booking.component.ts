import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {WizardState} from '../client-campaign-wizard.component';
import {ClientCampaign} from '../client-campaign.model';
import {ClientCampaignService} from '../client-campaign.service';
import {Subscription} from 'rxjs';
import {JhiEventManager} from 'ng-jhipster';
import {BaseEntity, ConfirmService} from '../../../shared';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {
    BookingQuestion,
    BookingQuestionDialogComponent,
    BookingQuestionPopupService,
    BookingQuestionService
} from '../../booking-question';

@Component({
    selector: 'jhi-client-campaign-wizard-booking',
    templateUrl: './booking.component.html'
})
export class ClientCampaignWizardBookingComponent implements OnInit, OnDestroy {

    _wizardState: WizardState;
    @Input()
    set wizardState(value: WizardState) {
        this._wizardState = value;
        this.populateSortedBookingQuestions();
    }

    campaigns: ClientCampaign[];
    campaign: ClientCampaign;

    sortedBookingQuestions: BookingQuestion[] = [];

    eventSubscriber: Subscription;

    constructor(
        private clientCampaignService: ClientCampaignService,
        private bookingQuestionService: BookingQuestionService,
        private bookingQuestionPopupService: BookingQuestionPopupService,
        private confirmService: ConfirmService,
        private eventManager: JhiEventManager,
    ) {
    }

    ngOnInit() {
        this.loadCampaigns();
        this.registerChangeInBookingQuestions();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    loadCampaigns() {
        this.clientCampaignService.queryAll().toPromise()
            .then((response) => {
                const allCampaigns = response.body;
                // exclude current campaign
                this.campaigns = allCampaigns.filter((cc) => cc.id !== this._wizardState.clientCampaign.id);
            });
    }

    populateSortedBookingQuestions() {
        this.sortedBookingQuestions = this._wizardState.clientCampaign.bookingQuestions.sort((a, b) => a.order - b.order);
    }

    addCampaignsBookingQuestions() {
        if (!this.campaign) {
            return;
        }

        this.clientCampaignService.find(this.campaign.id).toPromise()
            .then((response) => {
                const clientCampaign = response.body;
                const sorted = clientCampaign.bookingQuestions.sort((a, b) => a.order - b.order);
                this.addBookingQuestions(...sorted);
                delete this.campaign;
            });
    }

    addBookingQuestions(...bookingQuestions: BaseEntity[]) {
        /*
         * serial executes Promises sequentially.
         * @param {funcs} An array of funcs that return promises.
         * @example
         * const urls = ['/url1', '/url2', '/url3']
         * serial(urls.map(url => () => $.ajax(url)))
         *     .then(console.log.bind(console))
         */
        // see https://stackoverflow.com/a/41115086/579680
        const serial = (funcs) =>
            funcs.reduce((promise, func) =>
                promise.then((result) => func().then(Array.prototype.concat.bind(result))), Promise.resolve([]));

        // double arrows
        const promises = bookingQuestions.map((bookingQuestion: BookingQuestion) => () => {
            return this.bookingQuestionService.find(bookingQuestion.id).toPromise()
                .then((response) => {
                    const existedBookingQuestion = response.body;
                    const shallow = this.shallowCopy(existedBookingQuestion);

                    return this.bookingQuestionService.create(shallow).toPromise()
                        .then((res) => {
                            const savedBookingQuestion = res.body;
                            const id = this._wizardState.clientCampaign.id;
                            this._wizardState.clientCampaign.bookingQuestions.push(savedBookingQuestion);
                            if (id) {
                                return this.clientCampaignService.addBookingQuestion(id, savedBookingQuestion.id).toPromise();
                            } else {
                                return Promise.resolve(null);
                            }
                        });
                });
        });

        serial(promises)
            .then(() => this.populateSortedBookingQuestions());
    }

    shallowCopy(value: BookingQuestion) {
        const shallow = {...value};
        if (shallow.fields) {
            shallow.fields = shallow.fields.map((field) => {
                const newField = {...field};
                delete newField.id;
                return newField;
            });
        }
        delete shallow.id;
        return shallow;
    }

    createNewBookingQuestion() {
        const bookingQuestion = new BookingQuestion();
        this.bookingQuestionPopupService.openStandalone(BookingQuestionDialogComponent as Component, bookingQuestion)
            .then((ngbModalRef: NgbModalRef) => {
                ngbModalRef.result
                    .then((sq) => {
                        const id = this._wizardState.clientCampaign.id;
                        if (id) {
                            this.clientCampaignService.addBookingQuestion(id, sq.id).toPromise();
                        }
                        this._wizardState.clientCampaign.bookingQuestions.push(sq);
                        this.populateSortedBookingQuestions();
                    });
            });
    }

    registerChangeInBookingQuestions() {
        this.eventSubscriber = this.eventManager.subscribe('bookingQuestionListModification', (event) => {
            const bookingQuestions = this._wizardState.clientCampaign.bookingQuestions;
            if (event.content === 'Deleted an bookingQuestion') {
                const deletedId = event.id;
                bookingQuestions.filter((sq) => sq.id === deletedId)
                    .forEach((sq) => {
                        bookingQuestions.splice(bookingQuestions.indexOf(sq), 1);
                        this.populateSortedBookingQuestions();
                    });
            } else {
                const bookingQuestion = event.bookingQuestion;
                bookingQuestions.filter((x) => x.id === bookingQuestion.id)
                    .forEach((x) => {
                        const index = bookingQuestions.indexOf(x);
                        // user can reassign to another campaign
                        if (this._wizardState.clientCampaign.id === bookingQuestion.clientCampaignId) {
                            bookingQuestions[index] = bookingQuestion;
                        } else {
                            bookingQuestions.splice(index, 1);
                        }
                        this.populateSortedBookingQuestions();
                    });
            }
        });
    }

    canMoveDown(bookingQuestion: BookingQuestion, i: number) {
        return i < this.sortedBookingQuestions.length - 1;
    }

    canMoveUp(bookingQuestion: BookingQuestion, i: number) {
        return i > 0;
    }

    moveDown(bookingQuestion: BookingQuestion) {
        const i = this.sortedBookingQuestions.indexOf(bookingQuestion);
        const anotherBookingQuestion = this.sortedBookingQuestions[i + 1];
        this.swapOrders(bookingQuestion, anotherBookingQuestion);
    }

    moveUp(bookingQuestion: BookingQuestion) {
        const i = this.sortedBookingQuestions.indexOf(bookingQuestion);
        const anotherBookingQuestion = this.sortedBookingQuestions[i - 1];
        this.swapOrders(bookingQuestion, anotherBookingQuestion);
    }

    swapOrders(bookingQuestion, anotherBookingQuestion: BookingQuestion) {
        // swap orders
        const order = bookingQuestion.order;
        bookingQuestion.order = anotherBookingQuestion.order;
        anotherBookingQuestion.order = order;

        this.populateSortedBookingQuestions();

        // const a: BookingQuestion = {id: bookingQuestion.id, order: bookingQuestion.order} as BookingQuestion;
        this.bookingQuestionService.update(bookingQuestion).toPromise();

        // const b: BookingQuestion = {id: anotherBookingQuestion.id, order: anotherBookingQuestion.order} as BookingQuestion;
        this.bookingQuestionService.update(anotherBookingQuestion).toPromise();
    }
}
