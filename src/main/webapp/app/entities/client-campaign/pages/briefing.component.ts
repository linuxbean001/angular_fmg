import {Component, ElementRef, Input, OnInit, QueryList, ViewChildren} from '@angular/core';
import {ConfirmService, User, UserService} from '../../../shared';
import {OutcomeService} from '../../outcome';
import {ClientCampaignService} from '../client-campaign.service';
import {AppointmentAttendee, AppointmentAttendeeService, AppointmentDays} from '../../appointment-attendee';
import {ClientCampaign} from '../client-campaign.model';

@Component({
    selector: 'jhi-client-campaign-wizard-briefing',
    templateUrl: './briefing.component.html'
})
export class ClientCampaignWizardBriefingComponent implements OnInit {

    _clientCampaign: ClientCampaign;
    @Input()
    set clientCampaign(value: ClientCampaign) {
        this._clientCampaign = value;
        this.populateUsersById();
    }

    @Input()
    mode = BriefingComponentMode.WIZARD;
    BriefingComponentMode = BriefingComponentMode;

    @Input()
    panelsNgStyle: { [key: string]: string; };

    users: User[] = [];
    AppointmentDays: typeof AppointmentDays = AppointmentDays;

    @ViewChildren('appointmentNames') appointmentNames: QueryList<ElementRef>;

    constructor(
        private userService: UserService,
        private outcomeService: OutcomeService,
        private appointmentAttendeeService: AppointmentAttendeeService,
        private confirmService: ConfirmService,
        private clientCampaignService: ClientCampaignService,
    ) {
    }

    ngOnInit() {
        this.loadUsers();
    }

    loadUsers() {
        this.userService.superUsersWithSameUserCompany().toPromise()
            .then((response) => {
                this.users = response.body;
                this.populateUsersById();
            });
    }

    populateUsersById() {
        // select by resolved value
        const users = this._clientCampaign.managers;
        if (users.length) {
            const selectedUserIds: number[] = users.map((x) => x.id);
            const suitable = this.users.filter((x) => selectedUserIds.indexOf(x.id) > -1);
            if (suitable.length) {
                this._clientCampaign.managers = suitable;
            }
        }
    }

    addAttendee() {
        const appointmentAttendees = this._clientCampaign.appointmentAttendees;
        const name = `Appointment Attendee #${appointmentAttendees.length + 1}`;

        const clientCampaignId = this._clientCampaign.id;
        const clientCampaignName = this._clientCampaign.name;

        const appointmentAttendee = new AppointmentAttendee(null, name, clientCampaignId, clientCampaignName);
        const length = appointmentAttendees.push(appointmentAttendee);

        this.appointmentAttendeeService.create(appointmentAttendee).toPromise()
            .then((res) => {
                const savedAppointmentAttendee = res.body;
                appointmentAttendees[length - 1] = savedAppointmentAttendee;

                setTimeout(() => {
                    const elementRef = this.appointmentNames.find((item, index) => index === length - 1);
                    elementRef.nativeElement.focus();
                }, 0);
            })
            .catch((reason) => {
                appointmentAttendees.splice(length - 1, 1);
                throw reason;
            });
    }

    removeAttendee(item: AppointmentAttendee) {
        const options = {
            title: 'Confirmation',
            message: `Are you sure you would like to remove appointment attendee ${item.name ? '"' + item.name + '"' : 'Empty name'}? This operation can not be undone`
        };

        this.confirmService.confirm(options)
            .then((result) => this.removeAttendeeInternal(item));
    }

    removeAttendeeInternal(item: AppointmentAttendee) {
        const appointmentAttendees = this._clientCampaign.appointmentAttendees;
        const index = appointmentAttendees.indexOf(item);
        appointmentAttendees.splice(index, 1);

        this.appointmentAttendeeService.delete(item.id).toPromise()
            .catch((reason) => {
                appointmentAttendees.splice(index, 0, item);
                throw reason;
            });
    }

    exitAttendee(appointmentAttendee: AppointmentAttendee, $event: FocusEvent) {
        this.appointmentAttendeeService.update(appointmentAttendee).toPromise();
    }

    isReadonlyMode() {
        return this.mode === BriefingComponentMode.CONSOLE;
    }

    // exitEditingMode(item: AppointmentAttendee, $event: any) {
    //     const potentialEmail = ($event.target.value || '').trim();
    //     if (!potentialEmail) {
    //         if (!item.email) {
    //             return;
    //         }
    //
    //         const opts: ConfirmOptions = {
    //             title: 'Confirmation',
    //             message: `Are you sure you would like to remove portal user?`
    //         };
    //
    //         this.confirmService.confirm(opts)
    //             .then((result) => this.removeAttendeeInternal(item))
    //             .catch((reason) => $event.target.value = item.email);
    //
    //         return;
    //     }
    //
    //     if (item.email === potentialEmail) {
    //         return;
    //     }
    //
    //     if (potentialEmail.length < 3 || potentialEmail.indexOf('@') === -1) {
    //         this.warningIncorrectEmail(potentialEmail)
    //             .then((result) => $event.target.value = item.email)
    //             .catch((reason) => $event.target.value = item.email);
    //         return;
    //     }
    //
    //     const options: ConfirmOptions = {
    //         title: 'Confirmation',
    //         message: `Are you sure you would like portal user with email address \"${potentialEmail}\"
    //         to be added to given access to campaign ${this._clientCampaign.name || '\"not specified\"'}?`
    //     };
    //
    //     this.confirmService.confirm(options)
    //         .then((result) => {
    //             item.email = potentialEmail;
    //
    //             if (item.id) {
    //                 this.appointmentAttendeeService.update(item).toPromise();
    //             } else {
    //                 const id = this._clientCampaign.id;
    //                 this.appointmentAttendeeService.create(item).toPromise()
    //                     .then((res) => {
    //                         item.id = res.body.id;
    //
    //                         this.clientCampaignService.addAppointmentAttendee(id, item.id).toPromise();
    //                     });
    //             }
    //         })
    //         .catch((reason) => $event.target.value = item.email);
    // }
    //
    // warningIncorrectEmail(email: string) {
    //     const options: ConfirmOptions = {
    //         title: 'Warning',
    //         message: `Incorrect email \"${email}\"`,
    //         noHidden: true,
    //         yesCaption: 'OK'
    //     };
    //
    //     return this.confirmService.confirm(options);
    // }
}

export class BriefingComponentMode {
    /**
     * Default.
     */
    static WIZARD = new BriefingComponentMode();
    /**
     * Console (readonly) mode.
     */
    static CONSOLE = new BriefingComponentMode();
}
