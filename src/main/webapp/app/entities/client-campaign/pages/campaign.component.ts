import {Component, ElementRef, Input, OnInit, QueryList, ViewChildren} from '@angular/core';
import {WizardState} from '../client-campaign-wizard.component';
import {CampaignPortalUser} from '../../campaign-portal-user';
import {ConfirmService} from '../../../shared';
import {ClientCampaignAddDoc, ClientCampaignAddDocService} from '../../client-campaign-add-doc';

@Component({
    selector: 'jhi-client-campaign-wizard-campaign',
    templateUrl: './campaign.component.html'
})
export class ClientCampaignWizardCampaignComponent implements OnInit {

    _wizardState: WizardState;
    @Input()
    set wizardState(value: WizardState) {
        this._wizardState = value;
    }

    @ViewChildren('addDocLinks') addDocLinks: QueryList<ElementRef>;

    constructor(
        private confirmService: ConfirmService,
        private clientCampaignAddDocService: ClientCampaignAddDocService,
    ) {
    }

    ngOnInit() {
    }

    save() {
        // nothing
    }

    addAdditionalDocument() {
        const addDocs = this._wizardState.clientCampaign.addDocs;

        const clientCampaignId = this._wizardState.clientCampaign.id;
        const clientCampaignName = this._wizardState.clientCampaign.name;

        const addDoc = new ClientCampaignAddDoc(null, null, null, clientCampaignId, clientCampaignName);
        const length = addDocs.push(addDoc);

        this.clientCampaignAddDocService.create(addDoc).toPromise()
            .then((res) => {
                const savedAddDoc = res.body;
                addDocs[length - 1] = savedAddDoc;

                setTimeout(() => {
                    const elementRef = this.addDocLinks.find((item, index) => index === length - 1);
                    elementRef.nativeElement.focus();
                }, 0);
            })
            .catch((reason) => {
                addDocs.splice(length - 1, 1);
                throw reason;
            });

        // const addDoc = new ClientCampaignAddDoc(null, null, null);
        // this._wizardState.clientCampaign.addDocs.push(addDoc);
    }

    removeAdditionalDocument(item: ClientCampaignAddDoc) {
        const options = {
            title: 'Confirmation',
            message: `Are you sure you would like to remove additional document ${item.link ? '"' + item.link + '"' : 'Empty link'}?`
        };

        this.confirmService.confirm(options)
            .then((result) => this.removeAdditionalDocumentInternal(item));
    }

    removeAdditionalDocumentInternal(item: CampaignPortalUser) {
        const addDocs = this._wizardState.clientCampaign.addDocs;
        const index = addDocs.indexOf(item);
        addDocs.splice(index, 1);

        this.clientCampaignAddDocService.delete(item.id).toPromise()
            .catch((reason) => {
                addDocs.splice(index, 0, item);
                throw reason;
            });
    }

    exitAddDoc(addDoc: ClientCampaignAddDoc, $event: FocusEvent) {
        this.clientCampaignAddDocService.update(addDoc).toPromise();
    }
}
