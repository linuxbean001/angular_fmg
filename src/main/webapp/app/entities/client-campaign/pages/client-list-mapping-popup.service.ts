import {Component, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {MappingState} from './mapping.component';

@Injectable()
export class ClientListMappingPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
    ) {
        this.ngbModalRef = null;
    }

    openStandalone(component: Component, mappingState: MappingState): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            this.ngbModalRef = this.clientListMappingModalRef(component, mappingState, false);
            resolve(this.ngbModalRef);
        });
    }

    clientListMappingModalRef(component: Component, mappingState: MappingState, sendToServer = true): NgbModalRef {
        // hack the size https://github.com/ng-bootstrap/ng-bootstrap/issues/1309#issuecomment-289310540
        const xl = 'xl' as 'lg';
        const modalRef = this.modalService.open(component, {size: xl, backdrop: 'static'});
        modalRef.componentInstance.mappingState = mappingState;
        modalRef.result.then((result) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
