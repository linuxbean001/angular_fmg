import {Component, Input, OnInit} from '@angular/core';
import {Observable} from 'rxjs';

import {ClientCompany, ClientCompanyService} from '../../client-company';
import {ClientCompanyStatus, ClientCompanyStatusService} from '../../client-company-status';
import {WizardState} from '../client-campaign-wizard.component';
import {ConfirmOptions, ConfirmService} from '../../../shared';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Industry, IndustryService} from '../../industry';

@Component({
    selector: 'jhi-client-campaign-wizard-client',
    templateUrl: './client.component.html'
})
export class ClientCampaignWizardClientComponent implements OnInit {

    clientCompanies: ClientCompany[] = [];
    clientCompaniesLoading = false;
    industries: Industry[];

    _wizardData: WizardState;
    @Input()
    set wizardData(wizardData: WizardState) {
        this._wizardData = wizardData;
        // this.populateClientCompanyById();
    }

    fakeAllCompanyStatus = new ClientCompanyStatus(0, 'All');
    clientCompanyStatuses$: Observable<ClientCompanyStatus[]>;
    selectedClientCompanyStatus = new ClientCompanyStatus(1, 'ACTIVE');

    constructor(
        private clientCompanyService: ClientCompanyService,
        private clientCompanyStatusService: ClientCompanyStatusService,
        private industryService: IndustryService,
        private confirmService: ConfirmService
    ) {
    }

    ngOnInit() {
        this.clientCompanyStatuses$ = this.clientCompanyStatusService.query()
            .map((response) => {
                return [this.fakeAllCompanyStatus].concat(response.body);
            });

        this.loadClientCompanies();
        this.loadIndustries();
    }

    loadClientCompanies() {
        const req = {};
        if (this.selectedClientCompanyStatus && this.selectedClientCompanyStatus.id !== 0) {
            req['statusId.equals'] = this.selectedClientCompanyStatus.id;
        }

        this.clientCompanyService.query(req).toPromise()
            .then((response) => {
                this.clientCompanies = response.body;
                // this.populateClientCompanyById();
            });
    }

    loadIndustries() {
        this.industryService.query().toPromise()
            .then((res: HttpResponse<Industry[]>) => {
                this.industries = res.body;
            });
    }

    // populateClientCompanyById() {
    //     if (!this.clientCompanies.length) {
    //         return;
    //     }
    //     // select by resolved value
    //     let item = this._wizardData.clientPage.clientCompany;
    //     if (item) {
    //         let suitable = this.clientCompanies.filter(x => x.id === item.id);
    //         if (suitable.length) {
    //             this._wizardData.clientPage.clientCompany = suitable[0];
    //         }
    //     }
    // }

    addClientCompanyPromise(name: string) {
        const options: ConfirmOptions = {
            title: 'Confirmation',
            message: 'Do you want to create a new client with name "' + name + '"'
        };

        return this.confirmService.confirm(options)
            .then((value) => {
                const clientCompany = new ClientCompany();
                clientCompany.name = name;
                if (this.selectedClientCompanyStatus && this.selectedClientCompanyStatus.id !== 0) {
                    clientCompany.statusId = this.selectedClientCompanyStatus.id;
                }

                return this.clientCompanyService.create(clientCompany).toPromise()
                    .then((response) => {
                        const savedClientCompany = response.body;

                        // apply newly created client
                        this._wizardData.clientCampaign.clientCompanyId = savedClientCompany.id;
                        this._wizardData.clientCampaign.clientCompanyName = savedClientCompany.name;

                        // why I can't simple do this.clientCompanies.push(savedClientCompany) ???
                        this.loadClientCompanies();
                    });
            });
    }

    statusChange(newStatus) {
        // if status changed to something different, except cleared - clear selected company
        if (newStatus) {
            this._wizardData.clientCampaign.clientCompanyId = null;
            this._wizardData.clientCampaign.clientCompanyName = null;
            // this.clientCompaniesInput$.next(null);
        }
        this.loadClientCompanies();
    }

    changeClientCompany(clientCompanyId: number) {
        if (clientCompanyId) {
            const clientCompany = this.clientCompanies.find((x) => x.id === clientCompanyId);
            this._wizardData.clientCampaign.clientCompanyId = clientCompany.id;
            this._wizardData.clientCampaign.clientCompanyName = clientCompany.name;
            this._wizardData.clientCampaign.clientCompanyIndustryId = clientCompany.industryId;
        } else {
            delete this._wizardData.clientCampaign.clientCompanyId;
            delete this._wizardData.clientCampaign.clientCompanyName;
            delete this._wizardData.clientCampaign.clientCompanyIndustryId;
        }
    }

    trackIndustryById(index: number, item: Industry) {
        return item.id;
    }

    changeClientCompanyIndustry($event: number) {
        const clientCompanyId = this._wizardData.clientCampaign.clientCompanyId;
        if (!clientCompanyId) {
            return;
        }

        const industryId = this._wizardData.clientCampaign.clientCompanyIndustryId;

        this.clientCompanyService.updateIndustry(clientCompanyId, industryId).toPromise();
    }
}
