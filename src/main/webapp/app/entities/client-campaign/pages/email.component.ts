import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {WizardState} from '../client-campaign-wizard.component';
import {
    EmailTemplate,
    EmailTemplateDialogComponent,
    EmailTemplatePopupService,
    EmailTemplateService
} from '../../email-template';
import {ClientCampaignService} from '../client-campaign.service';
import {ClientCampaign} from '../client-campaign.model';
import {Router} from '@angular/router';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {Subscription} from 'rxjs';
import {JhiEventManager} from 'ng-jhipster';

@Component({
    selector: 'jhi-client-campaign-wizard-email',
    templateUrl: './email.component.html'
})
export class ClientCampaignWizardEmailComponent implements OnInit, OnDestroy {

    _wizardState: WizardState;
    @Input()
    set wizardState(value: WizardState) {
        this._wizardState = value;
    }

    clientCampaigns: ClientCampaign[];
    clientCampaign: ClientCampaign;
    emailTemplates: EmailTemplate[] = [];
    emailTemplate: EmailTemplate;

    eventSubscriber: Subscription;

    constructor(
        public clientCampaignService: ClientCampaignService,
        public emailTemplateService: EmailTemplateService,
        public emailTemplatePopupService: EmailTemplatePopupService,
        public router: Router,
        private eventManager: JhiEventManager,
    ) {
    }

    ngOnInit() {
        this.loadCampaigns();
        this.registerChangeInEmailTemplates();
    }

    ngOnDestroy(): void {
        this.eventManager.destroy(this.eventSubscriber);
    }

    loadCampaigns() {
        this.clientCampaignService.query().toPromise()
            .then((response) => {
                const allCampaigns = response.body;
                // exclude current campaign
                this.clientCampaigns = allCampaigns.filter((cc) => cc.id !== this._wizardState.clientCampaign.id);
            });
    }

    createNewEmailTemplate() {
        this.editInternal(new EmailTemplate())
            .then((template) => {
                const id = this._wizardState.clientCampaign.id;
                if (id) {
                    this.clientCampaignService.addEmailTemplate(id, template.id).toPromise();
                }
                this._wizardState.clientCampaign.emailTemplates.push(template);
            });
    }

    editTemplate(emailTemplate: EmailTemplate) {
        this.editInternal(emailTemplate)
            .then((template) => {
                const emailTemplates = this._wizardState.clientCampaign.emailTemplates;
                emailTemplates[emailTemplates.indexOf(emailTemplate)] = template;
            });
    }

    editInternal(template: EmailTemplate): Promise<EmailTemplate> {
        return new Promise<EmailTemplate>((resolve, reject) => {
            // make shallow copy
            const shallow = {...template};
            this.emailTemplatePopupService.openStandalone(EmailTemplateDialogComponent as Component, shallow)
                .then((ngbModalRef: NgbModalRef) => {
                    ngbModalRef.result
                        .then(resolve)
                        .catch(reject);
                });
        });
    }

    cloneEmail() {
        if (!this.clientCampaign || !this.emailTemplate) {
            return;
        }

        // make shallow copy
        const newEmailTemplate = {...this.emailTemplate};
        delete newEmailTemplate.clientCampaignId;
        delete newEmailTemplate.clientCampaignName;
        delete newEmailTemplate.id;

        this.emailTemplateService.create(newEmailTemplate).toPromise()
            .then((response) => {
                const emailTemplate = response.body;
                const id = this._wizardState.clientCampaign.id;
                if (id) {
                    this.clientCampaignService.addEmailTemplate(id, emailTemplate.id).toPromise();
                }
                this._wizardState.clientCampaign.emailTemplates.push(emailTemplate);
            });
    }

    registerChangeInEmailTemplates() {
        this.eventSubscriber = this.eventManager.subscribe('emailTemplateListModification', (event) => {
            if (event.content === 'Deleted an emailTemplate') {
                const deletedId = event.id;
                const emailTemplates = this._wizardState.clientCampaign.emailTemplates;
                emailTemplates.filter((emailTemplate) => emailTemplate.id === deletedId)
                    .forEach((emailTemplate) => {
                        emailTemplates.splice(emailTemplates.indexOf(emailTemplate), 1);
                    });
            } else {
                const emailTemplate = event.emailTemplate;
                const emailTemplates = this._wizardState.clientCampaign.emailTemplates;
                emailTemplates.filter((x) => x.id === emailTemplate.id)
                    .forEach((x) => {
                        const index = emailTemplates.indexOf(x);
                        // user can reassign to another campaign
                        if (this._wizardState.clientCampaign.id === emailTemplate.clientCampaignId) {
                            emailTemplates[index] = emailTemplate;
                        } else {
                            emailTemplates.splice(index, 1);
                        }
                    });
            }
        });
    }
}
