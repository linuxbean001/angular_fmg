import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {ClientCampaign} from '../client-campaign.model';

@Component({
    selector: 'jhi-client-campaign-wizard-gdpr',
    templateUrl: './gdpr.component.html'
})
export class ClientCampaignWizardGdprComponent implements OnInit {

    _clientCampaign: ClientCampaign;
    @Input()
    set clientCampaign(value: ClientCampaign) {
        this._clientCampaign = value;
    }

    @Input()
    mode = GdprComponentMode.WIZARD;
    GdprComponentMode = GdprComponentMode;
    @Input()
    panelsNgStyle: { [key: string]: string; };

    @ViewChild('liaHyperlink')
    liaHyperlinkField: ElementRef;

    constructor() {
    }

    ngOnInit() {
    }

    isReadonlyMode() {
        return this.mode === GdprComponentMode.CONSOLE;
    }
}

export class GdprComponentMode {
    /**
     * Default.
     */
    static WIZARD = new GdprComponentMode();
    /**
     * Console (readonly) mode.
     */
    static CONSOLE = new GdprComponentMode();
}
