import {Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {BaseEntity, ConfirmService, Principal, User, UserService} from '../../../shared';
import {WizardState} from '../client-campaign-wizard.component';
import {EmailTemplate} from '../../email-template';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {ClientListMappingPopupService} from './client-list-mapping-popup.service';
import {ClientCampaignWizardMappingComponent, MappingState} from './mapping.component';
import {FileService} from '../file.service';
import {ClientList} from '../../client-list';
import {Subscription} from 'rxjs';
import {JhiEventManager} from 'ng-jhipster';
import {ClientCampaignService} from '../client-campaign.service';
import {CampaignPortalUser} from '../../campaign-portal-user';

@Component({
    selector: 'jhi-client-campaign-wizard-list',
    templateUrl: './list.component.html'
})
export class ClientCampaignWizardListComponent implements OnInit, OnDestroy {

    _wizardState: WizardState;
    @Input()
    set wizardState(value: WizardState) {
        this._wizardState = value;
    }

    @ViewChild('fileInput')
    fileInputVariable: ElementRef;

    users: User[];
    usersLoading = false;

    eventSubscriber: Subscription;

    listName: string;
    listFile: File;

    lstOfPortalUsersLists: CampaignPortalUser[][] = [];
    portalUsersNameForLists: string[] = [];

    constructor(
        private userService: UserService,
        private clientListMappingPopupService: ClientListMappingPopupService,
        private clientCampaignService: ClientCampaignService,
        private fileService: FileService,
        private confirmService: ConfirmService,
        private eventManager: JhiEventManager,
        private principal: Principal,
    ) {
    }

    ngOnInit() {
        this.loadUsers();
        this.registerChangeInClientLists();
        this.setPortalUsersNamesForLists();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    loadUsers() {
        this.userService.superUsersWithSameUserCompany().toPromise()
            .then((response) => this.users = response.body);
    }

    handleFileInput(files: FileList) {
        this.listFile = files.item(0);
    }

    getFileNameWithoutExtension(): string {
        const file = this.listFile;
        if (!file || !file.name) {
            return null;
        }
        const name = file.name;
        const pos = name.lastIndexOf('.');
        if (pos > 0 && pos < (name.length - 1)) {
            // there is a '.' and it's not the first, or last character.
            return name.substring(0, pos);
        }
        return name;
    }

    createNewClientList() {
        this.createNewClientListInternal()
            .then((clientList) => {
                const id = this._wizardState.clientCampaign.id;
                if (id) {
                    this.clientCampaignService.addClientList(id, clientList.id).toPromise();
                }

                this._wizardState.clientCampaign.clientLists.push(clientList);
                this.fileInputVariable.nativeElement.value = '';
                delete this.listName;
                delete this.listFile;
            });
    }

    createNewClientListInternal(): Promise<ClientList> {
        if (!this.listName) {
            const options = {
                title: 'Warning',
                message: 'Please specify a List Name',
                noHidden: true,
                yesCaption: 'Ok'
            };
            this.confirmService.confirm(options);
            return;
        }

        if (!this.listFile) {
            const options = {
                title: 'Warning',
                message: 'Please choose a CSV File',
                noHidden: true,
                yesCaption: 'Ok'
            };
            this.confirmService.confirm(options);
            return;
        }

        return new Promise<EmailTemplate>((resolve, reject) => {
            this.fileService.readLine(this.listFile).toPromise()
                .then((headers) => {
                    const mappingState = new MappingState(this.listName, this.listFile, headers);
                    this.clientListMappingPopupService.openStandalone(ClientCampaignWizardMappingComponent as Component, mappingState)
                        .then((ngbModalRef: NgbModalRef) => {
                            ngbModalRef.result
                                .then(resolve)
                                .catch(reject);
                        });
                });
        });
    }

    registerChangeInClientLists() {
        this.eventSubscriber = this.eventManager.subscribe('clientListListModification', (event) => {
            if (event.content === 'Deleted an clientList') {
                const deletedId = event.id;
                const clientLists = this._wizardState.clientCampaign.clientLists;
                clientLists.filter((clientList) => clientList.id === deletedId)
                    .forEach((clientList) => {
                        clientLists.splice(clientLists.indexOf(clientList), 1);
                    });
            } else {

                const clientList = event.clientList;
                const clientLists = this._wizardState.clientCampaign.clientLists;
                for (let i = 0; i < clientLists.length; i++) {
                    if (clientLists[i].id === clientList.id) {
                        clientLists[i].name = clientList.name;
                        clientLists[i].portalUsers = clientList.portalUsers;

                    }
                }

                 // const clientLists = this._wizardState.clientCampaign.clientLists;
                 // clientLists.filter((cl) => cl.id === cl.id)
                 //     .forEach((cl) => {
                 //         clientLists[clientLists.indexOf(cl)] = clientList;
                 //     });

                 this.setPortalUsersNamesForLists();
            }
        });
    }

    setPortalUsersNamesForLists() {
        this.lstOfPortalUsersLists = [];
        this.portalUsersNameForLists = [];
        for (const val of this._wizardState.clientCampaign.clientLists) {
            let names = '';
            for (const user of val.portalUsers) {
                if (user.email == null ||
                    user.email.length === 0) {
                    // ignore
                } else {
                    if (names.length > 0) {
                        names = names + ', ';
                    }
                    names = names + user.email;
                }
            }
            this.portalUsersNameForLists.push(names);
            this.lstOfPortalUsersLists.push(val.portalUsers);
        }
    }
}
