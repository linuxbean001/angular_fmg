import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Field, FieldService} from '../../field';
import {DragulaService} from 'ng2-dragula';
import {Subscription} from 'rxjs';
import {CsvCell} from '../file.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ClientList, ClientListService} from '../../client-list';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {ClientListMapping} from '../client-campaign.model';
import {ConfirmService} from '../../../shared';

@Component({
    selector: 'jhi-client-campaign-wizard-mapping',
    templateUrl: './mapping.component.html'
})
export class ClientCampaignWizardMappingComponent implements OnInit, OnDestroy {

    mappingState: MappingState;

    // filled from wizardState
    headers: CsvCell[];
    // value contains only one element but defined as array (dragula wants it)
    elementByIndex: { [index: number]: CsvCell[]; } = {};

    // filled from a server
    oldFields: OldField[] = [];
    // value contains only one element but defined as array (dragula wants it)
    elementByField: { [id: number]: CsvCell[]; } = {};

    // added by user
    newFields: NewField[] = [];

    // added by user
    newFieldsNoMapping: NewFieldNoMapping[] = [];

    subs = new Subscription();
    COLUMN = 'COLUMN';
    onlyMapped = false;
    newFieldName = null;

    @ViewChild('enterHeaderName')
    enterHeaderNameField: ElementRef;

    editingHeader: CsvCell;
    enteredHeaderName: string;
    nameByIndex: { [index: number]: string; } = {};

    isSaving = false;

    constructor(
        private fieldService: FieldService,
        private dragulaService: DragulaService,
        private confirmService: ConfirmService,
        public activeModal: NgbActiveModal,
        public clientListService: ClientListService,
    ) {
    }

    ngOnInit() {
        this.dragulaService.createGroup(this.COLUMN, {
            revertOnSpill: true,
            accepts: (el, target, source, sibling) => {
                const headerIndex = el.getAttribute('data-header-index');

                // left side
                const fieldId = target.getAttribute('data-field-id');
                if (fieldId !== null && this.elementByField[fieldId] && this.elementByField[fieldId].length === 0) {
                    return true;
                }

                // right side
                const targetHeaderIndex = target.getAttribute('data-header-index');
                if (targetHeaderIndex !== null && this.elementByIndex[targetHeaderIndex] && this.elementByIndex[targetHeaderIndex].length === 0) {
                    // allow to drop to a column's container or to a field
                    return headerIndex === targetHeaderIndex;
                }

                return false;
            }
        });

        this.update();
    }

    ngOnDestroy(): void {
        this.dragulaService.destroy(this.COLUMN);
        // destroy all the subscriptions at once
        this.subs.unsubscribe();
    }

    reset() {
        this.headers = [];
        this.elementByIndex = {};

        this.oldFields = [];
        this.elementByField = {};

        this.newFields = [];

        this.newFieldsNoMapping = [];

        this.editingHeader = null;
        this.enteredHeaderName = null;
        this.nameByIndex = {};
    }

    update() {
        this.reset();

        this.fieldService.query().toPromise()
            .then((response) => {
                const fields = response.body;

                this.oldFields = fields
                    .sort((a, b) => {
                        // mandatory first
                        if (a.mandatory !== b.mandatory) {
                            return a.mandatory ? -1 : 1;
                        }

                        const nameA = a.name.toUpperCase(); // ignore upper and lowercase
                        const nameB = b.name.toUpperCase(); // ignore upper and lowercase
                        if (nameA < nameB) {
                            return -1;
                        }
                        if (nameA > nameB) {
                            return 1;
                        }

                        // names must be equal
                        return 0;
                    }).map((field) => {
                        return new OldField(field, null, true, field.mandatory);
                    });

                this.mapFields();
            });
    }

    mapFields() {
        this.headers = this.mappingState.headers;

        for (const header of this.headers) {
            this.elementByIndex[header.index] = [header];
        }

        for (const oldField of this.oldFields) {
            this.elementByField[oldField.field.id] = [];
        }

        for (let i = 0; i < this.oldFields.length; i++) {
            const oldField = this.oldFields[i];

            for (let j = this.headers.length - 1; j >= 0; j--) {
                const header = this.headers[j];

                if (this.nameEquals(oldField.field.name, header.value)) {
                    this.elementByField[oldField.field.id] = [header];
                    this.elementByIndex[header.index] = [];
                    break;
                }
            }
        }
    }

    nameEquals(s1, s2: string) {
        s1 = s1.trim().toLowerCase();
        s2 = s2.trim().toLowerCase();
        return s1 === s2;
    }

    unMapOldField(oldField: OldField) {
        const elements = this.elementByField[oldField.field.id];
        const header = elements[0];
        this.elementByField[oldField.field.id] = [];
        this.elementByIndex[header.index] = [header];
    }

    getFileNameWithoutExtension(): string {
        const name = this.mappingState.file.name;
        const pos = name.lastIndexOf('.');
        if (pos > 0 && pos < (name.length - 1)) {
            // there is a '.' and it's not the first, or last character.
            return name.substring(0, pos);
        }
        return name;
    }

    displayCheckboxes(field: Field) {
        if (this.elementByField[field.id].length > 0) {
            return true;
        }

        return field.mandatory;
    }

    displayNewField(oldField: OldField) {
        if (!this.onlyMapped) {
            return true;
        }

        return this.elementByField[oldField.field.id].length > 0 ||
            oldField.field.mandatory;
    }

    mapOrMakeNewField(header: CsvCell) {
        // trying to map
        for (const oldField of this.oldFields) {
            if (this.elementByField[oldField.field.id].length > 0) {
                continue;
            }

            if (this.nameEquals(oldField.field.name, header.value)) {
                this.elementByField[oldField.field.id] = [header];
                this.elementByIndex[header.index] = [];
                return;
            }
        }

        // if could not map -> create new field
        this.newFields.push(new NewField(header));
        this.elementByIndex[header.index] = [];
    }

    removeNewField(newField: NewField) {
        this.elementByIndex[newField.header.index] = [newField.header];
        this.newFields.splice(this.newFields.indexOf(newField), 1);
    }

    tryToAddNewFieldNoMapping() {
        if (!this.newFieldName) {
            return;
        }

        let exists = this.newFieldsNoMapping
            .some((newFieldNoMapping) => this.nameEquals(newFieldNoMapping.name, this.newFieldName));
        if (exists) {
            this.showDialogFieldExists(this.newFieldName);
            return;
        }

        exists = this.oldFields.some((oldField) => this.nameEquals(oldField.field.name, this.newFieldName));
        if (exists) {
            this.showDialogFieldExists(this.newFieldName);
            return;
        }

        this.newFieldsNoMapping.push(new NewFieldNoMapping(this.newFieldName));
        this.newFieldName = null;
    }

    showDialogFieldExists(name: string) {
        const options = {
            title: 'Warning',
            message: 'Field with name "' + name + '" already exists',
            noHidden: true,
            yesCaption: 'Ok'
        };

        this.confirmService.confirm(options, {container: '.mapping-component'})
        // workaround https://github.com/ng-bootstrap/ng-bootstrap/issues/643#issuecomment-306256651
            .catch(() => {
            })
            .then(() => {
                if (document.querySelector('body > .modal')) {
                    document.body.classList.add('modal-open');
                }
            });
    }

    removeNewFieldNoMapping(newFieldNoMapping: NewFieldNoMapping) {
        this.newFieldsNoMapping.splice(this.newFieldsNoMapping.indexOf(newFieldNoMapping), 1);
    }

    getHeaderName(cell: CsvCell) {
        return cell.value ||
            this.nameByIndex[cell.index] ||
            'Column: ' + (cell.index + 1 + 9).toString(36).toUpperCase() + ' (empty caption)';
    }

    containsCaption(cell: CsvCell) {
        return !!cell.value;
    }

    containsNewName(cell: CsvCell) {
        return !!this.nameByIndex[cell.index];
    }

    enterEditingMode(header: CsvCell) {
        this.editingHeader = header;
        this.enteredHeaderName = this.nameByIndex[this.editingHeader.index];
        setTimeout(() => this.enterHeaderNameField.nativeElement.focus(), 0);
    }

    applyHeaderName() {
        this.nameByIndex[this.editingHeader.index] = this.enteredHeaderName;
        this.exitEditingMode();
    }

    exitEditingMode() {
        this.enteredHeaderName = null;
        this.editingHeader = null;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        const oldFields = this.oldFields
            .filter((oldField) => this.elementByField[oldField.field.id].length > 0)
            .map((oldField) => new OldField(oldField.field, this.elementByField[oldField.field.id][0],
                    oldField.editable, oldField.visible, oldField.isOrgInformation));

        const newFields = this.newFields
            .map((newField) => {
                const header = newField.header;
                const newName = this.nameByIndex[header.index];

                const newField1 = new NewField(header, newField.editable, newField.visible, newField.isOrgInformation);
                newField1.header.value = newName || header.value;
                return newField1;
            });

        const mapping = new ClientListMapping(
            oldFields,
            newFields,
            this.newFieldsNoMapping,
        );

        const isPkExists = oldFields.some((f) => f.isOrgInformation) ||
            newFields.some((f) => f.isOrgInformation) ||
            this.newFieldsNoMapping.some((f) => f.isOrgInformation);
        if (!isPkExists) {
            const options = {
                title: 'Error',
                message: 'Organisation Information has not been selected',
                noHidden: true,
                yesCaption: 'Ok'
            };

            this.confirmService.confirm(options, {container: '.mapping-component'})
            // workaround https://github.com/ng-bootstrap/ng-bootstrap/issues/643#issuecomment-306256651
                .catch(() => {
                })
                .then(() => {
                    if (document.querySelector('body > .modal')) {
                        document.body.classList.add('modal-open');
                    }
                });
            return;
        }

        const clientList = new ClientList(null, this.mappingState.listName, null, null, mapping, null);

        this.isSaving = true;
        this.clientListService.create(clientList, this.mappingState.file)
            .subscribe((result: HttpResponse<ClientList>) => {
                this.isSaving = false;
                this.activeModal.close(result.body);
            }, (res: HttpErrorResponse) => {
                this.isSaving = false;
            });
    }

    toggleOrgInformation(field: OldField | NewField | NewFieldNoMapping) {
        field.isOrgInformation = !field.isOrgInformation;
    }
}

/**
 * Field that already exists in a database. Data mapped to {@link #header}.
 */
export class OldField {
    constructor(
        public field: Field,
        public header?: CsvCell,
        public editable = true,
        public visible = true,
        public isOrgInformation = false,
    ) {
    }
}

/**
 * Field that will be made after finish. Data mapped to {@link #header}.
 */
export class NewField {
    constructor(
        public header: CsvCell,
        public editable = true,
        public visible = true,
        public isOrgInformation = false,
    ) {
    }
}

/**
 * Field that will be made after finish. No data.
 */
export class NewFieldNoMapping {
    constructor(
        public name: string,
        public editable = true,
        public visible = true,
        public isOrgInformation = false,
    ) {
    }
}

export class MappingState {
    constructor(
        public listName?: string,
        public file?: File,
        public headers?: CsvCell[],
    ) {
    }
}
