import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Outcome, OutcomePopupService, OutcomeService} from '../../outcome';
import {ClientCampaign} from '../client-campaign.model';
import {ClientCampaignService} from '../client-campaign.service';
import {ClientCampaignTemplate, ClientCampaignTemplateService} from '../../client-campaign-template';
import {Subscription} from 'rxjs';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';
import {BaseEntity, ConfirmService} from '../../../shared';

@Component({
    selector: 'jhi-client-campaign-wizard-outcome',
    templateUrl: './outcome.component.html'
})
export class ClientCampaignWizardOutcomeComponent implements OnInit, OnDestroy {

    _clientCampaign: ClientCampaign;
    @Input()
    set clientCampaign(value: ClientCampaign) {
        this._clientCampaign = value;
        this.populateSortedOutcomes();
    }

    templates: ClientCampaignTemplate[];
    template: ClientCampaignTemplate;
    campaigns: ClientCampaign[];
    campaign: ClientCampaign;
    outcomes: Outcome[];
    outcome: Outcome;
    eventSubscriber: Subscription;
    sortedOutcomes: Outcome[] = [];

    constructor(
        private clientCampaignTemplateService: ClientCampaignTemplateService,
        private clientCampaignService: ClientCampaignService,
        private outcomeService: OutcomeService,
        private outcomePopupService: OutcomePopupService,
        private confirmService: ConfirmService,
        private eventManager: JhiEventManager,
        private alertService: JhiAlertService,
    ) {
    }

    ngOnInit() {
        this.loadTemplates();
        this.loadCampaigns();
        this.loadOutcomes();
        this.registerChangeInOutcomes();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    loadTemplates() {
        this.clientCampaignTemplateService.query().toPromise()
            .then((response) => {
                this.templates = response.body;
            });
    }

    loadCampaigns() {
        this.clientCampaignService.query().toPromise()
            .then((response) => {
                const allCampaigns = response.body;
                // exclude current campaign
                this.campaigns = allCampaigns.filter((cc) => cc.id !== this._clientCampaign.id);
            });
    }

    loadOutcomes() {
        this.outcomeService.queryAll().toPromise()
            .then((outcomesResponse) => {
                // only outcomes that doesn't belong to any campaign
                this.outcomes = outcomesResponse.body.filter((outcome) => !outcome.clientCampaignId);
            });
    }

    addTemplatesOutcomes() {
        if (!this.template) {
            return;
        }

        const req = {
            'templateId.equals': this.template.id
        };

        this.outcomeService.query(req).toPromise()
            .then((response) => {
                const outcomes = response.body;
                this.addOutcomes(...outcomes);
                delete this.template;
            });
    }

    addCampaignsOutcomes() {
        if (!this.campaign) {
            return;
        }

        this.clientCampaignService.find(this.campaign.id).toPromise()
            .then((response) => {
                const clientCampaign = response.body;
                this.addOutcomes(...clientCampaign.outcomes);
                delete this.campaign;
            });
    }

    addOutcomes(...outcomes: BaseEntity[]) {
        // TODO optimize, should I make it return Promise?
        const promises = outcomes.map((outcome: Outcome) => this.outcomeService.find(outcome.id).toPromise());
        Promise.all(promises)
            .then((responses) => {
                const shallows = responses.map((response) => {
                    const existedOutcome = response.body;
                    return this.shallowCopy(existedOutcome);
                });

                this.outcomeService.createMultiple(shallows).toPromise()
                    .then((response) => {
                        const savedOutcomes = response.body;

                        const id = this._clientCampaign.id;
                        if (id) {
                            savedOutcomes.forEach((savedOutcome) => {
                                this.clientCampaignService.addOutcome(id, savedOutcome.id).toPromise();
                            });
                        }
                        this._clientCampaign.outcomes.push(...savedOutcomes);

                        this.alertService.success('cmaApp.outcome.copied', {}, null);
                    });
            })
            .then(() => this.populateSortedOutcomes());
    }

    shallowCopy(value: Outcome): Outcome {
        const shallow = {...value};
        delete shallow.id;
        delete shallow.templateId;
        return shallow;
    }

    addOutcome() {
        if (!this.outcome) {
            return;
        }

        this.addOutcomes(this.outcome);
    }

    registerChangeInOutcomes() {
        this.eventSubscriber = this.eventManager.subscribe('outcomeListModification', (event) => {
            if (event.content === 'Deleted an outcome') {
                const deletedId = event.id;
                const outcomes = this._clientCampaign.outcomes;
                outcomes.filter((outcome) => outcome.id === deletedId)
                    .forEach((outcome) => {
                        outcomes.splice(outcomes.indexOf(outcome), 1);
                    });
            } else {
                const outcome = event.outcome;
                const outcomes = this._clientCampaign.outcomes;
                outcomes.filter((x) => x.id === outcome.id)
                    .forEach((x) => {
                        outcomes[outcomes.indexOf(x)] = outcome;
                    });
            }
        });
    }

    populateSortedOutcomes() {
        this.sortedOutcomes = this._clientCampaign.outcomes.sort((a, b) => a.order - b.order);
    }

    canMoveDown(outcome: Outcome, i: number) {
        return i < this.sortedOutcomes.length - 1;
    }

    canMoveUp(outcome: Outcome, i: number) {
        return i > 0;
    }

    moveDown(outcome: Outcome) {
        const i = this.sortedOutcomes.indexOf(outcome);
        const anotherBookingQuestion = this.sortedOutcomes[i + 1];
        this.swapOrders(outcome, anotherBookingQuestion);
    }

    moveUp(outcome: Outcome) {
        const i = this.sortedOutcomes.indexOf(outcome);
        const anotherBookingQuestion = this.sortedOutcomes[i - 1];
        this.swapOrders(outcome, anotherBookingQuestion);
    }

    swapOrders(outcome, anotherOutcome: Outcome) {
        // swap orders
        const order = outcome.order;
        outcome.order = anotherOutcome.order;
        anotherOutcome.order = order;

        this.populateSortedOutcomes();

        // const a: Outcome = {id: outcome.id, order: outcome.order} as Outcome;
        this.outcomeService.update(outcome).toPromise();

        // const b: Outcome = {id: anotherOutcome.id, order: anotherOutcome.order} as Outcome;
        this.outcomeService.update(anotherOutcome).toPromise();
    }
}
