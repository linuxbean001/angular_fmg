import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {WizardState} from '../client-campaign-wizard.component';
import {Subscription} from 'rxjs';
import {JhiEventManager} from 'ng-jhipster';
import {CampaignPortalUser, CampaignPortalUserService, CampaignPortalUserSummary} from '../../campaign-portal-user';
import {EventFrequencyRate, ReportDistributionMethod} from '../client-campaign.model';
import {NgSelectComponent} from '@ng-select/ng-select';
import {CampaignPortalUserOutcomePopupService} from '../../campaign-portal-user-outcome';
import {CampaignPortalUserOutcomeSummaryComponent} from '../../campaign-portal-user-outcome/campaign-portal-user-outcome-summary.component';
import {ConfirmService} from '../../../shared';

@Component({
    selector: 'jhi-client-campaign-wizard-reporting',
    templateUrl: './reporting.component.html'
})
export class ClientCampaignWizardReportingComponent implements OnInit, OnDestroy {

    _wizardState: WizardState;
    @Input()
    set wizardState(value: WizardState) {
        this._wizardState = value;
        this.update();
    }

    eventSubscriber: Subscription;

    @ViewChild('portalUsersAllowedToExportSelect')
    public portalUsersAllowedToExportSelect: NgSelectComponent;

    @ViewChild('reportsDeliveredToUsersSelect')
    public reportsDeliveredToUsersSelect: NgSelectComponent;

    RATES = EventFrequencyRate.RATES;
    METHODS = ReportDistributionMethod.METHODS;

    portalUsers: CampaignPortalUser[];

    summaryMap: { [id: number]: CampaignPortalUserSummary; } = {};

    // fakeAllPortalUser = new CampaignPortalUser(0, "All");

    constructor(
        private campaignPortalUserService: CampaignPortalUserService,
        private confirmService: ConfirmService,
        private eventManager: JhiEventManager,
        private campaignPortalUserOutcomePopupService: CampaignPortalUserOutcomePopupService,
    ) {
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        if (this.eventSubscriber) {
            this.eventManager.destroy(this.eventSubscriber);
        }
    }

    update() {
        // workaround (always re-initialize the array) for the issue https://github.com/valor-software/ng2-select/issues/360
        // this.portalUsers = [this.fakeAllPortalUser].concat(...this._wizardState.accessPage.portalUsers);
        this.portalUsers = [...this._wizardState.clientCampaign.portalUsers];

        this.portalUsers.forEach((x) => this.updateSummary(x.id));
    }

    updateSummary(campaignPortalUserId: number) {
        this.campaignPortalUserService.getSummary(campaignPortalUserId).toPromise()
            .then((res) => this.summaryMap[campaignPortalUserId] = res.body);
    }

    editPortalUser(campaignPortalUser: CampaignPortalUser) {
        const outcomes = this._wizardState.clientCampaign.outcomes;
        this.campaignPortalUserOutcomePopupService
            .openSummary(CampaignPortalUserOutcomeSummaryComponent as Component, outcomes, campaignPortalUser)
            .then((ngbModalRef) => ngbModalRef.result
                .then((value) => this.updateSummary(campaignPortalUser.id)));
    }
}
