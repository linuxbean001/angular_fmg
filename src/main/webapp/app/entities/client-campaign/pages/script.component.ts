import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {WizardState} from '../client-campaign-wizard.component';
import {ClientCampaign} from '../client-campaign.model';
import {ClientCampaignService} from '../client-campaign.service';
import {ClientCampaignTemplate} from '../../client-campaign-template';
import {Subscription} from 'rxjs';
import {JhiEventManager} from 'ng-jhipster';
import {BaseEntity, ConfirmService} from '../../../shared';
import {
    ClientCampaignScript,
    ClientCampaignScriptDialogComponent,
    ClientCampaignScriptPopupService,
    ClientCampaignScriptService
} from '../../client-campaign-script';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-client-campaign-wizard-script',
    templateUrl: './script.component.html'
})
export class ClientCampaignWizardScriptComponent implements OnInit, OnDestroy {

    _wizardState: WizardState;
    @Input()
    set wizardState(value: WizardState) {
        this._wizardState = value;
    }

    templates: ClientCampaignTemplate[];
    template: ClientCampaignTemplate;

    campaigns: ClientCampaign[];
    campaign: ClientCampaign;

    eventSubscriber: Subscription;

    constructor(
        private clientCampaignService: ClientCampaignService,
        private clientCampaignScriptService: ClientCampaignScriptService,
        private clientCampaignScriptPopupService: ClientCampaignScriptPopupService,
        private confirmService: ConfirmService,
        private eventManager: JhiEventManager,
    ) {
    }

    ngOnInit() {
        this.loadCampaigns();
        this.registerChangeInScripts();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    loadCampaigns() {
        this.clientCampaignService.query().toPromise()
            .then((response) => {
                const allCampaigns = response.body;
                // exclude current campaign
                this.campaigns = allCampaigns.filter((cc) => cc.id !== this._wizardState.clientCampaign.id);
            });
    }

    addCampaignsScripts() {
        if (!this.campaign) {
            return;
        }

        this.clientCampaignService.find(this.campaign.id).toPromise()
            .then((response) => {
                const clientCampaign = response.body;
                this.addScripts(...clientCampaign.scripts);
                delete this.campaign;
            });
    }

    addScripts(...scripts: BaseEntity[]) {
        // TODO optimize, should I make it return Promise?
        scripts.forEach((script: ClientCampaignScript) => {
            this.clientCampaignScriptService.find(script.id).toPromise()
                .then((response) => {
                    const existedScript = response.body;
                    const shallow = this.shallowCopy(existedScript);
                    shallow.clientCampaignId = this._wizardState.clientCampaign.id;
                    shallow.clientCampaignName = this._wizardState.clientCampaign.name;

                    this.clientCampaignScriptService.create(shallow).toPromise()
                        .then((res) => {
                            const savedScript = res.body;
                            const id = this._wizardState.clientCampaign.id;
                            if (id) {
                                this.clientCampaignService.addScript(id, savedScript.id).toPromise();
                            }
                            this._wizardState.clientCampaign.scripts.push(savedScript);
                        });
                });
        });
    }

    shallowCopy(value: ClientCampaignScript) {
        const shallow = {...value};
        delete shallow.id;
        return shallow;
    }

    createNewScript() {
        const clientCampaignScript = new ClientCampaignScript();
        this.clientCampaignScriptPopupService.openStandalone(ClientCampaignScriptDialogComponent as Component, clientCampaignScript)
            .then((ngbModalRef: NgbModalRef) => {
                ngbModalRef.result
                    .then((script) => {
                        const id = this._wizardState.clientCampaign.id;
                        if (id) {
                            this.clientCampaignService.addScript(id, script.id).toPromise();
                        }
                        this._wizardState.clientCampaign.scripts.push(script);
                    });
            });
    }

    registerChangeInScripts() {
        this.eventSubscriber = this.eventManager.subscribe('clientCampaignScriptListModification', (event) => {
            if (event.content === 'Deleted an clientCampaignScript') {
                const deletedId = event.id;
                const scripts = this._wizardState.clientCampaign.scripts;
                scripts.filter((script) => script.id === deletedId)
                    .forEach((script) => {
                        scripts.splice(scripts.indexOf(script), 1);
                    });
            } else {
                const script = event.script;
                const scripts = this._wizardState.clientCampaign.scripts;
                scripts.filter((x) => x.id === script.id)
                    .forEach((x) => {
                        const index = scripts.indexOf(x);
                        // user can reassign to another campaign
                        if (this._wizardState.clientCampaign.id === script.clientCampaignId) {
                            scripts[index] = script;
                        } else {
                            scripts.splice(index, 1);
                        }
                    });
            }
        });
    }
}
