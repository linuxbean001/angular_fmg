import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {WizardState} from '../client-campaign-wizard.component';
import {ClientCampaign} from '../client-campaign.model';
import {ClientCampaignService} from '../client-campaign.service';
import {ClientCampaignTemplate} from '../../client-campaign-template';
import {Subscription} from 'rxjs';
import {JhiEventManager} from 'ng-jhipster';
import {BaseEntity, ConfirmService} from '../../../shared';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {
    ClientCampaignSms,
    ClientCampaignSmsDialogComponent,
    ClientCampaignSmsPopupService,
    ClientCampaignSmsService
} from '../../client-campaign-sms';

@Component({
    selector: 'jhi-client-campaign-wizard-sms',
    templateUrl: './sms.component.html'
})
export class ClientCampaignWizardSmsComponent implements OnInit, OnDestroy {

    _wizardState: WizardState;
    @Input()
    set wizardState(value: WizardState) {
        this._wizardState = value;
    }

    templates: ClientCampaignTemplate[];
    template: ClientCampaignTemplate;

    campaigns: ClientCampaign[];
    campaign: ClientCampaign;

    eventSubscriber: Subscription;

    constructor(
        private clientCampaignService: ClientCampaignService,
        private clientCampaignSmsService: ClientCampaignSmsService,
        private clientCampaignSmsPopupService: ClientCampaignSmsPopupService,
        private confirmService: ConfirmService,
        private eventManager: JhiEventManager,
    ) {
    }

    ngOnInit() {
        this.loadCampaigns();
        this.registerChangeInSmss();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    loadCampaigns() {
        this.clientCampaignService.query().toPromise()
            .then((response) => {
                const allCampaigns = response.body;
                // exclude current campaign
                this.campaigns = allCampaigns.filter((cc) => cc.id !== this._wizardState.clientCampaign.id);
            });
    }

    addCampaignsSmss() {
        if (!this.campaign) {
            return;
        }

        this.clientCampaignService.find(this.campaign.id).toPromise()
            .then((response) => {
                const clientCampaign = response.body;
                this.addSmss(...clientCampaign.smss);
                delete this.campaign;
            });
    }

    addSmss(...smss: BaseEntity[]) {
        // TODO optimize, should I make it return Promise?
        smss.forEach((sms: ClientCampaignSms) => {
            this.clientCampaignSmsService.find(sms.id).toPromise()
                .then((response) => {
                    const existedSms = response.body;
                    const shallow = this.shallowCopy(existedSms);

                    this.clientCampaignSmsService.create(shallow).toPromise()
                        .then((res) => {
                            const savedSms = res.body;
                            const id = this._wizardState.clientCampaign.id;
                            if (id) {
                                this.clientCampaignService.addSms(id, savedSms.id).toPromise();
                            }
                            this._wizardState.clientCampaign.smss.push(savedSms);
                        });
                });
        });
    }

    shallowCopy(value: ClientCampaignSms) {
        const shallow = {...value};
        delete shallow.id;
        return shallow;
    }

    createNewSms() {
        const clientCampaignSms = new ClientCampaignSms();
        this.clientCampaignSmsPopupService.openStandalone(ClientCampaignSmsDialogComponent as Component, clientCampaignSms)
            .then((ngbModalRef: NgbModalRef) => {
                ngbModalRef.result
                    .then((sms) => {
                        const id = this._wizardState.clientCampaign.id;
                        if (id) {
                            this.clientCampaignService.addSms(id, sms.id).toPromise()
                                .then((result) => {
                                    // nothing
                                });
                        }
                        this._wizardState.clientCampaign.smss.push(sms);
                    });
            });
    }

    registerChangeInSmss() {
        this.eventSubscriber = this.eventManager.subscribe('clientCampaignSmsListModification', (event) => {
            if (event.content === 'Deleted an clientCampaignSms') {
                const deletedId = event.id;
                const smss = this._wizardState.clientCampaign.smss;
                smss.filter((sms) => sms.id === deletedId)
                    .forEach((sms) => {
                        smss.splice(smss.indexOf(sms), 1);
                    });
            } else {
                const sms = event.sms;
                const smss = this._wizardState.clientCampaign.smss;
                smss.filter((x) => x.id === sms.id)
                    .forEach((x) => {
                        const index = smss.indexOf(x);
                        // user can reassign to another campaign
                        if (this._wizardState.clientCampaign.id === sms.clientCampaignId) {
                            smss[index] = sms;
                        } else {
                            smss.splice(index, 1);
                        }
                    });
            }
        });
    }
}
