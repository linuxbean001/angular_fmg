import {Component, Input, OnInit} from '@angular/core';
import {ClientCampaign} from '../client-campaign.model';

@Component({
    selector: 'jhi-client-campaign-wizard-summary',
    templateUrl: './summary.component.html'
})
export class ClientCampaignWizardSummaryComponent implements OnInit {

    @Input() clientCampaign: ClientCampaign;

    constructor() {
    }

    ngOnInit() {
    }
}
