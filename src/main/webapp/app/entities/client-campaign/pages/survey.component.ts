import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {WizardState} from '../client-campaign-wizard.component';
import {ClientCampaign} from '../client-campaign.model';
import {ClientCampaignService} from '../client-campaign.service';
import {Subscription} from 'rxjs';
import {JhiEventManager} from 'ng-jhipster';
import {BaseEntity, ConfirmService} from '../../../shared';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {
    SurveyQuestion,
    SurveyQuestionDialogComponent,
    SurveyQuestionPopupService,
    SurveyQuestionService
} from '../../survey-question';

@Component({
    selector: 'jhi-client-campaign-wizard-survey',
    templateUrl: './survey.component.html'
})
export class ClientCampaignWizardSurveyComponent implements OnInit, OnDestroy {

    _wizardState: WizardState;
    @Input()
    set wizardState(value: WizardState) {
        this._wizardState = value;
        this.populateSortedSurveyQuestions();
    }

    campaigns: ClientCampaign[];
    campaign: ClientCampaign;

    sortedSurveyQuestions: SurveyQuestion[] = [];

    eventSubscriber: Subscription;

    constructor(
        private clientCampaignService: ClientCampaignService,
        private surveyQuestionService: SurveyQuestionService,
        private surveyQuestionPopupService: SurveyQuestionPopupService,
        private confirmService: ConfirmService,
        private eventManager: JhiEventManager,
    ) {
    }

    ngOnInit() {
        this.loadCampaigns();
        this.registerChangeInSurveyQuestions();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    loadCampaigns() {
        this.clientCampaignService.query().toPromise()
            .then((response) => {
                const allCampaigns = response.body;
                // exclude current campaign
                this.campaigns = allCampaigns.filter((cc) => cc.id !== this._wizardState.clientCampaign.id);
            });
    }

    populateSortedSurveyQuestions() {
        this.sortedSurveyQuestions = this._wizardState.clientCampaign.surveyQuestions.sort((a, b) => a.order - b.order);
    }

    addCampaignsSurveyQuestions() {
        if (!this.campaign) {
            return;
        }

        this.clientCampaignService.find(this.campaign.id).toPromise()
            .then((response) => {
                const clientCampaign = response.body;
                const sorted = clientCampaign.surveyQuestions.sort((a, b) => a.order - b.order);
                this.addSurveyQuestions(...sorted);
                delete this.campaign;
            });
    }

    addSurveyQuestions(...surveyQuestions: BaseEntity[]) {
        /*
         * serial executes Promises sequentially.
         * @param {funcs} An array of funcs that return promises.
         * @example
         * const urls = ['/url1', '/url2', '/url3']
         * serial(urls.map(url => () => $.ajax(url)))
         *     .then(console.log.bind(console))
         */
        // see https://stackoverflow.com/a/41115086/579680
        const serial = (funcs) =>
            funcs.reduce((promise, func) =>
                promise.then((result) => func().then(Array.prototype.concat.bind(result))), Promise.resolve([]));

        // double arrows
        const promises = surveyQuestions.map((surveyQuestion: SurveyQuestion) => () => {
            return this.surveyQuestionService.find(surveyQuestion.id).toPromise()
                .then((response) => {
                    const existedSurveyQuestion = response.body;
                    const shallow = this.shallowCopy(existedSurveyQuestion);

                    return this.surveyQuestionService.create(shallow).toPromise()
                        .then((res) => {
                            const savedSurveyQuestion = res.body;
                            const id = this._wizardState.clientCampaign.id;
                            this._wizardState.clientCampaign.surveyQuestions.push(savedSurveyQuestion);
                            if (id) {
                                return this.clientCampaignService.addSurveyQuestion(id, savedSurveyQuestion.id).toPromise();
                            } else {
                                return Promise.resolve(null);
                            }
                        });
                });
        });

        serial(promises)
            .then(() => this.populateSortedSurveyQuestions());
    }

    shallowCopy(value: SurveyQuestion) {
        const shallow = {...value};
        delete shallow.id;
        return shallow;
    }

    createNewSurveyQuestion() {
        const surveyQuestion = new SurveyQuestion();
        this.surveyQuestionPopupService.openStandalone(SurveyQuestionDialogComponent as Component, surveyQuestion)
            .then((ngbModalRef: NgbModalRef) => {
                ngbModalRef.result
                    .then((sq) => {
                        const id = this._wizardState.clientCampaign.id;
                        if (id) {
                            this.clientCampaignService.addSurveyQuestion(id, sq.id).toPromise();
                        }
                        this._wizardState.clientCampaign.surveyQuestions.push(sq);
                        this.populateSortedSurveyQuestions();
                    });
            });
    }

    registerChangeInSurveyQuestions() {
        this.eventSubscriber = this.eventManager.subscribe('surveyQuestionListModification', (event) => {
            const surveyQuestions = this._wizardState.clientCampaign.surveyQuestions;
            if (event.content === 'Deleted an surveyQuestion') {
                const deletedId = event.id;
                surveyQuestions.filter((sq) => sq.id === deletedId)
                    .forEach((x) => {
                        surveyQuestions.splice(surveyQuestions.indexOf(x), 1);
                        this.populateSortedSurveyQuestions();
                    });
            } else {
                const surveyQuestion = event.surveyQuestion;
                surveyQuestions.filter((x) => x.id === surveyQuestion.id)
                    .forEach((x) => {
                        const index = surveyQuestions.indexOf(x);
                        // user can reassign to another campaign
                        if (this._wizardState.clientCampaign.id === surveyQuestion.clientCampaignId) {
                            surveyQuestions[index] = surveyQuestion;
                        } else {
                            surveyQuestions.splice(index, 1);
                        }
                        this.populateSortedSurveyQuestions();
                    });
            }
        });
    }

    canMoveDown(surveyQuestion: SurveyQuestion, i: number) {
        return i < this.sortedSurveyQuestions.length - 1;
    }

    canMoveUp(surveyQuestion: SurveyQuestion, i: number) {
        return i > 0;
    }

    moveDown(surveyQuestion: SurveyQuestion) {
        const i = this.sortedSurveyQuestions.indexOf(surveyQuestion);
        const anotherSurveyQuestion = this.sortedSurveyQuestions[i + 1];
        this.swapOrders(surveyQuestion, anotherSurveyQuestion);
    }

    moveUp(surveyQuestion: SurveyQuestion) {
        const i = this.sortedSurveyQuestions.indexOf(surveyQuestion);
        const anotherSurveyQuestion = this.sortedSurveyQuestions[i - 1];
        this.swapOrders(surveyQuestion, anotherSurveyQuestion);
    }

    swapOrders(surveyQuestion, anotherSurveyQuestion: SurveyQuestion) {
        // swap orders
        const order = surveyQuestion.order;
        surveyQuestion.order = anotherSurveyQuestion.order;
        anotherSurveyQuestion.order = order;

        this.populateSortedSurveyQuestions();

        // const a: SurveyQuestion = {id: surveyQuestion.id, order: surveyQuestion.order} as SurveyQuestion;
        this.surveyQuestionService.update(surveyQuestion).toPromise();

        // const b: SurveyQuestion = {id: anotherSurveyQuestion.id, order: anotherSurveyQuestion.order} as SurveyQuestion;
        this.surveyQuestionService.update(anotherSurveyQuestion).toPromise();
    }
}
