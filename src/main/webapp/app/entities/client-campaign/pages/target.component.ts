import {Component, Input, OnInit} from '@angular/core';
import {WizardState} from '../client-campaign-wizard.component';

@Component({
    selector: 'jhi-client-campaign-wizard-target',
    templateUrl: './target.component.html'
})
export class ClientCampaignWizardTargetComponent implements OnInit {

    _wizardState: WizardState;
    @Input()
    set wizardState(value: WizardState) {
        this._wizardState = value;
    }

    public CALL_PER_HOUR_MIN = 1;
    public CALL_PER_HOUR_MAX = 40;

    public CONVERSATIONS_PER_HOUR_MIN = 1;
    public CONVERSATIONS_PER_HOUR_MAX = 40;

    public CONTACT_RATE_MIN = 1;
    public CONTACT_RATE_MAX = 100;

    public HOURS_PER_LEAD_MIN = 1;
    public HOURS_PER_LEAD_MAX = 200;

    public CONVERSION_RATE_MIN = 1;
    public CONVERSION_RATE_MAX = 100;

    public LEAD_TO_SALE_CONVERSION_MIN = 1;
    public LEAD_TO_SALE_CONVERSION_MAX = 100;

    constructor() {
    }

    ngOnInit() {
    }

    onSubmit() {
        return;
    }

    callPerHourMin() {
        return this._wizardState.clientCampaign.callPerHourLow || this.CALL_PER_HOUR_MIN;
    }

    callPerHourMax() {
        return this._wizardState.clientCampaign.callPerHourHigh || this.CALL_PER_HOUR_MAX;
    }

    conversationsPerHourMin() {
        return this._wizardState.clientCampaign.conversationsPerHourLow || this.CONVERSATIONS_PER_HOUR_MIN;
    }

    conversationsPerHourMax() {
        return this._wizardState.clientCampaign.conversationsPerHourHigh || this.CONVERSATIONS_PER_HOUR_MAX;
    }

    contactRateMin() {
        return this._wizardState.clientCampaign.contactRateLow || this.CONTACT_RATE_MIN;
    }

    contactRateMax() {
        return this._wizardState.clientCampaign.contactRateHigh || this.CONTACT_RATE_MAX;
    }

    hoursPerLeadMin() {
        return this._wizardState.clientCampaign.hoursPerLeadLow || this.HOURS_PER_LEAD_MIN;
    }

    hoursPerLeadMax() {
        return this._wizardState.clientCampaign.hoursPerLeadHigh || this.HOURS_PER_LEAD_MAX;
    }

    conversionRateMin() {
        return this._wizardState.clientCampaign.conversionRateLow || this.CONVERSION_RATE_MIN;
    }

    conversionRateMax() {
        return this._wizardState.clientCampaign.conversionRateHigh || this.CONVERSION_RATE_MAX;
    }

    leadToSaleConversionMin() {
        return this._wizardState.clientCampaign.leadToSaleConversionLow || this.LEAD_TO_SALE_CONVERSION_MIN;
    }

    leadToSaleConversionMax() {
        return this._wizardState.clientCampaign.leadToSaleConversionHigh || this.LEAD_TO_SALE_CONVERSION_MAX;
    }
}
