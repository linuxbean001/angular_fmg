import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager} from 'ng-jhipster';

import {ClientCompanyStatus} from './client-company-status.model';
import {ClientCompanyStatusPopupService} from './client-company-status-popup.service';
import {ClientCompanyStatusService} from './client-company-status.service';

@Component({
    selector: 'jhi-client-company-status-delete-dialog',
    templateUrl: './client-company-status-delete-dialog.component.html'
})
export class ClientCompanyStatusDeleteDialogComponent {

    clientCompanyStatus: ClientCompanyStatus;

    constructor(
        private clientCompanyStatusService: ClientCompanyStatusService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.clientCompanyStatusService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'clientCompanyStatusListModification',
                content: 'Deleted an clientCompanyStatus',
                id,
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-client-company-status-delete-popup',
    template: ''
})
export class ClientCompanyStatusDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientCompanyStatusPopupService: ClientCompanyStatusPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.clientCompanyStatusPopupService
                .open(ClientCompanyStatusDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
