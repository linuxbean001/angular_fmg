import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpResponse} from '@angular/common/http';
import {Subscription} from 'rxjs/Subscription';
import {JhiEventManager} from 'ng-jhipster';

import {ClientCompanyStatus} from './client-company-status.model';
import {ClientCompanyStatusService} from './client-company-status.service';

@Component({
    selector: 'jhi-client-company-status-detail',
    templateUrl: './client-company-status-detail.component.html'
})
export class ClientCompanyStatusDetailComponent implements OnInit, OnDestroy {

    clientCompanyStatus: ClientCompanyStatus;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private clientCompanyStatusService: ClientCompanyStatusService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInClientCompanyStatuses();
    }

    load(id) {
        this.clientCompanyStatusService.find(id)
            .subscribe((clientCompanyStatusResponse: HttpResponse<ClientCompanyStatus>) => {
                this.clientCompanyStatus = clientCompanyStatusResponse.body;
            });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInClientCompanyStatuses() {
        this.eventSubscriber = this.eventManager.subscribe(
            'clientCompanyStatusListModification',
            (response) => this.load(this.clientCompanyStatus.id)
        );
    }
}
