import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager} from 'ng-jhipster';

import {ClientCompanyStatus} from './client-company-status.model';
import {ClientCompanyStatusPopupService} from './client-company-status-popup.service';
import {ClientCompanyStatusService} from './client-company-status.service';

@Component({
    selector: 'jhi-client-company-status-dialog',
    templateUrl: './client-company-status-dialog.component.html'
})
export class ClientCompanyStatusDialogComponent implements OnInit {

    clientCompanyStatus: ClientCompanyStatus;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private clientCompanyStatusService: ClientCompanyStatusService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.clientCompanyStatus.id !== undefined) {
            this.subscribeToSaveResponse(
                this.clientCompanyStatusService.update(this.clientCompanyStatus));
        } else {
            this.subscribeToSaveResponse(
                this.clientCompanyStatusService.create(this.clientCompanyStatus));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ClientCompanyStatus>>) {
        result.subscribe((res: HttpResponse<ClientCompanyStatus>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ClientCompanyStatus) {
        this.eventManager.broadcast({name: 'clientCompanyStatusListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-client-company-status-popup',
    template: ''
})
export class ClientCompanyStatusPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientCompanyStatusPopupService: ClientCompanyStatusPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.clientCompanyStatusPopupService
                    .open(ClientCompanyStatusDialogComponent as Component, params['id']);
            } else {
                this.clientCompanyStatusPopupService
                    .open(ClientCompanyStatusDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
