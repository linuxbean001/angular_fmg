import {Component, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {HttpResponse} from '@angular/common/http';
import {ClientCompanyStatus} from './client-company-status.model';
import {ClientCompanyStatusService} from './client-company-status.service';

@Injectable()
export class ClientCompanyStatusPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private clientCompanyStatusService: ClientCompanyStatusService
    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.clientCompanyStatusService.find(id)
                    .subscribe((clientCompanyStatusResponse: HttpResponse<ClientCompanyStatus>) => {
                        const clientCompanyStatus: ClientCompanyStatus = clientCompanyStatusResponse.body;
                        this.ngbModalRef = this.clientCompanyStatusModalRef(component, clientCompanyStatus);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.clientCompanyStatusModalRef(component, new ClientCompanyStatus());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    clientCompanyStatusModalRef(component: Component, clientCompanyStatus: ClientCompanyStatus): NgbModalRef {
        const modalRef = this.modalService.open(component, {size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.clientCompanyStatus = clientCompanyStatus;
        modalRef.result.then((result) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
