import {BaseEntity} from './../../shared';

export class ClientCompanyStatus implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
    ) {
    }
}
