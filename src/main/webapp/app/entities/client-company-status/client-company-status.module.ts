import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {CmaSharedModule} from '../../shared';
import {
    ClientCompanyStatusComponent,
    ClientCompanyStatusDeleteDialogComponent,
    ClientCompanyStatusDeletePopupComponent,
    ClientCompanyStatusDetailComponent,
    ClientCompanyStatusDialogComponent,
    ClientCompanyStatusPopupComponent,
    clientCompanyStatusPopupRoute,
    ClientCompanyStatusPopupService,
    ClientCompanyStatusResolvePagingParams,
    clientCompanyStatusRoute,
    ClientCompanyStatusService,
} from './';

const ENTITY_STATES = [
    ...clientCompanyStatusRoute,
    ...clientCompanyStatusPopupRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ClientCompanyStatusComponent,
        ClientCompanyStatusDetailComponent,
        ClientCompanyStatusDialogComponent,
        ClientCompanyStatusDeleteDialogComponent,
        ClientCompanyStatusPopupComponent,
        ClientCompanyStatusDeletePopupComponent,
    ],
    entryComponents: [
        ClientCompanyStatusComponent,
        ClientCompanyStatusDialogComponent,
        ClientCompanyStatusPopupComponent,
        ClientCompanyStatusDeleteDialogComponent,
        ClientCompanyStatusDeletePopupComponent,
    ],
    providers: [
        ClientCompanyStatusService,
        ClientCompanyStatusPopupService,
        ClientCompanyStatusResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaClientCompanyStatusModule {
}
