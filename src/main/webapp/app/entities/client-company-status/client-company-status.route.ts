import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes} from '@angular/router';
import {JhiPaginationUtil} from 'ng-jhipster';

import {UserRouteAccessService} from '../../shared';
import {ClientCompanyStatusComponent} from './client-company-status.component';
import {ClientCompanyStatusDetailComponent} from './client-company-status-detail.component';
import {ClientCompanyStatusPopupComponent} from './client-company-status-dialog.component';
import {ClientCompanyStatusDeletePopupComponent} from './client-company-status-delete-dialog.component';

@Injectable()
export class ClientCompanyStatusResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const clientCompanyStatusRoute: Routes = [
    {
        path: 'client-company-status',
        component: ClientCompanyStatusComponent,
        resolve: {
            'pagingParams': ClientCompanyStatusResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCompanyStatus.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'client-company-status/:id',
        component: ClientCompanyStatusDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCompanyStatus.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const clientCompanyStatusPopupRoute: Routes = [
    {
        path: 'client-company-status-new',
        component: ClientCompanyStatusPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCompanyStatus.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-company-status/:id/edit',
        component: ClientCompanyStatusPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCompanyStatus.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-company-status/:id/delete',
        component: ClientCompanyStatusDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCompanyStatus.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
