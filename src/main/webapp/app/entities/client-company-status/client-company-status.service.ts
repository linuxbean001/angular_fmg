import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';

import {ClientCompanyStatus} from './client-company-status.model';
import {createRequestOption} from '../../shared';

export type EntityResponseType = HttpResponse<ClientCompanyStatus>;

@Injectable()
export class ClientCompanyStatusService {

    private resourceUrl = SERVER_API_URL + 'api/client-company-statuses';

    constructor(private http: HttpClient) {
    }

    create(clientCompanyStatus: ClientCompanyStatus): Observable<EntityResponseType> {
        const copy = this.convert(clientCompanyStatus);
        return this.http.post<ClientCompanyStatus>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(clientCompanyStatus: ClientCompanyStatus): Observable<EntityResponseType> {
        const copy = this.convert(clientCompanyStatus);
        return this.http.put<ClientCompanyStatus>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ClientCompanyStatus>(`${this.resourceUrl}/${id}`, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ClientCompanyStatus[]>> {
        const options = createRequestOption(req);
        return this.http.get<ClientCompanyStatus[]>(this.resourceUrl, {params: options, observe: 'response'})
            .map((res: HttpResponse<ClientCompanyStatus[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ClientCompanyStatus = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ClientCompanyStatus[]>): HttpResponse<ClientCompanyStatus[]> {
        const jsonResponse: ClientCompanyStatus[] = res.body;
        const body: ClientCompanyStatus[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ClientCompanyStatus.
     */
    private convertItemFromServer(clientCompanyStatus: ClientCompanyStatus): ClientCompanyStatus {
        const copy: ClientCompanyStatus = Object.assign({}, clientCompanyStatus);
        return copy;
    }

    /**
     * Convert a ClientCompanyStatus to a JSON which can be sent to the server.
     */
    private convert(clientCompanyStatus: ClientCompanyStatus): ClientCompanyStatus {
        const copy: ClientCompanyStatus = Object.assign({}, clientCompanyStatus);
        return copy;
    }
}
