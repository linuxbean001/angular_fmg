import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager} from 'ng-jhipster';

import {ClientCompany} from './client-company.model';
import {ClientCompanyPopupService} from './client-company-popup.service';
import {ClientCompanyService} from './client-company.service';

@Component({
    selector: 'jhi-client-company-delete-dialog',
    templateUrl: './client-company-delete-dialog.component.html'
})
export class ClientCompanyDeleteDialogComponent {

    clientCompany: ClientCompany;

    constructor(
        private clientCompanyService: ClientCompanyService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.clientCompanyService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'clientCompanyListModification',
                content: 'Deleted an clientCompany',
                id,
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-client-company-delete-popup',
    template: ''
})
export class ClientCompanyDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientCompanyPopupService: ClientCompanyPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.clientCompanyPopupService
                .open(ClientCompanyDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
