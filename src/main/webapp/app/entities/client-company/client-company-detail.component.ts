import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpResponse} from '@angular/common/http';
import {Subscription} from 'rxjs/Subscription';
import {JhiEventManager} from 'ng-jhipster';

import {ClientCompany} from './client-company.model';
import {ClientCompanyService} from './client-company.service';

@Component({
    selector: 'jhi-client-company-detail',
    templateUrl: './client-company-detail.component.html'
})
export class ClientCompanyDetailComponent implements OnInit, OnDestroy {

    clientCompany: ClientCompany;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private clientCompanyService: ClientCompanyService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInClientCompanies();
    }

    load(id) {
        this.clientCompanyService.find(id)
            .subscribe((clientCompanyResponse: HttpResponse<ClientCompany>) => {
                this.clientCompany = clientCompanyResponse.body;
            });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInClientCompanies() {
        this.eventSubscriber = this.eventManager.subscribe(
            'clientCompanyListModification',
            (response) => this.load(this.clientCompany.id)
        );
    }
}
