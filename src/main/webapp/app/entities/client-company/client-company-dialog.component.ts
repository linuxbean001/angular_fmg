import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';

import {ClientCompany} from './client-company.model';
import {ClientCompanyPopupService} from './client-company-popup.service';
import {ClientCompanyService} from './client-company.service';
import {User, UserService} from '../../shared';
import {ClientCompanyStatus, ClientCompanyStatusService} from '../client-company-status';
import {Industry, IndustryService} from '../industry';

@Component({
    selector: 'jhi-client-company-dialog',
    templateUrl: './client-company-dialog.component.html'
})
export class ClientCompanyDialogComponent implements OnInit {

    clientCompany: ClientCompany;
    isSaving: boolean;

    users: User[];

    usercompanystatuses: ClientCompanyStatus[];
    industries: Industry[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private clientCompanyService: ClientCompanyService,
        private userService: UserService,
        private clientCompanyStatusService: ClientCompanyStatusService,
        private industryService: IndustryService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.userService.query()
            .subscribe((res: HttpResponse<User[]>) => {
                this.users = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.clientCompanyStatusService.query()
            .subscribe((res: HttpResponse<ClientCompanyStatus[]>) => {
                this.usercompanystatuses = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.industryService.query()
            .subscribe((res: HttpResponse<Industry[]>) => {
                this.industries = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.clientCompany.id !== undefined) {
            this.subscribeToSaveResponse(
                this.clientCompanyService.update(this.clientCompany));
        } else {
            this.subscribeToSaveResponse(
                this.clientCompanyService.create(this.clientCompany));
        }
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }

    trackClientCompanyStatusById(index: number, item: ClientCompanyStatus) {
        return item.id;
    }

    trackIndustryById(index: number, item: Industry) {
        return item.id;
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ClientCompany>>) {
        result.subscribe((res: HttpResponse<ClientCompany>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ClientCompany) {
        this.eventManager.broadcast({name: 'clientCompanyListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-client-company-popup',
    template: ''
})
export class ClientCompanyPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientCompanyPopupService: ClientCompanyPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.clientCompanyPopupService
                    .open(ClientCompanyDialogComponent as Component, params['id']);
            } else {
                this.clientCompanyPopupService
                    .open(ClientCompanyDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
