import {Component, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {HttpResponse} from '@angular/common/http';
import {DatePipe} from '@angular/common';
import {ClientCompany} from './client-company.model';
import {ClientCompanyService} from './client-company.service';

@Injectable()
export class ClientCompanyPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private clientCompanyService: ClientCompanyService
    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.clientCompanyService.find(id)
                    .subscribe((clientCompanyResponse: HttpResponse<ClientCompany>) => {
                        const clientCompany: ClientCompany = clientCompanyResponse.body;
                        clientCompany.createdDate = this.datePipe
                            .transform(clientCompany.createdDate, 'yyyy-MM-ddTHH:mm:ss');
                        clientCompany.updatedDate = this.datePipe
                            .transform(clientCompany.updatedDate, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.clientCompanyModalRef(component, clientCompany);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.clientCompanyModalRef(component, new ClientCompany());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    clientCompanyModalRef(component: Component, clientCompany: ClientCompany): NgbModalRef {
        const modalRef = this.modalService.open(component, {size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.clientCompany = clientCompany;
        modalRef.result.then((result) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
