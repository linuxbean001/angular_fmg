import {BaseEntity} from '../../shared';

export class ClientCompany implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public createdDate?: any,
        public createdByLogin?: string,
        public createdById?: number,
        public statusName?: string,
        public statusId?: number,
        public updatedDate?: any,
        public updatedByLogin?: string,
        public updatedById?: number,
        public industryName?: string,
        public industryId?: number,
    ) {
    }
}
