import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {CmaSharedModule} from '../../shared';
import {CmaAdminModule} from '../../admin/admin.module';
import {
    ClientCompanyComponent,
    ClientCompanyDeleteDialogComponent,
    ClientCompanyDeletePopupComponent,
    ClientCompanyDetailComponent,
    ClientCompanyDialogComponent,
    ClientCompanyPopupComponent,
    clientCompanyPopupRoute,
    ClientCompanyPopupService,
    ClientCompanyResolvePagingParams,
    clientCompanyRoute,
    ClientCompanyService,
} from './';

const ENTITY_STATES = [
    ...clientCompanyRoute,
    ...clientCompanyPopupRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        CmaAdminModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ClientCompanyComponent,
        ClientCompanyDetailComponent,
        ClientCompanyDialogComponent,
        ClientCompanyDeleteDialogComponent,
        ClientCompanyPopupComponent,
        ClientCompanyDeletePopupComponent,
    ],
    entryComponents: [
        ClientCompanyComponent,
        ClientCompanyDialogComponent,
        ClientCompanyPopupComponent,
        ClientCompanyDeleteDialogComponent,
        ClientCompanyDeletePopupComponent,
    ],
    providers: [
        ClientCompanyService,
        ClientCompanyPopupService,
        ClientCompanyResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaClientCompanyModule {
}
