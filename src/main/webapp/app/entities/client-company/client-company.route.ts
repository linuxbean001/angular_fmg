import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes} from '@angular/router';
import {JhiPaginationUtil} from 'ng-jhipster';

import {UserRouteAccessService} from '../../shared';
import {ClientCompanyComponent} from './client-company.component';
import {ClientCompanyDetailComponent} from './client-company-detail.component';
import {ClientCompanyPopupComponent} from './client-company-dialog.component';
import {ClientCompanyDeletePopupComponent} from './client-company-delete-dialog.component';

@Injectable()
export class ClientCompanyResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const clientCompanyRoute: Routes = [
    {
        path: 'client-company',
        component: ClientCompanyComponent,
        resolve: {
            'pagingParams': ClientCompanyResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCompany.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'client-company/:id',
        component: ClientCompanyDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCompany.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const clientCompanyPopupRoute: Routes = [
    {
        path: 'client-company-new',
        component: ClientCompanyPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCompany.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-company/:id/edit',
        component: ClientCompanyPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCompany.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-company/:id/delete',
        component: ClientCompanyDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientCompany.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
