import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';

import {JhiDateUtils} from 'ng-jhipster';

import {ClientCompany} from './client-company.model';
import {createRequestOption} from '../../shared';

export type EntityResponseType = HttpResponse<ClientCompany>;

@Injectable()
export class ClientCompanyService {

    private resourceUrl = SERVER_API_URL + 'api/client-companies';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) {
    }

    create(clientCompany: ClientCompany): Observable<EntityResponseType> {
        const copy = this.convert(clientCompany);
        return this.http.post<ClientCompany>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(clientCompany: ClientCompany): Observable<EntityResponseType> {
        const copy = this.convert(clientCompany);
        return this.http.put<ClientCompany>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ClientCompany>(`${this.resourceUrl}/${id}`, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ClientCompany[]>> {
        const options = createRequestOption(req);
        return this.http.get<ClientCompany[]>(this.resourceUrl, {params: options, observe: 'response'})
            .map((res: HttpResponse<ClientCompany[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }

    updateIndustry(clientCompanyId: number, industryId?: number): Observable<HttpResponse<void>> {
        const url = SERVER_API_URL + 'api/client-companies-update-industry/' + clientCompanyId +
            (industryId ? '?industryId=' + industryId : '');
        return this.http.put<void>(url, {}, {observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ClientCompany = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ClientCompany[]>): HttpResponse<ClientCompany[]> {
        const jsonResponse: ClientCompany[] = res.body;
        const body: ClientCompany[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ClientCompany.
     */
    private convertItemFromServer(clientCompany: ClientCompany): ClientCompany {
        const copy: ClientCompany = Object.assign({}, clientCompany);
        copy.createdDate = this.dateUtils
            .convertDateTimeFromServer(clientCompany.createdDate);
        copy.updatedDate = this.dateUtils
            .convertDateTimeFromServer(clientCompany.updatedDate);
        return copy;
    }

    /**
     * Convert a ClientCompany to a JSON which can be sent to the server.
     */
    private convert(clientCompany: ClientCompany): ClientCompany {
        const copy: ClientCompany = Object.assign({}, clientCompany);

        copy.createdDate = this.dateUtils.toDate(clientCompany.createdDate);
        copy.updatedDate = this.dateUtils.toDate(clientCompany.updatedDate);
        return copy;
    }
}
