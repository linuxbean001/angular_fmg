import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ClientField } from './client-field.model';
import { ClientFieldPopupService } from './client-field-popup.service';
import { ClientFieldService } from './client-field.service';

@Component({
    selector: 'jhi-client-field-delete-dialog',
    templateUrl: './client-field-delete-dialog.component.html'
})
export class ClientFieldDeleteDialogComponent {

    clientField: ClientField;

    constructor(
        private clientFieldService: ClientFieldService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.clientFieldService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'clientFieldListModification',
                content: 'Deleted an clientField',
                id,
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-client-field-delete-popup',
    template: ''
})
export class ClientFieldDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientFieldPopupService: ClientFieldPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.clientFieldPopupService
                .open(ClientFieldDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
