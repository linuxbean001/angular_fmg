import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { ClientField } from './client-field.model';
import { ClientFieldService } from './client-field.service';

@Component({
    selector: 'jhi-client-field-detail',
    templateUrl: './client-field-detail.component.html'
})
export class ClientFieldDetailComponent implements OnInit, OnDestroy {

    clientField: ClientField;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private clientFieldService: ClientFieldService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInClientFields();
    }

    load(id) {
        this.clientFieldService.find(id)
            .subscribe((clientFieldResponse: HttpResponse<ClientField>) => {
                this.clientField = clientFieldResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInClientFields() {
        this.eventSubscriber = this.eventManager.subscribe(
            'clientFieldListModification',
            (response) => this.load(this.clientField.id)
        );
    }
}
