import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ClientField } from './client-field.model';
import { ClientFieldPopupService } from './client-field-popup.service';
import { ClientFieldService } from './client-field.service';
import { Field, FieldService } from '../field';
import { ClientList, ClientListService } from '../client-list';

@Component({
    selector: 'jhi-client-field-dialog',
    templateUrl: './client-field-dialog.component.html'
})
export class ClientFieldDialogComponent implements OnInit {

    clientField: ClientField;
    isSaving: boolean;

    fields: Field[];

    clientlists: ClientList[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private clientFieldService: ClientFieldService,
        private fieldService: FieldService,
        private clientListService: ClientListService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.fieldService.query()
            .subscribe((res: HttpResponse<Field[]>) => { this.fields = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.clientListService.query()
            .subscribe((res: HttpResponse<ClientList[]>) => { this.clientlists = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.clientField.id !== undefined) {
            this.subscribeToSaveResponse(
                this.clientFieldService.update(this.clientField));
        } else {
            this.subscribeToSaveResponse(
                this.clientFieldService.create(this.clientField));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ClientField>>) {
        result.subscribe((res: HttpResponse<ClientField>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ClientField) {
        this.eventManager.broadcast({ name: 'clientFieldListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackFieldById(index: number, item: Field) {
        return item.id;
    }

    trackClientListById(index: number, item: ClientList) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-client-field-popup',
    template: ''
})
export class ClientFieldPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientFieldPopupService: ClientFieldPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.clientFieldPopupService
                    .open(ClientFieldDialogComponent as Component, params['id']);
            } else {
                this.clientFieldPopupService
                    .open(ClientFieldDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
