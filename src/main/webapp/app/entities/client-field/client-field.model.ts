import {BaseEntity} from '../../shared';

export class ClientField implements BaseEntity {
    constructor(
        public id?: number,
        public visible?: boolean,
        public editable?: boolean,
        public isOrgInformation?: boolean,
        public fieldName?: string,
        public fieldId?: number,
        public fieldOrder?: number,
        public listName?: string,
        public listId?: number,
    ) {
        this.visible = false;
        this.editable = false;
        this.isOrgInformation = false;
    }
}
