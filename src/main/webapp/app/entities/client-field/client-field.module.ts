import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CmaSharedModule } from '../../shared';
import {
    ClientFieldService,
    ClientFieldPopupService,
    ClientFieldComponent,
    ClientFieldDetailComponent,
    ClientFieldDialogComponent,
    ClientFieldPopupComponent,
    ClientFieldDeletePopupComponent,
    ClientFieldDeleteDialogComponent,
    clientFieldRoute,
    clientFieldPopupRoute,
    ClientFieldResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...clientFieldRoute,
    ...clientFieldPopupRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ClientFieldComponent,
        ClientFieldDetailComponent,
        ClientFieldDialogComponent,
        ClientFieldDeleteDialogComponent,
        ClientFieldPopupComponent,
        ClientFieldDeletePopupComponent,
    ],
    entryComponents: [
        ClientFieldComponent,
        ClientFieldDialogComponent,
        ClientFieldPopupComponent,
        ClientFieldDeleteDialogComponent,
        ClientFieldDeletePopupComponent,
    ],
    providers: [
        ClientFieldService,
        ClientFieldPopupService,
        ClientFieldResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaClientFieldModule {}
