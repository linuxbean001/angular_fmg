import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { ClientFieldComponent } from './client-field.component';
import { ClientFieldDetailComponent } from './client-field-detail.component';
import { ClientFieldPopupComponent } from './client-field-dialog.component';
import { ClientFieldDeletePopupComponent } from './client-field-delete-dialog.component';

@Injectable()
export class ClientFieldResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const clientFieldRoute: Routes = [
    {
        path: 'client-field',
        component: ClientFieldComponent,
        resolve: {
            'pagingParams': ClientFieldResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientField.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'client-field/:id',
        component: ClientFieldDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientField.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const clientFieldPopupRoute: Routes = [
    {
        path: 'client-field-new',
        component: ClientFieldPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientField.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-field/:id/edit',
        component: ClientFieldPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientField.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-field/:id/delete',
        component: ClientFieldDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientField.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
