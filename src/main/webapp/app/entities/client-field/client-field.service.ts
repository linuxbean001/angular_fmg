import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { ClientField } from './client-field.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<ClientField>;

@Injectable()
export class ClientFieldService {

    private resourceUrl =  SERVER_API_URL + 'api/client-fields';

    constructor(private http: HttpClient) { }

    create(clientField: ClientField): Observable<EntityResponseType> {
        const copy = this.convert(clientField);
        return this.http.post<ClientField>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(clientField: ClientField): Observable<EntityResponseType> {
        const copy = this.convert(clientField);
        return this.http.put<ClientField>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ClientField>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ClientField[]>> {
        const options = createRequestOption(req);
        return this.http.get<ClientField[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ClientField[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ClientField = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    convertArrayResponse(res: HttpResponse<ClientField[]>): HttpResponse<ClientField[]> {
        const jsonResponse: ClientField[] = res.body;
        const body: ClientField[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ClientField.
     */
    private convertItemFromServer(clientField: ClientField): ClientField {
        const copy: ClientField = Object.assign({}, clientField);
        return copy;
    }

    /**
     * Convert a ClientField to a JSON which can be sent to the server.
     */
    private convert(clientField: ClientField): ClientField {
        const copy: ClientField = Object.assign({}, clientField);
        return copy;
    }
}
