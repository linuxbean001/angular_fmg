import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ClientListData } from './client-list-data.model';
import { ClientListDataPopupService } from './client-list-data-popup.service';
import { ClientListDataService } from './client-list-data.service';

@Component({
    selector: 'jhi-client-list-data-delete-dialog',
    templateUrl: './client-list-data-delete-dialog.component.html'
})
export class ClientListDataDeleteDialogComponent {

    clientListData: ClientListData;

    constructor(
        private clientListDataService: ClientListDataService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.clientListDataService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'clientListDataListModification',
                content: 'Deleted an clientListData',
                id,
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-client-list-data-delete-popup',
    template: ''
})
export class ClientListDataDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientListDataPopupService: ClientListDataPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.clientListDataPopupService
                .open(ClientListDataDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
