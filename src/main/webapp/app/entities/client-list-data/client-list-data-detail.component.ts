import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { ClientListData } from './client-list-data.model';
import { ClientListDataService } from './client-list-data.service';

@Component({
    selector: 'jhi-client-list-data-detail',
    templateUrl: './client-list-data-detail.component.html'
})
export class ClientListDataDetailComponent implements OnInit, OnDestroy {

    clientListData: ClientListData;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private clientListDataService: ClientListDataService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInClientListData();
    }

    load(id) {
        this.clientListDataService.find(id)
            .subscribe((clientListDataResponse: HttpResponse<ClientListData>) => {
                this.clientListData = clientListDataResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInClientListData() {
        this.eventSubscriber = this.eventManager.subscribe(
            'clientListDataListModification',
            (response) => this.load(this.clientListData.id)
        );
    }
}
