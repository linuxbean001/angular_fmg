import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ClientListData } from './client-list-data.model';
import { ClientListDataPopupService } from './client-list-data-popup.service';
import { ClientListDataService } from './client-list-data.service';
import { ClientField, ClientFieldService } from '../client-field';

@Component({
    selector: 'jhi-client-list-data-dialog',
    templateUrl: './client-list-data-dialog.component.html'
})
export class ClientListDataDialogComponent implements OnInit {

    clientListData: ClientListData;
    isSaving: boolean;

    clientfields: ClientField[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private clientListDataService: ClientListDataService,
        private clientFieldService: ClientFieldService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.clientFieldService.query()
            .subscribe((res: HttpResponse<ClientField[]>) => { this.clientfields = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.clientListData.id !== undefined) {
            this.subscribeToSaveResponse(
                this.clientListDataService.update(this.clientListData));
        } else {
            this.subscribeToSaveResponse(
                this.clientListDataService.create(this.clientListData));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ClientListData>>) {
        result.subscribe((res: HttpResponse<ClientListData>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ClientListData) {
        this.eventManager.broadcast({ name: 'clientListDataListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackClientFieldById(index: number, item: ClientField) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-client-list-data-popup',
    template: ''
})
export class ClientListDataPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientListDataPopupService: ClientListDataPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.clientListDataPopupService
                    .open(ClientListDataDialogComponent as Component, params['id']);
            } else {
                this.clientListDataPopupService
                    .open(ClientListDataDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
