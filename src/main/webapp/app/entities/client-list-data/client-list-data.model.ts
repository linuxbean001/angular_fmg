import { BaseEntity } from './../../shared';

export class ClientListData implements BaseEntity {
    constructor(
        public id?: number,
        public rowNum?: number,
        public cvalue?: string,
        public clientFieldId?: number,
    ) {
    }
}
