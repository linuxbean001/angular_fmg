import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CmaSharedModule } from '../../shared';
import {
    ClientListDataService,
    ClientListDataPopupService,
    ClientListDataComponent,
    ClientListDataDetailComponent,
    ClientListDataDialogComponent,
    ClientListDataPopupComponent,
    ClientListDataDeletePopupComponent,
    ClientListDataDeleteDialogComponent,
    clientListDataRoute,
    clientListDataPopupRoute,
    ClientListDataResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...clientListDataRoute,
    ...clientListDataPopupRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ClientListDataComponent,
        ClientListDataDetailComponent,
        ClientListDataDialogComponent,
        ClientListDataDeleteDialogComponent,
        ClientListDataPopupComponent,
        ClientListDataDeletePopupComponent,
    ],
    entryComponents: [
        ClientListDataComponent,
        ClientListDataDialogComponent,
        ClientListDataPopupComponent,
        ClientListDataDeleteDialogComponent,
        ClientListDataDeletePopupComponent,
    ],
    providers: [
        ClientListDataService,
        ClientListDataPopupService,
        ClientListDataResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaClientListDataModule {}
