import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { ClientListDataComponent } from './client-list-data.component';
import { ClientListDataDetailComponent } from './client-list-data-detail.component';
import { ClientListDataPopupComponent } from './client-list-data-dialog.component';
import { ClientListDataDeletePopupComponent } from './client-list-data-delete-dialog.component';

@Injectable()
export class ClientListDataResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const clientListDataRoute: Routes = [
    {
        path: 'client-list-data',
        component: ClientListDataComponent,
        resolve: {
            'pagingParams': ClientListDataResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientListData.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'client-list-data/:id',
        component: ClientListDataDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientListData.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const clientListDataPopupRoute: Routes = [
    {
        path: 'client-list-data-new',
        component: ClientListDataPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientListData.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-list-data/:id/edit',
        component: ClientListDataPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientListData.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-list-data/:id/delete',
        component: ClientListDataDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientListData.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
