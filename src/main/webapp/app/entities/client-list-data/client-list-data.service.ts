import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';

import {ClientListData} from './client-list-data.model';
import {createRequestOption} from '../../shared';

export type EntityResponseType = HttpResponse<ClientListData>;

@Injectable()
export class ClientListDataService {

    private resourceUrl =  SERVER_API_URL + 'api/client-list-data';

    constructor(private http: HttpClient) { }

    create(clientListData: ClientListData): Observable<EntityResponseType> {
        const copy = this.convert(clientListData);
        return this.http.post<ClientListData>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(clientListData: ClientListData): Observable<EntityResponseType> {
        const copy = this.convert(clientListData);
        return this.http.put<ClientListData>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    updateByCoord(clientListData: ClientListData): Observable<EntityResponseType> {
        const copy = this.convert(clientListData);
        return this.http.put<ClientListData>(this.resourceUrl + '-by-coord', copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ClientListData>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ClientListData[]>> {
        const options = createRequestOption(req);
        return this.http.get<ClientListData[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ClientListData[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ClientListData = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ClientListData[]>): HttpResponse<ClientListData[]> {
        const jsonResponse: ClientListData[] = res.body;
        const body: ClientListData[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ClientListData.
     */
    private convertItemFromServer(clientListData: ClientListData): ClientListData {
        const copy: ClientListData = Object.assign({}, clientListData);
        return copy;
    }

    /**
     * Convert a ClientListData to a JSON which can be sent to the server.
     */
    private convert(clientListData: ClientListData): ClientListData {
        const copy: ClientListData = Object.assign({}, clientListData);
        return copy;
    }
}
