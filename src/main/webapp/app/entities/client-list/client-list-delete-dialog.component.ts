import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager} from 'ng-jhipster';

import {ClientList} from './client-list.model';
import {ClientListPopupService} from './client-list-popup.service';
import {ClientListService} from './client-list.service';

@Component({
    selector: 'jhi-client-list-delete-dialog',
    templateUrl: './client-list-delete-dialog.component.html'
})
export class ClientListDeleteDialogComponent {

    clientList: ClientList;

    constructor(
        private clientListService: ClientListService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.clientListService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'clientListListModification',
                content: 'Deleted an clientList',
                id
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-client-list-delete-popup',
    template: ''
})
export class ClientListDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientListPopupService: ClientListPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.clientListPopupService
                .open(ClientListDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
