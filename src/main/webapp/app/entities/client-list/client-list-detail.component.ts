import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpResponse} from '@angular/common/http';
import {Subscription} from 'rxjs/Subscription';
import {JhiEventManager} from 'ng-jhipster';

import {ClientList} from './client-list.model';
import {ClientListService} from './client-list.service';

@Component({
    selector: 'jhi-client-list-detail',
    templateUrl: './client-list-detail.component.html'
})
export class ClientListDetailComponent implements OnInit, OnDestroy {

    clientList: ClientList;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private clientListService: ClientListService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInClientLists();
    }

    load(id) {
        this.clientListService.find(id)
            .subscribe((clientListResponse: HttpResponse<ClientList>) => {
                this.clientList = clientListResponse.body;
            });
    }

    hasPreviousState() {
        return window.history.length - 1 > 0;
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInClientLists() {
        this.eventSubscriber = this.eventManager.subscribe(
            'clientListListModification',
            (response) => this.load(this.clientList.id)
        );
    }
}
