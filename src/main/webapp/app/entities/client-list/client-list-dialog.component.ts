import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';

import {ClientList} from './client-list.model';
import {ClientListPopupService} from './client-list-popup.service';
import {ClientListService} from './client-list.service';
import {ClientCampaign, ClientCampaignService} from '../client-campaign';
import {CampaignPortalUser} from '../campaign-portal-user';

@Component({
    selector: 'jhi-client-list-dialog',
    templateUrl: './client-list-dialog.component.html'
})
export class ClientListDialogComponent implements OnInit {

    clientList: ClientList;
    isSaving: boolean;
    clientcampaigns: ClientCampaign[];
    portalUsersForCampaigns: CampaignPortalUser[];
    allclentlistid:any = [];
    isfunsas: boolean = false;
   
    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private clientListService: ClientListService,
        private clientCampaignService: ClientCampaignService,
        private eventManager: JhiEventManager
    ) {
    }


 

    ngOnInit() {

   //client list of id
  
   for (const valss of this.clientList.portalUsers) {     
    this.allclentlistid.push(valss.id);
   }

        
        this.isSaving = false;
        this.clientCampaignService.query()
            .subscribe((res: HttpResponse<ClientCampaign[]>) => {
                this.clientcampaigns = res.body;

                for (const val of this.clientcampaigns) {
                    if (val.id === this.clientList.clientCampaignId) {
                        this.portalUsersForCampaigns = val.portalUsers;
                    }
                   
                }
              
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    changeLeagueOwner(){  
            this.isfunsas =true;
            this.allclentlistid = this.clientList.portalUsers;

    }

    onSubmit() {
        this.isSaving = true;
      


        if (this.clientList.id !== undefined) {
            this.subscribeToSaveResponse(
                this.clientListService.update(this.clientList));
        } else {
            this.subscribeToSaveResponse(
                this.clientListService.create(this.clientList));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ClientList>>) {
        result.subscribe((res: HttpResponse<ClientList>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ClientList) {
        this.eventManager.broadcast({
            name: 'clientListListModification',
            content: 'OK',
            clientList: result
        });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackClientCampaignById(index: number, item: ClientCampaign) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-client-list-popup',
    template: ''
})
export class ClientListPopupComponent implements OnInit, OnDestroy {

    routeSub: any;
    constructor(
        private route: ActivatedRoute,
        private clientListPopupService: ClientListPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.clientListPopupService
                    .open(ClientListDialogComponent as Component, params['id']);
            } else {
                this.clientListPopupService
                    .open(ClientListDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
