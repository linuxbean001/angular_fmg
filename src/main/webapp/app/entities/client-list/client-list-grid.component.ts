import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {JhiAlertService, JhiEventManager, JhiParseLinks} from 'ng-jhipster';

import {ClientListRow} from './client-list.model';
import {ITEMS_PER_PAGE, Principal} from '../../shared';
import {ClientListGridService} from './client-list-grid.service';
import {ClientField} from '../client-field';
import {ClientListData, ClientListDataService} from '../client-list-data';

@Component({
    selector: 'jhi-client-list-grid',
    templateUrl: './client-list-grid.component.html'
})
export class ClientListGridComponent implements OnInit, OnDestroy {

    currentAccount: any;
    error: any;
    success: any;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    eventSubscriber: Subscription;
    listId: number;

    editCell: { clientField?: ClientField, row?: ClientListRow } = {};
    enteredValue: string;
    @ViewChild('enterValue')
    enterValueField: ElementRef;

    subscription: Subscription;
    fields: ClientField[];
    rows: ClientListRow[];

    constructor(
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private clientListGridService: ClientListGridService,
        private clientListDataService: ClientListDataService,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });

        this.subscription = this.activatedRoute.params.subscribe((params) => {
            const id = params['id'];
            this.listId = id;

            this.loadFields(id);
            this.loadRows(id);
        });
    }

    loadFields(id) {
        this.clientListGridService.getFields(id)
            .subscribe((res: HttpResponse<ClientField[]>) => {
                this.fields = res.body;
            });
    }

    loadRows(id) {
        const req = {
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        };
        this.clientListGridService.getRows(id, req)
            .subscribe((res: HttpResponse<ClientListRow[]>) => this.onSuccess(res.body, res.headers));
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.routeData = this.activatedRoute.params.subscribe((params) => {
            const id = params['id'];
            this.router.navigate(['/client-list/' + id + '/grid'], {
                queryParams:
                    {
                        page: this.page,
                        size: this.itemsPerPage,
                        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
                    }
            });
            this.loadRows(id);
        });
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/client-list/' + this.activatedRoute.params['id'] + '/grid', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInClientLists();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.subscription);
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInClientLists() {
        this.eventSubscriber = this.eventManager.subscribe('clientListRowModification', (response) => this.loadRows(this.listId));
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'row_num') {
            result.push('row_num');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.rows = data;
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    isEditing(row: ClientListRow, clientField: ClientField): boolean {
        return this.editCell.row === row && this.editCell.clientField === clientField;
    }

    enterEditingMode(row: ClientListRow, clientField: ClientField) {
        this.enteredValue = row.values[clientField.id].cvalue;
        this.editCell.row = row;
        this.editCell.clientField = clientField;
        setTimeout(() => this.enterValueField.nativeElement.focus(), 0);
    }

    exitEditingMode() {
        this.editCell = {};
    }

    applyValue() {
        const row = this.editCell.row;
        const clientField = this.editCell.clientField;
        const cvalue = this.enteredValue;

        row.values[clientField.id].cvalue = cvalue;
        this.exitEditingMode();

        const clientListData = new ClientListData();
        clientListData.rowNum = row.rowNum;
        clientListData.clientFieldId = clientField.id;
        clientListData.cvalue = cvalue;
        this.clientListDataService.updateByCoord(clientListData).subscribe(
            (res: HttpResponse<ClientListData>) => {
                // this.onSuccess(res.body, res.headers)
            },
            (res: HttpErrorResponse) => this.onError(res.message));
    }

    hasPreviousState() {
        return window.history.length - 1 > 0;
    }

    previousState() {
        window.history.back();
    }
}
