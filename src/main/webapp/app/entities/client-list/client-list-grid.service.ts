import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';

import {ClientListRow, DataListCompany, DataListContact, DataListSummary} from './client-list.model';
import {ClientField, ClientFieldService} from '../client-field';
import {createRequestOption} from '../../shared';

@Injectable()
export class ClientListGridService {

    private resourceUrl = SERVER_API_URL + 'api/client-lists';

    constructor(private http: HttpClient,
                private clientFieldService: ClientFieldService
    ) {
    }

    getFields(id: number): Observable<HttpResponse<ClientField[]>> {
        return this.http.get<ClientField[]>(`${this.resourceUrl}/${id}/fields`, {observe: 'response'})
            .map((res: HttpResponse<ClientField[]>) => this.clientFieldService.convertArrayResponse(res));
    }

    getRows(id: number, req?: any): Observable<HttpResponse<ClientListRow[]>> {
        const options = createRequestOption(req);
        return this.http.get<ClientListRow[]>(`${this.resourceUrl}/${id}/rows`, {params: options, observe: 'response'})
            .map((res: HttpResponse<ClientListRow[]>) => res);
    }

    delete(id, rowNum: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}/rows/${rowNum}`, {observe: 'response'});
    }

    getSummary(id: number): Observable<HttpResponse<DataListSummary>> {
        return this.http.get<DataListSummary>(`${this.resourceUrl}/${id}/summary`, {observe: 'response'})
            .map((res: HttpResponse<DataListSummary>) => res);
    }

    // getCompanyContacts(id: number, orgInformationPk: DataListCompanyPk): Observable<HttpResponse<DataListContact[]>> {
    //     return this.http.post<DataListContact[]>(`${this.resourceUrl}/${id}/company-contacts`, orgInformationPk, {observe: 'response'})
    //         .map((res: HttpResponse<DataListContact[]>) => res);
    // }

    saveCompanyContact(listId: number, company: DataListCompany, contact: DataListContact): Observable<HttpResponse<DataListContact>> {
        const formData = new FormData();
        formData.append('company', new Blob([JSON.stringify(company)], {
            type: 'application/json'
        }));
        formData.append('contact', new Blob([JSON.stringify(contact)], {
            type: 'application/json'
        }));

        const url = `${this.resourceUrl}/${listId}/save-company-contact`;
        return this.http.post<DataListContact>(url, formData, {observe: 'response'});
    }

    saveCompany(listId: number, company: DataListCompany): Observable<HttpResponse<DataListCompany>> {
        const url = `${this.resourceUrl}/${listId}/save-company`;
        return this.http.post<any>(url, company, {observe: 'response'});
    }

    saveCompanyNote(listId: number, rowNum: number, note: string): Observable<HttpResponse<any>> {
        const formData = new FormData();
        formData.append('rowNum', new Blob([rowNum], {
            type: 'application/json'
        }));
        formData.append('note', new Blob([note], {
            type: 'application/json'
        }));

        const url = `${this.resourceUrl}/${listId}/save-company-note`;
        return this.http.post<number>(url, formData, {observe: 'response'});
    }

    moveContactToAnotherList(oldListId, newListId, contactRowNum: number): Observable<HttpResponse<any>> {
        const url = `${this.resourceUrl}/${oldListId}/move-contact/${contactRowNum}/to/${newListId}`;
        return this.http.post<DataListContact>(url, null, {observe: 'response'});
    }
}
