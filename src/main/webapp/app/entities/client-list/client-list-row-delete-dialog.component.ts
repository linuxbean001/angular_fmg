import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager} from 'ng-jhipster';

import {ClientList} from './client-list.model';
import {ClientListRowPopupService} from './client-list-row-popup.service';
import {ClientListGridService} from './client-list-grid.service';

@Component({
    selector: 'jhi-client-list-row-delete-dialog',
    templateUrl: './client-list-row-delete-dialog.component.html'
})
export class ClientListRowDeleteDialogComponent {

    clientList: ClientList;
    rowNum: number;

    constructor(
        private clientListGridService: ClientListGridService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id, rowNum: number) {
        this.clientListGridService.delete(id, rowNum).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'clientListRowModification',
                content: 'Deleted an row',
                id,
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-client-list-row-delete-popup',
    template: ''
})
export class ClientListRowDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientListRowPopupService: ClientListRowPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.clientListRowPopupService
                .open(ClientListRowDeleteDialogComponent as Component, params['id'], params['rowNum']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
