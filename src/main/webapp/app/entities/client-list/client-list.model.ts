import {BaseEntity} from './../../shared';
import {ClientListMapping} from '../client-campaign';
import {ClientField} from '../client-field';
import {CallHistory} from '../call-history';
import {CampaignPortalUser} from '../campaign-portal-user';

export class ClientList implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public clientCampaignName?: string,
        public clientCampaignId?: number,
        public mapping?: ClientListMapping,
        public portalUsers?: CampaignPortalUser[],
        public rowCount?: number
    ) {
    }
}

export class RowValueDto {
    constructor(
        public cvalue?: string,
    ) {
    }
}

export class ClientListRow {
    constructor(
        public rowNum?: number,
        public values?: { [id: number]: RowValueDto },
    ) {
    }
}

/**
 * See {@link DataListSummary}.
 */
export class DataListSummary {
    constructor(
        /**
         * Fields with {@link com.fmg.cma.domain.ClientField#isOrgInformation} = true.
         */
        public pkFields: ClientField[],
        /**
         * Companies.
         */
        public companies: DataListCompany[],
        /**
         * Other non-pk fields.
         */
        public fields: ClientField[],
    ) {
    }
}

/**
 * See {@link DataListCompany}.
 */
export class DataListCompany {
    static isFieldsValueEquals(valueMap1, valueMap2: { [id: number]: string }, clientFields: ClientField[]): boolean {
        for (const clientField of clientFields) {
            if (valueMap1[clientField.id] !== valueMap2[clientField.id]) {
                return false;
            }
        }
        return true;
    }

    static fieldValuesToString(clientFields: ClientField[], valueMap: { [id: number]: string }): string {
        let str = '';
        let prefix = '';
        clientFields
            .forEach((clientField) => {
                str += `${prefix}${clientField.fieldName}: ${valueMap[clientField.id]}`;
                prefix = ', ';
            });
        return str;
    }

    constructor(
        public valueMap: { [id: number]: string },
        public rowNum: number,
        public rowNums: number[],
        public note: string,
        /**
         * Contacts.
         */
        public contacts: DataListContact[],
        /**
         * Call histories.
         */
        public histories: CallHistory[],
        /**
         * Used only at client.
         */
        public isDummy?: boolean,
        /**
         * Is changed.
         */
        public isChanged?: boolean,
    ) {
    }
}

/**
 * See {@link DataListContact}.
 */
export class DataListContact {
    constructor(
        public contactType: string,
        public rowNum: number,
        public note: string,
        public isLeftCompany: boolean,
        // public fields: ClientField[],
        public valueMap: { [id: number]: string },
        /**
         * Used only at client.
         */
        public isDummy?: boolean,
        /**
         * Is changed.
         */
        public isChanged?: boolean,
        /**
         * See {@link CampaignListContactResult#state}.
         */
        public state?: string,
        //
        public assignUserLogin?: string,
        public assignUserId?: number,
        public assignStamp?: any,
        public clickedOutcomeId?: number,
    ) {
    }
}

/**
 * See {@link DataListCompanyPk}.
 */
export class DataListCompanyPk {
    constructor(
        public valueMap: { [id: number]: string },
    ) {
    }
}
