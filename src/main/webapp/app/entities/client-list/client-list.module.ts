import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {CmaSharedModule} from '../../shared';
import {
    ClientListComponent,
    ClientListDeleteDialogComponent,
    ClientListDeletePopupComponent,
    ClientListDetailComponent,
    ClientListDialogComponent,
    ClientListGridComponent,
    ClientListGridResolvePagingParams,
    ClientListGridService,
    ClientListPopupComponent,
    clientListPopupRoute,
    ClientListPopupService,
    ClientListResolvePagingParams,
    clientListRoute,
    ClientListService,
} from './';
import {
    ClientListRowDeleteDialogComponent,
    ClientListRowDeletePopupComponent
} from './client-list-row-delete-dialog.component';
import {ClientListRowPopupService} from './client-list-row-popup.service';

const ENTITY_STATES = [
    ...clientListRoute,
    ...clientListPopupRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ClientListComponent,
        ClientListGridComponent,
        ClientListDetailComponent,
        ClientListDialogComponent,
        ClientListDeleteDialogComponent,
        ClientListRowDeleteDialogComponent,
        ClientListPopupComponent,
        ClientListDeletePopupComponent,
        ClientListRowDeletePopupComponent,
    ],
    entryComponents: [
        ClientListComponent,
        ClientListGridComponent,
        ClientListDialogComponent,
        ClientListPopupComponent,
        ClientListDeleteDialogComponent,
        ClientListRowDeleteDialogComponent,
        ClientListDeletePopupComponent,
        ClientListRowDeletePopupComponent,
    ],
    providers: [
        ClientListService,
        ClientListGridService,
        ClientListPopupService,
        ClientListRowPopupService,
        ClientListResolvePagingParams,
        ClientListGridResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaClientListModule {
}
