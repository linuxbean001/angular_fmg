import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes} from '@angular/router';
import {JhiPaginationUtil} from 'ng-jhipster';

import {UserRouteAccessService} from '../../shared';
import {ClientListComponent} from './client-list.component';
import {ClientListDetailComponent} from './client-list-detail.component';
import {ClientListPopupComponent} from './client-list-dialog.component';
import {ClientListDeletePopupComponent} from './client-list-delete-dialog.component';
import {ClientListGridComponent} from './client-list-grid.component';
import {ClientListRowDeletePopupComponent} from './client-list-row-delete-dialog.component';

@Injectable()
export class ClientListResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

@Injectable()
export class ClientListGridResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'row_num,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const clientListRoute: Routes = [
    {
        path: 'client-list',
        component: ClientListComponent,
        resolve: {
            'pagingParams': ClientListResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientList.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'client-list/:id',
        component: ClientListDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientList.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'client-list/:id/grid',
        component: ClientListGridComponent,
        resolve: {
            'pagingParams': ClientListGridResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientList.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const clientListPopupRoute: Routes = [
    {
        path: 'client-list-new',
        component: ClientListPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientList.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-list/:id/edit',
        component: ClientListPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientList.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-list/:id/delete',
        component: ClientListDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientList.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-list/:id/rows/:rowNum/delete',
        component: ClientListRowDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.clientList.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
