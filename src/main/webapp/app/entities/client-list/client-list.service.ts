import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';

import {ClientList} from './client-list.model';
import {createRequestOption} from '../../shared';

export type EntityResponseType = HttpResponse<ClientList>;

@Injectable()
export class ClientListService {

    private resourceUrl = SERVER_API_URL + 'api/client-lists';

    constructor(private http: HttpClient) {
    }

    create(clientList: ClientList, file?: File): Observable<EntityResponseType> {
        // const copy = this.convert(clientList);
        // return this.http.post<ClientList>(this.resourceUrl, copy, { observe: 'response' })
        //     .map((res: EntityResponseType) => this.convertResponse(res));

        const formData = new FormData();
        if (file) {
            formData.append('file', file, file.name);
        }
        formData.append('clientList', new Blob([JSON.stringify(this.convert(clientList))], {
            type: 'application/json'
        }));

        return this.http.post<ClientList>(this.resourceUrl, formData, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(clientList: ClientList): Observable<EntityResponseType> {
        // const copy = this.convert(clientList);
        // return this.http.put<ClientList>(this.resourceUrl, copy, {observe: 'response'})
        //     .map((res: EntityResponseType) => this.convertResponse(res));

        const formData = new FormData();
        formData.append('clientList', new Blob([JSON.stringify(this.convert(clientList))], {
            type: 'application/json'
        }));

        return this.http.put<ClientList>(this.resourceUrl, formData, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ClientList>(`${this.resourceUrl}/${id}`, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ClientList[]>> {
        const options = createRequestOption(req);
        return this.http.get<ClientList[]>(this.resourceUrl, {params: options, observe: 'response'})
            .map((res: HttpResponse<ClientList[]>) => this.convertArrayResponse(res));
    }

    getClientCampaignClientLists(clientCampaignId: number, req?: any): Observable<HttpResponse<ClientList[]>> {
        const options = createRequestOption(req);
        const url = `${SERVER_API_URL}api/client-campaigns/${clientCampaignId}/client-lists`;
        return this.http.get<ClientList[]>(url, {params: options, observe: 'response'})
            .map((res: HttpResponse<ClientList[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ClientList = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ClientList[]>): HttpResponse<ClientList[]> {
        const jsonResponse: ClientList[] = res.body;
        const body: ClientList[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ClientList.
     */
    private convertItemFromServer(clientList: ClientList): ClientList {
        const copy: ClientList = Object.assign({}, clientList);
        return copy;
    }

    /**
     * Convert a ClientList to a JSON which can be sent to the server.
     */
    private convert(clientList: ClientList): ClientList {
        const copy: ClientList = Object.assign({}, clientList);
        return copy;
    }
}
