import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager} from 'ng-jhipster';

import {ConsoleStats} from './console-stats.model';
import {ConsoleStatsPopupService} from './console-stats-popup.service';
import {ConsoleStatsService} from './console-stats.service';

@Component({
    selector: 'jhi-console-stats-delete-dialog',
    templateUrl: './console-stats-delete-dialog.component.html'
})
export class ConsoleStatsDeleteDialogComponent {

    consoleStats: ConsoleStats;

    constructor(
        private consoleStatsService: ConsoleStatsService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.consoleStatsService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'consoleStatsListModification',
                content: 'Deleted an consoleStats'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-console-stats-delete-popup',
    template: ''
})
export class ConsoleStatsDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private consoleStatsPopupService: ConsoleStatsPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.consoleStatsPopupService
                .open(ConsoleStatsDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
