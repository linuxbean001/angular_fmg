import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpResponse} from '@angular/common/http';
import {Subscription} from 'rxjs/Subscription';
import {JhiEventManager} from 'ng-jhipster';

import {ConsoleStats} from './console-stats.model';
import {ConsoleStatsService} from './console-stats.service';

@Component({
    selector: 'jhi-console-stats-detail',
    templateUrl: './console-stats-detail.component.html'
})
export class ConsoleStatsDetailComponent implements OnInit, OnDestroy {

    consoleStats: ConsoleStats;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private consoleStatsService: ConsoleStatsService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInConsoleStats();
    }

    load(id) {
        this.consoleStatsService.find(id)
            .subscribe((consoleStatsResponse: HttpResponse<ConsoleStats>) => {
                this.consoleStats = consoleStatsResponse.body;
            });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInConsoleStats() {
        this.eventSubscriber = this.eventManager.subscribe(
            'consoleStatsListModification',
            (response) => this.load(this.consoleStats.id)
        );
    }
}
