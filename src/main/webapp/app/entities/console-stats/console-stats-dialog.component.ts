import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';

import {ConsoleStats} from './console-stats.model';
import {ConsoleStatsPopupService} from './console-stats-popup.service';
import {ConsoleStatsService} from './console-stats.service';
import {User, UserService} from '../../shared';

@Component({
    selector: 'jhi-console-stats-dialog',
    templateUrl: './console-stats-dialog.component.html'
})
export class ConsoleStatsDialogComponent implements OnInit {

    consoleStats: ConsoleStats;
    isSaving: boolean;

    users: User[];
    stampDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private consoleStatsService: ConsoleStatsService,
        private userService: UserService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.userService.query()
            .subscribe((res: HttpResponse<User[]>) => {
                this.users = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.consoleStats.id !== undefined) {
            this.subscribeToSaveResponse(
                this.consoleStatsService.update(this.consoleStats));
        } else {
            this.subscribeToSaveResponse(
                this.consoleStatsService.create(this.consoleStats));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ConsoleStats>>) {
        result.subscribe((res: HttpResponse<ConsoleStats>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ConsoleStats) {
        this.eventManager.broadcast({name: 'consoleStatsListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-console-stats-popup',
    template: ''
})
export class ConsoleStatsPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private consoleStatsPopupService: ConsoleStatsPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.consoleStatsPopupService
                    .open(ConsoleStatsDialogComponent as Component, params['id']);
            } else {
                this.consoleStatsPopupService
                    .open(ConsoleStatsDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
