import {Component, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {HttpResponse} from '@angular/common/http';
import {ConsoleStats} from './console-stats.model';
import {ConsoleStatsService} from './console-stats.service';

@Injectable()
export class ConsoleStatsPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private consoleStatsService: ConsoleStatsService
    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.consoleStatsService.find(id)
                    .subscribe((consoleStatsResponse: HttpResponse<ConsoleStats>) => {
                        const consoleStats: ConsoleStats = consoleStatsResponse.body;
                        if (consoleStats.stamp) {
                            consoleStats.stamp = {
                                year: consoleStats.stamp.getFullYear(),
                                month: consoleStats.stamp.getMonth() + 1,
                                day: consoleStats.stamp.getDate()
                            };
                        }
                        this.ngbModalRef = this.consoleStatsModalRef(component, consoleStats);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.consoleStatsModalRef(component, new ConsoleStats());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    consoleStatsModalRef(component: Component, consoleStats: ConsoleStats): NgbModalRef {
        const modalRef = this.modalService.open(component, {size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.consoleStats = consoleStats;
        modalRef.result.then((result) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
