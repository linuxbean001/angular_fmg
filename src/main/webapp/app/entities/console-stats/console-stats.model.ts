import {BaseEntity} from './../../shared';

export class ConsoleStats implements BaseEntity {
    constructor(
        public id?: number,
        public userId?: number,
        public userLogin?: string,
        public stamp?: any,
        public calls?: number,
        public skips?: number,
        public pending?: number,
        public dmContact?: number,
        public leadSales?: number,
        public pipelineOpps?: number,
    ) {
    }
}
