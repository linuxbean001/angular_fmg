import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {CmaSharedModule} from '../../shared';
import {CmaAdminModule} from '../../admin/admin.module';
import {
    ConsoleStatsComponent,
    ConsoleStatsDeleteDialogComponent,
    ConsoleStatsDeletePopupComponent,
    ConsoleStatsDetailComponent,
    ConsoleStatsDialogComponent,
    ConsoleStatsPopupComponent,
    consoleStatsPopupRoute,
    ConsoleStatsPopupService,
    ConsoleStatsResolvePagingParams,
    consoleStatsRoute,
    ConsoleStatsService,
} from './';

const ENTITY_STATES = [
    ...consoleStatsRoute,
    ...consoleStatsPopupRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        CmaAdminModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ConsoleStatsComponent,
        ConsoleStatsDetailComponent,
        ConsoleStatsDialogComponent,
        ConsoleStatsDeleteDialogComponent,
        ConsoleStatsPopupComponent,
        ConsoleStatsDeletePopupComponent,
    ],
    entryComponents: [
        ConsoleStatsComponent,
        ConsoleStatsDialogComponent,
        ConsoleStatsPopupComponent,
        ConsoleStatsDeleteDialogComponent,
        ConsoleStatsDeletePopupComponent,
    ],
    providers: [
        ConsoleStatsService,
        ConsoleStatsPopupService,
        ConsoleStatsResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaConsoleStatsModule {
}
