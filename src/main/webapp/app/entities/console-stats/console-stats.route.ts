import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes} from '@angular/router';
import {JhiPaginationUtil} from 'ng-jhipster';

import {UserRouteAccessService} from '../../shared';
import {ConsoleStatsComponent} from './console-stats.component';
import {ConsoleStatsDetailComponent} from './console-stats-detail.component';
import {ConsoleStatsPopupComponent} from './console-stats-dialog.component';
import {ConsoleStatsDeletePopupComponent} from './console-stats-delete-dialog.component';

@Injectable()
export class ConsoleStatsResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const consoleStatsRoute: Routes = [
    {
        path: 'console-stats',
        component: ConsoleStatsComponent,
        resolve: {
            'pagingParams': ConsoleStatsResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.consoleStats.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'console-stats/:id',
        component: ConsoleStatsDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.consoleStats.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const consoleStatsPopupRoute: Routes = [
    {
        path: 'console-stats-new',
        component: ConsoleStatsPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.consoleStats.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'console-stats/:id/edit',
        component: ConsoleStatsPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.consoleStats.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'console-stats/:id/delete',
        component: ConsoleStatsDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.consoleStats.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
