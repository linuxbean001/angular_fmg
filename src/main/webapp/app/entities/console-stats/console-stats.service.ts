import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';

import {JhiDateUtils} from 'ng-jhipster';

import {ConsoleStats} from './console-stats.model';
import {createRequestOption} from '../../shared';

export type EntityResponseType = HttpResponse<ConsoleStats>;

@Injectable()
export class ConsoleStatsService {

    private resourceUrl = SERVER_API_URL + 'api/console-stats';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) {
    }

    create(consoleStats: ConsoleStats): Observable<EntityResponseType> {
        const copy = this.convert(consoleStats);
        return this.http.post<ConsoleStats>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(consoleStats: ConsoleStats): Observable<EntityResponseType> {
        const copy = this.convert(consoleStats);
        return this.http.put<ConsoleStats>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ConsoleStats>(`${this.resourceUrl}/${id}`, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ConsoleStats[]>> {
        const options = createRequestOption(req);
        return this.http.get<ConsoleStats[]>(this.resourceUrl, {params: options, observe: 'response'})
            .map((res: HttpResponse<ConsoleStats[]>) => this.convertArrayResponse(res));
    }

    getByUserIsCurrentUserAndStampIsCurrentDate(): Observable<HttpResponse<ConsoleStats>> {
        return this.http.get<ConsoleStats>(`${this.resourceUrl}/current-date`, {observe: 'response'})
            .map((res: HttpResponse<ConsoleStats>) => this.convertResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }

    incrementSkips(): Observable<EntityResponseType> {
        return this.http.post<ConsoleStats>(`${this.resourceUrl}/incrementSkips`, null, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    incrementCalls(): Observable<EntityResponseType> {
        return this.http.post<ConsoleStats>(`${this.resourceUrl}/incrementCalls`, null, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    incrementPipelineOpps(): Observable<EntityResponseType> {
        return this.http.post<ConsoleStats>(`${this.resourceUrl}/incrementPipelineOpps`, null, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    incrementDMContact(): Observable<EntityResponseType> {
        return this.http.post<ConsoleStats>(`${this.resourceUrl}/incrementDMContact`, null, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ConsoleStats = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ConsoleStats[]>): HttpResponse<ConsoleStats[]> {
        const jsonResponse: ConsoleStats[] = res.body;
        const body: ConsoleStats[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ConsoleStats.
     */
    private convertItemFromServer(consoleStats: ConsoleStats): ConsoleStats {
        const copy: ConsoleStats = Object.assign({}, consoleStats);
        copy.stamp = this.dateUtils
            .convertLocalDateFromServer(consoleStats.stamp);
        return copy;
    }

    /**
     * Convert a ConsoleStats to a JSON which can be sent to the server.
     */
    private convert(consoleStats: ConsoleStats): ConsoleStats {
        const copy: ConsoleStats = Object.assign({}, consoleStats);
        copy.stamp = this.dateUtils
            .convertLocalDateToServer(consoleStats.stamp);
        return copy;
    }
}
