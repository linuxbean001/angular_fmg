import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {CmaSharedModule} from '../../shared';
import {consoleRoute} from './console.route';
import {CallbackPipe, ConsoleComponent} from './console.component';
import {Tab} from '../../layouts/tabs/tab.component';
import {Tabs} from '../../layouts/tabs/tabs.component';
import {TabModule} from '../../layouts/tabs/tab.module';
import {AngularSplitModule} from 'angular-split';
import {ConsoleScriptComponent} from './pages/script.component';
import {ConsoleSmsComponent} from './pages/sms.component';
import {NewSmsComponent} from './pages/new-sms.component';
import {ConsoleEmailComponent} from './pages/email.component';
import {CmaClientCampaignModule} from '../client-campaign/client-campaign.module';
import {ConsoleAddDocComponent} from './pages/add-doc.component';
import {ConsoleCallHistoryComponent} from './pages/call-history.component';
import {ConsoleOutcomeComponent} from './pages/outcome.component';
import {ConsoleNoContactComponent} from './pages/no-contact.component';
import {OutcomeWizardComponent} from './pages/outcome-tab/outcome-wizard.component';
import {ConsoleOutcomeBookingComponent} from './pages/outcome-tab/console-outcome-booking.component';
import {ConsoleOutcomeEmailComponent} from './pages/outcome-tab/console-outcome-email.component';
import {ConsoleOutcomeSmsComponent} from './pages/outcome-tab/console-outcome-sms.component';
import {CmaEmailTemplateModule} from '../email-template/email-template.module';
import {CmaClientCampaignSmsModule} from '../client-campaign-sms/client-campaign-sms.module';
import {ConsoleOutcomeSubReasonComponent} from './pages/outcome-tab/console-outcome-sub-reason.component';
import {ConsoleOutcomeContactComponent} from './pages/outcome-tab/console-outcome-contact.component';
import {ConsoleOutcomeMoveComponent} from './pages/outcome-tab/console-outcome-move.component';
import {ConsoleOutcomeApplyComponent} from './pages/outcome-tab/console-outcome-apply.component';
import {ConsoleOutcomeQaApprovalComponent} from './pages/outcome-tab/console-outcome-qa-approval.component';

const ENTITY_STATES = [
    ...consoleRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        TabModule,
        RouterModule.forChild(ENTITY_STATES),
        AngularSplitModule,
        CmaClientCampaignModule,
        CmaEmailTemplateModule,
        CmaClientCampaignSmsModule,
    ],
    declarations: [
        ConsoleComponent,
        ConsoleScriptComponent,
        ConsoleSmsComponent,
        NewSmsComponent,
        ConsoleEmailComponent,
        ConsoleAddDocComponent,
        ConsoleCallHistoryComponent,
        ConsoleOutcomeComponent,
        ConsoleNoContactComponent,
        OutcomeWizardComponent,
        ConsoleOutcomeBookingComponent,
        ConsoleOutcomeSubReasonComponent,
        ConsoleOutcomeEmailComponent,
        ConsoleOutcomeSmsComponent,
        ConsoleOutcomeContactComponent,
        ConsoleOutcomeMoveComponent,
        ConsoleOutcomeQaApprovalComponent,
        ConsoleOutcomeApplyComponent,
        CallbackPipe,
    ],
    entryComponents: [
        ConsoleComponent,
        Tab,
        Tabs,
        ConsoleScriptComponent,
        ConsoleSmsComponent,
        NewSmsComponent,
        ConsoleEmailComponent,
        ConsoleAddDocComponent,
        ConsoleCallHistoryComponent,
        ConsoleOutcomeComponent,
        ConsoleNoContactComponent,
        OutcomeWizardComponent,
        ConsoleOutcomeBookingComponent,
        ConsoleOutcomeSubReasonComponent,
        ConsoleOutcomeEmailComponent,
        ConsoleOutcomeSmsComponent,
        ConsoleOutcomeContactComponent,
        ConsoleOutcomeMoveComponent,
        ConsoleOutcomeQaApprovalComponent,
        ConsoleOutcomeApplyComponent,
    ],
    providers: [
        // ConsoleParamsResolve,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaConsoleModule {
}
