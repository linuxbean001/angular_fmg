import {Routes} from '@angular/router';

import {UserRouteAccessService} from '../../shared';
import {ConsoleComponent} from './console.component';

export class ConsoleParams {
    constructor(
        public method: string, // APPROVE_ONLY
        public campaignId: number,
        public listId: number,
        public contactRowNums: number[],
    ) {
    }
}

// @Injectable()
// export class ConsoleParamsResolve implements Resolve<any> {
//
//     constructor() {
//     }
//
//     resolve(route: ActivatedRouteSnapshot, snapshot: RouterStateSnapshot) {
//         const method = route.queryParams['method'];
//         // tslint:disable-next-line:radix
//         const campaignId = route.queryParams['campaignId'] ? parseInt(route.queryParams['campaignId']) : null;
//         // tslint:disable-next-line:radix
//         const listId = route.queryParams['listId'] ? parseInt(route.queryParams['listId']) : null;
//         // tslint:disable-next-line:radix
//         const contactRowNums = route.queryParams['contactRowNums'] ? route.queryParams['contactRowNums'].map((str) => parseInt(str)) : null;
//         return new ConsoleParams(method, campaignId, listId, contactRowNums);
//     }
// }

export const consoleRoute: Routes = [
    {
        path: 'console',
        component: ConsoleComponent,
        // resolve: {
        //     'consoleParams': ConsoleParamsResolve
        // },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'console.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
