import {Component, Input} from '@angular/core';
import {ClientCampaign} from '../../client-campaign';

@Component({
    selector: 'jhi-console-add-doc',
    templateUrl: './add-doc.component.html'
})
export class ConsoleAddDocComponent {

    @Input() clientCampaign: ClientCampaign;
    @Input()
    panelsNgStyle: { [key: string]: string; };

    constructor() {
    }

}
