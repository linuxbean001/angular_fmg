import {Component, Input} from '@angular/core';
import {DataListCompany, DataListContact, DataListSummary} from '../../client-list';
import {CallHistory} from '../../call-history';

@Component({
    selector: 'jhi-console-call-history',
    templateUrl: './call-history.component.html'
})
export class ConsoleCallHistoryComponent {

    @Input() dataListSummary: DataListSummary;

    _dataListCompany: DataListCompany;
    @Input()
    set dataListCompany(value: DataListCompany) {
        this._dataListCompany = value;
    }

    _dataListContact: DataListContact;
    @Input()
    set dataListContact(value: DataListContact) {
        this._dataListContact = value;
        this.fillHistories();
    }

    @Input()
    panelsNgStyle: { [key: string]: string; };

    mode = CallHistoryMode.SHOW_ALL;
    CallHistoryMode = CallHistoryMode;
    callHistories: CallHistory[] = [];

    constructor() {
    }

    setMode(mode: CallHistoryMode) {
        this.mode = mode;
        this.fillHistories();
    }

    fillHistories() {
        if (this.mode === CallHistoryMode.SHOW_ALL) {
            // history relates to all transactions against that Organisation
            this.callHistories = this._dataListCompany.histories;
        } else if (this._dataListContact) {
            this.callHistories = this._dataListCompany.histories
                .filter((history) => history.contactRowNum === this._dataListContact.rowNum);
        } else {
            this.callHistories = [];
        }
    }
}

// @Pipe({
//     name: 'callback',
//     pure: false
// })
// export class CallbackPipe implements PipeTransform {
//     transform(items: any[], callback: (item: any) => boolean): any {
//         if (!items || !callback) {
//             return items;
//         }
//         return items.filter((item) => callback(item));
//     }
// }

export class CallHistoryMode {
    static SHOW_ALL = new CallHistoryMode();
    static SHOW_BY_CONTACT = new CallHistoryMode();
}
