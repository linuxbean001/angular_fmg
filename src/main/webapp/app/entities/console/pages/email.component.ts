import {Component, Input} from '@angular/core';
import {ClientCampaign} from '../../client-campaign';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {
    EmailTemplate,
    EmailTemplateDialogComponent,
    EmailTemplateDialogComponentMode,
    EmailTemplatePopupService
} from '../../email-template';
import {CallHistory, CallHistoryService} from '../../call-history';
import {CallHistoryType} from '../../call-history-type';
import {ClientList, DataListCompany, DataListContact, DataListSummary} from '../../client-list';
import {ClientField} from '../../client-field';

@Component({
    selector: 'jhi-console-email',
    templateUrl: './email.component.html'
})
export class ConsoleEmailComponent {

    static emailField = 'Email';

    @Input() clientCampaign: ClientCampaign;
    @Input() clientList: ClientList;
    @Input() dataListSummary: DataListSummary;
    @Input() dataListCompany: DataListCompany;
    @Input() dataListContact: DataListContact;
    @Input()
    panelsNgStyle: { [key: string]: string; };
    @Input() readonly = false;

    static getEmailTo(fields: ClientField[], valueMap: { [id: number]: string }) {
        const clientField: ClientField = fields.find((field) => field.fieldName.toLowerCase() === ConsoleEmailComponent.emailField.toLowerCase());
        if (!clientField) {
            return `Could not find the field "${ConsoleEmailComponent.emailField}"`;
        }
        return valueMap[clientField.id];
    }

    constructor(
        private modalService: NgbModal,
        private emailTemplatePopupService: EmailTemplatePopupService,
        private callHistoryService: CallHistoryService,
    ) {
    }

    getEmailTo() {
        return ConsoleEmailComponent.getEmailTo(this.dataListSummary.fields, this.dataListContact.valueMap);
    }

    newEmail() {
        this.sendEmail(new EmailTemplate());
    }

    selectFromTemplate(emailTemplate: EmailTemplate) {
        // make shallow copy
        this.sendEmail({...emailTemplate});
    }

    sendEmail(emailTemplate: EmailTemplate) {
        emailTemplate.to = this.getEmailTo();
        this.emailTemplatePopupService.openStandalone(EmailTemplateDialogComponent as Component, emailTemplate,
            false, EmailTemplateDialogComponentMode.SEND)
            .then((ngbModalRef: NgbModalRef) => {
                ngbModalRef.result
                    .then((resultEmailTemplate: EmailTemplate) => {
                        this.addEmailAuditRecord(emailTemplate.id);
                    });
            });
    }

    addEmailAuditRecord(emailTemplateId: number) {
        const callHistory = new CallHistory({
            typeIdent: CallHistoryType.EMAIL,
            listId: this.clientList.id,
            orgRowNum: this.dataListCompany.rowNum,
            contactRowNum: this.dataListContact && this.dataListContact.rowNum,
            emailTemplateId
        });
        this.callHistoryService.create(callHistory).toPromise()
            .then((response) => {
                this.dataListCompany.histories.push(response.body);
            });
    }
}
