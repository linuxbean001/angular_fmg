import {Component, Input} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-new-sms',
    templateUrl: './new-sms.component.html'
})
export class NewSmsComponent {

    @Input() text: string;
    @Input() phone: string;

    constructor(
        private modalService: NgbModal,
        private activeModal: NgbActiveModal,
    ) {
    }

    send() {
        this.activeModal.close({text: this.text, phone: this.phone});
    }

    cancel() {
        this.activeModal.dismiss('not confirmed');
    }
}
