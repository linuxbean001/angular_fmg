import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ClientCampaign} from '../../client-campaign';
import {ClientList, DataListCompany, DataListContact} from '../../client-list';
import {CallHistory, CallHistoryService} from '../../call-history';
import {ConfirmOptions, ConfirmService} from '../../../shared';

@Component({
    selector: 'jhi-console-no-contact',
    templateUrl: './no-contact.component.html'
})
export class ConsoleNoContactComponent {

    @Input() clientCampaign: ClientCampaign;
    @Input() clientList: ClientList;
    @Input() dataListCompany: DataListCompany;
    @Input() dataListContact: DataListContact;
    @Input()
    panelsNgStyle: { [key: string]: string; };
    @Input() readonly = false;
    @Output() apply: EventEmitter<CallHistory> = new EventEmitter();

    noContact: NoContact;
    NoContact = NoContact;

    constructor(
        private callHistoryService: CallHistoryService,
        private confirmService: ConfirmService,
    ) {
        this.reset();
    }

    applyToOrganisation() {
        this.addCallHistory().then(() => this.reset());
    }

    applyToContact() {
        this.addCallHistory(this.dataListContact && this.dataListContact.rowNum).then(() => this.reset());
    }

    addCallHistory(contactRowNum?: number) {
        if (!this.clientList) {
            const options: ConfirmOptions = {
                title: 'Warning',
                message: 'Data list is not selected',
                yesCaption: 'OK',
                noHidden: true,
            };

            this.confirmService.confirm(options)
                .then((result) => {
                    // nothing
                });

            return Promise.reject(null);
        }

        const callHistory = new CallHistory({
            typeIdent: this.noContact.type,
            listId: this.clientList.id,
            orgRowNum: this.dataListCompany.rowNum,
            note: this.noContact.notes,
            contactRowNum,
            isNoContactHoldFromCallPool: this.noContact.isHoldFromCallPool,
            noContactStamp: this.noContact.stamp,
        });
        return this.callHistoryService.create(callHistory).toPromise()
            .then((callHistoryResponse) => {
                const serverCallHistory = callHistoryResponse.body;
                this.apply.emit(serverCallHistory);
            });
    }

    private reset() {
        this.noContact = new NoContact(NoContact.IS_VOICE_MAIL);
    }
}

export class NoContact {
    static IS_VOICE_MAIL = 'NO_CONTACT_IS_VOICE_MAIL';
    static IS_AWAY = 'NO_CONTACT_IS_AWAY';
    static IS_IN_MEETING = 'NO_CONTACT_IS_IN_MEETING';
    static IS_ENGAGED = 'NO_CONTACT_IS_ENGAGED';

    constructor(
        public type?: string,
        public notes?: string,
        public isHoldFromCallPool?: boolean,
        public stamp?: string,
    ) {
    }
}
