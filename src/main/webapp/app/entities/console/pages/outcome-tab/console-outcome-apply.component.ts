import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
    selector: 'jhi-console-outcome-apply',
    templateUrl: './console-outcome-apply.component.html'
})
export class ConsoleOutcomeApplyComponent implements OnInit {

    @Output() apply: EventEmitter<ApplyTo> = new EventEmitter();

    constructor() {
    }

    ngOnInit() {
    }

    applyToRecord() {
        this.apply.emit(ApplyTo.RECORD);
    }

    applyToOrganisation() {
        this.apply.emit(ApplyTo.ORGANISATION);
    }
}

export class ApplyTo {
    static RECORD = new ApplyTo('RECORD');
    static ORGANISATION = new ApplyTo('ORGANISATION');

    public constructor(
        public ident: string,
    ) {
    }
}
