import {Component, Input, OnInit} from '@angular/core';
import {ClientCampaign} from '../../../client-campaign';
import {
    BookingQuestionField,
    BookingQuestionFieldCategory,
    BookingQuestionFieldDataType
} from '../../../booking-question-field';
import {CampaignListContactQuestionResult} from '../../../campaign-list-contact-question-result';
import {ClientList, DataListContact} from '../../../client-list';

@Component({
    selector: 'jhi-console-outcome-booking',
    templateUrl: './console-outcome-booking.component.html'
})
export class ConsoleOutcomeBookingComponent implements OnInit {

    @Input() clientCampaign: ClientCampaign;
    @Input() clientList: ClientList;
    @Input() dataListContact: DataListContact;
    @Input() campaignListContactQuestionResults: CampaignListContactQuestionResult[];

    BookingQuestionFieldDataType: typeof BookingQuestionFieldDataType = BookingQuestionFieldDataType;
    BookingQuestionFieldCategory: typeof BookingQuestionFieldCategory = BookingQuestionFieldCategory;

    valueMap: { [id: number]: CampaignListContactQuestionResult };

    constructor() {
    }

    ngOnInit() {
        this.toValueMap();
    }

    toValueMap() {
        this.valueMap = {};
        for (const campaignListContactQuestionResult of this.campaignListContactQuestionResults) {
            // this.valueMap[campaignListContactQuestionResult.bookingQuestionFieldId] = campaignListContactQuestionResult.cvalue;
            this.valueMap[campaignListContactQuestionResult.bookingQuestionFieldId] = campaignListContactQuestionResult;
        }
    }

    valueChange(field: BookingQuestionField, value: any) {
        let campaignListContactQuestionResult = this.valueMap[field.id];
        if (!campaignListContactQuestionResult) {
            campaignListContactQuestionResult = new CampaignListContactQuestionResult({
                clientCampaignId: this.clientCampaign.id,
                clientCampaignName: this.clientCampaign.name,
                clientListId: this.clientList.id,
                clientListName: this.clientList.name,
                contactRowNum: this.dataListContact.rowNum,
                bookingQuestionFieldId: field.id,
            });
            this.campaignListContactQuestionResults.push(campaignListContactQuestionResult);
            this.valueMap[field.id] = campaignListContactQuestionResult;
        }
        campaignListContactQuestionResult.cvalue = value;
    }
}
