import {Component, ElementRef, Input, OnInit} from '@angular/core';
import {CallPoolCycle, Invite, Outcome, Ownership, YesAndNo} from '../../../outcome';
import {CampaignListContactManagement} from '../../../campaign-list-contact-management';

@Component({
    selector: 'jhi-console-outcome-contact',
    templateUrl: './console-outcome-contact.component.html'
})
export class ConsoleOutcomeContactComponent implements OnInit {

    @Input() outcome: Outcome;
    @Input() campaignListContactManagement: CampaignListContactManagement;

    YesAndNo: typeof YesAndNo = YesAndNo;

    constructor(private elementRef: ElementRef) {
    }

    ngOnInit() {
        const s = document.createElement('script');
        s.type = 'text/javascript';
        s.src = 'https://assets.calendly.com/assets/external/widget.js';
        this.elementRef.nativeElement.appendChild(s);
    }

    showHoldFromCallPool() {
        return (this.outcome.ownership === Ownership.OPTIONAL.ident ||
            this.outcome.calendarInvite === Invite.OPTIONAL.ident) &&
            // If no is selected in the drop down of the outcome edit in the Campaign Wizard
            // Then call pool option does not appear in the Contact management tab of the Outcomes wizard
            this.outcome.callPoolCycle !== CallPoolCycle.NO.ident;
    }

    showRemainder() {
        // If 'Mandatory' has been selected in the 'Ownership/Kill Record option in the Outcome
        // edit the user will not see the option to Hold from Cool Pool.
        // If 'Mandatory' has been selected in the 'Calendar' drop down in the Outcome edit,
        // the user will also not see the Option to Hold from Cool pool.
        // Instead they will see a calendar to record date and time to make a notification on the system
        return this.campaignListContactManagement.holdFromCallPool ||
            this.outcome.ownership === Ownership.MANDATORY.ident ||
            this.outcome.calendarInvite === Invite.MANDATORY.ident;
    }

    isForbidden() {
        // TODO implement this logic:
        //  see #133
        //  3d. If Forbidden has been selected in the 'Ownership/Kill Record option in the Outcome edit
        //  The record is made not accessible (greyed out) on completion of the Outcome wizard
        return this.outcome.ownership === Ownership.FORBIDDEN.ident;
    }
}
