import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {EmailTemplate, EmailTemplateDialogComponentMode, EmailTemplateService} from '../../../email-template';
import {NgForm} from '@angular/forms';
import {ConsoleEmailComponent} from '../email.component';
import {DataListContact, DataListSummary} from '../../../client-list';
import {CampaignListContactResult} from '../../../campaign-list-contact-result';

@Component({
    selector: 'jhi-console-outcome-email',
    templateUrl: './console-outcome-email.component.html'
})
export class ConsoleOutcomeEmailComponent implements OnInit {

    @Input() baseEmailTemplateId: number;
    @Input() dataListSummary: DataListSummary;
    @Input() dataListContact: DataListContact;
    @Input() campaignListContactResult: CampaignListContactResult;
    @Input() sendToQa: boolean;
    @Output() send: EventEmitter<EmailTemplate> = new EventEmitter();
    editForm: NgForm;
    emailTemplate: EmailTemplate;
    EmailTemplateDialogMode: typeof EmailTemplateDialogComponentMode = EmailTemplateDialogComponentMode;

    constructor(
        private emailTemplateService: EmailTemplateService,
    ) {
    }

    ngOnInit() {
        this.editForm = new NgForm([], []);
        this.reset();
    }

    reset() {
        if (this.baseEmailTemplateId) {
            this.emailTemplateService.find(this.baseEmailTemplateId).toPromise()
                .then((emailTemplateResponse) => {
                    this.emailTemplate = emailTemplateResponse.body;
                    this.emailTemplate.to = this.getEmailTo();
                });
        } else {
            this.emailTemplate = new EmailTemplate();
            this.emailTemplate.to = this.getEmailTo();
        }
    }

    getEmailTo() {
        if (!this.dataListContact) {
            return '';
        }
        return ConsoleEmailComponent.getEmailTo(this.dataListSummary.fields, this.dataListContact.valueMap);
    }

    doSend() {
        this.send.emit(this.emailTemplate);
    }
}
