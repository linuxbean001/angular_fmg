import {ClientCampaign, ClientCampaignService} from '../../../client-campaign';
import {Component, Input, OnInit} from '@angular/core';
import {ClientList} from '../../../client-list';

@Component({
    selector: 'jhi-console-outcome-move',
    templateUrl: './console-outcome-move.component.html'
})
export class ConsoleOutcomeMoveComponent implements OnInit {

    @Input() moveData: ConsoleOutcomeMoveData;

    clientCampaigns: ClientCampaign[];

    constructor(
        private clientCampaignService: ClientCampaignService,
    ) {
        this.loadCampaigns();
    }

    ngOnInit() {
    }

    loadCampaigns() {
        this.clientCampaignService.consoleRelatedClientCampaigns().toPromise()
            .then((res) => this.clientCampaigns = res.body);
    }

    clientCampaignChange(newClientCampaign: ClientCampaign) {
        this.moveData.selectedClientList = null;
    }
}

export class ConsoleOutcomeMoveData {
    public constructor(
        public selectedClientCampaign?: ClientCampaign,
        public selectedClientList?: ClientList,
    ) {
    }
}
