import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CallHistory, CallHistoryService} from '../../../call-history';
import {ClientList, DataListCompany, DataListContact} from '../../../client-list';
import {CallHistoryType} from '../../../call-history-type';

@Component({
    selector: 'jhi-console-outcome-qa-approval',
    templateUrl: './console-outcome-qa-approval.component.html'
})
export class ConsoleOutcomeQaApprovalComponent implements OnInit {

    @Input() clientList: ClientList;
    @Input() dataListCompany: DataListCompany;
    @Input() dataListContact: DataListContact;
    @Output() approve: EventEmitter<any> = new EventEmitter();
    @Output() decline: EventEmitter<any> = new EventEmitter();

    notes: string;
    callHistories: CallHistory[] = [];

    constructor(
        public callHistoryService: CallHistoryService,
    ) {
    }

    ngOnInit() {
        this.callHistories = this.dataListCompany.histories
            .filter((history) => history.contactRowNum === this.dataListContact.rowNum)
            .filter((history) => history.typeIdent === CallHistoryType.QA);
    }

    doApprove() {
        this.storeNoteInHistory();
        this.approve.emit();
    }

    doDecline() {
        this.storeNoteInHistory();
        this.decline.emit();
    }

    storeNoteInHistory() {
        if (!this.notes) {
            return;
        }
        const callHistory = new CallHistory({
            typeIdent: CallHistoryType.QA,
            listId: this.clientList.id,
            orgRowNum: this.dataListCompany.rowNum,
            contactRowNum: this.dataListContact.rowNum,
            note: this.notes,
        });
        this.callHistoryService.create(callHistory).toPromise()
            .then((response) => {
                this.dataListCompany.histories.push(response.body);
                // this.callHistoryComponent.fillHistories();
            });
    }
}
