import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgForm} from '@angular/forms';
import {
    ClientCampaignSms,
    ClientCampaignSmsDialogComponentMode,
    ClientCampaignSmsService
} from '../../../client-campaign-sms';
import {CampaignListContactResult} from '../../../campaign-list-contact-result';

@Component({
    selector: 'jhi-console-outcome-sms',
    templateUrl: './console-outcome-sms.component.html'
})
export class ConsoleOutcomeSmsComponent implements OnInit {

    @Input() baseSmsId: number;
    @Input() campaignListContactResult: CampaignListContactResult;
    @Input() sendToQa: boolean;
    @Output() send: EventEmitter<any> = new EventEmitter();
    editForm: NgForm;
    sms: ClientCampaignSms;
    ClientCampaignSmsDialogComponentMode: typeof ClientCampaignSmsDialogComponentMode = ClientCampaignSmsDialogComponentMode;

    constructor(
        private clientCampaignSmsService: ClientCampaignSmsService,
    ) {
    }

    ngOnInit() {
        this.editForm = new NgForm([], []);
        this.reset();
    }

    reset() {
        if (this.baseSmsId) {
            this.clientCampaignSmsService.find(this.baseSmsId).toPromise()
                .then((smsResponse) => {
                    this.sms = smsResponse.body;
                });
        } else {
            this.sms = new ClientCampaignSms();
        }
    }

    doSend() {
        this.send.emit(null);
    }
}
