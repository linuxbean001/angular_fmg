import {Component, Input, OnInit} from '@angular/core';
import {OutcomeSubReason, OutcomeSubReasonService} from '../../../outcome-sub-reason';
import {CampaignListContactResult} from '../../../campaign-list-contact-result';

@Component({
    selector: 'jhi-console-outcome-sub-reason',
    templateUrl: './console-outcome-sub-reason.component.html'
})
export class ConsoleOutcomeSubReasonComponent implements OnInit {

    @Input() campaignListResult: CampaignListContactResult;

    outcomeSubReasons: OutcomeSubReason[];

    constructor(
        private outcomeSubReasonService: OutcomeSubReasonService,
    ) {
    }

    ngOnInit() {
        this.outcomeSubReasonService.query().toPromise()
            .then((outcomeSubReasonsResponse) => this.outcomeSubReasons = outcomeSubReasonsResponse.body);
    }
}
