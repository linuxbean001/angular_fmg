import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ClientCampaign, TabLeavedData} from '../../../client-campaign';
import {ClientList, DataListCompany, DataListContact, DataListSummary} from '../../../client-list';
import {TabChangedEvent, TabClickEvent, Tabs} from '../../../../layouts/tabs/tabs.component';
import {Outcome, Ownership, QcQa, YesAndNo} from '../../../outcome';
import {
    CampaignListContactResult,
    CampaignListContactResultService,
    RecordState
} from '../../../campaign-list-contact-result';
import {
    CampaignListContactQuestionResult,
    CampaignListContactQuestionResultService
} from '../../../campaign-list-contact-question-result';
import {
    CampaignListContactManagement,
    CampaignListContactManagementService
} from '../../../campaign-list-contact-management';
import {ConsoleOutcomeMoveData} from './console-outcome-move.component';
import {MoveContactEvent} from '../outcome.component';
import {ConfirmOptions, ConfirmService} from '../../../../shared';
import {CallHistoryService} from '../../../call-history';
import {ConsoleOutcomeSmsComponent} from './console-outcome-sms.component';
import {ApplyTo} from './console-outcome-apply.component';
import {EmailTemplate} from '../../../email-template';

@Component({
    selector: 'jhi-outcome-wizard',
    templateUrl: './outcome-wizard.component.html'
})
export class OutcomeWizardComponent implements OnInit {

    @Input() clientCampaign: ClientCampaign;
    @Input() clientList: ClientList;
    @Input() outcome: Outcome;
    @Input() dataListSummary: DataListSummary;
    @Input() dataListCompany: DataListCompany;
    @Input() withQaApprovalTab: boolean;
    @Output() finish: EventEmitter<OutcomeWizardResult> = new EventEmitter();
    @Output() cancel: EventEmitter<any> = new EventEmitter();
    @Output() moveContact: EventEmitter<MoveContactEvent> = new EventEmitter();

    loadingIsComplete: boolean;
    campaignListContactResult: CampaignListContactResult;
    campaignListContactQuestionResults: CampaignListContactQuestionResult[];
    campaignListContactManagement: CampaignListContactManagement;
    moveData: ConsoleOutcomeMoveData;
    SUB_REASON = 'SUB_REASON';
    BOOKING_QUESTION = 'BOOKING_QUESTION';
    CONTACT_MANAGEMENT = 'CONTACT_MANAGEMENT';
    EMAIL = 'EMAIL';
    SMS = 'SMS';
    MOVE = 'MOVE';
    QA_APPROVAL = 'QA_APPROVAL';
    APPLY = 'APPLY';
    _dataListContact: DataListContact;

    @ViewChild(Tabs) tabs: Tabs;

    // @ViewChild(ConsoleOutcomeSubReasonComponent)
    // private consoleOutcomeSubReasonComponent: ConsoleOutcomeSubReasonComponent;
    // @ViewChild(ConsoleOutcomeBookingComponent)
    // private consoleOutcomeBookingComponent: ConsoleOutcomeBookingComponent;
    @ViewChild(ConsoleOutcomeSmsComponent)
    private consoleOutcomeSmsComponent: ConsoleOutcomeSmsComponent;

    constructor(
        private confirmService: ConfirmService,
        private campaignListContactResultService: CampaignListContactResultService,
        private campaignListContactQuestionResultService: CampaignListContactQuestionResultService,
        private campaignListContactManagementService: CampaignListContactManagementService,
        private callHistoryService: CallHistoryService,
    ) {
    }

    @Input()
    set dataListContact(value: DataListContact) {
        this._dataListContact = value;

        this.loadingIsComplete = false;
        Promise.all([this.loadSubReasonResult(), this.loadBookingQuestionResult(), this.loadContactManagementResult()])
            .then((results) => {
                this.campaignListContactResult = results[0];
                this.campaignListContactQuestionResults = results[1];
                this.campaignListContactManagement = results[2];
                this.moveData = new ConsoleOutcomeMoveData();
                this.loadingIsComplete = true;

                // TODO is this is required?
                // give a timeout
                setTimeout(() => {
                    this.tabs.selectTab(this.tabs.tabs.first);
                });
            });
    }

    ngOnInit(): void {
    }

    loadSubReasonResult(): Promise<CampaignListContactResult> {
        if (!this._dataListContact) {
            return Promise.resolve(null);
        }
        return this.campaignListContactResultService.queryByCampaignAndListAndRowNum(this.clientCampaign.id, this.clientList.id, this._dataListContact.rowNum).toPromise()
            .then((campaignListContactResultResponse) => {
                const items = campaignListContactResultResponse.body;
                // get the first item
                if (items.length) {
                    return items.length && items[0];
                } else {
                    return new CampaignListContactResult(null,
                        this.dataListCompany.rowNum,
                        this._dataListContact.rowNum,
                        this.clientCampaign.name,
                        this.clientCampaign.id,
                        this.clientList.name,
                        this.clientList.id
                    );
                }
            });
    }

    loadBookingQuestionResult(): Promise<CampaignListContactQuestionResult[]> {
        if (!this._dataListContact) {
            return Promise.resolve(null);
        }
        return this.campaignListContactQuestionResultService.queryByCampaignAndListAndRowNum(this.clientCampaign.id, this.clientList.id, this._dataListContact.rowNum).toPromise()
            .then((campaignListContactQuestionResultsResponse) => campaignListContactQuestionResultsResponse.body);
    }

    loadContactManagementResult(): Promise<CampaignListContactManagement> {
        if (!this._dataListContact) {
            return Promise.resolve(null);
        }
        return this.campaignListContactManagementService.queryByCampaignAndListAndRowNum(this.clientCampaign.id, this.clientList.id, this._dataListContact.rowNum).toPromise()
            .then((campaignListContactManagementsResponse) => {
                const items = campaignListContactManagementsResponse.body;
                // get the first item
                if (items.length) {
                    return items.length && items[0];
                } else {
                    return new CampaignListContactManagement(null,
                        this._dataListContact.rowNum,
                        this.clientCampaign.name,
                        this.clientCampaign.id,
                        this.clientList.name,
                        this.clientList.id
                    );
                }
            });
    }

    showInformation(message: string): Promise<any> {
        const options: ConfirmOptions = {
            title: 'Information',
            message,
            yesCaption: 'OK',
            noHidden: true,
        };

        return this.confirmService.confirm(options)
            .then((result) => {
                // nothing
            });
    }

    showWarning(message: string) {
        const options: ConfirmOptions = {
            title: 'Warning',
            message,
            yesCaption: 'OK',
            noHidden: true,
        };

        this.confirmService.confirm(options)
            .then((result) => {
                // nothing
            });
    }

    onTabClick(event: TabClickEvent) {
        if (!event.from) {
            this.tabs.selectTab(event.to);
        } else {
            this.tabs.selectTab(event.to, new TabLeavedData(false));
        }
    }

    onTabChanged(event: TabChangedEvent) {
        // skip first render
        if (!event.from) {
            return;
        }

        const data = event.data as TabLeavedData;
        if (data && data.hasValidationIssue) {
            // just leave, save not required
            return;
        }

        switch (event.from.ident) {
            case this.SUB_REASON:
                this.saveResult();
                break;
            case this.BOOKING_QUESTION:
                this.campaignListContactQuestionResultService.updateAll(this.campaignListContactQuestionResults).toPromise()
                    .then((campaignListContactQuestionResultsResponse) => {
                        this.campaignListContactQuestionResults = campaignListContactQuestionResultsResponse.body;
                    });
                break;
            case this.CONTACT_MANAGEMENT:
                this.campaignListContactManagementService.update(this.campaignListContactManagement).toPromise()
                    .then((campaignListContactManagementResponse) => {
                        this.campaignListContactManagement = campaignListContactManagementResponse.body;
                    });
                break;
            case this.EMAIL:
                break;
            case this.SMS:
                break;
            case this.MOVE:
                // see this.finishClick()
                break;
            default:
                // nothing
                break;
        }
    }

    showSubReasonTab(): boolean {
        return this.campaignListContactResult && Outcome.showSubReasonTab(this.outcome);
    }

    showBookingFromTab(): boolean {
        return this.campaignListContactQuestionResults && Outcome.showBookingFromTab(this.outcome);
    }

    showContactManagementTab(): boolean {
        return this.campaignListContactManagement && Outcome.showContactManagementTab(this.outcome);
    }

    showSendEmailTab(): boolean {
        return this.campaignListContactResult && this.loadingIsComplete && Outcome.showSendEmailTab(this.outcome);
    }

    showSendSMSTab(): boolean {
        return this.campaignListContactResult && this.loadingIsComplete && Outcome.showSendSMSTab(this.outcome);
    }

    showMoveTab(): boolean {
        return this.loadingIsComplete && Outcome.showMoveTab(this.outcome);
    }

    showQaApprovalTab(): boolean {
        return this.loadingIsComplete && this.withQaApprovalTab;
    }

    subReasonTabIsCompleted(): boolean {
        return this.campaignListContactResult && !!this.campaignListContactResult.outcomeSubReasonId;
    }

    bookingFromTabIsCompleted(): boolean {
        return this.campaignListContactQuestionResults && this.campaignListContactQuestionResults.length > 0;
    }

    contactManagementTabIsCompleted(): boolean {
        return this.campaignListContactManagement && !!this.campaignListContactManagement.id;
    }

    sendEmailTabIsCompleted(): boolean {
        return !!this.campaignListContactResult.emailTemplateJson;
    }

    sendSMSTabIsCompleted(): boolean {
        return this.campaignListContactResult.smsIsSent || this.campaignListContactResult.sendSMSWasClicked;
    }

    moveTabIsCompleted(): boolean {
        return false;
    }

    getCompletePercent(): number {
        let total = 0;
        let completed = 0;
        if (this.showSubReasonTab()) {
            total++;
            if (this.subReasonTabIsCompleted()) {
                completed++;
            }
        }
        if (this.showBookingFromTab()) {
            total++;
            if (this.bookingFromTabIsCompleted()) {
                completed++;
            }
        }
        if (this.showContactManagementTab()) {
            total++;
            if (this.contactManagementTabIsCompleted()) {
                completed++;
            }
        }
        if (this.showSendEmailTab()) {
            total++;
            if (this.sendEmailTabIsCompleted()) {
                completed++;
            }
        }
        if (this.showSendSMSTab()) {
            total++;
            if (this.sendSMSTabIsCompleted()) {
                completed++;
            }
        }

        if (completed === 0) {
            return 0;
        }
        return Math.round(100 * completed / total);
    }

    saveResult() {
        return this.campaignListContactResultService.update(this.campaignListContactResult).toPromise()
            .then((campaignListContactResultResponse) => {
                this.campaignListContactResult = campaignListContactResultResponse.body;
            });
    }

    // addEmailAuditRecord(emailTemplateId: number) {
    //     const callHistory = new CallHistory({
    //         typeIdent: CallHistoryType.EMAIL,
    //         listId: this.clientList.id,
    //         orgRowNum: this.dataListCompany.rowNum,
    //         contactRowNum: this.dataListContact && this.dataListContact.rowNum,
    //         emailTemplateId
    //     });
    //     this.callHistoryService.create(callHistory).toPromise()
    //         .then((response) => {
    //             this.dataListCompany.histories.push(response.body);
    //         });
    // }
    //
    // addSMSAuditRecord(text: string, smsId?: number) {
    //     const callHistory = new CallHistory({
    //         typeIdent: CallHistoryType.SMS,
    //         listId: this.clientList.id,
    //         orgRowNum: this.dataListCompany.rowNum,
    //         contactRowNum: this.dataListContact && this.dataListContact.rowNum,
    //         smsText: text,
    //         smsId
    //     });
    //     this.callHistoryService.create(callHistory).toPromise()
    //         .then((response) => {
    //             this.dataListCompany.histories.push(response.body);
    //         });
    // }

    emailSendHandler(emailTemplate: EmailTemplate) {
        if (!!this.campaignListContactResult.emailTemplateJson) {
            this.showInformation('An Email has already been sent to this contact check call history for further details');
            return;
        }

        this.showInformation('Email will be sent when Outcome is completed')
            .then((value) => {
                this.campaignListContactResult.emailTemplateJson = JSON.stringify(emailTemplate);
                this.tabs.nextTab();
            });
    }

    smsSendHandler() {
        if (this.campaignListContactResult.smsIsSent) {
            this.showInformation('An SMS has already been sent to this contact check call history for further details');
            return;
        }

        this.showInformation('SMS will be sent when Outcome is completed')
            .then((value) => {
                this.campaignListContactResult.sendSMSWasClicked = true;
                this.tabs.nextTab();
            });
    }

    isSendToQA() {
        return this.outcome.qcqa === QcQa.URGENT.ident ||
            this.outcome.qcqa === QcQa.EYE_CHECK.ident;
    }

    applyHandler(applyTo: ApplyTo) {
        const noteExists = !!this._dataListContact.note ||
            // trying to find history note
            this.dataListCompany.histories.some((history) => history.contactRowNum === this._dataListContact.rowNum && !!history.note);
        if (!noteExists) {
            this.showWarning('Outcome cannot be saved unless Notes are completed');
            return;
        }

        let newState: RecordState;
        // see https://gitlab.com/mzdw4w/fmg/issues/131, I'm not sure that this is correct
        if (this.outcome.qcqa) {
            newState = RecordState.PENDING;
        } else {
            // see https://gitlab.com/mzdw4w/fmg/issues/135#note_229708124
            switch (this.outcome.ownership) {
                case Ownership.FORBIDDEN.ident:
                    newState = RecordState.COMPLETED;
                    break;
                case Ownership.MANDATORY.ident:
                    newState = RecordState.MY_LIST;
                    break;
                case Ownership.OPTIONAL.ident:
                    if (this.campaignListContactManagement.holdFromCallPool) {
                        newState = RecordState.MY_LIST;
                    } else {
                        newState = RecordState.ACTIVE;
                    }
                    break;
                default:
                    // what should I do?
                    break;
            }
        }

        if (applyTo === ApplyTo.RECORD) {
            // this.campaignListContactResult.state = state;
        } else if (applyTo === ApplyTo.ORGANISATION) {
        }

        this.finishClick(new OutcomeWizardResult(newState, applyTo, this.outcome));
    }

    finishClick(wizardResult: OutcomeWizardResult) {
        if (this.moveData.selectedClientCampaign && this.moveData.selectedClientList) {
            if (this.outcome.moveToAnotherCampaign === YesAndNo.YES.ident) {
                // const moveContactEvent = new MoveContactEvent(this.dataListContact, this.outcome.moveToCampaignId, this.outcome.clientListId);
                const moveContactEvent = new MoveContactEvent(this.dataListContact, this.moveData.selectedClientCampaign.id, this.moveData.selectedClientList.id);
                this.moveContact.emit(moveContactEvent);
                // notify user
                const message = 'As specified in outcome "' + this.outcome.name + '". ' +
                    'Contact "' + this.dataListContact.contactType + '" has been ' +
                    'moved to campaign "' + this.outcome.moveToCampaignName + '" ' +
                    'list "' + this.outcome.clientListName + '"';
                this.showInformation(message);
            }
        }

        // const promises = [];
        // if (this.campaignListContactResult.emailState === ResultEmailState.SEND.ident) {
        //     promises.push(this.addEmailAuditRecord(this.outcome.emailTemplateId));
        //     promises.push(this.saveResult());
        // }
        //
        // if (this.campaignListContactResult.sendSMSWasClicked) {
        //     this.campaignListContactResult.smsIsSent = true;
        //     promises.push(this.addSMSAuditRecord(this.consoleOutcomeSmsComponent.sms.excerpt, this.outcome.smsId));
        //     promises.push(this.saveResult());
        // }
        //
        // Promise.all(promises)
        //     .then((values) => {
        //         this.finish.emit(wizardResult);
        //     });

        this.campaignListContactResult.isApproved = !(this.outcome.qcqa === QcQa.URGENT.ident || this.outcome.qcqa === QcQa.EYE_CHECK.ident);
        this.campaignListContactResult.clickedOutcomeId = wizardResult.clickedOutcome.id;
        this.campaignListContactResult.clickedOutcomeName = wizardResult.clickedOutcome.name;
        this.saveResult()
            .then((values) => {
                this.finish.emit(wizardResult);
            });
    }

    cancelClick() {
        this.cancel.emit('not confirmed');
    }

    approveHandler() {
        this.campaignListContactResult.isApproved = true;
        this.saveResult()
            .then((values) => {
                // If they Accept - the email/SMS is sent to the Record, the booking form is sent
                // to the Client (or an alert is sent) and the status of the outcome changes to lead / sale
                const result = new OutcomeWizardResult(RecordState.COMPLETED, ApplyTo.RECORD, this.outcome);
                this.finish.emit(result);
            });
    }

    declineHandler() {
        this.campaignListContactResult.isApproved = false;
        this.saveResult()
            .then((values) => {
                const result = new OutcomeWizardResult(RecordState.PENDING, ApplyTo.RECORD, this.outcome);
                this.finish.emit(result);
            });
    }
}

export class OutcomeWizardResult {
    public constructor(
        public newState: RecordState,
        public applyTo: ApplyTo,
        public clickedOutcome: Outcome,
    ) {
    }
}
