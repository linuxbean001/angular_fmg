import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ClientCampaign} from '../../client-campaign';
import {ClientList, DataListCompany, DataListContact, DataListSummary} from '../../client-list';
import {Outcome, OutcomeCategory, QcQa, YesAndNo} from '../../outcome';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ConfirmOptions, ConfirmService} from '../../../shared';
import {OutcomeWizardResult} from './outcome-tab/outcome-wizard.component';

@Component({
    selector: 'jhi-console-outcome',
    templateUrl: './outcome.component.html'
})
export class ConsoleOutcomeComponent implements OnInit {

    @Input() clientCampaign: ClientCampaign;
    @Input() clientList: ClientList;
    @Input() dataListCompany: DataListCompany;
    @Input() dataListSummary: DataListSummary;
    @Input() buttonsAreDisabled = false;
    _dataListContact: DataListContact;
    @Output() moveContact: EventEmitter<MoveContactEvent> = new EventEmitter();
    @Output() finish: EventEmitter<OutcomeWizardResult> = new EventEmitter();

    withQaApprovalTab = false;

    @Input()
    set dataListContact(contact: DataListContact) {
        this._dataListContact = contact;
        if (contact && contact.clickedOutcomeId) {
            this.selectedOutcome = this.clientCampaign.outcomes.find((outcome) => outcome.id === contact.clickedOutcomeId);
            // this is correct logic?
            this.withQaApprovalTab = this.selectedOutcome.qcqa === QcQa.URGENT.ident || this.selectedOutcome.qcqa === QcQa.EYE_CHECK.ident;
        } else {
            this.selectedOutcome = null;
            this.withQaApprovalTab = false;
        }
    }

    @Input()
    panelsNgStyle: { [key: string]: string; };

    selectedOutcome: Outcome;

    constructor(
        private modalService: NgbModal,
        private confirmService: ConfirmService,
    ) {
    }

    ngOnInit() {
    }

    outcomeClick(outcome: Outcome) {
        this.selectedOutcome = outcome;
        // const anyTabRequired = Outcome.showBookingFromTab(outcome) || Outcome.showSendEmailTab(outcome) || Outcome.showSendSMSTab(outcome);
        // // move a contact, should I mix this and Email/Sms/Booking?
        // if (!anyTabRequired) {
        //     this.moveToAnotherCampaignIfRequired(outcome);
        // } else {
        //     // if this is correct for else clause?
        //     const modalRef = this.modalService.open(OutcomeWizardComponent, {size: 'lg', backdrop: 'static'});
        //     modalRef.componentInstance.clientCampaign = this.clientCampaign;
        //     modalRef.componentInstance.clientList = this.clientList;
        //     modalRef.componentInstance.dataListContact = this.dataListContact;
        //     modalRef.componentInstance.outcome = outcome;
        //     modalRef.result.then((result: { phone, text: string }) => {
        //         this.moveToAnotherCampaignIfRequired(outcome);
        //     }, (reason) => {
        //     });
        // }
    }

    moveToAnotherCampaignIfRequired(outcome: Outcome) {
        if (outcome.moveToAnotherCampaign === YesAndNo.YES.ident) {
            this.moveContact.emit(new MoveContactEvent(this.dataListContact, outcome.moveToCampaignId, outcome.clientListId));
            // is this is working? I didn't see a message
            const message = 'As specified in outcome "' + outcome.name + '". ' +
                'Contact "' + this.dataListContact.contactType + '" has been ' +
                'moved to campaign "' + outcome.moveToCampaignName + '" ' +
                'list "' + outcome.clientListName + '"';
            this.showInformation(message);
        }
    }

    showInformation(message: string) {
        const options: ConfirmOptions = {
            title: 'Information',
            message,
            yesCaption: 'OK',
            noHidden: true,
        };

        this.confirmService.confirm(options)
            .then((result) => {
                // nothing
            });
    }

    doMoveContact(moveContactEvent: MoveContactEvent) {
        this.moveContact.emit(moveContactEvent);
    }

    cancelHandler() {
        this.selectedOutcome = null;
    }

    finishHandler(result: OutcomeWizardResult) {
        this.selectedOutcome = null;
        this.finish.emit(result);
    }

    outcomeColorClass(outcome: Outcome) {
        switch (outcome.categoryIdent) {
            case OutcomeCategory.POSITIVE.ident:
                return 'btn-outline-success';
            case OutcomeCategory.NEGATIVE.ident:
                return 'btn-outline-danger';
            case OutcomeCategory.PIPELINE.ident:
            case OutcomeCategory.PIPELINE_GT_50.ident:
                return 'btn-outline-warning';
            default:
                return 'btn-outline-primary';
        }
    }
}

export class MoveContactEvent {
    public constructor(
        public contact: DataListContact,
        public destinationClientCampaignId: number,
        public destinationClientListId: number,
    ) {
    }
}
