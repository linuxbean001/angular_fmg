import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ClientCampaign} from '../../client-campaign';
import {ConfirmOptions, ConfirmService} from '../../../shared';
import {ClientCampaignScript} from '../../client-campaign-script';

@Component({
    selector: 'jhi-console-script',
    templateUrl: './script.component.html'
})
export class ConsoleScriptComponent implements OnInit, OnDestroy {

    @Input() clientCampaign: ClientCampaign;
    @Input()
    panelsNgStyle: { [key: string]: string; };
    @Input() readonly = false;

    states: boolean[] = [true];
    changes: string[] = [];
    allExpanded = false;

    constructor(
        private confirmService: ConfirmService,
    ) {
    }

    ngOnInit() {
    }

    ngOnDestroy() {
    }

    toggleState(i: number) {
        this.states[i] = !this.states[i];
        for (let j = 0; j < this.states.length; j++) {
            if (i === j) {
                continue;
            }
            this.states[j] = false;
        }
    }

    toggleAll() {
        this.allExpanded = !this.allExpanded;
        for (let i = 0; i < this.clientCampaign.scripts.length; i++) {
            this.states[i] = this.allExpanded;
        }
    }

    submitChange(script: ClientCampaignScript, i: number) {
        this.changes[i] = null;

        // just so they can get the feel of it
        const options: ConfirmOptions = {
            title: 'Information',
            message: 'Submitted',
            yesCaption: 'OK',
            noHidden: true,
        };

        this.confirmService.confirm(options)
            .then((result) => {
                // nothing
            });
    }
}
