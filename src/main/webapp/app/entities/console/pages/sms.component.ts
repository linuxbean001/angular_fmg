import {Component, Input} from '@angular/core';
import {ClientCampaign} from '../../client-campaign';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {NewSmsComponent} from './new-sms.component';
import {ClientCampaignSms} from '../../client-campaign-sms';
import {ClientList, DataListCompany, DataListContact, DataListSummary} from '../../client-list';
import {CallHistory, CallHistoryService} from '../../call-history';
import {CallHistoryType} from '../../call-history-type';
import {ClientField} from '../../client-field';

@Component({
    selector: 'jhi-console-sms',
    templateUrl: './sms.component.html'
})
export class ConsoleSmsComponent {

    @Input() clientCampaign: ClientCampaign;
    @Input() clientList: ClientList;
    @Input() dataListSummary: DataListSummary;
    @Input() dataListCompany: DataListCompany;
    @Input() dataListContact: DataListContact;
    @Input()
    panelsNgStyle: { [key: string]: string; };
    @Input() readonly = false;

    private mobileField = 'Mobile';

    constructor(
        private modalService: NgbModal,
        private callHistoryService: CallHistoryService,
    ) {
    }

    private getPhone() {
        const clientField: ClientField = this.dataListSummary.fields.find((field) => field.fieldName.toLowerCase() === this.mobileField.toLowerCase());
        if (!clientField) {
            return `Could not find the field "${this.mobileField}"`;
        }
        return this.dataListContact.valueMap[clientField.id];
    }

    newSms() {
        this.sendSms(this.getPhone());
    }

    selectFromTemplate(sms: ClientCampaignSms) {
        this.sendSms(this.getPhone(), sms.excerpt, sms.id);
    }

    sendSms(phone?: string, text?: string, smsId?: number) {
        const modalRef = this.modalService.open(NewSmsComponent);
        modalRef.componentInstance.phone = phone;
        modalRef.componentInstance.text = text;
        modalRef.result.then((result: { phone, text: string }) => {
            // Left blank intentionally, nothing to do here
            this.addSMSAuditRecord(text, smsId);
        }, (reason) => {
            // Left blank intentionally, nothing to do here
        });
    }

    addSMSAuditRecord(text: string, smsId?: number) {
        const callHistory = new CallHistory({
            typeIdent: CallHistoryType.SMS,
            listId: this.clientList.id,
            orgRowNum: this.dataListCompany.rowNum,
            contactRowNum: this.dataListContact && this.dataListContact.rowNum,
            smsText: text,
            smsId
        });
        this.callHistoryService.create(callHistory).toPromise()
            .then((response) => {
                this.dataListCompany.histories.push(response.body);
            });
    }
}
