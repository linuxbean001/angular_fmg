import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';

import {EmailTemplate, EmailTemplateDialogComponentMode} from './email-template.model';
import {EmailTemplatePopupService} from './email-template-popup.service';
import {EmailTemplateService} from './email-template.service';

import {ClientCampaign, ClientCampaignService} from '../client-campaign';

@Component({
    selector: 'jhi-email-template-dialog',
    templateUrl: './email-template-dialog.component.html'
})
export class EmailTemplateDialogComponent implements OnInit {

    emailTemplate: EmailTemplate;
    sendToServer = true;
    isSaving: boolean;
    options: Object;

    mode = EmailTemplateDialogComponentMode.CREATE_OR_EDIT;
    EmailTemplateDialogMode = EmailTemplateDialogComponentMode;

    clientCampaigns: ClientCampaign[];

    constructor(
        public activeModal: NgbActiveModal,
        private emailTemplateService: EmailTemplateService,
        private eventManager: JhiEventManager,
        private jhiAlertService: JhiAlertService,
        private clientCampaignService: ClientCampaignService,
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.options = {
            toolbarButtons: ['undo', 'redo', '|', 'bold', 'italic', 'underline', 'strikeThrough',
                'outdent', 'indent', 'align', 'clearFormatting', 'insertTable', 'html'],
            height: 300,
            quickInsertButtons: ['table', 'ul', 'ol', 'hr'],
        };
        this.clientCampaignService.query()
            .subscribe((res: HttpResponse<ClientCampaign[]>) => {
                this.clientCampaigns = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.sendToServer) {
            if (this.emailTemplate.id !== undefined) {
                this.subscribeToSaveResponse(
                    this.emailTemplateService.update(this.emailTemplate));
            } else {
                this.subscribeToSaveResponse(
                    this.emailTemplateService.create(this.emailTemplate));
            }
        } else {
            this.onSaveSuccess(this.emailTemplate);
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<EmailTemplate>>) {
        result.subscribe((res: HttpResponse<EmailTemplate>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: EmailTemplate) {
        this.eventManager.broadcast({
            name: 'emailTemplateListModification',
            content: 'OK',
            emailTemplate: result
        });
        this.isSaving = false;
        this.activeModal.close(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    /*getBody() {
        return this.emailTemplate.body || '';
    }

    setBody($event) {
        this.emailTemplate.body = $event;
    }*/

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackClientCampaignById(index: number, item: ClientCampaign) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-email-template-popup',
    template: ''
})
export class EmailTemplatePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private emailTemplatePopupService: EmailTemplatePopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.emailTemplatePopupService
                    .open(EmailTemplateDialogComponent as Component, params['id']);
            } else {
                this.emailTemplatePopupService
                    .open(EmailTemplateDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
