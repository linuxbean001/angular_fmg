import {Component, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {HttpResponse} from '@angular/common/http';
import {EmailTemplate, EmailTemplateDialogComponentMode} from './email-template.model';
import {EmailTemplateService} from './email-template.service';

@Injectable()
export class EmailTemplatePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private emailTemplateService: EmailTemplateService
    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.emailTemplateService.find(id)
                    .subscribe((emailTemplateResponse: HttpResponse<EmailTemplate>) => {
                        const emailTemplate: EmailTemplate = emailTemplateResponse.body;
                        this.ngbModalRef = this.emailTemplateModalRef(component, emailTemplate);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.emailTemplateModalRef(component, new EmailTemplate());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    openStandalone(component: Component, emailTemplate: EmailTemplate,
                   sendToServer = true,
                   mode = EmailTemplateDialogComponentMode.CREATE_OR_EDIT): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            this.ngbModalRef = this.emailTemplateModalRef(component, emailTemplate, sendToServer, mode);
            resolve(this.ngbModalRef);
        });
    }

    emailTemplateModalRef(component: Component, emailTemplate: EmailTemplate, sendToServer = true,
                          mode = EmailTemplateDialogComponentMode.CREATE_OR_EDIT): NgbModalRef {
        const xl = 'xl' as 'lg';
        const modalRef = this.modalService.open(component, {size: xl, backdrop: 'static'});
        modalRef.componentInstance.emailTemplate = emailTemplate;
        modalRef.componentInstance.sendToServer = sendToServer;
        modalRef.componentInstance.mode = mode;
        modalRef.result.then((result) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
