import {Component, Input, OnInit} from '@angular/core';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';

import {EmailTemplate, EmailTemplateDialogComponentMode} from './email-template.model';
import {EmailTemplateService} from './email-template.service';

import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {ClientCampaign, ClientCampaignService} from '../client-campaign';
import {NgForm} from '@angular/forms';
import {CKEditor5} from '@ckeditor/ckeditor5-angular/ckeditor';
import MyCustomUploadAdapterPlugin from './MyCustomUploadAdapterPlugin';

@Component({
    selector: 'jhi-email-template-properties',
    templateUrl: './email-template-properties.component.html'
})
export class EmailTemplatePropertiesComponent implements OnInit {

    public Editor = ClassicEditor;
    public editorConfig: CKEditor5.Config = {
        extraPlugins: [MyCustomUploadAdapterPlugin]
    };
    @Input() editForm: NgForm;
    @Input() emailTemplate: EmailTemplate;
    @Input() mode = EmailTemplateDialogComponentMode.CREATE_OR_EDIT;
    EmailTemplateDialogMode: typeof EmailTemplateDialogComponentMode = EmailTemplateDialogComponentMode;

    clientCampaigns: ClientCampaign[];

    constructor(
        private emailTemplateService: EmailTemplateService,
        private eventManager: JhiEventManager,
        private jhiAlertService: JhiAlertService,
        private clientCampaignService: ClientCampaignService,
    ) {
    }

    ngOnInit() {
        this.clientCampaignService.query()
            .subscribe((res: HttpResponse<ClientCampaign[]>) => {
                this.clientCampaigns = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    getBody() {
        return this.emailTemplate.body || '';
    }

    setBody($event) {
        this.emailTemplate.body = $event;
    }

    trackClientCampaignById(index: number, item: ClientCampaign) {
        return item.id;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
