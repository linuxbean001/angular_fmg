import {BaseEntity} from '../../shared';

export class EmailTemplate implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public from?: string,
        public subject?: string,
        public cc?: string,
        public bcc?: string,
        public replyTo?: string,
        public body?: string,
        public clientCampaignName?: string,
        public clientCampaignId?: number,
        /**
         * It's using in conjunction with {@link EmailTemplateDialogComponentMode#SEND}.
         */
        public to?: string,
    ) {
    }
}

export class EmailTemplateDialogComponentMode {
    /**
     * This is default mode.
     */
    static CREATE_OR_EDIT = new EmailTemplateDialogComponentMode();

    /**
     * This mode is used in Console to display the {@link EmailTemplate#to} property.
     */
    static SEND = new EmailTemplateDialogComponentMode();
}
