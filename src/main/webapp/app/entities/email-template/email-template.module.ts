import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {CmaSharedModule} from '../../shared';
import {
    EmailTemplateComponent,
    EmailTemplateDeleteDialogComponent,
    EmailTemplateDeletePopupComponent,
    EmailTemplateDetailComponent,
    EmailTemplateDialogComponent,
    EmailTemplatePopupComponent,
    emailTemplatePopupRoute,
    EmailTemplatePopupService,
    EmailTemplateResolvePagingParams,
    emailTemplateRoute,
    EmailTemplateService,
} from './';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import {EmailTemplatePropertiesComponent} from './email-template-properties.component';

const ENTITY_STATES = [
    ...emailTemplateRoute,
    ...emailTemplatePopupRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        CKEditorModule
    ],
    declarations: [
        EmailTemplateComponent,
        EmailTemplateDetailComponent,
        EmailTemplateDialogComponent,
        EmailTemplateDeleteDialogComponent,
        EmailTemplatePopupComponent,
        EmailTemplateDeletePopupComponent,
        EmailTemplatePropertiesComponent,
    ],
    entryComponents: [
        EmailTemplateComponent,
        EmailTemplateDialogComponent,
        EmailTemplatePopupComponent,
        EmailTemplateDeleteDialogComponent,
        EmailTemplateDeletePopupComponent,
        EmailTemplatePropertiesComponent,
    ],
    providers: [
        EmailTemplateService,
        EmailTemplatePopupService,
        EmailTemplateResolvePagingParams,
    ],
    exports: [
        EmailTemplateDetailComponent,
        EmailTemplatePropertiesComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaEmailTemplateModule {
}
