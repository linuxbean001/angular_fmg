import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import {CmaUserCompanyModule} from './user-company/user-company.module';
import {CmaClientCompanyStatusModule} from './client-company-status/client-company-status.module';
import {CmaClientCompanyModule} from './client-company/client-company.module';

import {CmaClientCampaignTemplateModule} from './client-campaign-template/client-campaign-template.module';
import {CmaClientCampaignModule} from './client-campaign/client-campaign.module';
import {CmaClientListModule} from './client-list/client-list.module';
import {CmaFieldModule} from './field/field.module';
import {CmaClientFieldModule} from './client-field/client-field.module';
import {CmaClientListDataModule} from './client-list-data/client-list-data.module';
import {CmaOutcomeModule} from './outcome/outcome.module';
import {CmaCampaignPortalUserModule} from './campaign-portal-user/campaign-portal-user.module';
import {CmaCampaignPortalUserOutcomeModule} from './campaign-portal-user-outcome/campaign-portal-user-outcome.module';
import {CmaEmailTemplateModule} from './email-template/email-template.module';
import {CmaOutcomeTypeModule} from './outcome-type/outcome-type.module';
import {CmaClientCampaignScriptModule} from './client-campaign-script/client-campaign-script.module';
import {CmaClientCampaignSmsModule} from './client-campaign-sms/client-campaign-sms.module';
import {CmaAppointmentAttendeeModule} from './appointment-attendee/appointment-attendee.module';
import {CmaSurveyQuestionModule} from './survey-question/survey-question.module';
import {CmaSurveyQuestionFieldModule} from './survey-question-field/survey-question-field.module';
import {CmaBookingQuestionModule} from './booking-question/booking-question.module';
import {CmaBookingQuestionFieldModule} from './booking-question-field/booking-question-field.module';
import {CmaClientCampaignAddDocModule} from './client-campaign-add-doc/client-campaign-add-doc.module';
import {CmaConsoleModule} from './console/console.module';
import {CmaCallHistoryTypeModule} from './call-history-type/call-history-type.module';
import {CmaCallHistoryModule} from './call-history/call-history.module';
import {CmaIndustryModule} from './industry/industry.module';
import {CmaOutcomeSubReasonModule} from './outcome-sub-reason/outcome-sub-reason.module';
import {CmaCampaignListContactResultModule} from './campaign-list-contact-result/campaign-list-contact-result.module';
import {CmaCampaignListContactQuestionResultModule} from './campaign-list-contact-question-result/campaign-list-contact-question-result.module';
import {CmaCampaignListContactManagementModule} from './campaign-list-contact-management/campaign-list-contact-management.module';
import {CmaConsoleStatsModule} from './console-stats/console-stats.module';
import {CmaAlertsModule} from './alerts/alerts.module';
import {CmaPipelineModule} from './my-pipeline/pipeline.module';
import {CmaReportsModule} from './reports/reports.module';

import { CmaAlertModule } from './alert/alert.module';
import { CmaAlertUserModule } from './alert-user/alert-user.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        CmaUserCompanyModule,
        CmaClientCompanyStatusModule,
        CmaClientCompanyModule,
        CmaClientCampaignTemplateModule,
        CmaClientCampaignModule,
        CmaClientListModule,
        CmaFieldModule,
        CmaClientFieldModule,
        CmaClientListDataModule,
        CmaOutcomeModule,
        CmaCampaignPortalUserModule,
        CmaCampaignPortalUserOutcomeModule,
        CmaEmailTemplateModule,
        CmaOutcomeTypeModule,
        CmaClientCampaignScriptModule,
        CmaClientCampaignSmsModule,
        CmaAppointmentAttendeeModule,
        CmaSurveyQuestionModule,
        CmaSurveyQuestionFieldModule,
        CmaBookingQuestionModule,
        CmaBookingQuestionFieldModule,
        CmaClientCampaignAddDocModule,
        CmaCallHistoryTypeModule,
        CmaCallHistoryModule,
        CmaIndustryModule,
        CmaOutcomeSubReasonModule,
        CmaCampaignListContactResultModule,
        CmaCampaignListContactQuestionResultModule,
        CmaCampaignListContactManagementModule,
        CmaConsoleStatsModule,
        CmaPipelineModule,
        CmaReportsModule,
        CmaAlertModule,
        CmaAlertUserModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
        CmaConsoleModule,
        CmaAlertsModule,
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaEntityModule {
}
