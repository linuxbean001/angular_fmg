import {Component, OnDestroy, OnInit} from '@angular/core';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {JhiAlertService, JhiEventManager, JhiParseLinks} from 'ng-jhipster';

import {Field} from './field.model';
import {FieldService} from './field.service';
import {ITEMS_PER_PAGE, Principal} from '../../shared';

@Component({
    selector: 'jhi-field',
    templateUrl: './field.component.html'
})
export class FieldComponent implements OnInit, OnDestroy {

    currentAccount: any;
    fields: Field[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    constructor(
        private fieldService: FieldService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager
    ) {
        this.itemsPerPage = 2048; // ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
    }

    loadAll() {
        this.fieldService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res: HttpResponse<Field[]>) => this.onSuccess(res.body, res.headers),
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/field'], {
            queryParams:
                {
                    page: this.page,
                    size: this.itemsPerPage,
                    sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
                }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/field', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInFields();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Field) {
        return item.id;
    }

    registerChangeInFields() {
        this.eventSubscriber = this.eventManager.subscribe('fieldListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    canMoveDown(field: Field, i: number) {
        return i < this.fields.length - 1;
    }

    canMoveUp(field: Field, i: number) {
        return i > 0;
    }

    moveDown(field: Field) {
        const i = this.fields.indexOf(field);
        const anotherBookingQuestion = this.fields[i + 1];
        this.swapOrders(field, anotherBookingQuestion);
    }

    moveUp(field: Field) {
        const i = this.fields.indexOf(field);
        const anotherBookingQuestion = this.fields[i - 1];
        this.swapOrders(field, anotherBookingQuestion);
    }

    swapOrders(field, anotherField: Field) {
        // swap orders
        const order = field.order;
        field.order = anotherField.order;
        anotherField.order = order;

        const index = this.fields.indexOf(field);
        const anotherIndex = this.fields.indexOf(anotherField);
        this.fields.splice(index, 1, anotherField);
        this.fields.splice(anotherIndex, 1, field);

        // const a: Field = {id: field.id, order: field.order} as Field;
        this.fieldService.update(field).toPromise();

        // const b: Field = {id: anotherField.id, order: anotherField.order} as Field;
        this.fieldService.update(anotherField).toPromise();
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.fields = data;
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
