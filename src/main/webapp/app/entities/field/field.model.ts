import {BaseEntity} from '../../shared';

export class Field implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public mandatory?: boolean,
        public order?: number,
    ) {
        this.mandatory = false;
    }
}
