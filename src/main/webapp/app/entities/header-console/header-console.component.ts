import {Component, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ClientCampaign, ClientCampaignService} from '../client-campaign';
import {ClientList, ClientListGridService, DataListCompany, DataListContact, DataListSummary} from '../client-list';
import {SplitAreaDirective, SplitComponent} from 'angular-split';
import {TabChangedEvent, TabClickEvent, Tabs} from '../../layouts/tabs/tabs.component';
import {ConsoleCallHistoryComponent} from '../console/pages/call-history.component';
import {BriefingComponentMode} from '../client-campaign/pages/briefing.component';
import {Account, ConfirmOptions, ConfirmService, Principal} from '../../shared';
import {ConsoleStats, ConsoleStatsService} from '../console-stats';
import {ConsoleParams} from '../console/console.route';
import {Subscription} from 'rxjs';
import {CallHistory, CallHistoryService} from '../call-history';
import {JhiDateUtils, JhiEventManager} from 'ng-jhipster';
import {ActivatedRoute, Params} from '@angular/router';
import {ClientField} from '../client-field';
import {CallHistoryType} from '../call-history-type';
import {MoveContactEvent} from '../console/pages/outcome.component';
import {OutcomeWizardResult} from '../console/pages/outcome-tab/outcome-wizard.component';
import {ApplyTo} from '../console/pages/outcome-tab/console-outcome-apply.component';
import {OutcomeCategory} from '../outcome';
import {GdprComponentMode} from '../client-campaign/pages/gdpr.component';
import {RecordState} from '../campaign-list-contact-result';
import {UserMode} from '../console/console.component';
import {animate, keyframes, style, transition, trigger} from '@angular/animations';

@Component({
    selector: 'jhi-header-console',
    templateUrl: './header-console.component.html',
    animations: [
        trigger(
            'highlight', [
                transition('* => valueChanged', animate(1500, keyframes([
                        style({
                            backgroundColor: '#2fab06',
                            'box-shadow': '0px 0 14px 2px #2fab06',
                        }),
                        style({
                            backgroundColor: '#6c757d',
                            'box-shadow': 'none',
                        })
                    ])
                ))
            ]),
    ]
})
export class HeaderConsoleComponent implements OnInit, OnDestroy {

    /**
     * See com.fmg.cma.service.ClientListGridService.CONTACT_TYPE_FIELD_NAME.
     */
    CONTACT_TYPE_FIELD_NAME = 'Contact Type';

    // campaign
    selectedClientCampaign: ClientCampaign;
    clientCampaigns: ClientCampaign[] = [];

    // list
    selectedClientList: ClientList;
    clientLists: ClientList[] = [];

    // summary
    dataListSummary: DataListSummary;

    // companies
    filteredCompanies: DataListCompany[];
    selectedCompany: DataListCompany;
    companyActivePage = 1;

    // contacts
    filteredContacts: DataListContact[];
    selectedContact: DataListContact;
    dummyDataListContact: DataListContact;
    contactActiveTabIndex: number;

    // stats
    recordSearch: string;

    Calls = 'Calls';
    DM_Contact = 'DM Contact';
    Pending = 'Pending';
    Leads_Sales = 'Leads/Sales';
    Conversion_rate = 'Conversion rate';
    Pipeline_opps = 'Pipeline opps.';
    Skips = 'Skips';
    availableStats = [
        {name: this.Calls},
        {name: this.DM_Contact},
        {name: this.Pending},
        {name: this.Leads_Sales},
        {name: this.Conversion_rate},
        {name: this.Pipeline_opps},
        {name: this.Skips},
    ];
    selectedStats = [...this.availableStats];
    changedStats: { [stat: string]: boolean } = {};

    @ViewChild('split') split: SplitComponent;
    @ViewChild('area1') area1: SplitAreaDirective;
    @ViewChild('area2') area2: SplitAreaDirective;

    @ViewChild('companyTabs') companyTabs: Tabs;
    @ViewChild('contactTabs') contactTabs: Tabs;
    @ViewChild('tabs3') tabs3: Tabs;
    BriefingComponentMode = BriefingComponentMode;
    GdprComponentMode = GdprComponentMode;
    @ViewChild(ConsoleCallHistoryComponent)
    private callHistoryComponent: ConsoleCallHistoryComponent;

    showRecordsState = RecordState.ACTIVE;
    RecordState = RecordState;
    currentAccount: Account;
    stats: ConsoleStats;
    consoleParams: ConsoleParams;
    UserMode = UserMode;
    userMode: UserMode = UserMode.USER;
    subscription: Subscription;
    innerHeight: number;
    rootStyle = {'height': '700px'};
    panelsStyle = {'height': '300px', 'overflow': 'auto'};
    rightPanelsStyle = {'height': '264px', 'overflow': 'auto'};

    constructor(
        private clientCampaignService: ClientCampaignService,
        private clientListGridService: ClientListGridService,
        private confirmService: ConfirmService,
        private callHistoryService: CallHistoryService,
        private principal: Principal,
        private dateUtils: JhiDateUtils,
        private consoleStatsService: ConsoleStatsService,
        private activatedRoute: ActivatedRoute,
        private eventManager: JhiEventManager,
    ) {
        this.subscription = this.activatedRoute.queryParams.subscribe((queryParams: Params) => {
            if (queryParams['method'] === 'APPROVE_ONLY') {
                const method = queryParams['method'];
                // tslint:disable-next-line:radix
                const campaignId = queryParams['campaignId'] ? parseInt(queryParams['campaignId']) : null;
                // tslint:disable-next-line:radix
                const listId = queryParams['listId'] ? parseInt(queryParams['listId']) : null;
                // tslint:disable-next-line:radix
                const contactRowNums = queryParams['contactRowNums'] ? queryParams['contactRowNums'].map((str) => parseInt(str)) : null;
                this.consoleParams = new ConsoleParams(method, campaignId, listId, contactRowNums);
                this.showRecordsState = RecordState.PENDING;
                this.userMode = UserMode.QA;
            } else {
                this.consoleParams = null;
                this.showRecordsState = RecordState.ACTIVE;
                this.userMode = UserMode.USER;
            }
        });
    }

    ngOnInit() {
        this.innerHeight = window.innerHeight;
        this.adjustHeight();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.loadCampaigns();
        this.resetDummyContact();
        this.loadStats();
    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.innerHeight = window.innerHeight;
        this.adjustHeight();
    }

    adjustHeight() {
        const availableHeight = this.innerHeight - 185;
        this.rootStyle = {'height': availableHeight + 'px'};
        this.panelsStyle = {'height': (availableHeight - 340) + 'px', 'overflow': 'auto'};
        this.rightPanelsStyle = {'height': (availableHeight - 340 - 36) + 'px', 'overflow': 'auto'};
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.subscription);
    }

    loadCampaigns() {
        this.clientCampaignService.consoleRelatedClientCampaigns().toPromise()
            .then((res) => {
                this.clientCampaigns = res.body;
                if (this.consoleParams) {
                    if (this.consoleParams.campaignId) {
                        const clientCampaign = this.clientCampaigns.find((campaign) => campaign.id === this.consoleParams.campaignId);
                        this.clientCampaignChange(clientCampaign)
                            .then((value) => {
                                if (this.consoleParams.listId) {
                                    const clientList = this.clientLists.find((list) => list.id === this.consoleParams.listId);
                                    this.clientListChange(clientList);
                                }
                            });
                    }
                }
            });
    }

    clientCampaignChange(newClientCampaign: ClientCampaign) {
        this.selectedClientCampaign = newClientCampaign;
        this.selectedClientList = null;
        this.clientListChange(this.selectedClientList);

        if (!this.selectedClientCampaign) {
            this.clientLists = [];
            return;
        }
        return this.clientCampaignService.find(this.selectedClientCampaign.id).toPromise()
            .then((clientCampaignResponse) => this.clientLists = clientCampaignResponse.body.clientLists);
    }

    onTabClick(tabs: Tabs, event: TabClickEvent) {
        tabs.selectTab(event.to);
    }

    onTabChanged(tabs: Tabs, $event: TabChangedEvent) {

    }

    companyTabChanged(tabs: Tabs, $event: TabChangedEvent) {

    }

    contactTabChanged(tabs: Tabs, $event: TabChangedEvent) {
        const index = tabs.getFirstActiveTabIndex();
        this.selectedContact = this.filteredContacts[index];
        this.contactActiveTabIndex = index;
    }

    clientListChange(newClientList: ClientList) {
        this.selectedClientList = newClientList;
        this.resetCompanyPage();
        if (this.selectedClientList) {
            this.clientListGridService.getSummary(this.selectedClientList.id).toPromise()
                .then((summaryResponse) => this.applySummary(summaryResponse.body));
        } else {
            this.applySummary(null);
        }
        this.updateUserMode();
    }

    updateUserMode() {
        const isQa = this.selectedClientCampaign &&
            this.selectedClientCampaign.qaUsers.some((user) => user.id === this.currentAccount.id);
        const isUser = this.selectedClientCampaign &&
            this.selectedClientCampaign.users.some((user) => user.id === this.currentAccount.id);
        const isManager = this.selectedClientCampaign &&
            this.selectedClientCampaign.managers.some((user) => user.id === this.currentAccount.id);

        // should I check this.consoleParams ??
        this.userMode = (isQa && !isUser && !isManager) ? UserMode.QA : UserMode.USER;
    }

    applySummary(summary: DataListSummary) {
        if (summary) {
            this.dataListSummary = summary;
            this.filteredCompanies = this.dataListSummary.companies.filter(this.filterCompany);
        } else {
            this.dataListSummary = null;
            this.filteredCompanies = [];
        }
        this.companyActivePage = 1;
        this.companyPageChanged(this.companyActivePage);
    }

    companyPageChanged(newCompanyPage: number) {
        this.companyActivePage = newCompanyPage;
        const index = newCompanyPage - 1;
        this.selectedCompany = this.filteredCompanies.length > index ? this.filteredCompanies[index] : null;
        this.filteredContacts = this.selectedCompany ? this.selectedCompany.contacts.filter((contact) => this.filterContact(contact)) : null;
        this.selectContactTab(0);
    }

    selectContactTab(index: number) {
        this.contactActiveTabIndex = index;
        setTimeout(() => {
            if (this.contactTabs) {
                const tab = this.contactTabs.tabs.toArray()[index];
                this.contactTabs.selectTab(tab);
            } else {
                this.selectedContact = null;
            }
        });
    }

    resetDummyContact() {
        // see https://gitlab.com/mzdw4w/fmg/issues/87
        // TODO with what rowNum should I populate the tab?
        this.dummyDataListContact = new DataListContact('+', null, null, false, {}, true);
    }

    companyAlreadyExists(company: DataListCompany): DataListCompany | null {
        return this.dataListSummary.companies
            .find((c) => c !== company &&
                DataListCompany.isFieldsValueEquals(company.valueMap, c.valueMap, this.dataListSummary.pkFields)
            );
    }

    saveContact(contact: DataListContact): Promise<DataListContact> {
        const company = this.selectedCompany;
        // if company is completely new required to click Save/Next
        if (company.isDummy) {
            return Promise.resolve(contact);
        }
        if (!contact.isChanged) {
            this.showWarning(`Contact didn't changed`);
            return Promise.resolve(contact);
        }

        return this.saveCompanyContact(company, contact);
    }

    saveContactButtonDisabled() {
        return !this.selectedContact || this.qaUserAndPendingState();
    }

    showWarning(message: string) {
        const options: ConfirmOptions = {
            title: 'Warning',
            message,
            yesCaption: 'OK',
            noHidden: true,
        };

        this.confirmService.confirm(options)
            .then((result) => {
                // nothing
            });
    }

    isPhoneField(field: ClientField): boolean {
        const fieldName = field.fieldName.toLowerCase();
        return fieldName.indexOf('phone') > -1 || fieldName.indexOf('mobile') > -1;
    }

    skipCompany() {
        if (this.companyActivePage === this.filteredCompanies.length) {
            return;
        }
        this.companyActivePage = this.companyActivePage + 1;
        this.companyPageChanged(this.companyActivePage);

        this.consoleStatsService.incrementSkips().toPromise()
            .then((statsResponse) => this.applyStats(statsResponse.body));
    }

    addNewCompany() {
        // is this is correct? or I should use RecordState.ACTIVE.ident?
        const state = this.showRecordsState.ident;
        const dataListContact = new DataListContact(null, 0, null, false, {}, true, false, state);
        const dataListCompany = new DataListCompany({}, null, [], null, [dataListContact], [], true);

        // TODO what about filteredCompanies?
        this.dataListSummary.companies.push(dataListCompany);
        this.companyActivePage = this.filteredCompanies.push(dataListCompany);
        // navigate to the last (new) company
        this.companyPageChanged(this.companyActivePage);
    }

    saveCompany() {
        if (this.selectedCompany.isChanged) {
            const existedCompany = this.companyAlreadyExists(this.selectedCompany);
            if (existedCompany) {
                const str = DataListCompany.fieldValuesToString(this.dataListSummary.pkFields, this.selectedCompany.valueMap);

                const message = 'This Company already exists on this list. "' + str + '". ' +
                    'Do you want to save this contact to this existing Company (current company\'s changes will be reverted)?';
                const options: ConfirmOptions = {
                    title: 'Warning',
                    message,
                    yesCaption: 'Yes',
                    noCaption: 'Cancel',
                };

                this.confirmService.confirm(options)
                    .then((result) => {
                        // THIS IS ABSOLUTELY NOT CLEAR! revert current company changes
                        this.clientListGridService.getSummary(this.selectedClientList.id).toPromise()
                            .then((summaryResponse) => {
                                const summary = summaryResponse.body;
                                const serverCompany = summary.companies.find((c) => c.rowNum === this.selectedCompany.rowNum);
                                this.selectedCompany.valueMap = serverCompany.valueMap;
                            });

                        // remove contact from current company
                        this.removeActiveContactFromItsCompany();
                        // move contact to the existed company (database)
                        this.saveCompanyContact(existedCompany, this.selectedContact);
                        // and client side
                        existedCompany.contacts.push(this.selectedContact);

                        this.focusOnNextOrPreviousContact();
                    });
            } else {
                this.doCompanySave(this.selectedCompany);
                this.skipCompany();
            }
        } else {
            this.selectedCompany.contacts
                .filter((contact) => contact.isChanged)
                .forEach((contact) => this.saveContact(contact));
            this.skipCompany();
        }
    }

    /**
     * Remove contact from current company.
     */
    removeActiveContactFromItsCompany() {
        this.filteredContacts.splice(this.filteredContacts.indexOf(this.selectedContact), 1);
        this.selectedCompany.contacts.splice(this.selectedCompany.contacts.indexOf(this.selectedContact), 1);
    }

    /**
     * Focus next or previous contact.
     */
    focusOnNextOrPreviousContact() {
        setTimeout(() => {
            if (this.contactTabs) {
                const tbs = this.contactTabs.tabs.toArray();
                const tab = tbs.length - 1 > this.contactActiveTabIndex ? tbs[this.contactActiveTabIndex] : tbs[this.contactActiveTabIndex - 1];
                if (tab) {
                    this.contactTabs.selectTab(tab);
                }
            }
        });
    }

    phoneClick() {
        const callHistory = this.makeCallHistory(CallHistoryType.CALL);
        callHistory.contactRowNum = this.selectedContact.rowNum;
        this.callHistoryService.create(callHistory).toPromise()
            .then((response) => {
                this.selectedCompany.histories.push(response.body);
                this.callHistoryComponent.fillHistories();
            });

        this.consoleStatsService.incrementCalls().toPromise()
            .then((statsResponse) => this.applyStats(statsResponse.body));
    }

    companyFieldChange(dataListCompany: DataListCompany) {
        dataListCompany.isChanged = true;
    }

    contactFieldChange(contact: DataListContact) {
        contact.isChanged = true;
    }

    contactFieldIsReadonly(contact: DataListContact) {
        return contact.isLeftCompany || this.qaUserAndPendingState();
    }

    outcomeMoveContact($event: MoveContactEvent) {
        // TODO implement this
        const destinationClientCampaignId = $event.destinationClientCampaignId;
        const destinationClientListId = $event.destinationClientListId;
        // this.saveCompanyContact()

        // if campaign and list is not changed I think I can exit immediately, yes?
        if (this.selectedClientCampaign.id === destinationClientCampaignId && this.selectedClientList.id === destinationClientListId) {
            console.log('campaign and list is not changed I think I can exit immediately, yes?');
            return;
        }

        const contact = this.selectedContact;

        this.clientListGridService.moveContactToAnotherList(this.selectedClientList.id, destinationClientListId, contact.rowNum).toPromise()
            .then((response) => {
                // nothing?
            });

        // move contact
        // remove current contact tab
        this.removeActiveContactFromItsCompany();
        this.focusOnNextOrPreviousContact();
        // if company doesn't contains any contact anymore - remove company from summary
        if (this.selectedCompany.contacts.length === 0) {
            const dataListCompanies = this.dataListSummary.companies;
            const index = dataListCompanies.indexOf(this.selectedCompany);
            dataListCompanies.splice(index, 1);
            // this.companyPage starts from 1 (not 0)
            const newCompanyPage = dataListCompanies.length > this.companyActivePage ? this.companyActivePage : this.companyActivePage - 1;
            this.companyPageChanged(newCompanyPage);
        }

        // // actualize summary if required
        // if (this.selectedClientCampaign.id === destinationClientCampaignId && this.selectedClientList.id !== destinationClientListId) {
        //     // this.dataListSummary.companies
        //     //     .find((company))
        // }
    }

    noContactApply(callHistory: CallHistory) {
        this.actualizeCompanyCallHistories(this.selectedCompany);
        // see (p.2) https://gitlab.com/mzdw4w/fmg/issues/134#note_236072351
        this.consoleStatsService.incrementCalls().toPromise()
            .then((statsResponse) => this.applyStats(statsResponse.body));
    }

    private saveCompanyContact(company: DataListCompany, contact: DataListContact): Promise<DataListContact> {
        const contactTypeClientField = this.dataListSummary.fields.find((field) => field.fieldName === this.CONTACT_TYPE_FIELD_NAME);
        if (!contactTypeClientField) {
            const message = `Unable to find field '${this.CONTACT_TYPE_FIELD_NAME}'`;
            this.showWarning(message);
            return Promise.reject(message);
        }

        const contactType = contact.valueMap[contactTypeClientField.id];
        if (!contactType) {
            this.showWarning(`Contact Type is required`);
            // revert
            if (!contact.isDummy) {
                contact.valueMap[contactTypeClientField.id] = contact.contactType;
            }
            return Promise.resolve(contact);
        }

        // TODO simplify (remove checking Dummy property)
        if (contact.isDummy) {
            contact.isDummy = false;

            company.contacts.push(contact);
            this.resetDummyContact();
        }

        contact.contactType = contactType;

        return this.clientListGridService.saveCompanyContact(this.selectedClientList.id, company, contact).toPromise()
            .then((contactResponse) => {
                contact.rowNum = contactResponse.body.rowNum;
                contact.note = null;
                this.actualizeCompanyCallHistories(company);
                return contact;
            });
    }

    private doCompanySave(company: DataListCompany) {
        const clonedCompany: DataListCompany = JSON.parse(JSON.stringify(company));
        // persist only changes contacts
        clonedCompany.contacts = clonedCompany.contacts.filter((contact) => contact.isChanged);
        // no need to send histories
        clonedCompany.histories = [];

        this.clientListGridService.saveCompany(this.selectedClientList.id, clonedCompany).toPromise()
            .then((companyResponse) => {
                const serverCompany = companyResponse.body;
                company.rowNum = serverCompany.rowNum;
                // clear notes
                company.note = null;
                serverCompany.contacts.forEach((serverContact) => {
                    const dataListContact = company.contacts.find((contact) => contact.rowNum === serverContact.rowNum);
                    if (dataListContact) {
                        dataListContact.note = null;
                    }
                });

                this.actualizeCompanyCallHistories(company);
            });
    }

    /**
     * actualize call history list.
     */
    private actualizeCompanyCallHistories(company) {
        this.callHistoryService.getCallHistories(this.selectedClientList.id, company.rowNum).toPromise()
            .then((callHistoriesResponse) => {
                company.histories = callHistoriesResponse.body;
                this.callHistoryComponent.fillHistories();
            });
    }

    outcomeFinishHandler(result: OutcomeWizardResult) {
        this.actualizeCompanyCallHistories(this.selectedCompany);
        // TODO use the result

        let promise;
        if (result.applyTo === ApplyTo.RECORD) {
            const shallow = {...this.selectedContact};
            this.alterContact(shallow, result);
            promise = this.saveContact(shallow)
                .then((savedContact) => {
                    if (savedContact.state !== this.selectedContact.state) {
                        this.clientListChange(this.selectedClientList);
                    } else {
                        this.selectedContact = savedContact;
                    }

                    // Pipeline opps  relate to outcomes which have been assigned to a record that have the category Pipeline and pipeline>50%
                    if (result.clickedOutcome.categoryIdent === OutcomeCategory.PIPELINE.ident ||
                        result.clickedOutcome.categoryIdent === OutcomeCategory.PIPELINE_GT_50.ident) {
                        this.consoleStatsService.incrementPipelineOpps().toPromise()
                            .then((statsResponse) => this.applyStats(statsResponse.body));
                    }

                    // see (p.1) https://gitlab.com/mzdw4w/fmg/issues/134#note_236072351
                    this.consoleStatsService.incrementCalls().toPromise()
                        .then((statsResponse) => this.consoleStatsService.incrementDMContact().toPromise())
                        .then((statsResponse) => this.applyStats(statsResponse.body));
                });
        } else if (result.applyTo === ApplyTo.ORGANISATION) {
            const promises = this.filteredContacts.map((contact) => {
                this.alterContact(contact, result);
                return this.saveContact(contact);
            });
            promise = Promise.all(promises)
                .then((responses) => {
                    this.clientListChange(this.selectedClientList);
                });
        }

        promise.then((value) => {
            // the view of the right hand screen return to 'Call history' instead
            const tab = this.tabs3.tabs.toArray()[0];
            this.tabs3.selectTab(tab);
        });
    }

    alterContact = (contact: DataListContact, result: OutcomeWizardResult) => {
        // prevent 'Contact didn't changed' message
        contact.isChanged = true;
        contact.state = result.newState.ident;
        contact.clickedOutcomeId = result.clickedOutcome.id;
        if (result.newState === RecordState.MY_LIST) {
            // server will do this, see com.fmg.cma.service.ClientListGridService.saveCompanyContact
            // contact.assignUserId = this.currentAccount.id;
            // contact.assignUserLogin = this.currentAccount.login;
            // contact.assignStamp = this.dateUtils.toDate(new Date);
        } else {
            contact.assignUserId = null;
            contact.assignUserLogin = null;
            contact.assignStamp = null;
        }
    }

    setShowMode(showMode: RecordState) {
        this.showRecordsState = showMode;
        this.resetCompanyPage();
        this.applySummary(this.dataListSummary);
    }

    resetCompanyPage() {
        this.companyActivePage = 1;
    }

    private makeCallHistory(typeIdent: string): CallHistory {
        return new CallHistory({
            typeIdent,
            listId: this.selectedClientList.id,
            orgRowNum: this.selectedCompany.rowNum,
        });
    }

    filterCompany = (company: DataListCompany) => {
        return company.contacts.some((contact) => this.filterContact(contact));
    }

    filterContact = (contact: DataListContact) => {
        if (this.showRecordsState === RecordState.MY_LIST) {
            return contact.state === this.showRecordsState.ident && contact.assignUserId === this.currentAccount.id;
        } else {
            return contact.state === this.showRecordsState.ident;
        }
    }

    showStat(name: string) {
        return this.selectedStats.some((item: { name: string }) => item.name.trim() === name.trim());
    }

    private loadStats() {
        this.consoleStatsService.getByUserIsCurrentUserAndStampIsCurrentDate().toPromise()
            .then((statsResponse) => this.applyStats(statsResponse.body));
    }

    private applyStats(newStats: ConsoleStats) {
        if (!this.stats) {
            this.stats = newStats;
            return;
        }

        this.changedStats[this.Skips] = this.stats.skips !== newStats.skips;
        this.changedStats[this.Calls] = this.stats.calls !== newStats.calls;
        this.changedStats[this.Pipeline_opps] = this.stats.pipelineOpps !== newStats.pipelineOpps;
        this.changedStats[this.DM_Contact] = this.stats.dmContact !== newStats.dmContact;
        this.changedStats[this.Pending] = this.stats.pending !== newStats.pending;
        this.stats = newStats;

        setTimeout(() => {
            this.changedStats = {};
        }, 1000);
    }

    displayQaUserCombobox() {
        // see https://gitlab.com/mzdw4w/fmg/issues/169#note_257728486
        // if user is just a QA .. show toggle
        return this.selectedClientCampaign &&
            this.selectedClientCampaign.qaUsers.some((user) => user.id === this.currentAccount.id);
    }

    disabledQaUserCombobox() {
        // see https://gitlab.com/mzdw4w/fmg/issues/169#note_257728486
        // if user is just a QA .. disable the toggle (so cannot be moved to user)
        return !(this.selectedClientCampaign &&
            this.selectedClientCampaign.users.some((user) => user.id === this.currentAccount.id));
    }

    toggleQaUserCombobox() {
        this.userMode = this.userMode === UserMode.QA ? UserMode.USER : UserMode.QA;
    }

    qaUserAndPendingState() {
        return (this.userMode === UserMode.QA && this.showRecordsState !== RecordState.PENDING);
    }

}
