import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {HeaderConsoleComponent} from './header-console.component';
import {CmaSharedModule} from '../../shared';
import {TabModule} from '../../layouts/tabs/tab.module';
import {AngularSplitModule} from 'angular-split';
import {CmaClientCampaignModule} from '../client-campaign/client-campaign.module';
import {CmaEmailTemplateModule} from '../email-template/email-template.module';
import {CmaClientCampaignSmsModule} from '../client-campaign-sms/client-campaign-sms.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
    imports: [
        CmaSharedModule,
        TabModule,
        AngularSplitModule,
        BrowserAnimationsModule,
        CmaClientCampaignModule,
        CmaEmailTemplateModule,
        CmaClientCampaignSmsModule,
    ],
    declarations: [
        HeaderConsoleComponent,
    ],
    entryComponents: [
        HeaderConsoleComponent,
    ],
    providers: [],
    exports: [
        HeaderConsoleComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaHeaderConsoleModule {
}
