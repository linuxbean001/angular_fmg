import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TypeSearch, TypesSearch} from '../table/domain/TypeSearch';
import {ColumnSearch} from '../domain/ColumnSearch';
import {Searching} from '../table/domain/Searching';
import {PipelineDateTimePickerDialogComponent} from './pipeline.datetimepicker-dialog.component';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {NgbTime} from '@ng-bootstrap/ng-bootstrap/timepicker/ngb-time';
import {NgbDate} from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date';

@Component({
    selector: 'jhi-datetimepicker',
    templateUrl: './datetimepicker.component.html',
    styleUrls: ['./datetimepicker.component.css']
})
export class DatetimepickerComponent implements OnInit {

    modalRef: NgbModalRef;
    datePickVal: NgbDate;
    timerPickVal: NgbTime;

    @Input() typesSearch: Array<TypeSearch>;
    @Input() column2Search: ColumnSearch;
    @Input() initDate?: NgbDate;
    @Output() responseObj = new EventEmitter<Searching>();

    search: Searching;

    constructor(private _modalService: NgbModal) {
        this.search = new Searching('', new Date(), this.column2Search, TypesSearch.GREATERTHAN);
    }

    ngOnInit() {
        this.search.column = this.column2Search;
        this.initDate ? this.datePickVal = this.initDate : this.datePickVal = undefined;
    }

    openModalDate() {
        this.modalRef = this._modalService.open(PipelineDateTimePickerDialogComponent, {size: 'sm'});
        this.modalRef.componentInstance.dateTimePick = this.datePickVal;
        this.modalRef.result.then((response) => {
            this.timerPickVal = response.timePick;
            this.datePickVal = response.datePick;
            this.search.dateTime2Search = this.toDate(this.timerPickVal, this.datePickVal);
            this.sendFilter();
        }, (reason) => {
            console.log('Reason of close ' + reason);
        });
    }

    sendFilter() {
        this.responseObj.emit(this.search);
    }

    toDate(theTime: NgbTime, theDate: NgbDate) {
        if (theTime && theDate) {
            return new Date(theDate.year, (theDate.month - 1), theDate.day, theTime.hour, theTime.minute, theTime.second);
        }
        return null;
    }

}
