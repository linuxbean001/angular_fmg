import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgbActiveModal, NgbCalendar, NgbDateParserFormatter, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {NgbDate} from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date';
import {NgbTime} from '@ng-bootstrap/ng-bootstrap/timepicker/ngb-time';

@Component({
    selector: 'jhi-datetimepicker-dialog',
    templateUrl: './pipeline.datetimepicker-dialog.component.html',
    styleUrls: ['../modal/pipeline.dialog-component.css']
})
export class PipelineDateTimePickerDialogComponent implements OnInit {

    @Input() dateTimePick: NgbDate;
    @Output() outDateTimePick = new EventEmitter();
    minDatePick: NgbDateStruct;
    today: NgbDate;
    modelDatePick: NgbDate;
    modelTimePick: NgbTime;
    meridian: boolean;

    constructor(
        public modal: NgbActiveModal,
        private calendar: NgbCalendar,
        private dateFormat: NgbDateParserFormatter
    ) {
        this.minDatePick = {year: 1900, month: 1, day: 1};
        this.meridian = true;
    }

    ngOnInit() {
        this.modelDatePick = this.dateTimePick;
        this.today = this.calendar.getToday();
        this.modelTimePick = this.formatTime(new Date());
    }

    pickDate() {
        if (this.dateFormat.format(this.modelDatePick) !== this.dateFormat.format(this.dateTimePick)) {
            this.modal.close({datePick: this.modelDatePick, timePick: this.modelTimePick});
        }
    }

    pickToday() {
        this.modal.close({datePick: this.today, timePick: this.modelTimePick});
    }

    clear() {
        this.modal.close({datePick: '', timePick: ''});
    }

    formatTime(currentDate: Date) {
        const source = new NgbTime();
        return Object.assign(source, {
            hour: currentDate.getHours(),
            minute: currentDate.getMinutes(),
            second: currentDate.getSeconds()
        });
    }

}
