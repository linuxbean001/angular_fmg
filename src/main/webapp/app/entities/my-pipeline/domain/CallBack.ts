export class CallBack {
    constructor(public actionDate: Date,
                public campaing: string,
                public  dataList: string,
                public companyName: string,
                public contactName: string,
                public state: string,
                public creationDate: Date,
                public comment: string,
                public lastOutcome: string,
                public priority: string) {
    }
}
