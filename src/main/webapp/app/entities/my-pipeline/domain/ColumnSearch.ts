export class ColumnSearch {
    constructor(public name: string,
                public typeColumn: TypeColumn,
                public column: Columns
    ) {
    }
}

export enum TypeColumn {
    'STRING',
    'DATE',
    'NUMBER',
    'TIME',
}

export enum Columns {
    'ALL',
    'ACTIONDATE',
    'CAMPING',
    'DATALIST',
    'COMPANYNAME',
    'CONTACTNAME',
    'STATE',
    'CREATIONDATE',
    'PRIORITY',
    'LASTOUT',
    'NONE',
    'APPOINTMENT',
    'CAMPAIGNDATE',
    'QUESTION1',
    'QUESTION2',
    'QUESTION3',
    'QUESTION4',
    'QUESTION5',
    'MOBILENUM',
    'EMAILADD',
    'DATEMAIL',
    'TIMEMAIL',
    'AGENTNAME',
    'TRANSACTION',
    'TYPEMAIL',
    'TEMPLATEMAIL',
    'FIRSTNAME',
    'PHONENUM',
    'SURMAE',
    'ADDRESS',
    'ADDRESS2',
    'CITY',
    'COUNTRY',
    'POSTCODE',
    'CALLERID',
    'NOTES',
    'CONTACTYPE',
    'TITLE'

}
