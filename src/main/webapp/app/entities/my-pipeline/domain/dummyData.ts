import {CallBack} from './CallBack';

export const CALL_BACKS: CallBack[] = [
    {
        actionDate: new Date(1992, 0, 21),
        campaing: 'Just Do iT !',
        dataList: 'List A',
        companyName: 'Microsoft',
        contactName: 'Bob Fish',
        state: 'London',
        creationDate: new Date(2002, 10, 10),
        comment: 'Loved the product we check that and is awesome for all ! ',
        lastOutcome: 'DM Contacted',
        priority: 'High'
    },
    {
        actionDate: new Date(1991, 11, 22),
        campaing: 'Smell the Victory',
        dataList: 'SW Region',
        companyName: 'Virgin',
        contactName: 'Lola Guacamole',
        state: 'Manchester',
        creationDate: new Date(1991, 11, 22),
        comment: 'Away back Thusday',
        lastOutcome: 'No Contact',
        priority: 'Low'
    },
    {
        actionDate: new Date(1992, 9, 13),
        campaing: 'Fancy a Chat',
        dataList: 'Regional Office',
        companyName: 'Sainsbury',
        contactName: 'Rey Hedge',
        state: 'Edinburgh',
        creationDate: new Date(1991, 11, 22),
        comment: 'Checking budget I deposit the amount pending.',
        lastOutcome: 'Hot Lead',
        priority: 'High'
    },
    {
        actionDate: new Date(2002, 10, 10),
        campaing: 'Fancy a Chat',
        dataList: 'Regional Office',
        companyName: 'Sainsbury',
        contactName: 'Rey Hedge',
        state: 'Edinburgh',
        creationDate: new Date(),
        comment: 'Checking budget I deposit the amount pending.',
        lastOutcome: 'Hot Lead',
        priority: 'Low'
    },
    {
        actionDate: new Date(),
        campaing: 'Fancy a Chat',
        dataList: 'Regional Office',
        companyName: 'Sainsbury',
        contactName: 'Rey Hedge',
        state: 'Edinburgh',
        creationDate: new Date(),
        comment: 'Checking budget I deposit the amount pending.Checking budget I deposit the amount pending.Checking ' +
            'budget I deposit the amount pending.Checking budget I deposit the amount pending.Checking budget I deposit ' +
            'the amount pending.',
        lastOutcome: 'Hot Lead',
        priority: 'High'
    },
    {
        actionDate: new Date(),
        campaing: 'Fancy a Chat',
        dataList: 'Regional Office',
        companyName: 'Sainsbury',
        contactName: 'Rey Hedge',
        state: 'Edinburgh',
        creationDate: new Date(2002, 10, 10),
        comment: 'Checking budget I deposit the amount pending.',
        lastOutcome: 'Hot Lead jajajajajajajajajjajaja',
        priority: 'Medium'
    },
    {
        actionDate: new Date(),
        campaing: 'Fancy a Chat',
        dataList: 'Regional Office',
        companyName: 'Sainsbury',
        contactName: 'Rey Hedge',
        state: 'Edinburgh',
        creationDate: new Date(),
        comment: 'Checking budget I deposit the amount pending.',
        lastOutcome: 'Hot Lead',
        priority: 'Low'
    },
    {
        actionDate: new Date(),
        campaing: 'Fancy a Chat',
        dataList: 'Regional Office',
        companyName: 'Sainsbury',
        contactName: 'Rey Hedge',
        state: 'Edinburgh',
        creationDate: new Date(),
        comment: 'Checking budget I deposit the amount pending.',
        lastOutcome: 'Hot Lead',
        priority: 'High'
    }
];
