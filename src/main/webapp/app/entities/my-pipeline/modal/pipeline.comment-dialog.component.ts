import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-comment-dialog',
    templateUrl: './pipeline.comment-dialog.component.html'
})
export class PipelineCommentDialogComponent implements OnInit {

    @Input() message: string;

    constructor(
        public modal: NgbActiveModal
    ) {
    }

    ngOnInit() {
    }

}
