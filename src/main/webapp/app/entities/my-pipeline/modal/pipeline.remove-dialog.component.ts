import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-remove-dialog',
    templateUrl: './pipeline.remove-dialog.component.html'
})
export class PipelineRemoveDialogComponent implements OnInit {

    isSaving: boolean;
    @Input() callBack: object;

    constructor(
        public modal: NgbActiveModal
    ) {
    }

    ngOnInit() {
        console.log(this.callBack);
    }

}
