import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';

@Injectable()
export class PipelineExport {

    private resourceUrl = SERVER_API_URL + 'api/export';

    constructor(private http: HttpClient) {
    }

    pdf(): Observable<Blob> {
        let headers = new HttpHeaders();
        headers = headers.set('Accept', 'application/pdf');
        return this.http.get(this.resourceUrl.concat('/pdf'), {headers, responseType: 'blob'});
    }

    csv(): Observable<Blob> {
        let headers = new HttpHeaders();
        headers = headers.set('Accept', 'text/csv');
        return this.http.get(this.resourceUrl.concat('/csv'), {headers, responseType: 'blob'});
    }

}
