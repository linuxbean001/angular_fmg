import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {pipelineRoute} from './pipeline/pipeline.route';
import {NgbDateParserFormatter, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BrowserModule} from '@angular/platform-browser';
import {CommonModule, DecimalPipe} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PipelineRemoveDialogComponent} from './modal/pipeline.remove-dialog.component';
import {PipelineComponent} from './pipeline/pipeline.component';
import {NgbdSortableHeaderDirective} from './table/directives/sortable.directive';
import {PipelineService} from './pipeline/pipeline.service';
import {CustomNgbDateParserFormatter} from '../../utils/CustomNgbDateParserFormatter';
import {PipelineCommentDialogComponent} from './modal/pipeline.comment-dialog.component';
import {PipelineDateTimePickerDialogComponent} from './datetimepicker/pipeline.datetimepicker-dialog.component';
import {PipelineExport} from './pipeline.export.service';
import {SearchColumnComponent} from './table/search-column/search-column.component';
import {DatetimepickerComponent} from './datetimepicker/datetimepicker.component';
import {CmaHeaderConsoleModule} from '../header-console/header-console.module';
import {AgGridModule} from '@ag-grid-community/angular';
import {TransactionReportComponent} from '../../home/transaction-report/transaction-report.component';
import {PipelineButtonsComponent} from './pipeline/pipeline-buttons.component';

const ENTITY_STATES = [
    ...pipelineRoute,
];

@NgModule({
    imports: [
        RouterModule.forChild(ENTITY_STATES),
        BrowserModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        CmaHeaderConsoleModule,
        AgGridModule.withComponents(
            [
                PipelineComponent,
                PipelineButtonsComponent
            ]
        )
    ],
    declarations: [
        PipelineComponent,
        PipelineRemoveDialogComponent,
        PipelineCommentDialogComponent,
        PipelineDateTimePickerDialogComponent,
        NgbdSortableHeaderDirective,
        SearchColumnComponent,
        DatetimepickerComponent,
        PipelineButtonsComponent
    ],
    entryComponents: [
        PipelineComponent,
        PipelineRemoveDialogComponent,
        PipelineCommentDialogComponent,
        PipelineDateTimePickerDialogComponent
    ],
    providers: [PipelineService, PipelineExport, DecimalPipe, CustomNgbDateParserFormatter, {
        provide: NgbDateParserFormatter,
        useClass: CustomNgbDateParserFormatter
    }],
    exports: [
        DatetimepickerComponent,
        SearchColumnComponent,
        NgbdSortableHeaderDirective
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaPipelineModule {
}
