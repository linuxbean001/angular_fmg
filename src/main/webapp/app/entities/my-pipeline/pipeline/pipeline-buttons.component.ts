import {Component} from '@angular/core';
import {ICellRendererAngularComp} from '@ag-grid-community/angular';
import {PipelineRemoveDialogComponent} from '../modal/pipeline.remove-dialog.component';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-buttons-pipeline',
    template: `
        <div class='btn-group'>
            <a class='btn btn-outline-info'>View</a>
            <a class='btn btn-outline-info' (click)='removeModal()'>Remove</a>
        </div>`,
    styles: [
            `.btn {
            line-height: 0.5
        }`
    ]
})
export class PipelineButtonsComponent implements ICellRendererAngularComp {
    public params: any;
    modalRef: NgbModalRef;

    agInit(params: any): void {
        this.params = params;
    }

    constructor(private _modalService: NgbModal) {
    }

    public invokeParentMethod() {
        this.params.context.componentParent.methodFromParent(`Row: ${this.params.node.rowIndex}, Col: ${this.params.colDef.headerName}`);
    }

    removeModal() {
        this.modalRef = this._modalService.open(PipelineRemoveDialogComponent);
        this.modalRef.componentInstance.callBack = {'data': 'its a data'};
        this.modalRef.result.then((response) => {
            console.log('Reason of ok ' + response);
        }, (reason) => {
            console.log('Reason of close ' + reason);
        });
    }

    refresh(): boolean {
        return false;
    }
}
