import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {PipelineService} from './pipeline.service';
import {LowerCasePipe} from '@angular/common';

import {Observable} from 'rxjs';
import {NgbCalendar, NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {NgbdSortableHeaderDirective} from '../table/directives/sortable.directive';
import {CallBack} from '../domain/CallBack';
import {Columns, ColumnSearch} from '../domain/ColumnSearch';
import {FormControl} from '@angular/forms';
import {TypeSearch} from '../table/domain/TypeSearch';
import {CustomNgbDateParserFormatter} from '../../../utils/CustomNgbDateParserFormatter';
import {PipelineExport} from '../pipeline.export.service';
import {Searching} from '../table/domain/Searching';
import {GridOptions, Module} from '@ag-grid-community/core';
import {AllCommunityModules} from '@ag-grid-community/all-modules';
import * as moment from 'moment';
import {PipelineRemoveDialogComponent} from '../modal/pipeline.remove-dialog.component';
import {PipelineButtonsComponent} from './pipeline-buttons.component';

@Component({
    selector: 'jhi-pipeline',
    templateUrl: './pipeline.component.html',
    styleUrls: ['./pipeline.component.css'],
    providers: [PipelineService, LowerCasePipe]
})
export class PipelineComponent implements OnInit {

    columns = Columns;
    pointerClass: string;
    filter = new FormControl('');
    typesSearch = Array<TypeSearch>();
    modalRef: NgbModalRef;
    gridOptions: GridOptions;
    private gridApi;

    public modules: Module[] = AllCommunityModules;

    columnDefs = [
        {headerName: 'Action Date', field: 'actionDate', type: 'dateColumn'},
        {headerName: 'Campaign', field: 'campaing'},
        {headerName: 'Data List', field: 'dataList'},
        {headerName: 'Company Name', field: 'companyName'},
        {headerName: 'Contact Name', field: 'contactName'},
        {headerName: 'State', field: 'state'},
        {headerName: 'Creation Date', field: 'creationDate', type: 'dateColumn'},
        {headerName: 'Comment/Note', field: 'comment'},
        {headerName: 'Last Outcome', field: 'lastOutcome'},
        {headerName: 'Priority', field: 'priority'},
        {headerName: 'Actions', field: '', cellRenderer: 'pipelineButtonsComponent', suppressMenu: true}
    ];

    private frameworkComponents;

    @ViewChildren(NgbdSortableHeaderDirective) headers: QueryList<NgbdSortableHeaderDirective>;

    constructor(pipe: LowerCasePipe, private _modalService: NgbModal, public service: PipelineService,
                private dateFormat: CustomNgbDateParserFormatter, private calendar: NgbCalendar, private exportService: PipelineExport) {
        this.frameworkComponents = {
            pipelineButtonsComponent: PipelineButtonsComponent
        };
        this.gridOptions = <GridOptions>{
            rowHeight: 35,
            enableRangeSelection: true,
            columnDefs: this.columnDefs,
            onGridReady: (params) => {
                service.countries$.subscribe(
                    (rowData) => {
                        if (this.gridOptions.api) { // can be null when tabbing between the examples
                            this.gridOptions.api.setRowData(rowData);
                        }
                    }
                );
                this.gridApi = params.api;
            },
            pagination: true,
            paginationAutoPageSize: true,
            defaultColDef: {
                sortable: true,
                filter: true,
                resizable: true
            },
            columnTypes: {
                'dateColumn': {
                    filter: 'agDateColumnFilter',
                    filterParams: {
                        comparator: (filterLocalDateAtMidnight, cellValue) => {
                            const dateParts = moment(cellValue).format('DD/MM/YYYY').split('/');
                            const day = Number(dateParts[0]);
                            const month = Number(dateParts[1]) - 1;
                            const year = Number(dateParts[2]);
                            const cellDate = new Date(year, month, day);
                            if (cellDate < filterLocalDateAtMidnight) {
                                return -1;
                            } else if (cellDate > filterLocalDateAtMidnight) {
                                return 1;
                            } else {
                                return 0;
                            }
                        }
                    },
                    valueFormatter: (params) => {
                        return moment(params.value).format('DD/MM/YYYY');
                    }
                }
            },
            frameworkComponents: this.frameworkComponents
        };
    }

    ngOnInit() {

    }

    setPointer(pointer: string) {
        this.pointerClass = pointer;
    }

    getPdf() {
        this.exportService.pdf().subscribe((theBlob) => {
            const file = new Blob([theBlob], {type: 'application/pdf'});
            const fileURL = URL.createObjectURL(file);
            window.open(fileURL);
        });
    }

    getCsv() {
        const params = this.getParams();
        this.gridApi.exportDataAsCsv(params);
    }

    getParams() {
        return {
            suppressQuotes: '',
            columnSeparator: '',
            customHeader: ('My Pipeline')
        };
    }

    removeModal() {
        this.modalRef = this._modalService.open(PipelineRemoveDialogComponent);
        this.modalRef.componentInstance.callBack = {'data': 'its a data'};
        this.modalRef.result.then((response) => {
            console.log('Reason of ok ' + response);
        }, (reason) => {
            console.log('Reason of close ' + reason);
        });
    }

}
