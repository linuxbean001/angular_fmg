import {Routes} from '@angular/router';
import {PipelineComponent} from './pipeline.component';
import {UserRouteAccessService} from '../../../shared';

export const pipelineRoute: Routes = [
    {
        path: 'my-pipeline',
        component: PipelineComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'my-pipeline.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
