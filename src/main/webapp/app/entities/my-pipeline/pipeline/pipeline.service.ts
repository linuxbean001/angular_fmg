import {Injectable} from '@angular/core';

import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {debounceTime, switchMap, tap} from 'rxjs/operators';
import {SortDirection} from '../table/directives/sortable.directive';
import {of} from 'rxjs/observable/of';
import {CALL_BACKS} from '../domain/dummyData';
import {CallBack} from '../domain/CallBack';
import {Columns, ColumnSearch, TypeColumn} from '../domain/ColumnSearch';
import {DatePipe} from '@angular/common';
import {TypesSearch} from '../table/domain/TypeSearch';
import {CustomNgbDateParserFormatter} from '../../../utils/CustomNgbDateParserFormatter';
import {Searching} from '../table/domain/Searching';

interface SearchResult {
    callBacksList: CallBack[];
    total: number;
}

interface State {
    page: number;
    pageSize: number;
    searchTerm: Searching;
    sortColumn: string;
    sortDirection: SortDirection;
}

function compare(v1, v2) {
    return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
}

function sort(callBacksList: CallBack[], column: string, direction: string) {
    if (direction === '') {
        return callBacksList;
    } else {
        return [...callBacksList].sort((a, b) => {
            const res = compare(a[column], b[column]);
            return direction === 'asc' ? res : -res;
        });
    }
}

function searchAll(rec: CallBack, term: Searching, datePipe: DatePipe) {
    const tmp = term.val2Search.toLowerCase();
    return datePipe.transform(rec.actionDate, 'dd/MM/yyyy').toLowerCase().includes(tmp)
        || rec.campaing.toLowerCase().includes(tmp)
        || rec.dataList.toLowerCase().includes(tmp)
        || rec.companyName.toLowerCase().includes(tmp)
        || rec.contactName.toLowerCase().includes(tmp)
        || rec.state.toLowerCase().includes(tmp)
        || datePipe.transform(rec.creationDate, 'dd/MM/yyyy').toLowerCase().includes(tmp)
        || rec.priority.toLowerCase().includes(tmp)
        || rec.lastOutcome.toLowerCase().includes(tmp);
}

function searchDate(dateOrigin: Date, term: Date, datePipe: DatePipe, type2Search: TypesSearch, customDatePipe: CustomNgbDateParserFormatter) {
    if (term) {
        const tmp = datePipe.transform(term, 'dd/MM/yyyy').toLowerCase();
        switch (type2Search) {
            case TypesSearch.EQUALS:
                return datePipe.transform(dateOrigin, 'dd/MM/yyyy').toLowerCase() === tmp;
            case TypesSearch.NOTEQUALS:
                return datePipe.transform(dateOrigin, 'dd/MM/yyyy').toLowerCase() !== tmp;
            case TypesSearch.LESSTHAN:
                return term <= dateOrigin;
            case TypesSearch.GREATERTHAN:
                return term >= dateOrigin;
            case TypesSearch.SEARCH:
            default:
                return datePipe.transform(dateOrigin, 'dd/MM/yyyy').toLowerCase().includes(tmp);
        }
    }
    return true;
}

function searchStr(origin: string, str2Find: string, type2Search: TypesSearch) {
    const tmp = str2Find.toLowerCase();
    if (tmp) {
        switch (type2Search) {
            case TypesSearch.CONTAINS:
            case TypesSearch.SEARCH:
                return origin.toLowerCase().includes(tmp);
            case TypesSearch.NOTCONTAINS:
                return !origin.toLowerCase().includes(tmp);
            case TypesSearch.EQUALS:
                return origin.toLowerCase() === tmp;
            case TypesSearch.NOTEQUALS:
                return origin.toLowerCase() !== tmp;
            default:
                return origin.toLowerCase().includes(tmp);
        }
    }
    return true;
}

function search(rec: CallBack, term: Searching, datePipe: DatePipe, customDatePipe: CustomNgbDateParserFormatter) {
    switch (term.column.column) {
        case Columns.ALL:
            return searchAll(rec, term, datePipe);
        case Columns.ACTIONDATE:
            return searchDate(rec.actionDate, term.dateTime2Search, datePipe, term.type, customDatePipe);
        case Columns.CAMPING:
            return searchStr(rec.campaing, term.val2Search, term.type);
        case Columns.DATALIST:
            return searchStr(rec.dataList, term.val2Search, term.type);
        case Columns.COMPANYNAME:
            return searchStr(rec.companyName, term.val2Search, term.type);
        case Columns.CONTACTNAME:
            return searchStr(rec.contactName, term.val2Search, term.type);
        case Columns.STATE:
            return searchStr(rec.state, term.val2Search, term.type);
        case Columns.CREATIONDATE:
            return searchDate(rec.creationDate, term.dateTime2Search, datePipe, term.type, customDatePipe);
        case Columns.PRIORITY:
            return searchStr(rec.priority, term.val2Search, term.type);
        case Columns.LASTOUT:
            return searchStr(rec.lastOutcome, term.val2Search, term.type);
        default:
            return true;
    }
}

@Injectable()
export class PipelineService {

    private _loading$ = new BehaviorSubject<boolean>(true);
    private _search$ = new Subject<void>();
    private _countries$ = new BehaviorSubject<CallBack[]>([]);
    private _total$ = new BehaviorSubject<number>(0);

    private _state: State = {
        page: 1,
        pageSize: 4,
        searchTerm: new Searching('', new Date(), new ColumnSearch('', TypeColumn.STRING, Columns.ALL), TypesSearch.SEARCH),
        sortColumn: '',
        sortDirection: ''
    };

    constructor(private datePipe: DatePipe, private customDatePipe: CustomNgbDateParserFormatter) {
        this._search$.pipe(
            tap(() => this._loading$.next(true)),
            debounceTime(200),
            switchMap(() => this._search()),
            tap(() => this._loading$.next(false))
        ).subscribe((result) => {
            this._countries$.next(result.callBacksList);
            this._total$.next(result.total);
        });

        this._search$.next();
    }

    get countries$(): BehaviorSubject<CallBack[]> {
        return this._countries$;
    }

    get total$() {
        return this._total$.asObservable();
    }

    get loading$() {
        return this._loading$.asObservable();
    }

    get page() {
        return this._state.page;
    }

    get pageSize() {
        return this._state.pageSize;
    }

    get searchTerm() {
        return this._state.searchTerm;
    }

    set page(page: number) {
        this._set({page});
    }

    set pageSize(pageSize: number) {
        this._set({pageSize});
    }

    set searchTerm(searchTerm: Searching) {
        this._set({searchTerm});
    }

    set sortColumn(sortColumn: string) {
        this._set({sortColumn});
    }

    set sortDirection(sortDirection: SortDirection) {
        this._set({sortDirection});
    }

    private _set(patch: Partial<State>) {
        Object.assign(this._state, patch);
        this._search$.next();
    }

    private _search(): Observable<SearchResult> {
        const {sortColumn, sortDirection, pageSize, page, searchTerm} = this._state;

        // 1. sort
        let callBacksList = sort(CALL_BACKS, sortColumn, sortDirection);

        // 2. filter
        callBacksList = callBacksList.filter((value) => search(value, searchTerm, this.datePipe, this.customDatePipe));

        const total = callBacksList.length;

        // 3. paginate
        callBacksList = callBacksList.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
        return of({callBacksList, total});
    }

}
