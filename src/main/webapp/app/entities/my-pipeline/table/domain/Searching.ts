import {TypesSearch} from './TypeSearch';
import {ColumnSearch} from '../../domain/ColumnSearch';

export class Searching {
    constructor(public val2Search: string,
                public dateTime2Search: Date,
                public column: ColumnSearch,
                public type: TypesSearch
    ) {
    }
}
