export class TypeSearch {
    constructor(public txt: string,
                public icon: string,
                public type: TypesSearch
    ) {
    }
}

export enum TypesSearch {
    'SEARCH',
    'CONTAINS',
    'NOTCONTAINS',
    'EQUALS',
    'NOTEQUALS',
    'LESSTHAN',
    'GREATERTHAN',
    'NONE'
}
