import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TypeSearch, TypesSearch} from '../domain/TypeSearch';
import {ColumnSearch} from '../../domain/ColumnSearch';
import {Searching} from '../domain/Searching';

@Component({
    selector: 'jhi-search-column',
    templateUrl: './search-column.component.html',
    styleUrls: ['./search-column.component.css']
})
export class SearchColumnComponent implements OnInit {

    @Input() typesSearch: Array<TypeSearch>;
    @Input() column2Search: ColumnSearch;
    @Output() responseObj = new EventEmitter<Searching>();
    search: Searching;

    constructor() {
        this.search = new Searching('', new Date(), this.column2Search, TypesSearch.CONTAINS);
    }

    ngOnInit() {
        this.search.column = this.column2Search;
    }

    sendFilter() {
        this.responseObj.emit(this.search);
    }

}
