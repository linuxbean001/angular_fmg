import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager} from 'ng-jhipster';

import {OutcomeSubReason} from './outcome-sub-reason.model';
import {OutcomeSubReasonPopupService} from './outcome-sub-reason-popup.service';
import {OutcomeSubReasonService} from './outcome-sub-reason.service';

@Component({
    selector: 'jhi-outcome-sub-reason-delete-dialog',
    templateUrl: './outcome-sub-reason-delete-dialog.component.html'
})
export class OutcomeSubReasonDeleteDialogComponent {

    outcomeSubReason: OutcomeSubReason;

    constructor(
        private outcomeSubReasonService: OutcomeSubReasonService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.outcomeSubReasonService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'outcomeSubReasonListModification',
                content: 'Deleted an outcomeSubReason'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-outcome-sub-reason-delete-popup',
    template: ''
})
export class OutcomeSubReasonDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private outcomeSubReasonPopupService: OutcomeSubReasonPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.outcomeSubReasonPopupService
                .open(OutcomeSubReasonDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
