import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpResponse} from '@angular/common/http';
import {Subscription} from 'rxjs/Subscription';
import {JhiEventManager} from 'ng-jhipster';

import {OutcomeSubReason} from './outcome-sub-reason.model';
import {OutcomeSubReasonService} from './outcome-sub-reason.service';

@Component({
    selector: 'jhi-outcome-sub-reason-detail',
    templateUrl: './outcome-sub-reason-detail.component.html'
})
export class OutcomeSubReasonDetailComponent implements OnInit, OnDestroy {

    outcomeSubReason: OutcomeSubReason;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private outcomeSubReasonService: OutcomeSubReasonService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInOutcomeSubReasons();
    }

    load(id) {
        this.outcomeSubReasonService.find(id)
            .subscribe((outcomeSubReasonResponse: HttpResponse<OutcomeSubReason>) => {
                this.outcomeSubReason = outcomeSubReasonResponse.body;
            });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInOutcomeSubReasons() {
        this.eventSubscriber = this.eventManager.subscribe(
            'outcomeSubReasonListModification',
            (response) => this.load(this.outcomeSubReason.id)
        );
    }
}
