import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager} from 'ng-jhipster';

import {OutcomeSubReason} from './outcome-sub-reason.model';
import {OutcomeSubReasonPopupService} from './outcome-sub-reason-popup.service';
import {OutcomeSubReasonService} from './outcome-sub-reason.service';

@Component({
    selector: 'jhi-outcome-sub-reason-dialog',
    templateUrl: './outcome-sub-reason-dialog.component.html'
})
export class OutcomeSubReasonDialogComponent implements OnInit {

    outcomeSubReason: OutcomeSubReason;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private outcomeSubReasonService: OutcomeSubReasonService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.outcomeSubReason.id !== undefined) {
            this.subscribeToSaveResponse(
                this.outcomeSubReasonService.update(this.outcomeSubReason));
        } else {
            this.subscribeToSaveResponse(
                this.outcomeSubReasonService.create(this.outcomeSubReason));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<OutcomeSubReason>>) {
        result.subscribe((res: HttpResponse<OutcomeSubReason>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: OutcomeSubReason) {
        this.eventManager.broadcast({name: 'outcomeSubReasonListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-outcome-sub-reason-popup',
    template: ''
})
export class OutcomeSubReasonPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private outcomeSubReasonPopupService: OutcomeSubReasonPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.outcomeSubReasonPopupService
                    .open(OutcomeSubReasonDialogComponent as Component, params['id']);
            } else {
                this.outcomeSubReasonPopupService
                    .open(OutcomeSubReasonDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
