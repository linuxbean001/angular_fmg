import {Component, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {HttpResponse} from '@angular/common/http';
import {OutcomeSubReason} from './outcome-sub-reason.model';
import {OutcomeSubReasonService} from './outcome-sub-reason.service';

@Injectable()
export class OutcomeSubReasonPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private outcomeSubReasonService: OutcomeSubReasonService
    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.outcomeSubReasonService.find(id)
                    .subscribe((outcomeSubReasonResponse: HttpResponse<OutcomeSubReason>) => {
                        const outcomeSubReason: OutcomeSubReason = outcomeSubReasonResponse.body;
                        this.ngbModalRef = this.outcomeSubReasonModalRef(component, outcomeSubReason);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.outcomeSubReasonModalRef(component, new OutcomeSubReason());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    outcomeSubReasonModalRef(component: Component, outcomeSubReason: OutcomeSubReason): NgbModalRef {
        const modalRef = this.modalService.open(component, {size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.outcomeSubReason = outcomeSubReason;
        modalRef.result.then((result) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
