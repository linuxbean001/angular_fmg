import {BaseEntity} from './../../shared';

export class OutcomeSubReason implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
    ) {
    }
}
