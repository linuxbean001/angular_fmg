import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {CmaSharedModule} from '../../shared';
import {
    OutcomeSubReasonComponent,
    OutcomeSubReasonDeleteDialogComponent,
    OutcomeSubReasonDeletePopupComponent,
    OutcomeSubReasonDetailComponent,
    OutcomeSubReasonDialogComponent,
    OutcomeSubReasonPopupComponent,
    outcomeSubReasonPopupRoute,
    OutcomeSubReasonPopupService,
    OutcomeSubReasonResolvePagingParams,
    outcomeSubReasonRoute,
    OutcomeSubReasonService,
} from './';

const ENTITY_STATES = [
    ...outcomeSubReasonRoute,
    ...outcomeSubReasonPopupRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        OutcomeSubReasonComponent,
        OutcomeSubReasonDetailComponent,
        OutcomeSubReasonDialogComponent,
        OutcomeSubReasonDeleteDialogComponent,
        OutcomeSubReasonPopupComponent,
        OutcomeSubReasonDeletePopupComponent,
    ],
    entryComponents: [
        OutcomeSubReasonComponent,
        OutcomeSubReasonDialogComponent,
        OutcomeSubReasonPopupComponent,
        OutcomeSubReasonDeleteDialogComponent,
        OutcomeSubReasonDeletePopupComponent,
    ],
    providers: [
        OutcomeSubReasonService,
        OutcomeSubReasonPopupService,
        OutcomeSubReasonResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaOutcomeSubReasonModule {
}
