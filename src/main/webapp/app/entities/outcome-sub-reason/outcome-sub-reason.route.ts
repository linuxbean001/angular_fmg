import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes} from '@angular/router';
import {JhiPaginationUtil} from 'ng-jhipster';

import {UserRouteAccessService} from '../../shared';
import {OutcomeSubReasonComponent} from './outcome-sub-reason.component';
import {OutcomeSubReasonDetailComponent} from './outcome-sub-reason-detail.component';
import {OutcomeSubReasonPopupComponent} from './outcome-sub-reason-dialog.component';
import {OutcomeSubReasonDeletePopupComponent} from './outcome-sub-reason-delete-dialog.component';

@Injectable()
export class OutcomeSubReasonResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const outcomeSubReasonRoute: Routes = [
    {
        path: 'outcome-sub-reason',
        component: OutcomeSubReasonComponent,
        resolve: {
            'pagingParams': OutcomeSubReasonResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.outcomeSubReason.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'outcome-sub-reason/:id',
        component: OutcomeSubReasonDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.outcomeSubReason.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const outcomeSubReasonPopupRoute: Routes = [
    {
        path: 'outcome-sub-reason-new',
        component: OutcomeSubReasonPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.outcomeSubReason.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'outcome-sub-reason/:id/edit',
        component: OutcomeSubReasonPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.outcomeSubReason.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'outcome-sub-reason/:id/delete',
        component: OutcomeSubReasonDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.outcomeSubReason.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
