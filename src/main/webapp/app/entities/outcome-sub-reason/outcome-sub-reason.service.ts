import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';

import {OutcomeSubReason} from './outcome-sub-reason.model';
import {createRequestOption} from '../../shared';

export type EntityResponseType = HttpResponse<OutcomeSubReason>;

@Injectable()
export class OutcomeSubReasonService {

    private resourceUrl = SERVER_API_URL + 'api/outcome-sub-reasons';

    constructor(private http: HttpClient) {
    }

    create(outcomeSubReason: OutcomeSubReason): Observable<EntityResponseType> {
        const copy = this.convert(outcomeSubReason);
        return this.http.post<OutcomeSubReason>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(outcomeSubReason: OutcomeSubReason): Observable<EntityResponseType> {
        const copy = this.convert(outcomeSubReason);
        return this.http.put<OutcomeSubReason>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<OutcomeSubReason>(`${this.resourceUrl}/${id}`, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<OutcomeSubReason[]>> {
        const options = createRequestOption(req);
        return this.http.get<OutcomeSubReason[]>(this.resourceUrl, {params: options, observe: 'response'})
            .map((res: HttpResponse<OutcomeSubReason[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: OutcomeSubReason = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<OutcomeSubReason[]>): HttpResponse<OutcomeSubReason[]> {
        const jsonResponse: OutcomeSubReason[] = res.body;
        const body: OutcomeSubReason[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to OutcomeSubReason.
     */
    private convertItemFromServer(outcomeSubReason: OutcomeSubReason): OutcomeSubReason {
        const copy: OutcomeSubReason = Object.assign({}, outcomeSubReason);
        return copy;
    }

    /**
     * Convert a OutcomeSubReason to a JSON which can be sent to the server.
     */
    private convert(outcomeSubReason: OutcomeSubReason): OutcomeSubReason {
        const copy: OutcomeSubReason = Object.assign({}, outcomeSubReason);
        return copy;
    }
}
