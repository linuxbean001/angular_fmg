import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { OutcomeType } from './outcome-type.model';
import { OutcomeTypePopupService } from './outcome-type-popup.service';
import { OutcomeTypeService } from './outcome-type.service';

@Component({
    selector: 'jhi-outcome-type-delete-dialog',
    templateUrl: './outcome-type-delete-dialog.component.html'
})
export class OutcomeTypeDeleteDialogComponent {

    outcomeType: OutcomeType;

    constructor(
        private outcomeTypeService: OutcomeTypeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.outcomeTypeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'outcomeTypeListModification',
                content: 'Deleted an outcomeType',
                id,
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-outcome-type-delete-popup',
    template: ''
})
export class OutcomeTypeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private outcomeTypePopupService: OutcomeTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.outcomeTypePopupService
                .open(OutcomeTypeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
