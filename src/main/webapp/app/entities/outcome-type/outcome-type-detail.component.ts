import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { OutcomeType } from './outcome-type.model';
import { OutcomeTypeService } from './outcome-type.service';

@Component({
    selector: 'jhi-outcome-type-detail',
    templateUrl: './outcome-type-detail.component.html'
})
export class OutcomeTypeDetailComponent implements OnInit, OnDestroy {

    outcomeType: OutcomeType;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private outcomeTypeService: OutcomeTypeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInOutcomeTypes();
    }

    load(id) {
        this.outcomeTypeService.find(id)
            .subscribe((outcomeTypeResponse: HttpResponse<OutcomeType>) => {
                this.outcomeType = outcomeTypeResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInOutcomeTypes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'outcomeTypeListModification',
            (response) => this.load(this.outcomeType.id)
        );
    }
}
