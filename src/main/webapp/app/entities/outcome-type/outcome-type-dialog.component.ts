import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { OutcomeType } from './outcome-type.model';
import { OutcomeTypePopupService } from './outcome-type-popup.service';
import { OutcomeTypeService } from './outcome-type.service';

@Component({
    selector: 'jhi-outcome-type-dialog',
    templateUrl: './outcome-type-dialog.component.html'
})
export class OutcomeTypeDialogComponent implements OnInit {

    outcomeType: OutcomeType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private outcomeTypeService: OutcomeTypeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.outcomeType.id !== undefined) {
            this.subscribeToSaveResponse(
                this.outcomeTypeService.update(this.outcomeType));
        } else {
            this.subscribeToSaveResponse(
                this.outcomeTypeService.create(this.outcomeType));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<OutcomeType>>) {
        result.subscribe((res: HttpResponse<OutcomeType>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: OutcomeType) {
        this.eventManager.broadcast({ name: 'outcomeTypeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-outcome-type-popup',
    template: ''
})
export class OutcomeTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private outcomeTypePopupService: OutcomeTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.outcomeTypePopupService
                    .open(OutcomeTypeDialogComponent as Component, params['id']);
            } else {
                this.outcomeTypePopupService
                    .open(OutcomeTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
