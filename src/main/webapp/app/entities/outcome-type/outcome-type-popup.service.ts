import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { OutcomeType } from './outcome-type.model';
import { OutcomeTypeService } from './outcome-type.service';

@Injectable()
export class OutcomeTypePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private outcomeTypeService: OutcomeTypeService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.outcomeTypeService.find(id)
                    .subscribe((outcomeTypeResponse: HttpResponse<OutcomeType>) => {
                        const outcomeType: OutcomeType = outcomeTypeResponse.body;
                        this.ngbModalRef = this.outcomeTypeModalRef(component, outcomeType);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.outcomeTypeModalRef(component, new OutcomeType());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    outcomeTypeModalRef(component: Component, outcomeType: OutcomeType): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.outcomeType = outcomeType;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
