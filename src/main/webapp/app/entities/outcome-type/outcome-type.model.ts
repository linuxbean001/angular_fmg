import { BaseEntity } from './../../shared';

export class OutcomeType implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
    ) {
    }
}
