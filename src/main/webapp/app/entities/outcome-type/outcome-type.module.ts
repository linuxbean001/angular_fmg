import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CmaSharedModule } from '../../shared';
import {
    OutcomeTypeService,
    OutcomeTypePopupService,
    OutcomeTypeComponent,
    OutcomeTypeDetailComponent,
    OutcomeTypeDialogComponent,
    OutcomeTypePopupComponent,
    OutcomeTypeDeletePopupComponent,
    OutcomeTypeDeleteDialogComponent,
    outcomeTypeRoute,
    outcomeTypePopupRoute,
    OutcomeTypeResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...outcomeTypeRoute,
    ...outcomeTypePopupRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        OutcomeTypeComponent,
        OutcomeTypeDetailComponent,
        OutcomeTypeDialogComponent,
        OutcomeTypeDeleteDialogComponent,
        OutcomeTypePopupComponent,
        OutcomeTypeDeletePopupComponent,
    ],
    entryComponents: [
        OutcomeTypeComponent,
        OutcomeTypeDialogComponent,
        OutcomeTypePopupComponent,
        OutcomeTypeDeleteDialogComponent,
        OutcomeTypeDeletePopupComponent,
    ],
    providers: [
        OutcomeTypeService,
        OutcomeTypePopupService,
        OutcomeTypeResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaOutcomeTypeModule {}
