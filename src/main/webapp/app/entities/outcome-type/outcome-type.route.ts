import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { OutcomeTypeComponent } from './outcome-type.component';
import { OutcomeTypeDetailComponent } from './outcome-type-detail.component';
import { OutcomeTypePopupComponent } from './outcome-type-dialog.component';
import { OutcomeTypeDeletePopupComponent } from './outcome-type-delete-dialog.component';

@Injectable()
export class OutcomeTypeResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const outcomeTypeRoute: Routes = [
    {
        path: 'outcome-type',
        component: OutcomeTypeComponent,
        resolve: {
            'pagingParams': OutcomeTypeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.outcomeType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'outcome-type/:id',
        component: OutcomeTypeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.outcomeType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const outcomeTypePopupRoute: Routes = [
    {
        path: 'outcome-type-new',
        component: OutcomeTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.outcomeType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'outcome-type/:id/edit',
        component: OutcomeTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.outcomeType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'outcome-type/:id/delete',
        component: OutcomeTypeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.outcomeType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
