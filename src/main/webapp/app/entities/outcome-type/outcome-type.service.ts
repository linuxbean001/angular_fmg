import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { OutcomeType } from './outcome-type.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<OutcomeType>;

@Injectable()
export class OutcomeTypeService {

    private resourceUrl =  SERVER_API_URL + 'api/outcome-types';

    constructor(private http: HttpClient) { }

    create(outcomeType: OutcomeType): Observable<EntityResponseType> {
        const copy = this.convert(outcomeType);
        return this.http.post<OutcomeType>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(outcomeType: OutcomeType): Observable<EntityResponseType> {
        const copy = this.convert(outcomeType);
        return this.http.put<OutcomeType>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<OutcomeType>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<OutcomeType[]>> {
        const options = createRequestOption(req);
        return this.http.get<OutcomeType[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<OutcomeType[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: OutcomeType = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<OutcomeType[]>): HttpResponse<OutcomeType[]> {
        const jsonResponse: OutcomeType[] = res.body;
        const body: OutcomeType[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to OutcomeType.
     */
    private convertItemFromServer(outcomeType: OutcomeType): OutcomeType {
        const copy: OutcomeType = Object.assign({}, outcomeType);
        return copy;
    }

    /**
     * Convert a OutcomeType to a JSON which can be sent to the server.
     */
    private convert(outcomeType: OutcomeType): OutcomeType {
        const copy: OutcomeType = Object.assign({}, outcomeType);
        return copy;
    }
}
