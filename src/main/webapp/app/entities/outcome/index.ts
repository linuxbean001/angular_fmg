export * from './outcome.model';
export * from './outcome-popup.service';
export * from './outcome.service';
export * from './outcome-dialog.component';
export * from './outcome-delete-dialog.component';
export * from './outcome-detail.component';
export * from './outcome.component';
export * from './outcome.route';
