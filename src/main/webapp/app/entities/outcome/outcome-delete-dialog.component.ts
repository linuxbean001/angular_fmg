import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager} from 'ng-jhipster';

import {Outcome} from './outcome.model';
import {OutcomePopupService} from './outcome-popup.service';
import {OutcomeService} from './outcome.service';

@Component({
    selector: 'jhi-outcome-delete-dialog',
    templateUrl: './outcome-delete-dialog.component.html'
})
export class OutcomeDeleteDialogComponent {

    outcome: Outcome;

    constructor(
        private outcomeService: OutcomeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.outcomeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'outcomeListModification',
                content: 'Deleted an outcome',
                id
            });
            this.activeModal.close(true);
        });
    }
}

@Component({
    selector: 'jhi-outcome-delete-popup',
    template: ''
})
export class OutcomeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private outcomePopupService: OutcomePopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.outcomePopupService
                .open(OutcomeDeleteDialogComponent as Component, params['id'], null);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
