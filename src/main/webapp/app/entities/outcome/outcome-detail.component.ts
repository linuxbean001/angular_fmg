import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpResponse} from '@angular/common/http';
import {Subscription} from 'rxjs/Subscription';
import {JhiEventManager} from 'ng-jhipster';

import {Outcome} from './outcome.model';
import {OutcomeService} from './outcome.service';

@Component({
    selector: 'jhi-outcome-detail',
    templateUrl: './outcome-detail.component.html'
})
export class OutcomeDetailComponent implements OnInit, OnDestroy {

    outcome: Outcome;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private outcomeService: OutcomeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInOutcomes();
    }

    load(id) {
        this.outcomeService.find(id)
            .subscribe((outcomeResponse: HttpResponse<Outcome>) => {
                this.outcome = outcomeResponse.body;
            });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInOutcomes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'outcomeListModification',
            (response) => this.load(this.outcome.id)
        );
    }
}
