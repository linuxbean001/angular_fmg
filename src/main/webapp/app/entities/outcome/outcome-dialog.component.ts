import {Component, Directive, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';

import {
    CallPoolCycle,
    Invite,
    Outcome,
    OutcomeCategory,
    Ownership,
    OwnershipDuration,
    Parked,
    QcQa,
    YesAndNo
} from './outcome.model';
import {OutcomePopupService} from './outcome-popup.service';
import {OutcomeService} from './outcome.service';
import {ClientCampaignTemplate, ClientCampaignTemplateService} from '../client-campaign-template';
import {OutcomeType, OutcomeTypeService} from '../outcome-type';
import {ClientCampaign, ClientCampaignService} from '../client-campaign';
import {AbstractControl, FormGroup, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn} from '@angular/forms';
import {ClientCampaignSms, ClientCampaignSmsService} from '../client-campaign-sms';
import {BaseEntity, ConfirmService} from '../../shared';
import {EmailTemplate, EmailTemplateService} from '../email-template';
import {ClientList, ClientListService} from '../client-list';
import {OutcomeSubReason, OutcomeSubReasonService} from '../outcome-sub-reason';

@Component({
    selector: 'jhi-outcome-dialog',
    templateUrl: './outcome-dialog.component.html'
})
export class OutcomeDialogComponent implements OnInit {

    outcome: Outcome;
    isSaving: boolean;

    clientcampaigntemplates: ClientCampaignTemplate[];
    outcomeTypes: OutcomeType[];
    clientCampaigns: ClientCampaign[];
    emailTemplates: EmailTemplate[];
    clientLists: ClientList[] = [];

    CATEGORIES = OutcomeCategory.ITEMS;
    OWNERSHIPS = Ownership.ITEMS;
    PARKEDS = Parked.ITEMS;
    OWNERSHIPDURATIONS = OwnershipDuration.ITEMS;
    OwnershipDuration = OwnershipDuration;
    QCQAS = QcQa.ITEMS;

    YesAndNo = YesAndNo;
    CallPoolCycle = CallPoolCycle;
    Invite = Invite;

    excludeCampaignId: number;
    smss: ClientCampaignSms[] = [];
    outcomesubreasons: OutcomeSubReason[] = [];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private outcomeService: OutcomeService,
        private clientCampaignTemplateService: ClientCampaignTemplateService,
        private outcomeTypeService: OutcomeTypeService,
        private eventManager: JhiEventManager,
        private confirmService: ConfirmService,
        private clientCampaignService: ClientCampaignService,
        private clientCampaignSmsService: ClientCampaignSmsService,
        private emailTemplateService: EmailTemplateService,
        private clientListService: ClientListService,
        private outcomeSubReasonService: OutcomeSubReasonService,
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.clientCampaignTemplateService.query()
            .subscribe((res: HttpResponse<ClientCampaignTemplate[]>) => {
                this.clientcampaigntemplates = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.outcomeTypeService.query()
            .subscribe((res: HttpResponse<OutcomeType[]>) => {
                this.outcomeTypes = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.clientCampaignService.query()
            .subscribe((res: HttpResponse<ClientCampaign[]>) => {
                this.clientCampaigns = res.body
                    .filter((clientCampaign) => clientCampaign.id !== this.excludeCampaignId);
            }, (res: HttpErrorResponse) => this.onError(res.message));
        if (this.excludeCampaignId) {
            this.clientCampaignService.find(this.excludeCampaignId)
                .subscribe((res: HttpResponse<ClientCampaign>) => {
                    this.smss = res.body.smss;
                }, (res: HttpErrorResponse) => this.onError(res.message));
        } else {
            this.clientCampaignSmsService.query()
                .subscribe((res: HttpResponse<ClientCampaignSms[]>) => {
                    this.smss = res.body;
                }, (res: HttpErrorResponse) => this.onError(res.message));
        }
        this.emailTemplateService.query()
            .subscribe((res: HttpResponse<EmailTemplate[]>) => {
                this.emailTemplates = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.outcomeSubReasonService.query()
            .subscribe((res: HttpResponse<OutcomeSubReason[]>) => {
                this.outcomesubreasons = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));

        this.loadClientCampaignClientLists(this.outcome.moveToCampaignId);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        const inviteNoIdent = Invite.NO.ident;
        if (this.outcome.emailToContact === inviteNoIdent &&
            this.outcome.sms === inviteNoIdent &&
            this.outcome.calendarInvite === inviteNoIdent) {

            // TODO replace this with form validation
            // let options: ConfirmOptions = {
            //     title: "Validation",
            //     message: "At least one of these 3 selected boxes must have a value other than \"no\""
            // };
            //
            // this.confirmService.confirm(options);

            return;
        }

        this.isSaving = true;
        if (this.outcome.id !== undefined) {
            this.subscribeToSaveResponse(
                this.outcomeService.update(this.outcome));
        } else {
            this.subscribeToSaveResponse(
                this.outcomeService.create(this.outcome));
        }
    }

    subscribeToSaveResponse(result: Observable<HttpResponse<Outcome>>) {
        result.subscribe((res: HttpResponse<Outcome>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    onSaveSuccess(result: Outcome) {
        this.eventManager.broadcast({
            name: 'outcomeListModification',
            content: 'OK',
            outcome: result
        });
        this.isSaving = false;
        this.activeModal.close(result);
    }

    onSaveError() {
        this.isSaving = false;
    }

    onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackClientCampaignTemplateById(index: number, item: ClientCampaignTemplate) {
        return item.id;
    }

    trackOutcomeTypeById(index: number, item: OutcomeType) {
        return item.id;
    }

    trackClientCampaignById(index: number, item: ClientCampaign) {
        return item.id;
    }

    trackClientListById(index: number, item: ClientList) {
        return item.id;
    }

    trackById(index: number, item: BaseEntity) {
        return item.id;
    }

    moveToAnotherCampaignChange() {
        if (this.outcome.moveToAnotherCampaign !== YesAndNo.YES.ident) {
            this.outcome.moveToCampaignId = null;
            this.outcome.clientListId = null;
        }
    }

    moveToCampaignIdChange() {
        this.outcome.clientListId = null;
        this.loadClientCampaignClientLists(this.outcome.moveToCampaignId);
    }

    loadClientCampaignClientLists(campaignId: number) {
        if (!campaignId) {
            this.clientLists = [];
            return;
        }
        this.clientListService.getClientCampaignClientLists(campaignId).toPromise()
            .then((clientListsResponse) => {
                this.clientLists = clientListsResponse.body;
            });
    }

    toggleShowCampaignBookingFormQuestions() {
        this.outcome.showCampaignBookingFormQuestions = !this.outcome.showCampaignBookingFormQuestions;
    }

    addSubReasonChange(addSubReason: boolean) {
        // if (!addSubReason) {
        //     this.outcome.outcomeSubReasonId = null;
        //     this.outcome.outcomeSubReasonName = null;
        // }
    }
}

@Component({
    selector: 'jhi-outcome-popup',
    template: ''
})
export class OutcomePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private outcomePopupService: OutcomePopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            const id = params['id'];
            const excludeCampaignId = params['excludeCampaignId'];
            if (id) {
                this.outcomePopupService
                    .open(OutcomeDialogComponent as Component, id, excludeCampaignId);
            } else {
                this.outcomePopupService
                    .open(OutcomeDialogComponent as Component, excludeCampaignId);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}

@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[atLeastOneNotNo]',
    providers: [{provide: NG_VALIDATORS, useExisting: AtLeastOneNotNoValidatorDirective, multi: true}]
})
export class AtLeastOneNotNoValidatorDirective implements Validator {
    validate(control: AbstractControl): ValidationErrors {
        return atLeastOneNotNoValidator(control);
    }
}

export const atLeastOneNotNoValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
    const emailToContact = control.get('field_email_to_contact');
    const sms = control.get('field_sms');
    const calendarInvite = control.get('field_calendar_invite');

    if (!(emailToContact && sms && calendarInvite)) {
        return null;
    }

    const no = YesAndNo.NO.ident;
    if (emailToContact.value !== no || sms.value !== no || calendarInvite.value !== no) {
        return null;
    }

    return {'allHasNoValue': true};
};
