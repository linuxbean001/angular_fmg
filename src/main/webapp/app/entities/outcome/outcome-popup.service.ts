import {Component, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {HttpResponse} from '@angular/common/http';
import {Outcome} from './outcome.model';
import {OutcomeService} from './outcome.service';

@Injectable()
export class OutcomePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private outcomeService: OutcomeService
    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any, excludeCampaignId?: number): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.outcomeService.find(id)
                    .subscribe((outcomeResponse: HttpResponse<Outcome>) => {
                        const outcome: Outcome = outcomeResponse.body;
                        this.ngbModalRef = this.outcomeModalRef(component, outcome, excludeCampaignId);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.outcomeModalRef(component, new Outcome(), excludeCampaignId);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    openStandalone(component: Component, outcome: Outcome): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            this.ngbModalRef = this.outcomeModalRef(component, outcome, null);
            resolve(this.ngbModalRef);
        });
    }

    outcomeModalRef(component: Component, outcome: Outcome, excludeCampaignId?: number): NgbModalRef {
        const modalRef = this.modalService.open(component, {size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.outcome = outcome;
        modalRef.componentInstance.excludeCampaignId = Number(excludeCampaignId);
        modalRef.result.then((result) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
