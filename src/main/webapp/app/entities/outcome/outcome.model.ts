import {BaseEntity} from '../../shared';

export class Outcome implements BaseEntity {

    static showSubReasonTab(outcome: Outcome): boolean {
        return outcome.addSubReason;
    }

    static showBookingFromTab(outcome: Outcome): boolean {
        return outcome.showCampaignBookingFormQuestions;
    }

    static showContactManagementTab(outcome: Outcome): boolean {
        // Angela: This tab appears in this instance if ownership is mandatory or optional in the edit outcome
        // or if calendar invite is optional
        return outcome.ownership === Ownership.OPTIONAL.ident ||
            outcome.ownership === Ownership.MANDATORY.ident ||
            outcome.calendarInvite === Invite.OPTIONAL.ident ||
            // but if mandatory has been selected in calendar drop down -  we should see
            // a contact management tab with calendar and time (no option to hold from call pool)
            outcome.calendarInvite === Invite.MANDATORY.ident;
    }

    static showSendEmailTab(outcome: Outcome): boolean {
        return outcome.emailToContact === Invite.MANDATORY.ident ||
            outcome.emailToContact === Invite.OPTIONAL.ident;
    }

    static showSendSMSTab(outcome: Outcome): boolean {
        return outcome.sms === Invite.MANDATORY.ident ||
            outcome.sms === Invite.OPTIONAL.ident;
    }

    static showMoveTab(outcome: Outcome): boolean {
        return outcome.moveToAnotherCampaign === YesAndNo.YES.ident;
    }

    constructor(
        public id?: number,
        public name?: string,
        public templateName?: string,
        public templateId?: number,
        public addSubReason?: boolean,
        /**
         * One of the {@link CallPoolCycle#ITEMS}
         */
        public callPoolCycle?: string,
        public daysAfterCallBack?: number,
        public parked?: string,
        /**
         * One of the {@link QcQa#ITEMS}.
         */
        public qcqa?: string,
        public killRecord?: string,
        public killRecordAttempts?: number,
        public enterNurtureStream?: boolean,
        public typeName?: string,
        public typeId?: number,
        // TODO should I add this properties?
        // public emailImmediately?:boolean,
        // public emailDaily?:boolean,
        /**
         * One of the {@link OutcomeCategory#ITEMS}.
         */
        public categoryIdent?: string,
        /**
         * One of the {@link Ownership#ITEMS}.
         */
        public ownership?: string,
        public ownershipDuration?: string,
        public ownershipDurationCount?: number,
        /**
         * One of the {@link Invite#ITEMS}.
         */
        public emailToContact?: string,
        public emailTemplateId?: number,
        public emailTemplateName?: string,
        /**
         * One of the {@link Invite#ITEMS}.
         */
        public sms?: string,
        public smsId?: number,
        /**
         * One of the {@link Invite#ITEMS}.
         */
        public calendarInvite?: string,
        // ---
        public moveToAnotherCampaign?: string,
        public moveToCampaignId?: number,
        public moveToCampaignName?: string,
        public clientListId?: number,
        public clientListName?: string,
        // ---
        public clientCampaignId?: number,
        public clientCampaignName?: string,
        // ---
        public showCampaignBookingFormQuestions?: boolean,
        public order?: number,
        // public outcomeSubReasonId?: number,
        // public outcomeSubReasonName?: string,
        public linkToSchedule?: string,
    ) {
    }
}

export class OutcomeCategory {
    static NONE = new OutcomeCategory('None', 'NONE');
    static POSITIVE = new OutcomeCategory('Positive', 'POSITIVE');
    static NEGATIVE = new OutcomeCategory('Negative', 'NEGATIVE');
    static PIPELINE = new OutcomeCategory('Pipeline', 'PIPELINE');
    static PIPELINE_GT_50 = new OutcomeCategory('Pipeline > 50%', 'PIPELINE_GT_50');

    static ITEMS: OutcomeCategory[] = [
        OutcomeCategory.NONE,
        OutcomeCategory.POSITIVE,
        OutcomeCategory.NEGATIVE,
        OutcomeCategory.PIPELINE,
        OutcomeCategory.PIPELINE_GT_50,
    ];

    public constructor(
        public name: string,
        public ident: string,
    ) {
    }
}

export class Ownership {
    static OPTIONAL = new Ownership('Optional', 'OPTIONAL');
    static MANDATORY = new Ownership('Mandatory', 'MANDATORY');
    static FORBIDDEN = new Ownership('Forbidden', 'FORBIDDEN');

    static ITEMS: Ownership[] = [
        Ownership.OPTIONAL,
        Ownership.MANDATORY,
        Ownership.FORBIDDEN,
    ];

    public constructor(
        public name: string,
        public ident: string,
    ) {
    }
}

export class Parked {
    static ITEMS: Parked[] = [
        new Parked('No', 'NO'),
        new Parked('Yes (agent to set period)', 'YES'),
        new Parked('Optional (defined by agent)', 'OPTIONAL'),
    ];

    public constructor(
        public name: string,
        public ident: string,
    ) {
    }
}

export class OwnershipDuration {
    static YES = new OwnershipDuration('Yes - covers returns, reschedules & also appointment reporting', 'YES');

    static DUMP = new OwnershipDuration('Dump to call list (according to call back date)', 'DUMP');
    static TRANSFER = new OwnershipDuration('Transfer to call pool as a priority (according to call back date)', 'TRANSFER');
    static ITEMS: OwnershipDuration[] = [
        OwnershipDuration.YES,
        OwnershipDuration.DUMP,
        OwnershipDuration.TRANSFER,
    ];

    public constructor(
        public name: string,
        public ident: string,
    ) {
    }
}

export class QcQa {
    static NO = new QcQa('No', 'NO');
    static URGENT = new QcQa('Urgent', 'URGENT');
    static EYE_CHECK = new QcQa('Eye check', 'EYE_CHECK');

    static ITEMS: QcQa[] = [
        QcQa.NO,
        QcQa.URGENT,
        QcQa.EYE_CHECK,
    ];

    public constructor(
        public name: string,
        public ident: string,
    ) {
    }
}

export class YesAndNo {
    static NO = new YesAndNo('No', 'NO');
    static YES = new YesAndNo('Yes', 'YES');

    static ITEMS: YesAndNo[] = [
        YesAndNo.NO,
        YesAndNo.YES,
    ];

    public constructor(
        public name: string,
        public ident: string,
    ) {
    }
}

export class CallPoolCycle {
    static NO = new CallPoolCycle('No', 'NO');
    static NO_LIST = new CallPoolCycle('No (pushed to follow up list)', 'NO_LIST');
    static YES = new CallPoolCycle('Yes', 'YES');
    static YES_DATE = new CallPoolCycle('Yes (according to call date..)', 'YES_DATE');

    static ITEMS: CallPoolCycle[] = [
        CallPoolCycle.NO,
        CallPoolCycle.NO_LIST,
        CallPoolCycle.YES,
        CallPoolCycle.YES_DATE,
    ];

    public constructor(
        public name: string,
        public ident: string,
    ) {
    }
}

export class Invite {
    static NO = new Invite('No', 'NO');
    static MANDATORY = new Invite('Mandatory', 'MANDATORY');
    static OPTIONAL = new Invite('Optional', 'OPTIONAL');

    static ITEMS: Invite[] = [
        Invite.NO,
        Invite.MANDATORY,
        Invite.OPTIONAL,
    ];

    public constructor(
        public name: string,
        public ident: string,
    ) {
    }
}
