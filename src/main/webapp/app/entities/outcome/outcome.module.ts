import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CmaSharedModule } from '../../shared';
import {
    OutcomeService,
    OutcomePopupService,
    OutcomeComponent,
    OutcomeDetailComponent,
    OutcomeDialogComponent,
    OutcomePopupComponent,
    OutcomeDeletePopupComponent,
    OutcomeDeleteDialogComponent,
    outcomeRoute,
    outcomePopupRoute,
    OutcomeResolvePagingParams,
    AtLeastOneNotNoValidatorDirective,
} from './';

const ENTITY_STATES = [
    ...outcomeRoute,
    ...outcomePopupRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        OutcomeComponent,
        OutcomeDetailComponent,
        OutcomeDialogComponent,
        OutcomeDeleteDialogComponent,
        OutcomePopupComponent,
        OutcomeDeletePopupComponent,
        AtLeastOneNotNoValidatorDirective,
    ],
    entryComponents: [
        OutcomeComponent,
        OutcomeDialogComponent,
        OutcomePopupComponent,
        OutcomeDeleteDialogComponent,
        OutcomeDeletePopupComponent,
    ],
    providers: [
        OutcomeService,
        OutcomePopupService,
        OutcomeResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaOutcomeModule {}
