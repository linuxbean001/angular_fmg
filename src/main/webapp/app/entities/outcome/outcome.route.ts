import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { OutcomeComponent } from './outcome.component';
import { OutcomeDetailComponent } from './outcome-detail.component';
import { OutcomePopupComponent } from './outcome-dialog.component';
import { OutcomeDeletePopupComponent } from './outcome-delete-dialog.component';

@Injectable()
export class OutcomeResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const outcomeRoute: Routes = [
    {
        path: 'outcome',
        component: OutcomeComponent,
        resolve: {
            'pagingParams': OutcomeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.outcome.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'outcome/:id',
        component: OutcomeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.outcome.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const outcomePopupRoute: Routes = [
    {
        path: 'outcome-new',
        component: OutcomePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.outcome.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'outcome/:id/edit',
        component: OutcomePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.outcome.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'outcome/:id/edit/:excludeCampaignId',
        component: OutcomePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.outcome.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'outcome/:id/delete',
        component: OutcomeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.outcome.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
