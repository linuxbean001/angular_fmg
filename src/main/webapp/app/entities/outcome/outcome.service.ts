import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';

import {Outcome} from './outcome.model';
import {createRequestOption} from '../../shared';

export type EntityResponseType = HttpResponse<Outcome>;

@Injectable()
export class OutcomeService {

    private resourceUrl = SERVER_API_URL + 'api/outcomes';

    constructor(private http: HttpClient) {
    }

    create(outcome: Outcome): Observable<EntityResponseType> {
        const copy = this.convert(outcome);
        return this.http.post<Outcome>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    createMultiple(outcomes: Outcome[]): Observable<HttpResponse<Outcome[]>> {
        const copy = outcomes.map(this.convert);
        return this.http.post<Outcome[]>(this.resourceUrl + '-multiple', copy, {observe: 'response'})
            .map((res: HttpResponse<Outcome[]>) => this.convertArrayResponse(res));
    }

    update(outcome: Outcome): Observable<EntityResponseType> {
        const copy = this.convert(outcome);
        return this.http.put<Outcome>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Outcome>(`${this.resourceUrl}/${id}`, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    queryAll(): Observable<HttpResponse<Outcome[]>> {
        return this.query({size: 2000});
    }

    query(req?: any): Observable<HttpResponse<Outcome[]>> {
        const options = createRequestOption(req);
        return this.http.get<Outcome[]>(this.resourceUrl, {params: options, observe: 'response'})
            .map((res: HttpResponse<Outcome[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Outcome = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Outcome[]>): HttpResponse<Outcome[]> {
        const jsonResponse: Outcome[] = res.body;
        const body: Outcome[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Outcome.
     */
    private convertItemFromServer(outcome: Outcome): Outcome {
        const copy: Outcome = Object.assign({}, outcome);
        return copy;
    }

    /**
     * Convert a Outcome to a JSON which can be sent to the server.
     */
    private convert(outcome: Outcome): Outcome {
        const copy: Outcome = Object.assign({}, outcome);
        return copy;
    }
}
