export let multi = [
    {
        'name': 'Target',
        'series': [
            {
                'name': 10,
                'value': 60
            },
            {
                'name': 20,
                'value': 60
            },
            {
                'name': 30,
                'value': 60
            },
            {
                'name': 40,
                'value': 60
            },
            {
                'name': 50,
                'value': 60
            },
            {
                'name': 60,
                'value': 60
            },
            {
                'name': 70,
                'value': 60
            },
            {
                'name': 80,
                'value': 60
            }
        ]
    },
    {
        'name': 'Actual',
        'series': [
            {
                'name': 10,
                'value': 5
            },
            {
                'name': 17,
                'value': 20
            },
            {
                'name': 25,
                'value': 33
            },
            {
                'name': 35,
                'value': 40
            }, {
                'name': 40,
                'value': 45
            },
            {
                'name': 50,
                'value': 50
            },
            {
                'name': 60,
                'value': 65
            },
            {
                'name': 70,
                'value': 70
            },
            {
                'name': 80,
                'value': 80
            }
        ]
    }
];
