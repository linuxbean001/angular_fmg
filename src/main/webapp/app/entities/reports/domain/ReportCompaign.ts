export class ReportCompaign {
    constructor(public callPerHour?: JSON,
                public contactRate?: JSON,
                public hoursPerLead?: JSON,
                public conversionRate?: JSON,
                public conversationPerHour?: JSON,
                public leadToSales?: JSON,
                public averageSale?: JSON,
                public costumePhase?: JSON) {
    }
}
