import {NgbDate} from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date';

export class ReportSearch {
    constructor(public fromReport?: NgbDate,
                public toReport?: NgbDate,
                public campaingId?: number,
                public clientList?: number) {
    }
}
