import {Component, OnInit} from '@angular/core';
import {Searching} from '../my-pipeline/table/domain/Searching';
import {TypeSearch} from '../my-pipeline/table/domain/TypeSearch';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {ClientCampaign, ClientCampaignService} from '../client-campaign';
import {ClientList, ClientListService} from '../client-list';
import {JhiAlertService} from 'ng-jhipster';
import {ReportSearch} from './domain/ReportSearch';
import {NgbCalendar, NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import {NgbDate} from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date';
import {ReportCompaign} from './domain/ReportCompaign';

@Component({
    selector: 'jhi-reports',
    templateUrl: './reports.component.html',
    styleUrls: ['reports.component.css']
})
export class ReportsComponent implements OnInit {

    multi: ReportCompaign;
    view: any[] = [1500, 1000];
    reportSearch: ReportSearch;

    // options
    legend: boolean;
    showLabels: boolean;
    animations: boolean;
    xAxis: boolean;
    yAxis: boolean;
    showYAxisLabel: boolean;
    showXAxisLabel: boolean;
    xAxisLabel: string;
    yAxisLabel: string;
    timeline: boolean;
    dateTypesSearch = Array<TypeSearch>();
    clientcampaigns: ClientCampaign[];
    clientlists: ClientList[];

    colorScheme = {
        domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
    };

    constructor(private jhiAlertService: JhiAlertService,
                private clientCampaignService: ClientCampaignService,
                private clientListService: ClientListService,
                private calendar: NgbCalendar,
                private dateFormat: NgbDateParserFormatter) {
        this.multi = new ReportCompaign();
        this.legend = true;
        this.showLabels = true;
        this.animations = true;
        this.xAxis = true;
        this.yAxis = true;
        this.showYAxisLabel = true;
        this.showXAxisLabel = true;
        this.xAxisLabel = 'Duration';
        this.yAxisLabel = '';
        this.timeline = true;
        this.dateTypesSearch = [];
        this.reportSearch = new ReportSearch();
        this.reportSearch.toReport = this.calendar.getToday();
        this.reportSearch.fromReport = this.toNgDate(this.reduceMonth(new Date(), 30));
    }

    ngOnInit() {
        this.clientCampaignService.consoleRelatedClientCampaigns()
            .subscribe((res: HttpResponse<ClientCampaign[]>) => {
                this.clientcampaigns = res.body;
                this.loadDefaultCampaign();
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    loadDefaultCampaign() {
        if (this.clientcampaigns.length > 0) {
            this.reportSearch.campaingId = this.clientcampaigns[0].id;
            this.getListByCampign();
        }
    }

    onSelect(data): void {
        console.log('Item clicked', JSON.parse(JSON.stringify(data)));
    }

    onActivate(data): void {
        console.log('Activate', JSON.parse(JSON.stringify(data)));
    }

    onDeactivate(data): void {
        console.log('Deactivate', JSON.parse(JSON.stringify(data)));
    }

    sendSearch(searching: Searching) {
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    getListByCampign() {
        if (!this.reportSearch.campaingId) {
            this.clientlists = [];
            return;
        }
        return this.clientCampaignService.find(this.reportSearch.campaingId).subscribe((res: HttpResponse<ClientCampaign>) => {
            this.clientlists = res.body.clientLists;
            this.loadDefaultClientList();
            this.loadReports();
        }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    loadDefaultClientList() {
        if (this.clientlists.length > 0) {
            this.clientlists.unshift(new ClientList(0, 'Show For All Lists', 'Show For All Lists', 0, null, null, 0));
            this.reportSearch.clientList = this.clientlists[0].id;
        }
    }

    loadReports() {
        if (this.reportSearch.clientList && this.reportSearch.clientList > 0) {
            this.clientCampaignService
                .getReportsCampaignByIdAndClientList(this.reportSearch.campaingId, this.reportSearch.clientList)
                .subscribe((res: HttpResponse<ReportCompaign>) => {
                this.multi = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
        } else {
            this.clientCampaignService.getReportsCampaignById(this.reportSearch.campaingId).subscribe((res: HttpResponse<ReportCompaign>) => {
                this.multi = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
        }
    }

    reduceMonth(currentDate: Date, reduceDays: number) {
        currentDate.setDate(currentDate.getDate() - reduceDays);
        return currentDate;
    }

    toNgDate(theDate: Date) {
        if (theDate) {
            return new NgbDate(theDate.getFullYear(), theDate.getMonth() + 1, theDate.getDate());
        }
        return null;
    }

}
