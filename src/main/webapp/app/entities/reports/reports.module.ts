import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {NgbDateParserFormatter, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CmaHeaderConsoleModule} from '../header-console/header-console.module';
import {ReportsComponent} from './reports.component';
import {reportsRoute} from './reports.route';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {CmaPipelineModule} from '../my-pipeline/pipeline.module';
import {CustomNgbDateParserFormatter} from '../../utils/CustomNgbDateParserFormatter';

const ENTITY_STATES = [
    ...reportsRoute,
];

@NgModule({
    imports: [
        RouterModule.forChild(ENTITY_STATES),
        BrowserModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        CmaHeaderConsoleModule,
        NgxChartsModule,
        CmaPipelineModule
    ],
    declarations: [
        ReportsComponent
    ],
    entryComponents: [
        ReportsComponent
    ],
    providers: [CustomNgbDateParserFormatter, {
        provide: NgbDateParserFormatter,
        useClass: CustomNgbDateParserFormatter
    }],
    exports: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaReportsModule {
}
