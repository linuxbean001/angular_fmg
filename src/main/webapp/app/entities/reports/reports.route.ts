import {Routes} from '@angular/router';
import {ReportsComponent} from './reports.component';
import {UserRouteAccessService} from '../../shared';

export const reportsRoute: Routes = [
    {
        path: 'reports',
        component: ReportsComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_PORTAL_USER'],
            pageTitle: 'reports.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
