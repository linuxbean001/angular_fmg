import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SurveyQuestionField } from './survey-question-field.model';
import { SurveyQuestionFieldPopupService } from './survey-question-field-popup.service';
import { SurveyQuestionFieldService } from './survey-question-field.service';

@Component({
    selector: 'jhi-survey-question-field-delete-dialog',
    templateUrl: './survey-question-field-delete-dialog.component.html'
})
export class SurveyQuestionFieldDeleteDialogComponent {

    surveyQuestionField: SurveyQuestionField;

    constructor(
        private surveyQuestionFieldService: SurveyQuestionFieldService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.surveyQuestionFieldService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'surveyQuestionFieldListModification',
                content: 'Deleted an surveyQuestionField',
                id,
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-survey-question-field-delete-popup',
    template: ''
})
export class SurveyQuestionFieldDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private surveyQuestionFieldPopupService: SurveyQuestionFieldPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.surveyQuestionFieldPopupService
                .open(SurveyQuestionFieldDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
