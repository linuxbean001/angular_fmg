import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { SurveyQuestionField } from './survey-question-field.model';
import { SurveyQuestionFieldService } from './survey-question-field.service';

@Component({
    selector: 'jhi-survey-question-field-detail',
    templateUrl: './survey-question-field-detail.component.html'
})
export class SurveyQuestionFieldDetailComponent implements OnInit, OnDestroy {

    surveyQuestionField: SurveyQuestionField;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private surveyQuestionFieldService: SurveyQuestionFieldService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSurveyQuestionFields();
    }

    load(id) {
        this.surveyQuestionFieldService.find(id)
            .subscribe((surveyQuestionFieldResponse: HttpResponse<SurveyQuestionField>) => {
                this.surveyQuestionField = surveyQuestionFieldResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSurveyQuestionFields() {
        this.eventSubscriber = this.eventManager.subscribe(
            'surveyQuestionFieldListModification',
            (response) => this.load(this.surveyQuestionField.id)
        );
    }
}
