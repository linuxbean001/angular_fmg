import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';

import {SurveyQuestionField} from './survey-question-field.model';
import {SurveyQuestionFieldPopupService} from './survey-question-field-popup.service';
import {SurveyQuestionFieldService} from './survey-question-field.service';
import {SurveyQuestion, SurveyQuestionService} from '../survey-question';

@Component({
    selector: 'jhi-survey-question-field-dialog',
    templateUrl: './survey-question-field-dialog.component.html'
})
export class SurveyQuestionFieldDialogComponent implements OnInit {

    surveyQuestionField: SurveyQuestionField;
    isSaving: boolean;

    surveyquestions: SurveyQuestion[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private surveyQuestionFieldService: SurveyQuestionFieldService,
        private surveyQuestionService: SurveyQuestionService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.surveyQuestionService.query()
            .subscribe((res: HttpResponse<SurveyQuestion[]>) => { this.surveyquestions = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.surveyQuestionField.id !== undefined) {
            this.subscribeToSaveResponse(
                this.surveyQuestionFieldService.update(this.surveyQuestionField));
        } else {
            this.subscribeToSaveResponse(
                this.surveyQuestionFieldService.create(this.surveyQuestionField));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<SurveyQuestionField>>) {
        result.subscribe((res: HttpResponse<SurveyQuestionField>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: SurveyQuestionField) {
        this.eventManager.broadcast({
            name: 'surveyQuestionFieldListModification',
            content: 'OK',
            surveyQuestionField: result,
        });
        this.isSaving = false;
        this.activeModal.close(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackSurveyQuestionById(index: number, item: SurveyQuestion) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-survey-question-field-popup',
    template: ''
})
export class SurveyQuestionFieldPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private surveyQuestionFieldPopupService: SurveyQuestionFieldPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.surveyQuestionFieldPopupService
                    .open(SurveyQuestionFieldDialogComponent as Component, params['id']);
            } else {
                this.surveyQuestionFieldPopupService
                    .open(SurveyQuestionFieldDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
