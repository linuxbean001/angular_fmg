import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { SurveyQuestionField } from './survey-question-field.model';
import { SurveyQuestionFieldService } from './survey-question-field.service';

@Injectable()
export class SurveyQuestionFieldPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private surveyQuestionFieldService: SurveyQuestionFieldService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.surveyQuestionFieldService.find(id)
                    .subscribe((surveyQuestionFieldResponse: HttpResponse<SurveyQuestionField>) => {
                        const surveyQuestionField: SurveyQuestionField = surveyQuestionFieldResponse.body;
                        this.ngbModalRef = this.surveyQuestionFieldModalRef(component, surveyQuestionField);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.surveyQuestionFieldModalRef(component, new SurveyQuestionField());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    surveyQuestionFieldModalRef(component: Component, surveyQuestionField: SurveyQuestionField): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.surveyQuestionField = surveyQuestionField;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
