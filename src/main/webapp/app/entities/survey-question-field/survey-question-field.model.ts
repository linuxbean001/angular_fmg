import {BaseEntity} from './../../shared';

export class SurveyQuestionField implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public dataType?: string,
        public category?: string,
        public prePopulationData?: string,
        public isVisible?: boolean,
        public isRequired?: boolean,
        public surveyQuestionName?: string,
        public surveyQuestionId?: number,
    ) {
        this.isVisible = false;
        this.isRequired = false;
    }
}

export class SurveyQuestionFieldDataType {
    static SHORT_TEXT = new SurveyQuestionFieldDataType('Short text', 'SHORT_TEX');
    static LONG_TEXT = new SurveyQuestionFieldDataType('Long text', 'LONG_TEXT');
    static DROPDOWN = new SurveyQuestionFieldDataType('Dropdown', 'DROPDOWN');
    static NON_EDITABLE = new SurveyQuestionFieldDataType('Non-editable', 'NON_EDITABLE');

    static ITEMS: SurveyQuestionFieldDataType[] = [
        SurveyQuestionFieldDataType.SHORT_TEXT,
        new SurveyQuestionFieldDataType('Number', 'NUMBER'),
        new SurveyQuestionFieldDataType('Checkbox', 'CHECKBOX'),
        SurveyQuestionFieldDataType.DROPDOWN,
        new SurveyQuestionFieldDataType('Date', 'DATE'),
        new SurveyQuestionFieldDataType('Time', 'TIME'),
        SurveyQuestionFieldDataType.LONG_TEXT,
        SurveyQuestionFieldDataType.NON_EDITABLE,
    ];

    public constructor(
        public name: string,
        public ident: string,
    ) {
    }
}

export class SurveyQuestionFieldCategory {
    static ITEMS: SurveyQuestionFieldCategory[] = [
        new SurveyQuestionFieldCategory('None', 'NONE'),
        new SurveyQuestionFieldCategory('Agents', 'AGENTS'),
        new SurveyQuestionFieldCategory('Contacts', 'CONTACTS'),
        new SurveyQuestionFieldCategory('Sites', 'SITES'),
    ];

    public constructor(
        public name: string,
        public ident: string,
    ) {
    }
}
