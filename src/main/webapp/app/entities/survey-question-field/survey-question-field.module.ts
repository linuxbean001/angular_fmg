import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CmaSharedModule } from '../../shared';
import {
    SurveyQuestionFieldService,
    SurveyQuestionFieldPopupService,
    SurveyQuestionFieldComponent,
    SurveyQuestionFieldDetailComponent,
    SurveyQuestionFieldDialogComponent,
    SurveyQuestionFieldPopupComponent,
    SurveyQuestionFieldDeletePopupComponent,
    SurveyQuestionFieldDeleteDialogComponent,
    surveyQuestionFieldRoute,
    surveyQuestionFieldPopupRoute,
    SurveyQuestionFieldResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...surveyQuestionFieldRoute,
    ...surveyQuestionFieldPopupRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        SurveyQuestionFieldComponent,
        SurveyQuestionFieldDetailComponent,
        SurveyQuestionFieldDialogComponent,
        SurveyQuestionFieldDeleteDialogComponent,
        SurveyQuestionFieldPopupComponent,
        SurveyQuestionFieldDeletePopupComponent,
    ],
    entryComponents: [
        SurveyQuestionFieldComponent,
        SurveyQuestionFieldDialogComponent,
        SurveyQuestionFieldPopupComponent,
        SurveyQuestionFieldDeleteDialogComponent,
        SurveyQuestionFieldDeletePopupComponent,
    ],
    providers: [
        SurveyQuestionFieldService,
        SurveyQuestionFieldPopupService,
        SurveyQuestionFieldResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaSurveyQuestionFieldModule {}
