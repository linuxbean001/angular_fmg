import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { SurveyQuestionFieldComponent } from './survey-question-field.component';
import { SurveyQuestionFieldDetailComponent } from './survey-question-field-detail.component';
import { SurveyQuestionFieldPopupComponent } from './survey-question-field-dialog.component';
import { SurveyQuestionFieldDeletePopupComponent } from './survey-question-field-delete-dialog.component';

@Injectable()
export class SurveyQuestionFieldResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const surveyQuestionFieldRoute: Routes = [
    {
        path: 'survey-question-field',
        component: SurveyQuestionFieldComponent,
        resolve: {
            'pagingParams': SurveyQuestionFieldResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.surveyQuestionField.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'survey-question-field/:id',
        component: SurveyQuestionFieldDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.surveyQuestionField.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const surveyQuestionFieldPopupRoute: Routes = [
    {
        path: 'survey-question-field-new',
        component: SurveyQuestionFieldPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.surveyQuestionField.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'survey-question-field/:id/edit',
        component: SurveyQuestionFieldPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.surveyQuestionField.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'survey-question-field/:id/delete',
        component: SurveyQuestionFieldDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.surveyQuestionField.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
