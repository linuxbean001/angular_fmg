import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { SurveyQuestionField } from './survey-question-field.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<SurveyQuestionField>;

@Injectable()
export class SurveyQuestionFieldService {

    private resourceUrl =  SERVER_API_URL + 'api/survey-question-fields';

    constructor(private http: HttpClient) { }

    create(surveyQuestionField: SurveyQuestionField): Observable<EntityResponseType> {
        const copy = this.convert(surveyQuestionField);
        return this.http.post<SurveyQuestionField>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(surveyQuestionField: SurveyQuestionField): Observable<EntityResponseType> {
        const copy = this.convert(surveyQuestionField);
        return this.http.put<SurveyQuestionField>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<SurveyQuestionField>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<SurveyQuestionField[]>> {
        const options = createRequestOption(req);
        return this.http.get<SurveyQuestionField[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<SurveyQuestionField[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: SurveyQuestionField = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<SurveyQuestionField[]>): HttpResponse<SurveyQuestionField[]> {
        const jsonResponse: SurveyQuestionField[] = res.body;
        const body: SurveyQuestionField[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to SurveyQuestionField.
     */
    private convertItemFromServer(surveyQuestionField: SurveyQuestionField): SurveyQuestionField {
        const copy: SurveyQuestionField = Object.assign({}, surveyQuestionField);
        return copy;
    }

    /**
     * Convert a SurveyQuestionField to a JSON which can be sent to the server.
     */
    private convert(surveyQuestionField: SurveyQuestionField): SurveyQuestionField {
        const copy: SurveyQuestionField = Object.assign({}, surveyQuestionField);
        return copy;
    }
}
