import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';

import {SurveyQuestion} from './survey-question.model';
import {SurveyQuestionPopupService} from './survey-question-popup.service';
import {SurveyQuestionService} from './survey-question.service';
import {ClientCampaign, ClientCampaignService} from '../client-campaign';
import {
    SurveyQuestionField,
    SurveyQuestionFieldCategory,
    SurveyQuestionFieldDataType,
    SurveyQuestionFieldService
} from '../survey-question-field';
import {ConfirmService} from '../../shared';

@Component({
    selector: 'jhi-survey-question-dialog',
    templateUrl: './survey-question-dialog.component.html'
})
export class SurveyQuestionDialogComponent implements OnInit {

    surveyQuestion: SurveyQuestion;
    isSaving: boolean;

    SurveyQuestionFieldDataType: typeof SurveyQuestionFieldDataType = SurveyQuestionFieldDataType;
    SurveyQuestionFieldCategory: typeof SurveyQuestionFieldCategory = SurveyQuestionFieldCategory;

    clientCampaigns: ClientCampaign[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private surveyQuestionService: SurveyQuestionService,
        private surveyQuestionFieldService: SurveyQuestionFieldService,
        private eventManager: JhiEventManager,
        private confirmService: ConfirmService,
        private clientCampaignService: ClientCampaignService,
    ) {

    }

    ngOnInit() {
        this.isSaving = false;
        this.clientCampaignService.query()
            .subscribe((res: HttpResponse<ClientCampaign[]>) => {
                this.clientCampaigns = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.surveyQuestion.id !== undefined) {
            this.subscribeToSaveResponse(
                this.surveyQuestionService.update(this.surveyQuestion));
        } else {
            this.subscribeToSaveResponse(
                this.surveyQuestionService.create(this.surveyQuestion));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<SurveyQuestion>>) {
        result.subscribe((res: HttpResponse<SurveyQuestion>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: SurveyQuestion) {
        this.eventManager.broadcast({
            name: 'surveyQuestionListModification',
            content: 'OK',
            surveyQuestion: result,
        });
        this.isSaving = false;
        this.activeModal.close(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackClientCampaignById(index: number, item: ClientCampaign) {
        return item.id;
    }

    createNewField() {
        const surveyQuestionField = new SurveyQuestionField();
        this.surveyQuestion.fields.push(surveyQuestionField);
        // this.clientCampaignScriptPopupService.openStandalone(ClientCampaignScriptDialogComponent as Component, surveyQuestionField)
        //     .then((ngbModalRef: NgbModalRef) => {
        //         ngbModalRef.result
        //             .then((sq) => {
        //                 const id = this._wizardState.clientCampaign.id;
        //                 if (id) {
        //                     this.clientCampaignService.addSurveyQuestion(id, sq.id).toPromise();
        //                 }
        //                 this._wizardState.clientCampaign.surveyQuestions.push(sq);
        //             });
        //     });
    }

    removeField(item: SurveyQuestionField) {
        this.removeFieldInternal(item);

        // const options = {
        //     title: 'Confirmation',
        //     message: `Are you sure you would like to remove field ${item.name ? '"' + item.name + '"' : 'Empty label'}?`
        // };
        //
        // // trying to append into current modal window
        // const modalOptions = {container: 'ngb-modal-window'};
        // this.confirmService.confirm(options, modalOptions)
        //     .then((result) => this.removeFieldInternal(item));
        // // .catch(reason => {
        // // });
    }

    removeFieldInternal(item: SurveyQuestionField) {
        const fields = this.surveyQuestion.fields;
        fields.splice(fields.indexOf(item), 1);

        // we don't delete immediately, only at a question save process
        // if (item.id) {
        //     this.surveyQuestionFieldService.delete(item.id).toPromise();
        // }
    }
}

@Component({
    selector: 'jhi-survey-question-popup',
    template: ''
})
export class SurveyQuestionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private surveyQuestionPopupService: SurveyQuestionPopupService,
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            // const displayCampaign = params['displayCampaign'] === undefined ? true : params['displayCampaign'] === 'true';

            if (params['id']) {
                this.surveyQuestionPopupService
                    .open(SurveyQuestionDialogComponent as Component, params['id']);
            } else {
                this.surveyQuestionPopupService
                    .open(SurveyQuestionDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
