import {BaseEntity} from './../../shared';
import {SurveyQuestionField} from '../survey-question-field';

export class SurveyQuestion implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public questionText?: string,
        public order?: number,
        public isVisible?: boolean,
        public fields: SurveyQuestionField[] = [],
        public clientCampaignName?: string,
        public clientCampaignId?: number,
    ) {
        this.isVisible = true;
    }
}
