import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { UserCompany } from './user-company.model';
import { UserCompanyPopupService } from './user-company-popup.service';
import { UserCompanyService } from './user-company.service';

@Component({
    selector: 'jhi-user-company-delete-dialog',
    templateUrl: './user-company-delete-dialog.component.html'
})
export class UserCompanyDeleteDialogComponent {

    userCompany: UserCompany;

    constructor(
        private userCompanyService: UserCompanyService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.userCompanyService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'userCompanyListModification',
                content: 'Deleted an userCompany',
                id,
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-user-company-delete-popup',
    template: ''
})
export class UserCompanyDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private userCompanyPopupService: UserCompanyPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.userCompanyPopupService
                .open(UserCompanyDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
