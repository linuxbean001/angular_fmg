import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { UserCompany } from './user-company.model';
import { UserCompanyService } from './user-company.service';

@Component({
    selector: 'jhi-user-company-detail',
    templateUrl: './user-company-detail.component.html'
})
export class UserCompanyDetailComponent implements OnInit, OnDestroy {

    userCompany: UserCompany;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private userCompanyService: UserCompanyService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInUserCompanies();
    }

    load(id) {
        this.userCompanyService.find(id)
            .subscribe((userCompanyResponse: HttpResponse<UserCompany>) => {
                this.userCompany = userCompanyResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInUserCompanies() {
        this.eventSubscriber = this.eventManager.subscribe(
            'userCompanyListModification',
            (response) => this.load(this.userCompany.id)
        );
    }
}
