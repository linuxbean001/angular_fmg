import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { UserCompany } from './user-company.model';
import { UserCompanyPopupService } from './user-company-popup.service';
import { UserCompanyService } from './user-company.service';

@Component({
    selector: 'jhi-user-company-dialog',
    templateUrl: './user-company-dialog.component.html'
})
export class UserCompanyDialogComponent implements OnInit {

    userCompany: UserCompany;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private userCompanyService: UserCompanyService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.userCompany.id !== undefined) {
            this.subscribeToSaveResponse(
                this.userCompanyService.update(this.userCompany));
        } else {
            this.subscribeToSaveResponse(
                this.userCompanyService.create(this.userCompany));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<UserCompany>>) {
        result.subscribe((res: HttpResponse<UserCompany>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: UserCompany) {
        this.eventManager.broadcast({ name: 'userCompanyListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-user-company-popup',
    template: ''
})
export class UserCompanyPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private userCompanyPopupService: UserCompanyPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.userCompanyPopupService
                    .open(UserCompanyDialogComponent as Component, params['id']);
            } else {
                this.userCompanyPopupService
                    .open(UserCompanyDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
