import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { UserCompany } from './user-company.model';
import { UserCompanyService } from './user-company.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-user-company',
    templateUrl: './user-company.component.html'
})
export class UserCompanyComponent implements OnInit, OnDestroy {
userCompanies: UserCompany[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private userCompanyService: UserCompanyService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.userCompanyService.query().subscribe(
            (res: HttpResponse<UserCompany[]>) => {
                this.userCompanies = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInUserCompanies();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: UserCompany) {
        return item.id;
    }
    registerChangeInUserCompanies() {
        this.eventSubscriber = this.eventManager.subscribe('userCompanyListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
