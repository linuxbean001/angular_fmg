import {BaseEntity} from '../../shared';

export class UserCompany implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
    ) {
    }
}
