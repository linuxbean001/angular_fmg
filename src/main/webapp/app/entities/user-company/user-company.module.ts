import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CmaSharedModule } from '../../shared';
import {
    UserCompanyService,
    UserCompanyPopupService,
    UserCompanyComponent,
    UserCompanyDetailComponent,
    UserCompanyDialogComponent,
    UserCompanyPopupComponent,
    UserCompanyDeletePopupComponent,
    UserCompanyDeleteDialogComponent,
    userCompanyRoute,
    userCompanyPopupRoute,
} from './';

const ENTITY_STATES = [
    ...userCompanyRoute,
    ...userCompanyPopupRoute,
];

@NgModule({
    imports: [
        CmaSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        UserCompanyComponent,
        UserCompanyDetailComponent,
        UserCompanyDialogComponent,
        UserCompanyDeleteDialogComponent,
        UserCompanyPopupComponent,
        UserCompanyDeletePopupComponent,
    ],
    entryComponents: [
        UserCompanyComponent,
        UserCompanyDialogComponent,
        UserCompanyPopupComponent,
        UserCompanyDeleteDialogComponent,
        UserCompanyDeletePopupComponent,
    ],
    providers: [
        UserCompanyService,
        UserCompanyPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaUserCompanyModule {}
