import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { UserCompanyComponent } from './user-company.component';
import { UserCompanyDetailComponent } from './user-company-detail.component';
import { UserCompanyPopupComponent } from './user-company-dialog.component';
import { UserCompanyDeletePopupComponent } from './user-company-delete-dialog.component';

export const userCompanyRoute: Routes = [
    {
        path: 'user-company',
        component: UserCompanyComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.userCompany.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'user-company/:id',
        component: UserCompanyDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.userCompany.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const userCompanyPopupRoute: Routes = [
    {
        path: 'user-company-new',
        component: UserCompanyPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.userCompany.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'user-company/:id/edit',
        component: UserCompanyPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.userCompany.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'user-company/:id/delete',
        component: UserCompanyDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'cmaApp.userCompany.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
