import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';

import {UserCompany} from './user-company.model';
import {createRequestOption} from '../../shared';

export type EntityResponseType = HttpResponse<UserCompany>;

@Injectable()
export class UserCompanyService {

    private resourceUrl = SERVER_API_URL + 'api/user-companies';

    constructor(private http: HttpClient) {
    }

    create(userCompany: UserCompany): Observable<EntityResponseType> {
        const copy = this.convert(userCompany);
        return this.http.post<UserCompany>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(userCompany: UserCompany): Observable<EntityResponseType> {
        const copy = this.convert(userCompany);
        return this.http.put<UserCompany>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<UserCompany>(`${this.resourceUrl}/${id}`, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<UserCompany[]>> {
        const options = createRequestOption(req);
        return this.http.get<UserCompany[]>(this.resourceUrl, {params: options, observe: 'response'})
            .map((res: HttpResponse<UserCompany[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: UserCompany = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<UserCompany[]>): HttpResponse<UserCompany[]> {
        const jsonResponse: UserCompany[] = res.body;
        const body: UserCompany[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to UserCompany.
     */
    private convertItemFromServer(userCompany: UserCompany): UserCompany {
        const copy: UserCompany = Object.assign({}, userCompany);
        return copy;
    }

    /**
     * Convert a UserCompany to a JSON which can be sent to the server.
     */
    private convert(userCompany: UserCompany): UserCompany {
        const copy: UserCompany = Object.assign({}, userCompany);
        return copy;
    }
}
