export class EmailSmsModel {
    constructor(public campaingName?: string,
                public listName?: string,
                public companyName?: string,
                public contact?: string,
                public mobileNumber?: string,
                public emailAddress?: string,
                public dateEmail?: Date,
                public timeEmail?: Date,
                public agentName?: string,
                public transaction?: string,
                public typeEmail?: string,
                public template?: string,
                public noteEmail?: string,
                public viewEmail?: string) {
    }
}
