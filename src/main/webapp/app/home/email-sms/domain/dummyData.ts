import {EmailSmsModel} from './EmailSmsModel';

export const EMAIL_SMS_DATA: EmailSmsModel[] = [
    {
        'campaingName': 'Just do It',
        'listName': 'Just do It',
        'companyName': 'Microsoft',
        'contact': 'Bob Fish',
        'mobileNumber': '1234567',
        'emailAddress': 'data@test.com',
        'dateEmail': new Date(1991, 11, 22),
        'timeEmail': new Date(1991, 11, 22, 22, 31, 12),
        'agentName': 'Roy Toel',
        'transaction': 'DM Contacted',
        'typeEmail': 'Email',
        'template': 'Request fot info',
        'noteEmail': 'Nice product i liked this for use with my family',
        'viewEmail': 'Dear Bob, we are so happy you choice us to manage your account'
    },
    {
        'campaingName': 'Francis Camp',
        'listName': 'Client Exclusive',
        'companyName': 'Virgin SA',
        'contact': 'Jhon Mackensi',
        'mobileNumber': '13425672',
        'emailAddress': 'aemail@virgin.com',
        'dateEmail': new Date(2013, 12, 14),
        'timeEmail': new Date(1991, 11, 22, 15, 2, 1),
        'agentName': 'Charles Darwin',
        'transaction': 'Transactionable',
        'typeEmail': 'Outlook',
        'template': 'Custom template worked',
        'noteEmail': 'Nice product i liked this for use with my family',
        'viewEmail': 'Dear Bob, we are so happy you choice us to manage your account'
    },
    {
        'campaingName': 'Can you make',
        'listName': 'Making',
        'companyName': 'Apple',
        'contact': 'Steve Jobs',
        'mobileNumber': '3334123',
        'emailAddress': 'person@test.com',
        'dateEmail': new Date(2019, 9, 22),
        'timeEmail': new Date(1991, 11, 22, 12, 43, 52),
        'agentName': 'Sam Raimi',
        'transaction': 'Do IT',
        'typeEmail': 'GMAIL',
        'template': 'it a custom template maked for us',
        'noteEmail': 'Nice product i liked this for use with my family',
        'viewEmail': 'Dear Bob, we are so happy you choice us to manage your account'
    }
];
