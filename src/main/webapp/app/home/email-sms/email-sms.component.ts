import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {NgbdSortableHeaderDirective} from '../../entities/my-pipeline/table/directives/sortable.directive';
import {ColumnSearch} from '../../entities/my-pipeline/domain/ColumnSearch';
import {TypeSearch} from '../../entities/my-pipeline/table/domain/TypeSearch';
import {Observable} from 'rxjs';
import {EmailSmsModel} from './domain/EmailSmsModel';
import {FormControl} from '@angular/forms';
import {PipelineCommentDialogComponent} from '../../entities/my-pipeline/modal/pipeline.comment-dialog.component';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {EmailSmsService} from './email-sms.service';
import {EmailSmsExport} from './email-sms.export.service';
import {GridOptions, Module} from '@ag-grid-community/core';
import {AllCommunityModules} from '@ag-grid-community/all-modules';
import * as moment from 'moment';

@Component({
    selector: 'jhi-email-sms',
    templateUrl: './email-sms.component.html',
    styleUrls: ['email-sms.component.css']
})
export class EmailSmsComponent implements OnInit {

    filter = new FormControl('');
    modalRef: NgbModalRef;
    gridOptions: GridOptions;
    private gridApi;

    public modules: Module[] = AllCommunityModules;

    columnDefs = [
        {headerName: 'Campaign Name', field: 'campaingName'},
        {headerName: 'List Name', field: 'listName'},
        {headerName: 'Company Name', field: 'companyName'},
        {headerName: 'Contact', field: 'contact'},
        {headerName: 'Mobile Number', field: 'mobileNumber'},
        {headerName: 'Email Address', field: 'emailAddress'},
        {
            headerName: 'Date',
            field: 'date',
            filter: 'agDateColumnFilter',
            valueFormatter: (params) => {
                return moment(params.value).format('DD/MM/YYYY');
            },
            filterParams: {
                comparator: (filterLocalDateAtMidnight, cellValue) => {
                    const dateParts = moment(cellValue).format('DD/MM/YYYY').split('/');
                    const day = Number(dateParts[0]);
                    const month = Number(dateParts[1]) - 1;
                    const year = Number(dateParts[2]);
                    const cellDate = new Date(year, month, day);
                    if (cellDate < filterLocalDateAtMidnight) {
                        return -1;
                    } else if (cellDate > filterLocalDateAtMidnight) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            }
        },
        {
            headerName: 'Time',
            field: 'time',
            filter: 'agDateColumnFilter',
            valueFormatter: (params) => {
                return moment(params.value).format('DD/MM/YYYY');
            },
            filterParams: {
                comparator: (filterLocalDateAtMidnight, cellValue) => {
                    const dateParts = moment(cellValue).format('DD/MM/YYYY').split('/');
                    const day = Number(dateParts[0]);
                    const month = Number(dateParts[1]) - 1;
                    const year = Number(dateParts[2]);
                    const cellDate = new Date(year, month, day);
                    if (cellDate < filterLocalDateAtMidnight) {
                        return -1;
                    } else if (cellDate > filterLocalDateAtMidnight) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            }
        },
        {headerName: 'Agent Name', field: 'agentName'},
        {headerName: 'Transaction', field: 'transaction'},
        {headerName: 'Type', field: 'typeEmail'},
        {headerName: 'Template', field: 'template'},
        {headerName: 'Note', field: 'noteEmail'},
        {headerName: 'View', field: 'viewEmail'},
    ];

    @ViewChildren(NgbdSortableHeaderDirective) headers: QueryList<NgbdSortableHeaderDirective>;

    constructor(private _modalService: NgbModal, public service: EmailSmsService, private exportService: EmailSmsExport) {
        this.gridOptions = <GridOptions>{
            enableRangeSelection: true,
            columnDefs: this.columnDefs,
            onGridReady: (params) => {
                service.emailSms$.subscribe(
                    (rowData) => {
                        if (this.gridOptions.api) { // can be null when tabbing between the examples
                            this.gridOptions.api.setRowData(rowData);
                        }
                    }
                );
                this.gridApi = params.api;
            },
            pagination: true,
            paginationAutoPageSize: true,
            defaultColDef: {
                sortable: true,
                filter: true,
                resizable: true
            }
        };
    }

    ngOnInit() {
    }

    commentModal(comment: string) {
        this.modalRef = this._modalService.open(PipelineCommentDialogComponent);
        this.modalRef.componentInstance.message = comment;
        this.modalRef.result.then((response) => {
            console.log('Reason of ok ' + response);
        }, (reason) => {
            console.log('Reason of close ' + reason);
        });
    }

    getPdf() {
        this.exportService.pdf().subscribe((theBlob) => {
            const file = new Blob([theBlob], {type: 'application/pdf'});
            const fileURL = URL.createObjectURL(file);
            window.open(fileURL);
        });
    }

    getCsv() {
        const params = this.getParams();
        this.gridApi.exportDataAsCsv(params);
    }

    getParams() {
        return {
            suppressQuotes: '',
            columnSeparator: '',
            customHeader: ('Email and SMS log')
        };
    }

}
