import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {NgbDateParserFormatter, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CustomNgbDateParserFormatter} from '../../utils/CustomNgbDateParserFormatter';
import {EmailSmsComponent} from './email-sms.component';
import {EmailSmsService} from './email-sms.service';
import {CmaHeaderConsoleModule} from '../../entities/header-console/header-console.module';
import {emailSmsRoute} from './email-sms.route';
import {CmaPipelineModule} from '../../entities/my-pipeline/pipeline.module';
import {EmailSmsExport} from './email-sms.export.service';
import {AgGridModule} from '@ag-grid-community/angular';
import {TransactionReportComponent} from '../transaction-report/transaction-report.component';
import {LoadingOverlayComponent} from '../../utils/LoadingOverlayComponent';

const ENTITY_STATES = [
    ...emailSmsRoute,
];

@NgModule({
    imports: [
        RouterModule.forChild(ENTITY_STATES),
        BrowserModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        CmaHeaderConsoleModule,
        CmaPipelineModule,
        AgGridModule.withComponents(
            [
                EmailSmsComponent
            ]
        )
    ],
    declarations: [
        EmailSmsComponent
    ],
    entryComponents: [
        EmailSmsComponent,
    ],
    providers: [EmailSmsService, EmailSmsExport, CustomNgbDateParserFormatter, {
        provide: NgbDateParserFormatter,
        useClass: CustomNgbDateParserFormatter
    }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaEmailSmsModule {
}
