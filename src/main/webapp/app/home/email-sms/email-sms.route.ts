import {Routes} from '@angular/router';
import {EmailSmsComponent} from './email-sms.component';
import {UserRouteAccessService} from '../../shared';

export const emailSmsRoute: Routes = [
    {
        path: 'email-sms',
        component: EmailSmsComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'email-sms.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
