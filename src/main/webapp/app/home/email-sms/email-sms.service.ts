import {Injectable} from '@angular/core';
import {Searching} from '../../entities/my-pipeline/table/domain/Searching';
import {SortDirection} from '../../entities/my-pipeline/table/directives/sortable.directive';
import {DatePipe} from '@angular/common';
import {TypesSearch} from '../../entities/my-pipeline/table/domain/TypeSearch';
import {CustomNgbDateParserFormatter} from '../../utils/CustomNgbDateParserFormatter';
import {Columns, ColumnSearch, TypeColumn} from '../../entities/my-pipeline/domain/ColumnSearch';
import {EmailSmsModel} from './domain/EmailSmsModel';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {debounceTime, switchMap, tap} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {EMAIL_SMS_DATA} from './domain/dummyData';

interface SearchResult {
    emailSmsList: EmailSmsModel[];
    total: number;
}

interface State {
    page: number;
    pageSize: number;
    searchTerm: Searching;
    sortColumn: string;
    sortDirection: SortDirection;
}

@Injectable()
export class EmailSmsService {

    private _loading$ = new BehaviorSubject<boolean>(true);
    private _search$ = new Subject<void>();
    private _emailSms$ = new BehaviorSubject<EmailSmsModel[]>([]);
    private _total$ = new BehaviorSubject<number>(0);

    private _state: State = {
        page: 1,
        pageSize: 4,
        searchTerm: new Searching('', new Date(), new ColumnSearch('', TypeColumn.STRING, Columns.ALL), TypesSearch.SEARCH),
        sortColumn: '',
        sortDirection: ''
    };

    constructor(private datePipe: DatePipe, private customDatePipe: CustomNgbDateParserFormatter) {
        this._search$.pipe(
            tap(() => this._loading$.next(true)),
            debounceTime(200),
            switchMap(() => this._search()),
            tap(() => this._loading$.next(false))
        ).subscribe((result) => {
            this._emailSms$.next(result.emailSmsList);
            this._total$.next(result.total);
        });

        this._search$.next();
    }

    get emailSms$() {
        return this._emailSms$;
    }

    get total$() {
        return this._total$.asObservable();
    }

    get loading$() {
        return this._loading$.asObservable();
    }

    get page() {
        return this._state.page;
    }

    get pageSize() {
        return this._state.pageSize;
    }

    get searchTerm() {
        return this._state.searchTerm;
    }

    set page(page: number) {
        this._set({page});
    }

    set pageSize(pageSize: number) {
        this._set({pageSize});
    }

    set searchTerm(searchTerm: Searching) {
        this._set({searchTerm});
    }

    set sortColumn(sortColumn: string) {
        this._set({sortColumn});
    }

    set sortDirection(sortDirection: SortDirection) {
        this._set({sortDirection});
    }

    private _set(patch: Partial<State>) {
        Object.assign(this._state, patch);
        this._search$.next();
    }

    public _search(): Observable<SearchResult> {
        const {sortColumn, sortDirection, pageSize, page, searchTerm} = this._state;

        const emailSmsList = EMAIL_SMS_DATA;

        const total = emailSmsList.length;

        return of({emailSmsList, total});
    }

}
