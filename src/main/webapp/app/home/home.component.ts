import {Component, OnInit} from '@angular/core';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager} from 'ng-jhipster';

import {Account, LoginModalService, Principal} from '../shared';
import {CustomiseDialogComponent} from './modal/customise-dialog.component';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: [
        'home.css'
    ]

})
export class HomeComponent implements OnInit {
    account: Account;
    modalRef: NgbModalRef;
    isCallBack: boolean;
    isClientFeedback: boolean;
    isPersonalBests: boolean;
    isClientMeeting: boolean;
    isEmail: boolean;
    isTransactionReport: boolean;

    constructor(
        public principal: Principal,
        private loginModalService: LoginModalService,
        private eventManager: JhiEventManager,
        private _modalService: NgbModal
    ) {
        this.isCallBack = true;
        this.isClientFeedback = true;
        this.isPersonalBests = true;
        this.isClientMeeting = true;
        this.isEmail = true;
        this.isTransactionReport = true;
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.account = account;
        });
        this.registerAuthenticationSuccess();
    }

    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', (message) => {
            this.principal.identity().then((account) => {
                this.account = account;
            });
        });
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    customiseModal() {
        this.modalRef = this._modalService.open(CustomiseDialogComponent);
        this.modalRef.componentInstance.customiser = this.buildCustomiser();
        this.modalRef.result.then((response) => {
            this.isCallBack = response.isCallBack;
            this.isClientFeedback = response.isClientFeedback;
            this.isPersonalBests = response.isPersonalBests;
            this.isClientMeeting = response.isClientMeeting;
            this.isEmail = response.isEmail;
            this.isTransactionReport = response.isTransactionReport;
        }, (reason) => {
            console.log('Reason of close ' + reason);
        });
    }

    buildCustomiser(): object {
        return {
            'isCallBack': this.isCallBack,
            'isClientFeedback': this.isClientFeedback,
            'isPersonalBests': this.isPersonalBests,
            'isClientMeeting': this.isClientMeeting,
            'isEmail': this.isEmail,
            'isTransactionReport': this.isTransactionReport
        };
    }
}
