import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {CmaSharedModule} from '../shared';

import {HOME_ROUTE, HomeComponent} from './';
import {CustomiseDialogComponent} from './modal/customise-dialog.component';

@NgModule({
    imports: [
        CmaSharedModule,
        RouterModule.forChild([HOME_ROUTE])
    ],
    declarations: [
        HomeComponent,
        CustomiseDialogComponent
    ],
    entryComponents: [CustomiseDialogComponent],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaHomeModule {
}
