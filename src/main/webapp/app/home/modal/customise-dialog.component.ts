import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-customise-dialog',
    templateUrl: './customise-dialog.component.html'
})
export class CustomiseDialogComponent implements OnInit {

    isSaving: boolean;
    @Input() customiser = {
        'isCallBack': false,
        'isClientFeedback': false,
        'isPersonalBests': false,
        'isClientMeeting': false,
        'isEmail': false,
        'isTransactionReport' : false
    };

    constructor(
        public modal: NgbActiveModal
    ) {
    }

    ngOnInit() {
        console.log(this.customiser);
    }

}
