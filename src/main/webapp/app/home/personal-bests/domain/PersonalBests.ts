export class PersonalBests {
    constructor(public campaign: string,
                public avgCall: number,
                public conversations: number,
                public contactRate: number,
                public hoursLead: number,
                public conversionRate: number,
                public lead2Sale: number,
                public averageSale: number) {
    }
}
