export class PersonalSummary {
    constructor(public campaign: string,
                public value: number) {
    }
}
