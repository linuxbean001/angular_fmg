import {PersonalBests} from './PersonalBests';

export const PERSONALBESTSS: PersonalBests[] = [
    {
        campaign: 'Campaign 1',
        avgCall: 15.03,
        conversations: 4,
        contactRate: 0.266,
        hoursLead: 9.31,
        conversionRate: 0.0268,
        lead2Sale: 0.50,
        averageSale: 12000
    },
    {
        campaign: 'Campaign 2',
        avgCall: 16.75,
        conversations: 4,
        contactRate: 0.24,
        hoursLead: 8,
        conversionRate: 0.0342,
        lead2Sale: 0,
        averageSale: 0
    },
    {
        campaign: 'Campaign 3',
        avgCall: 12.5,
        conversations: 4,
        contactRate: 0.32,
        hoursLead: 0,
        conversionRate: 0,
        lead2Sale: 0,
        averageSale: 0
    },
    {
        campaign: 'Campaign 4',
        avgCall: 16.75,
        conversations: 4,
        contactRate: 0.2388,
        hoursLead: 4,
        conversionRate: 0.0625,
        lead2Sale: 0,
        averageSale: 0
    },
    {
        campaign: 'Campaign 5',
        avgCall: 20,
        conversations: 4,
        contactRate: 0.20,
        hoursLead: 10,
        conversionRate: 0.0333,
        lead2Sale: 0.50,
        averageSale: 4000
    }
];
