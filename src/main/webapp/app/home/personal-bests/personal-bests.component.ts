import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {FormControl} from '@angular/forms';
import {Columns} from '../../entities/my-pipeline/domain/ColumnSearch';
import {PersonalBests} from './domain/PersonalBests';
import {
    NgbdSortableHeaderDirective,
    SortDirection,
    SortEvent
} from '../../entities/my-pipeline/table/directives/sortable.directive';
import {PERSONALBESTSS} from './domain/dummyData';
import {PersonalSummary} from './domain/PersonalSummary';

interface State {
    page: number;
    pageSize: number;
    sortColumn: string;
    sortDirection: SortDirection;
}

function compare(v1, v2) {
    return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
}

function sort(personalBests: PersonalBests[], column: string, direction: string) {
    if (direction === '') {
        return personalBests;
    } else {
        return [...personalBests].sort((a, b) => {
            const res = compare(a[column], b[column]);
            return direction === 'asc' ? res : -res;
        });
    }
}

@Component({
    selector: 'jhi-personal-bests',
    templateUrl: './personal-bests.component.html',
    styleUrls: ['personal-bests.component.css']
})
export class PersonalBestsComponent implements OnInit {

    personalBestslist = new BehaviorSubject<PersonalBests[]>([]);
    personalSummary = new Array<PersonalSummary>();
    filter = new FormControl('');
    columns = Columns;
    personalBests: Array<PersonalBests>;
    private _state: State = {
        page: 1,
        pageSize: 4,
        sortColumn: '',
        sortDirection: ''
    };
    pointerClass: string;

    @ViewChildren(NgbdSortableHeaderDirective) headers: QueryList<NgbdSortableHeaderDirective>;

    constructor() {
        this.personalBests = PERSONALBESTSS;
        this.personalBestslist.next(this.personalBests);
        this.getMaxValues();
    }

    ngOnInit() {
    }

    onSort({column, direction}: SortEvent) {
        // resetting other headers
        this.headers.forEach((header) => {
            if (header.jhiSortable !== column) {
                header.direction = '';
            }
        });

        this._state.sortColumn = column;
        this._state.sortDirection = direction;

        const tmpList = sort(PERSONALBESTSS, this._state.sortColumn, this._state.sortDirection);
        this.personalBests = tmpList;
        this.personalBestslist.next(this.personalBests);
    }

    getMaxValues() {
        let tmp = this.personalBests.reduce((a, b) => a.avgCall > b.avgCall ? a : b);
        this.personalSummary.push(new PersonalSummary(tmp.campaign, tmp.avgCall));

        tmp = this.personalBests.reduce((a, b) => a.conversations > b.conversations ? a : b);
        this.personalSummary.push(new PersonalSummary(tmp.campaign, tmp.conversations));

        tmp = this.personalBests.reduce((a, b) => a.contactRate > b.contactRate ? a : b);
        this.personalSummary.push(new PersonalSummary(tmp.campaign, tmp.contactRate));

        tmp = this.personalBests.reduce((a, b) => a.hoursLead > b.hoursLead ? a : b);
        this.personalSummary.push(new PersonalSummary(tmp.campaign, tmp.hoursLead));

        tmp = this.personalBests.reduce((a, b) => a.conversionRate > b.conversionRate ? a : b);
        this.personalSummary.push(new PersonalSummary(tmp.campaign, tmp.conversionRate));

        tmp = this.personalBests.reduce((a, b) => a.lead2Sale > b.lead2Sale ? a : b);
        this.personalSummary.push(new PersonalSummary(tmp.campaign, tmp.lead2Sale));

        tmp = this.personalBests.reduce((a, b) => a.averageSale > b.averageSale ? a : b);
        this.personalSummary.push(new PersonalSummary(tmp.campaign, tmp.averageSale));
    }

    setPointer(pointer: string) {
        this.pointerClass = pointer;
    }

}
