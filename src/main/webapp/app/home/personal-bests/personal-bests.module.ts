import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {personalBestsRoute} from './personal-bests.route';
import {PersonalBestsComponent} from './personal-bests.component';
import {CmaPipelineModule} from '../../entities/my-pipeline/pipeline.module';

const ENTITY_STATES = [
    ...personalBestsRoute,
];

@NgModule({
    imports: [
        RouterModule.forChild(ENTITY_STATES),
        BrowserModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        CmaPipelineModule
    ],
    declarations: [
        PersonalBestsComponent
    ],
    entryComponents: [
        PersonalBestsComponent
    ],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaPersonalBestskModule {
}
