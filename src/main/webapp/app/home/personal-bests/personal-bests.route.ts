import {Routes} from '@angular/router';
import {PersonalBestsComponent} from './personal-bests.component';
import {UserRouteAccessService} from '../../shared';

export const personalBestsRoute: Routes = [
    {
        path: 'personal-bests',
        component: PersonalBestsComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'personal-bests.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
