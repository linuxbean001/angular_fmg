export class TransactionReportModel {
    constructor(public campaignName?: string,
                public listName?: string,
                public companyName?: string,
                public contactType?: string,
                public firstName?: string,
                public phoneNumber?: string,
                public surname?: string,
                public address?: string,
                public address2?: string,
                public city?: string,
                public country?: string,
                public email?: string,
                public postcode?: string,
                public title?: string,
                public callerDate?: number,
                public callerId?: string,
                public callerType   ?: string,
                public notes?: string) {
    }
}
