import {Component, OnInit} from '@angular/core';
import {TypeSearch} from '../../entities/my-pipeline/table/domain/TypeSearch';
import {FormControl} from '@angular/forms';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {TransactionReportService} from './transaction-report.service';
import {TransactionReportExport} from './transaction-report.export.service';
import {AllCommunityModules} from '@ag-grid-community/all-modules';
import {GridOptions, Module} from '@ag-grid-community/core';
import * as moment from 'moment';
import {LoadingOverlayComponent} from '../../utils/LoadingOverlayComponent';

@Component({
    selector: 'jhi-transaction-report',
    templateUrl: './transaction-report.component.html',
    styleUrls: ['transaction-report.component.css']
})
export class TransactionReportComponent implements OnInit {

    filter = new FormControl('');
    typesSearch = Array<TypeSearch>();
    modalRef: NgbModalRef;
    gridOptions: GridOptions;
    private gridApi;
    private frameworkComponents;
    private loadingOverlayComponent;
    private loadingOverlayComponentParams;

    public modules: Module[] = AllCommunityModules;

    columnDefs = [
        {headerName: 'Campaign', field: 'campaignName'},
        {headerName: 'List', field: 'listName'},
        {headerName: 'Company Name', field: 'companyName'},
        {headerName: 'Contact Type', field: 'contactType'},
        {headerName: 'First Name', field: 'firstName'},
        {headerName: 'Phone Number', field: 'phoneNumber'},
        {headerName: 'Surname', field: 'surname'},
        {headerName: 'Address', field: 'address'},
        {headerName: 'Address2', field: 'address2'},
        {headerName: 'City', field: 'city'},
        {headerName: 'Country', field: 'country'},
        {headerName: 'Email', field: 'email'},
        {headerName: 'PostCode', field: 'postcode'},
        {headerName: 'Title', field: 'title'},
        {
            headerName: 'Date',
            field: 'callerDate',
            filter: 'agDateColumnFilter',
            valueFormatter: (params) => {
                return moment(params.value).format('DD/MM/YYYY');
            },
            filterParams: {
                comparator: (filterLocalDateAtMidnight, cellValue) => {
                    const dateParts = moment(cellValue).format('DD/MM/YYYY').split('/');
                    const day = Number(dateParts[0]);
                    const month = Number(dateParts[1]) - 1;
                    const year = Number(dateParts[2]);
                    const cellDate = new Date(year, month, day);
                    if (cellDate < filterLocalDateAtMidnight) {
                        return -1;
                    } else if (cellDate > filterLocalDateAtMidnight) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            }
        },
        {headerName: 'CallerId', field: 'callerId', filter: 'agNumberColumnFilter'},
        {headerName: 'Type', field: 'callerType'},
        {headerName: 'Notes', field: 'notes'},
    ];

    constructor(private _modalService: NgbModal, public service: TransactionReportService, private exportService: TransactionReportExport) {
        this.frameworkComponents = {
            customLoadingOverlay: LoadingOverlayComponent
        };
        this.loadingOverlayComponent = 'customLoadingOverlay';
        this.loadingOverlayComponentParams = {loadingMessage: 'One moment please...'};
        this.gridOptions = <GridOptions>{
            enableRangeSelection: true,
            columnDefs: this.columnDefs,
            onGridReady: (params) => {
                service.loadReports();
                service.transactionReportList$.subscribe(
                    (rowData) => {
                        if (this.gridOptions.api) { // can be null when tabbing between the examples
                            this.gridOptions.api.setRowData(rowData);
                            this.gridOptions.api.hideOverlay();
                        }
                    }
                );
                this.gridApi = params.api;
                this.gridOptions.api.showLoadingOverlay();
            },
            pagination: true,
            paginationAutoPageSize: true,
            defaultColDef: {
                sortable: true,
                filter: true,
                resizable: true
            },
            frameworkComponents: this.frameworkComponents,
            loadingOverlayComponent: this.loadingOverlayComponent,
            loadingOverlayComponentParams: this.loadingOverlayComponentParams
        };
    }

    ngOnInit() {
    }

    private createColumnDefs() {
        return [
            {headerName: 'Code', field: 'code', width: 70, resizable: true},
            {headerName: 'Name', field: 'name', width: 280, resizable: true},
            {
                headerName: 'Bid', field: 'bid', width: 100, resizable: true,
                cellClass: 'cell-number',
                // valueFormatter: this.numberFormatter,
                cellRenderer: 'agAnimateShowChangeCellRenderer'
            },
            {
                headerName: 'Mid', field: 'mid', width: 100, resizable: true,
                cellClass: 'cell-number',
                cellRenderer: 'agAnimateShowChangeCellRenderer'
            },
            {
                headerName: 'Ask', field: 'ask', width: 100, resizable: true,
                cellClass: 'cell-number',
                cellRenderer: 'agAnimateShowChangeCellRenderer'
            },
            {
                headerName: 'Volume', field: 'volume', width: 100, resizable: true,
                cellClass: 'cell-number',
                cellRenderer: 'agAnimateSlideCellRenderer'
            }
        ];
    }

    getPdf() {
        this.exportService.pdf().subscribe((theBlob) => {
            const file = new Blob([theBlob], {type: 'application/pdf'});
            const fileURL = URL.createObjectURL(file);
            window.open(fileURL);
        });
    }

    getCsv() {
        const params = this.getParams();
        this.gridApi.exportDataAsCsv(params);
    }

    getParams() {
        return {
            suppressQuotes: '',
            columnSeparator: '',
            customHeader: ('Transaction Report')
        };
    }

}
