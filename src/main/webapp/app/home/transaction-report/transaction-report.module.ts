import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {NgbDateParserFormatter, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CustomNgbDateParserFormatter} from '../../utils/CustomNgbDateParserFormatter';
import {TransactionReportService} from './transaction-report.service';
import {CmaHeaderConsoleModule} from '../../entities/header-console/header-console.module';
import {transactionReportRoute} from './transaction-report.route';
import {CmaPipelineModule} from '../../entities/my-pipeline/pipeline.module';
import {TransactionReportExport} from './transaction-report.export.service';
import {TransactionReportComponent} from './transaction-report.component';
import {AgGridModule} from '@ag-grid-community/angular';
import {LoadingOverlayComponent} from '../../utils/LoadingOverlayComponent';

const ENTITY_STATES = [
    ...transactionReportRoute,
];

@NgModule({
    imports: [
        RouterModule.forChild(ENTITY_STATES),
        BrowserModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        CmaHeaderConsoleModule,
        CmaPipelineModule,
        AgGridModule.withComponents(
            [
                TransactionReportComponent,
                LoadingOverlayComponent
            ]
        )
    ],
    declarations: [
        TransactionReportComponent,
        LoadingOverlayComponent
    ],
    providers: [TransactionReportService, TransactionReportExport],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CmaTransactionReportModule {
}
