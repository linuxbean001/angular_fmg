import {Routes} from '@angular/router';
import {UserRouteAccessService} from '../../shared';
import {TransactionReportComponent} from './transaction-report.component';

export const transactionReportRoute: Routes = [
    {
        path: 'transaction-report',
        component: TransactionReportComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'transaction-report.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
