import {Injectable} from '@angular/core';
import {Searching} from '../../entities/my-pipeline/table/domain/Searching';
import {SortDirection} from '../../entities/my-pipeline/table/directives/sortable.directive';
import {DatePipe} from '@angular/common';
import {TypesSearch} from '../../entities/my-pipeline/table/domain/TypeSearch';
import {CustomNgbDateParserFormatter} from '../../utils/CustomNgbDateParserFormatter';
import {Columns, ColumnSearch, TypeColumn} from '../../entities/my-pipeline/domain/ColumnSearch';
import {TransactionReportModel} from './domain/TransactionReportModel';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {switchMap} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {HttpClient, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {SERVER_API_URL} from '../../app.constants';
import {JhiAlertService} from 'ng-jhipster';

interface SearchResult {
    transactionReportList: TransactionReportModel[];
    total: number;
}

interface State {
    page: number;
    pageSize: number;
    searchTerm: Searching;
    sortColumn: string;
    sortDirection: SortDirection;
}

@Injectable()
export class TransactionReportService {

    private _loading$ = new BehaviorSubject<boolean>(true);
    private _search$ = new Subject<void>();
    private _transactionReportList$ = new BehaviorSubject<TransactionReportModel[]>([]);
    private tmpTransactionReportList = new Array<TransactionReportModel>();
    private _total$ = new BehaviorSubject<number>(0);
    private resourceUrl = SERVER_API_URL + 'api/transaction-report/';

    private _state: State = {
        page: 1,
        pageSize: 4,
        searchTerm: new Searching('', new Date(), new ColumnSearch('', TypeColumn.STRING, Columns.ALL), TypesSearch.SEARCH),
        sortColumn: '',
        sortDirection: ''
    };

    constructor(private datePipe: DatePipe, private customDatePipe: CustomNgbDateParserFormatter, private http: HttpClient, private jhiAlertService: JhiAlertService) {
        this._search$.pipe(
            switchMap(() => this._search())
        ).subscribe((result) => {
            this._transactionReportList$.next(result.transactionReportList);
            this._total$.next(result.total);
        });

        this.loadReports();
    }

    get transactionReportList$(): BehaviorSubject<TransactionReportModel[]> {
        return this._transactionReportList$;
    }

    private _search(): Observable<SearchResult> {
        const total = 0;

        return of({transactionReportList: this.tmpTransactionReportList, total});
    }

    transactionsResponse(): Observable<HttpResponse<TransactionReportModel[]>> {
        return this.http.get<TransactionReportModel[]>(this.resourceUrl, {observe: 'response'});
    }

    loadReports() {
        this.transactionsResponse().subscribe((res: HttpResponse<TransactionReportModel[]>) => {
            this.tmpTransactionReportList = res.body;
            this._search$.next();
        }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

}
