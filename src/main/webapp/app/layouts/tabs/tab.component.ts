/* tslint:disable:no-input-rename */
import {Component, Input} from '@angular/core';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'tab',
    styles: [`
        .pane {
            padding: 1em;
        }
    `],
    template: `
        <div [hidden]="!active" class="pane" [ngClass]="paneClass">
            <ng-content></ng-content>
        </div>
    `
})
// tslint:disable-next-line:component-class-suffix
export class Tab {

    title: string;

    @Input('tabTitle')
    set wizardState(value: string) {
        this.title = value;
        this.ident = value;
    }

    @Input('tabIdent') ident: string;
    @Input('tabData') data: any;
    @Input() tabClass: string;
    @Input() paneClass: string;
    @Input() active = false;
    @Input() disabled = false;
}
