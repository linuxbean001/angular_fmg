import {NgModule} from '@angular/core';
import {Tab} from './tab.component';
import {Tabs} from './tabs.component';
import {CmaSharedModule} from '../../shared';

@NgModule({
    imports: [
        CmaSharedModule,
    ],
    declarations: [
        Tab,
        Tabs,
    ],
    exports: [
        Tab,
        Tabs,
    ]
})
export class TabModule {
}
