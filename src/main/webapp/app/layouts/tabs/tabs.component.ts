import {AfterContentInit, Component, ContentChildren, EventEmitter, Output, QueryList} from '@angular/core';
import {Tab} from './tab.component';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'tabs',
    template: `
        <ul class="nav nav-tabs" role="tablist">
            <li *ngFor="let tab of tabs" (click)="tabClick(tab)" class="nav-item" [ngClass]="tab.tabClass">
                <a class="nav-link" data-toggle="tab" role="tab"
                   [class.active]="tab.active"
                   [class.disabled]="tab.disabled">{{tab.title}}</a>
            </li>
        </ul>

        <ng-content></ng-content>
    `
})
// tslint:disable-next-line:component-class-suffix
export class Tabs implements AfterContentInit {

    @ContentChildren(Tab) tabs: QueryList<Tab>;
    @Output() onTabClick: EventEmitter<TabClickEvent> = new EventEmitter();
    @Output() tabChanged: EventEmitter<TabChangedEvent> = new EventEmitter();

    // contentChildren are set
    ngAfterContentInit() {
        // get all active tabs
        const activeTabs = this.tabs.filter((tab) => tab.active);

        // if there is no active tab set, activate the first
        if (activeTabs.length === 0) {
            this.selectTab(this.tabs.first);
        }
    }

    find(ident: string): Tab {
        return this.tabs.toArray().find((tab) => tab.ident === ident);
    }

    tabClick(tab: Tab) {
        this.onTabClick.emit(new TabClickEvent(this.getCurrentTab(), tab));
    }

    selectTab(tab: Tab, data?: any) {
        if (!tab || tab.disabled) {
            return;
        }

        const currentTab = this.getCurrentTab();

        // deactivate all tabs
        this.tabs.toArray().forEach((t) => t.active = false);

        // activate the tab the user has clicked on.
        tab.active = true;

        this.tabChanged.emit(new TabChangedEvent(currentTab, tab, data));
    }

    getCurrentTab() {
        const activeTabs = this.tabs.filter((tab) => tab.active);
        if (activeTabs.length > 0) {
            return activeTabs[0];
        }
    }

    getFirstActiveTabIndex() {
        let firstActiveTabIndex = -1;
        this.tabs.forEach((tab, index) => {
            if (firstActiveTabIndex === -1 && tab.active) {
                firstActiveTabIndex = index;
            }
        });
        return firstActiveTabIndex;
    }

    getNextTab() {
        const firstActiveTabIndex = this.getFirstActiveTabIndex();

        if (firstActiveTabIndex < this.tabs.length - 1) {
            return this.tabs.toArray()[firstActiveTabIndex + 1];
        }
    }

    hasNextTab() {
        return this.getFirstActiveTabIndex() < this.tabs.length - 1;
    }

    nextTab() {
        const firstActiveTabIndex = this.getFirstActiveTabIndex();

        if (firstActiveTabIndex < this.tabs.length - 1) {
            const currentActiveTab = this.getCurrentTab();
            const nextTab = this.tabs.toArray()[firstActiveTabIndex + 1];

            this.tabClick(nextTab);
            // this.selectTab(nextTab);
        }
    }

    hasPrevTab() {
        return this.getFirstActiveTabIndex() > 0;
    }

    prevTab() {
        const firstActiveTabIndex = this.getFirstActiveTabIndex();

        if (firstActiveTabIndex > 0) {
            const prevTab = this.tabs.toArray()[firstActiveTabIndex - 1];

            this.tabClick(prevTab);
            // this.selectTab(prevTab);
        }
    }

    isLastTab() {
        return this.getFirstActiveTabIndex() === this.tabs.length - 1;
    }

    finish() {

    }
}

export class TabClickEvent {
    public constructor(
        public from: Tab,
        public to: Tab,
    ) {
    }
}

export class TabChangedEvent {
    public constructor(
        public from: Tab,
        public to: Tab,
        public data?: any,
    ) {
    }
}
