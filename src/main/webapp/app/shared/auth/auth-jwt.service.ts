import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {LocalStorageService, SessionStorageService} from 'ngx-webstorage';
import {SERVER_API_URL} from '../../app.constants';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ConfirmModalComponent, ConfirmOptions, ConfirmStateService} from '..';
import {Principal} from './principal.service';
import {Router} from '@angular/router';

@Injectable()
export class AuthServerProvider {
    private authenticationToken = 'authenticationToken';
    private authenticationExpiration = 'authenticationExpiration';
    private authenticationRememberMe = 'authenticationRememberMe';

    private COUNTDOWN_SECONDS = 60;
    // private COUNTDOWN_SECONDS = 8;

    private SKEW_SECONDS = 5;
    private timeout;
    private countDownTimeout;

    constructor(
        private http: HttpClient,
        private $localStorage: LocalStorageService,
        private $sessionStorage: SessionStorageService,
        private modalService: NgbModal,
        private principal: Principal,
        private router: Router,
    ) {
    }

    getToken() {
        return this.$localStorage.retrieve(this.authenticationToken) || this.$sessionStorage.retrieve(this.authenticationToken);
    }

    getExpiration() {
        return this.$localStorage.retrieve(this.authenticationExpiration) || this.$sessionStorage.retrieve(this.authenticationExpiration);
    }

    getRememberMe() {
        return this.$localStorage.retrieve(this.authenticationRememberMe) || this.$sessionStorage.retrieve(this.authenticationRememberMe);
    }

    login(credentials): Observable<any> {
        const data = {
            username: credentials.username,
            password: credentials.password,
            rememberMe: credentials.rememberMe
        };
        return this.http.post(SERVER_API_URL + 'api/authenticate', data, {observe: 'response'})
            .map((resp) => this.authenticateSuccess(resp, credentials.rememberMe));
    }

    loginWithToken(jwt, rememberMe) {
        if (jwt) {
            this.storeAuthenticationToken(jwt, rememberMe, null);
            return Promise.resolve(jwt);
        } else {
            return Promise.reject('auth-jwt-service Promise reject'); // Put appropriate error message here
        }
    }

    refresh(data: { rememberMe: boolean }): Observable<any> {
        return this.http.post(SERVER_API_URL + 'api/refresh', data, {observe: 'response'})
            .map((resp) => this.authenticateSuccess(resp, this.getRememberMe()));
    }

    authenticateSuccess(resp, rememberMe) {
        const {id_token, expiration} = resp.body;
        this.storeAuthenticationToken(id_token, rememberMe, expiration);
        this.reStartTimer(expiration);
        return {id_token, expiration};
    }

    reStartTimer(expiration) {
        // @ts-ignore
        const jwtExpireInSeconds = Math.floor((new Date(expiration) - new Date()) / 1000);
        const countDownWarningInSeconds = jwtExpireInSeconds - this.COUNTDOWN_SECONDS - this.SKEW_SECONDS;
        console.log(`Session will expire in ${jwtExpireInSeconds} sec, warning popup will be shown in ${countDownWarningInSeconds} sec`);

        // https://stackoverflow.com/questions/3468607/why-does-settimeout-break-for-large-millisecond-delay-values
        // show warning only if remember me (30 days by default) is not used
        const countDownWarningInMs = countDownWarningInSeconds * 1000;
        if (countDownWarningInMs <= 2147483647) {
            clearTimeout(this.timeout);
            this.timeout = setTimeout(() => this.showWarning(), countDownWarningInMs);

            this.doCountDown(expiration);
        } else {
            console.log(`We'll not display warning because user is used RememberMe (or token validity is too big)`);
        }
    }

    doCountDown(expiration) {
        // @ts-ignore
        const jwtExpireInSeconds = Math.floor((new Date(expiration) - new Date()) / 1000);

        console.log(`Warning popup will be shown in ${jwtExpireInSeconds} sec`);

        clearTimeout(this.countDownTimeout);
        this.countDownTimeout = setTimeout(() => this.doCountDown(expiration), 5000);
    }

    showWarning() {
        // tslint:disable-next-line:prefer-const
        let secondsToExpire = this.COUNTDOWN_SECONDS - this.SKEW_SECONDS;
        const options: ConfirmOptions = {
            title: 'Your session is about to expire',
            message: this.warningMessage(secondsToExpire),
            yesCaption: `Yes, keep me signed in`,
            noCaption: `No, sign me out`
        };

        console.log(`Showing warning popup`);

        // const modalRef = this.modalService.open(JhiLoginModalComponent, {
        //     backdrop: 'static',
        //     keyboard: false
        // });
        const modalRef = this.modalService.open(ConfirmModalComponent, {
            backdrop: 'static',
            keyboard: false
        });
        modalRef.componentInstance.options = options;
        modalRef.componentInstance.state = {modal: modalRef} as ConfirmStateService;

        const interval = setInterval(() => {
            secondsToExpire--;
            if (secondsToExpire === 0) {
                clearInterval(interval);
                modalRef.dismiss('auto logout');
                return;
            }
            // it can be undefined? just in case
            if (modalRef) {
                modalRef.componentInstance.options = {...options, message: this.warningMessage(secondsToExpire)};
            }
        }, 1000);

        modalRef.result
            .then((confirmResult) => {
                console.log(confirmResult);

                clearInterval(interval);

                // just call backend to renew a session
                this.refresh({rememberMe: this.getRememberMe()}).toPromise()
                    .then((accountResult) => {
                        // TODO?
                    })
                    .catch((reason) => {
                        // TODO
                    });
            })
            .catch((reason) => {
                console.log(reason);

                // immediately logout, see NavbarComponent.logout
                this.logout();
                this.principal.authenticate(null);
                this.router.navigate(['']);
            });
    }

    warningMessage(secondsToExpire: number) {
        return `You will be logged out in ${secondsToExpire} seconds. Do you want to stay signed in?`;
    }

    storeAuthenticationToken(jwt, rememberMe, expiration) {
        if (rememberMe) {
            this.$localStorage.store(this.authenticationToken, jwt);
            this.$localStorage.store(this.authenticationExpiration, expiration);
            this.$localStorage.store(this.authenticationRememberMe, rememberMe);
        } else {
            this.$sessionStorage.store(this.authenticationToken, jwt);
            this.$sessionStorage.store(this.authenticationExpiration, expiration);
            this.$sessionStorage.store(this.authenticationRememberMe, rememberMe);
        }
    }

    logout(): Observable<any> {
        return new Observable((observer) => {
            this.$localStorage.clear(this.authenticationToken);
            this.$sessionStorage.clear(this.authenticationToken);
            this.$localStorage.clear(this.authenticationExpiration);
            this.$sessionStorage.clear(this.authenticationExpiration);
            this.$localStorage.clear(this.authenticationRememberMe);
            this.$sessionStorage.clear(this.authenticationRememberMe);
            observer.complete();
        });
    }
}
