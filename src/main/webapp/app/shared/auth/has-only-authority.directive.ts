import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';
import {Principal} from './principal.service';

/**
 * @whatItDoes Conditionally includes an HTML element if current user has only
 * of the authorities passed as the `expression`.
 *
 * @howToUse
 * ```
 *     <some-element *jhiHasOnlyAuthority="'ROLE_ADMIN'">...</some-element>
 *
 *     <some-element *jhiHasOnlyAuthority="['ROLE_ADMIN', 'ROLE_USER']">...</some-element>
 * ```
 */
@Directive({
    selector: '[jhiHasOnlyAuthority]'
})
export class HasOnlyAuthorityDirective {

    private authorities: string[];

    constructor(private principal: Principal, private templateRef: TemplateRef<any>, private viewContainerRef: ViewContainerRef) {
    }

    @Input()
    set jhiHasOnlyAuthority(value: string | string[]) {
        this.authorities = typeof value === 'string' ? [<string> value] : <string[]> value;
        this.updateView();
        // Get notified each time authentication state changes.
        this.principal.getAuthenticationState().subscribe((identity) => this.updateView());
    }

    private updateView(): void {
        this.principal.hasOnlyAuthority(this.authorities).then((result) => {
            this.viewContainerRef.clear();
            if (result) {
                this.viewContainerRef.createEmbeddedView(this.templateRef);
            }
        });
    }
}
