/**
 * The component displayed in the confirmation modal opened by the ConfirmService.
 */
import {Component} from '@angular/core';
import {ConfirmStateService} from './confirm-state.service';
import {ConfirmOptions} from './confirm-options';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'confirm-modal-component',
    templateUrl: './confirm-modal.component.html'
})
export class ConfirmModalComponent {

    options: ConfirmOptions;

    constructor(private state: ConfirmStateService) {
        this.options = state.options;
    }

    yes() {
        this.state.modal.close('yes');
    }

    no() {
        this.state.modal.dismiss('not confirmed');
    }
}
