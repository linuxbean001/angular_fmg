import {Directive, TemplateRef} from '@angular/core';
import {ConfirmStateService} from './confirm-state.service';

/**
 * Directive allowing to get a reference to the template containing the confirmation modal component,
 * and to store it into the internal confirm state service. Somewhere in the view, there must be
 *
 * ```
 * <ng-template confirm>
 *   <confirm-modal-component></confirm-modal-component>
 * </ng-template>
 * ```
 *
 * in order to register the confirm template to the internal confirm state
 */
@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[confirm]'
})
export class ConfirmTemplateDirective {
    constructor(confirmTemplate: TemplateRef<any>, state: ConfirmStateService) {
        state.template = confirmTemplate;
    }
}
