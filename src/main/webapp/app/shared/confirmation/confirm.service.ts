/**
 * A confirmation service, allowing to open a confirmation modal from anywhere and get back a promise.
 */
import {Injectable} from '@angular/core';
import {NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {ConfirmStateService} from './confirm-state.service';
import {ConfirmOptions} from './confirm-options';

@Injectable()
export class ConfirmService {

    constructor(private modalService: NgbModal, private state: ConfirmStateService) {
    }

    /**
     * Opens a confirmation modal
     * @param options the options for the modal (title and message)
     * @param modalOptions modal options
     * @returns {Promise<any>} a promise that is fulfilled when the user chooses to confirm, and rejected when
     * the user chooses not to confirm, or closes the modal
     */
    confirm(options: ConfirmOptions, modalOptions?: NgbModalOptions): Promise<any> {
        this.state.options = options;
        this.state.modal = this.modalService.open(this.state.template, modalOptions);
        return this.state.modal.result;
    }
}
