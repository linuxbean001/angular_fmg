import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {DatePipe} from '@angular/common';
import {ConfirmStateService} from './confirmation/confirm-state.service';
import {ConfirmModalComponent} from './confirmation/confirm-modal.component';
import {ConfirmService} from './confirmation/confirm.service';
import {ConfirmTemplateDirective} from './confirmation/confirm-template.directive';

import {
    AccountService,
    AuthServerProvider,
    CmaSharedCommonModule,
    CmaSharedLibsModule,
    CSRFService,
    HasAnyAuthorityDirective,
    HasOnlyAuthorityDirective,
    JhiLoginModalComponent,
    LoginModalService,
    LoginService,
    Principal,
    StateStorageService,
    UserService,
} from './';

@NgModule({
    imports: [
        CmaSharedLibsModule,
        CmaSharedCommonModule
    ],
    declarations: [
        JhiLoginModalComponent,
        HasAnyAuthorityDirective,
        HasOnlyAuthorityDirective,
        ConfirmTemplateDirective,
        ConfirmModalComponent,
    ],
    providers: [
        LoginService,
        LoginModalService,
        AccountService,
        StateStorageService,
        Principal,
        CSRFService,
        AuthServerProvider,
        UserService,
        DatePipe,
        ConfirmService,
        ConfirmStateService,
    ],
    entryComponents: [
        JhiLoginModalComponent,
        ConfirmModalComponent,
    ],
    exports: [
        CmaSharedCommonModule,
        JhiLoginModalComponent,
        HasAnyAuthorityDirective,
        HasOnlyAuthorityDirective,
        ConfirmTemplateDirective,
        ConfirmModalComponent,
        DatePipe
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class CmaSharedModule {
}
