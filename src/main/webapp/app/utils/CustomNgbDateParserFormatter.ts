import {NgbDateParserFormatter, NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date-struct';
import {Injectable} from '@angular/core';
import {NgbTime} from '@ng-bootstrap/ng-bootstrap/timepicker/ngb-time';

@Injectable()
export class CustomNgbDateParserFormatter extends NgbDateParserFormatter {
    parse(value: string): NgbDateStruct {
        if (value) {
            const data = value.split('/');
            return {year: parseInt(data[2], 10), month: (parseInt(data[1], 10)), day: parseInt(data[0], 10)};
        }
        return null;
    }

    parseDate(value: string): Date {
        if (value) {
            const data = value.split('/');
            return new Date(parseInt(data[2], 10), (parseInt(data[1], 10) - 1), parseInt(data[0], 10));
        }
        return null;
    }

    format(date: any): string {
        if (date) {
            return this.minNumber(date.day) + '/' + this.minNumber(date.month) + '/' + date.year;
        }
        return '';
    }

    timeFormat(timer: NgbTimeStruct): string {
        if (timer) {
            return timer.hour + ':' + timer.minute + ':' + timer.second;
        }
        return '';
    }

    minNumber(num): string {
        if (num) {
            if (parseInt(num, 10) <= 9) {
                return '0' + num;
            } else {
                return num;
            }
        }
        return num;
    }
}
