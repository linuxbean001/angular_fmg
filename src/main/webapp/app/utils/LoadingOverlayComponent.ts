import { Component, Input } from '@angular/core';
import { ILoadingOverlayAngularComp } from '@ag-grid-community/angular';

@Component({
    selector: 'jhi-loading-overlay',
    template: `<div class="ag-custom-loading-cell" style="padding-left: 10px; line-height: 25px;">` +
        `   <i class="fa fa-spinner fa-spin fa-2x fa-fw"></i> <span> {{this.params.loadingMessage}} </span>` +
        `</div>`
})
export class LoadingOverlayComponent implements ILoadingOverlayAngularComp {

    params: any;

    agInit(params): void {
        this.params = params;
    }
}
