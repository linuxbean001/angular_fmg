package com.fmg.cma.service.mapper;

import com.fmg.cma.domain.CampaignPortalUser;
import com.fmg.cma.domain.ClientCampaign;
import com.fmg.cma.domain.ClientList;
import com.fmg.cma.domain.User;
import com.fmg.cma.repository.*;
import com.fmg.cma.service.dto.ClientCampaignDTO;
import com.fmg.cma.service.dto.ClientListDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ClientCampaignMapperImpl.class, UserMapper.class, UserCompanyMapperImpl.class, ClientCompanyMapperImpl.class,
    ClientCompanyStatusMapperImpl.class, IndustryMapperImpl.class, ClientListMapperImpl.class, EmailTemplateMapperImpl.class,
    OutcomeMapperImpl.class, ClientCampaignTemplateMapperImpl.class, OutcomeTypeMapperImpl.class,
    OutcomeSubReasonMapperImpl.class, ClientCampaignScriptMapperImpl.class, AppointmentAttendeeMapperImpl.class,
    ClientCampaignAddDocMapperImpl.class})
public class ClientCampaignMapperTest {


    @MockBean
    private ClientCampaignRepository clientCampaignRepository;
    @MockBean
    private UserRepository userRepository;

    @MockBean
    private ClientCompanyRepository clientCompanyRepository;

    @MockBean
    private ClientListRepository clientListRepository;

    @MockBean
    private EmailTemplateRepository emailTemplateRepository;
    @MockBean
    private OutcomeRepository outcomeRepository;
    @MockBean
    private ClientCampaignTemplateRepository clientCampaignTemplateRepository;
    @MockBean
    private OutcomeTypeRepository outcomeTypeRepository;

    @MockBean
    private ClientCampaignScriptRepository clientCampaignScriptRepository;

    @MockBean
    private ClientCampaignAddDocRepository clientCampaignAddDocRepository;



    @Autowired
    private ClientCampaignMapper clientCampaignMapper;




    @Test
    public void toDto() {

        User user = new User();
        user.setFirstName("BOBFIRST");
        user.setFirstName("LAST");
        user.setId(1L);
        Mockito.when(userRepository.getOne(1L)).thenReturn(user);

        Set<User> users = new HashSet<>();
        users.add(user);


        ClientCampaign clientCampaign = new ClientCampaign();
        clientCampaign.setName("BOB");
        clientCampaign.setId(1L);
        clientCampaign.setUsers(users);
        Mockito.when(clientCampaignRepository.getOne(1L)).thenReturn(clientCampaign);

        Mockito.when(clientCampaignRepository.findByUsers_Id(1L)).thenReturn(Arrays.asList(clientCampaign));




        Set<CampaignPortalUser> portalUsers1 = new HashSet<>();
        CampaignPortalUser campaignPortalUser = new CampaignPortalUser();
        campaignPortalUser.setEmail("TESTTEST@gmail.com");
        portalUsers1.add(campaignPortalUser);

        Set<ClientList> clientLists = new HashSet<>();
        ClientList clientList = new ClientList();
        clientList.setName("LIST1");
        clientList.setPortalUsers(portalUsers1);
        clientLists.add(clientList);
        clientCampaign.setClientLists(clientLists);





        ClientCampaignDTO clientCampaignDTO = clientCampaignMapper.toDto(clientCampaign);
        System.out.println("JON!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!11 clientCampaignDTO:" +  clientCampaignDTO);


        assertNotNull(clientCampaignDTO);

        assertNotNull(clientCampaignDTO.getClientLists());
        assertEquals(clientCampaignDTO.getClientLists().size(), 1);
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!GOT LISTS" + clientCampaignDTO.getClientLists());


        List<ClientListDTO> clientListDTO = clientCampaignDTO.getClientLists();
        for (ClientListDTO clientListDto : clientListDTO) {
            assertNotNull(clientListDto);
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!PORTALA USERS " + (clientListDto.getPortalUsers()));
            assertNotNull(clientListDto.getPortalUsers());
            assertEquals(clientListDto.getPortalUsers().size(), 1);
        }


    }
}
